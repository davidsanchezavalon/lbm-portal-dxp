 <%--
 /**
  * Description
  *		This file contain the Prior Authorization Queue portlet.
  *
  * CHANGE History
  * 	Version 1.0
  *			Initial version.
  * 	Version 1.1  
  *			When no search type is selected, display in-process records when the search button is selected.
  *			Change the initial page length of the results table to 25.
  * 	Version 1.2  
  *			Fixed the display for a PA with an error.
  *		Version 1.3						01/30/2018
  *			Disable Find Member button until all required fields are valid.
  *			When the Find Member button is disabled show a mouse over tooltip with what is missing.
  *			Added Enter key functionality.
  *		Version 1.4						06/06/2018
  *			Change the cancel button from a hyperlink to a button.
  *		Version 1.5						07/16/2018
  *			Move the label to the left of the field.
  *
  */
--%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<%@ page import="com.avalon.priorAuth.search.beans.QueueSearchConstants" %>
<%@ page import="com.avalon.priorAuth.search.utils.QueueUtil" %>

<portlet:defineObjects />
<liferay-theme:defineObjects/>

<portlet:resourceURL var="queueSearchActionURL" id="queueSearchAction">
</portlet:resourceURL>

<portlet:actionURL var="getAuthorizationDetailsURL">
	<portlet:param name="action" value="getAuthorizationDetails" />
	<portlet:param name="authorizationNumberLink" value="0" />
	<portlet:param name="idCardNumberLink" value="0" />
</portlet:actionURL>

<portlet:actionURL var="cancelURL">
	<portlet:param name="action" value="cancelPage" />
</portlet:actionURL>


<script>
	<%
		final int NUM_LEN = 17;
	%>
</script>

<aui:script>
	var toolTip = "";
	var browserStr = getBrowser();
	var bogusAuthorizationNumber = "99999999999999999"
	var bogusIdCardNumber = "REQID";

	function getNamespace() {
		return('<portlet:namespace/>');
	}

	$(function() {

		// Show date picker for IE and Firefox
		if ((browserStr == "IE") || (browserStr == "Firefox") || (browserStr == "unknown")) {

			$("#<portlet:namespace/>authDateReceived").datepicker({
				showOn: "button",
				buttonImage: "<%=request.getContextPath()%>	/images/icondatepicker.png",
				buttonImageOnly : true,
				dateFormat: 'mm/dd/yy',
				changeMonth: true,
				changeYear: true,
				onSelect: function(selected) {
					
					// Cause the validation on the date fields
					processItem("<portlet:namespace/>authDateReceived", "<portlet:namespace/>authDateReceived");
					setSearchButtonStatus();
				}
			});
			$("#<portlet:namespace/>authDateReceived").attr("placeholder", "mm/dd/yyyy");
		}

		// Clear the search field
		showSearchField();

		// Disable the search button until the fields are valid
		setSearchButtonStatus();

		// Set the initial hidden authorization type
		setAuthType();
		
		// Determine if the results table should be shown.
		var errorMsg = '<%=renderRequest.getParameter("message")%>';

		if (errorMsg == "null") {
			errorMsg = "";
			$('#showResults').hide();
		}
		
		// Set the initial radio button selection
		$("#radioSelectInProcess").prop("checked", true);
		setAuthorizationStatusType("inProcessRecords");
		
		if (($("#<portlet:namespace/>searchType").val() == "") && (errorMsg == "")) {
			
			// Display all in-process PA records if no search type is selected and there is no error message displayed
			$("#<portlet:namespace/>authType").prop("selectedIndex", 1);
			setAuthType();
			updateQueue("1");
		}
		
		// Execute the search when the enter key is pressed for the authorization number
		processEnterKey("authorizationNumber");
		
		// Execute the search when the enter key is pressed for the date received
		processEnterKey("authDateReceived");
	});

	YUI().ready('aui-node', 'event', function(Y) {
		var msg = '<%=renderRequest.getParameter("message")%>';
		if (msg == 'null') {
			document.getElementById("errorText").style.display = 'none';
		} else {
			document.getElementById("errorText").innerHTML = msg;
		}
	});

	function showSearchField() {
		var searchType = $("#<portlet:namespace/>searchType").val();
		
		// Hide the error, results and loader message when the auth Status is changed
		$("#queueSearchResults").hide();
		$("#errorMessage").hide();
		$("#dataloader").hide();

		if (searchType == "") {
			
			// Disable all but the in-process radio button
			$("#radioSelectCompleted").prop("disabled", "true");
			$("#radioSelectAll").prop("disabled", "true");
			
			// Select the in-process radio button
			$("#radioSelectInProcess").prop("checked", true);
			setAuthorizationStatusType("inProcessRecords");
		} else {
			
			// Enable any disabled radio button
			$("#radioSelectCompleted").removeProp("disabled");
			$("#radioSelectAll").removeProp("disabled");
		}

		// Show the inputs for the selected search type
		$("#authorizationNumberField").hide();
		$("#workedByField").hide();
		$("#priorityLevelField").hide();
		$("#dateReceivedField").hide();
		$("#memberIdField").hide();
		if (searchType == "<%= QueueSearchConstants.AUTH_NUMBER %>") {
			$("#authorizationNumberField").show();
		} else if (searchType == "<%= QueueSearchConstants.WORKED_BY %>") {
			$("#workedByField").show();
		} else if (searchType == "<%= QueueSearchConstants.PRIORITY_LEVEL %>") {
			$("#priorityLevelField").show();
		} else if (searchType == "<%= QueueSearchConstants.DATE_RECEIVED %>") {
			$("#dateReceivedField").show();
		} else if (searchType == "<%= QueueSearchConstants.CARD_NUMBER %>") {
			$("#memberIdField").show();
		}
	}
	
	function clearSearchField() {
		var searchType = $("#<portlet:namespace/>searchType").val();
		
		if (searchType == "<%= QueueSearchConstants.AUTH_NUMBER %>") {
			
			// Clear the text field
			$("#<portlet:namespace/>authorizationNumber").val("");

		} else if (searchType == "<%= QueueSearchConstants.WORKED_BY %>") {

			// remove "selected" from any options that might already be selected
			$('#<portlet:namespace/>authWorkedBy option[selected="selected"]').each(
			    function() {
			        $(this).removeAttr('selected');
			    }
			);
			
			// mark the first option as selected
			$("#<portlet:namespace/>authWorkedBy option:first").attr('selected','selected');

		} else if (searchType == "<%= QueueSearchConstants.PRIORITY_LEVEL %>") {

			// remove "selected" from any options that might already be selected
			$('#<portlet:namespace/>authPriority option[selected="selected"]').each(
			    function() {
			        $(this).removeAttr('selected');
			    }
			);
			
			// mark the first option as selected
			$("#<portlet:namespace/>authPriority option:first").attr('selected','selected');

		} else if (searchType == "<%= QueueSearchConstants.DATE_RECEIVED %>") {
			
			// Clear the date field
			$("#<portlet:namespace/>authDateReceived").val("");
		} else if (searchType == "<%= QueueSearchConstants.CARD_NUMBER %>") {
			
			// Clear the text field
			$("#<portlet:namespace/>memberId").val("");
		}
		
		// Hide the results and the errors
		$("#queueSearchResults").hide();
		$("#errorMessage").hide();
		$("#dataloader").hide();
		 
		// Disable the search button until the fields are valid
		setSearchButtonStatus();
	}
	
	function closePortlet() {
	    var url='<%=cancelURL.toString()%>';
	    window.location.href = url;
	}

	function isValidForm() {
		var formValid = false;
		var searchType = $("#<portlet:namespace/>searchType").val();
		var authorizationNumber = $("#<portlet:namespace/>authorizationNumber").val();
		var authWorkedBy = $("#<portlet:namespace/>authWorkedBy").val();
		var authPriority = $("#<portlet:namespace/>authPriority").prop("selectedIndex");
		var authDateReceived = $("#<portlet:namespace/>authDateReceived").val();
		var memberId = $("#<portlet:namespace/>memberId").val();
		
		// Initialize the tool tip for the search button
		toolTip = "";
		
		if (searchType == "<%= QueueSearchConstants.AUTH_NUMBER %>") {

			// Test for a valid PA or PSR number
			if (isValidAlphanum(authorizationNumber, <%= NUM_LEN %>, true, true)) {
				formValid = true;
			} else {
				addToToolTip("The Authorization Number is not valid");
			}
		} else if (searchType == "<%= QueueSearchConstants.WORKED_BY %>") {
			
			// Any selection is valid
			formValid = true;
		} else if (searchType == "<%= QueueSearchConstants.PRIORITY_LEVEL %>") {
			
			// Any selection but the first one is valid
			if (authPriority > 0) {
				formValid = true;
			} else {
				addToToolTip("The Priority is not valid");
			}
		} else if (searchType == "<%= QueueSearchConstants.DATE_RECEIVED %>") {
			
			// Test for a valid date
			if (isValidDate(authDateReceived, true)) {
				formValid = true;
			} else {
				addToToolTip("The Date Received is not valid");
			}
		} else if (searchType == "<%= QueueSearchConstants.CARD_NUMBER %>") {

			// Test for a valid ID card number
			if (isValidAlphanum(memberId, <%= NUM_LEN %>, true)) {
				formValid = true;
			}
		} else {
	
			// Make no search type valid
			formValid = true;
		}

		return formValid;
	}
	
	function addToToolTip(str) {
		
		// Test for a new line needed
		if (toolTip.length != 0) {
			toolTip += "\n";
		}
		toolTip += str;
	}

	function setSearchButtonStatus() {

		// Disable the search button until all the fields are valid
		if (isValidForm()) {
			$("#<portlet:namespace/>queueSearch").removeAttr("disabled");
		} else {
			$("#<portlet:namespace/>queueSearch").attr("disabled", "disabled");
		}
		
		// Add the mouse over text for the search button
		$("#<portlet:namespace/>queueSearch").prop("title", toolTip);
	}

	function setAuthType() {
		var authType = $("#<portlet:namespace/>authType").val();
		
		// Set the hidden auth type
		$("#<portlet:namespace/>hiddenAuthType").val(authType);
		
		// Hide the error, results and loader message when the authType is changed
		$("#errorMessage").hide();
		$("#queueSearchResults").hide();
		$("#dataloader").hide();

		// Hide Priority Level is PSR is selected
		if (authType == "<%= QueueSearchConstants.AUTH_PSR %>") {
			
			// Change the selection if it is Priority Level
			if ($("#<portlet:namespace/>searchType").val() ==  "<%= QueueSearchConstants.PRIORITY_LEVEL %>") {
				$("#<portlet:namespace/>searchType").prop("selectedIndex", 0);
				setAuthType();
			}
			$("#<portlet:namespace/>searchType").children("option[value='<%= QueueSearchConstants.PRIORITY_LEVEL %>']").hide();
			$("#priorityLevelField").hide();
		} else {
			$("#<portlet:namespace/>searchType").children("option[value='<%= QueueSearchConstants.PRIORITY_LEVEL %>']").show();
		}
	}
</aui:script>

<script>
	function createLink(authNum, cardNum) {
		var lnk = '<a href="<portlet:actionURL><portlet:param name="action" value="getAuthorizationDetails" />' +
		          '<portlet:param name="authorizationNumberLink" value="000000" /> ' +
		          '<portlet:param name="idCardNumberLink" value="111111" /> ' +
		          '</portlet:actionURL>">' + authNum + '</a>';
		lnk = lnk.replace("000000", authNum);
		lnk = lnk.replace("111111", cardNum);
		return lnk;
	}

	//  Refresh table variables
	var refreshIntervalId = null;
	var timerRunning = false;
	var refreshMinutes = 0;

	// This method will reset the timer when the user tabs into the results area
	function divFocus(x) {
		$(window).keyup(function (e) {
		    var code = (e.keyCode ? e.keyCode : e.which);
		    if (code == 9) {
		    	resetTimer();
		    }
		});
	}
	
	// This method will test for any searches
	function isSearching() {
		var retValue = false;
		
		if ($("#queueSearchResults").css("display") != "none") {
			var cnt = 1;
			
			$("#queueResults thead th").each( function () {
				var id = "queueResultsCol_" + cnt++;
				
				if ($("#" + id).val().length > 0) {
					retValue = true;
					return false;
				}
			});
		}

		return retValue;
	}
	
	// This method will test for the initial sort
	function isSorted() {
		var retValue = false;
		var sortedCol = $('#queueResults').dataTable().fnSettings().aaSorting[0][0];
		var sortedDir = $('#queueResults').dataTable().fnSettings().aaSorting[0][1];
	
		if ($("#queueSearchResults").css("display") != "none") {

			// The initial sort is column 0, desc
			if ((sortedCol != "0") || (sortedDir != "desc")) {
				retValue = true;
			}
		}

		return retValue;
	}

	// This method will restart the timer when the user clicks in the results area
	function resetTimer() {
		if (timerRunning) {
			clearInterval(refreshIntervalId);
			timerRunning = false;
			
			// Do not start the timer if sort order or a search changed
			if (!isSearching() && !isSorted()) {
				refreshIntervalId = setInterval(updateQueue, refreshMinutes * 60000);
				timerRunning = true;
			}
		}
	}

	// This method will refresh the queue at a set rate.
	function updateQueue(searchButtonFlag) {

		// Get the refresh rate from the properties file
		<% 
			QueueUtil queueUtil = new QueueUtil(); 
			int refreshRate = queueUtil.getIntFromPropertiesFile("queue.refreshRate", 5);
		%>
		refreshMinutes = <%= refreshRate %>;

		// Stop the timer when the search button is selected
		if (searchButtonFlag == "1") {
			clearInterval(refreshIntervalId);
			timerRunning = false;
		}
		
		// Set up the timer
		if (!timerRunning) {

			// Do not start the timer if sort order or a search changed
			if (!isSearching() && !isSorted()) {

				// This will set interval method which calls the update method.  The rate is defined in the properties file. 
				// The default rate is 5 minutes.
				refreshIntervalId = setInterval(updateQueue, refreshMinutes * 60000);
				timerRunning = true;
			}
		} else {

			// Stop the timer if the results and data loading are not displayed
			if (($("#queueSearchResults").css("display") == "none") && ($("#dataloader").css("display") == "none")) {
				clearInterval(refreshIntervalId);
				timerRunning = false;
			}
		}
		
		if (timerRunning || (searchButtonFlag == "1")) {

			// Do not start the timer if sort order or a search changed
			if ((searchButtonFlag == "1") || (!isSearching() && !isSorted())) {
			
				// This method will call the ajax method to get the search results for the queue
				getSearchResult();
				
				if (!timerRunning) {
				
					// Start the timer
					refreshIntervalId = setInterval(updateQueue, refreshMinutes * 60000);
					timerRunning = true;
				}
			}
		}
	}
	
	function getSearchResult() {
		$("#queueSearchResults").hide();
		$("#errorMessage").hide();
	
		if (isValidForm()) {
			$("#dataloader").show();
	
			$.ajax({
				type : "POST",
				data : $('#<portlet:namespace/>queueSearchDetails').serialize(),
				dataType : "json",
				url : "${queueSearchActionURL}",
				
				success : function(queueInfo) {
					var authdata = JSON.stringify(queueInfo);

					var isError;
					for (var i = 0; i < queueInfo.length; i++) { 
					     isError = queueInfo[i].error;
					}
				
					$("#dataloader").hide();
					
					// Start the timer after the data is loaded
					resetTimer();
					if( (isError != null && isError != "" && isError != "undefined")  ){
						$("#errorMessage").show();
						$("#errorTxt").text(isError);
					} else {
						
						$("#queueSearchResults").show();
						$("#queueResults").dataTable({
							"bDestroy" : true,
							"sPaginationType" : "full_numbers",
							"bProcessing" : true,
							"aaData" : jQuery.parseJSON(authdata),
							"aoColumns" : [{
								"mData" : "<%= QueueSearchConstants.AUTH_NUMBER %>"
							}, {
								"mData" : "<%= QueueSearchConstants.CARD_NUMBER %>"
							}, {
								"mData" : "<%= QueueSearchConstants.HEALTH_PLAN_LOB %>"
							}, {
								"mData" : "<%= QueueSearchConstants.SUBMISSION_STATUS %>"
							}, {
								"mData" : "<%= QueueSearchConstants.PRIORITY_LEVEL %>"
							}, {
								"mData" : "<%= QueueSearchConstants.AUTH_STATUS %>"
							}, {
								"mData" : "<%= QueueSearchConstants.DATE_RECEIVED %>"
							}, {
								"mData" : "<%= QueueSearchConstants.AUTH_AGE %>"
							}, {
								"mData" : "<%= QueueSearchConstants.DUE_DATE %>"
							}, {
								"mData" : "<%= QueueSearchConstants.WORKED_BY %>"
							}],
							"order": [[ 0, "desc" ]],
							"aoColumnDefs" : [{
									"aTargets" : [0],
									"sWidth": '8%',
									"mRender" : function (data, type, row, meta) {
										if (type === 'display') {
											data = createLink(data, row.idCardNumber);
										}
										return data;
									}
								},
								{
									"aTargets" : [1],
					                "visible": false,
					                "searchable": false
								}
							]
						}); 

						// Setup - add a text input to each footer cell.  The search boxes, which is in the footer cells, are moved under the header using css.						
						$("#queueResults tfoot").css("display: table-row-group");

						var idCnt = 1;
						$("#queueResults tfoot th").each( function () {
							var id = "queueResultsCol_" + idCnt++;
							
							$(this).html('<input type="text" placeholder="Search" id="' + id + '" style="width:98%" />');
						});
						
						// DataTable
						var table = $("#queueResults").DataTable();
						
						// Apply the search
						table.columns().every(function () {
						    var that = this;
						
						    $('input', this.footer()).on('keyup change', function () {
						        if (that.search() !== this.value) {
						            that
						                .search(this.value)
						                .draw();
						        }
						    });
						});
					}
				}
			});
		}
	}

	function setAuthorizationStatusType(type) {
		
		// Hide the results and loader message when the auth Status is changed
		$("#queueSearchResults").hide();
		$("#dataloader").hide();
		
		// The hidden value is used to set the operationType for the soap message
		$("#<portlet:namespace/>hiddenInprocessView").val(type);
	}
	
	function processEnterKey(id) {
		document.getElementById(getNamespace() + id).addEventListener("keyup", function(event) {
			event.preventDefault();
			if (event.keyCode === 13) {
				$("#errorText").hide();
				updateQueue('1');
			}
		});
	}
	
</script>


<div id="busy_indicator" style="display: none" >
	<img src="<%=request.getContextPath()%>/images/busy-spinner.gif" alt="Busy indicator">
</div>

<liferay-ui:panel title="queue.title" collapsible="false">
	<aui:form name="queueSearchDetails" method="post" commandname="queueSearchFO" onChange="javascript:setSearchButtonStatus()" >

		<aui:fieldset>
			<aui:container>
				<aui:input type="hidden" id="hiddenInprocessView" name="hiddenInprocessView" value="" />
				<aui:row>
					<aui:col span="12">
						<div class=middle>
							<label for="radioSelect" class="labelClass">Search By Authorization Status:</label>
							<input type="radio" name="radioSelect" id="radioSelectInProcess" value="<%= QueueSearchConstants.INPROCESSSEARCH %>" onClick="javascript:setAuthorizationStatusType(this.value)" >In Process
							<input type="radio" name="radioSelect" id="radioSelectCompleted" value="<%= QueueSearchConstants.COMPLETEDSEARCH %>" onClick="javascript:setAuthorizationStatusType(this.value)" >Completed
							<input type="radio" name="radioSelect" id="radioSelectAll" value="<%= QueueSearchConstants.ALLSEARCH %>" onClick="javascript:setAuthorizationStatusType(this.value)" >All
						</div>
   					</aui:col>
				</aui:row>
			</aui:container>
		</aui:fieldset>

		<aui:fieldset label="queue.label.search.criteria">
			<aui:container>
				<aui:input type="hidden" id="hiddenAuthType" name="hiddenAuthType" value="" />
				<aui:row>
					<aui:col span="6">
						<aui:select inlineLabel="true" name="authType" id="authType" label="queue.label.auth.type" cssClass="span7 sntdis" onChange="javascript:setAuthType()">
							<aui:option value="<%= QueueSearchConstants.AUTH_ALL %>"><%= QueueSearchConstants.AUTH_ALL %></aui:option>
							<aui:option value="<%= QueueSearchConstants.AUTH_PA %>"><%= QueueSearchConstants.AUTH_PA_DESC %></aui:option>
							<aui:option value="<%= QueueSearchConstants.AUTH_PSR %>"><%= QueueSearchConstants.AUTH_PSR_DESC %></aui:option>
						</aui:select>
					</aui:col>
				</aui:row>
				
				<aui:row>
					<aui:col span="6">
						<aui:select inlineLabel="true" name="searchType" id="searchType" label="queue.label.search.type" cssClass="span7 sntdis" onChange="javascript:showSearchField()">
							<aui:option value="">Make a Selection</aui:option>
							<aui:option value="<%= QueueSearchConstants.AUTH_NUMBER %>">Authorization Number</aui:option>
							<aui:option value="<%= QueueSearchConstants.WORKED_BY %>">Being Worked By</aui:option>
							<aui:option value="<%= QueueSearchConstants.DATE_RECEIVED %>">Date Received</aui:option>
							<aui:option value="<%= QueueSearchConstants.PRIORITY_LEVEL %>">Priority Level</aui:option>
						</aui:select>
					</aui:col>
				</aui:row>

				<div id="authorizationNumberField" style="display: none" >
					<aui:row>
						<aui:col span="6">
							<aui:input type="text" inlineLabel="true" name="authorizationNumber" resizable="false" label="queue.label.auth.number" maxlength="<%= NUM_LEN %>" value='${queueSearchFO.authorizationNumber}' >
								<aui:validator name="required" errorMessage="Please enter valid  Authorization Number" />
								<aui:validator name="alphanum" />
								<aui:validator name="minLength"><%= NUM_LEN %></aui:validator>
								<aui:validator name="maxLength"><%= NUM_LEN %></aui:validator>
							</aui:input>
						</aui:col>
					</aui:row>
				</div>
		
				<div id="workedByField" style="display: none" >
					<aui:row>
						<aui:col span="6">
							<aui:select inlineLabel="true" name="authWorkedBy" id="authWorkedBy" label="queue.label.auth.worked.by" cssClass="span7 checkForDisable" required="true" showRequiredLabel="true" >
								<aui:option value="Make a selection">Make a selection</aui:option>
								<c:choose>
									<c:when test="${fn:length(userList) > 0}">
										<c:forEach items="${userList}" var="user" >
											<aui:option value="${user}">${user}</aui:option>
										</c:forEach>
									</c:when>
								</c:choose>
							</aui:select>
						</aui:col>
					</aui:row>
				</div>
		
				<div id="priorityLevelField" style="display: none" >
					<aui:row>
						<aui:col span="6">
							<aui:select inlineLabel="true" name="authPriority" id="authPriority" label="queue.label.priority" cssClass="span7 sntdis checkForDisable" required="true" showRequiredLabel="true">
								<aui:option value="">Make a Selection</aui:option>
								<aui:option value="routine">Routine</aui:option>
								<aui:option value="urgent">Urgent</aui:option>
							</aui:select>
						</aui:col>
					</aui:row>
				</div>
		
				<div id="dateReceivedField" style="display: none" >
					<aui:row>
						<aui:col span="6">
       						<aui:input type="date" inlineLabel="true" name="authDateReceived" id="authDateReceived" cssClass="checkForDisable sntdis" label="queue.date.received" required="true" value="${queueSearchFO.dateReceived}" >
								<aui:validator name="custom" errorMessage="Please enter a valid date (format mm/dd/yyyy).">
									function (val, fieldNode, ruleValue) {
										var retValue = false;
										
										// Test for invalid date
										if ((browserStr == "IE") || (browserStr == "Firefox") || (browserStr == "unknown")) {
											retValue = isValidDate(val, true);
										} else {
											var authDateReceived = document.getElementById("<portlet:namespace/>authDateReceived");
											retValue = !authDateReceived.validity.badInput;
										}
									 	
										return retValue;
									}
								</aui:validator>
							</aui:input>
						</aui:col>
					</aui:row>
				</div>

				<div id="memberIdField" style="display: none" >
					<aui:row>
						<aui:col span="6">
							<aui:input type="text" inlineLabel="true" name="memberId" cssClass="form-control toUppercase" label="queue.card.number" maxlength="<%= NUM_LEN %>" 
							           style="text-transform:uppercase"  value='${queueSearchFO.memberId}' onfocusout="javascript:convertToUppercase(this)" 
							           onblur="this.value=removeSpaces(this.value)" >
								<aui:validator name="required" errorMessage="Please enter valid ID Card Number" />
								<aui:validator name="alphanum" />
							</aui:input>
						</aui:col>
					</aui:row>
				</div>
			</aui:container>
		</aui:fieldset>

		<aui:fieldset>
			<aui:button-row cssClass="btn-divider">
				<aui:button name="queueSearch" cssClass="pull-right" type="button" primary="true" value="Search" onclick="$('#errorText').hide(); updateQueue('1')" />
				<aui:button name="queueClear"  cssClass="pull-right" type="button"value="Clear" onclick="clearSearchField()" />
  				<aui:button name="queueCancel" cssClass="btn-gray pull-left" type="button" value="Cancel" onclick="closePortlet()" />
			</aui:button-row>
		</aui:fieldset>
	</aui:form>
</liferay-ui:panel>

<div onmousedown="resetTimer()" onFocusin="divFocus(this)" id="queueSearchResults" style="display: none" >
	<liferay-ui:panel title="Results" collapsible="false">
		<table id="queueResults" class="display" style="border-collapse: collapse; width: 100%" data-page-length="25" >
			<thead>
				<tr>
					<th>Authorization Number</th>
					<th>ID Card Number</th>
					<th>Health Plan - LOB</th>
					<th>Submission Status</th>
					<th>Priority Level</th>
					<th>Authorization Status</th>
					<th>Date Received</th>
					<th>Authorization Age</th>
					<th>Due Date &amp; Time</th>
					<th>Being Worked By</th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<th>Authorization Number</th>
					<th>ID Card Number</th>
					<th>Health Plan - LOB</th>
					<th>Submission Status</th>
					<th>Priority Level</th>
					<th>Authorization Status</th>
					<th>Date Received</th>
					<th>Authorization Age</th>
					<th>Due Date &amp; Time</th>
					<th>Being Worked By</th>
				</tr>
			</tfoot>
			<tbody>
	
			</tbody>
		</table>
	</liferay-ui:panel>
</div>

<div id="errorMessage" style="display: none; text-align: left; color: red" >
	<span id="errorTxt" > </span>
</div>

<div id="dataloader" class="blink_text" style="display: none" >  
     Please wait while loading the page...
</div>

<div class="aui-message aui-message-error" style="text-align: left">
	<span id="errorText" style="color: red"> </span>
</div>
