/**
 * Description
 *		This file contain the javascript needed for Alloy UI and Prior Auth Queue.
 *
 * CHANGE History
 * 		Version 1.0  
 *			Initial version.  
 *     
 */

var exitString = 'You have made updates to this page that have not been saved.  If you continue the updates will be lost.';

makeFormResponsive();
formatRequiredLabels('*');

function makeFormResponsive() {
	var groups = document.getElementById('main-content').getElementsByClassName('control-group');
	
	for (var i = 0, n = groups.length; i < n; i++ ) {
		groups[i].setAttribute('class', groups[i].getAttribute('class') + ' row-fluid');
		
		var label = groups[i].getElementsByTagName('label')[0];
		
		if (label) {
			label.setAttribute('class', label.getAttribute('class') + ' span5');
		}
		
		var input = groups[i].getElementsByTagName('input')[0];
		
		if (input) {
			input.setAttribute('class', input.getAttribute('class') + ' span7');
		}
	}
}


function formatRequiredLabels(text) {
	var reqLabels = document.getElementById('main-content').getElementsByClassName('label-required');
	
	for ( var i = 0, n = reqLabels.length; i < n; i++ ) {
		reqLabels[i].innerHTML = text;
	}
}

function getBrowser() {
	var rtnValue = "unknown";
	
	if ((navigator.userAgent.indexOf("Opera") || navigator.userAgent.indexOf('OPR')) != -1 ) {
		rtnValue = "Opera";
	} if (navigator.userAgent.indexOf("Chrome") != -1 ) {
		rtnValue = "Chrome";
	} if (navigator.userAgent.indexOf("Safari") != -1) {
		rtnValue = "Safari";
	} else if ((navigator.userAgent.indexOf("MSIE") != -1 ) || (!!document.documentMode == true )) { //IF IE > 10
		rtnValue = "IE";
	} else if (navigator.userAgent.indexOf("Firefox") != -1 )  {
		rtnValue = "Firefox";
	} else {
		rtnValue = "unknown";
	}

	return rtnValue;
}

function removeSpaces(str) {
	 return str.split(' ').join('');
}

function isValidDate(_dateString, _requiredFlag) {
	var rtnValue = true;
	var browserStr = getBrowser();
	var parts;
 	var day;
 	var month;
	var year;

	if (_dateString.length > 0) {

		// First check for the pattern
		if ((browserStr == "IE") || (browserStr == "Firefox")) {
			
			// Check format mm/dd/yyyy
			if (!/^\d{1,2}\/\d{1,2}\/\d{4}$/.test(_dateString)) {
				rtnValue = false;
			}
			
			if (rtnValue) {
	
				// Parse the date parts to integers
				parts = _dateString.split("/");
				day = parseInt(parts[1], 10);
				month = parseInt(parts[0], 10);
				year = parseInt(parts[2], 10);
			}
		} else {
			
			// Check format yyyy-mm-dd
			if (!/^\d{4}-\d{1,2}-\d{1,2}$/.test(_dateString)) {
				rtnValue = false;
			}
		
			
			if (rtnValue) {
				// Parse the date parts to integers
				parts = _dateString.split("-");
				day = parseInt(parts[2], 10);
				month = parseInt(parts[1], 10);
				year = parseInt(parts[0], 10);
			}
		}
	
		if (rtnValue) {
	
			// Check the ranges of month and year
			if ((year < 1000) || (year > 3000) || (month == 0) || (month > 12)) {
				rtnValue = false;
			}
		
			var monthLength = [ 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 ];
		
			// Adjust for leap years
		 	if ((year % 400 == 0) || ((year % 100 != 0) && (year % 4 == 0))) {
				monthLength[1] = 29;
			}
		
			// Check the range of the day
			rtnValue = ((day > 0) && (day <= monthLength[month - 1]));
		}
	} else {
		if (_requiredFlag){
			
			// Return not valid for a blank required date
			rtnValue = false;
		}
	}
	return rtnValue;
}

function isValidDigit(_dat, _len, _requiredFlag, equalFlag) {
	var rtnValue = false;
	var length = _dat.length;

	if (length == 0) {
		if (!_requiredFlag) {
			rtnValue = true;
		}
	} else {
		var isNumber =  /^\d+$/.test(_dat);
		if (equalFlag) {
			if (isNumber && (_dat.length == _len)) {
				rtnValue = true;
			}
		} else if (isNumber && (_dat.length <= _len)) {
			rtnValue = true;
		}
	}
	
	return rtnValue;
}

function isValidAlphanum(_dat, _len, _requiredFlag) {
	var rtnValue = false;
	var length = _dat.length;

	if (length == 0) {
		if (!_requiredFlag) {
			rtnValue = true;
		}
	} else {
		var isAlphanumeric =  /^[a-zA-Z0-9]+$/.test(_dat);
		if (isAlphanumeric && (length <= _len)) {
			rtnValue = true;
		}
	}
	
	return rtnValue;
}

function convertToUppercase(val) {
	val.value = val.value.toLocaleUpperCase();
}

function processItem(nextId, currentId) {

	// Save the current active element
	var activeId = document.activeElement.id;

	// Force validation of dateofServiceTo (only do this once)
	$("#" + nextId).focus();
	$("#" + nextId).blur();
	$("#" + currentId).focus();
	$("#" + currentId).blur();
		
	// Set focus to the saved field
	$("#" + activeId).focus();
}
