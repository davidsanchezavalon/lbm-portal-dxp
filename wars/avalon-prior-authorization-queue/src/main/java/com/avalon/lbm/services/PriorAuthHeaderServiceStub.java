/**
 * PriorAuthHeaderServiceStub.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.3  Built on : Jun 27, 2015 (11:17:49 BST)
 * 
 * For avalon-prior-authorization-queue portlet
 */
package com.avalon.lbm.services;


/*
 *  PriorAuthHeaderServiceStub java implementation
 */
public class PriorAuthHeaderServiceStub extends org.apache.axis2.client.Stub {
    private static int counter = 0;
    protected org.apache.axis2.description.AxisOperation[] _operations;

    //hashmaps to keep the fault mapping
    private java.util.HashMap faultExceptionNameMap = new java.util.HashMap();
    private java.util.HashMap faultExceptionClassNameMap = new java.util.HashMap();
    private java.util.HashMap faultMessageMap = new java.util.HashMap();
    private javax.xml.namespace.QName[] opNameArray = null;

    /**
     *Constructor that takes in a configContext
     */
    public PriorAuthHeaderServiceStub(
        org.apache.axis2.context.ConfigurationContext configurationContext,
        java.lang.String targetEndpoint) throws org.apache.axis2.AxisFault {
        this(configurationContext, targetEndpoint, false);
    }

    /**
     * Constructor that takes in a configContext  and useseperate listner
     */
    public PriorAuthHeaderServiceStub(
        org.apache.axis2.context.ConfigurationContext configurationContext,
        java.lang.String targetEndpoint, boolean useSeparateListener)
        throws org.apache.axis2.AxisFault {
        //To populate AxisService
        populateAxisService();
        populateFaults();

        _serviceClient = new org.apache.axis2.client.ServiceClient(configurationContext,
                _service);

        _serviceClient.getOptions()
                      .setTo(new org.apache.axis2.addressing.EndpointReference(
                targetEndpoint));
        _serviceClient.getOptions().setUseSeparateListener(useSeparateListener);
    }

    /**
     * Default Constructor
     */
    public PriorAuthHeaderServiceStub(
        org.apache.axis2.context.ConfigurationContext configurationContext)
        throws org.apache.axis2.AxisFault {
    	this(configurationContext,EsbUrlDelegate.getEsbUrlDelegate().getEsbUrl().get("serviceHeaderUrl"));
    }

    /**
     * Default Constructor
     */
    public PriorAuthHeaderServiceStub() throws org.apache.axis2.AxisFault {
    	this(EsbUrlDelegate.getEsbUrlDelegate().getEsbUrl().get("serviceHeaderUrl")); /* To set the end point url */
    }

    /**
     * Constructor taking the target endpoint
     */
    public PriorAuthHeaderServiceStub(java.lang.String targetEndpoint)
        throws org.apache.axis2.AxisFault {
        this(null, targetEndpoint);
    }

    private static synchronized java.lang.String getUniqueSuffix() {
        // reset the counter if it is greater than 99999
        if (counter > 99999) {
            counter = 0;
        }

        counter = counter + 1;

        return java.lang.Long.toString(java.lang.System.currentTimeMillis()) +
        "_" + counter;
    }

    private void populateAxisService() throws org.apache.axis2.AxisFault {
        //creating the Service with a unique name
        _service = new org.apache.axis2.description.AxisService(
                "PriorAuthHeaderService" + getUniqueSuffix());
        addAnonymousOperations();

        //creating the operations
        org.apache.axis2.description.AxisOperation __operation;

        _operations = new org.apache.axis2.description.AxisOperation[3];

        __operation = new org.apache.axis2.description.OutInAxisOperation();

        __operation.setName(new javax.xml.namespace.QName(
                "http://services.lbm.avalon.com/", "savePriorAuthHeader"));
        _service.addOperation(__operation);

        _operations[0] = __operation;

        __operation = new org.apache.axis2.description.OutInAxisOperation();

        __operation.setName(new javax.xml.namespace.QName(
                "http://services.lbm.avalon.com/", "priorAuthorizationSearch"));
        _service.addOperation(__operation);

        _operations[1] = __operation;

        __operation = new org.apache.axis2.description.OutInAxisOperation();

        __operation.setName(new javax.xml.namespace.QName(
                "http://services.lbm.avalon.com/", "getPriorAuthHeader"));
        _service.addOperation(__operation);

        _operations[2] = __operation;
    }

    //populates the faults
    private void populateFaults() {
        faultExceptionNameMap.put(new org.apache.axis2.client.FaultMapKey(
                new javax.xml.namespace.QName(
                    "http://services.lbm.avalon.com/", "DAOException"),
                "savePriorAuthHeader"),
            "com.avalon.lbm.services.DAOExceptionException");
        faultExceptionClassNameMap.put(new org.apache.axis2.client.FaultMapKey(
                new javax.xml.namespace.QName(
                    "http://services.lbm.avalon.com/", "DAOException"),
                "savePriorAuthHeader"),
            "com.avalon.lbm.services.DAOExceptionException");
        faultMessageMap.put(new org.apache.axis2.client.FaultMapKey(
                new javax.xml.namespace.QName(
                    "http://services.lbm.avalon.com/", "DAOException"),
                "savePriorAuthHeader"),
            "com.avalon.lbm.services.PriorAuthHeaderServiceStub$DAOExceptionE");

        faultExceptionNameMap.put(new org.apache.axis2.client.FaultMapKey(
                new javax.xml.namespace.QName(
                    "http://services.lbm.avalon.com/", "DAOException"),
                "priorAuthorizationSearch"),
            "com.avalon.lbm.services.DAOExceptionException");
        faultExceptionClassNameMap.put(new org.apache.axis2.client.FaultMapKey(
                new javax.xml.namespace.QName(
                    "http://services.lbm.avalon.com/", "DAOException"),
                "priorAuthorizationSearch"),
            "com.avalon.lbm.services.DAOExceptionException");
        faultMessageMap.put(new org.apache.axis2.client.FaultMapKey(
                new javax.xml.namespace.QName(
                    "http://services.lbm.avalon.com/", "DAOException"),
                "priorAuthorizationSearch"),
            "com.avalon.lbm.services.PriorAuthHeaderServiceStub$DAOExceptionE");

        faultExceptionNameMap.put(new org.apache.axis2.client.FaultMapKey(
                new javax.xml.namespace.QName(
                    "http://services.lbm.avalon.com/", "DAOException"),
                "getPriorAuthHeader"),
            "com.avalon.lbm.services.DAOExceptionException");
        faultExceptionClassNameMap.put(new org.apache.axis2.client.FaultMapKey(
                new javax.xml.namespace.QName(
                    "http://services.lbm.avalon.com/", "DAOException"),
                "getPriorAuthHeader"),
            "com.avalon.lbm.services.DAOExceptionException");
        faultMessageMap.put(new org.apache.axis2.client.FaultMapKey(
                new javax.xml.namespace.QName(
                    "http://services.lbm.avalon.com/", "DAOException"),
                "getPriorAuthHeader"),
            "com.avalon.lbm.services.PriorAuthHeaderServiceStub$DAOExceptionE");
    }

    /**
     * Auto generated method signature
     *
     * @see com.avalon.lbm.services.PriorAuthHeaderService#savePriorAuthHeader
     * @param savePriorAuthHeader0
     * @throws com.avalon.lbm.services.DAOExceptionException :
     */
    public com.avalon.lbm.services.PriorAuthHeaderServiceStub.SavePriorAuthHeaderResponseE savePriorAuthHeader(
        com.avalon.lbm.services.PriorAuthHeaderServiceStub.SavePriorAuthHeaderE savePriorAuthHeader0)
        throws java.rmi.RemoteException,
            com.avalon.lbm.services.DAOExceptionException {
        org.apache.axis2.context.MessageContext _messageContext = null;

        try {
            org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[0].getName());
            _operationClient.getOptions()
                            .setAction("urn:savePriorAuthHeaderAction");
            _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

            addPropertyToOperationClient(_operationClient,
                org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR,
                "&");

            // create a message context
            _messageContext = new org.apache.axis2.context.MessageContext();

            // create SOAP envelope with that payload
            org.apache.axiom.soap.SOAPEnvelope env = null;

            env = toEnvelope(getFactory(_operationClient.getOptions()
                                                        .getSoapVersionURI()),
                    savePriorAuthHeader0,
                    optimizeContent(
                        new javax.xml.namespace.QName(
                            "http://services.lbm.avalon.com/",
                            "savePriorAuthHeader")),
                    new javax.xml.namespace.QName(
                        "http://services.lbm.avalon.com/", "savePriorAuthHeader"));

            //adding SOAP soap_headers
            _serviceClient.addHeadersToEnvelope(env);
            // set the message context with that soap envelope
            _messageContext.setEnvelope(env);

            // add the message contxt to the operation client
            _operationClient.addMessageContext(_messageContext);

            //execute the operation client
            _operationClient.execute(true);

            org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
            org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();

            java.lang.Object object = fromOM(_returnEnv.getBody()
                                                       .getFirstElement(),
                    com.avalon.lbm.services.PriorAuthHeaderServiceStub.SavePriorAuthHeaderResponseE.class,
                    getEnvelopeNamespaces(_returnEnv));

            return (com.avalon.lbm.services.PriorAuthHeaderServiceStub.SavePriorAuthHeaderResponseE) object;
        } catch (org.apache.axis2.AxisFault f) {
            org.apache.axiom.om.OMElement faultElt = f.getDetail();

            if (faultElt != null) {
                if (faultExceptionNameMap.containsKey(
                            new org.apache.axis2.client.FaultMapKey(
                                faultElt.getQName(), "savePriorAuthHeader"))) {
                    //make the fault by reflection
                    try {
                        java.lang.String exceptionClassName = (java.lang.String) faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(
                                    faultElt.getQName(), "savePriorAuthHeader"));
                        java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
                        java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
                        java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());

                        //message class
                        java.lang.String messageClassName = (java.lang.String) faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(
                                    faultElt.getQName(), "savePriorAuthHeader"));
                        java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
                        java.lang.Object messageObject = fromOM(faultElt,
                                messageClass, null);
                        java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage",
                                new java.lang.Class[] { messageClass });
                        m.invoke(ex, new java.lang.Object[] { messageObject });

                        if (ex instanceof com.avalon.lbm.services.DAOExceptionException) {
                            throw (com.avalon.lbm.services.DAOExceptionException) ex;
                        }

                        throw new java.rmi.RemoteException(ex.getMessage(), ex);
                    } catch (java.lang.ClassCastException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    } catch (java.lang.ClassNotFoundException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    } catch (java.lang.NoSuchMethodException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    } catch (java.lang.reflect.InvocationTargetException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    } catch (java.lang.IllegalAccessException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    } catch (java.lang.InstantiationException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    }
                } else {
                    throw f;
                }
            } else {
                throw f;
            }
        } finally {
            if (_messageContext.getTransportOut() != null) {
                _messageContext.getTransportOut().getSender()
                               .cleanup(_messageContext);
            }
        }
    }

    /**
     * Auto generated method signature for Asynchronous Invocations
     *
     * @see com.avalon.lbm.services.PriorAuthHeaderService#startsavePriorAuthHeader
     * @param savePriorAuthHeader0
     */
    public void startsavePriorAuthHeader(
        com.avalon.lbm.services.PriorAuthHeaderServiceStub.SavePriorAuthHeaderE savePriorAuthHeader0,
        final com.avalon.lbm.services.PriorAuthHeaderServiceCallbackHandler callback)
        throws java.rmi.RemoteException {
        org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[0].getName());
        _operationClient.getOptions().setAction("urn:savePriorAuthHeaderAction");
        _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

        addPropertyToOperationClient(_operationClient,
            org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR,
            "&");

        // create SOAP envelope with that payload
        org.apache.axiom.soap.SOAPEnvelope env = null;
        final org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();

        //Style is Doc.
        env = toEnvelope(getFactory(_operationClient.getOptions()
                                                    .getSoapVersionURI()),
                savePriorAuthHeader0,
                optimizeContent(
                    new javax.xml.namespace.QName(
                        "http://services.lbm.avalon.com/", "savePriorAuthHeader")),
                new javax.xml.namespace.QName(
                    "http://services.lbm.avalon.com/", "savePriorAuthHeader"));

        // adding SOAP soap_headers
        _serviceClient.addHeadersToEnvelope(env);
        // create message context with that soap envelope
        _messageContext.setEnvelope(env);

        // add the message context to the operation client
        _operationClient.addMessageContext(_messageContext);

        _operationClient.setCallback(new org.apache.axis2.client.async.AxisCallback() {
                public void onMessage(
                    org.apache.axis2.context.MessageContext resultContext) {
                    try {
                        org.apache.axiom.soap.SOAPEnvelope resultEnv = resultContext.getEnvelope();

                        java.lang.Object object = fromOM(resultEnv.getBody()
                                                                  .getFirstElement(),
                                com.avalon.lbm.services.PriorAuthHeaderServiceStub.SavePriorAuthHeaderResponseE.class,
                                getEnvelopeNamespaces(resultEnv));
                        callback.receiveResultsavePriorAuthHeader((com.avalon.lbm.services.PriorAuthHeaderServiceStub.SavePriorAuthHeaderResponseE) object);
                    } catch (org.apache.axis2.AxisFault e) {
                        callback.receiveErrorsavePriorAuthHeader(e);
                    }
                }

                public void onError(java.lang.Exception error) {
                    if (error instanceof org.apache.axis2.AxisFault) {
                        org.apache.axis2.AxisFault f = (org.apache.axis2.AxisFault) error;
                        org.apache.axiom.om.OMElement faultElt = f.getDetail();

                        if (faultElt != null) {
                            if (faultExceptionNameMap.containsKey(
                                        new org.apache.axis2.client.FaultMapKey(
                                            faultElt.getQName(),
                                            "savePriorAuthHeader"))) {
                                //make the fault by reflection
                                try {
                                    java.lang.String exceptionClassName = (java.lang.String) faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(
                                                faultElt.getQName(),
                                                "savePriorAuthHeader"));
                                    java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
                                    java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
                                    java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());

                                    //message class
                                    java.lang.String messageClassName = (java.lang.String) faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(
                                                faultElt.getQName(),
                                                "savePriorAuthHeader"));
                                    java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
                                    java.lang.Object messageObject = fromOM(faultElt,
                                            messageClass, null);
                                    java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage",
                                            new java.lang.Class[] { messageClass });
                                    m.invoke(ex,
                                        new java.lang.Object[] { messageObject });

                                    if (ex instanceof com.avalon.lbm.services.DAOExceptionException) {
                                        callback.receiveErrorsavePriorAuthHeader((com.avalon.lbm.services.DAOExceptionException) ex);

                                        return;
                                    }

                                    callback.receiveErrorsavePriorAuthHeader(new java.rmi.RemoteException(
                                            ex.getMessage(), ex));
                                } catch (java.lang.ClassCastException e) {
                                    // we cannot intantiate the class - throw the original Axis fault
                                    callback.receiveErrorsavePriorAuthHeader(f);
                                } catch (java.lang.ClassNotFoundException e) {
                                    // we cannot intantiate the class - throw the original Axis fault
                                    callback.receiveErrorsavePriorAuthHeader(f);
                                } catch (java.lang.NoSuchMethodException e) {
                                    // we cannot intantiate the class - throw the original Axis fault
                                    callback.receiveErrorsavePriorAuthHeader(f);
                                } catch (java.lang.reflect.InvocationTargetException e) {
                                    // we cannot intantiate the class - throw the original Axis fault
                                    callback.receiveErrorsavePriorAuthHeader(f);
                                } catch (java.lang.IllegalAccessException e) {
                                    // we cannot intantiate the class - throw the original Axis fault
                                    callback.receiveErrorsavePriorAuthHeader(f);
                                } catch (java.lang.InstantiationException e) {
                                    // we cannot intantiate the class - throw the original Axis fault
                                    callback.receiveErrorsavePriorAuthHeader(f);
                                } catch (org.apache.axis2.AxisFault e) {
                                    // we cannot intantiate the class - throw the original Axis fault
                                    callback.receiveErrorsavePriorAuthHeader(f);
                                }
                            } else {
                                callback.receiveErrorsavePriorAuthHeader(f);
                            }
                        } else {
                            callback.receiveErrorsavePriorAuthHeader(f);
                        }
                    } else {
                        callback.receiveErrorsavePriorAuthHeader(error);
                    }
                }

                public void onFault(
                    org.apache.axis2.context.MessageContext faultContext) {
                    org.apache.axis2.AxisFault fault = org.apache.axis2.util.Utils.getInboundFaultFromMessageContext(faultContext);
                    onError(fault);
                }

                public void onComplete() {
                    try {
                        _messageContext.getTransportOut().getSender()
                                       .cleanup(_messageContext);
                    } catch (org.apache.axis2.AxisFault axisFault) {
                        callback.receiveErrorsavePriorAuthHeader(axisFault);
                    }
                }
            });

        org.apache.axis2.util.CallbackReceiver _callbackReceiver = null;

        if ((_operations[0].getMessageReceiver() == null) &&
                _operationClient.getOptions().isUseSeparateListener()) {
            _callbackReceiver = new org.apache.axis2.util.CallbackReceiver();
            _operations[0].setMessageReceiver(_callbackReceiver);
        }

        //execute the operation client
        _operationClient.execute(false);
    }

    /**
     * Auto generated method signature
     *
     * @see com.avalon.lbm.services.PriorAuthHeaderService#priorAuthorizationSearch
     * @param priorAuthorizationSearch2
     * @throws com.avalon.lbm.services.DAOExceptionException :
     */
    public com.avalon.lbm.services.PriorAuthHeaderServiceStub.PriorAuthorizationSearchResponseE priorAuthorizationSearch(
        com.avalon.lbm.services.PriorAuthHeaderServiceStub.PriorAuthorizationSearchE priorAuthorizationSearch2)
        throws java.rmi.RemoteException,
            com.avalon.lbm.services.DAOExceptionException {
        org.apache.axis2.context.MessageContext _messageContext = null;

        try {
            org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[1].getName());
            _operationClient.getOptions()
                            .setAction("urn:priorAuthorizationSearch");
            _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

            addPropertyToOperationClient(_operationClient,
                org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR,
                "&");

            // create a message context
            _messageContext = new org.apache.axis2.context.MessageContext();

            // create SOAP envelope with that payload
            org.apache.axiom.soap.SOAPEnvelope env = null;

            env = toEnvelope(getFactory(_operationClient.getOptions()
                                                        .getSoapVersionURI()),
                    priorAuthorizationSearch2,
                    optimizeContent(
                        new javax.xml.namespace.QName(
                            "http://services.lbm.avalon.com/",
                            "priorAuthorizationSearch")),
                    new javax.xml.namespace.QName(
                        "http://services.lbm.avalon.com/",
                        "priorAuthorizationSearch"));

            //adding SOAP soap_headers
            _serviceClient.addHeadersToEnvelope(env);
            // set the message context with that soap envelope
            _messageContext.setEnvelope(env);

            // add the message contxt to the operation client
            _operationClient.addMessageContext(_messageContext);

            //execute the operation client
            _operationClient.execute(true);

            org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
            org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();

            java.lang.Object object = fromOM(_returnEnv.getBody()
                                                       .getFirstElement(),
                    com.avalon.lbm.services.PriorAuthHeaderServiceStub.PriorAuthorizationSearchResponseE.class,
                    getEnvelopeNamespaces(_returnEnv));

            return (com.avalon.lbm.services.PriorAuthHeaderServiceStub.PriorAuthorizationSearchResponseE) object;
        } catch (org.apache.axis2.AxisFault f) {
            org.apache.axiom.om.OMElement faultElt = f.getDetail();

            if (faultElt != null) {
                if (faultExceptionNameMap.containsKey(
                            new org.apache.axis2.client.FaultMapKey(
                                faultElt.getQName(), "priorAuthorizationSearch"))) {
                    //make the fault by reflection
                    try {
                        java.lang.String exceptionClassName = (java.lang.String) faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(
                                    faultElt.getQName(),
                                    "priorAuthorizationSearch"));
                        java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
                        java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
                        java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());

                        //message class
                        java.lang.String messageClassName = (java.lang.String) faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(
                                    faultElt.getQName(),
                                    "priorAuthorizationSearch"));
                        java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
                        java.lang.Object messageObject = fromOM(faultElt,
                                messageClass, null);
                        java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage",
                                new java.lang.Class[] { messageClass });
                        m.invoke(ex, new java.lang.Object[] { messageObject });

                        if (ex instanceof com.avalon.lbm.services.DAOExceptionException) {
                            throw (com.avalon.lbm.services.DAOExceptionException) ex;
                        }

                        throw new java.rmi.RemoteException(ex.getMessage(), ex);
                    } catch (java.lang.ClassCastException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    } catch (java.lang.ClassNotFoundException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    } catch (java.lang.NoSuchMethodException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    } catch (java.lang.reflect.InvocationTargetException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    } catch (java.lang.IllegalAccessException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    } catch (java.lang.InstantiationException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    }
                } else {
                    throw f;
                }
            } else {
                throw f;
            }
        } finally {
            if (_messageContext.getTransportOut() != null) {
                _messageContext.getTransportOut().getSender()
                               .cleanup(_messageContext);
            }
        }
    }

    /**
     * Auto generated method signature for Asynchronous Invocations
     *
     * @see com.avalon.lbm.services.PriorAuthHeaderService#startpriorAuthorizationSearch
     * @param priorAuthorizationSearch2
     */
    public void startpriorAuthorizationSearch(
        com.avalon.lbm.services.PriorAuthHeaderServiceStub.PriorAuthorizationSearchE priorAuthorizationSearch2,
        final com.avalon.lbm.services.PriorAuthHeaderServiceCallbackHandler callback)
        throws java.rmi.RemoteException {
        org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[1].getName());
        _operationClient.getOptions().setAction("urn:priorAuthorizationSearch");
        _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

        addPropertyToOperationClient(_operationClient,
            org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR,
            "&");

        // create SOAP envelope with that payload
        org.apache.axiom.soap.SOAPEnvelope env = null;
        final org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();

        //Style is Doc.
        env = toEnvelope(getFactory(_operationClient.getOptions()
                                                    .getSoapVersionURI()),
                priorAuthorizationSearch2,
                optimizeContent(
                    new javax.xml.namespace.QName(
                        "http://services.lbm.avalon.com/",
                        "priorAuthorizationSearch")),
                new javax.xml.namespace.QName(
                    "http://services.lbm.avalon.com/",
                    "priorAuthorizationSearch"));

        // adding SOAP soap_headers
        _serviceClient.addHeadersToEnvelope(env);
        // create message context with that soap envelope
        _messageContext.setEnvelope(env);

        // add the message context to the operation client
        _operationClient.addMessageContext(_messageContext);

        _operationClient.setCallback(new org.apache.axis2.client.async.AxisCallback() {
                public void onMessage(
                    org.apache.axis2.context.MessageContext resultContext) {
                    try {
                        org.apache.axiom.soap.SOAPEnvelope resultEnv = resultContext.getEnvelope();

                        java.lang.Object object = fromOM(resultEnv.getBody()
                                                                  .getFirstElement(),
                                com.avalon.lbm.services.PriorAuthHeaderServiceStub.PriorAuthorizationSearchResponseE.class,
                                getEnvelopeNamespaces(resultEnv));
                        callback.receiveResultpriorAuthorizationSearch((com.avalon.lbm.services.PriorAuthHeaderServiceStub.PriorAuthorizationSearchResponseE) object);
                    } catch (org.apache.axis2.AxisFault e) {
                        callback.receiveErrorpriorAuthorizationSearch(e);
                    }
                }

                public void onError(java.lang.Exception error) {
                    if (error instanceof org.apache.axis2.AxisFault) {
                        org.apache.axis2.AxisFault f = (org.apache.axis2.AxisFault) error;
                        org.apache.axiom.om.OMElement faultElt = f.getDetail();

                        if (faultElt != null) {
                            if (faultExceptionNameMap.containsKey(
                                        new org.apache.axis2.client.FaultMapKey(
                                            faultElt.getQName(),
                                            "priorAuthorizationSearch"))) {
                                //make the fault by reflection
                                try {
                                    java.lang.String exceptionClassName = (java.lang.String) faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(
                                                faultElt.getQName(),
                                                "priorAuthorizationSearch"));
                                    java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
                                    java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
                                    java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());

                                    //message class
                                    java.lang.String messageClassName = (java.lang.String) faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(
                                                faultElt.getQName(),
                                                "priorAuthorizationSearch"));
                                    java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
                                    java.lang.Object messageObject = fromOM(faultElt,
                                            messageClass, null);
                                    java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage",
                                            new java.lang.Class[] { messageClass });
                                    m.invoke(ex,
                                        new java.lang.Object[] { messageObject });

                                    if (ex instanceof com.avalon.lbm.services.DAOExceptionException) {
                                        callback.receiveErrorpriorAuthorizationSearch((com.avalon.lbm.services.DAOExceptionException) ex);

                                        return;
                                    }

                                    callback.receiveErrorpriorAuthorizationSearch(new java.rmi.RemoteException(
                                            ex.getMessage(), ex));
                                } catch (java.lang.ClassCastException e) {
                                    // we cannot intantiate the class - throw the original Axis fault
                                    callback.receiveErrorpriorAuthorizationSearch(f);
                                } catch (java.lang.ClassNotFoundException e) {
                                    // we cannot intantiate the class - throw the original Axis fault
                                    callback.receiveErrorpriorAuthorizationSearch(f);
                                } catch (java.lang.NoSuchMethodException e) {
                                    // we cannot intantiate the class - throw the original Axis fault
                                    callback.receiveErrorpriorAuthorizationSearch(f);
                                } catch (java.lang.reflect.InvocationTargetException e) {
                                    // we cannot intantiate the class - throw the original Axis fault
                                    callback.receiveErrorpriorAuthorizationSearch(f);
                                } catch (java.lang.IllegalAccessException e) {
                                    // we cannot intantiate the class - throw the original Axis fault
                                    callback.receiveErrorpriorAuthorizationSearch(f);
                                } catch (java.lang.InstantiationException e) {
                                    // we cannot intantiate the class - throw the original Axis fault
                                    callback.receiveErrorpriorAuthorizationSearch(f);
                                } catch (org.apache.axis2.AxisFault e) {
                                    // we cannot intantiate the class - throw the original Axis fault
                                    callback.receiveErrorpriorAuthorizationSearch(f);
                                }
                            } else {
                                callback.receiveErrorpriorAuthorizationSearch(f);
                            }
                        } else {
                            callback.receiveErrorpriorAuthorizationSearch(f);
                        }
                    } else {
                        callback.receiveErrorpriorAuthorizationSearch(error);
                    }
                }

                public void onFault(
                    org.apache.axis2.context.MessageContext faultContext) {
                    org.apache.axis2.AxisFault fault = org.apache.axis2.util.Utils.getInboundFaultFromMessageContext(faultContext);
                    onError(fault);
                }

                public void onComplete() {
                    try {
                        _messageContext.getTransportOut().getSender()
                                       .cleanup(_messageContext);
                    } catch (org.apache.axis2.AxisFault axisFault) {
                        callback.receiveErrorpriorAuthorizationSearch(axisFault);
                    }
                }
            });

        org.apache.axis2.util.CallbackReceiver _callbackReceiver = null;

        if ((_operations[1].getMessageReceiver() == null) &&
                _operationClient.getOptions().isUseSeparateListener()) {
            _callbackReceiver = new org.apache.axis2.util.CallbackReceiver();
            _operations[1].setMessageReceiver(_callbackReceiver);
        }

        //execute the operation client
        _operationClient.execute(false);
    }

    /**
     * Auto generated method signature
     *
     * @see com.avalon.lbm.services.PriorAuthHeaderService#getPriorAuthHeader
     * @param getPriorAuthHeader4
     * @throws com.avalon.lbm.services.DAOExceptionException :
     */
    public com.avalon.lbm.services.PriorAuthHeaderServiceStub.GetPriorAuthHeaderResponseE getPriorAuthHeader(
        com.avalon.lbm.services.PriorAuthHeaderServiceStub.GetPriorAuthHeaderE getPriorAuthHeader4)
        throws java.rmi.RemoteException,
            com.avalon.lbm.services.DAOExceptionException {
        org.apache.axis2.context.MessageContext _messageContext = null;

        try {
            org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[2].getName());
            _operationClient.getOptions().setAction("urn:getPriorAuthHeader");
            _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

            addPropertyToOperationClient(_operationClient,
                org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR,
                "&");

            // create a message context
            _messageContext = new org.apache.axis2.context.MessageContext();

            // create SOAP envelope with that payload
            org.apache.axiom.soap.SOAPEnvelope env = null;

            env = toEnvelope(getFactory(_operationClient.getOptions()
                                                        .getSoapVersionURI()),
                    getPriorAuthHeader4,
                    optimizeContent(
                        new javax.xml.namespace.QName(
                            "http://services.lbm.avalon.com/",
                            "getPriorAuthHeader")),
                    new javax.xml.namespace.QName(
                        "http://services.lbm.avalon.com/", "getPriorAuthHeader"));

            //adding SOAP soap_headers
            _serviceClient.addHeadersToEnvelope(env);
            // set the message context with that soap envelope
            _messageContext.setEnvelope(env);

            // add the message contxt to the operation client
            _operationClient.addMessageContext(_messageContext);

            //execute the operation client
            _operationClient.execute(true);

            org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
            org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();

            java.lang.Object object = fromOM(_returnEnv.getBody()
                                                       .getFirstElement(),
                    com.avalon.lbm.services.PriorAuthHeaderServiceStub.GetPriorAuthHeaderResponseE.class,
                    getEnvelopeNamespaces(_returnEnv));

            return (com.avalon.lbm.services.PriorAuthHeaderServiceStub.GetPriorAuthHeaderResponseE) object;
        } catch (org.apache.axis2.AxisFault f) {
            org.apache.axiom.om.OMElement faultElt = f.getDetail();

            if (faultElt != null) {
                if (faultExceptionNameMap.containsKey(
                            new org.apache.axis2.client.FaultMapKey(
                                faultElt.getQName(), "getPriorAuthHeader"))) {
                    //make the fault by reflection
                    try {
                        java.lang.String exceptionClassName = (java.lang.String) faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(
                                    faultElt.getQName(), "getPriorAuthHeader"));
                        java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
                        java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
                        java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());

                        //message class
                        java.lang.String messageClassName = (java.lang.String) faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(
                                    faultElt.getQName(), "getPriorAuthHeader"));
                        java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
                        java.lang.Object messageObject = fromOM(faultElt,
                                messageClass, null);
                        java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage",
                                new java.lang.Class[] { messageClass });
                        m.invoke(ex, new java.lang.Object[] { messageObject });

                        if (ex instanceof com.avalon.lbm.services.DAOExceptionException) {
                            throw (com.avalon.lbm.services.DAOExceptionException) ex;
                        }

                        throw new java.rmi.RemoteException(ex.getMessage(), ex);
                    } catch (java.lang.ClassCastException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    } catch (java.lang.ClassNotFoundException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    } catch (java.lang.NoSuchMethodException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    } catch (java.lang.reflect.InvocationTargetException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    } catch (java.lang.IllegalAccessException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    } catch (java.lang.InstantiationException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    }
                } else {
                    throw f;
                }
            } else {
                throw f;
            }
        } finally {
            if (_messageContext.getTransportOut() != null) {
                _messageContext.getTransportOut().getSender()
                               .cleanup(_messageContext);
            }
        }
    }

    /**
     * Auto generated method signature for Asynchronous Invocations
     *
     * @see com.avalon.lbm.services.PriorAuthHeaderService#startgetPriorAuthHeader
     * @param getPriorAuthHeader4
     */
    public void startgetPriorAuthHeader(
        com.avalon.lbm.services.PriorAuthHeaderServiceStub.GetPriorAuthHeaderE getPriorAuthHeader4,
        final com.avalon.lbm.services.PriorAuthHeaderServiceCallbackHandler callback)
        throws java.rmi.RemoteException {
        org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[2].getName());
        _operationClient.getOptions().setAction("urn:getPriorAuthHeader");
        _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

        addPropertyToOperationClient(_operationClient,
            org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR,
            "&");

        // create SOAP envelope with that payload
        org.apache.axiom.soap.SOAPEnvelope env = null;
        final org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();

        //Style is Doc.
        env = toEnvelope(getFactory(_operationClient.getOptions()
                                                    .getSoapVersionURI()),
                getPriorAuthHeader4,
                optimizeContent(
                    new javax.xml.namespace.QName(
                        "http://services.lbm.avalon.com/", "getPriorAuthHeader")),
                new javax.xml.namespace.QName(
                    "http://services.lbm.avalon.com/", "getPriorAuthHeader"));

        // adding SOAP soap_headers
        _serviceClient.addHeadersToEnvelope(env);
        // create message context with that soap envelope
        _messageContext.setEnvelope(env);

        // add the message context to the operation client
        _operationClient.addMessageContext(_messageContext);

        _operationClient.setCallback(new org.apache.axis2.client.async.AxisCallback() {
                public void onMessage(
                    org.apache.axis2.context.MessageContext resultContext) {
                    try {
                        org.apache.axiom.soap.SOAPEnvelope resultEnv = resultContext.getEnvelope();

                        java.lang.Object object = fromOM(resultEnv.getBody()
                                                                  .getFirstElement(),
                                com.avalon.lbm.services.PriorAuthHeaderServiceStub.GetPriorAuthHeaderResponseE.class,
                                getEnvelopeNamespaces(resultEnv));
                        callback.receiveResultgetPriorAuthHeader((com.avalon.lbm.services.PriorAuthHeaderServiceStub.GetPriorAuthHeaderResponseE) object);
                    } catch (org.apache.axis2.AxisFault e) {
                        callback.receiveErrorgetPriorAuthHeader(e);
                    }
                }

                public void onError(java.lang.Exception error) {
                    if (error instanceof org.apache.axis2.AxisFault) {
                        org.apache.axis2.AxisFault f = (org.apache.axis2.AxisFault) error;
                        org.apache.axiom.om.OMElement faultElt = f.getDetail();

                        if (faultElt != null) {
                            if (faultExceptionNameMap.containsKey(
                                        new org.apache.axis2.client.FaultMapKey(
                                            faultElt.getQName(),
                                            "getPriorAuthHeader"))) {
                                //make the fault by reflection
                                try {
                                    java.lang.String exceptionClassName = (java.lang.String) faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(
                                                faultElt.getQName(),
                                                "getPriorAuthHeader"));
                                    java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
                                    java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
                                    java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());

                                    //message class
                                    java.lang.String messageClassName = (java.lang.String) faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(
                                                faultElt.getQName(),
                                                "getPriorAuthHeader"));
                                    java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
                                    java.lang.Object messageObject = fromOM(faultElt,
                                            messageClass, null);
                                    java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage",
                                            new java.lang.Class[] { messageClass });
                                    m.invoke(ex,
                                        new java.lang.Object[] { messageObject });

                                    if (ex instanceof com.avalon.lbm.services.DAOExceptionException) {
                                        callback.receiveErrorgetPriorAuthHeader((com.avalon.lbm.services.DAOExceptionException) ex);

                                        return;
                                    }

                                    callback.receiveErrorgetPriorAuthHeader(new java.rmi.RemoteException(
                                            ex.getMessage(), ex));
                                } catch (java.lang.ClassCastException e) {
                                    // we cannot intantiate the class - throw the original Axis fault
                                    callback.receiveErrorgetPriorAuthHeader(f);
                                } catch (java.lang.ClassNotFoundException e) {
                                    // we cannot intantiate the class - throw the original Axis fault
                                    callback.receiveErrorgetPriorAuthHeader(f);
                                } catch (java.lang.NoSuchMethodException e) {
                                    // we cannot intantiate the class - throw the original Axis fault
                                    callback.receiveErrorgetPriorAuthHeader(f);
                                } catch (java.lang.reflect.InvocationTargetException e) {
                                    // we cannot intantiate the class - throw the original Axis fault
                                    callback.receiveErrorgetPriorAuthHeader(f);
                                } catch (java.lang.IllegalAccessException e) {
                                    // we cannot intantiate the class - throw the original Axis fault
                                    callback.receiveErrorgetPriorAuthHeader(f);
                                } catch (java.lang.InstantiationException e) {
                                    // we cannot intantiate the class - throw the original Axis fault
                                    callback.receiveErrorgetPriorAuthHeader(f);
                                } catch (org.apache.axis2.AxisFault e) {
                                    // we cannot intantiate the class - throw the original Axis fault
                                    callback.receiveErrorgetPriorAuthHeader(f);
                                }
                            } else {
                                callback.receiveErrorgetPriorAuthHeader(f);
                            }
                        } else {
                            callback.receiveErrorgetPriorAuthHeader(f);
                        }
                    } else {
                        callback.receiveErrorgetPriorAuthHeader(error);
                    }
                }

                public void onFault(
                    org.apache.axis2.context.MessageContext faultContext) {
                    org.apache.axis2.AxisFault fault = org.apache.axis2.util.Utils.getInboundFaultFromMessageContext(faultContext);
                    onError(fault);
                }

                public void onComplete() {
                    try {
                        _messageContext.getTransportOut().getSender()
                                       .cleanup(_messageContext);
                    } catch (org.apache.axis2.AxisFault axisFault) {
                        callback.receiveErrorgetPriorAuthHeader(axisFault);
                    }
                }
            });

        org.apache.axis2.util.CallbackReceiver _callbackReceiver = null;

        if ((_operations[2].getMessageReceiver() == null) &&
                _operationClient.getOptions().isUseSeparateListener()) {
            _callbackReceiver = new org.apache.axis2.util.CallbackReceiver();
            _operations[2].setMessageReceiver(_callbackReceiver);
        }

        //execute the operation client
        _operationClient.execute(false);
    }

    /**
     *  A utility method that copies the namepaces from the SOAPEnvelope
     */
    private java.util.Map getEnvelopeNamespaces(
        org.apache.axiom.soap.SOAPEnvelope env) {
        java.util.Map returnMap = new java.util.HashMap();
        java.util.Iterator namespaceIterator = env.getAllDeclaredNamespaces();

        while (namespaceIterator.hasNext()) {
            org.apache.axiom.om.OMNamespace ns = (org.apache.axiom.om.OMNamespace) namespaceIterator.next();
            returnMap.put(ns.getPrefix(), ns.getNamespaceURI());
        }

        return returnMap;
    }

    private boolean optimizeContent(javax.xml.namespace.QName opName) {
        if (opNameArray == null) {
            return false;
        }

        for (int i = 0; i < opNameArray.length; i++) {
            if (opName.equals(opNameArray[i])) {
                return true;
            }
        }

        return false;
    }

    private org.apache.axiom.om.OMElement toOM(
        com.avalon.lbm.services.PriorAuthHeaderServiceStub.SavePriorAuthHeaderE param,
        boolean optimizeContent) throws org.apache.axis2.AxisFault {
        try {
            return param.getOMElement(com.avalon.lbm.services.PriorAuthHeaderServiceStub.SavePriorAuthHeaderE.MY_QNAME,
                org.apache.axiom.om.OMAbstractFactory.getOMFactory());
        } catch (org.apache.axis2.databinding.ADBException e) {
            throw org.apache.axis2.AxisFault.makeFault(e);
        }
    }

    private org.apache.axiom.om.OMElement toOM(
        com.avalon.lbm.services.PriorAuthHeaderServiceStub.SavePriorAuthHeaderResponseE param,
        boolean optimizeContent) throws org.apache.axis2.AxisFault {
        try {
            return param.getOMElement(com.avalon.lbm.services.PriorAuthHeaderServiceStub.SavePriorAuthHeaderResponseE.MY_QNAME,
                org.apache.axiom.om.OMAbstractFactory.getOMFactory());
        } catch (org.apache.axis2.databinding.ADBException e) {
            throw org.apache.axis2.AxisFault.makeFault(e);
        }
    }

    private org.apache.axiom.om.OMElement toOM(
        com.avalon.lbm.services.PriorAuthHeaderServiceStub.DAOExceptionE param,
        boolean optimizeContent) throws org.apache.axis2.AxisFault {
        try {
            return param.getOMElement(com.avalon.lbm.services.PriorAuthHeaderServiceStub.DAOExceptionE.MY_QNAME,
                org.apache.axiom.om.OMAbstractFactory.getOMFactory());
        } catch (org.apache.axis2.databinding.ADBException e) {
            throw org.apache.axis2.AxisFault.makeFault(e);
        }
    }

    private org.apache.axiom.om.OMElement toOM(
        com.avalon.lbm.services.PriorAuthHeaderServiceStub.PriorAuthorizationSearchE param,
        boolean optimizeContent) throws org.apache.axis2.AxisFault {
        try {
            return param.getOMElement(com.avalon.lbm.services.PriorAuthHeaderServiceStub.PriorAuthorizationSearchE.MY_QNAME,
                org.apache.axiom.om.OMAbstractFactory.getOMFactory());
        } catch (org.apache.axis2.databinding.ADBException e) {
            throw org.apache.axis2.AxisFault.makeFault(e);
        }
    }

    private org.apache.axiom.om.OMElement toOM(
        com.avalon.lbm.services.PriorAuthHeaderServiceStub.PriorAuthorizationSearchResponseE param,
        boolean optimizeContent) throws org.apache.axis2.AxisFault {
        try {
            return param.getOMElement(com.avalon.lbm.services.PriorAuthHeaderServiceStub.PriorAuthorizationSearchResponseE.MY_QNAME,
                org.apache.axiom.om.OMAbstractFactory.getOMFactory());
        } catch (org.apache.axis2.databinding.ADBException e) {
            throw org.apache.axis2.AxisFault.makeFault(e);
        }
    }

    private org.apache.axiom.om.OMElement toOM(
        com.avalon.lbm.services.PriorAuthHeaderServiceStub.GetPriorAuthHeaderE param,
        boolean optimizeContent) throws org.apache.axis2.AxisFault {
        try {
            return param.getOMElement(com.avalon.lbm.services.PriorAuthHeaderServiceStub.GetPriorAuthHeaderE.MY_QNAME,
                org.apache.axiom.om.OMAbstractFactory.getOMFactory());
        } catch (org.apache.axis2.databinding.ADBException e) {
            throw org.apache.axis2.AxisFault.makeFault(e);
        }
    }

    private org.apache.axiom.om.OMElement toOM(
        com.avalon.lbm.services.PriorAuthHeaderServiceStub.GetPriorAuthHeaderResponseE param,
        boolean optimizeContent) throws org.apache.axis2.AxisFault {
        try {
            return param.getOMElement(com.avalon.lbm.services.PriorAuthHeaderServiceStub.GetPriorAuthHeaderResponseE.MY_QNAME,
                org.apache.axiom.om.OMAbstractFactory.getOMFactory());
        } catch (org.apache.axis2.databinding.ADBException e) {
            throw org.apache.axis2.AxisFault.makeFault(e);
        }
    }

    private org.apache.axiom.soap.SOAPEnvelope toEnvelope(
        org.apache.axiom.soap.SOAPFactory factory,
        com.avalon.lbm.services.PriorAuthHeaderServiceStub.SavePriorAuthHeaderE param,
        boolean optimizeContent, javax.xml.namespace.QName methodQName)
        throws org.apache.axis2.AxisFault {
        try {
            org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
            emptyEnvelope.getBody()
                         .addChild(param.getOMElement(
                    com.avalon.lbm.services.PriorAuthHeaderServiceStub.SavePriorAuthHeaderE.MY_QNAME,
                    factory));

            return emptyEnvelope;
        } catch (org.apache.axis2.databinding.ADBException e) {
            throw org.apache.axis2.AxisFault.makeFault(e);
        }
    }

    /* methods to provide back word compatibility */
    private org.apache.axiom.soap.SOAPEnvelope toEnvelope(
        org.apache.axiom.soap.SOAPFactory factory,
        com.avalon.lbm.services.PriorAuthHeaderServiceStub.PriorAuthorizationSearchE param,
        boolean optimizeContent, javax.xml.namespace.QName methodQName)
        throws org.apache.axis2.AxisFault {
        try {
            org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
            emptyEnvelope.getBody()
                         .addChild(param.getOMElement(
                    com.avalon.lbm.services.PriorAuthHeaderServiceStub.PriorAuthorizationSearchE.MY_QNAME,
                    factory));

            return emptyEnvelope;
        } catch (org.apache.axis2.databinding.ADBException e) {
            throw org.apache.axis2.AxisFault.makeFault(e);
        }
    }

    /* methods to provide back word compatibility */
    private org.apache.axiom.soap.SOAPEnvelope toEnvelope(
        org.apache.axiom.soap.SOAPFactory factory,
        com.avalon.lbm.services.PriorAuthHeaderServiceStub.GetPriorAuthHeaderE param,
        boolean optimizeContent, javax.xml.namespace.QName methodQName)
        throws org.apache.axis2.AxisFault {
        try {
            org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
            emptyEnvelope.getBody()
                         .addChild(param.getOMElement(
                    com.avalon.lbm.services.PriorAuthHeaderServiceStub.GetPriorAuthHeaderE.MY_QNAME,
                    factory));

            return emptyEnvelope;
        } catch (org.apache.axis2.databinding.ADBException e) {
            throw org.apache.axis2.AxisFault.makeFault(e);
        }
    }

    /* methods to provide back word compatibility */

    /**
     *  get the default envelope
     */
    private org.apache.axiom.soap.SOAPEnvelope toEnvelope(
        org.apache.axiom.soap.SOAPFactory factory) {
        return factory.getDefaultEnvelope();
    }

    private java.lang.Object fromOM(org.apache.axiom.om.OMElement param,
        java.lang.Class type, java.util.Map extraNamespaces)
        throws org.apache.axis2.AxisFault {
        try {
            if (com.avalon.lbm.services.PriorAuthHeaderServiceStub.DAOExceptionE.class.equals(
                        type)) {
                return com.avalon.lbm.services.PriorAuthHeaderServiceStub.DAOExceptionE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
            }

            if (com.avalon.lbm.services.PriorAuthHeaderServiceStub.GetPriorAuthHeaderE.class.equals(
                        type)) {
                return com.avalon.lbm.services.PriorAuthHeaderServiceStub.GetPriorAuthHeaderE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
            }

            if (com.avalon.lbm.services.PriorAuthHeaderServiceStub.GetPriorAuthHeaderResponseE.class.equals(
                        type)) {
                return com.avalon.lbm.services.PriorAuthHeaderServiceStub.GetPriorAuthHeaderResponseE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
            }

            if (com.avalon.lbm.services.PriorAuthHeaderServiceStub.PriorAuthorizationSearchE.class.equals(
                        type)) {
                return com.avalon.lbm.services.PriorAuthHeaderServiceStub.PriorAuthorizationSearchE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
            }

            if (com.avalon.lbm.services.PriorAuthHeaderServiceStub.PriorAuthorizationSearchResponseE.class.equals(
                        type)) {
                return com.avalon.lbm.services.PriorAuthHeaderServiceStub.PriorAuthorizationSearchResponseE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
            }

            if (com.avalon.lbm.services.PriorAuthHeaderServiceStub.SavePriorAuthHeaderE.class.equals(
                        type)) {
                return com.avalon.lbm.services.PriorAuthHeaderServiceStub.SavePriorAuthHeaderE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
            }

            if (com.avalon.lbm.services.PriorAuthHeaderServiceStub.SavePriorAuthHeaderResponseE.class.equals(
                        type)) {
                return com.avalon.lbm.services.PriorAuthHeaderServiceStub.SavePriorAuthHeaderResponseE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
            }
        } catch (java.lang.Exception e) {
            throw org.apache.axis2.AxisFault.makeFault(e);
        }

        return null;
    }

    //http://HDC3-D-514SLH9:8080/LBM-PriorAuthDAOWS-2.0/PriorAuthHeaderService
    public static class ProviderDTO implements org.apache.axis2.databinding.ADBBean {
        /* This type was generated from the piece of schema that had
           name = providerDTO
           Namespace URI = http://services.lbm.avalon.com/
           Namespace Prefix = ns1
         */

        /**
         * field for OrderingFirstName
         */
        protected java.lang.String localOrderingFirstName;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localOrderingFirstNameTracker = false;

        /**
         * field for OrderingLastName
         */
        protected java.lang.String localOrderingLastName;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localOrderingLastNameTracker = false;

        /**
         * field for OrderingNpi
         */
        protected java.lang.String localOrderingNpi;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localOrderingNpiTracker = false;

        /**
         * field for OrderingPhoneNumber
         */
        protected java.lang.String localOrderingPhoneNumber;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localOrderingPhoneNumberTracker = false;

        /**
         * field for OrderingTinEin
         */
        protected java.lang.String localOrderingTinEin;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localOrderingTinEinTracker = false;

        /**
         * field for OrderingFaxNumber
         */
        protected java.lang.String localOrderingFaxNumber;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localOrderingFaxNumberTracker = false;

        /**
         * field for OrderingAddressLine1
         */
        protected java.lang.String localOrderingAddressLine1;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localOrderingAddressLine1Tracker = false;

        /**
         * field for OrderingAddressLine2
         */
        protected java.lang.String localOrderingAddressLine2;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localOrderingAddressLine2Tracker = false;

        /**
         * field for OrderingCity
         */
        protected java.lang.String localOrderingCity;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localOrderingCityTracker = false;

        /**
         * field for OrderingState
         */
        protected java.lang.String localOrderingState;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localOrderingStateTracker = false;

        /**
         * field for OrderingZip
         */
        protected java.lang.String localOrderingZip;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localOrderingZipTracker = false;

        /**
         * field for RenderingLabName
         */
        protected java.lang.String localRenderingLabName;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localRenderingLabNameTracker = false;

        /**
         * field for RenderingFirstName
         */
        protected java.lang.String localRenderingFirstName;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localRenderingFirstNameTracker = false;

        /**
         * field for RenderingLastName
         */
        protected java.lang.String localRenderingLastName;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localRenderingLastNameTracker = false;

        /**
         * field for RenderingNpi
         */
        protected java.lang.String localRenderingNpi;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localRenderingNpiTracker = false;

        /**
         * field for RenderingPhoneNumber
         */
        protected java.lang.String localRenderingPhoneNumber;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localRenderingPhoneNumberTracker = false;

        /**
         * field for RenderingTinEin
         */
        protected java.lang.String localRenderingTinEin;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localRenderingTinEinTracker = false;

        /**
         * field for RenderingFaxNumber
         */
        protected java.lang.String localRenderingFaxNumber;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localRenderingFaxNumberTracker = false;

        /**
         * field for RenderingAddressLine1
         */
        protected java.lang.String localRenderingAddressLine1;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localRenderingAddressLine1Tracker = false;

        /**
         * field for RenderingAddressLine2
         */
        protected java.lang.String localRenderingAddressLine2;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localRenderingAddressLine2Tracker = false;

        /**
         * field for RenderingCity
         */
        protected java.lang.String localRenderingCity;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localRenderingCityTracker = false;

        /**
         * field for RenderingState
         */
        protected java.lang.String localRenderingState;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localRenderingStateTracker = false;

        /**
         * field for RenderingZip
         */
        protected java.lang.String localRenderingZip;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localRenderingZipTracker = false;

        /**
         * field for PriorAuthProviderTypeCode
         */
        protected java.lang.String localPriorAuthProviderTypeCode;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localPriorAuthProviderTypeCodeTracker = false;

        /**
         * field for PriorAuthHeaderKey
         */
        protected int localPriorAuthHeaderKey;

        /**
         * field for AppCreatedDateTime
         */
        protected java.lang.String localAppCreatedDateTime;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localAppCreatedDateTimeTracker = false;

        /**
         * field for AppCreatedUserId
         */
        protected java.lang.String localAppCreatedUserId;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localAppCreatedUserIdTracker = false;

        /**
         * field for AppMantDateTime
         */
        protected java.lang.String localAppMantDateTime;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localAppMantDateTimeTracker = false;

        /**
         * field for AppMantUserId
         */
        protected java.lang.String localAppMantUserId;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localAppMantUserIdTracker = false;

        public boolean isOrderingFirstNameSpecified() {
            return localOrderingFirstNameTracker;
        }

        /**
         * Auto generated getter method
         * @return java.lang.String
         */
        public java.lang.String getOrderingFirstName() {
            return localOrderingFirstName;
        }

        /**
         * Auto generated setter method
         * @param param OrderingFirstName
         */
        public void setOrderingFirstName(java.lang.String param) {
            localOrderingFirstNameTracker = param != null;

            this.localOrderingFirstName = param;
        }

        public boolean isOrderingLastNameSpecified() {
            return localOrderingLastNameTracker;
        }

        /**
         * Auto generated getter method
         * @return java.lang.String
         */
        public java.lang.String getOrderingLastName() {
            return localOrderingLastName;
        }

        /**
         * Auto generated setter method
         * @param param OrderingLastName
         */
        public void setOrderingLastName(java.lang.String param) {
            localOrderingLastNameTracker = param != null;

            this.localOrderingLastName = param;
        }

        public boolean isOrderingNpiSpecified() {
            return localOrderingNpiTracker;
        }

        /**
         * Auto generated getter method
         * @return java.lang.String
         */
        public java.lang.String getOrderingNpi() {
            return localOrderingNpi;
        }

        /**
         * Auto generated setter method
         * @param param OrderingNpi
         */
        public void setOrderingNpi(java.lang.String param) {
            localOrderingNpiTracker = param != null;

            this.localOrderingNpi = param;
        }

        public boolean isOrderingPhoneNumberSpecified() {
            return localOrderingPhoneNumberTracker;
        }

        /**
         * Auto generated getter method
         * @return java.lang.String
         */
        public java.lang.String getOrderingPhoneNumber() {
            return localOrderingPhoneNumber;
        }

        /**
         * Auto generated setter method
         * @param param OrderingPhoneNumber
         */
        public void setOrderingPhoneNumber(java.lang.String param) {
            localOrderingPhoneNumberTracker = param != null;

            this.localOrderingPhoneNumber = param;
        }

        public boolean isOrderingTinEinSpecified() {
            return localOrderingTinEinTracker;
        }

        /**
         * Auto generated getter method
         * @return java.lang.String
         */
        public java.lang.String getOrderingTinEin() {
            return localOrderingTinEin;
        }

        /**
         * Auto generated setter method
         * @param param OrderingTinEin
         */
        public void setOrderingTinEin(java.lang.String param) {
            localOrderingTinEinTracker = param != null;

            this.localOrderingTinEin = param;
        }

        public boolean isOrderingFaxNumberSpecified() {
            return localOrderingFaxNumberTracker;
        }

        /**
         * Auto generated getter method
         * @return java.lang.String
         */
        public java.lang.String getOrderingFaxNumber() {
            return localOrderingFaxNumber;
        }

        /**
         * Auto generated setter method
         * @param param OrderingFaxNumber
         */
        public void setOrderingFaxNumber(java.lang.String param) {
            localOrderingFaxNumberTracker = param != null;

            this.localOrderingFaxNumber = param;
        }

        public boolean isOrderingAddressLine1Specified() {
            return localOrderingAddressLine1Tracker;
        }

        /**
         * Auto generated getter method
         * @return java.lang.String
         */
        public java.lang.String getOrderingAddressLine1() {
            return localOrderingAddressLine1;
        }

        /**
         * Auto generated setter method
         * @param param OrderingAddressLine1
         */
        public void setOrderingAddressLine1(java.lang.String param) {
            localOrderingAddressLine1Tracker = param != null;

            this.localOrderingAddressLine1 = param;
        }

        public boolean isOrderingAddressLine2Specified() {
            return localOrderingAddressLine2Tracker;
        }

        /**
         * Auto generated getter method
         * @return java.lang.String
         */
        public java.lang.String getOrderingAddressLine2() {
            return localOrderingAddressLine2;
        }

        /**
         * Auto generated setter method
         * @param param OrderingAddressLine2
         */
        public void setOrderingAddressLine2(java.lang.String param) {
            localOrderingAddressLine2Tracker = param != null;

            this.localOrderingAddressLine2 = param;
        }

        public boolean isOrderingCitySpecified() {
            return localOrderingCityTracker;
        }

        /**
         * Auto generated getter method
         * @return java.lang.String
         */
        public java.lang.String getOrderingCity() {
            return localOrderingCity;
        }

        /**
         * Auto generated setter method
         * @param param OrderingCity
         */
        public void setOrderingCity(java.lang.String param) {
            localOrderingCityTracker = param != null;

            this.localOrderingCity = param;
        }

        public boolean isOrderingStateSpecified() {
            return localOrderingStateTracker;
        }

        /**
         * Auto generated getter method
         * @return java.lang.String
         */
        public java.lang.String getOrderingState() {
            return localOrderingState;
        }

        /**
         * Auto generated setter method
         * @param param OrderingState
         */
        public void setOrderingState(java.lang.String param) {
            localOrderingStateTracker = param != null;

            this.localOrderingState = param;
        }

        public boolean isOrderingZipSpecified() {
            return localOrderingZipTracker;
        }

        /**
         * Auto generated getter method
         * @return java.lang.String
         */
        public java.lang.String getOrderingZip() {
            return localOrderingZip;
        }

        /**
         * Auto generated setter method
         * @param param OrderingZip
         */
        public void setOrderingZip(java.lang.String param) {
            localOrderingZipTracker = param != null;

            this.localOrderingZip = param;
        }

        public boolean isRenderingLabNameSpecified() {
            return localRenderingLabNameTracker;
        }

        /**
         * Auto generated getter method
         * @return java.lang.String
         */
        public java.lang.String getRenderingLabName() {
            return localRenderingLabName;
        }

        /**
         * Auto generated setter method
         * @param param RenderingLabName
         */
        public void setRenderingLabName(java.lang.String param) {
            localRenderingLabNameTracker = param != null;

            this.localRenderingLabName = param;
        }

        public boolean isRenderingFirstNameSpecified() {
            return localRenderingFirstNameTracker;
        }

        /**
         * Auto generated getter method
         * @return java.lang.String
         */
        public java.lang.String getRenderingFirstName() {
            return localRenderingFirstName;
        }

        /**
         * Auto generated setter method
         * @param param RenderingFirstName
         */
        public void setRenderingFirstName(java.lang.String param) {
            localRenderingFirstNameTracker = param != null;

            this.localRenderingFirstName = param;
        }

        public boolean isRenderingLastNameSpecified() {
            return localRenderingLastNameTracker;
        }

        /**
         * Auto generated getter method
         * @return java.lang.String
         */
        public java.lang.String getRenderingLastName() {
            return localRenderingLastName;
        }

        /**
         * Auto generated setter method
         * @param param RenderingLastName
         */
        public void setRenderingLastName(java.lang.String param) {
            localRenderingLastNameTracker = param != null;

            this.localRenderingLastName = param;
        }

        public boolean isRenderingNpiSpecified() {
            return localRenderingNpiTracker;
        }

        /**
         * Auto generated getter method
         * @return java.lang.String
         */
        public java.lang.String getRenderingNpi() {
            return localRenderingNpi;
        }

        /**
         * Auto generated setter method
         * @param param RenderingNpi
         */
        public void setRenderingNpi(java.lang.String param) {
            localRenderingNpiTracker = param != null;

            this.localRenderingNpi = param;
        }

        public boolean isRenderingPhoneNumberSpecified() {
            return localRenderingPhoneNumberTracker;
        }

        /**
         * Auto generated getter method
         * @return java.lang.String
         */
        public java.lang.String getRenderingPhoneNumber() {
            return localRenderingPhoneNumber;
        }

        /**
         * Auto generated setter method
         * @param param RenderingPhoneNumber
         */
        public void setRenderingPhoneNumber(java.lang.String param) {
            localRenderingPhoneNumberTracker = param != null;

            this.localRenderingPhoneNumber = param;
        }

        public boolean isRenderingTinEinSpecified() {
            return localRenderingTinEinTracker;
        }

        /**
         * Auto generated getter method
         * @return java.lang.String
         */
        public java.lang.String getRenderingTinEin() {
            return localRenderingTinEin;
        }

        /**
         * Auto generated setter method
         * @param param RenderingTinEin
         */
        public void setRenderingTinEin(java.lang.String param) {
            localRenderingTinEinTracker = param != null;

            this.localRenderingTinEin = param;
        }

        public boolean isRenderingFaxNumberSpecified() {
            return localRenderingFaxNumberTracker;
        }

        /**
         * Auto generated getter method
         * @return java.lang.String
         */
        public java.lang.String getRenderingFaxNumber() {
            return localRenderingFaxNumber;
        }

        /**
         * Auto generated setter method
         * @param param RenderingFaxNumber
         */
        public void setRenderingFaxNumber(java.lang.String param) {
            localRenderingFaxNumberTracker = param != null;

            this.localRenderingFaxNumber = param;
        }

        public boolean isRenderingAddressLine1Specified() {
            return localRenderingAddressLine1Tracker;
        }

        /**
         * Auto generated getter method
         * @return java.lang.String
         */
        public java.lang.String getRenderingAddressLine1() {
            return localRenderingAddressLine1;
        }

        /**
         * Auto generated setter method
         * @param param RenderingAddressLine1
         */
        public void setRenderingAddressLine1(java.lang.String param) {
            localRenderingAddressLine1Tracker = param != null;

            this.localRenderingAddressLine1 = param;
        }

        public boolean isRenderingAddressLine2Specified() {
            return localRenderingAddressLine2Tracker;
        }

        /**
         * Auto generated getter method
         * @return java.lang.String
         */
        public java.lang.String getRenderingAddressLine2() {
            return localRenderingAddressLine2;
        }

        /**
         * Auto generated setter method
         * @param param RenderingAddressLine2
         */
        public void setRenderingAddressLine2(java.lang.String param) {
            localRenderingAddressLine2Tracker = param != null;

            this.localRenderingAddressLine2 = param;
        }

        public boolean isRenderingCitySpecified() {
            return localRenderingCityTracker;
        }

        /**
         * Auto generated getter method
         * @return java.lang.String
         */
        public java.lang.String getRenderingCity() {
            return localRenderingCity;
        }

        /**
         * Auto generated setter method
         * @param param RenderingCity
         */
        public void setRenderingCity(java.lang.String param) {
            localRenderingCityTracker = param != null;

            this.localRenderingCity = param;
        }

        public boolean isRenderingStateSpecified() {
            return localRenderingStateTracker;
        }

        /**
         * Auto generated getter method
         * @return java.lang.String
         */
        public java.lang.String getRenderingState() {
            return localRenderingState;
        }

        /**
         * Auto generated setter method
         * @param param RenderingState
         */
        public void setRenderingState(java.lang.String param) {
            localRenderingStateTracker = param != null;

            this.localRenderingState = param;
        }

        public boolean isRenderingZipSpecified() {
            return localRenderingZipTracker;
        }

        /**
         * Auto generated getter method
         * @return java.lang.String
         */
        public java.lang.String getRenderingZip() {
            return localRenderingZip;
        }

        /**
         * Auto generated setter method
         * @param param RenderingZip
         */
        public void setRenderingZip(java.lang.String param) {
            localRenderingZipTracker = param != null;

            this.localRenderingZip = param;
        }

        public boolean isPriorAuthProviderTypeCodeSpecified() {
            return localPriorAuthProviderTypeCodeTracker;
        }

        /**
         * Auto generated getter method
         * @return java.lang.String
         */
        public java.lang.String getPriorAuthProviderTypeCode() {
            return localPriorAuthProviderTypeCode;
        }

        /**
         * Auto generated setter method
         * @param param PriorAuthProviderTypeCode
         */
        public void setPriorAuthProviderTypeCode(java.lang.String param) {
            localPriorAuthProviderTypeCodeTracker = param != null;

            this.localPriorAuthProviderTypeCode = param;
        }

        /**
         * Auto generated getter method
         * @return int
         */
        public int getPriorAuthHeaderKey() {
            return localPriorAuthHeaderKey;
        }

        /**
         * Auto generated setter method
         * @param param PriorAuthHeaderKey
         */
        public void setPriorAuthHeaderKey(int param) {
            this.localPriorAuthHeaderKey = param;
        }

        public boolean isAppCreatedDateTimeSpecified() {
            return localAppCreatedDateTimeTracker;
        }

        /**
         * Auto generated getter method
         * @return java.lang.String
         */
        public java.lang.String getAppCreatedDateTime() {
            return localAppCreatedDateTime;
        }

        /**
         * Auto generated setter method
         * @param param AppCreatedDateTime
         */
        public void setAppCreatedDateTime(java.lang.String param) {
            localAppCreatedDateTimeTracker = param != null;

            this.localAppCreatedDateTime = param;
        }

        public boolean isAppCreatedUserIdSpecified() {
            return localAppCreatedUserIdTracker;
        }

        /**
         * Auto generated getter method
         * @return java.lang.String
         */
        public java.lang.String getAppCreatedUserId() {
            return localAppCreatedUserId;
        }

        /**
         * Auto generated setter method
         * @param param AppCreatedUserId
         */
        public void setAppCreatedUserId(java.lang.String param) {
            localAppCreatedUserIdTracker = param != null;

            this.localAppCreatedUserId = param;
        }

        public boolean isAppMantDateTimeSpecified() {
            return localAppMantDateTimeTracker;
        }

        /**
         * Auto generated getter method
         * @return java.lang.String
         */
        public java.lang.String getAppMantDateTime() {
            return localAppMantDateTime;
        }

        /**
         * Auto generated setter method
         * @param param AppMantDateTime
         */
        public void setAppMantDateTime(java.lang.String param) {
            localAppMantDateTimeTracker = param != null;

            this.localAppMantDateTime = param;
        }

        public boolean isAppMantUserIdSpecified() {
            return localAppMantUserIdTracker;
        }

        /**
         * Auto generated getter method
         * @return java.lang.String
         */
        public java.lang.String getAppMantUserId() {
            return localAppMantUserId;
        }

        /**
         * Auto generated setter method
         * @param param AppMantUserId
         */
        public void setAppMantUserId(java.lang.String param) {
            localAppMantUserIdTracker = param != null;

            this.localAppMantUserId = param;
        }

        /**
         *
         * @param parentQName
         * @param factory
         * @return org.apache.axiom.om.OMElement
         */
        public org.apache.axiom.om.OMElement getOMElement(
            final javax.xml.namespace.QName parentQName,
            final org.apache.axiom.om.OMFactory factory)
            throws org.apache.axis2.databinding.ADBException {
            org.apache.axiom.om.OMDataSource dataSource = new org.apache.axis2.databinding.ADBDataSource(this,
                    parentQName);

            return factory.createOMElement(dataSource, parentQName);
        }

        public void serialize(final javax.xml.namespace.QName parentQName,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException,
                org.apache.axis2.databinding.ADBException {
            serialize(parentQName, xmlWriter, false);
        }

        public void serialize(final javax.xml.namespace.QName parentQName,
            javax.xml.stream.XMLStreamWriter xmlWriter, boolean serializeType)
            throws javax.xml.stream.XMLStreamException,
                org.apache.axis2.databinding.ADBException {
            java.lang.String prefix = null;
            java.lang.String namespace = null;

            prefix = parentQName.getPrefix();
            namespace = parentQName.getNamespaceURI();
            writeStartElement(prefix, namespace, parentQName.getLocalPart(),
                xmlWriter);

            if (serializeType) {
                java.lang.String namespacePrefix = registerPrefix(xmlWriter,
                        "http://services.lbm.avalon.com/");

                if ((namespacePrefix != null) &&
                        (namespacePrefix.trim().length() > 0)) {
                    writeAttribute("xsi",
                        "http://www.w3.org/2001/XMLSchema-instance", "type",
                        namespacePrefix + ":providerDTO", xmlWriter);
                } else {
                    writeAttribute("xsi",
                        "http://www.w3.org/2001/XMLSchema-instance", "type",
                        "providerDTO", xmlWriter);
                }
            }

            if (localOrderingFirstNameTracker) {
                namespace = "";
                writeStartElement(null, namespace, "orderingFirstName",
                    xmlWriter);

                if (localOrderingFirstName == null) {
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException(
                        "orderingFirstName cannot be null!!");
                } else {
                    xmlWriter.writeCharacters(localOrderingFirstName);
                }

                xmlWriter.writeEndElement();
            }

            if (localOrderingLastNameTracker) {
                namespace = "";
                writeStartElement(null, namespace, "orderingLastName", xmlWriter);

                if (localOrderingLastName == null) {
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException(
                        "orderingLastName cannot be null!!");
                } else {
                    xmlWriter.writeCharacters(localOrderingLastName);
                }

                xmlWriter.writeEndElement();
            }

            if (localOrderingNpiTracker) {
                namespace = "";
                writeStartElement(null, namespace, "orderingNpi", xmlWriter);

                if (localOrderingNpi == null) {
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException(
                        "orderingNpi cannot be null!!");
                } else {
                    xmlWriter.writeCharacters(localOrderingNpi);
                }

                xmlWriter.writeEndElement();
            }

            if (localOrderingPhoneNumberTracker) {
                namespace = "";
                writeStartElement(null, namespace, "orderingPhoneNumber",
                    xmlWriter);

                if (localOrderingPhoneNumber == null) {
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException(
                        "orderingPhoneNumber cannot be null!!");
                } else {
                    xmlWriter.writeCharacters(localOrderingPhoneNumber);
                }

                xmlWriter.writeEndElement();
            }

            if (localOrderingTinEinTracker) {
                namespace = "";
                writeStartElement(null, namespace, "orderingTinEin", xmlWriter);

                if (localOrderingTinEin == null) {
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException(
                        "orderingTinEin cannot be null!!");
                } else {
                    xmlWriter.writeCharacters(localOrderingTinEin);
                }

                xmlWriter.writeEndElement();
            }

            if (localOrderingFaxNumberTracker) {
                namespace = "";
                writeStartElement(null, namespace, "orderingFaxNumber",
                    xmlWriter);

                if (localOrderingFaxNumber == null) {
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException(
                        "orderingFaxNumber cannot be null!!");
                } else {
                    xmlWriter.writeCharacters(localOrderingFaxNumber);
                }

                xmlWriter.writeEndElement();
            }

            if (localOrderingAddressLine1Tracker) {
                namespace = "";
                writeStartElement(null, namespace, "orderingAddressLine1",
                    xmlWriter);

                if (localOrderingAddressLine1 == null) {
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException(
                        "orderingAddressLine1 cannot be null!!");
                } else {
                    xmlWriter.writeCharacters(localOrderingAddressLine1);
                }

                xmlWriter.writeEndElement();
            }

            if (localOrderingAddressLine2Tracker) {
                namespace = "";
                writeStartElement(null, namespace, "orderingAddressLine2",
                    xmlWriter);

                if (localOrderingAddressLine2 == null) {
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException(
                        "orderingAddressLine2 cannot be null!!");
                } else {
                    xmlWriter.writeCharacters(localOrderingAddressLine2);
                }

                xmlWriter.writeEndElement();
            }

            if (localOrderingCityTracker) {
                namespace = "";
                writeStartElement(null, namespace, "orderingCity", xmlWriter);

                if (localOrderingCity == null) {
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException(
                        "orderingCity cannot be null!!");
                } else {
                    xmlWriter.writeCharacters(localOrderingCity);
                }

                xmlWriter.writeEndElement();
            }

            if (localOrderingStateTracker) {
                namespace = "";
                writeStartElement(null, namespace, "orderingState", xmlWriter);

                if (localOrderingState == null) {
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException(
                        "orderingState cannot be null!!");
                } else {
                    xmlWriter.writeCharacters(localOrderingState);
                }

                xmlWriter.writeEndElement();
            }

            if (localOrderingZipTracker) {
                namespace = "";
                writeStartElement(null, namespace, "orderingZip", xmlWriter);

                if (localOrderingZip == null) {
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException(
                        "orderingZip cannot be null!!");
                } else {
                    xmlWriter.writeCharacters(localOrderingZip);
                }

                xmlWriter.writeEndElement();
            }

            if (localRenderingLabNameTracker) {
                namespace = "";
                writeStartElement(null, namespace, "renderingLabName", xmlWriter);

                if (localRenderingLabName == null) {
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException(
                        "renderingLabName cannot be null!!");
                } else {
                    xmlWriter.writeCharacters(localRenderingLabName);
                }

                xmlWriter.writeEndElement();
            }

            if (localRenderingFirstNameTracker) {
                namespace = "";
                writeStartElement(null, namespace, "renderingFirstName",
                    xmlWriter);

                if (localRenderingFirstName == null) {
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException(
                        "renderingFirstName cannot be null!!");
                } else {
                    xmlWriter.writeCharacters(localRenderingFirstName);
                }

                xmlWriter.writeEndElement();
            }

            if (localRenderingLastNameTracker) {
                namespace = "";
                writeStartElement(null, namespace, "renderingLastName",
                    xmlWriter);

                if (localRenderingLastName == null) {
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException(
                        "renderingLastName cannot be null!!");
                } else {
                    xmlWriter.writeCharacters(localRenderingLastName);
                }

                xmlWriter.writeEndElement();
            }

            if (localRenderingNpiTracker) {
                namespace = "";
                writeStartElement(null, namespace, "renderingNpi", xmlWriter);

                if (localRenderingNpi == null) {
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException(
                        "renderingNpi cannot be null!!");
                } else {
                    xmlWriter.writeCharacters(localRenderingNpi);
                }

                xmlWriter.writeEndElement();
            }

            if (localRenderingPhoneNumberTracker) {
                namespace = "";
                writeStartElement(null, namespace, "renderingPhoneNumber",
                    xmlWriter);

                if (localRenderingPhoneNumber == null) {
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException(
                        "renderingPhoneNumber cannot be null!!");
                } else {
                    xmlWriter.writeCharacters(localRenderingPhoneNumber);
                }

                xmlWriter.writeEndElement();
            }

            if (localRenderingTinEinTracker) {
                namespace = "";
                writeStartElement(null, namespace, "renderingTinEin", xmlWriter);

                if (localRenderingTinEin == null) {
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException(
                        "renderingTinEin cannot be null!!");
                } else {
                    xmlWriter.writeCharacters(localRenderingTinEin);
                }

                xmlWriter.writeEndElement();
            }

            if (localRenderingFaxNumberTracker) {
                namespace = "";
                writeStartElement(null, namespace, "renderingFaxNumber",
                    xmlWriter);

                if (localRenderingFaxNumber == null) {
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException(
                        "renderingFaxNumber cannot be null!!");
                } else {
                    xmlWriter.writeCharacters(localRenderingFaxNumber);
                }

                xmlWriter.writeEndElement();
            }

            if (localRenderingAddressLine1Tracker) {
                namespace = "";
                writeStartElement(null, namespace, "renderingAddressLine1",
                    xmlWriter);

                if (localRenderingAddressLine1 == null) {
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException(
                        "renderingAddressLine1 cannot be null!!");
                } else {
                    xmlWriter.writeCharacters(localRenderingAddressLine1);
                }

                xmlWriter.writeEndElement();
            }

            if (localRenderingAddressLine2Tracker) {
                namespace = "";
                writeStartElement(null, namespace, "renderingAddressLine2",
                    xmlWriter);

                if (localRenderingAddressLine2 == null) {
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException(
                        "renderingAddressLine2 cannot be null!!");
                } else {
                    xmlWriter.writeCharacters(localRenderingAddressLine2);
                }

                xmlWriter.writeEndElement();
            }

            if (localRenderingCityTracker) {
                namespace = "";
                writeStartElement(null, namespace, "renderingCity", xmlWriter);

                if (localRenderingCity == null) {
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException(
                        "renderingCity cannot be null!!");
                } else {
                    xmlWriter.writeCharacters(localRenderingCity);
                }

                xmlWriter.writeEndElement();
            }

            if (localRenderingStateTracker) {
                namespace = "";
                writeStartElement(null, namespace, "renderingState", xmlWriter);

                if (localRenderingState == null) {
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException(
                        "renderingState cannot be null!!");
                } else {
                    xmlWriter.writeCharacters(localRenderingState);
                }

                xmlWriter.writeEndElement();
            }

            if (localRenderingZipTracker) {
                namespace = "";
                writeStartElement(null, namespace, "renderingZip", xmlWriter);

                if (localRenderingZip == null) {
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException(
                        "renderingZip cannot be null!!");
                } else {
                    xmlWriter.writeCharacters(localRenderingZip);
                }

                xmlWriter.writeEndElement();
            }

            if (localPriorAuthProviderTypeCodeTracker) {
                namespace = "";
                writeStartElement(null, namespace, "priorAuthProviderTypeCode",
                    xmlWriter);

                if (localPriorAuthProviderTypeCode == null) {
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException(
                        "priorAuthProviderTypeCode cannot be null!!");
                } else {
                    xmlWriter.writeCharacters(localPriorAuthProviderTypeCode);
                }

                xmlWriter.writeEndElement();
            }

            namespace = "";
            writeStartElement(null, namespace, "priorAuthHeaderKey", xmlWriter);

            if (localPriorAuthHeaderKey == java.lang.Integer.MIN_VALUE) {
                throw new org.apache.axis2.databinding.ADBException(
                    "priorAuthHeaderKey cannot be null!!");
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        localPriorAuthHeaderKey));
            }

            xmlWriter.writeEndElement();

            if (localAppCreatedDateTimeTracker) {
                namespace = "";
                writeStartElement(null, namespace, "appCreatedDateTime",
                    xmlWriter);

                if (localAppCreatedDateTime == null) {
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException(
                        "appCreatedDateTime cannot be null!!");
                } else {
                    xmlWriter.writeCharacters(localAppCreatedDateTime);
                }

                xmlWriter.writeEndElement();
            }

            if (localAppCreatedUserIdTracker) {
                namespace = "";
                writeStartElement(null, namespace, "appCreatedUserId", xmlWriter);

                if (localAppCreatedUserId == null) {
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException(
                        "appCreatedUserId cannot be null!!");
                } else {
                    xmlWriter.writeCharacters(localAppCreatedUserId);
                }

                xmlWriter.writeEndElement();
            }

            if (localAppMantDateTimeTracker) {
                namespace = "";
                writeStartElement(null, namespace, "appMantDateTime", xmlWriter);

                if (localAppMantDateTime == null) {
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException(
                        "appMantDateTime cannot be null!!");
                } else {
                    xmlWriter.writeCharacters(localAppMantDateTime);
                }

                xmlWriter.writeEndElement();
            }

            if (localAppMantUserIdTracker) {
                namespace = "";
                writeStartElement(null, namespace, "appMantUserId", xmlWriter);

                if (localAppMantUserId == null) {
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException(
                        "appMantUserId cannot be null!!");
                } else {
                    xmlWriter.writeCharacters(localAppMantUserId);
                }

                xmlWriter.writeEndElement();
            }

            xmlWriter.writeEndElement();
        }

        private static java.lang.String generatePrefix(
            java.lang.String namespace) {
            if (namespace.equals("http://services.lbm.avalon.com/")) {
                return "ns1";
            }

            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix,
            java.lang.String namespace, java.lang.String localPart,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

            if (writerPrefix != null) {
                xmlWriter.writeStartElement(namespace, localPart);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }

        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,
            java.lang.String namespace, java.lang.String attName,
            java.lang.String attValue,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }

            xmlWriter.writeAttribute(namespace, attName, attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,
            java.lang.String attName, java.lang.String attValue,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attValue);
            }
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeQNameAttribute(java.lang.String namespace,
            java.lang.String attName, javax.xml.namespace.QName qname,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String attributeNamespace = qname.getNamespaceURI();
            java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);

            if (attributePrefix == null) {
                attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
            }

            java.lang.String attributeValue;

            if (attributePrefix.trim().length() > 0) {
                attributeValue = attributePrefix + ":" + qname.getLocalPart();
            } else {
                attributeValue = qname.getLocalPart();
            }

            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attributeValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attributeValue);
            }
        }

        /**
         *  method to handle Qnames
         */
        private void writeQName(javax.xml.namespace.QName qname,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();

            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);

                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix, namespaceURI);
                }

                if (prefix.trim().length() > 0) {
                    xmlWriter.writeCharacters(prefix + ":" +
                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            qname));
                }
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }

                    namespaceURI = qnames[i].getNamespaceURI();

                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);

                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix, namespaceURI);
                        }

                        if (prefix.trim().length() > 0) {
                            stringToWrite.append(prefix).append(":")
                                         .append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                    qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                    qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                qnames[i]));
                    }
                }

                xmlWriter.writeCharacters(stringToWrite.toString());
            }
        }

        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(
            javax.xml.stream.XMLStreamWriter xmlWriter,
            java.lang.String namespace)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);

            if (prefix == null) {
                prefix = generatePrefix(namespace);

                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();

                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);

                    if ((uri == null) || (uri.length() == 0)) {
                        break;
                    }

                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }

                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }

            return prefix;
        }

        /**
         * databinding method to get an XML representation of this object
         *
         */
        public javax.xml.stream.XMLStreamReader getPullParser(
            javax.xml.namespace.QName qName)
            throws org.apache.axis2.databinding.ADBException {
            java.util.ArrayList elementList = new java.util.ArrayList();
            java.util.ArrayList attribList = new java.util.ArrayList();

            if (localOrderingFirstNameTracker) {
                elementList.add(new javax.xml.namespace.QName("",
                        "orderingFirstName"));

                if (localOrderingFirstName != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            localOrderingFirstName));
                } else {
                    throw new org.apache.axis2.databinding.ADBException(
                        "orderingFirstName cannot be null!!");
                }
            }

            if (localOrderingLastNameTracker) {
                elementList.add(new javax.xml.namespace.QName("",
                        "orderingLastName"));

                if (localOrderingLastName != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            localOrderingLastName));
                } else {
                    throw new org.apache.axis2.databinding.ADBException(
                        "orderingLastName cannot be null!!");
                }
            }

            if (localOrderingNpiTracker) {
                elementList.add(new javax.xml.namespace.QName("", "orderingNpi"));

                if (localOrderingNpi != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            localOrderingNpi));
                } else {
                    throw new org.apache.axis2.databinding.ADBException(
                        "orderingNpi cannot be null!!");
                }
            }

            if (localOrderingPhoneNumberTracker) {
                elementList.add(new javax.xml.namespace.QName("",
                        "orderingPhoneNumber"));

                if (localOrderingPhoneNumber != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            localOrderingPhoneNumber));
                } else {
                    throw new org.apache.axis2.databinding.ADBException(
                        "orderingPhoneNumber cannot be null!!");
                }
            }

            if (localOrderingTinEinTracker) {
                elementList.add(new javax.xml.namespace.QName("",
                        "orderingTinEin"));

                if (localOrderingTinEin != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            localOrderingTinEin));
                } else {
                    throw new org.apache.axis2.databinding.ADBException(
                        "orderingTinEin cannot be null!!");
                }
            }

            if (localOrderingFaxNumberTracker) {
                elementList.add(new javax.xml.namespace.QName("",
                        "orderingFaxNumber"));

                if (localOrderingFaxNumber != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            localOrderingFaxNumber));
                } else {
                    throw new org.apache.axis2.databinding.ADBException(
                        "orderingFaxNumber cannot be null!!");
                }
            }

            if (localOrderingAddressLine1Tracker) {
                elementList.add(new javax.xml.namespace.QName("",
                        "orderingAddressLine1"));

                if (localOrderingAddressLine1 != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            localOrderingAddressLine1));
                } else {
                    throw new org.apache.axis2.databinding.ADBException(
                        "orderingAddressLine1 cannot be null!!");
                }
            }

            if (localOrderingAddressLine2Tracker) {
                elementList.add(new javax.xml.namespace.QName("",
                        "orderingAddressLine2"));

                if (localOrderingAddressLine2 != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            localOrderingAddressLine2));
                } else {
                    throw new org.apache.axis2.databinding.ADBException(
                        "orderingAddressLine2 cannot be null!!");
                }
            }

            if (localOrderingCityTracker) {
                elementList.add(new javax.xml.namespace.QName("", "orderingCity"));

                if (localOrderingCity != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            localOrderingCity));
                } else {
                    throw new org.apache.axis2.databinding.ADBException(
                        "orderingCity cannot be null!!");
                }
            }

            if (localOrderingStateTracker) {
                elementList.add(new javax.xml.namespace.QName("",
                        "orderingState"));

                if (localOrderingState != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            localOrderingState));
                } else {
                    throw new org.apache.axis2.databinding.ADBException(
                        "orderingState cannot be null!!");
                }
            }

            if (localOrderingZipTracker) {
                elementList.add(new javax.xml.namespace.QName("", "orderingZip"));

                if (localOrderingZip != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            localOrderingZip));
                } else {
                    throw new org.apache.axis2.databinding.ADBException(
                        "orderingZip cannot be null!!");
                }
            }

            if (localRenderingLabNameTracker) {
                elementList.add(new javax.xml.namespace.QName("",
                        "renderingLabName"));

                if (localRenderingLabName != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            localRenderingLabName));
                } else {
                    throw new org.apache.axis2.databinding.ADBException(
                        "renderingLabName cannot be null!!");
                }
            }

            if (localRenderingFirstNameTracker) {
                elementList.add(new javax.xml.namespace.QName("",
                        "renderingFirstName"));

                if (localRenderingFirstName != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            localRenderingFirstName));
                } else {
                    throw new org.apache.axis2.databinding.ADBException(
                        "renderingFirstName cannot be null!!");
                }
            }

            if (localRenderingLastNameTracker) {
                elementList.add(new javax.xml.namespace.QName("",
                        "renderingLastName"));

                if (localRenderingLastName != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            localRenderingLastName));
                } else {
                    throw new org.apache.axis2.databinding.ADBException(
                        "renderingLastName cannot be null!!");
                }
            }

            if (localRenderingNpiTracker) {
                elementList.add(new javax.xml.namespace.QName("", "renderingNpi"));

                if (localRenderingNpi != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            localRenderingNpi));
                } else {
                    throw new org.apache.axis2.databinding.ADBException(
                        "renderingNpi cannot be null!!");
                }
            }

            if (localRenderingPhoneNumberTracker) {
                elementList.add(new javax.xml.namespace.QName("",
                        "renderingPhoneNumber"));

                if (localRenderingPhoneNumber != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            localRenderingPhoneNumber));
                } else {
                    throw new org.apache.axis2.databinding.ADBException(
                        "renderingPhoneNumber cannot be null!!");
                }
            }

            if (localRenderingTinEinTracker) {
                elementList.add(new javax.xml.namespace.QName("",
                        "renderingTinEin"));

                if (localRenderingTinEin != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            localRenderingTinEin));
                } else {
                    throw new org.apache.axis2.databinding.ADBException(
                        "renderingTinEin cannot be null!!");
                }
            }

            if (localRenderingFaxNumberTracker) {
                elementList.add(new javax.xml.namespace.QName("",
                        "renderingFaxNumber"));

                if (localRenderingFaxNumber != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            localRenderingFaxNumber));
                } else {
                    throw new org.apache.axis2.databinding.ADBException(
                        "renderingFaxNumber cannot be null!!");
                }
            }

            if (localRenderingAddressLine1Tracker) {
                elementList.add(new javax.xml.namespace.QName("",
                        "renderingAddressLine1"));

                if (localRenderingAddressLine1 != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            localRenderingAddressLine1));
                } else {
                    throw new org.apache.axis2.databinding.ADBException(
                        "renderingAddressLine1 cannot be null!!");
                }
            }

            if (localRenderingAddressLine2Tracker) {
                elementList.add(new javax.xml.namespace.QName("",
                        "renderingAddressLine2"));

                if (localRenderingAddressLine2 != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            localRenderingAddressLine2));
                } else {
                    throw new org.apache.axis2.databinding.ADBException(
                        "renderingAddressLine2 cannot be null!!");
                }
            }

            if (localRenderingCityTracker) {
                elementList.add(new javax.xml.namespace.QName("",
                        "renderingCity"));

                if (localRenderingCity != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            localRenderingCity));
                } else {
                    throw new org.apache.axis2.databinding.ADBException(
                        "renderingCity cannot be null!!");
                }
            }

            if (localRenderingStateTracker) {
                elementList.add(new javax.xml.namespace.QName("",
                        "renderingState"));

                if (localRenderingState != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            localRenderingState));
                } else {
                    throw new org.apache.axis2.databinding.ADBException(
                        "renderingState cannot be null!!");
                }
            }

            if (localRenderingZipTracker) {
                elementList.add(new javax.xml.namespace.QName("", "renderingZip"));

                if (localRenderingZip != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            localRenderingZip));
                } else {
                    throw new org.apache.axis2.databinding.ADBException(
                        "renderingZip cannot be null!!");
                }
            }

            if (localPriorAuthProviderTypeCodeTracker) {
                elementList.add(new javax.xml.namespace.QName("",
                        "priorAuthProviderTypeCode"));

                if (localPriorAuthProviderTypeCode != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            localPriorAuthProviderTypeCode));
                } else {
                    throw new org.apache.axis2.databinding.ADBException(
                        "priorAuthProviderTypeCode cannot be null!!");
                }
            }

            elementList.add(new javax.xml.namespace.QName("",
                    "priorAuthHeaderKey"));

            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                    localPriorAuthHeaderKey));

            if (localAppCreatedDateTimeTracker) {
                elementList.add(new javax.xml.namespace.QName("",
                        "appCreatedDateTime"));

                if (localAppCreatedDateTime != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            localAppCreatedDateTime));
                } else {
                    throw new org.apache.axis2.databinding.ADBException(
                        "appCreatedDateTime cannot be null!!");
                }
            }

            if (localAppCreatedUserIdTracker) {
                elementList.add(new javax.xml.namespace.QName("",
                        "appCreatedUserId"));

                if (localAppCreatedUserId != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            localAppCreatedUserId));
                } else {
                    throw new org.apache.axis2.databinding.ADBException(
                        "appCreatedUserId cannot be null!!");
                }
            }

            if (localAppMantDateTimeTracker) {
                elementList.add(new javax.xml.namespace.QName("",
                        "appMantDateTime"));

                if (localAppMantDateTime != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            localAppMantDateTime));
                } else {
                    throw new org.apache.axis2.databinding.ADBException(
                        "appMantDateTime cannot be null!!");
                }
            }

            if (localAppMantUserIdTracker) {
                elementList.add(new javax.xml.namespace.QName("",
                        "appMantUserId"));

                if (localAppMantUserId != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            localAppMantUserId));
                } else {
                    throw new org.apache.axis2.databinding.ADBException(
                        "appMantUserId cannot be null!!");
                }
            }

            return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName,
                elementList.toArray(), attribList.toArray());
        }

        /**
         *  Factory class that keeps the parse method
         */
        public static class Factory {
            /**
             * static method to create the object
             * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
             *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
             * Postcondition: If this object is an element, the reader is positioned at its end element
             *                If this object is a complex type, the reader is positioned at the end element of its outer element
             */
            public static ProviderDTO parse(
                javax.xml.stream.XMLStreamReader reader)
                throws java.lang.Exception {
                ProviderDTO object = new ProviderDTO();

                int event;
                java.lang.String nillableValue = null;
                java.lang.String prefix = "";
                java.lang.String namespaceuri = "";

                try {
                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.getAttributeValue(
                                "http://www.w3.org/2001/XMLSchema-instance",
                                "type") != null) {
                        java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                "type");

                        if (fullTypeName != null) {
                            java.lang.String nsPrefix = null;

                            if (fullTypeName.indexOf(":") > -1) {
                                nsPrefix = fullTypeName.substring(0,
                                        fullTypeName.indexOf(":"));
                            }

                            nsPrefix = (nsPrefix == null) ? "" : nsPrefix;

                            java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(
                                        ":") + 1);

                            if (!"providerDTO".equals(type)) {
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext()
                                                               .getNamespaceURI(nsPrefix);

                                return (ProviderDTO) ExtensionMapper.getTypeObject(nsUri,
                                    type, reader);
                            }
                        }
                    }

                    // Note all attributes that were handled. Used to differ normal attributes
                    // from anyAttributes.
                    java.util.Vector handledAttributes = new java.util.Vector();

                    reader.next();

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                            new javax.xml.namespace.QName("",
                                "orderingFirstName").equals(reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                "nil");

                        if ("true".equals(nillableValue) ||
                                "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "orderingFirstName" +
                                "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setOrderingFirstName(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                            new javax.xml.namespace.QName("", "orderingLastName").equals(
                                reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                "nil");

                        if ("true".equals(nillableValue) ||
                                "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "orderingLastName" +
                                "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setOrderingLastName(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                            new javax.xml.namespace.QName("", "orderingNpi").equals(
                                reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                "nil");

                        if ("true".equals(nillableValue) ||
                                "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "orderingNpi" +
                                "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setOrderingNpi(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                            new javax.xml.namespace.QName("",
                                "orderingPhoneNumber").equals(reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                "nil");

                        if ("true".equals(nillableValue) ||
                                "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "orderingPhoneNumber" +
                                "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setOrderingPhoneNumber(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                            new javax.xml.namespace.QName("", "orderingTinEin").equals(
                                reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                "nil");

                        if ("true".equals(nillableValue) ||
                                "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "orderingTinEin" +
                                "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setOrderingTinEin(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                            new javax.xml.namespace.QName("",
                                "orderingFaxNumber").equals(reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                "nil");

                        if ("true".equals(nillableValue) ||
                                "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "orderingFaxNumber" +
                                "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setOrderingFaxNumber(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                            new javax.xml.namespace.QName("",
                                "orderingAddressLine1").equals(reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                "nil");

                        if ("true".equals(nillableValue) ||
                                "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "orderingAddressLine1" +
                                "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setOrderingAddressLine1(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                            new javax.xml.namespace.QName("",
                                "orderingAddressLine2").equals(reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                "nil");

                        if ("true".equals(nillableValue) ||
                                "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "orderingAddressLine2" +
                                "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setOrderingAddressLine2(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                            new javax.xml.namespace.QName("", "orderingCity").equals(
                                reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                "nil");

                        if ("true".equals(nillableValue) ||
                                "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "orderingCity" +
                                "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setOrderingCity(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                            new javax.xml.namespace.QName("", "orderingState").equals(
                                reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                "nil");

                        if ("true".equals(nillableValue) ||
                                "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "orderingState" +
                                "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setOrderingState(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                            new javax.xml.namespace.QName("", "orderingZip").equals(
                                reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                "nil");

                        if ("true".equals(nillableValue) ||
                                "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "orderingZip" +
                                "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setOrderingZip(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                            new javax.xml.namespace.QName("", "renderingLabName").equals(
                                reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                "nil");

                        if ("true".equals(nillableValue) ||
                                "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "renderingLabName" +
                                "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setRenderingLabName(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                            new javax.xml.namespace.QName("",
                                "renderingFirstName").equals(reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                "nil");

                        if ("true".equals(nillableValue) ||
                                "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "renderingFirstName" +
                                "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setRenderingFirstName(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                            new javax.xml.namespace.QName("",
                                "renderingLastName").equals(reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                "nil");

                        if ("true".equals(nillableValue) ||
                                "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "renderingLastName" +
                                "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setRenderingLastName(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                            new javax.xml.namespace.QName("", "renderingNpi").equals(
                                reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                "nil");

                        if ("true".equals(nillableValue) ||
                                "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "renderingNpi" +
                                "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setRenderingNpi(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                            new javax.xml.namespace.QName("",
                                "renderingPhoneNumber").equals(reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                "nil");

                        if ("true".equals(nillableValue) ||
                                "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "renderingPhoneNumber" +
                                "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setRenderingPhoneNumber(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                            new javax.xml.namespace.QName("", "renderingTinEin").equals(
                                reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                "nil");

                        if ("true".equals(nillableValue) ||
                                "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "renderingTinEin" +
                                "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setRenderingTinEin(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                            new javax.xml.namespace.QName("",
                                "renderingFaxNumber").equals(reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                "nil");

                        if ("true".equals(nillableValue) ||
                                "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "renderingFaxNumber" +
                                "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setRenderingFaxNumber(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                            new javax.xml.namespace.QName("",
                                "renderingAddressLine1").equals(
                                reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                "nil");

                        if ("true".equals(nillableValue) ||
                                "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "renderingAddressLine1" +
                                "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setRenderingAddressLine1(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                            new javax.xml.namespace.QName("",
                                "renderingAddressLine2").equals(
                                reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                "nil");

                        if ("true".equals(nillableValue) ||
                                "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "renderingAddressLine2" +
                                "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setRenderingAddressLine2(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                            new javax.xml.namespace.QName("", "renderingCity").equals(
                                reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                "nil");

                        if ("true".equals(nillableValue) ||
                                "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "renderingCity" +
                                "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setRenderingCity(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                            new javax.xml.namespace.QName("", "renderingState").equals(
                                reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                "nil");

                        if ("true".equals(nillableValue) ||
                                "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "renderingState" +
                                "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setRenderingState(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                            new javax.xml.namespace.QName("", "renderingZip").equals(
                                reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                "nil");

                        if ("true".equals(nillableValue) ||
                                "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "renderingZip" +
                                "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setRenderingZip(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                            new javax.xml.namespace.QName("",
                                "priorAuthProviderTypeCode").equals(
                                reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                "nil");

                        if ("true".equals(nillableValue) ||
                                "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "priorAuthProviderTypeCode" +
                                "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setPriorAuthProviderTypeCode(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                            new javax.xml.namespace.QName("",
                                "priorAuthHeaderKey").equals(reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                "nil");

                        if ("true".equals(nillableValue) ||
                                "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "priorAuthHeaderKey" +
                                "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setPriorAuthHeaderKey(org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(
                                content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                        // A start element we are not expecting indicates an invalid parameter was passed
                        throw new org.apache.axis2.databinding.ADBException(
                            "Unexpected subelement " + reader.getName());
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                            new javax.xml.namespace.QName("",
                                "appCreatedDateTime").equals(reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                "nil");

                        if ("true".equals(nillableValue) ||
                                "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "appCreatedDateTime" +
                                "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setAppCreatedDateTime(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                            new javax.xml.namespace.QName("", "appCreatedUserId").equals(
                                reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                "nil");

                        if ("true".equals(nillableValue) ||
                                "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "appCreatedUserId" +
                                "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setAppCreatedUserId(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                            new javax.xml.namespace.QName("", "appMantDateTime").equals(
                                reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                "nil");

                        if ("true".equals(nillableValue) ||
                                "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "appMantDateTime" +
                                "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setAppMantDateTime(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                            new javax.xml.namespace.QName("", "appMantUserId").equals(
                                reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                "nil");

                        if ("true".equals(nillableValue) ||
                                "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "appMantUserId" +
                                "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setAppMantUserId(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()) {
                        // A start element we are not expecting indicates a trailing invalid property
                        throw new org.apache.axis2.databinding.ADBException(
                            "Unexpected subelement " + reader.getName());
                    }
                } catch (javax.xml.stream.XMLStreamException e) {
                    throw new java.lang.Exception(e);
                }

                return object;
            }
        } //end of factory class
    }

    public static class SavePriorAuthHeaderE implements org.apache.axis2.databinding.ADBBean {
        public static final javax.xml.namespace.QName MY_QNAME = new javax.xml.namespace.QName("http://services.lbm.avalon.com/",
                "savePriorAuthHeader", "ns1");

        /**
         * field for SavePriorAuthHeader
         */
        protected SavePriorAuthHeader localSavePriorAuthHeader;

        /**
         * Auto generated getter method
         * @return SavePriorAuthHeader
         */
        public SavePriorAuthHeader getSavePriorAuthHeader() {
            return localSavePriorAuthHeader;
        }

        /**
         * Auto generated setter method
         * @param param SavePriorAuthHeader
         */
        public void setSavePriorAuthHeader(SavePriorAuthHeader param) {
            this.localSavePriorAuthHeader = param;
        }

        /**
         *
         * @param parentQName
         * @param factory
         * @return org.apache.axiom.om.OMElement
         */
        public org.apache.axiom.om.OMElement getOMElement(
            final javax.xml.namespace.QName parentQName,
            final org.apache.axiom.om.OMFactory factory)
            throws org.apache.axis2.databinding.ADBException {
            org.apache.axiom.om.OMDataSource dataSource = new org.apache.axis2.databinding.ADBDataSource(this,
                    MY_QNAME);

            return factory.createOMElement(dataSource, MY_QNAME);
        }

        public void serialize(final javax.xml.namespace.QName parentQName,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException,
                org.apache.axis2.databinding.ADBException {
            serialize(parentQName, xmlWriter, false);
        }

        public void serialize(final javax.xml.namespace.QName parentQName,
            javax.xml.stream.XMLStreamWriter xmlWriter, boolean serializeType)
            throws javax.xml.stream.XMLStreamException,
                org.apache.axis2.databinding.ADBException {
            //We can safely assume an element has only one type associated with it
            if (localSavePriorAuthHeader == null) {
                throw new org.apache.axis2.databinding.ADBException(
                    "savePriorAuthHeader cannot be null!");
            }

            localSavePriorAuthHeader.serialize(MY_QNAME, xmlWriter);
        }

        private static java.lang.String generatePrefix(
            java.lang.String namespace) {
            if (namespace.equals("http://services.lbm.avalon.com/")) {
                return "ns1";
            }

            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix,
            java.lang.String namespace, java.lang.String localPart,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

            if (writerPrefix != null) {
                xmlWriter.writeStartElement(namespace, localPart);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }

        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,
            java.lang.String namespace, java.lang.String attName,
            java.lang.String attValue,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }

            xmlWriter.writeAttribute(namespace, attName, attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,
            java.lang.String attName, java.lang.String attValue,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attValue);
            }
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeQNameAttribute(java.lang.String namespace,
            java.lang.String attName, javax.xml.namespace.QName qname,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String attributeNamespace = qname.getNamespaceURI();
            java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);

            if (attributePrefix == null) {
                attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
            }

            java.lang.String attributeValue;

            if (attributePrefix.trim().length() > 0) {
                attributeValue = attributePrefix + ":" + qname.getLocalPart();
            } else {
                attributeValue = qname.getLocalPart();
            }

            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attributeValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attributeValue);
            }
        }

        /**
         *  method to handle Qnames
         */
        private void writeQName(javax.xml.namespace.QName qname,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();

            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);

                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix, namespaceURI);
                }

                if (prefix.trim().length() > 0) {
                    xmlWriter.writeCharacters(prefix + ":" +
                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            qname));
                }
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }

                    namespaceURI = qnames[i].getNamespaceURI();

                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);

                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix, namespaceURI);
                        }

                        if (prefix.trim().length() > 0) {
                            stringToWrite.append(prefix).append(":")
                                         .append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                    qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                    qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                qnames[i]));
                    }
                }

                xmlWriter.writeCharacters(stringToWrite.toString());
            }
        }

        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(
            javax.xml.stream.XMLStreamWriter xmlWriter,
            java.lang.String namespace)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);

            if (prefix == null) {
                prefix = generatePrefix(namespace);

                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();

                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);

                    if ((uri == null) || (uri.length() == 0)) {
                        break;
                    }

                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }

                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }

            return prefix;
        }

        /**
         * databinding method to get an XML representation of this object
         *
         */
        public javax.xml.stream.XMLStreamReader getPullParser(
            javax.xml.namespace.QName qName)
            throws org.apache.axis2.databinding.ADBException {
            //We can safely assume an element has only one type associated with it
            return localSavePriorAuthHeader.getPullParser(MY_QNAME);
        }

        /**
         *  Factory class that keeps the parse method
         */
        public static class Factory {
            /**
             * static method to create the object
             * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
             *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
             * Postcondition: If this object is an element, the reader is positioned at its end element
             *                If this object is a complex type, the reader is positioned at the end element of its outer element
             */
            public static SavePriorAuthHeaderE parse(
                javax.xml.stream.XMLStreamReader reader)
                throws java.lang.Exception {
                SavePriorAuthHeaderE object = new SavePriorAuthHeaderE();

                int event;
                java.lang.String nillableValue = null;
                java.lang.String prefix = "";
                java.lang.String namespaceuri = "";

                try {
                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    // Note all attributes that were handled. Used to differ normal attributes
                    // from anyAttributes.
                    java.util.Vector handledAttributes = new java.util.Vector();

                    while (!reader.isEndElement()) {
                        if (reader.isStartElement()) {
                            if (reader.isStartElement() &&
                                    new javax.xml.namespace.QName(
                                        "http://services.lbm.avalon.com/",
                                        "savePriorAuthHeader").equals(
                                        reader.getName())) {
                                object.setSavePriorAuthHeader(SavePriorAuthHeader.Factory.parse(
                                        reader));
                            } // End of if for expected property start element

                            else {
                                // A start element we are not expecting indicates an invalid parameter was passed
                                throw new org.apache.axis2.databinding.ADBException(
                                    "Unexpected subelement " +
                                    reader.getName());
                            }
                        } else {
                            reader.next();
                        }
                    } // end of while loop
                } catch (javax.xml.stream.XMLStreamException e) {
                    throw new java.lang.Exception(e);
                }

                return object;
            }
        } //end of factory class
    }

    public static class ExtensionMapper {
        public static java.lang.Object getTypeObject(
            java.lang.String namespaceURI, java.lang.String typeName,
            javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception {
            if ("http://services.lbm.avalon.com/".equals(namespaceURI) &&
                    "getPriorAuthHeaderResponse".equals(typeName)) {
                return GetPriorAuthHeaderResponse.Factory.parse(reader);
            }

            if ("http://services.lbm.avalon.com/".equals(namespaceURI) &&
                    "priorAuthorizationSearchResponse".equals(typeName)) {
                return PriorAuthorizationSearchResponse.Factory.parse(reader);
            }

            if ("http://services.lbm.avalon.com/".equals(namespaceURI) &&
                    "diagnosisDTO".equals(typeName)) {
                return DiagnosisDTO.Factory.parse(reader);
            }

            if ("http://services.lbm.avalon.com/".equals(namespaceURI) &&
                    "savePriorAuthHeader".equals(typeName)) {
                return SavePriorAuthHeader.Factory.parse(reader);
            }

            if ("http://services.lbm.avalon.com/".equals(namespaceURI) &&
                    "DAOException".equals(typeName)) {
                return DAOException.Factory.parse(reader);
            }

            if ("http://services.lbm.avalon.com/".equals(namespaceURI) &&
                    "providerDTO".equals(typeName)) {
                return ProviderDTO.Factory.parse(reader);
            }

            if ("http://services.lbm.avalon.com/".equals(namespaceURI) &&
                    "priorAuthorizationSearch".equals(typeName)) {
                return PriorAuthorizationSearch.Factory.parse(reader);
            }

            if ("http://services.lbm.avalon.com/".equals(namespaceURI) &&
                    "savePriorAuthHeaderResponse".equals(typeName)) {
                return SavePriorAuthHeaderResponse.Factory.parse(reader);
            }

            if ("http://services.lbm.avalon.com/".equals(namespaceURI) &&
                    "getPriorAuthHeader".equals(typeName)) {
                return GetPriorAuthHeader.Factory.parse(reader);
            }

            if ("http://services.lbm.avalon.com/".equals(namespaceURI) &&
                    "procedureDTO".equals(typeName)) {
                return ProcedureDTO.Factory.parse(reader);
            }

            if ("http://services.lbm.avalon.com/".equals(namespaceURI) &&
                    "PriorAuthHeader".equals(typeName)) {
                return PriorAuthHeader.Factory.parse(reader);
            }

            if ("http://services.lbm.avalon.com/".equals(namespaceURI) &&
                    "priorAuthHeaderDTO".equals(typeName)) {
                return PriorAuthHeaderDTO.Factory.parse(reader);
            }

            throw new org.apache.axis2.databinding.ADBException(
                "Unsupported type " + namespaceURI + " " + typeName);
        }
    }

    public static class GetPriorAuthHeader implements org.apache.axis2.databinding.ADBBean {
        /* This type was generated from the piece of schema that had
           name = getPriorAuthHeader
           Namespace URI = http://services.lbm.avalon.com/
           Namespace Prefix = ns1
         */

        /**
         * field for SearchCriteriaRequest
         */
        protected PriorAuthHeader localSearchCriteriaRequest;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localSearchCriteriaRequestTracker = false;

        public boolean isSearchCriteriaRequestSpecified() {
            return localSearchCriteriaRequestTracker;
        }

        /**
         * Auto generated getter method
         * @return PriorAuthHeader
         */
        public PriorAuthHeader getSearchCriteriaRequest() {
            return localSearchCriteriaRequest;
        }

        /**
         * Auto generated setter method
         * @param param SearchCriteriaRequest
         */
        public void setSearchCriteriaRequest(PriorAuthHeader param) {
            localSearchCriteriaRequestTracker = param != null;

            this.localSearchCriteriaRequest = param;
        }

        /**
         *
         * @param parentQName
         * @param factory
         * @return org.apache.axiom.om.OMElement
         */
        public org.apache.axiom.om.OMElement getOMElement(
            final javax.xml.namespace.QName parentQName,
            final org.apache.axiom.om.OMFactory factory)
            throws org.apache.axis2.databinding.ADBException {
            org.apache.axiom.om.OMDataSource dataSource = new org.apache.axis2.databinding.ADBDataSource(this,
                    parentQName);

            return factory.createOMElement(dataSource, parentQName);
        }

        public void serialize(final javax.xml.namespace.QName parentQName,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException,
                org.apache.axis2.databinding.ADBException {
            serialize(parentQName, xmlWriter, false);
        }

        public void serialize(final javax.xml.namespace.QName parentQName,
            javax.xml.stream.XMLStreamWriter xmlWriter, boolean serializeType)
            throws javax.xml.stream.XMLStreamException,
                org.apache.axis2.databinding.ADBException {
            java.lang.String prefix = null;
            java.lang.String namespace = null;

            prefix = parentQName.getPrefix();
            namespace = parentQName.getNamespaceURI();
            writeStartElement(prefix, namespace, parentQName.getLocalPart(),
                xmlWriter);

            if (serializeType) {
                java.lang.String namespacePrefix = registerPrefix(xmlWriter,
                        "http://services.lbm.avalon.com/");

                if ((namespacePrefix != null) &&
                        (namespacePrefix.trim().length() > 0)) {
                    writeAttribute("xsi",
                        "http://www.w3.org/2001/XMLSchema-instance", "type",
                        namespacePrefix + ":getPriorAuthHeader", xmlWriter);
                } else {
                    writeAttribute("xsi",
                        "http://www.w3.org/2001/XMLSchema-instance", "type",
                        "getPriorAuthHeader", xmlWriter);
                }
            }

            if (localSearchCriteriaRequestTracker) {
                if (localSearchCriteriaRequest == null) {
                    throw new org.apache.axis2.databinding.ADBException(
                        "searchCriteriaRequest cannot be null!!");
                }

                localSearchCriteriaRequest.serialize(new javax.xml.namespace.QName(
                        "http://services.lbm.avalon.com/",
                        "searchCriteriaRequest"), xmlWriter);
            }

            xmlWriter.writeEndElement();
        }

        private static java.lang.String generatePrefix(
            java.lang.String namespace) {
            if (namespace.equals("http://services.lbm.avalon.com/")) {
                return "ns1";
            }

            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix,
            java.lang.String namespace, java.lang.String localPart,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

            if (writerPrefix != null) {
                xmlWriter.writeStartElement(namespace, localPart);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }

        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,
            java.lang.String namespace, java.lang.String attName,
            java.lang.String attValue,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }

            xmlWriter.writeAttribute(namespace, attName, attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,
            java.lang.String attName, java.lang.String attValue,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attValue);
            }
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeQNameAttribute(java.lang.String namespace,
            java.lang.String attName, javax.xml.namespace.QName qname,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String attributeNamespace = qname.getNamespaceURI();
            java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);

            if (attributePrefix == null) {
                attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
            }

            java.lang.String attributeValue;

            if (attributePrefix.trim().length() > 0) {
                attributeValue = attributePrefix + ":" + qname.getLocalPart();
            } else {
                attributeValue = qname.getLocalPart();
            }

            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attributeValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attributeValue);
            }
        }

        /**
         *  method to handle Qnames
         */
        private void writeQName(javax.xml.namespace.QName qname,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();

            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);

                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix, namespaceURI);
                }

                if (prefix.trim().length() > 0) {
                    xmlWriter.writeCharacters(prefix + ":" +
                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            qname));
                }
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }

                    namespaceURI = qnames[i].getNamespaceURI();

                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);

                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix, namespaceURI);
                        }

                        if (prefix.trim().length() > 0) {
                            stringToWrite.append(prefix).append(":")
                                         .append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                    qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                    qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                qnames[i]));
                    }
                }

                xmlWriter.writeCharacters(stringToWrite.toString());
            }
        }

        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(
            javax.xml.stream.XMLStreamWriter xmlWriter,
            java.lang.String namespace)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);

            if (prefix == null) {
                prefix = generatePrefix(namespace);

                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();

                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);

                    if ((uri == null) || (uri.length() == 0)) {
                        break;
                    }

                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }

                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }

            return prefix;
        }

        /**
         * databinding method to get an XML representation of this object
         *
         */
        public javax.xml.stream.XMLStreamReader getPullParser(
            javax.xml.namespace.QName qName)
            throws org.apache.axis2.databinding.ADBException {
            java.util.ArrayList elementList = new java.util.ArrayList();
            java.util.ArrayList attribList = new java.util.ArrayList();

            if (localSearchCriteriaRequestTracker) {
                elementList.add(new javax.xml.namespace.QName(
                        "http://services.lbm.avalon.com/",
                        "searchCriteriaRequest"));

                if (localSearchCriteriaRequest == null) {
                    throw new org.apache.axis2.databinding.ADBException(
                        "searchCriteriaRequest cannot be null!!");
                }

                elementList.add(localSearchCriteriaRequest);
            }

            return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName,
                elementList.toArray(), attribList.toArray());
        }

        /**
         *  Factory class that keeps the parse method
         */
        public static class Factory {
            /**
             * static method to create the object
             * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
             *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
             * Postcondition: If this object is an element, the reader is positioned at its end element
             *                If this object is a complex type, the reader is positioned at the end element of its outer element
             */
            public static GetPriorAuthHeader parse(
                javax.xml.stream.XMLStreamReader reader)
                throws java.lang.Exception {
                GetPriorAuthHeader object = new GetPriorAuthHeader();

                int event;
                java.lang.String nillableValue = null;
                java.lang.String prefix = "";
                java.lang.String namespaceuri = "";

                try {
                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.getAttributeValue(
                                "http://www.w3.org/2001/XMLSchema-instance",
                                "type") != null) {
                        java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                "type");

                        if (fullTypeName != null) {
                            java.lang.String nsPrefix = null;

                            if (fullTypeName.indexOf(":") > -1) {
                                nsPrefix = fullTypeName.substring(0,
                                        fullTypeName.indexOf(":"));
                            }

                            nsPrefix = (nsPrefix == null) ? "" : nsPrefix;

                            java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(
                                        ":") + 1);

                            if (!"getPriorAuthHeader".equals(type)) {
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext()
                                                               .getNamespaceURI(nsPrefix);

                                return (GetPriorAuthHeader) ExtensionMapper.getTypeObject(nsUri,
                                    type, reader);
                            }
                        }
                    }

                    // Note all attributes that were handled. Used to differ normal attributes
                    // from anyAttributes.
                    java.util.Vector handledAttributes = new java.util.Vector();

                    reader.next();

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                            new javax.xml.namespace.QName(
                                "http://services.lbm.avalon.com/",
                                "searchCriteriaRequest").equals(
                                reader.getName())) {
                        object.setSearchCriteriaRequest(PriorAuthHeader.Factory.parse(
                                reader));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()) {
                        // A start element we are not expecting indicates a trailing invalid property
                        throw new org.apache.axis2.databinding.ADBException(
                            "Unexpected subelement " + reader.getName());
                    }
                } catch (javax.xml.stream.XMLStreamException e) {
                    throw new java.lang.Exception(e);
                }

                return object;
            }
        } //end of factory class
    }

    public static class DAOException implements org.apache.axis2.databinding.ADBBean {
        /* This type was generated from the piece of schema that had
           name = DAOException
           Namespace URI = http://services.lbm.avalon.com/
           Namespace Prefix = ns1
         */

        /**
         * field for Message
         */
        protected java.lang.String localMessage;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localMessageTracker = false;

        public boolean isMessageSpecified() {
            return localMessageTracker;
        }

        /**
         * Auto generated getter method
         * @return java.lang.String
         */
        public java.lang.String getMessage() {
            return localMessage;
        }

        /**
         * Auto generated setter method
         * @param param Message
         */
        public void setMessage(java.lang.String param) {
            localMessageTracker = param != null;

            this.localMessage = param;
        }

        /**
         *
         * @param parentQName
         * @param factory
         * @return org.apache.axiom.om.OMElement
         */
        public org.apache.axiom.om.OMElement getOMElement(
            final javax.xml.namespace.QName parentQName,
            final org.apache.axiom.om.OMFactory factory)
            throws org.apache.axis2.databinding.ADBException {
            org.apache.axiom.om.OMDataSource dataSource = new org.apache.axis2.databinding.ADBDataSource(this,
                    parentQName);

            return factory.createOMElement(dataSource, parentQName);
        }

        public void serialize(final javax.xml.namespace.QName parentQName,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException,
                org.apache.axis2.databinding.ADBException {
            serialize(parentQName, xmlWriter, false);
        }

        public void serialize(final javax.xml.namespace.QName parentQName,
            javax.xml.stream.XMLStreamWriter xmlWriter, boolean serializeType)
            throws javax.xml.stream.XMLStreamException,
                org.apache.axis2.databinding.ADBException {
            java.lang.String prefix = null;
            java.lang.String namespace = null;

            prefix = parentQName.getPrefix();
            namespace = parentQName.getNamespaceURI();
            writeStartElement(prefix, namespace, parentQName.getLocalPart(),
                xmlWriter);

            if (serializeType) {
                java.lang.String namespacePrefix = registerPrefix(xmlWriter,
                        "http://services.lbm.avalon.com/");

                if ((namespacePrefix != null) &&
                        (namespacePrefix.trim().length() > 0)) {
                    writeAttribute("xsi",
                        "http://www.w3.org/2001/XMLSchema-instance", "type",
                        namespacePrefix + ":DAOException", xmlWriter);
                } else {
                    writeAttribute("xsi",
                        "http://www.w3.org/2001/XMLSchema-instance", "type",
                        "DAOException", xmlWriter);
                }
            }

            if (localMessageTracker) {
                namespace = "";
                writeStartElement(null, namespace, "message", xmlWriter);

                if (localMessage == null) {
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException(
                        "message cannot be null!!");
                } else {
                    xmlWriter.writeCharacters(localMessage);
                }

                xmlWriter.writeEndElement();
            }

            xmlWriter.writeEndElement();
        }

        private static java.lang.String generatePrefix(
            java.lang.String namespace) {
            if (namespace.equals("http://services.lbm.avalon.com/")) {
                return "ns1";
            }

            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix,
            java.lang.String namespace, java.lang.String localPart,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

            if (writerPrefix != null) {
                xmlWriter.writeStartElement(namespace, localPart);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }

        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,
            java.lang.String namespace, java.lang.String attName,
            java.lang.String attValue,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }

            xmlWriter.writeAttribute(namespace, attName, attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,
            java.lang.String attName, java.lang.String attValue,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attValue);
            }
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeQNameAttribute(java.lang.String namespace,
            java.lang.String attName, javax.xml.namespace.QName qname,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String attributeNamespace = qname.getNamespaceURI();
            java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);

            if (attributePrefix == null) {
                attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
            }

            java.lang.String attributeValue;

            if (attributePrefix.trim().length() > 0) {
                attributeValue = attributePrefix + ":" + qname.getLocalPart();
            } else {
                attributeValue = qname.getLocalPart();
            }

            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attributeValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attributeValue);
            }
        }

        /**
         *  method to handle Qnames
         */
        private void writeQName(javax.xml.namespace.QName qname,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();

            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);

                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix, namespaceURI);
                }

                if (prefix.trim().length() > 0) {
                    xmlWriter.writeCharacters(prefix + ":" +
                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            qname));
                }
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }

                    namespaceURI = qnames[i].getNamespaceURI();

                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);

                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix, namespaceURI);
                        }

                        if (prefix.trim().length() > 0) {
                            stringToWrite.append(prefix).append(":")
                                         .append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                    qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                    qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                qnames[i]));
                    }
                }

                xmlWriter.writeCharacters(stringToWrite.toString());
            }
        }

        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(
            javax.xml.stream.XMLStreamWriter xmlWriter,
            java.lang.String namespace)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);

            if (prefix == null) {
                prefix = generatePrefix(namespace);

                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();

                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);

                    if ((uri == null) || (uri.length() == 0)) {
                        break;
                    }

                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }

                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }

            return prefix;
        }

        /**
         * databinding method to get an XML representation of this object
         *
         */
        public javax.xml.stream.XMLStreamReader getPullParser(
            javax.xml.namespace.QName qName)
            throws org.apache.axis2.databinding.ADBException {
            java.util.ArrayList elementList = new java.util.ArrayList();
            java.util.ArrayList attribList = new java.util.ArrayList();

            if (localMessageTracker) {
                elementList.add(new javax.xml.namespace.QName("", "message"));

                if (localMessage != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            localMessage));
                } else {
                    throw new org.apache.axis2.databinding.ADBException(
                        "message cannot be null!!");
                }
            }

            return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName,
                elementList.toArray(), attribList.toArray());
        }

        /**
         *  Factory class that keeps the parse method
         */
        public static class Factory {
            /**
             * static method to create the object
             * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
             *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
             * Postcondition: If this object is an element, the reader is positioned at its end element
             *                If this object is a complex type, the reader is positioned at the end element of its outer element
             */
            public static DAOException parse(
                javax.xml.stream.XMLStreamReader reader)
                throws java.lang.Exception {
                DAOException object = new DAOException();

                int event;
                java.lang.String nillableValue = null;
                java.lang.String prefix = "";
                java.lang.String namespaceuri = "";

                try {
                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.getAttributeValue(
                                "http://www.w3.org/2001/XMLSchema-instance",
                                "type") != null) {
                        java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                "type");

                        if (fullTypeName != null) {
                            java.lang.String nsPrefix = null;

                            if (fullTypeName.indexOf(":") > -1) {
                                nsPrefix = fullTypeName.substring(0,
                                        fullTypeName.indexOf(":"));
                            }

                            nsPrefix = (nsPrefix == null) ? "" : nsPrefix;

                            java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(
                                        ":") + 1);

                            if (!"DAOException".equals(type)) {
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext()
                                                               .getNamespaceURI(nsPrefix);

                                return (DAOException) ExtensionMapper.getTypeObject(nsUri,
                                    type, reader);
                            }
                        }
                    }

                    // Note all attributes that were handled. Used to differ normal attributes
                    // from anyAttributes.
                    java.util.Vector handledAttributes = new java.util.Vector();

                    reader.next();

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                            new javax.xml.namespace.QName("", "message").equals(
                                reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                "nil");

                        if ("true".equals(nillableValue) ||
                                "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "message" +
                                "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setMessage(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()) {
                        // A start element we are not expecting indicates a trailing invalid property
                        throw new org.apache.axis2.databinding.ADBException(
                            "Unexpected subelement " + reader.getName());
                    }
                } catch (javax.xml.stream.XMLStreamException e) {
                    throw new java.lang.Exception(e);
                }

                return object;
            }
        } //end of factory class
    }

    public static class ProviderDTOE implements org.apache.axis2.databinding.ADBBean {
        public static final javax.xml.namespace.QName MY_QNAME = new javax.xml.namespace.QName("http://services.lbm.avalon.com/",
                "providerDTO", "ns1");

        /**
         * field for ProviderDTO
         */
        protected ProviderDTO localProviderDTO;

        /**
         * Auto generated getter method
         * @return ProviderDTO
         */
        public ProviderDTO getProviderDTO() {
            return localProviderDTO;
        }

        /**
         * Auto generated setter method
         * @param param ProviderDTO
         */
        public void setProviderDTO(ProviderDTO param) {
            this.localProviderDTO = param;
        }

        /**
         *
         * @param parentQName
         * @param factory
         * @return org.apache.axiom.om.OMElement
         */
        public org.apache.axiom.om.OMElement getOMElement(
            final javax.xml.namespace.QName parentQName,
            final org.apache.axiom.om.OMFactory factory)
            throws org.apache.axis2.databinding.ADBException {
            org.apache.axiom.om.OMDataSource dataSource = new org.apache.axis2.databinding.ADBDataSource(this,
                    MY_QNAME);

            return factory.createOMElement(dataSource, MY_QNAME);
        }

        public void serialize(final javax.xml.namespace.QName parentQName,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException,
                org.apache.axis2.databinding.ADBException {
            serialize(parentQName, xmlWriter, false);
        }

        public void serialize(final javax.xml.namespace.QName parentQName,
            javax.xml.stream.XMLStreamWriter xmlWriter, boolean serializeType)
            throws javax.xml.stream.XMLStreamException,
                org.apache.axis2.databinding.ADBException {
            //We can safely assume an element has only one type associated with it
            if (localProviderDTO == null) {
                throw new org.apache.axis2.databinding.ADBException(
                    "providerDTO cannot be null!");
            }

            localProviderDTO.serialize(MY_QNAME, xmlWriter);
        }

        private static java.lang.String generatePrefix(
            java.lang.String namespace) {
            if (namespace.equals("http://services.lbm.avalon.com/")) {
                return "ns1";
            }

            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix,
            java.lang.String namespace, java.lang.String localPart,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

            if (writerPrefix != null) {
                xmlWriter.writeStartElement(namespace, localPart);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }

        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,
            java.lang.String namespace, java.lang.String attName,
            java.lang.String attValue,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }

            xmlWriter.writeAttribute(namespace, attName, attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,
            java.lang.String attName, java.lang.String attValue,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attValue);
            }
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeQNameAttribute(java.lang.String namespace,
            java.lang.String attName, javax.xml.namespace.QName qname,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String attributeNamespace = qname.getNamespaceURI();
            java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);

            if (attributePrefix == null) {
                attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
            }

            java.lang.String attributeValue;

            if (attributePrefix.trim().length() > 0) {
                attributeValue = attributePrefix + ":" + qname.getLocalPart();
            } else {
                attributeValue = qname.getLocalPart();
            }

            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attributeValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attributeValue);
            }
        }

        /**
         *  method to handle Qnames
         */
        private void writeQName(javax.xml.namespace.QName qname,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();

            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);

                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix, namespaceURI);
                }

                if (prefix.trim().length() > 0) {
                    xmlWriter.writeCharacters(prefix + ":" +
                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            qname));
                }
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }

                    namespaceURI = qnames[i].getNamespaceURI();

                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);

                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix, namespaceURI);
                        }

                        if (prefix.trim().length() > 0) {
                            stringToWrite.append(prefix).append(":")
                                         .append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                    qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                    qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                qnames[i]));
                    }
                }

                xmlWriter.writeCharacters(stringToWrite.toString());
            }
        }

        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(
            javax.xml.stream.XMLStreamWriter xmlWriter,
            java.lang.String namespace)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);

            if (prefix == null) {
                prefix = generatePrefix(namespace);

                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();

                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);

                    if ((uri == null) || (uri.length() == 0)) {
                        break;
                    }

                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }

                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }

            return prefix;
        }

        /**
         * databinding method to get an XML representation of this object
         *
         */
        public javax.xml.stream.XMLStreamReader getPullParser(
            javax.xml.namespace.QName qName)
            throws org.apache.axis2.databinding.ADBException {
            //We can safely assume an element has only one type associated with it
            return localProviderDTO.getPullParser(MY_QNAME);
        }

        /**
         *  Factory class that keeps the parse method
         */
        public static class Factory {
            /**
             * static method to create the object
             * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
             *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
             * Postcondition: If this object is an element, the reader is positioned at its end element
             *                If this object is a complex type, the reader is positioned at the end element of its outer element
             */
            public static ProviderDTOE parse(
                javax.xml.stream.XMLStreamReader reader)
                throws java.lang.Exception {
                ProviderDTOE object = new ProviderDTOE();

                int event;
                java.lang.String nillableValue = null;
                java.lang.String prefix = "";
                java.lang.String namespaceuri = "";

                try {
                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    // Note all attributes that were handled. Used to differ normal attributes
                    // from anyAttributes.
                    java.util.Vector handledAttributes = new java.util.Vector();

                    while (!reader.isEndElement()) {
                        if (reader.isStartElement()) {
                            if (reader.isStartElement() &&
                                    new javax.xml.namespace.QName(
                                        "http://services.lbm.avalon.com/",
                                        "providerDTO").equals(reader.getName())) {
                                object.setProviderDTO(ProviderDTO.Factory.parse(
                                        reader));
                            } // End of if for expected property start element

                            else {
                                // A start element we are not expecting indicates an invalid parameter was passed
                                throw new org.apache.axis2.databinding.ADBException(
                                    "Unexpected subelement " +
                                    reader.getName());
                            }
                        } else {
                            reader.next();
                        }
                    } // end of while loop
                } catch (javax.xml.stream.XMLStreamException e) {
                    throw new java.lang.Exception(e);
                }

                return object;
            }
        } //end of factory class
    }

    public static class DAOExceptionE implements org.apache.axis2.databinding.ADBBean {
        public static final javax.xml.namespace.QName MY_QNAME = new javax.xml.namespace.QName("http://services.lbm.avalon.com/",
                "DAOException", "ns1");

        /**
         * field for DAOException
         */
        protected DAOException localDAOException;

        /**
         * Auto generated getter method
         * @return DAOException
         */
        public DAOException getDAOException() {
            return localDAOException;
        }

        /**
         * Auto generated setter method
         * @param param DAOException
         */
        public void setDAOException(DAOException param) {
            this.localDAOException = param;
        }

        /**
         *
         * @param parentQName
         * @param factory
         * @return org.apache.axiom.om.OMElement
         */
        public org.apache.axiom.om.OMElement getOMElement(
            final javax.xml.namespace.QName parentQName,
            final org.apache.axiom.om.OMFactory factory)
            throws org.apache.axis2.databinding.ADBException {
            org.apache.axiom.om.OMDataSource dataSource = new org.apache.axis2.databinding.ADBDataSource(this,
                    MY_QNAME);

            return factory.createOMElement(dataSource, MY_QNAME);
        }

        public void serialize(final javax.xml.namespace.QName parentQName,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException,
                org.apache.axis2.databinding.ADBException {
            serialize(parentQName, xmlWriter, false);
        }

        public void serialize(final javax.xml.namespace.QName parentQName,
            javax.xml.stream.XMLStreamWriter xmlWriter, boolean serializeType)
            throws javax.xml.stream.XMLStreamException,
                org.apache.axis2.databinding.ADBException {
            //We can safely assume an element has only one type associated with it
            if (localDAOException == null) {
                throw new org.apache.axis2.databinding.ADBException(
                    "DAOException cannot be null!");
            }

            localDAOException.serialize(MY_QNAME, xmlWriter);
        }

        private static java.lang.String generatePrefix(
            java.lang.String namespace) {
            if (namespace.equals("http://services.lbm.avalon.com/")) {
                return "ns1";
            }

            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix,
            java.lang.String namespace, java.lang.String localPart,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

            if (writerPrefix != null) {
                xmlWriter.writeStartElement(namespace, localPart);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }

        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,
            java.lang.String namespace, java.lang.String attName,
            java.lang.String attValue,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }

            xmlWriter.writeAttribute(namespace, attName, attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,
            java.lang.String attName, java.lang.String attValue,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attValue);
            }
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeQNameAttribute(java.lang.String namespace,
            java.lang.String attName, javax.xml.namespace.QName qname,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String attributeNamespace = qname.getNamespaceURI();
            java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);

            if (attributePrefix == null) {
                attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
            }

            java.lang.String attributeValue;

            if (attributePrefix.trim().length() > 0) {
                attributeValue = attributePrefix + ":" + qname.getLocalPart();
            } else {
                attributeValue = qname.getLocalPart();
            }

            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attributeValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attributeValue);
            }
        }

        /**
         *  method to handle Qnames
         */
        private void writeQName(javax.xml.namespace.QName qname,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();

            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);

                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix, namespaceURI);
                }

                if (prefix.trim().length() > 0) {
                    xmlWriter.writeCharacters(prefix + ":" +
                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            qname));
                }
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }

                    namespaceURI = qnames[i].getNamespaceURI();

                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);

                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix, namespaceURI);
                        }

                        if (prefix.trim().length() > 0) {
                            stringToWrite.append(prefix).append(":")
                                         .append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                    qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                    qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                qnames[i]));
                    }
                }

                xmlWriter.writeCharacters(stringToWrite.toString());
            }
        }

        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(
            javax.xml.stream.XMLStreamWriter xmlWriter,
            java.lang.String namespace)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);

            if (prefix == null) {
                prefix = generatePrefix(namespace);

                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();

                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);

                    if ((uri == null) || (uri.length() == 0)) {
                        break;
                    }

                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }

                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }

            return prefix;
        }

        /**
         * databinding method to get an XML representation of this object
         *
         */
        public javax.xml.stream.XMLStreamReader getPullParser(
            javax.xml.namespace.QName qName)
            throws org.apache.axis2.databinding.ADBException {
            //We can safely assume an element has only one type associated with it
            return localDAOException.getPullParser(MY_QNAME);
        }

        /**
         *  Factory class that keeps the parse method
         */
        public static class Factory {
            /**
             * static method to create the object
             * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
             *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
             * Postcondition: If this object is an element, the reader is positioned at its end element
             *                If this object is a complex type, the reader is positioned at the end element of its outer element
             */
            public static DAOExceptionE parse(
                javax.xml.stream.XMLStreamReader reader)
                throws java.lang.Exception {
                DAOExceptionE object = new DAOExceptionE();

                int event;
                java.lang.String nillableValue = null;
                java.lang.String prefix = "";
                java.lang.String namespaceuri = "";

                try {
                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    // Note all attributes that were handled. Used to differ normal attributes
                    // from anyAttributes.
                    java.util.Vector handledAttributes = new java.util.Vector();

                    while (!reader.isEndElement()) {
                        if (reader.isStartElement()) {
                            if (reader.isStartElement() &&
                                    new javax.xml.namespace.QName(
                                        "http://services.lbm.avalon.com/",
                                        "DAOException").equals(reader.getName())) {
                                object.setDAOException(DAOException.Factory.parse(
                                        reader));
                            } // End of if for expected property start element

                            else {
                                // A start element we are not expecting indicates an invalid parameter was passed
                                throw new org.apache.axis2.databinding.ADBException(
                                    "Unexpected subelement " +
                                    reader.getName());
                            }
                        } else {
                            reader.next();
                        }
                    } // end of while loop
                } catch (javax.xml.stream.XMLStreamException e) {
                    throw new java.lang.Exception(e);
                }

                return object;
            }
        } //end of factory class
    }

    public static class PriorAuthorizationSearchResponse implements org.apache.axis2.databinding.ADBBean {
        /* This type was generated from the piece of schema that had
           name = priorAuthorizationSearchResponse
           Namespace URI = http://services.lbm.avalon.com/
           Namespace Prefix = ns1
         */

        /**
         * field for _return
         */
        protected PriorAuthHeader local_return;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean local_returnTracker = false;

        public boolean is_returnSpecified() {
            return local_returnTracker;
        }

        /**
         * Auto generated getter method
         * @return PriorAuthHeader
         */
        public PriorAuthHeader get_return() {
            return local_return;
        }

        /**
         * Auto generated setter method
         * @param param _return
         */
        public void set_return(PriorAuthHeader param) {
            local_returnTracker = param != null;

            this.local_return = param;
        }

        /**
         *
         * @param parentQName
         * @param factory
         * @return org.apache.axiom.om.OMElement
         */
        public org.apache.axiom.om.OMElement getOMElement(
            final javax.xml.namespace.QName parentQName,
            final org.apache.axiom.om.OMFactory factory)
            throws org.apache.axis2.databinding.ADBException {
            org.apache.axiom.om.OMDataSource dataSource = new org.apache.axis2.databinding.ADBDataSource(this,
                    parentQName);

            return factory.createOMElement(dataSource, parentQName);
        }

        public void serialize(final javax.xml.namespace.QName parentQName,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException,
                org.apache.axis2.databinding.ADBException {
            serialize(parentQName, xmlWriter, false);
        }

        public void serialize(final javax.xml.namespace.QName parentQName,
            javax.xml.stream.XMLStreamWriter xmlWriter, boolean serializeType)
            throws javax.xml.stream.XMLStreamException,
                org.apache.axis2.databinding.ADBException {
            java.lang.String prefix = null;
            java.lang.String namespace = null;

            prefix = parentQName.getPrefix();
            namespace = parentQName.getNamespaceURI();
            writeStartElement(prefix, namespace, parentQName.getLocalPart(),
                xmlWriter);

            if (serializeType) {
                java.lang.String namespacePrefix = registerPrefix(xmlWriter,
                        "http://services.lbm.avalon.com/");

                if ((namespacePrefix != null) &&
                        (namespacePrefix.trim().length() > 0)) {
                    writeAttribute("xsi",
                        "http://www.w3.org/2001/XMLSchema-instance", "type",
                        namespacePrefix + ":priorAuthorizationSearchResponse",
                        xmlWriter);
                } else {
                    writeAttribute("xsi",
                        "http://www.w3.org/2001/XMLSchema-instance", "type",
                        "priorAuthorizationSearchResponse", xmlWriter);
                }
            }

            if (local_returnTracker) {
                if (local_return == null) {
                    throw new org.apache.axis2.databinding.ADBException(
                        "return cannot be null!!");
                }

                local_return.serialize(new javax.xml.namespace.QName("",
                        "return"), xmlWriter);
            }

            xmlWriter.writeEndElement();
        }

        private static java.lang.String generatePrefix(
            java.lang.String namespace) {
            if (namespace.equals("http://services.lbm.avalon.com/")) {
                return "ns1";
            }

            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix,
            java.lang.String namespace, java.lang.String localPart,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

            if (writerPrefix != null) {
                xmlWriter.writeStartElement(namespace, localPart);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }

        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,
            java.lang.String namespace, java.lang.String attName,
            java.lang.String attValue,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }

            xmlWriter.writeAttribute(namespace, attName, attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,
            java.lang.String attName, java.lang.String attValue,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attValue);
            }
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeQNameAttribute(java.lang.String namespace,
            java.lang.String attName, javax.xml.namespace.QName qname,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String attributeNamespace = qname.getNamespaceURI();
            java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);

            if (attributePrefix == null) {
                attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
            }

            java.lang.String attributeValue;

            if (attributePrefix.trim().length() > 0) {
                attributeValue = attributePrefix + ":" + qname.getLocalPart();
            } else {
                attributeValue = qname.getLocalPart();
            }

            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attributeValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attributeValue);
            }
        }

        /**
         *  method to handle Qnames
         */
        private void writeQName(javax.xml.namespace.QName qname,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();

            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);

                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix, namespaceURI);
                }

                if (prefix.trim().length() > 0) {
                    xmlWriter.writeCharacters(prefix + ":" +
                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            qname));
                }
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }

                    namespaceURI = qnames[i].getNamespaceURI();

                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);

                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix, namespaceURI);
                        }

                        if (prefix.trim().length() > 0) {
                            stringToWrite.append(prefix).append(":")
                                         .append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                    qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                    qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                qnames[i]));
                    }
                }

                xmlWriter.writeCharacters(stringToWrite.toString());
            }
        }

        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(
            javax.xml.stream.XMLStreamWriter xmlWriter,
            java.lang.String namespace)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);

            if (prefix == null) {
                prefix = generatePrefix(namespace);

                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();

                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);

                    if ((uri == null) || (uri.length() == 0)) {
                        break;
                    }

                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }

                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }

            return prefix;
        }

        /**
         * databinding method to get an XML representation of this object
         *
         */
        public javax.xml.stream.XMLStreamReader getPullParser(
            javax.xml.namespace.QName qName)
            throws org.apache.axis2.databinding.ADBException {
            java.util.ArrayList elementList = new java.util.ArrayList();
            java.util.ArrayList attribList = new java.util.ArrayList();

            if (local_returnTracker) {
                elementList.add(new javax.xml.namespace.QName("", "return"));

                if (local_return == null) {
                    throw new org.apache.axis2.databinding.ADBException(
                        "return cannot be null!!");
                }

                elementList.add(local_return);
            }

            return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName,
                elementList.toArray(), attribList.toArray());
        }

        /**
         *  Factory class that keeps the parse method
         */
        public static class Factory {
            /**
             * static method to create the object
             * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
             *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
             * Postcondition: If this object is an element, the reader is positioned at its end element
             *                If this object is a complex type, the reader is positioned at the end element of its outer element
             */
            public static PriorAuthorizationSearchResponse parse(
                javax.xml.stream.XMLStreamReader reader)
                throws java.lang.Exception {
                PriorAuthorizationSearchResponse object = new PriorAuthorizationSearchResponse();

                int event;
                java.lang.String nillableValue = null;
                java.lang.String prefix = "";
                java.lang.String namespaceuri = "";

                try {
                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.getAttributeValue(
                                "http://www.w3.org/2001/XMLSchema-instance",
                                "type") != null) {
                        java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                "type");

                        if (fullTypeName != null) {
                            java.lang.String nsPrefix = null;

                            if (fullTypeName.indexOf(":") > -1) {
                                nsPrefix = fullTypeName.substring(0,
                                        fullTypeName.indexOf(":"));
                            }

                            nsPrefix = (nsPrefix == null) ? "" : nsPrefix;

                            java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(
                                        ":") + 1);

                            if (!"priorAuthorizationSearchResponse".equals(type)) {
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext()
                                                               .getNamespaceURI(nsPrefix);

                                return (PriorAuthorizationSearchResponse) ExtensionMapper.getTypeObject(nsUri,
                                    type, reader);
                            }
                        }
                    }

                    // Note all attributes that were handled. Used to differ normal attributes
                    // from anyAttributes.
                    java.util.Vector handledAttributes = new java.util.Vector();

                    reader.next();

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                            new javax.xml.namespace.QName("", "return").equals(
                                reader.getName())) {
                        object.set_return(PriorAuthHeader.Factory.parse(reader));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()) {
                        // A start element we are not expecting indicates a trailing invalid property
                        throw new org.apache.axis2.databinding.ADBException(
                            "Unexpected subelement " + reader.getName());
                    }
                } catch (javax.xml.stream.XMLStreamException e) {
                    throw new java.lang.Exception(e);
                }

                return object;
            }
        } //end of factory class
    }

    public static class SavePriorAuthHeaderResponseE implements org.apache.axis2.databinding.ADBBean {
        public static final javax.xml.namespace.QName MY_QNAME = new javax.xml.namespace.QName("http://services.lbm.avalon.com/",
                "savePriorAuthHeaderResponse", "ns1");

        /**
         * field for SavePriorAuthHeaderResponse
         */
        protected SavePriorAuthHeaderResponse localSavePriorAuthHeaderResponse;

        /**
         * Auto generated getter method
         * @return SavePriorAuthHeaderResponse
         */
        public SavePriorAuthHeaderResponse getSavePriorAuthHeaderResponse() {
            return localSavePriorAuthHeaderResponse;
        }

        /**
         * Auto generated setter method
         * @param param SavePriorAuthHeaderResponse
         */
        public void setSavePriorAuthHeaderResponse(
            SavePriorAuthHeaderResponse param) {
            this.localSavePriorAuthHeaderResponse = param;
        }

        /**
         *
         * @param parentQName
         * @param factory
         * @return org.apache.axiom.om.OMElement
         */
        public org.apache.axiom.om.OMElement getOMElement(
            final javax.xml.namespace.QName parentQName,
            final org.apache.axiom.om.OMFactory factory)
            throws org.apache.axis2.databinding.ADBException {
            org.apache.axiom.om.OMDataSource dataSource = new org.apache.axis2.databinding.ADBDataSource(this,
                    MY_QNAME);

            return factory.createOMElement(dataSource, MY_QNAME);
        }

        public void serialize(final javax.xml.namespace.QName parentQName,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException,
                org.apache.axis2.databinding.ADBException {
            serialize(parentQName, xmlWriter, false);
        }

        public void serialize(final javax.xml.namespace.QName parentQName,
            javax.xml.stream.XMLStreamWriter xmlWriter, boolean serializeType)
            throws javax.xml.stream.XMLStreamException,
                org.apache.axis2.databinding.ADBException {
            //We can safely assume an element has only one type associated with it
            if (localSavePriorAuthHeaderResponse == null) {
                throw new org.apache.axis2.databinding.ADBException(
                    "savePriorAuthHeaderResponse cannot be null!");
            }

            localSavePriorAuthHeaderResponse.serialize(MY_QNAME, xmlWriter);
        }

        private static java.lang.String generatePrefix(
            java.lang.String namespace) {
            if (namespace.equals("http://services.lbm.avalon.com/")) {
                return "ns1";
            }

            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix,
            java.lang.String namespace, java.lang.String localPart,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

            if (writerPrefix != null) {
                xmlWriter.writeStartElement(namespace, localPart);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }

        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,
            java.lang.String namespace, java.lang.String attName,
            java.lang.String attValue,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }

            xmlWriter.writeAttribute(namespace, attName, attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,
            java.lang.String attName, java.lang.String attValue,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attValue);
            }
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeQNameAttribute(java.lang.String namespace,
            java.lang.String attName, javax.xml.namespace.QName qname,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String attributeNamespace = qname.getNamespaceURI();
            java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);

            if (attributePrefix == null) {
                attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
            }

            java.lang.String attributeValue;

            if (attributePrefix.trim().length() > 0) {
                attributeValue = attributePrefix + ":" + qname.getLocalPart();
            } else {
                attributeValue = qname.getLocalPart();
            }

            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attributeValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attributeValue);
            }
        }

        /**
         *  method to handle Qnames
         */
        private void writeQName(javax.xml.namespace.QName qname,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();

            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);

                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix, namespaceURI);
                }

                if (prefix.trim().length() > 0) {
                    xmlWriter.writeCharacters(prefix + ":" +
                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            qname));
                }
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }

                    namespaceURI = qnames[i].getNamespaceURI();

                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);

                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix, namespaceURI);
                        }

                        if (prefix.trim().length() > 0) {
                            stringToWrite.append(prefix).append(":")
                                         .append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                    qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                    qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                qnames[i]));
                    }
                }

                xmlWriter.writeCharacters(stringToWrite.toString());
            }
        }

        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(
            javax.xml.stream.XMLStreamWriter xmlWriter,
            java.lang.String namespace)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);

            if (prefix == null) {
                prefix = generatePrefix(namespace);

                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();

                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);

                    if ((uri == null) || (uri.length() == 0)) {
                        break;
                    }

                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }

                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }

            return prefix;
        }

        /**
         * databinding method to get an XML representation of this object
         *
         */
        public javax.xml.stream.XMLStreamReader getPullParser(
            javax.xml.namespace.QName qName)
            throws org.apache.axis2.databinding.ADBException {
            //We can safely assume an element has only one type associated with it
            return localSavePriorAuthHeaderResponse.getPullParser(MY_QNAME);
        }

        /**
         *  Factory class that keeps the parse method
         */
        public static class Factory {
            /**
             * static method to create the object
             * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
             *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
             * Postcondition: If this object is an element, the reader is positioned at its end element
             *                If this object is a complex type, the reader is positioned at the end element of its outer element
             */
            public static SavePriorAuthHeaderResponseE parse(
                javax.xml.stream.XMLStreamReader reader)
                throws java.lang.Exception {
                SavePriorAuthHeaderResponseE object = new SavePriorAuthHeaderResponseE();

                int event;
                java.lang.String nillableValue = null;
                java.lang.String prefix = "";
                java.lang.String namespaceuri = "";

                try {
                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    // Note all attributes that were handled. Used to differ normal attributes
                    // from anyAttributes.
                    java.util.Vector handledAttributes = new java.util.Vector();

                    while (!reader.isEndElement()) {
                        if (reader.isStartElement()) {
                            if (reader.isStartElement() &&
                                    new javax.xml.namespace.QName(
                                        "http://services.lbm.avalon.com/",
                                        "savePriorAuthHeaderResponse").equals(
                                        reader.getName())) {
                                object.setSavePriorAuthHeaderResponse(SavePriorAuthHeaderResponse.Factory.parse(
                                        reader));
                            } // End of if for expected property start element

                            else {
                                // A start element we are not expecting indicates an invalid parameter was passed
                                throw new org.apache.axis2.databinding.ADBException(
                                    "Unexpected subelement " +
                                    reader.getName());
                            }
                        } else {
                            reader.next();
                        }
                    } // end of while loop
                } catch (javax.xml.stream.XMLStreamException e) {
                    throw new java.lang.Exception(e);
                }

                return object;
            }
        } //end of factory class
    }

    public static class PriorAuthHeaderDTO implements org.apache.axis2.databinding.ADBBean {
        /* This type was generated from the piece of schema that had
           name = priorAuthHeaderDTO
           Namespace URI = http://services.lbm.avalon.com/
           Namespace Prefix = ns1
         */

        /**
         * field for AuthInboundChannel
         */
        protected java.lang.String localAuthInboundChannel;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localAuthInboundChannelTracker = false;

        /**
         * field for AuthRequestedDate
         */
        protected java.lang.String localAuthRequestedDate;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localAuthRequestedDateTracker = false;

        /**
         * field for AppCreateUserId
         */
        protected java.lang.String localAppCreateUserId;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localAppCreateUserIdTracker = false;

        /**
         * field for AppMaintDatetime
         */
        protected java.lang.String localAppMaintDatetime;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localAppMaintDatetimeTracker = false;

        /**
         * field for AppCreateDatetime
         */
        protected java.lang.String localAppCreateDatetime;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localAppCreateDatetimeTracker = false;

        /**
         * field for PriorAuthSubmissionStatusCode
         */
        protected java.lang.String localPriorAuthSubmissionStatusCode;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localPriorAuthSubmissionStatusCodeTracker = false;

        /**
         * field for PriorAuthPriorityCode
         */
        protected java.lang.String localPriorAuthPriorityCode;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localPriorAuthPriorityCodeTracker = false;

        /**
         * field for PriorAuthStatusCode
         */
        protected java.lang.String localPriorAuthStatusCode;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localPriorAuthStatusCodeTracker = false;

        /**
         * field for PriorAuthStatusDesc
         */
        protected java.lang.String localPriorAuthStatusDesc;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localPriorAuthStatusDescTracker = false;

        /**
         * field for PriorAuthNumber
         */
        protected java.lang.String localPriorAuthNumber;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localPriorAuthNumberTracker = false;

        /**
         * field for AuthNumberSearched
         */
        protected java.lang.String localAuthNumberSearched;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localAuthNumberSearchedTracker = false;

        /**
         * field for IdCardNumber
         */
        protected java.lang.String localIdCardNumber;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localIdCardNumberTracker = false;

        /**
         * field for MemberNumber
         */
        protected java.lang.String localMemberNumber;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localMemberNumberTracker = false;

        /**
         * field for HealthPlanId
         */
        protected java.lang.String localHealthPlanId;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localHealthPlanIdTracker = false;

        /**
         * field for HealthPlanGroupId
         */
        protected java.lang.String localHealthPlanGroupId;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localHealthPlanGroupIdTracker = false;

        /**
         * field for ExtractPerformedQuantity
         */
        protected short localExtractPerformedQuantity;

        /**
         * field for PriorAuthBeginServiceDate
         */
        protected java.lang.String localPriorAuthBeginServiceDate;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localPriorAuthBeginServiceDateTracker = false;

        /**
         * field for PriorAuthEndServiceDate
         */
        protected java.lang.String localPriorAuthEndServiceDate;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localPriorAuthEndServiceDateTracker = false;

        /**
         * field for MasterPatientId
         */
        protected java.lang.String localMasterPatientId;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localMasterPatientIdTracker = false;

        /**
         * field for IcdRevisionNumber
         */
        protected java.lang.String localIcdRevisionNumber;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localIcdRevisionNumberTracker = false;

        /**
         * field for PriorAuthHeaderKey
         */
        protected int localPriorAuthHeaderKey;

        /**
         * field for PatientGenderRelationshipCode
         */
        protected java.lang.String localPatientGenderRelationshipCode;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localPatientGenderRelationshipCodeTracker = false;

        /**
         * field for PriorAuthOriginalStatusDatetime
         */
        protected java.lang.String localPriorAuthOriginalStatusDatetime;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localPriorAuthOriginalStatusDatetimeTracker = false;

        /**
         * field for PriorAuthCurrentStatusDatetime
         */
        protected java.lang.String localPriorAuthCurrentStatusDatetime;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localPriorAuthCurrentStatusDatetimeTracker = false;

        /**
         * field for PriorAuthCurrentStatusUserId
         */
        protected java.lang.String localPriorAuthCurrentStatusUserId;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localPriorAuthCurrentStatusUserIdTracker = false;

        /**
         * field for PriorAuthOriginalStatusUserId
         */
        protected java.lang.String localPriorAuthOriginalStatusUserId;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localPriorAuthOriginalStatusUserIdTracker = false;

        /**
         * field for PriorAuthContactDatetime
         */
        protected java.lang.String localPriorAuthContactDatetime;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localPriorAuthContactDatetimeTracker = false;

        /**
         * field for PriorAuthDueDatetime
         */
        protected java.lang.String localPriorAuthDueDatetime;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localPriorAuthDueDatetimeTracker = false;

        /**
         * field for PriorAuthWorkedBy
         */
        protected java.lang.String localPriorAuthWorkedBy;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localPriorAuthWorkedByTracker = false;

        /**
         * field for PriorAuthWithdrawnReasonCode
         */
        protected java.lang.String localPriorAuthWithdrawnReasonCode;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localPriorAuthWithdrawnReasonCodeTracker = false;

        /**
         * field for PriorAuthWithdrawnReasonDesc
         */
        protected java.lang.String localPriorAuthWithdrawnReasonDesc;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localPriorAuthWithdrawnReasonDescTracker = false;

        // Line of Business and patient information items added July 14, 2017
        /**
         * field for PriorAuthBusinessSectorCode
         */
        protected java.lang.String localPriorAuthBusinessSectorCode;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localPriorAuthBusinessSectorCodeTracker = false;

        /**
         * field for PriorAuthBusinessSectorDescription
         */
        protected java.lang.String localPriorAuthBusinessSectorDescription;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localPriorAuthBusinessSectorDescriptionTracker = false;

        /**
         * field for PriorAuthBusinessSegmentCode
         */
        protected java.lang.String localPriorAuthBusinessSegmentCode;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localPriorAuthBusinessSegmentCodeTracker = false;

        /**
         * field for PriorAuthBusinessSegmentDescription
         */
        protected java.lang.String localPriorAuthBusinessSegmentDescription;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localPriorAuthBusinessSegmentDescriptionTracker = false;

        /**
         * field for PriorAuthPatientFirstName
         */
        protected java.lang.String localPriorAuthPatientFirstName;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localPriorAuthPatientFirstNameTracker = false;

        /**
         * field for PriorAuthPatientMiddleName
         */
        protected java.lang.String localPriorAuthPatientMiddleName;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localPriorAuthPatientMiddleNameTracker = false;

        /**
         * field for PriorAuthPatientLastName
         */
        protected java.lang.String localPriorAuthPatientLastName;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localPriorAuthPatientLastNameTracker = false;

        /**
         * field for PriorAuthPatientSuffixName
         */
        protected java.lang.String localPriorAuthPatientSuffixName;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localPriorAuthPatientSuffixNameTracker = false;

        /**
         * field for PriorAuthPatientBirthDate
         */
        protected java.lang.String localPriorAuthPatientBirthDate;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localPriorAuthPatientBirthDateTracker = false;

        /**
         * field for PriorAuthPatientGenderCode
         */
        protected java.lang.String localPriorAuthPatientGenderCode;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localPriorAuthPatientGenderCodeTracker = false;

        
        public boolean isAuthInboundChannelSpecified() {
            return localAuthInboundChannelTracker;
        }

        /**
         * Auto generated getter method
         * @return java.lang.String
         */
        public java.lang.String getAuthInboundChannel() {
            return localAuthInboundChannel;
        }

        /**
         * Auto generated setter method
         * @param param AuthInboundChannel
         */
        public void setAuthInboundChannel(java.lang.String param) {
            localAuthInboundChannelTracker = param != null;

            this.localAuthInboundChannel = param;
        }

        public boolean isAuthRequestedDateSpecified() {
            return localAuthRequestedDateTracker;
        }

        /**
         * Auto generated getter method
         * @return java.lang.String
         */
        public java.lang.String getAuthRequestedDate() {
            return localAuthRequestedDate;
        }

        /**
         * Auto generated setter method
         * @param param AuthRequestedDate
         */
        public void setAuthRequestedDate(java.lang.String param) {
            localAuthRequestedDateTracker = param != null;

            this.localAuthRequestedDate = param;
        }

        public boolean isAppCreateUserIdSpecified() {
            return localAppCreateUserIdTracker;
        }

        /**
         * Auto generated getter method
         * @return java.lang.String
         */
        public java.lang.String getAppCreateUserId() {
            return localAppCreateUserId;
        }

        /**
         * Auto generated setter method
         * @param param AppCreateUserId
         */
        public void setAppCreateUserId(java.lang.String param) {
            localAppCreateUserIdTracker = param != null;

            this.localAppCreateUserId = param;
        }

        public boolean isAppMaintDatetimeSpecified() {
            return localAppMaintDatetimeTracker;
        }

        /**
         * Auto generated getter method
         * @return java.lang.String
         */
        public java.lang.String getAppMaintDatetime() {
            return localAppMaintDatetime;
        }

        /**
         * Auto generated setter method
         * @param param AppMaintDatetime
         */
        public void setAppMaintDatetime(java.lang.String param) {
            localAppMaintDatetimeTracker = param != null;

            this.localAppMaintDatetime = param;
        }
        
        public boolean isAppCreateDatetimeSpecified() {
            return localAppCreateDatetimeTracker;
        }

        /**
         * Auto generated getter method
         * @return java.lang.String
         */
        public java.lang.String getAppCreateDatetime() {
            return localAppCreateDatetime;
        }

        /**
         * Auto generated setter method
         * @param param AppCreateDatetime
         */
        public void setAppCreateDatetime(java.lang.String param) {
            localAppCreateDatetimeTracker = param != null;

            this.localAppCreateDatetime = param;
        }

        public boolean isPriorAuthSubmissionStatusCodeSpecified() {
            return localPriorAuthSubmissionStatusCodeTracker;
        }

        /**
         * Auto generated getter method
         * @return java.lang.String
         */
        public java.lang.String getPriorAuthSubmissionStatusCode() {
            return localPriorAuthSubmissionStatusCode;
        }

        /**
         * Auto generated setter method
         * @param param PriorAuthSubmissionStatusCode
         */
        public void setPriorAuthSubmissionStatusCode(java.lang.String param) {
           localPriorAuthSubmissionStatusCodeTracker = param != null;

            this.localPriorAuthSubmissionStatusCode = param;
        }

        public boolean isPriorAuthPriorityCodeSpecified() {
            return localPriorAuthPriorityCodeTracker;
        }

        /**
         * Auto generated getter method
         * @return java.lang.String
         */
        public java.lang.String getPriorAuthPriorityCode() {
            return localPriorAuthPriorityCode;
        }

        /**
         * Auto generated setter method
         * @param param PriorAuthPriorityCode
         */
        public void setPriorAuthPriorityCode(java.lang.String param) {
            localPriorAuthPriorityCodeTracker = param != null;

            this.localPriorAuthPriorityCode = param;
        }

        public boolean isPriorAuthStatusCodeSpecified() {
            return localPriorAuthStatusCodeTracker;
        }

        /**
         * Auto generated getter method
         * @return java.lang.String
         */
        public java.lang.String getPriorAuthStatusCode() {
            return localPriorAuthStatusCode;
        }

        /**
         * Auto generated setter method
         * @param param PriorAuthStatusCode
         */
        public void setPriorAuthStatusCode(java.lang.String param) {
            localPriorAuthStatusCodeTracker = param != null;
            this.localPriorAuthStatusCode = param;
        }
 
        public boolean isPriorAuthStatusDescSpecified(){
	        return localPriorAuthStatusDescTracker;
	    }
	
	    /**
	     * Auto generated getter method
	     * @return java.lang.String
	     */
	    public  java.lang.String getPriorAuthStatusDesc(){
	        return localPriorAuthStatusDesc;
	    }
	    
	    /**
	      * Auto generated setter method
	      * @param param PriorAuthStatusDesc
	      */
	    public void setPriorAuthStatusDesc(java.lang.String param){
	        localPriorAuthStatusDescTracker = param != null;
	        this.localPriorAuthStatusDesc=param;
	    }

        public boolean isPriorAuthNumberSpecified() {
            return localPriorAuthNumberTracker;
        }

        /**
         * Auto generated getter method
         * @return java.lang.String
         */
        public java.lang.String getPriorAuthNumber() {
            return localPriorAuthNumber;
        }

        /**
         * Auto generated setter method
         * @param param PriorAuthNumber
         */
        public void setPriorAuthNumber(java.lang.String param) {
            localPriorAuthNumberTracker = param != null;

            this.localPriorAuthNumber = param;
        }

        public boolean isAuthNumberSearchedSpecified() {
            return localAuthNumberSearchedTracker;
        }

        /**
         * Auto generated getter method
         * @return java.lang.String
         */
        public java.lang.String getAuthNumberSearched() {
            return localAuthNumberSearched;
        }

        /**
         * Auto generated setter method
         * @param param AuthNumberSearched
         */
        public void setAuthNumberSearched(java.lang.String param) {
            localAuthNumberSearchedTracker = param != null;

            this.localAuthNumberSearched = param;
        }

        public boolean isIdCardNumberSpecified() {
            return localIdCardNumberTracker;
        }

        /**
         * Auto generated getter method
         * @return java.lang.String
         */
        public java.lang.String getIdCardNumber() {
            return localIdCardNumber;
        }

        /**
         * Auto generated setter method
         * @param param IdCardNumber
         */
        public void setIdCardNumber(java.lang.String param) {
            localIdCardNumberTracker = param != null;

            this.localIdCardNumber = param;
        }

        public boolean isMemberNumberSpecified() {
            return localMemberNumberTracker;
        }

        /**
         * Auto generated getter method
         * @return java.lang.String
         */
        public java.lang.String getMemberNumber() {
            return localMemberNumber;
        }

        /**
         * Auto generated setter method
         * @param param MemberNumber
         */
        public void setMemberNumber(java.lang.String param) {
            localMemberNumberTracker = param != null;

            this.localMemberNumber = param;
        }

        public boolean isHealthPlanIdSpecified() {
            return localHealthPlanIdTracker;
        }

        /**
         * Auto generated getter method
         * @return java.lang.String
         */
        public java.lang.String getHealthPlanId() {
            return localHealthPlanId;
        }

        /**
         * Auto generated setter method
         * @param param HealthPlanId
         */
        public void setHealthPlanId(java.lang.String param) {
            localHealthPlanIdTracker = param != null;

            this.localHealthPlanId = param;
        }

        public boolean isHealthPlanGroupIdSpecified() {
            return localHealthPlanGroupIdTracker;
        }

        /**
         * Auto generated getter method
         * @return java.lang.String
         */
        public java.lang.String getHealthPlanGroupId() {
            return localHealthPlanGroupId;
        }

        /**
         * Auto generated setter method
         * @param param HealthPlanGroupId
         */
        public void setHealthPlanGroupId(java.lang.String param) {
            localHealthPlanGroupIdTracker = param != null;

            this.localHealthPlanGroupId = param;
        }

        /**
         * Auto generated getter method
         * @return short
         */
        public short getExtractPerformedQuantity() {
            return localExtractPerformedQuantity;
        }

        /**
         * Auto generated setter method
         * @param param ExtractPerformedQuantity
         */
        public void setExtractPerformedQuantity(short param) {
            this.localExtractPerformedQuantity = param;
        }

        public boolean isPriorAuthBeginServiceDateSpecified() {
            return localPriorAuthBeginServiceDateTracker;
        }

        /**
         * Auto generated getter method
         * @return java.lang.String
         */
        public java.lang.String getPriorAuthBeginServiceDate() {
            return localPriorAuthBeginServiceDate;
        }

        /**
         * Auto generated setter method
         * @param param PriorAuthBeginServiceDate
         */
        public void setPriorAuthBeginServiceDate(java.lang.String param) {
            localPriorAuthBeginServiceDateTracker = param != null;

            this.localPriorAuthBeginServiceDate = param;
        }

        public boolean isPriorAuthEndServiceDateSpecified() {
            return localPriorAuthEndServiceDateTracker;
        }

        /**
         * Auto generated getter method
         * @return java.lang.String
         */
        public java.lang.String getPriorAuthEndServiceDate() {
            return localPriorAuthEndServiceDate;
        }

        /**
         * Auto generated setter method
         * @param param PriorAuthEndServiceDate
         */
        public void setPriorAuthEndServiceDate(java.lang.String param) {
            localPriorAuthEndServiceDateTracker = param != null;

            this.localPriorAuthEndServiceDate = param;
        }

        public boolean isMasterPatientIdSpecified() {
            return localMasterPatientIdTracker;
        }

        /**
         * Auto generated getter method
         * @return java.lang.String
         */
        public java.lang.String getMasterPatientId() {
            return localMasterPatientId;
        }

        /**
         * Auto generated setter method
         * @param param MasterPatientId
         */
        public void setMasterPatientId(java.lang.String param) {
            localMasterPatientIdTracker = param != null;

            this.localMasterPatientId = param;
        }

        public boolean isIcdRevisionNumberSpecified() {
            return localIcdRevisionNumberTracker;
        }

        /**
         * Auto generated getter method
         * @return java.lang.String
         */
        public java.lang.String getIcdRevisionNumber() {
            return localIcdRevisionNumber;
        }

        /**
         * Auto generated setter method
         * @param param IcdRevisionNumber
         */
        public void setIcdRevisionNumber(java.lang.String param) {
            localIcdRevisionNumberTracker = param != null;

            this.localIcdRevisionNumber = param;
        }

        /**
         * Auto generated getter method
         * @return int
         */
        public int getPriorAuthHeaderKey() {
            return localPriorAuthHeaderKey;
        }

        /**
         * Auto generated setter method
         * @param param PriorAuthHeaderKey
         */
        public void setPriorAuthHeaderKey(int param) {
            this.localPriorAuthHeaderKey = param;
        }

        public boolean isPatientGenderRelationshipCodeSpecified() {
            return localPatientGenderRelationshipCodeTracker;
        }

        /**
         * Auto generated getter method
         * @return java.lang.String
         */
        public java.lang.String getPatientGenderRelationshipCode() {
            return localPatientGenderRelationshipCode;
        }

        /**
         * Auto generated setter method
         * @param param PatientGenderRelationshipCode
         */
        public void setPatientGenderRelationshipCode(java.lang.String param) {
            localPatientGenderRelationshipCodeTracker = param != null;

            this.localPatientGenderRelationshipCode = param;
        }

        public boolean isPriorAuthOriginalStatusDatetimeSpecified() {
            return localPriorAuthOriginalStatusDatetimeTracker;
        }

        /**
         * Auto generated getter method
         * @return java.lang.String
         */
        public java.lang.String getPriorAuthOriginalStatusDatetime() {
            return localPriorAuthOriginalStatusDatetime;
        }

        /**
         * Auto generated setter method
         * @param param PriorAuthOriginalStatusDatetime
         */
        public void setPriorAuthOriginalStatusDatetime(java.lang.String param) {
            localPriorAuthOriginalStatusDatetimeTracker = param != null;

            this.localPriorAuthOriginalStatusDatetime = param;
        }

        public boolean isPriorAuthCurrentStatusDatetimeSpecified() {
            return localPriorAuthCurrentStatusDatetimeTracker;
        }

        /**
         * Auto generated getter method
         * @return java.lang.String
         */
        public java.lang.String getPriorAuthCurrentStatusDatetime() {
            return localPriorAuthCurrentStatusDatetime;
        }

        /**
         * Auto generated setter method
         * @param param PriorAuthCurrentStatusDatetime
         */
        public void setPriorAuthCurrentStatusDatetime(java.lang.String param) {
            localPriorAuthCurrentStatusDatetimeTracker = param != null;

            this.localPriorAuthCurrentStatusDatetime = param;
        }

        public boolean isPriorAuthCurrentStatusUserIdSpecified() {
            return localPriorAuthCurrentStatusUserIdTracker;
        }

        /**
         * Auto generated getter method
         * @return java.lang.String
         */
        public java.lang.String getPriorAuthCurrentStatusUserId() {
            return localPriorAuthCurrentStatusUserId;
        }

        /**
         * Auto generated setter method
         * @param param PriorAuthCurrentStatusUserId
         */
        public void setPriorAuthCurrentStatusUserId(java.lang.String param) {
            localPriorAuthCurrentStatusUserIdTracker = param != null;

            this.localPriorAuthCurrentStatusUserId = param;
        }

        public boolean isPriorAuthOriginalStatusUserIdSpecified() {
            return localPriorAuthOriginalStatusUserIdTracker;
        }

        /**
         * Auto generated getter method
         * @return java.lang.String
         */
        public java.lang.String getPriorAuthOriginalStatusUserId() {
            return localPriorAuthOriginalStatusUserId;
        }

        /**
         * Auto generated setter method
         * @param param PriorAuthOriginalStatusUserId
         */
        public void setPriorAuthOriginalStatusUserId(java.lang.String param) {
            localPriorAuthOriginalStatusUserIdTracker = param != null;

            this.localPriorAuthOriginalStatusUserId = param;
        }

        public boolean isPriorAuthContactDatetimeSpecified(){
             return localPriorAuthContactDatetimeTracker;
        }

        /**
         * Auto generated getter method
         * @return java.lang.String
         */
        public  java.lang.String getPriorAuthContactDatetime(){
           return localPriorAuthContactDatetime;
         }
         
        /**
         * Auto generated setter method
         * @param param PriorAuthDueDatetime
         */
        public void setPriorAuthContactDatetime(java.lang.String param){
           localPriorAuthContactDatetimeTracker = param != null;
           this.localPriorAuthContactDatetime=param;
        }

        public boolean isPriorAuthDueDatetimeSpecified(){
            return localPriorAuthDueDatetimeTracker;
        }

        /**
         * Auto generated getter method
         * @return java.lang.String
         */
        public  java.lang.String getPriorAuthDueDatetime(){
           return localPriorAuthDueDatetime;
         }
         
        /**
         * Auto generated setter method
         * @param param PriorAuthDueDatetime
         */
        public void setPriorAuthDueDatetime(java.lang.String param){
           localPriorAuthDueDatetimeTracker = param != null;
           this.localPriorAuthDueDatetime=param;
        }

        public boolean isPriorAuthWorkedBySpecified(){
            return localPriorAuthWorkedByTracker;
        }

        /**
         * Auto generated getter method
         * @return java.lang.String
         */
        public  java.lang.String getPriorAuthWorkedBy(){
           return localPriorAuthWorkedBy;
        }
         
        /**
         * Auto generated setter method
         * @param param PriorAuthWorkedBy
         */
        public void setPriorAuthWorkedBy(java.lang.String param){
           localPriorAuthWorkedByTracker = param != null;
           this.localPriorAuthWorkedBy=param;
        }

        public boolean isPriorAuthWithdrawnReasonCodeSpecified(){
            return localPriorAuthWithdrawnReasonCodeTracker;
        }

        /**
         * Auto generated getter method
         * @return java.lang.String
         */
        public  java.lang.String getPriorAuthWithdrawnReasonCode(){
           return localPriorAuthWithdrawnReasonCode;
        }
         
        /**
         * Auto generated setter method
         * @param param PriorAuthWithdrawnReasonCode
         */
        public void setPriorAuthWithdrawnReasonCode(java.lang.String param){
           localPriorAuthWithdrawnReasonCodeTracker = param != null;
           this.localPriorAuthWithdrawnReasonCode=param;
        }

        public boolean isPriorAuthWithdrawnReasonDescSpecified(){
            return localPriorAuthWithdrawnReasonDescTracker;
        }

        /**
         * Auto generated getter method
         * @return java.lang.String
         */
        public  java.lang.String getPriorAuthWithdrawnReasonDesc(){
           return localPriorAuthWithdrawnReasonDesc;
        }
         
        /**
         * Auto generated setter method
         * @param param PriorAuthWithdrawnReasonDesc
         */
        public void setPriorAuthWithdrawnReasonDesc(java.lang.String param){
           localPriorAuthWithdrawnReasonDescTracker = param != null;
           this.localPriorAuthWithdrawnReasonDesc=param;
        }

        public boolean isPriorAuthBusinessSectorCodeSpecified(){
            return localPriorAuthBusinessSectorCodeTracker;
        }

        /**
         * Auto generated getter method
         * @return java.lang.String
         */
        public  java.lang.String getPriorAuthBusinessSectorCode(){
           return localPriorAuthBusinessSectorCode;
        }
         
        /**
         * Auto generated setter method
         * @param param PriorAuthBusinessSectorCode
         */
        public void setPriorAuthBusinessSectorCode(java.lang.String param){
           localPriorAuthBusinessSectorCodeTracker = param != null;
           this.localPriorAuthBusinessSectorCode=param;
        }

        public boolean isPriorAuthBusinessSectorDescriptionSpecified(){
            return localPriorAuthBusinessSectorDescriptionTracker;
        }

        /**
         * Auto generated getter method
         * @return java.lang.String
         */
        public  java.lang.String getPriorAuthBusinessSectorDescription(){
           return localPriorAuthBusinessSectorDescription;
        }
         
        /**
         * Auto generated setter method
         * @param param PriorAuthBusinessSectorDescription
         */
        public void setPriorAuthBusinessSectorDescription(java.lang.String param){
           localPriorAuthBusinessSectorDescriptionTracker = param != null;
           this.localPriorAuthBusinessSectorDescription=param;
        }

        public boolean isPriorAuthBusinessSegmentCodeSpecified(){
            return localPriorAuthBusinessSegmentCodeTracker;
        }

        /**
         * Auto generated getter method
         * @return java.lang.String
         */
        public  java.lang.String getPriorAuthBusinessSegmentCode(){
           return localPriorAuthBusinessSegmentCode;
        }
         
        /**
         * Auto generated setter method
         * @param param PriorAuthBusinessSegmentCode
         */
        public void setPriorAuthBusinessSegmentCode(java.lang.String param){
           localPriorAuthBusinessSegmentCodeTracker = param != null;
           this.localPriorAuthBusinessSegmentCode=param;
        }

        public boolean isPriorAuthBusinessSegmentDescriptionSpecified(){
            return localPriorAuthBusinessSegmentDescriptionTracker;
        }

        /**
         * Auto generated getter method
         * @return java.lang.String
         */
        public  java.lang.String getPriorAuthBusinessSegmentDescription(){
           return localPriorAuthBusinessSegmentDescription;
        }
         
        /**
         * Auto generated setter method
         * @param param PriorAuthBusinessSegmentDescription
         */
        public void setPriorAuthBusinessSegmentDescription(java.lang.String param){
           localPriorAuthBusinessSegmentDescriptionTracker = param != null;
           this.localPriorAuthBusinessSegmentDescription=param;
        }

        public boolean isPriorAuthPatientFirstNameSpecified(){
            return localPriorAuthPatientFirstNameTracker;
        }

        /**
         * Auto generated getter method
         * @return java.lang.String
         */
        public  java.lang.String getPriorAuthPatientFirstName(){
           return localPriorAuthPatientFirstName;
        }
         
        /**
         * Auto generated setter method
         * @param param PriorAuthPatientFirstName
         */
        public void setPriorAuthPatientFirstName(java.lang.String param){
           localPriorAuthPatientFirstNameTracker = param != null;
           this.localPriorAuthPatientFirstName=param;
        }

        public boolean isPriorAuthPatientMiddleNameSpecified(){
            return localPriorAuthPatientMiddleNameTracker;
        }

        /**
         * Auto generated getter method
         * @return java.lang.String
         */
        public  java.lang.String getPriorAuthPatientMiddleName(){
           return localPriorAuthPatientMiddleName;
        }
         
        /**
         * Auto generated setter method
         * @param param PriorAuthPatientMiddleName
         */
        public void setPriorAuthPatientMiddleName(java.lang.String param){
           localPriorAuthPatientMiddleNameTracker = param != null;
           this.localPriorAuthPatientMiddleName=param;
        }

        public boolean isPriorAuthPatientLastNameSpecified(){
            return localPriorAuthPatientLastNameTracker;
        }

        /**
         * Auto generated getter method
         * @return java.lang.String
         */
        public  java.lang.String getPriorAuthPatientLastName(){
           return localPriorAuthPatientLastName;
        }
         
        /**
         * Auto generated setter method
         * @param param PriorAuthPatientLastName
         */
        public void setPriorAuthPatientLastName(java.lang.String param){
           localPriorAuthPatientLastNameTracker = param != null;
           this.localPriorAuthPatientLastName=param;
        }

        public boolean isPriorAuthPatientSuffixNameSpecified(){
            return localPriorAuthPatientSuffixNameTracker;
        }

        /**
         * Auto generated getter method
         * @return java.lang.String
         */
        public  java.lang.String getPriorAuthPatientSuffixName(){
           return localPriorAuthPatientSuffixName;
        }
         
        /**
         * Auto generated setter method
         * @param param PriorAuthPatientSuffixName
         */
        public void setPriorAuthPatientSuffixName(java.lang.String param){
           localPriorAuthPatientSuffixNameTracker = param != null;
           this.localPriorAuthPatientSuffixName=param;
        }

        public boolean isPriorAuthPatientBirthDateSpecified(){
            return localPriorAuthPatientBirthDateTracker;
        }

        /**
         * Auto generated getter method
         * @return java.lang.String
         */
        public  java.lang.String getPriorAuthPatientBirthDate(){
           return localPriorAuthPatientBirthDate;
        }
         
        /**
         * Auto generated setter method
         * @param param PriorAuthPatientBirthDate
         */
        public void setPriorAuthPatientBirthDate(java.lang.String param){
           localPriorAuthPatientBirthDateTracker = param != null;
           this.localPriorAuthPatientBirthDate=param;
        }

        public boolean isPriorAuthPatientGenderCodeSpecified(){
            return localPriorAuthPatientGenderCodeTracker;
        }

        /**
         * Auto generated getter method
         * @return java.lang.String
         */
        public  java.lang.String getPriorAuthPatientGenderCode(){
           return localPriorAuthPatientGenderCode;
        }
         
        /**
         * Auto generated setter method
         * @param param PriorAuthPatientGenderCode
         */
        public void setPriorAuthPatientGenderCode(java.lang.String param){
           localPriorAuthPatientGenderCodeTracker = param != null;
           this.localPriorAuthPatientGenderCode=param;
        }

        
        /**
         *
         * @param parentQName
         * @param factory
         * @return org.apache.axiom.om.OMElement
         */
        public org.apache.axiom.om.OMElement getOMElement(
            final javax.xml.namespace.QName parentQName,
            final org.apache.axiom.om.OMFactory factory)
            throws org.apache.axis2.databinding.ADBException {
            org.apache.axiom.om.OMDataSource dataSource = new org.apache.axis2.databinding.ADBDataSource(this,
                    parentQName);

            return factory.createOMElement(dataSource, parentQName);
        }

        public void serialize(final javax.xml.namespace.QName parentQName,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException,
                org.apache.axis2.databinding.ADBException {
            serialize(parentQName, xmlWriter, false);
        }

        public void serialize(final javax.xml.namespace.QName parentQName,
            javax.xml.stream.XMLStreamWriter xmlWriter, boolean serializeType)
            throws javax.xml.stream.XMLStreamException,
                org.apache.axis2.databinding.ADBException {
            java.lang.String prefix = null;
            java.lang.String namespace = null;

            prefix = parentQName.getPrefix();
            namespace = parentQName.getNamespaceURI();
            writeStartElement(prefix, namespace, parentQName.getLocalPart(),
                xmlWriter);

            if (serializeType) {
                java.lang.String namespacePrefix = registerPrefix(xmlWriter,
                        "http://services.lbm.avalon.com/");

                if ((namespacePrefix != null) &&
                        (namespacePrefix.trim().length() > 0)) {
                    writeAttribute("xsi",
                        "http://www.w3.org/2001/XMLSchema-instance", "type",
                        namespacePrefix + ":priorAuthHeaderDTO", xmlWriter);
                } else {
                    writeAttribute("xsi",
                        "http://www.w3.org/2001/XMLSchema-instance", "type",
                        "priorAuthHeaderDTO", xmlWriter);
                }
            }

            if (localAuthInboundChannelTracker) {
                namespace = "";
                writeStartElement(null, namespace, "authInboundChannel",
                    xmlWriter);

                if (localAuthInboundChannel == null) {
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException(
                        "authInboundChannel cannot be null!!");
                } else {
                    xmlWriter.writeCharacters(localAuthInboundChannel);
                }

                xmlWriter.writeEndElement();
            }

            if (localAuthRequestedDateTracker) {
                namespace = "";
                writeStartElement(null, namespace, "authRequestedDate",
                    xmlWriter);

                if (localAuthRequestedDate == null) {
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException(
                        "authRequestedDate cannot be null!!");
                } else {
                    xmlWriter.writeCharacters(localAuthRequestedDate);
                }

                xmlWriter.writeEndElement();
            }

            if (localAppCreateUserIdTracker) {
                namespace = "";
                writeStartElement(null, namespace, "appCreateUserId", xmlWriter);

                if (localAppCreateUserId == null) {
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException(
                        "appCreateUserId cannot be null!!");
                } else {
                    xmlWriter.writeCharacters(localAppCreateUserId);
                }

                xmlWriter.writeEndElement();
            }

            if (localAppMaintDatetimeTracker) {
                namespace = "";
                writeStartElement(null, namespace, "appMaintDatetime",
                    xmlWriter);

                if (localAppMaintDatetime == null) {
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException(
                        "appMaintDatetime cannot be null!!");
                } else {
                    xmlWriter.writeCharacters(localAppMaintDatetime);
                }

                xmlWriter.writeEndElement();
            }
            
            if (localAppCreateDatetimeTracker) {
                namespace = "";
                writeStartElement(null, namespace, "appCreateDatetime",
                    xmlWriter);

                if (localAppCreateDatetime == null) {
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException(
                        "appCreateDatetime cannot be null!!");
                } else {
                    xmlWriter.writeCharacters(localAppCreateDatetime);
                }

                xmlWriter.writeEndElement();
            }

            if (localPriorAuthSubmissionStatusCodeTracker) {
                namespace = "";
                writeStartElement(null, namespace,
                    "priorAuthSubmissionStatusCode", xmlWriter);

                if (localPriorAuthSubmissionStatusCode == null) {
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException(
                        "priorAuthSubmissionStatusCode cannot be null!!");
                } else {
                    xmlWriter.writeCharacters(localPriorAuthSubmissionStatusCode);
                }

                xmlWriter.writeEndElement();
            }

            if (localPriorAuthPriorityCodeTracker) {
                namespace = "";
                writeStartElement(null, namespace, "priorAuthPriorityCode",
                    xmlWriter);

                if (localPriorAuthPriorityCode == null) {
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException(
                        "priorAuthPriorityCode cannot be null!!");
                } else {
                    xmlWriter.writeCharacters(localPriorAuthPriorityCode);
                }

                xmlWriter.writeEndElement();
            }

            if (localPriorAuthStatusCodeTracker) {
                namespace = "";
                writeStartElement(null, namespace, "priorAuthStatusCode",
                    xmlWriter);

                if (localPriorAuthStatusCode == null) {
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException(
                        "priorAuthStatusCode cannot be null!!");
                } else {
                    xmlWriter.writeCharacters(localPriorAuthStatusCode);
                }

                xmlWriter.writeEndElement();
            }
            
            if (localPriorAuthStatusDescTracker) {
                namespace = "";
                writeStartElement(null, namespace, "PriorAuthStatusDesc", xmlWriter);

                if (localPriorAuthStatusDesc==null) {
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException("PriorAuthStatusDesc cannot be null!!");
                                 
                } else {
                    xmlWriter.writeCharacters(localPriorAuthStatusDesc);
                }
                   
                 xmlWriter.writeEndElement();
            } 

            if (localPriorAuthNumberTracker) {
                namespace = "";
                writeStartElement(null, namespace, "priorAuthNumber", xmlWriter);

                if (localPriorAuthNumber == null) {
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException(
                        "priorAuthNumber cannot be null!!");
                } else {
                    xmlWriter.writeCharacters(localPriorAuthNumber);
                }

                xmlWriter.writeEndElement();
            }

            if (localAuthNumberSearchedTracker) {
                namespace = "";
                writeStartElement(null, namespace, "authNumberSearched",
                    xmlWriter);

                if (localAuthNumberSearched == null) {
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException(
                        "authNumberSearched cannot be null!!");
                } else {
                    xmlWriter.writeCharacters(localAuthNumberSearched);
                }

                xmlWriter.writeEndElement();
            }

            if (localIdCardNumberTracker) {
                namespace = "";
                writeStartElement(null, namespace, "idCardNumber", xmlWriter);

                if (localIdCardNumber == null) {
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException(
                        "idCardNumber cannot be null!!");
                } else {
                    xmlWriter.writeCharacters(localIdCardNumber);
                }

                xmlWriter.writeEndElement();
            }

            if (localMemberNumberTracker) {
                namespace = "";
                writeStartElement(null, namespace, "memberNumber", xmlWriter);

                if (localMemberNumber == null) {
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException(
                        "memberNumber cannot be null!!");
                } else {
                    xmlWriter.writeCharacters(localMemberNumber);
                }

                xmlWriter.writeEndElement();
            }

            if (localHealthPlanIdTracker) {
                namespace = "";
                writeStartElement(null, namespace, "healthPlanId", xmlWriter);

                if (localHealthPlanId == null) {
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException(
                        "healthPlanId cannot be null!!");
                } else {
                    xmlWriter.writeCharacters(localHealthPlanId);
                }

                xmlWriter.writeEndElement();
            }

            if (localHealthPlanGroupIdTracker) {
                namespace = "";
                writeStartElement(null, namespace, "healthPlanGroupId",
                    xmlWriter);

                if (localHealthPlanGroupId == null) {
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException(
                        "healthPlanGroupId cannot be null!!");
                } else {
                    xmlWriter.writeCharacters(localHealthPlanGroupId);
                }

                xmlWriter.writeEndElement();
            }

            namespace = "";
            writeStartElement(null, namespace, "extractPerformedQuantity",
                xmlWriter);

            if (localExtractPerformedQuantity == java.lang.Short.MIN_VALUE) {
                throw new org.apache.axis2.databinding.ADBException(
                    "extractPerformedQuantity cannot be null!!");
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        localExtractPerformedQuantity));
            }

            xmlWriter.writeEndElement();

            if (localPriorAuthBeginServiceDateTracker) {
                namespace = "";
                writeStartElement(null, namespace, "priorAuthBeginServiceDate",
                    xmlWriter);

                if (localPriorAuthBeginServiceDate == null) {
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException(
                        "priorAuthBeginServiceDate cannot be null!!");
                } else {
                    xmlWriter.writeCharacters(localPriorAuthBeginServiceDate);
                }

                xmlWriter.writeEndElement();
            }

            if (localPriorAuthEndServiceDateTracker) {
                namespace = "";
                writeStartElement(null, namespace, "priorAuthEndServiceDate",
                    xmlWriter);

                if (localPriorAuthEndServiceDate == null) {
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException(
                        "priorAuthEndServiceDate cannot be null!!");
                } else {
                    xmlWriter.writeCharacters(localPriorAuthEndServiceDate);
                }

                xmlWriter.writeEndElement();
            }

            if (localMasterPatientIdTracker) {
                namespace = "";
                writeStartElement(null, namespace, "masterPatientId", xmlWriter);

                if (localMasterPatientId == null) {
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException(
                        "masterPatientId cannot be null!!");
                } else {
                    xmlWriter.writeCharacters(localMasterPatientId);
                }

                xmlWriter.writeEndElement();
            }

            if (localIcdRevisionNumberTracker) {
                namespace = "";
                writeStartElement(null, namespace, "icdRevisionNumber",
                    xmlWriter);

                if (localIcdRevisionNumber == null) {
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException(
                        "icdRevisionNumber cannot be null!!");
                } else {
                    xmlWriter.writeCharacters(localIcdRevisionNumber);
                }

                xmlWriter.writeEndElement();
            }

            namespace = "";
            writeStartElement(null, namespace, "priorAuthHeaderKey", xmlWriter);

            if (localPriorAuthHeaderKey == java.lang.Integer.MIN_VALUE) {
                throw new org.apache.axis2.databinding.ADBException(
                    "priorAuthHeaderKey cannot be null!!");
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        localPriorAuthHeaderKey));
            }

            xmlWriter.writeEndElement();

            if (localPatientGenderRelationshipCodeTracker) {
                namespace = "";
                writeStartElement(null, namespace,
                    "patientGenderRelationshipCode", xmlWriter);

                if (localPatientGenderRelationshipCode == null) {
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException(
                        "patientGenderRelationshipCode cannot be null!!");
                } else {
                    xmlWriter.writeCharacters(localPatientGenderRelationshipCode);
                }

                xmlWriter.writeEndElement();
            }

            if (localPriorAuthOriginalStatusDatetimeTracker) {
                namespace = "";
                writeStartElement(null, namespace,
                    "priorAuthOriginalStatusDatetime", xmlWriter);

                if (localPriorAuthOriginalStatusDatetime == null) {
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException(
                        "priorAuthOriginalStatusDatetime cannot be null!!");
                } else {
                    xmlWriter.writeCharacters(localPriorAuthOriginalStatusDatetime);
                }

                xmlWriter.writeEndElement();
            }

            if (localPriorAuthCurrentStatusDatetimeTracker) {
                namespace = "";
                writeStartElement(null, namespace,
                    "priorAuthCurrentStatusDatetime", xmlWriter);

                if (localPriorAuthCurrentStatusDatetime == null) {
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException(
                        "priorAuthCurrentStatusDatetime cannot be null!!");
                } else {
                    xmlWriter.writeCharacters(localPriorAuthCurrentStatusDatetime);
                }

                xmlWriter.writeEndElement();
            }

            if (localPriorAuthCurrentStatusUserIdTracker) {
                namespace = "";
                writeStartElement(null, namespace,
                    "priorAuthCurrentStatusUserId", xmlWriter);

                if (localPriorAuthCurrentStatusUserId == null) {
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException(
                        "priorAuthCurrentStatusUserId cannot be null!!");
                } else {
                    xmlWriter.writeCharacters(localPriorAuthCurrentStatusUserId);
                }

                xmlWriter.writeEndElement();
            }

            if (localPriorAuthOriginalStatusUserIdTracker) {
                namespace = "";
                writeStartElement(null, namespace,
                    "priorAuthOriginalStatusUserId", xmlWriter);

                if (localPriorAuthOriginalStatusUserId == null) {
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException(
                        "priorAuthOriginalStatusUserId cannot be null!!");
                } else {
                    xmlWriter.writeCharacters(localPriorAuthOriginalStatusUserId);
                }

                xmlWriter.writeEndElement();
            }

            if (localPriorAuthContactDatetimeTracker){
                namespace = "";
                writeStartElement(null, namespace, "priorAuthContactDatetime", xmlWriter);

                if (localPriorAuthContactDatetime==null){
               	 
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException("priorAuthContactDatetime cannot be null!!");
                }else{
                    xmlWriter.writeCharacters(localPriorAuthContactDatetime);
                }

                xmlWriter.writeEndElement();
            }

            if (localPriorAuthDueDatetimeTracker){
                namespace = "";
                writeStartElement(null, namespace, "priorAuthDueDatetime", xmlWriter);

                if (localPriorAuthDueDatetime==null){
               	 
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException("priorAuthDueDatetime cannot be null!!");
                }else{
                    xmlWriter.writeCharacters(localPriorAuthDueDatetime);
                }

                xmlWriter.writeEndElement();
            }

            if (localPriorAuthWorkedByTracker){
                namespace = "";
                writeStartElement(null, namespace, "priorAuthWorkedBy", xmlWriter);

                if (localPriorAuthWorkedBy==null){
               	 
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException("priorAuthWorkedBy cannot be null!!");
                }else{
                    xmlWriter.writeCharacters(localPriorAuthWorkedBy);
                }

                xmlWriter.writeEndElement();
            }

            if (localPriorAuthWithdrawnReasonCodeTracker){
                namespace = "";
                writeStartElement(null, namespace, "priorAuthWithdrawnReasonCode", xmlWriter);

                if (localPriorAuthWithdrawnReasonCode==null){
               	 
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException("priorAuthWithdrawnReasonCode cannot be null!!");
                }else{
                    xmlWriter.writeCharacters(localPriorAuthWithdrawnReasonCode);
                }

                xmlWriter.writeEndElement();
            }

            if (localPriorAuthWithdrawnReasonDescTracker){
                namespace = "";
                writeStartElement(null, namespace, "priorAuthWithdrawnReasonDesc", xmlWriter);

                if (localPriorAuthWithdrawnReasonDesc==null){
               	 
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException("priorAuthWithdrawnReasonDesc cannot be null!!");
                }else{
                    xmlWriter.writeCharacters(localPriorAuthWithdrawnReasonDesc);
                }

                xmlWriter.writeEndElement();
            }

            if (localPriorAuthBusinessSectorCodeTracker){
                namespace = "";
                writeStartElement(null, namespace, "priorAuthBusinessSectorCode", xmlWriter);

                if (localPriorAuthBusinessSectorCode==null){
               	 
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException("priorAuthBusinessSectorCode cannot be null!!");
                }else{
                    xmlWriter.writeCharacters(localPriorAuthBusinessSectorCode);
                }

                xmlWriter.writeEndElement();
            }

            if (localPriorAuthBusinessSectorDescriptionTracker){
                namespace = "";
                writeStartElement(null, namespace, "priorAuthBusinessSectorDescription", xmlWriter);

                if (localPriorAuthBusinessSectorDescription==null){
               	 
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException("priorAuthBusinessSectorDescription cannot be null!!");
                }else{
                    xmlWriter.writeCharacters(localPriorAuthBusinessSectorDescription);
                }

                xmlWriter.writeEndElement();
            }

            if (localPriorAuthBusinessSegmentCodeTracker){
                namespace = "";
                writeStartElement(null, namespace, "priorAuthBusinessSegmentCode", xmlWriter);

                if (localPriorAuthBusinessSegmentCode==null){
               	 
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException("priorAuthBusinessSegmentCode cannot be null!!");
                }else{
                    xmlWriter.writeCharacters(localPriorAuthBusinessSegmentCode);
                }

                xmlWriter.writeEndElement();
            }

            if (localPriorAuthBusinessSegmentDescriptionTracker){
                namespace = "";
                writeStartElement(null, namespace, "priorAuthBusinessSegmentDescription", xmlWriter);

                if (localPriorAuthBusinessSegmentDescription==null){
               	 
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException("priorAuthBusinessSegmentDescription cannot be null!!");
                }else{
                    xmlWriter.writeCharacters(localPriorAuthBusinessSegmentDescription);
                }

                xmlWriter.writeEndElement();
            }

            if (localPriorAuthPatientFirstNameTracker){
                namespace = "";
                writeStartElement(null, namespace, "priorAuthPatientFirstName", xmlWriter);

                if (localPriorAuthPatientFirstName==null){
               	 
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException("priorAuthPatientFirstName cannot be null!!");
                }else{
                    xmlWriter.writeCharacters(localPriorAuthPatientFirstName);
                }

                xmlWriter.writeEndElement();
            }

            if (localPriorAuthPatientMiddleNameTracker){
                namespace = "";
                writeStartElement(null, namespace, "priorAuthPatientMiddleName", xmlWriter);

                if (localPriorAuthPatientMiddleName==null){
               	 
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException("priorAuthPatientMiddleName cannot be null!!");
                }else{
                    xmlWriter.writeCharacters(localPriorAuthPatientMiddleName);
                }

                xmlWriter.writeEndElement();
            }

            if (localPriorAuthPatientLastNameTracker){
                namespace = "";
                writeStartElement(null, namespace, "priorAuthPatientLastName", xmlWriter);

                if (localPriorAuthPatientLastName==null){
               	 
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException("priorAuthPatientLastName cannot be null!!");
                }else{
                    xmlWriter.writeCharacters(localPriorAuthPatientLastName);
                }

                xmlWriter.writeEndElement();
            }

            if (localPriorAuthPatientSuffixNameTracker){
                namespace = "";
                writeStartElement(null, namespace, "priorAuthPatientSuffixName", xmlWriter);

                if (localPriorAuthPatientSuffixName==null){
               	 
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException("priorAuthPatientSuffixName cannot be null!!");
                }else{
                    xmlWriter.writeCharacters(localPriorAuthPatientSuffixName);
                }

                xmlWriter.writeEndElement();
            }

            if (localPriorAuthPatientBirthDateTracker){
                namespace = "";
                writeStartElement(null, namespace, "priorAuthPatientBirthDate", xmlWriter);

                if (localPriorAuthPatientBirthDate==null){
               	 
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException("priorAuthPatientBirthDate cannot be null!!");
                }else{
                    xmlWriter.writeCharacters(localPriorAuthPatientBirthDate);
                }

                xmlWriter.writeEndElement();
            }

            if (localPriorAuthPatientGenderCodeTracker){
                namespace = "";
                writeStartElement(null, namespace, "priorAuthPatientGenderCode", xmlWriter);

                if (localPriorAuthPatientGenderCode==null){
               	 
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException("priorAuthPatientGenderCode cannot be null!!");
                }else{
                    xmlWriter.writeCharacters(localPriorAuthPatientGenderCode);
                }

                xmlWriter.writeEndElement();
            }

            xmlWriter.writeEndElement();
        }

        private static java.lang.String generatePrefix(
            java.lang.String namespace) {
            if (namespace.equals("http://services.lbm.avalon.com/")) {
                return "ns1";
            }

            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix,
            java.lang.String namespace, java.lang.String localPart,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

            if (writerPrefix != null) {
                xmlWriter.writeStartElement(namespace, localPart);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }

        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,
            java.lang.String namespace, java.lang.String attName,
            java.lang.String attValue,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }

            xmlWriter.writeAttribute(namespace, attName, attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,
            java.lang.String attName, java.lang.String attValue,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attValue);
            }
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeQNameAttribute(java.lang.String namespace,
            java.lang.String attName, javax.xml.namespace.QName qname,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String attributeNamespace = qname.getNamespaceURI();
            java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);

            if (attributePrefix == null) {
                attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
            }

            java.lang.String attributeValue;

            if (attributePrefix.trim().length() > 0) {
                attributeValue = attributePrefix + ":" + qname.getLocalPart();
            } else {
                attributeValue = qname.getLocalPart();
            }

            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attributeValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attributeValue);
            }
        }

        /**
         *  method to handle Qnames
         */
        private void writeQName(javax.xml.namespace.QName qname,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();

            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);

                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix, namespaceURI);
                }

                if (prefix.trim().length() > 0) {
                    xmlWriter.writeCharacters(prefix + ":" +
                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            qname));
                }
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }

                    namespaceURI = qnames[i].getNamespaceURI();

                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);

                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix, namespaceURI);
                        }

                        if (prefix.trim().length() > 0) {
                            stringToWrite.append(prefix).append(":")
                                         .append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                    qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                    qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                qnames[i]));
                    }
                }

                xmlWriter.writeCharacters(stringToWrite.toString());
            }
        }

        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(
            javax.xml.stream.XMLStreamWriter xmlWriter,
            java.lang.String namespace)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);

            if (prefix == null) {
                prefix = generatePrefix(namespace);

                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();

                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);

                    if ((uri == null) || (uri.length() == 0)) {
                        break;
                    }

                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }

                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }

            return prefix;
        }

        /**
         * databinding method to get an XML representation of this object
         *
         */
        public javax.xml.stream.XMLStreamReader getPullParser(
            javax.xml.namespace.QName qName)
            throws org.apache.axis2.databinding.ADBException {
            java.util.ArrayList elementList = new java.util.ArrayList();
            java.util.ArrayList attribList = new java.util.ArrayList();

            if (localAuthInboundChannelTracker) {
                elementList.add(new javax.xml.namespace.QName("",
                        "authInboundChannel"));

                if (localAuthInboundChannel != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            localAuthInboundChannel));
                } else {
                    throw new org.apache.axis2.databinding.ADBException(
                        "authInboundChannel cannot be null!!");
                }
            }

            if (localAuthRequestedDateTracker) {
                elementList.add(new javax.xml.namespace.QName("",
                        "authRequestedDate"));

                if (localAuthRequestedDate != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            localAuthRequestedDate));
                } else {
                    throw new org.apache.axis2.databinding.ADBException(
                        "authRequestedDate cannot be null!!");
                }
            }

            if (localAppCreateUserIdTracker) {
                elementList.add(new javax.xml.namespace.QName("",
                        "appCreateUserId"));

                if (localAppCreateUserId != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            localAppCreateUserId));
                } else {
                    throw new org.apache.axis2.databinding.ADBException(
                        "appCreateUserId cannot be null!!");
                }
            }

            if (localAppMaintDatetimeTracker) {
                elementList.add(new javax.xml.namespace.QName("",
                        "appMaintDatetime"));

                if (localAppMaintDatetime != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            localAppMaintDatetime));
                } else {
                    throw new org.apache.axis2.databinding.ADBException(
                        "appMaintDatetime cannot be null!!");
                }
            }
            
            if (localAppCreateDatetimeTracker) {
                elementList.add(new javax.xml.namespace.QName("",
                        "appCreateDatetime"));

                if (localAppCreateDatetime != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            localAppCreateDatetime));
                } else {
                    throw new org.apache.axis2.databinding.ADBException(
                        "appCreateDatetime cannot be null!!");
                }
            }

            if (localPriorAuthSubmissionStatusCodeTracker) {
                elementList.add(new javax.xml.namespace.QName("",
                        "priorAuthSubmissionStatusCode"));

                if (localPriorAuthSubmissionStatusCode != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            localPriorAuthSubmissionStatusCode));
                } else {
                    throw new org.apache.axis2.databinding.ADBException(
                        "priorAuthSubmissionStatusCode cannot be null!!");
                }
            }

            if (localPriorAuthPriorityCodeTracker) {
                elementList.add(new javax.xml.namespace.QName("",
                        "priorAuthPriorityCode"));

                if (localPriorAuthPriorityCode != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            localPriorAuthPriorityCode));
                } else {
                    throw new org.apache.axis2.databinding.ADBException(
                        "priorAuthPriorityCode cannot be null!!");
                }
            }

            if (localPriorAuthStatusCodeTracker) {
                elementList.add(new javax.xml.namespace.QName("",
                        "priorAuthStatusCode"));

                if (localPriorAuthStatusCode != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            localPriorAuthStatusCode));
                } else {
                    throw new org.apache.axis2.databinding.ADBException(
                        "priorAuthStatusCode cannot be null!!");
                }
            }
            
            if (localPriorAuthStatusDescTracker){
                elementList.add(new javax.xml.namespace.QName("","PriorAuthStatusDesc"));
         
                if (localPriorAuthStatusDesc != null){
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPriorAuthStatusDesc));
                } else {
                   throw new org.apache.axis2.databinding.ADBException("PriorAuthStatusDesc cannot be null!!");
                }
            } 

            if (localPriorAuthNumberTracker) {
                elementList.add(new javax.xml.namespace.QName("",
                        "priorAuthNumber"));

                if (localPriorAuthNumber != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            localPriorAuthNumber));
                } else {
                    throw new org.apache.axis2.databinding.ADBException(
                        "priorAuthNumber cannot be null!!");
                }
            }

            if (localAuthNumberSearchedTracker) {
                elementList.add(new javax.xml.namespace.QName("",
                        "authNumberSearched"));

                if (localAuthNumberSearched != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            localAuthNumberSearched));
                } else {
                    throw new org.apache.axis2.databinding.ADBException(
                        "authNumberSearched cannot be null!!");
                }
            }

            if (localIdCardNumberTracker) {
                elementList.add(new javax.xml.namespace.QName("", "idCardNumber"));

                if (localIdCardNumber != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            localIdCardNumber));
                } else {
                    throw new org.apache.axis2.databinding.ADBException(
                        "idCardNumber cannot be null!!");
                }
            }

            if (localMemberNumberTracker) {
                elementList.add(new javax.xml.namespace.QName("", "memberNumber"));

                if (localMemberNumber != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            localMemberNumber));
                } else {
                    throw new org.apache.axis2.databinding.ADBException(
                        "memberNumber cannot be null!!");
                }
            }

            if (localHealthPlanIdTracker) {
                elementList.add(new javax.xml.namespace.QName("", "healthPlanId"));

                if (localHealthPlanId != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            localHealthPlanId));
                } else {
                    throw new org.apache.axis2.databinding.ADBException(
                        "healthPlanId cannot be null!!");
                }
            }

            if (localHealthPlanGroupIdTracker) {
                elementList.add(new javax.xml.namespace.QName("",
                        "healthPlanGroupId"));

                if (localHealthPlanGroupId != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            localHealthPlanGroupId));
                } else {
                    throw new org.apache.axis2.databinding.ADBException(
                        "healthPlanGroupId cannot be null!!");
                }
            }

            elementList.add(new javax.xml.namespace.QName("",
                    "extractPerformedQuantity"));

            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                    localExtractPerformedQuantity));

            if (localPriorAuthBeginServiceDateTracker) {
                elementList.add(new javax.xml.namespace.QName("",
                        "priorAuthBeginServiceDate"));

                if (localPriorAuthBeginServiceDate != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            localPriorAuthBeginServiceDate));
                } else {
                    throw new org.apache.axis2.databinding.ADBException(
                        "priorAuthBeginServiceDate cannot be null!!");
                }
            }

            if (localPriorAuthEndServiceDateTracker) {
                elementList.add(new javax.xml.namespace.QName("",
                        "priorAuthEndServiceDate"));

                if (localPriorAuthEndServiceDate != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            localPriorAuthEndServiceDate));
                } else {
                    throw new org.apache.axis2.databinding.ADBException(
                        "priorAuthEndServiceDate cannot be null!!");
                }
            }

            if (localMasterPatientIdTracker) {
                elementList.add(new javax.xml.namespace.QName("",
                        "masterPatientId"));

                if (localMasterPatientId != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            localMasterPatientId));
                } else {
                    throw new org.apache.axis2.databinding.ADBException(
                        "masterPatientId cannot be null!!");
                }
            }

            if (localIcdRevisionNumberTracker) {
                elementList.add(new javax.xml.namespace.QName("",
                        "icdRevisionNumber"));

                if (localIcdRevisionNumber != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            localIcdRevisionNumber));
                } else {
                    throw new org.apache.axis2.databinding.ADBException(
                        "icdRevisionNumber cannot be null!!");
                }
            }

            elementList.add(new javax.xml.namespace.QName("",
                    "priorAuthHeaderKey"));

            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                    localPriorAuthHeaderKey));

            if (localPatientGenderRelationshipCodeTracker) {
                elementList.add(new javax.xml.namespace.QName("",
                        "patientGenderRelationshipCode"));

                if (localPatientGenderRelationshipCode != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            localPatientGenderRelationshipCode));
                } else {
                    throw new org.apache.axis2.databinding.ADBException(
                        "patientGenderRelationshipCode cannot be null!!");
                }
            }

            if (localPriorAuthOriginalStatusDatetimeTracker) {
                elementList.add(new javax.xml.namespace.QName("",
                        "priorAuthOriginalStatusDatetime"));

                if (localPriorAuthOriginalStatusDatetime != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            localPriorAuthOriginalStatusDatetime));
                } else {
                    throw new org.apache.axis2.databinding.ADBException(
                        "priorAuthOriginalStatusDatetime cannot be null!!");
                }
            }

            if (localPriorAuthCurrentStatusDatetimeTracker) {
                elementList.add(new javax.xml.namespace.QName("",
                        "priorAuthCurrentStatusDatetime"));

                if (localPriorAuthCurrentStatusDatetime != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            localPriorAuthCurrentStatusDatetime));
                } else {
                    throw new org.apache.axis2.databinding.ADBException(
                        "priorAuthCurrentStatusDatetime cannot be null!!");
                }
            }

            if (localPriorAuthCurrentStatusUserIdTracker) {
                elementList.add(new javax.xml.namespace.QName("",
                        "priorAuthCurrentStatusUserId"));

                if (localPriorAuthCurrentStatusUserId != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            localPriorAuthCurrentStatusUserId));
                } else {
                    throw new org.apache.axis2.databinding.ADBException(
                        "priorAuthCurrentStatusUserId cannot be null!!");
                }
            }

            if (localPriorAuthOriginalStatusUserIdTracker) {
                elementList.add(new javax.xml.namespace.QName("",
                        "priorAuthOriginalStatusUserId"));

                if (localPriorAuthOriginalStatusUserId != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            localPriorAuthOriginalStatusUserId));
                } else {
                    throw new org.apache.axis2.databinding.ADBException(
                        "priorAuthOriginalStatusUserId cannot be null!!");
                }
            }

            if (localPriorAuthContactDatetimeTracker) {
                elementList.add(new javax.xml.namespace.QName("", "PriorAuthContactDatetime"));

                if (localPriorAuthContactDatetime != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPriorAuthContactDatetime));
                } else {
                    throw new org.apache.axis2.databinding.ADBException("PriorAuthContactDatetime cannot be null!!");
                }
            }

            if (localPriorAuthDueDatetimeTracker) {
                elementList.add(new javax.xml.namespace.QName("", "PriorAuthDueDatetime"));

                if (localPriorAuthDueDatetime != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPriorAuthDueDatetime));
                } else {
                    throw new org.apache.axis2.databinding.ADBException("PriorAuthDueDatetime cannot be null!!");
                }
            }

            if (localPriorAuthWorkedByTracker) {
                elementList.add(new javax.xml.namespace.QName("", "PriorAuthWorkedBy"));

                if (localPriorAuthWorkedBy != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPriorAuthWorkedBy));
                } else {
                    throw new org.apache.axis2.databinding.ADBException("PriorAuthWorkedBy cannot be null!!");
                }
            }

            if (localPriorAuthWithdrawnReasonCodeTracker) {
                elementList.add(new javax.xml.namespace.QName("", "PriorAuthWithdrawnReasonCode"));

                if (localPriorAuthWorkedBy != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPriorAuthWithdrawnReasonCode));
                } else {
                    throw new org.apache.axis2.databinding.ADBException("PriorAuthWithdrawnReasonCode cannot be null!!");
                }
            }

            if (localPriorAuthWithdrawnReasonDescTracker) {
                elementList.add(new javax.xml.namespace.QName("", "PriorAuthWithdrawnReasonDesc"));

                if (localPriorAuthWorkedBy != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPriorAuthWithdrawnReasonDesc));
                } else {
                    throw new org.apache.axis2.databinding.ADBException("PriorAuthWithdrawnReasonDesc cannot be null!!");
                }
            }

            if (localPriorAuthBusinessSectorCodeTracker) {
                elementList.add(new javax.xml.namespace.QName("", "PriorAuthBusinessSectorCode"));

                if (localPriorAuthWorkedBy != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPriorAuthBusinessSectorCode));
                } else {
                    throw new org.apache.axis2.databinding.ADBException("PriorAuthBusinessSectorCode cannot be null!!");
                }
            }

            if (localPriorAuthBusinessSectorDescriptionTracker) {
                elementList.add(new javax.xml.namespace.QName("", "PriorAuthBusinessSectorDescription"));

                if (localPriorAuthWorkedBy != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPriorAuthBusinessSectorDescription));
                } else {
                    throw new org.apache.axis2.databinding.ADBException("PriorAuthBusinessSectorDescription cannot be null!!");
                }
            }

            if (localPriorAuthBusinessSegmentCodeTracker) {
                elementList.add(new javax.xml.namespace.QName("", "PriorAuthBusinessSegmentCode"));

                if (localPriorAuthWorkedBy != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPriorAuthBusinessSegmentCode));
                } else {
                    throw new org.apache.axis2.databinding.ADBException("PriorAuthBusinessSegmentCode cannot be null!!");
                }
            }

            if (localPriorAuthBusinessSegmentDescriptionTracker) {
                elementList.add(new javax.xml.namespace.QName("", "PriorAuthBusinessSegmentDescription"));

                if (localPriorAuthWorkedBy != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPriorAuthBusinessSegmentDescription));
                } else {
                    throw new org.apache.axis2.databinding.ADBException("PriorAuthBusinessSegmentDescription cannot be null!!");
                }
            }

            if (localPriorAuthPatientFirstNameTracker) {
                elementList.add(new javax.xml.namespace.QName("", "PriorAuthPatientFirstName"));

                if (localPriorAuthWorkedBy != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPriorAuthPatientFirstName));
                } else {
                    throw new org.apache.axis2.databinding.ADBException("PriorAuthPatientFirstName cannot be null!!");
                }
            }

            if (localPriorAuthPatientMiddleNameTracker) {
                elementList.add(new javax.xml.namespace.QName("", "PriorAuthPatientMiddleName"));

                if (localPriorAuthWorkedBy != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPriorAuthPatientMiddleName));
                } else {
                    throw new org.apache.axis2.databinding.ADBException("PriorAuthPatientMiddleName cannot be null!!");
                }
            }

            if (localPriorAuthPatientLastNameTracker) {
                elementList.add(new javax.xml.namespace.QName("", "PriorAuthPatientLastName"));

                if (localPriorAuthWorkedBy != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPriorAuthPatientLastName));
                } else {
                    throw new org.apache.axis2.databinding.ADBException("PriorAuthPatientLastName cannot be null!!");
                }
            }

            if (localPriorAuthPatientSuffixNameTracker) {
                elementList.add(new javax.xml.namespace.QName("", "PriorAuthPatientSuffixName"));

                if (localPriorAuthWorkedBy != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPriorAuthPatientSuffixName));
                } else {
                    throw new org.apache.axis2.databinding.ADBException("PriorAuthPatientSuffixName cannot be null!!");
                }
            }

            if (localPriorAuthPatientBirthDateTracker) {
                elementList.add(new javax.xml.namespace.QName("", "PriorAuthPatientBirthDate"));

                if (localPriorAuthWorkedBy != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPriorAuthPatientBirthDate));
                } else {
                    throw new org.apache.axis2.databinding.ADBException("PriorAuthPatientBirthDate cannot be null!!");
                }
            }

            if (localPriorAuthPatientGenderCodeTracker) {
                elementList.add(new javax.xml.namespace.QName("", "PriorAuthPatientGenderCode"));

                if (localPriorAuthWorkedBy != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPriorAuthPatientGenderCode));
                } else {
                    throw new org.apache.axis2.databinding.ADBException("PriorAuthPatientGenderCode cannot be null!!");
                }
            }

            return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
        }

        /**
         *  Factory class that keeps the parse method
         */
        public static class Factory {
            /**
             * static method to create the object
             * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
             *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
             * Postcondition: If this object is an element, the reader is positioned at its end element
             *                If this object is a complex type, the reader is positioned at the end element of its outer element
             */
            public static PriorAuthHeaderDTO parse(
                javax.xml.stream.XMLStreamReader reader)
                throws java.lang.Exception {
                PriorAuthHeaderDTO object = new PriorAuthHeaderDTO();

                int event;
                java.lang.String nillableValue = null;
                java.lang.String prefix = "";
                java.lang.String namespaceuri = "";

                try {
                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.getAttributeValue(
                                "http://www.w3.org/2001/XMLSchema-instance",
                                "type") != null) {
                        java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                "type");

                        if (fullTypeName != null) {
                            java.lang.String nsPrefix = null;

                            if (fullTypeName.indexOf(":") > -1) {
                                nsPrefix = fullTypeName.substring(0,
                                        fullTypeName.indexOf(":"));
                            }

                            nsPrefix = (nsPrefix == null) ? "" : nsPrefix;

                            java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(
                                        ":") + 1);

                            if (!"priorAuthHeaderDTO".equals(type)) {
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext()
                                                               .getNamespaceURI(nsPrefix);

                                return (PriorAuthHeaderDTO) ExtensionMapper.getTypeObject(nsUri,
                                    type, reader);
                            }
                        }
                    }

                    // Note all attributes that were handled. Used to differ normal attributes
                    // from anyAttributes.
                    java.util.Vector handledAttributes = new java.util.Vector();

                    reader.next();

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                            new javax.xml.namespace.QName("",
                                "authInboundChannel").equals(reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                "nil");

                        if ("true".equals(nillableValue) ||
                                "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "authInboundChannel" +
                                "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setAuthInboundChannel(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                            new javax.xml.namespace.QName("",
                                "authRequestedDate").equals(reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                "nil");

                        if ("true".equals(nillableValue) ||
                                "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "authRequestedDate" +
                                "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setAuthRequestedDate(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                            new javax.xml.namespace.QName("", "appCreateUserId").equals(
                                reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                "nil");

                        if ("true".equals(nillableValue) ||
                                "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "appCreateUserId" +
                                "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setAppCreateUserId(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                            new javax.xml.namespace.QName("",
                                "appMaintDatetime").equals(reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                "nil");

                        if ("true".equals(nillableValue) ||
                                "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "appMaintDatetime" +
                                "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setAppMaintDatetime(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }
                     
                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                            new javax.xml.namespace.QName("",
                                "appCreateDatetime").equals(reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                "nil");

                        if ("true".equals(nillableValue) ||
                                "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "appCreateDatetime" +
                                "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setAppCreateDatetime(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                            new javax.xml.namespace.QName("",
                                "priorAuthSubmissionStatusCode").equals(
                                reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                "nil");

                        if ("true".equals(nillableValue) ||
                                "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " +
                                "priorAuthSubmissionStatusCode" +
                                "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setPriorAuthSubmissionStatusCode(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                            new javax.xml.namespace.QName("",
                                "priorAuthPriorityCode").equals(
                                reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                "nil");

                        if ("true".equals(nillableValue) ||
                                "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "priorAuthPriorityCode" +
                                "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setPriorAuthPriorityCode(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                            new javax.xml.namespace.QName("",
                                "priorAuthStatusCode").equals(reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                "nil");

                        if ("true".equals(nillableValue) ||
                                "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "priorAuthStatusCode" +
                                "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setPriorAuthStatusCode(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }
                    
                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

                    if (reader.isStartElement() && new javax.xml.namespace.QName("","priorAuthStatusDesc").equals(reader.getName())){

                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                        if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                            throw new org.apache.axis2.databinding.ADBException("The element: " + "PriorAuthStatusDesc" + " cannot be null");
                        }

                        java.lang.String content = reader.getElementText();
                        
                        object.setPriorAuthStatusDesc(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                  
                        reader.next();
                    
                    }  // End of if for expected property start element
                    else {
                        
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                            new javax.xml.namespace.QName("", "priorAuthNumber").equals(
                                reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                "nil");

                        if ("true".equals(nillableValue) ||
                                "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "priorAuthNumber" +
                                "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setPriorAuthNumber(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                            new javax.xml.namespace.QName("",
                                "authNumberSearched").equals(reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                "nil");

                        if ("true".equals(nillableValue) ||
                                "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "authNumberSearched" +
                                "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setAuthNumberSearched(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                            new javax.xml.namespace.QName("", "idCardNumber").equals(
                                reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                "nil");

                        if ("true".equals(nillableValue) ||
                                "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "idCardNumber" +
                                "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setIdCardNumber(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                            new javax.xml.namespace.QName("", "memberNumber").equals(
                                reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                "nil");

                        if ("true".equals(nillableValue) ||
                                "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "memberNumber" +
                                "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setMemberNumber(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                            new javax.xml.namespace.QName("", "healthPlanId").equals(
                                reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                "nil");

                        if ("true".equals(nillableValue) ||
                                "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "healthPlanId" +
                                "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setHealthPlanId(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                            new javax.xml.namespace.QName("",
                                "healthPlanGroupId").equals(reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                "nil");

                        if ("true".equals(nillableValue) ||
                                "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "healthPlanGroupId" +
                                "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setHealthPlanGroupId(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                            new javax.xml.namespace.QName("",
                                "extractPerformedQuantity").equals(
                                reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                "nil");

                        if ("true".equals(nillableValue) ||
                                "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "extractPerformedQuantity" +
                                "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setExtractPerformedQuantity(org.apache.axis2.databinding.utils.ConverterUtil.convertToShort(
                                content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                        // A start element we are not expecting indicates an invalid parameter was passed
                        throw new org.apache.axis2.databinding.ADBException(
                            "Unexpected subelement " + reader.getName());
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                            new javax.xml.namespace.QName("",
                                "priorAuthBeginServiceDate").equals(
                                reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                "nil");

                        if ("true".equals(nillableValue) ||
                                "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "priorAuthBeginServiceDate" +
                                "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setPriorAuthBeginServiceDate(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                            new javax.xml.namespace.QName("",
                                "priorAuthEndServiceDate").equals(
                                reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                "nil");

                        if ("true".equals(nillableValue) ||
                                "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "priorAuthEndServiceDate" +
                                "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setPriorAuthEndServiceDate(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                            new javax.xml.namespace.QName("", "masterPatientId").equals(
                                reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                "nil");

                        if ("true".equals(nillableValue) ||
                                "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "masterPatientId" +
                                "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setMasterPatientId(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                            new javax.xml.namespace.QName("",
                                "icdRevisionNumber").equals(reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                "nil");

                        if ("true".equals(nillableValue) ||
                                "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "icdRevisionNumber" +
                                "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setIcdRevisionNumber(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                            new javax.xml.namespace.QName("",
                                "priorAuthHeaderKey").equals(reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                "nil");

                        if ("true".equals(nillableValue) ||
                                "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "priorAuthHeaderKey" +
                                "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setPriorAuthHeaderKey(org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(
                                content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                        // A start element we are not expecting indicates an invalid parameter was passed
                        throw new org.apache.axis2.databinding.ADBException(
                            "Unexpected subelement " + reader.getName());
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                            new javax.xml.namespace.QName("",
                                "patientGenderRelationshipCode").equals(
                                reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                "nil");

                        if ("true".equals(nillableValue) ||
                                "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " +
                                "patientGenderRelationshipCode" +
                                "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setPatientGenderRelationshipCode(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                            new javax.xml.namespace.QName("",
                                "priorAuthOriginalStatusDatetime").equals(
                                reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                "nil");

                        if ("true".equals(nillableValue) ||
                                "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " +
                                "priorAuthOriginalStatusDatetime" +
                                "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setPriorAuthOriginalStatusDatetime(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                            new javax.xml.namespace.QName("",
                                "priorAuthCurrentStatusDatetime").equals(
                                reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                "nil");

                        if ("true".equals(nillableValue) ||
                                "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " +
                                "priorAuthCurrentStatusDatetime" +
                                "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setPriorAuthCurrentStatusDatetime(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                            new javax.xml.namespace.QName("",
                                "priorAuthCurrentStatusUserId").equals(
                                reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                "nil");

                        if ("true".equals(nillableValue) ||
                                "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " +
                                "priorAuthCurrentStatusUserId" +
                                "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setPriorAuthCurrentStatusUserId(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                            new javax.xml.namespace.QName("",
                                "priorAuthOriginalStatusUserId").equals(
                                reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                "nil");

                        if ("true".equals(nillableValue) ||
                                "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " +
                                "priorAuthOriginalStatusUserId" +
                                "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setPriorAuthOriginalStatusUserId(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }
                    
                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                    
                    if (reader.isStartElement() && new javax.xml.namespace.QName("","priorAuthContactDatetime").equals(reader.getName())){
                    
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                        if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                            throw new org.apache.axis2.databinding.ADBException("The element: " + "priorAuthContactDatetime" + " cannot be null");
                        }

                        java.lang.String content = reader.getElementText();
                        
                        object.setPriorAuthContactDatetime(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                  
                        reader.next();
                        
                    }  // End of if for expected property start element
                    else {
                    }
                    
                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                    
                    if (reader.isStartElement() && new javax.xml.namespace.QName("","priorAuthDueDatetime").equals(reader.getName())){
                    
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                        if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                            throw new org.apache.axis2.databinding.ADBException("The element: " + "priorAuthDueDatetime" + " cannot be null");
                        }

                        java.lang.String content = reader.getElementText();
                        
                        object.setPriorAuthDueDatetime(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                  
                        reader.next();
                        
                    }  // End of if for expected property start element
                    else {
                    }
                    
                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                    
                    if (reader.isStartElement() && new javax.xml.namespace.QName("","priorAuthWorkedBy").equals(reader.getName())){
                    
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                        if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                            throw new org.apache.axis2.databinding.ADBException("The element: " + "priorAuthWorkedBy" + " cannot be null");
                        }

                        java.lang.String content = reader.getElementText();
                        
                        object.setPriorAuthWorkedBy(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                  
                        reader.next();
                        
                    }  // End of if for expected property start element
                    else {
                    }
                    
                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                    
                    if (reader.isStartElement() && new javax.xml.namespace.QName("","priorAuthWithdrawnReasonCode").equals(reader.getName())){
                    
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                        if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                            throw new org.apache.axis2.databinding.ADBException("The element: " + "priorAuthWithdrawnReasonCode" + " cannot be null");
                        }

                        java.lang.String content = reader.getElementText();
                        
                        object.setPriorAuthWithdrawnReasonCode(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                  
                        reader.next();
                        
                    }  // End of if for expected property start element
                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                    
                    if (reader.isStartElement() && new javax.xml.namespace.QName("","priorAuthWithdrawnReasonDesc").equals(reader.getName())){
                    
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                        if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                            throw new org.apache.axis2.databinding.ADBException("The element: " + "priorAuthWithdrawnReasonDesc" + " cannot be null");
                        }

                        java.lang.String content = reader.getElementText();
                        
                        object.setPriorAuthWithdrawnReasonDesc(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                  
                        reader.next();
                        
                    }  // End of if for expected property start element
                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                   
                    if (reader.isStartElement() && new javax.xml.namespace.QName("","priorAuthBusinessSectorCode").equals(reader.getName())){
                    
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                        if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                            throw new org.apache.axis2.databinding.ADBException("The element: " + "priorAuthBusinessSectorCode" + " cannot be null");
                        }

                        java.lang.String content = reader.getElementText();
                        
                        object.setPriorAuthBusinessSectorCode(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                  
                        reader.next();
                        
                    }  // End of if for expected property start element
                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                   
                    if (reader.isStartElement() && new javax.xml.namespace.QName("","priorAuthBusinessSectorDescription").equals(reader.getName())){
                    
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                        if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                            throw new org.apache.axis2.databinding.ADBException("The element: " + "priorAuthBusinessSectorDescription" + " cannot be null");
                        }

                        java.lang.String content = reader.getElementText();
                        
                        object.setPriorAuthBusinessSectorDescription(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                  
                        reader.next();
                        
                    }  // End of if for expected property start element
                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                   
                    if (reader.isStartElement() && new javax.xml.namespace.QName("","priorAuthBusinessSegmentCode").equals(reader.getName())){
                    
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                        if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                            throw new org.apache.axis2.databinding.ADBException("The element: " + "priorAuthBusinessSegmentCode" + " cannot be null");
                        }

                        java.lang.String content = reader.getElementText();
                        
                        object.setPriorAuthBusinessSegmentCode(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                  
                        reader.next();
                        
                    }  // End of if for expected property start element
                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                   
                    if (reader.isStartElement() && new javax.xml.namespace.QName("","priorAuthBusinessSegmentDescription").equals(reader.getName())){
                    
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                        if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                            throw new org.apache.axis2.databinding.ADBException("The element: " + "priorAuthBusinessSegmentDescription" + " cannot be null");
                        }

                        java.lang.String content = reader.getElementText();
                        
                        object.setPriorAuthBusinessSegmentDescription(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                  
                        reader.next();
                        
                    }  // End of if for expected property start element
                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                   
                    if (reader.isStartElement() && new javax.xml.namespace.QName("","priorAuthPatientFirstName").equals(reader.getName())){
                    
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                        if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                            throw new org.apache.axis2.databinding.ADBException("The element: " + "priorAuthPatientFirstName" + " cannot be null");
                        }

                        java.lang.String content = reader.getElementText();
                        
                        object.setPriorAuthPatientFirstName(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                  
                        reader.next();
                        
                    }  // End of if for expected property start element
                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                   
                    if (reader.isStartElement() && new javax.xml.namespace.QName("","priorAuthPatientMiddleName").equals(reader.getName())){
                    
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                        if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                            throw new org.apache.axis2.databinding.ADBException("The element: " + "priorAuthPatientMiddleName" + " cannot be null");
                        }

                        java.lang.String content = reader.getElementText();
                        
                        object.setPriorAuthPatientMiddleName(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                  
                        reader.next();
                        
                    }  // End of if for expected property start element
                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                   
                    if (reader.isStartElement() && new javax.xml.namespace.QName("","priorAuthPatientLastName").equals(reader.getName())){
                    
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                        if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                            throw new org.apache.axis2.databinding.ADBException("The element: " + "priorAuthPatientLastName" + " cannot be null");
                        }

                        java.lang.String content = reader.getElementText();
                        
                        object.setPriorAuthPatientLastName(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                  
                        reader.next();
                        
                    }  // End of if for expected property start element
                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                   
                    if (reader.isStartElement() && new javax.xml.namespace.QName("","priorAuthPatientSuffixName").equals(reader.getName())){
                    
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                        if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                            throw new org.apache.axis2.databinding.ADBException("The element: " + "priorAuthPatientSuffixName" + " cannot be null");
                        }

                        java.lang.String content = reader.getElementText();
                        
                        object.setPriorAuthPatientSuffixName(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                  
                        reader.next();
                        
                    }  // End of if for expected property start element
                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                   
                    if (reader.isStartElement() && new javax.xml.namespace.QName("","priorAuthPatientBirthDate").equals(reader.getName())){
                    
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                        if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                            throw new org.apache.axis2.databinding.ADBException("The element: " + "priorAuthPatientBirthDate" + " cannot be null");
                        }

                        java.lang.String content = reader.getElementText();
                        
                        object.setPriorAuthPatientBirthDate(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                  
                        reader.next();
                        
                    }  // End of if for expected property start element
                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                   
                    if (reader.isStartElement() && new javax.xml.namespace.QName("","priorAuthPatientGenderCode").equals(reader.getName())){
                    
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                        if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                            throw new org.apache.axis2.databinding.ADBException("The element: " + "priorAuthPatientGenderCode" + " cannot be null");
                        }

                        java.lang.String content = reader.getElementText();
                        
                        object.setPriorAuthPatientGenderCode(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                  
                        reader.next();
                        
                    }  // End of if for expected property start element
                    else {
                    }

                    
                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()) {
                        // A start element we are not expecting indicates a trailing invalid property
                        throw new org.apache.axis2.databinding.ADBException(
                            "Unexpected subelement " + reader.getName());
                    }
                } catch (javax.xml.stream.XMLStreamException e) {
                    throw new java.lang.Exception(e);
                }

                return object;
            }
        } //end of factory class
    }

    public static class PriorAuthorizationSearchE implements org.apache.axis2.databinding.ADBBean {
        public static final javax.xml.namespace.QName MY_QNAME = new javax.xml.namespace.QName("http://services.lbm.avalon.com/",
                "priorAuthorizationSearch", "ns1");

        /**
         * field for PriorAuthorizationSearch
         */
        protected PriorAuthorizationSearch localPriorAuthorizationSearch;

        /**
         * Auto generated getter method
         * @return PriorAuthorizationSearch
         */
        public PriorAuthorizationSearch getPriorAuthorizationSearch() {
            return localPriorAuthorizationSearch;
        }

        /**
         * Auto generated setter method
         * @param param PriorAuthorizationSearch
         */
        public void setPriorAuthorizationSearch(PriorAuthorizationSearch param) {
            this.localPriorAuthorizationSearch = param;
        }

        /**
         *
         * @param parentQName
         * @param factory
         * @return org.apache.axiom.om.OMElement
         */
        public org.apache.axiom.om.OMElement getOMElement(
            final javax.xml.namespace.QName parentQName,
            final org.apache.axiom.om.OMFactory factory)
            throws org.apache.axis2.databinding.ADBException {
            org.apache.axiom.om.OMDataSource dataSource = new org.apache.axis2.databinding.ADBDataSource(this,
                    MY_QNAME);

            return factory.createOMElement(dataSource, MY_QNAME);
        }

        public void serialize(final javax.xml.namespace.QName parentQName,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException,
                org.apache.axis2.databinding.ADBException {
            serialize(parentQName, xmlWriter, false);
        }

        public void serialize(final javax.xml.namespace.QName parentQName,
            javax.xml.stream.XMLStreamWriter xmlWriter, boolean serializeType)
            throws javax.xml.stream.XMLStreamException,
                org.apache.axis2.databinding.ADBException {
            //We can safely assume an element has only one type associated with it
            if (localPriorAuthorizationSearch == null) {
                throw new org.apache.axis2.databinding.ADBException(
                    "priorAuthorizationSearch cannot be null!");
            }

            localPriorAuthorizationSearch.serialize(MY_QNAME, xmlWriter);
        }

        private static java.lang.String generatePrefix(
            java.lang.String namespace) {
            if (namespace.equals("http://services.lbm.avalon.com/")) {
                return "ns1";
            }

            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix,
            java.lang.String namespace, java.lang.String localPart,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

            if (writerPrefix != null) {
                xmlWriter.writeStartElement(namespace, localPart);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }

        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,
            java.lang.String namespace, java.lang.String attName,
            java.lang.String attValue,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }

            xmlWriter.writeAttribute(namespace, attName, attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,
            java.lang.String attName, java.lang.String attValue,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attValue);
            }
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeQNameAttribute(java.lang.String namespace,
            java.lang.String attName, javax.xml.namespace.QName qname,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String attributeNamespace = qname.getNamespaceURI();
            java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);

            if (attributePrefix == null) {
                attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
            }

            java.lang.String attributeValue;

            if (attributePrefix.trim().length() > 0) {
                attributeValue = attributePrefix + ":" + qname.getLocalPart();
            } else {
                attributeValue = qname.getLocalPart();
            }

            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attributeValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attributeValue);
            }
        }

        /**
         *  method to handle Qnames
         */
        private void writeQName(javax.xml.namespace.QName qname,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();

            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);

                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix, namespaceURI);
                }

                if (prefix.trim().length() > 0) {
                    xmlWriter.writeCharacters(prefix + ":" +
                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            qname));
                }
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }

                    namespaceURI = qnames[i].getNamespaceURI();

                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);

                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix, namespaceURI);
                        }

                        if (prefix.trim().length() > 0) {
                            stringToWrite.append(prefix).append(":")
                                         .append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                    qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                    qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                qnames[i]));
                    }
                }

                xmlWriter.writeCharacters(stringToWrite.toString());
            }
        }

        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(
            javax.xml.stream.XMLStreamWriter xmlWriter,
            java.lang.String namespace)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);

            if (prefix == null) {
                prefix = generatePrefix(namespace);

                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();

                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);

                    if ((uri == null) || (uri.length() == 0)) {
                        break;
                    }

                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }

                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }

            return prefix;
        }

        /**
         * databinding method to get an XML representation of this object
         *
         */
        public javax.xml.stream.XMLStreamReader getPullParser(
            javax.xml.namespace.QName qName)
            throws org.apache.axis2.databinding.ADBException {
            //We can safely assume an element has only one type associated with it
            return localPriorAuthorizationSearch.getPullParser(MY_QNAME);
        }

        /**
         *  Factory class that keeps the parse method
         */
        public static class Factory {
            /**
             * static method to create the object
             * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
             *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
             * Postcondition: If this object is an element, the reader is positioned at its end element
             *                If this object is a complex type, the reader is positioned at the end element of its outer element
             */
            public static PriorAuthorizationSearchE parse(
                javax.xml.stream.XMLStreamReader reader)
                throws java.lang.Exception {
                PriorAuthorizationSearchE object = new PriorAuthorizationSearchE();

                int event;
                java.lang.String nillableValue = null;
                java.lang.String prefix = "";
                java.lang.String namespaceuri = "";

                try {
                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    // Note all attributes that were handled. Used to differ normal attributes
                    // from anyAttributes.
                    java.util.Vector handledAttributes = new java.util.Vector();

                    while (!reader.isEndElement()) {
                        if (reader.isStartElement()) {
                            if (reader.isStartElement() &&
                                    new javax.xml.namespace.QName(
                                        "http://services.lbm.avalon.com/",
                                        "priorAuthorizationSearch").equals(
                                        reader.getName())) {
                                object.setPriorAuthorizationSearch(PriorAuthorizationSearch.Factory.parse(
                                        reader));
                            } // End of if for expected property start element

                            else {
                                // A start element we are not expecting indicates an invalid parameter was passed
                                throw new org.apache.axis2.databinding.ADBException(
                                    "Unexpected subelement " +
                                    reader.getName());
                            }
                        } else {
                            reader.next();
                        }
                    } // end of while loop
                } catch (javax.xml.stream.XMLStreamException e) {
                    throw new java.lang.Exception(e);
                }

                return object;
            }
        } //end of factory class
    }

    public static class ProcedureDTOE implements org.apache.axis2.databinding.ADBBean {
        public static final javax.xml.namespace.QName MY_QNAME = new javax.xml.namespace.QName("http://services.lbm.avalon.com/",
                "procedureDTO", "ns1");

        /**
         * field for ProcedureDTO
         */
        protected ProcedureDTO localProcedureDTO;

        /**
         * Auto generated getter method
         * @return ProcedureDTO
         */
        public ProcedureDTO getProcedureDTO() {
            return localProcedureDTO;
        }

        /**
         * Auto generated setter method
         * @param param ProcedureDTO
         */
        public void setProcedureDTO(ProcedureDTO param) {
            this.localProcedureDTO = param;
        }

        /**
         *
         * @param parentQName
         * @param factory
         * @return org.apache.axiom.om.OMElement
         */
        public org.apache.axiom.om.OMElement getOMElement(
            final javax.xml.namespace.QName parentQName,
            final org.apache.axiom.om.OMFactory factory)
            throws org.apache.axis2.databinding.ADBException {
            org.apache.axiom.om.OMDataSource dataSource = new org.apache.axis2.databinding.ADBDataSource(this,
                    MY_QNAME);

            return factory.createOMElement(dataSource, MY_QNAME);
        }

        public void serialize(final javax.xml.namespace.QName parentQName,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException,
                org.apache.axis2.databinding.ADBException {
            serialize(parentQName, xmlWriter, false);
        }

        public void serialize(final javax.xml.namespace.QName parentQName,
            javax.xml.stream.XMLStreamWriter xmlWriter, boolean serializeType)
            throws javax.xml.stream.XMLStreamException,
                org.apache.axis2.databinding.ADBException {
            //We can safely assume an element has only one type associated with it
            if (localProcedureDTO == null) {
                throw new org.apache.axis2.databinding.ADBException(
                    "procedureDTO cannot be null!");
            }

            localProcedureDTO.serialize(MY_QNAME, xmlWriter);
        }

        private static java.lang.String generatePrefix(
            java.lang.String namespace) {
            if (namespace.equals("http://services.lbm.avalon.com/")) {
                return "ns1";
            }

            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix,
            java.lang.String namespace, java.lang.String localPart,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

            if (writerPrefix != null) {
                xmlWriter.writeStartElement(namespace, localPart);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }

        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,
            java.lang.String namespace, java.lang.String attName,
            java.lang.String attValue,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }

            xmlWriter.writeAttribute(namespace, attName, attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,
            java.lang.String attName, java.lang.String attValue,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attValue);
            }
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeQNameAttribute(java.lang.String namespace,
            java.lang.String attName, javax.xml.namespace.QName qname,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String attributeNamespace = qname.getNamespaceURI();
            java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);

            if (attributePrefix == null) {
                attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
            }

            java.lang.String attributeValue;

            if (attributePrefix.trim().length() > 0) {
                attributeValue = attributePrefix + ":" + qname.getLocalPart();
            } else {
                attributeValue = qname.getLocalPart();
            }

            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attributeValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attributeValue);
            }
        }

        /**
         *  method to handle Qnames
         */
        private void writeQName(javax.xml.namespace.QName qname,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();

            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);

                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix, namespaceURI);
                }

                if (prefix.trim().length() > 0) {
                    xmlWriter.writeCharacters(prefix + ":" +
                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            qname));
                }
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }

                    namespaceURI = qnames[i].getNamespaceURI();

                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);

                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix, namespaceURI);
                        }

                        if (prefix.trim().length() > 0) {
                            stringToWrite.append(prefix).append(":")
                                         .append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                    qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                    qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                qnames[i]));
                    }
                }

                xmlWriter.writeCharacters(stringToWrite.toString());
            }
        }

        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(
            javax.xml.stream.XMLStreamWriter xmlWriter,
            java.lang.String namespace)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);

            if (prefix == null) {
                prefix = generatePrefix(namespace);

                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();

                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);

                    if ((uri == null) || (uri.length() == 0)) {
                        break;
                    }

                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }

                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }

            return prefix;
        }

        /**
         * databinding method to get an XML representation of this object
         *
         */
        public javax.xml.stream.XMLStreamReader getPullParser(
            javax.xml.namespace.QName qName)
            throws org.apache.axis2.databinding.ADBException {
            //We can safely assume an element has only one type associated with it
            return localProcedureDTO.getPullParser(MY_QNAME);
        }

        /**
         *  Factory class that keeps the parse method
         */
        public static class Factory {
            /**
             * static method to create the object
             * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
             *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
             * Postcondition: If this object is an element, the reader is positioned at its end element
             *                If this object is a complex type, the reader is positioned at the end element of its outer element
             */
            public static ProcedureDTOE parse(
                javax.xml.stream.XMLStreamReader reader)
                throws java.lang.Exception {
                ProcedureDTOE object = new ProcedureDTOE();

                int event;
                java.lang.String nillableValue = null;
                java.lang.String prefix = "";
                java.lang.String namespaceuri = "";

                try {
                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    // Note all attributes that were handled. Used to differ normal attributes
                    // from anyAttributes.
                    java.util.Vector handledAttributes = new java.util.Vector();

                    while (!reader.isEndElement()) {
                        if (reader.isStartElement()) {
                            if (reader.isStartElement() &&
                                    new javax.xml.namespace.QName(
                                        "http://services.lbm.avalon.com/",
                                        "procedureDTO").equals(reader.getName())) {
                                object.setProcedureDTO(ProcedureDTO.Factory.parse(
                                        reader));
                            } // End of if for expected property start element

                            else {
                                // A start element we are not expecting indicates an invalid parameter was passed
                                throw new org.apache.axis2.databinding.ADBException(
                                    "Unexpected subelement " +
                                    reader.getName());
                            }
                        } else {
                            reader.next();
                        }
                    } // end of while loop
                } catch (javax.xml.stream.XMLStreamException e) {
                    throw new java.lang.Exception(e);
                }

                return object;
            }
        } //end of factory class
    }

    public static class PriorAuthHeaderDTOE implements org.apache.axis2.databinding.ADBBean {
        public static final javax.xml.namespace.QName MY_QNAME = new javax.xml.namespace.QName("http://services.lbm.avalon.com/",
                "priorAuthHeaderDTO", "ns1");

        /**
         * field for PriorAuthHeaderDTO
         */
        protected PriorAuthHeaderDTO localPriorAuthHeaderDTO;

        /**
         * Auto generated getter method
         * @return PriorAuthHeaderDTO
         */
        public PriorAuthHeaderDTO getPriorAuthHeaderDTO() {
            return localPriorAuthHeaderDTO;
        }

        /**
         * Auto generated setter method
         * @param param PriorAuthHeaderDTO
         */
        public void setPriorAuthHeaderDTO(PriorAuthHeaderDTO param) {
            this.localPriorAuthHeaderDTO = param;
        }

        /**
         *
         * @param parentQName
         * @param factory
         * @return org.apache.axiom.om.OMElement
         */
        public org.apache.axiom.om.OMElement getOMElement(
            final javax.xml.namespace.QName parentQName,
            final org.apache.axiom.om.OMFactory factory)
            throws org.apache.axis2.databinding.ADBException {
            org.apache.axiom.om.OMDataSource dataSource = new org.apache.axis2.databinding.ADBDataSource(this,
                    MY_QNAME);

            return factory.createOMElement(dataSource, MY_QNAME);
        }

        public void serialize(final javax.xml.namespace.QName parentQName,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException,
                org.apache.axis2.databinding.ADBException {
            serialize(parentQName, xmlWriter, false);
        }

        public void serialize(final javax.xml.namespace.QName parentQName,
            javax.xml.stream.XMLStreamWriter xmlWriter, boolean serializeType)
            throws javax.xml.stream.XMLStreamException,
                org.apache.axis2.databinding.ADBException {
            //We can safely assume an element has only one type associated with it
            if (localPriorAuthHeaderDTO == null) {
                throw new org.apache.axis2.databinding.ADBException(
                    "priorAuthHeaderDTO cannot be null!");
            }

            localPriorAuthHeaderDTO.serialize(MY_QNAME, xmlWriter);
        }

        private static java.lang.String generatePrefix(
            java.lang.String namespace) {
            if (namespace.equals("http://services.lbm.avalon.com/")) {
                return "ns1";
            }

            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix,
            java.lang.String namespace, java.lang.String localPart,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

            if (writerPrefix != null) {
                xmlWriter.writeStartElement(namespace, localPart);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }

        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,
            java.lang.String namespace, java.lang.String attName,
            java.lang.String attValue,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }

            xmlWriter.writeAttribute(namespace, attName, attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,
            java.lang.String attName, java.lang.String attValue,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attValue);
            }
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeQNameAttribute(java.lang.String namespace,
            java.lang.String attName, javax.xml.namespace.QName qname,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String attributeNamespace = qname.getNamespaceURI();
            java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);

            if (attributePrefix == null) {
                attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
            }

            java.lang.String attributeValue;

            if (attributePrefix.trim().length() > 0) {
                attributeValue = attributePrefix + ":" + qname.getLocalPart();
            } else {
                attributeValue = qname.getLocalPart();
            }

            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attributeValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attributeValue);
            }
        }

        /**
         *  method to handle Qnames
         */
        private void writeQName(javax.xml.namespace.QName qname,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();

            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);

                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix, namespaceURI);
                }

                if (prefix.trim().length() > 0) {
                    xmlWriter.writeCharacters(prefix + ":" +
                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            qname));
                }
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }

                    namespaceURI = qnames[i].getNamespaceURI();

                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);

                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix, namespaceURI);
                        }

                        if (prefix.trim().length() > 0) {
                            stringToWrite.append(prefix).append(":")
                                         .append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                    qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                    qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                qnames[i]));
                    }
                }

                xmlWriter.writeCharacters(stringToWrite.toString());
            }
        }

        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(
            javax.xml.stream.XMLStreamWriter xmlWriter,
            java.lang.String namespace)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);

            if (prefix == null) {
                prefix = generatePrefix(namespace);

                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();

                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);

                    if ((uri == null) || (uri.length() == 0)) {
                        break;
                    }

                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }

                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }

            return prefix;
        }

        /**
         * databinding method to get an XML representation of this object
         *
         */
        public javax.xml.stream.XMLStreamReader getPullParser(
            javax.xml.namespace.QName qName)
            throws org.apache.axis2.databinding.ADBException {
            //We can safely assume an element has only one type associated with it
            return localPriorAuthHeaderDTO.getPullParser(MY_QNAME);
        }

        /**
         *  Factory class that keeps the parse method
         */
        public static class Factory {
            /**
             * static method to create the object
             * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
             *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
             * Postcondition: If this object is an element, the reader is positioned at its end element
             *                If this object is a complex type, the reader is positioned at the end element of its outer element
             */
            public static PriorAuthHeaderDTOE parse(
                javax.xml.stream.XMLStreamReader reader)
                throws java.lang.Exception {
                PriorAuthHeaderDTOE object = new PriorAuthHeaderDTOE();

                int event;
                java.lang.String nillableValue = null;
                java.lang.String prefix = "";
                java.lang.String namespaceuri = "";

                try {
                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    // Note all attributes that were handled. Used to differ normal attributes
                    // from anyAttributes.
                    java.util.Vector handledAttributes = new java.util.Vector();

                    while (!reader.isEndElement()) {
                        if (reader.isStartElement()) {
                            if (reader.isStartElement() &&
                                    new javax.xml.namespace.QName(
                                        "http://services.lbm.avalon.com/",
                                        "priorAuthHeaderDTO").equals(
                                        reader.getName())) {
                                object.setPriorAuthHeaderDTO(PriorAuthHeaderDTO.Factory.parse(
                                        reader));
                            } // End of if for expected property start element

                            else {
                                // A start element we are not expecting indicates an invalid parameter was passed
                                throw new org.apache.axis2.databinding.ADBException(
                                    "Unexpected subelement " +
                                    reader.getName());
                            }
                        } else {
                            reader.next();
                        }
                    } // end of while loop
                } catch (javax.xml.stream.XMLStreamException e) {
                    throw new java.lang.Exception(e);
                }

                return object;
            }
        } //end of factory class
    }

    public static class PriorAuthHeader implements org.apache.axis2.databinding.ADBBean {
        /* This type was generated from the piece of schema that had
           name = PriorAuthHeader
           Namespace URI = http://services.lbm.avalon.com/
           Namespace Prefix = ns1
         */

        /**
         * field for Provider
         */
        protected ProviderDTO localProvider;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localProviderTracker = false;

        /**
         * field for Procedures
         * This was an Array!
         */
        protected ProcedureDTO[] localProcedures;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localProceduresTracker = false;

        /**
         * field for Diagnosis
         * This was an Array!
         */
        protected DiagnosisDTO[] localDiagnosis;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localDiagnosisTracker = false;

        /**
         * field for AppCreateUserId
         */
        protected java.lang.String localAppCreateUserId;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localAppCreateUserIdTracker = false;

        /**
         * field for AppMaintUserId
         */
        protected java.lang.String localAppMaintUserId;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localAppMaintUserIdTracker = false;

        /**
         * field for SourceTab
         */
        protected java.lang.String localSourceTab;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localSourceTabTracker = false;

        /**
         * field for AuthorizationKey
         */
        protected long localAuthorizationKey;

        /**
         * field for PriorAuthHeaderDTO
         * This was an Array!
         */
        protected PriorAuthHeaderDTO[] localPriorAuthHeaderDTO;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localPriorAuthHeaderDTOTracker = false;

        /**
         * field for OperationType
         */
        protected java.lang.String localOperationType;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localOperationTypeTracker = false;

        /**
         * field for AuthType
         */
        protected java.lang.String localAuthType;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localAuthTypeTracker = false;

        public boolean isProviderSpecified() {
            return localProviderTracker;
        }

        /**
         * Auto generated getter method
         * @return ProviderDTO
         */
        public ProviderDTO getProvider() {
            return localProvider;
        }

        /**
         * Auto generated setter method
         * @param param Provider
         */
        public void setProvider(ProviderDTO param) {
            localProviderTracker = param != null;

            this.localProvider = param;
        }

        public boolean isProceduresSpecified() {
            return localProceduresTracker;
        }

        /**
         * Auto generated getter method
         * @return ProcedureDTO[]
         */
        public ProcedureDTO[] getProcedures() {
            return localProcedures;
        }

        /**
         * validate the array for Procedures
         */
        protected void validateProcedures(ProcedureDTO[] param) {
        }

        /**
         * Auto generated setter method
         * @param param Procedures
         */
        public void setProcedures(ProcedureDTO[] param) {
            validateProcedures(param);

            localProceduresTracker = true;

            this.localProcedures = param;
        }

        /**
         * Auto generated add method for the array for convenience
         * @param param ProcedureDTO
         */
        public void addProcedures(ProcedureDTO param) {
            if (localProcedures == null) {
                localProcedures = new ProcedureDTO[] {  };
            }

            //update the setting tracker
            localProceduresTracker = true;

            java.util.List list = org.apache.axis2.databinding.utils.ConverterUtil.toList(localProcedures);
            list.add(param);
            this.localProcedures = (ProcedureDTO[]) list.toArray(new ProcedureDTO[list.size()]);
        }

        public boolean isDiagnosisSpecified() {
            return localDiagnosisTracker;
        }

        /**
         * Auto generated getter method
         * @return DiagnosisDTO[]
         */
        public DiagnosisDTO[] getDiagnosis() {
            return localDiagnosis;
        }

        /**
         * validate the array for Diagnosis
         */
        protected void validateDiagnosis(DiagnosisDTO[] param) {
        }

        /**
         * Auto generated setter method
         * @param param Diagnosis
         */
        public void setDiagnosis(DiagnosisDTO[] param) {
            validateDiagnosis(param);

            localDiagnosisTracker = true;

            this.localDiagnosis = param;
        }

        /**
         * Auto generated add method for the array for convenience
         * @param param DiagnosisDTO
         */
        public void addDiagnosis(DiagnosisDTO param) {
            if (localDiagnosis == null) {
                localDiagnosis = new DiagnosisDTO[] {  };
            }

            //update the setting tracker
            localDiagnosisTracker = true;

            java.util.List list = org.apache.axis2.databinding.utils.ConverterUtil.toList(localDiagnosis);
            list.add(param);
            this.localDiagnosis = (DiagnosisDTO[]) list.toArray(new DiagnosisDTO[list.size()]);
        }

        public boolean isAppCreateUserIdSpecified() {
            return localAppCreateUserIdTracker;
        }

        /**
         * Auto generated getter method
         * @return java.lang.String
         */
        public java.lang.String getAppCreateUserId() {
            return localAppCreateUserId;
        }

        /**
         * Auto generated setter method
         * @param param AppCreateUserId
         */
        public void setAppCreateUserId(java.lang.String param) {
            localAppCreateUserIdTracker = param != null;

            this.localAppCreateUserId = param;
        }

        public boolean isAppMaintUserIdSpecified() {
            return localAppMaintUserIdTracker;
        }

        /**
         * Auto generated getter method
         * @return java.lang.String
         */
        public java.lang.String getAppMaintUserId() {
            return localAppMaintUserId;
        }

        /**
         * Auto generated setter method
         * @param param AppMaintUserId
         */
        public void setAppMaintUserId(java.lang.String param) {
            localAppMaintUserIdTracker = param != null;

            this.localAppMaintUserId = param;
        }

        public boolean isSourceTabSpecified() {
            return localSourceTabTracker;
        }

        /**
         * Auto generated getter method
         * @return java.lang.String
         */
        public java.lang.String getSourceTab() {
            return localSourceTab;
        }

        /**
         * Auto generated setter method
         * @param param SourceTab
         */
        public void setSourceTab(java.lang.String param) {
            localSourceTabTracker = param != null;

            this.localSourceTab = param;
        }

        /**
         * Auto generated getter method
         * @return long
         */
        public long getAuthorizationKey() {
            return localAuthorizationKey;
        }

        /**
         * Auto generated setter method
         * @param param AuthorizationKey
         */
        public void setAuthorizationKey(long param) {
            this.localAuthorizationKey = param;
        }

        public boolean isPriorAuthHeaderDTOSpecified() {
            return localPriorAuthHeaderDTOTracker;
        }

        /**
         * Auto generated getter method
         * @return PriorAuthHeaderDTO[]
         */
        public PriorAuthHeaderDTO[] getPriorAuthHeaderDTO() {
            return localPriorAuthHeaderDTO;
        }

        /**
         * validate the array for PriorAuthHeaderDTO
         */
        protected void validatePriorAuthHeaderDTO(PriorAuthHeaderDTO[] param) {
        }

        /**
         * Auto generated setter method
         * @param param PriorAuthHeaderDTO
         */
        public void setPriorAuthHeaderDTO(PriorAuthHeaderDTO[] param) {
            validatePriorAuthHeaderDTO(param);

            localPriorAuthHeaderDTOTracker = param != null;

            this.localPriorAuthHeaderDTO = param;
        }

        /**
         * Auto generated add method for the array for convenience
         * @param param PriorAuthHeaderDTO
         */
        public void addPriorAuthHeaderDTO(PriorAuthHeaderDTO param) {
            if (localPriorAuthHeaderDTO == null) {
                localPriorAuthHeaderDTO = new PriorAuthHeaderDTO[] {  };
            }

            //update the setting tracker
            localPriorAuthHeaderDTOTracker = true;

            java.util.List list = org.apache.axis2.databinding.utils.ConverterUtil.toList(localPriorAuthHeaderDTO);
            list.add(param);
            this.localPriorAuthHeaderDTO = (PriorAuthHeaderDTO[]) list.toArray(new PriorAuthHeaderDTO[list.size()]);
        }

        public boolean isOperationTypeSpecified() {
            return localOperationTypeTracker;
        }

        /**
         * Auto generated getter method
         * @return java.lang.String
         */
        public java.lang.String getOperationType() {
            return localOperationType;
        }

        /**
         * Auto generated setter method
         * @param param OperationType
         */
        public void setOperationType(java.lang.String param) {
            localOperationTypeTracker = param != null;

            this.localOperationType = param;
        }

        public boolean isAuthTypeSpecified() {
            return localAuthTypeTracker;
        }

        /**
         * Auto generated getter method
         * @return java.lang.String
         */
        public java.lang.String getAuthType() {
            return localAuthType;
        }

        /**
         * Auto generated setter method
         * @param param AuthType
         */
        public void setAuthType(java.lang.String param) {
            localAuthTypeTracker = param != null;

            this.localAuthType = param;
        }

        /**
         *
         * @param parentQName
         * @param factory
         * @return org.apache.axiom.om.OMElement
         */
        public org.apache.axiom.om.OMElement getOMElement(
            final javax.xml.namespace.QName parentQName,
            final org.apache.axiom.om.OMFactory factory)
            throws org.apache.axis2.databinding.ADBException {
            org.apache.axiom.om.OMDataSource dataSource = new org.apache.axis2.databinding.ADBDataSource(this,
                    parentQName);

            return factory.createOMElement(dataSource, parentQName);
        }

        public void serialize(final javax.xml.namespace.QName parentQName,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException,
                org.apache.axis2.databinding.ADBException {
            serialize(parentQName, xmlWriter, false);
        }

        public void serialize(final javax.xml.namespace.QName parentQName,
            javax.xml.stream.XMLStreamWriter xmlWriter, boolean serializeType)
            throws javax.xml.stream.XMLStreamException,
                org.apache.axis2.databinding.ADBException {
            java.lang.String prefix = null;
            java.lang.String namespace = null;

            prefix = parentQName.getPrefix();
            namespace = parentQName.getNamespaceURI();
            writeStartElement(prefix, namespace, parentQName.getLocalPart(),
                xmlWriter);

            if (serializeType) {
                java.lang.String namespacePrefix = registerPrefix(xmlWriter,
                        "http://services.lbm.avalon.com/");

                if ((namespacePrefix != null) &&
                        (namespacePrefix.trim().length() > 0)) {
                    writeAttribute("xsi",
                        "http://www.w3.org/2001/XMLSchema-instance", "type",
                        namespacePrefix + ":PriorAuthHeader", xmlWriter);
                } else {
                    writeAttribute("xsi",
                        "http://www.w3.org/2001/XMLSchema-instance", "type",
                        "PriorAuthHeader", xmlWriter);
                }
            }

            if (localProviderTracker) {
                if (localProvider == null) {
                    throw new org.apache.axis2.databinding.ADBException(
                        "provider cannot be null!!");
                }

                localProvider.serialize(new javax.xml.namespace.QName("",
                        "provider"), xmlWriter);
            }

            if (localProceduresTracker) {
                if (localProcedures != null) {
                    for (int i = 0; i < localProcedures.length; i++) {
                        if (localProcedures[i] != null) {
                            localProcedures[i].serialize(new javax.xml.namespace.QName(
                                    "", "procedures"), xmlWriter);
                        } else {
                            writeStartElement(null, "", "procedures", xmlWriter);

                            // write the nil attribute
                            writeAttribute("xsi",
                                "http://www.w3.org/2001/XMLSchema-instance",
                                "nil", "1", xmlWriter);
                            xmlWriter.writeEndElement();
                        }
                    }
                } else {
                    writeStartElement(null, "", "procedures", xmlWriter);

                    // write the nil attribute
                    writeAttribute("xsi",
                        "http://www.w3.org/2001/XMLSchema-instance", "nil",
                        "1", xmlWriter);
                    xmlWriter.writeEndElement();
                }
            }

            if (localDiagnosisTracker) {
                if (localDiagnosis != null) {
                    for (int i = 0; i < localDiagnosis.length; i++) {
                        if (localDiagnosis[i] != null) {
                            localDiagnosis[i].serialize(new javax.xml.namespace.QName(
                                    "", "diagnosis"), xmlWriter);
                        } else {
                            writeStartElement(null, "", "diagnosis", xmlWriter);

                            // write the nil attribute
                            writeAttribute("xsi",
                                "http://www.w3.org/2001/XMLSchema-instance",
                                "nil", "1", xmlWriter);
                            xmlWriter.writeEndElement();
                        }
                    }
                } else {
                    writeStartElement(null, "", "diagnosis", xmlWriter);

                    // write the nil attribute
                    writeAttribute("xsi",
                        "http://www.w3.org/2001/XMLSchema-instance", "nil",
                        "1", xmlWriter);
                    xmlWriter.writeEndElement();
                }
            }

            if (localAppCreateUserIdTracker) {
                namespace = "";
                writeStartElement(null, namespace, "appCreateUserId", xmlWriter);

                if (localAppCreateUserId == null) {
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException(
                        "appCreateUserId cannot be null!!");
                } else {
                    xmlWriter.writeCharacters(localAppCreateUserId);
                }

                xmlWriter.writeEndElement();
            }

            if (localAppMaintUserIdTracker) {
                namespace = "";
                writeStartElement(null, namespace, "appMaintUserId", xmlWriter);

                if (localAppMaintUserId == null) {
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException(
                        "appMaintUserId cannot be null!!");
                } else {
                    xmlWriter.writeCharacters(localAppMaintUserId);
                }

                xmlWriter.writeEndElement();
            }

            if (localSourceTabTracker) {
                namespace = "";
                writeStartElement(null, namespace, "sourceTab", xmlWriter);

                if (localSourceTab == null) {
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException(
                        "sourceTab cannot be null!!");
                } else {
                    xmlWriter.writeCharacters(localSourceTab);
                }

                xmlWriter.writeEndElement();
            }

            namespace = "";
            writeStartElement(null, namespace, "authorizationKey", xmlWriter);

            if (localAuthorizationKey == java.lang.Long.MIN_VALUE) {
                throw new org.apache.axis2.databinding.ADBException(
                    "authorizationKey cannot be null!!");
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        localAuthorizationKey));
            }

            xmlWriter.writeEndElement();

            if (localPriorAuthHeaderDTOTracker) {
                if (localPriorAuthHeaderDTO != null) {
                    for (int i = 0; i < localPriorAuthHeaderDTO.length; i++) {
                        if (localPriorAuthHeaderDTO[i] != null) {
                            localPriorAuthHeaderDTO[i].serialize(new javax.xml.namespace.QName(
                                    "http://services.lbm.avalon.com/",
                                    "priorAuthHeaderDTO"), xmlWriter);
                        } else {
                            // we don't have to do any thing since minOccures is zero
                        }
                    }
                } else {
                    throw new org.apache.axis2.databinding.ADBException(
                        "priorAuthHeaderDTO cannot be null!!");
                }
            }

            if (localOperationTypeTracker) {
                namespace = "";
                writeStartElement(null, namespace, "operationType", xmlWriter);

                if (localOperationType == null) {
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException(
                        "operationType cannot be null!!");
                } else {
                    xmlWriter.writeCharacters(localOperationType);
                }

                xmlWriter.writeEndElement();
            }

            if (localAuthTypeTracker) {
                namespace = "";
                writeStartElement(null, namespace, "authType", xmlWriter);

                if (localAuthType == null) {
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException(
                        "authType cannot be null!!");
                } else {
                    xmlWriter.writeCharacters(localAuthType);
                }

                xmlWriter.writeEndElement();
            }

            xmlWriter.writeEndElement();
        }

        private static java.lang.String generatePrefix(
            java.lang.String namespace) {
            if (namespace.equals("http://services.lbm.avalon.com/")) {
                return "ns1";
            }

            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix,
            java.lang.String namespace, java.lang.String localPart,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

            if (writerPrefix != null) {
                xmlWriter.writeStartElement(namespace, localPart);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }

        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,
            java.lang.String namespace, java.lang.String attName,
            java.lang.String attValue,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }

            xmlWriter.writeAttribute(namespace, attName, attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,
            java.lang.String attName, java.lang.String attValue,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attValue);
            }
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeQNameAttribute(java.lang.String namespace,
            java.lang.String attName, javax.xml.namespace.QName qname,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String attributeNamespace = qname.getNamespaceURI();
            java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);

            if (attributePrefix == null) {
                attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
            }

            java.lang.String attributeValue;

            if (attributePrefix.trim().length() > 0) {
                attributeValue = attributePrefix + ":" + qname.getLocalPart();
            } else {
                attributeValue = qname.getLocalPart();
            }

            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attributeValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attributeValue);
            }
        }

        /**
         *  method to handle Qnames
         */
        private void writeQName(javax.xml.namespace.QName qname,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();

            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);

                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix, namespaceURI);
                }

                if (prefix.trim().length() > 0) {
                    xmlWriter.writeCharacters(prefix + ":" +
                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            qname));
                }
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }

                    namespaceURI = qnames[i].getNamespaceURI();

                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);

                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix, namespaceURI);
                        }

                        if (prefix.trim().length() > 0) {
                            stringToWrite.append(prefix).append(":")
                                         .append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                    qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                    qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                qnames[i]));
                    }
                }

                xmlWriter.writeCharacters(stringToWrite.toString());
            }
        }

        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(
            javax.xml.stream.XMLStreamWriter xmlWriter,
            java.lang.String namespace)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);

            if (prefix == null) {
                prefix = generatePrefix(namespace);

                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();

                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);

                    if ((uri == null) || (uri.length() == 0)) {
                        break;
                    }

                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }

                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }

            return prefix;
        }

        /**
         * databinding method to get an XML representation of this object
         *
         */
        public javax.xml.stream.XMLStreamReader getPullParser(
            javax.xml.namespace.QName qName)
            throws org.apache.axis2.databinding.ADBException {
            java.util.ArrayList elementList = new java.util.ArrayList();
            java.util.ArrayList attribList = new java.util.ArrayList();

            if (localProviderTracker) {
                elementList.add(new javax.xml.namespace.QName("", "provider"));

                if (localProvider == null) {
                    throw new org.apache.axis2.databinding.ADBException(
                        "provider cannot be null!!");
                }

                elementList.add(localProvider);
            }

            if (localProceduresTracker) {
                if (localProcedures != null) {
                    for (int i = 0; i < localProcedures.length; i++) {
                        if (localProcedures[i] != null) {
                            elementList.add(new javax.xml.namespace.QName("",
                                    "procedures"));
                            elementList.add(localProcedures[i]);
                        } else {
                            elementList.add(new javax.xml.namespace.QName("",
                                    "procedures"));
                            elementList.add(null);
                        }
                    }
                } else {
                    elementList.add(new javax.xml.namespace.QName("",
                            "procedures"));
                    elementList.add(localProcedures);
                }
            }

            if (localDiagnosisTracker) {
                if (localDiagnosis != null) {
                    for (int i = 0; i < localDiagnosis.length; i++) {
                        if (localDiagnosis[i] != null) {
                            elementList.add(new javax.xml.namespace.QName("",
                                    "Diagnosis"));
                            elementList.add(localDiagnosis[i]);
                        } else {
                            elementList.add(new javax.xml.namespace.QName("",
                                    "diagnosis"));
                            elementList.add(null);
                        }
                    }
                } else {
                    elementList.add(new javax.xml.namespace.QName("", "diagnosis"));
                    elementList.add(localDiagnosis);
                }
            }

            if (localAppCreateUserIdTracker) {
                elementList.add(new javax.xml.namespace.QName("",
                        "appCreateUserId"));

                if (localAppCreateUserId != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            localAppCreateUserId));
                } else {
                    throw new org.apache.axis2.databinding.ADBException(
                        "appCreateUserId cannot be null!!");
                }
            }

            if (localAppMaintUserIdTracker) {
                elementList.add(new javax.xml.namespace.QName("",
                        "appMaintUserId"));

                if (localAppMaintUserId != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            localAppMaintUserId));
                } else {
                    throw new org.apache.axis2.databinding.ADBException(
                        "appMaintUserId cannot be null!!");
                }
            }

            if (localSourceTabTracker) {
                elementList.add(new javax.xml.namespace.QName("", "sourceTab"));

                if (localSourceTab != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            localSourceTab));
                } else {
                    throw new org.apache.axis2.databinding.ADBException(
                        "sourceTab cannot be null!!");
                }
            }

            elementList.add(new javax.xml.namespace.QName("", "authorizationKey"));

            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                    localAuthorizationKey));

            if (localPriorAuthHeaderDTOTracker) {
                if (localPriorAuthHeaderDTO != null) {
                    for (int i = 0; i < localPriorAuthHeaderDTO.length; i++) {
                        if (localPriorAuthHeaderDTO[i] != null) {
                            elementList.add(new javax.xml.namespace.QName(
                                    "http://services.lbm.avalon.com/",
                                    "priorAuthHeaderDTO"));
                            elementList.add(localPriorAuthHeaderDTO[i]);
                        } else {
                            // nothing to do
                        }
                    }
                } else {
                    throw new org.apache.axis2.databinding.ADBException(
                        "priorAuthHeaderDTO cannot be null!!");
                }
            }

            if (localOperationTypeTracker) {
                elementList.add(new javax.xml.namespace.QName("",
                        "operationType"));

                if (localOperationType != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            localOperationType));
                } else {
                    throw new org.apache.axis2.databinding.ADBException(
                        "operationType cannot be null!!");
                }
            }

            if (localAuthTypeTracker) {
                elementList.add(new javax.xml.namespace.QName("",
                        "authType"));

                if (localAuthType != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            localAuthType));
                } else {
                    throw new org.apache.axis2.databinding.ADBException(
                        "authType cannot be null!!");
                }
            }

            return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName,
                elementList.toArray(), attribList.toArray());
        }

        /**
         *  Factory class that keeps the parse method
         */
        public static class Factory {
            /**
             * static method to create the object
             * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
             *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
             * Postcondition: If this object is an element, the reader is positioned at its end element
             *                If this object is a complex type, the reader is positioned at the end element of its outer element
             */
            public static PriorAuthHeader parse(
                javax.xml.stream.XMLStreamReader reader)
                throws java.lang.Exception {
                PriorAuthHeader object = new PriorAuthHeader();

                int event;
                java.lang.String nillableValue = null;
                java.lang.String prefix = "";
                java.lang.String namespaceuri = "";

                try {
                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.getAttributeValue(
                                "http://www.w3.org/2001/XMLSchema-instance",
                                "type") != null) {
                        java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                "type");

                        if (fullTypeName != null) {
                            java.lang.String nsPrefix = null;

                            if (fullTypeName.indexOf(":") > -1) {
                                nsPrefix = fullTypeName.substring(0,
                                        fullTypeName.indexOf(":"));
                            }

                            nsPrefix = (nsPrefix == null) ? "" : nsPrefix;

                            java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(
                                        ":") + 1);

                            if (!"PriorAuthHeader".equals(type)) {
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext()
                                                               .getNamespaceURI(nsPrefix);

                                return (PriorAuthHeader) ExtensionMapper.getTypeObject(nsUri,
                                    type, reader);
                            }
                        }
                    }

                    // Note all attributes that were handled. Used to differ normal attributes
                    // from anyAttributes.
                    java.util.Vector handledAttributes = new java.util.Vector();

                    reader.next();

                    java.util.ArrayList list2 = new java.util.ArrayList();

                    java.util.ArrayList list3 = new java.util.ArrayList();

                    java.util.ArrayList list8 = new java.util.ArrayList();

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                            new javax.xml.namespace.QName("", "provider").equals(
                                reader.getName())) {
                        object.setProvider(ProviderDTO.Factory.parse(reader));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                            new javax.xml.namespace.QName("", "procedures").equals(
                                reader.getName())) {
                        // Process the array and step past its final element's end.
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                "nil");

                        if ("true".equals(nillableValue) ||
                                "1".equals(nillableValue)) {
                            list2.add(null);
                            reader.next();
                        } else {
                            list2.add(ProcedureDTO.Factory.parse(reader));
                        }

                        //loop until we find a start element that is not part of this array
                        boolean loopDone2 = false;

                        while (!loopDone2) {
                            // We should be at the end element, but make sure
                            while (!reader.isEndElement())
                                reader.next();

                            // Step out of this element
                            reader.next();

                            // Step to next element event.
                            while (!reader.isStartElement() &&
                                    !reader.isEndElement())
                                reader.next();

                            if (reader.isEndElement()) {
                                //two continuous end elements means we are exiting the xml structure
                                loopDone2 = true;
                            } else {
                                if (new javax.xml.namespace.QName("",
                                            "procedures").equals(
                                            reader.getName())) {
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                            "nil");

                                    if ("true".equals(nillableValue) ||
                                            "1".equals(nillableValue)) {
                                        list2.add(null);
                                        reader.next();
                                    } else {
                                        list2.add(ProcedureDTO.Factory.parse(
                                                reader));
                                    }
                                } else {
                                    loopDone2 = true;
                                }
                            }
                        }

                        // call the converter utility  to convert and set the array
                        object.setProcedures((ProcedureDTO[]) org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                ProcedureDTO.class, list2));
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                            new javax.xml.namespace.QName("", "diagnosis").equals(
                                reader.getName())) {
                        // Process the array and step past its final element's end.
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                "nil");

                        if ("true".equals(nillableValue) ||
                                "1".equals(nillableValue)) {
                            list3.add(null);
                            reader.next();
                        } else {
                            list3.add(DiagnosisDTO.Factory.parse(reader));
                        }

                        //loop until we find a start element that is not part of this array
                        boolean loopDone3 = false;

                        while (!loopDone3) {
                            // We should be at the end element, but make sure
                            while (!reader.isEndElement())
                                reader.next();

                            // Step out of this element
                            reader.next();

                            // Step to next element event.
                            while (!reader.isStartElement() &&
                                    !reader.isEndElement())
                                reader.next();

                            if (reader.isEndElement()) {
                                //two continuous end elements means we are exiting the xml structure
                                loopDone3 = true;
                            } else {
                                if (new javax.xml.namespace.QName("", "diagnosis").equals(
                                            reader.getName())) {
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                            "nil");

                                    if ("true".equals(nillableValue) ||
                                            "1".equals(nillableValue)) {
                                        list3.add(null);
                                        reader.next();
                                    } else {
                                        list3.add(DiagnosisDTO.Factory.parse(
                                                reader));
                                    }
                                } else {
                                    loopDone3 = true;
                                }
                            }
                        }

                        // call the converter utility  to convert and set the array
                        object.setDiagnosis((DiagnosisDTO[]) org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                DiagnosisDTO.class, list3));
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                            new javax.xml.namespace.QName("", "appCreateUserId").equals(
                                reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                "nil");

                        if ("true".equals(nillableValue) ||
                                "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "appCreateUserId" +
                                "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setAppCreateUserId(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                            new javax.xml.namespace.QName("", "appMaintUserId").equals(
                                reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                "nil");

                        if ("true".equals(nillableValue) ||
                                "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "appMaintUserId" +
                                "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setAppMaintUserId(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                            new javax.xml.namespace.QName("", "sourceTab").equals(
                                reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                "nil");

                        if ("true".equals(nillableValue) ||
                                "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "sourceTab" +
                                "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setSourceTab(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                            new javax.xml.namespace.QName("", "authorizationKey").equals(
                                reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                "nil");

                        if ("true".equals(nillableValue) ||
                                "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "authorizationKey" +
                                "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setAuthorizationKey(org.apache.axis2.databinding.utils.ConverterUtil.convertToLong(
                                content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                        // A start element we are not expecting indicates an invalid parameter was passed
                        throw new org.apache.axis2.databinding.ADBException(
                            "Unexpected subelement " + reader.getName());
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                            new javax.xml.namespace.QName(
                                "http://services.lbm.avalon.com/",
                                "priorAuthHeaderDTO").equals(reader.getName())) {
                        // Process the array and step past its final element's end.
                        list8.add(PriorAuthHeaderDTO.Factory.parse(reader));

                        //loop until we find a start element that is not part of this array
                        boolean loopDone8 = false;

                        while (!loopDone8) {
                            // We should be at the end element, but make sure
                            while (!reader.isEndElement())
                                reader.next();

                            // Step out of this element
                            reader.next();

                            // Step to next element event.
                            while (!reader.isStartElement() &&
                                    !reader.isEndElement())
                                reader.next();

                            if (reader.isEndElement()) {
                                //two continuous end elements means we are exiting the xml structure
                                loopDone8 = true;
                            } else {
                                if (new javax.xml.namespace.QName(
                                            "http://services.lbm.avalon.com/",
                                            "priorAuthHeaderDTO").equals(
                                            reader.getName())) {
                                    list8.add(PriorAuthHeaderDTO.Factory.parse(
                                            reader));
                                } else {
                                    loopDone8 = true;
                                }
                            }
                        }

                        // call the converter utility  to convert and set the array
                        object.setPriorAuthHeaderDTO((PriorAuthHeaderDTO[]) org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                PriorAuthHeaderDTO.class, list8));
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                            new javax.xml.namespace.QName("", "operationType").equals(
                                reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                "nil");

                        if ("true".equals(nillableValue) ||
                                "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "operationType" +
                                "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setOperationType(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                            new javax.xml.namespace.QName("", "authType").equals(
                                reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                "nil");

                        if ("true".equals(nillableValue) ||
                                "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "authType" +
                                "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setAuthType(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()) {
                        // A start element we are not expecting indicates a trailing invalid property
                        throw new org.apache.axis2.databinding.ADBException(
                            "Unexpected subelement " + reader.getName());
                    }
                } catch (javax.xml.stream.XMLStreamException e) {
                    throw new java.lang.Exception(e);
                }

                return object;
            }
        } //end of factory class
    }

    public static class SavePriorAuthHeaderResponse implements org.apache.axis2.databinding.ADBBean {
        /* This type was generated from the piece of schema that had
           name = savePriorAuthHeaderResponse
           Namespace URI = http://services.lbm.avalon.com/
           Namespace Prefix = ns1
         */

        /**
         * field for _return
         */
        protected PriorAuthHeader local_return;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean local_returnTracker = false;

        public boolean is_returnSpecified() {
            return local_returnTracker;
        }

        /**
         * Auto generated getter method
         * @return PriorAuthHeader
         */
        public PriorAuthHeader get_return() {
            return local_return;
        }

        /**
         * Auto generated setter method
         * @param param _return
         */
        public void set_return(PriorAuthHeader param) {
            local_returnTracker = param != null;

            this.local_return = param;
        }

        /**
         *
         * @param parentQName
         * @param factory
         * @return org.apache.axiom.om.OMElement
         */
        public org.apache.axiom.om.OMElement getOMElement(
            final javax.xml.namespace.QName parentQName,
            final org.apache.axiom.om.OMFactory factory)
            throws org.apache.axis2.databinding.ADBException {
            org.apache.axiom.om.OMDataSource dataSource = new org.apache.axis2.databinding.ADBDataSource(this,
                    parentQName);

            return factory.createOMElement(dataSource, parentQName);
        }

        public void serialize(final javax.xml.namespace.QName parentQName,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException,
                org.apache.axis2.databinding.ADBException {
            serialize(parentQName, xmlWriter, false);
        }

        public void serialize(final javax.xml.namespace.QName parentQName,
            javax.xml.stream.XMLStreamWriter xmlWriter, boolean serializeType)
            throws javax.xml.stream.XMLStreamException,
                org.apache.axis2.databinding.ADBException {
            java.lang.String prefix = null;
            java.lang.String namespace = null;

            prefix = parentQName.getPrefix();
            namespace = parentQName.getNamespaceURI();
            writeStartElement(prefix, namespace, parentQName.getLocalPart(),
                xmlWriter);

            if (serializeType) {
                java.lang.String namespacePrefix = registerPrefix(xmlWriter,
                        "http://services.lbm.avalon.com/");

                if ((namespacePrefix != null) &&
                        (namespacePrefix.trim().length() > 0)) {
                    writeAttribute("xsi",
                        "http://www.w3.org/2001/XMLSchema-instance", "type",
                        namespacePrefix + ":savePriorAuthHeaderResponse",
                        xmlWriter);
                } else {
                    writeAttribute("xsi",
                        "http://www.w3.org/2001/XMLSchema-instance", "type",
                        "savePriorAuthHeaderResponse", xmlWriter);
                }
            }

            if (local_returnTracker) {
                if (local_return == null) {
                    throw new org.apache.axis2.databinding.ADBException(
                        "return cannot be null!!");
                }

                local_return.serialize(new javax.xml.namespace.QName("",
                        "return"), xmlWriter);
            }

            xmlWriter.writeEndElement();
        }

        private static java.lang.String generatePrefix(
            java.lang.String namespace) {
            if (namespace.equals("http://services.lbm.avalon.com/")) {
                return "ns1";
            }

            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix,
            java.lang.String namespace, java.lang.String localPart,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

            if (writerPrefix != null) {
                xmlWriter.writeStartElement(namespace, localPart);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }

        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,
            java.lang.String namespace, java.lang.String attName,
            java.lang.String attValue,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }

            xmlWriter.writeAttribute(namespace, attName, attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,
            java.lang.String attName, java.lang.String attValue,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attValue);
            }
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeQNameAttribute(java.lang.String namespace,
            java.lang.String attName, javax.xml.namespace.QName qname,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String attributeNamespace = qname.getNamespaceURI();
            java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);

            if (attributePrefix == null) {
                attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
            }

            java.lang.String attributeValue;

            if (attributePrefix.trim().length() > 0) {
                attributeValue = attributePrefix + ":" + qname.getLocalPart();
            } else {
                attributeValue = qname.getLocalPart();
            }

            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attributeValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attributeValue);
            }
        }

        /**
         *  method to handle Qnames
         */
        private void writeQName(javax.xml.namespace.QName qname,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();

            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);

                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix, namespaceURI);
                }

                if (prefix.trim().length() > 0) {
                    xmlWriter.writeCharacters(prefix + ":" +
                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            qname));
                }
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }

                    namespaceURI = qnames[i].getNamespaceURI();

                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);

                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix, namespaceURI);
                        }

                        if (prefix.trim().length() > 0) {
                            stringToWrite.append(prefix).append(":")
                                         .append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                    qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                    qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                qnames[i]));
                    }
                }

                xmlWriter.writeCharacters(stringToWrite.toString());
            }
        }

        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(
            javax.xml.stream.XMLStreamWriter xmlWriter,
            java.lang.String namespace)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);

            if (prefix == null) {
                prefix = generatePrefix(namespace);

                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();

                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);

                    if ((uri == null) || (uri.length() == 0)) {
                        break;
                    }

                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }

                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }

            return prefix;
        }

        /**
         * databinding method to get an XML representation of this object
         *
         */
        public javax.xml.stream.XMLStreamReader getPullParser(
            javax.xml.namespace.QName qName)
            throws org.apache.axis2.databinding.ADBException {
            java.util.ArrayList elementList = new java.util.ArrayList();
            java.util.ArrayList attribList = new java.util.ArrayList();

            if (local_returnTracker) {
                elementList.add(new javax.xml.namespace.QName("", "return"));

                if (local_return == null) {
                    throw new org.apache.axis2.databinding.ADBException(
                        "return cannot be null!!");
                }

                elementList.add(local_return);
            }

            return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName,
                elementList.toArray(), attribList.toArray());
        }

        /**
         *  Factory class that keeps the parse method
         */
        public static class Factory {
            /**
             * static method to create the object
             * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
             *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
             * Postcondition: If this object is an element, the reader is positioned at its end element
             *                If this object is a complex type, the reader is positioned at the end element of its outer element
             */
            public static SavePriorAuthHeaderResponse parse(
                javax.xml.stream.XMLStreamReader reader)
                throws java.lang.Exception {
                SavePriorAuthHeaderResponse object = new SavePriorAuthHeaderResponse();

                int event;
                java.lang.String nillableValue = null;
                java.lang.String prefix = "";
                java.lang.String namespaceuri = "";

                try {
                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.getAttributeValue(
                                "http://www.w3.org/2001/XMLSchema-instance",
                                "type") != null) {
                        java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                "type");

                        if (fullTypeName != null) {
                            java.lang.String nsPrefix = null;

                            if (fullTypeName.indexOf(":") > -1) {
                                nsPrefix = fullTypeName.substring(0,
                                        fullTypeName.indexOf(":"));
                            }

                            nsPrefix = (nsPrefix == null) ? "" : nsPrefix;

                            java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(
                                        ":") + 1);

                            if (!"savePriorAuthHeaderResponse".equals(type)) {
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext()
                                                               .getNamespaceURI(nsPrefix);

                                return (SavePriorAuthHeaderResponse) ExtensionMapper.getTypeObject(nsUri,
                                    type, reader);
                            }
                        }
                    }

                    // Note all attributes that were handled. Used to differ normal attributes
                    // from anyAttributes.
                    java.util.Vector handledAttributes = new java.util.Vector();

                    reader.next();

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                            new javax.xml.namespace.QName("", "return").equals(
                                reader.getName())) {
                        object.set_return(PriorAuthHeader.Factory.parse(reader));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()) {
                        // A start element we are not expecting indicates a trailing invalid property
                        throw new org.apache.axis2.databinding.ADBException(
                            "Unexpected subelement " + reader.getName());
                    }
                } catch (javax.xml.stream.XMLStreamException e) {
                    throw new java.lang.Exception(e);
                }

                return object;
            }
        } //end of factory class
    }

    public static class GetPriorAuthHeaderResponseE implements org.apache.axis2.databinding.ADBBean {
        public static final javax.xml.namespace.QName MY_QNAME = new javax.xml.namespace.QName("http://services.lbm.avalon.com/",
                "getPriorAuthHeaderResponse", "ns1");

        /**
         * field for GetPriorAuthHeaderResponse
         */
        protected GetPriorAuthHeaderResponse localGetPriorAuthHeaderResponse;

        /**
         * Auto generated getter method
         * @return GetPriorAuthHeaderResponse
         */
        public GetPriorAuthHeaderResponse getGetPriorAuthHeaderResponse() {
            return localGetPriorAuthHeaderResponse;
        }

        /**
         * Auto generated setter method
         * @param param GetPriorAuthHeaderResponse
         */
        public void setGetPriorAuthHeaderResponse(
            GetPriorAuthHeaderResponse param) {
            this.localGetPriorAuthHeaderResponse = param;
        }

        /**
         *
         * @param parentQName
         * @param factory
         * @return org.apache.axiom.om.OMElement
         */
        public org.apache.axiom.om.OMElement getOMElement(
            final javax.xml.namespace.QName parentQName,
            final org.apache.axiom.om.OMFactory factory)
            throws org.apache.axis2.databinding.ADBException {
            org.apache.axiom.om.OMDataSource dataSource = new org.apache.axis2.databinding.ADBDataSource(this,
                    MY_QNAME);

            return factory.createOMElement(dataSource, MY_QNAME);
        }

        public void serialize(final javax.xml.namespace.QName parentQName,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException,
                org.apache.axis2.databinding.ADBException {
            serialize(parentQName, xmlWriter, false);
        }

        public void serialize(final javax.xml.namespace.QName parentQName,
            javax.xml.stream.XMLStreamWriter xmlWriter, boolean serializeType)
            throws javax.xml.stream.XMLStreamException,
                org.apache.axis2.databinding.ADBException {
            //We can safely assume an element has only one type associated with it
            if (localGetPriorAuthHeaderResponse == null) {
                throw new org.apache.axis2.databinding.ADBException(
                    "getPriorAuthHeaderResponse cannot be null!");
            }

            localGetPriorAuthHeaderResponse.serialize(MY_QNAME, xmlWriter);
        }

        private static java.lang.String generatePrefix(
            java.lang.String namespace) {
            if (namespace.equals("http://services.lbm.avalon.com/")) {
                return "ns1";
            }

            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix,
            java.lang.String namespace, java.lang.String localPart,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

            if (writerPrefix != null) {
                xmlWriter.writeStartElement(namespace, localPart);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }

        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,
            java.lang.String namespace, java.lang.String attName,
            java.lang.String attValue,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }

            xmlWriter.writeAttribute(namespace, attName, attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,
            java.lang.String attName, java.lang.String attValue,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attValue);
            }
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeQNameAttribute(java.lang.String namespace,
            java.lang.String attName, javax.xml.namespace.QName qname,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String attributeNamespace = qname.getNamespaceURI();
            java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);

            if (attributePrefix == null) {
                attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
            }

            java.lang.String attributeValue;

            if (attributePrefix.trim().length() > 0) {
                attributeValue = attributePrefix + ":" + qname.getLocalPart();
            } else {
                attributeValue = qname.getLocalPart();
            }

            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attributeValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attributeValue);
            }
        }

        /**
         *  method to handle Qnames
         */
        private void writeQName(javax.xml.namespace.QName qname,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();

            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);

                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix, namespaceURI);
                }

                if (prefix.trim().length() > 0) {
                    xmlWriter.writeCharacters(prefix + ":" +
                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            qname));
                }
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }

                    namespaceURI = qnames[i].getNamespaceURI();

                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);

                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix, namespaceURI);
                        }

                        if (prefix.trim().length() > 0) {
                            stringToWrite.append(prefix).append(":")
                                         .append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                    qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                    qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                qnames[i]));
                    }
                }

                xmlWriter.writeCharacters(stringToWrite.toString());
            }
        }

        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(
            javax.xml.stream.XMLStreamWriter xmlWriter,
            java.lang.String namespace)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);

            if (prefix == null) {
                prefix = generatePrefix(namespace);

                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();

                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);

                    if ((uri == null) || (uri.length() == 0)) {
                        break;
                    }

                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }

                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }

            return prefix;
        }

        /**
         * databinding method to get an XML representation of this object
         *
         */
        public javax.xml.stream.XMLStreamReader getPullParser(
            javax.xml.namespace.QName qName)
            throws org.apache.axis2.databinding.ADBException {
            //We can safely assume an element has only one type associated with it
            return localGetPriorAuthHeaderResponse.getPullParser(MY_QNAME);
        }

        /**
         *  Factory class that keeps the parse method
         */
        public static class Factory {
            /**
             * static method to create the object
             * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
             *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
             * Postcondition: If this object is an element, the reader is positioned at its end element
             *                If this object is a complex type, the reader is positioned at the end element of its outer element
             */
            public static GetPriorAuthHeaderResponseE parse(
                javax.xml.stream.XMLStreamReader reader)
                throws java.lang.Exception {
                GetPriorAuthHeaderResponseE object = new GetPriorAuthHeaderResponseE();

                int event;
                java.lang.String nillableValue = null;
                java.lang.String prefix = "";
                java.lang.String namespaceuri = "";

                try {
                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    // Note all attributes that were handled. Used to differ normal attributes
                    // from anyAttributes.
                    java.util.Vector handledAttributes = new java.util.Vector();

                    while (!reader.isEndElement()) {
                        if (reader.isStartElement()) {
                            if (reader.isStartElement() &&
                                    new javax.xml.namespace.QName(
                                        "http://services.lbm.avalon.com/",
                                        "getPriorAuthHeaderResponse").equals(
                                        reader.getName())) {
                                object.setGetPriorAuthHeaderResponse(GetPriorAuthHeaderResponse.Factory.parse(
                                        reader));
                            } // End of if for expected property start element

                            else {
                                // A start element we are not expecting indicates an invalid parameter was passed
                                throw new org.apache.axis2.databinding.ADBException(
                                    "Unexpected subelement " +
                                    reader.getName());
                            }
                        } else {
                            reader.next();
                        }
                    } // end of while loop
                } catch (javax.xml.stream.XMLStreamException e) {
                    throw new java.lang.Exception(e);
                }

                return object;
            }
        } //end of factory class
    }

    public static class SavePriorAuthHeader implements org.apache.axis2.databinding.ADBBean {
        /* This type was generated from the piece of schema that had
           name = savePriorAuthHeader
           Namespace URI = http://services.lbm.avalon.com/
           Namespace Prefix = ns1
         */

        /**
         * field for SavePriorAuthHeaderRequest
         */
        protected PriorAuthHeader localSavePriorAuthHeaderRequest;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localSavePriorAuthHeaderRequestTracker = false;

        public boolean isSavePriorAuthHeaderRequestSpecified() {
            return localSavePriorAuthHeaderRequestTracker;
        }

        /**
         * Auto generated getter method
         * @return PriorAuthHeader
         */
        public PriorAuthHeader getSavePriorAuthHeaderRequest() {
            return localSavePriorAuthHeaderRequest;
        }

        /**
         * Auto generated setter method
         * @param param SavePriorAuthHeaderRequest
         */
        public void setSavePriorAuthHeaderRequest(PriorAuthHeader param) {
            localSavePriorAuthHeaderRequestTracker = param != null;

            this.localSavePriorAuthHeaderRequest = param;
        }

        /**
         *
         * @param parentQName
         * @param factory
         * @return org.apache.axiom.om.OMElement
         */
        public org.apache.axiom.om.OMElement getOMElement(
            final javax.xml.namespace.QName parentQName,
            final org.apache.axiom.om.OMFactory factory)
            throws org.apache.axis2.databinding.ADBException {
            org.apache.axiom.om.OMDataSource dataSource = new org.apache.axis2.databinding.ADBDataSource(this,
                    parentQName);

            return factory.createOMElement(dataSource, parentQName);
        }

        public void serialize(final javax.xml.namespace.QName parentQName,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException,
                org.apache.axis2.databinding.ADBException {
            serialize(parentQName, xmlWriter, false);
        }

        public void serialize(final javax.xml.namespace.QName parentQName,
            javax.xml.stream.XMLStreamWriter xmlWriter, boolean serializeType)
            throws javax.xml.stream.XMLStreamException,
                org.apache.axis2.databinding.ADBException {
            java.lang.String prefix = null;
            java.lang.String namespace = null;

            prefix = parentQName.getPrefix();
            namespace = parentQName.getNamespaceURI();
            writeStartElement(prefix, namespace, parentQName.getLocalPart(),
                xmlWriter);

            if (serializeType) {
                java.lang.String namespacePrefix = registerPrefix(xmlWriter,
                        "http://services.lbm.avalon.com/");

                if ((namespacePrefix != null) &&
                        (namespacePrefix.trim().length() > 0)) {
                    writeAttribute("xsi",
                        "http://www.w3.org/2001/XMLSchema-instance", "type",
                        namespacePrefix + ":savePriorAuthHeader", xmlWriter);
                } else {
                    writeAttribute("xsi",
                        "http://www.w3.org/2001/XMLSchema-instance", "type",
                        "savePriorAuthHeader", xmlWriter);
                }
            }

            if (localSavePriorAuthHeaderRequestTracker) {
                if (localSavePriorAuthHeaderRequest == null) {
                    throw new org.apache.axis2.databinding.ADBException(
                        "savePriorAuthHeaderRequest cannot be null!!");
                }

                localSavePriorAuthHeaderRequest.serialize(new javax.xml.namespace.QName(
                        "http://services.lbm.avalon.com/",
                        "savePriorAuthHeaderRequest"), xmlWriter);
            }

            xmlWriter.writeEndElement();
        }

        private static java.lang.String generatePrefix(
            java.lang.String namespace) {
            if (namespace.equals("http://services.lbm.avalon.com/")) {
                return "ns1";
            }

            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix,
            java.lang.String namespace, java.lang.String localPart,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

            if (writerPrefix != null) {
                xmlWriter.writeStartElement(namespace, localPart);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }

        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,
            java.lang.String namespace, java.lang.String attName,
            java.lang.String attValue,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }

            xmlWriter.writeAttribute(namespace, attName, attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,
            java.lang.String attName, java.lang.String attValue,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attValue);
            }
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeQNameAttribute(java.lang.String namespace,
            java.lang.String attName, javax.xml.namespace.QName qname,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String attributeNamespace = qname.getNamespaceURI();
            java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);

            if (attributePrefix == null) {
                attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
            }

            java.lang.String attributeValue;

            if (attributePrefix.trim().length() > 0) {
                attributeValue = attributePrefix + ":" + qname.getLocalPart();
            } else {
                attributeValue = qname.getLocalPart();
            }

            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attributeValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attributeValue);
            }
        }

        /**
         *  method to handle Qnames
         */
        private void writeQName(javax.xml.namespace.QName qname,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();

            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);

                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix, namespaceURI);
                }

                if (prefix.trim().length() > 0) {
                    xmlWriter.writeCharacters(prefix + ":" +
                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            qname));
                }
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }

                    namespaceURI = qnames[i].getNamespaceURI();

                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);

                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix, namespaceURI);
                        }

                        if (prefix.trim().length() > 0) {
                            stringToWrite.append(prefix).append(":")
                                         .append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                    qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                    qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                qnames[i]));
                    }
                }

                xmlWriter.writeCharacters(stringToWrite.toString());
            }
        }

        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(
            javax.xml.stream.XMLStreamWriter xmlWriter,
            java.lang.String namespace)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);

            if (prefix == null) {
                prefix = generatePrefix(namespace);

                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();

                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);

                    if ((uri == null) || (uri.length() == 0)) {
                        break;
                    }

                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }

                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }

            return prefix;
        }

        /**
         * databinding method to get an XML representation of this object
         *
         */
        public javax.xml.stream.XMLStreamReader getPullParser(
            javax.xml.namespace.QName qName)
            throws org.apache.axis2.databinding.ADBException {
            java.util.ArrayList elementList = new java.util.ArrayList();
            java.util.ArrayList attribList = new java.util.ArrayList();

            if (localSavePriorAuthHeaderRequestTracker) {
                elementList.add(new javax.xml.namespace.QName(
                        "http://services.lbm.avalon.com/",
                        "savePriorAuthHeaderRequest"));

                if (localSavePriorAuthHeaderRequest == null) {
                    throw new org.apache.axis2.databinding.ADBException(
                        "savePriorAuthHeaderRequest cannot be null!!");
                }

                elementList.add(localSavePriorAuthHeaderRequest);
            }

            return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName,
                elementList.toArray(), attribList.toArray());
        }

        /**
         *  Factory class that keeps the parse method
         */
        public static class Factory {
            /**
             * static method to create the object
             * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
             *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
             * Postcondition: If this object is an element, the reader is positioned at its end element
             *                If this object is a complex type, the reader is positioned at the end element of its outer element
             */
            public static SavePriorAuthHeader parse(
                javax.xml.stream.XMLStreamReader reader)
                throws java.lang.Exception {
                SavePriorAuthHeader object = new SavePriorAuthHeader();

                int event;
                java.lang.String nillableValue = null;
                java.lang.String prefix = "";
                java.lang.String namespaceuri = "";

                try {
                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.getAttributeValue(
                                "http://www.w3.org/2001/XMLSchema-instance",
                                "type") != null) {
                        java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                "type");

                        if (fullTypeName != null) {
                            java.lang.String nsPrefix = null;

                            if (fullTypeName.indexOf(":") > -1) {
                                nsPrefix = fullTypeName.substring(0,
                                        fullTypeName.indexOf(":"));
                            }

                            nsPrefix = (nsPrefix == null) ? "" : nsPrefix;

                            java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(
                                        ":") + 1);

                            if (!"savePriorAuthHeader".equals(type)) {
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext()
                                                               .getNamespaceURI(nsPrefix);

                                return (SavePriorAuthHeader) ExtensionMapper.getTypeObject(nsUri,
                                    type, reader);
                            }
                        }
                    }

                    // Note all attributes that were handled. Used to differ normal attributes
                    // from anyAttributes.
                    java.util.Vector handledAttributes = new java.util.Vector();

                    reader.next();

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                            new javax.xml.namespace.QName(
                                "http://services.lbm.avalon.com/",
                                "savePriorAuthHeaderRequest").equals(
                                reader.getName())) {
                        object.setSavePriorAuthHeaderRequest(PriorAuthHeader.Factory.parse(
                                reader));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()) {
                        // A start element we are not expecting indicates a trailing invalid property
                        throw new org.apache.axis2.databinding.ADBException(
                            "Unexpected subelement " + reader.getName());
                    }
                } catch (javax.xml.stream.XMLStreamException e) {
                    throw new java.lang.Exception(e);
                }

                return object;
            }
        } //end of factory class
    }

    public static class DiagnosisDTO implements org.apache.axis2.databinding.ADBBean {
        /* This type was generated from the piece of schema that had
           name = diagnosisDTO
           Namespace URI = http://services.lbm.avalon.com/
           Namespace Prefix = ns1
         */

        /**
         * field for SequenceNumber
         */
        protected int localSequenceNumber;

        /**
         * field for IcdDiagnosisCode
         */
        protected java.lang.String localIcdDiagnosisCode;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localIcdDiagnosisCodeTracker = false;

        /**
         * Auto generated getter method
         * @return int
         */
        public int getSequenceNumber() {
            return localSequenceNumber;
        }

        /**
         * Auto generated setter method
         * @param param SequenceNumber
         */
        public void setSequenceNumber(int param) {
            this.localSequenceNumber = param;
        }

        public boolean isIcdDiagnosisCodeSpecified() {
            return localIcdDiagnosisCodeTracker;
        }

        /**
         * Auto generated getter method
         * @return java.lang.String
         */
        public java.lang.String getIcdDiagnosisCode() {
            return localIcdDiagnosisCode;
        }

        /**
         * Auto generated setter method
         * @param param IcdDiagnosisCode
         */
        public void setIcdDiagnosisCode(java.lang.String param) {
            localIcdDiagnosisCodeTracker = param != null;

            this.localIcdDiagnosisCode = param;
        }

        /**
         *
         * @param parentQName
         * @param factory
         * @return org.apache.axiom.om.OMElement
         */
        public org.apache.axiom.om.OMElement getOMElement(
            final javax.xml.namespace.QName parentQName,
            final org.apache.axiom.om.OMFactory factory)
            throws org.apache.axis2.databinding.ADBException {
            org.apache.axiom.om.OMDataSource dataSource = new org.apache.axis2.databinding.ADBDataSource(this,
                    parentQName);

            return factory.createOMElement(dataSource, parentQName);
        }

        public void serialize(final javax.xml.namespace.QName parentQName,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException,
                org.apache.axis2.databinding.ADBException {
            serialize(parentQName, xmlWriter, false);
        }

        public void serialize(final javax.xml.namespace.QName parentQName,
            javax.xml.stream.XMLStreamWriter xmlWriter, boolean serializeType)
            throws javax.xml.stream.XMLStreamException,
                org.apache.axis2.databinding.ADBException {
            java.lang.String prefix = null;
            java.lang.String namespace = null;

            prefix = parentQName.getPrefix();
            namespace = parentQName.getNamespaceURI();
            writeStartElement(prefix, namespace, parentQName.getLocalPart(),
                xmlWriter);

            if (serializeType) {
                java.lang.String namespacePrefix = registerPrefix(xmlWriter,
                        "http://services.lbm.avalon.com/");

                if ((namespacePrefix != null) &&
                        (namespacePrefix.trim().length() > 0)) {
                    writeAttribute("xsi",
                        "http://www.w3.org/2001/XMLSchema-instance", "type",
                        namespacePrefix + ":diagnosisDTO", xmlWriter);
                } else {
                    writeAttribute("xsi",
                        "http://www.w3.org/2001/XMLSchema-instance", "type",
                        "diagnosisDTO", xmlWriter);
                }
            }

            namespace = "";
            writeStartElement(null, namespace, "SequenceNumber", xmlWriter);

            if (localSequenceNumber == java.lang.Integer.MIN_VALUE) {
                throw new org.apache.axis2.databinding.ADBException(
                    "SequenceNumber cannot be null!!");
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        localSequenceNumber));
            }

            xmlWriter.writeEndElement();

            if (localIcdDiagnosisCodeTracker) {
                namespace = "";
                writeStartElement(null, namespace, "icdDiagnosisCode", xmlWriter);

                if (localIcdDiagnosisCode == null) {
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException(
                        "icdDiagnosisCode cannot be null!!");
                } else {
                    xmlWriter.writeCharacters(localIcdDiagnosisCode);
                }

                xmlWriter.writeEndElement();
            }

            xmlWriter.writeEndElement();
        }

        private static java.lang.String generatePrefix(
            java.lang.String namespace) {
            if (namespace.equals("http://services.lbm.avalon.com/")) {
                return "ns1";
            }

            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix,
            java.lang.String namespace, java.lang.String localPart,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

            if (writerPrefix != null) {
                xmlWriter.writeStartElement(namespace, localPart);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }

        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,
            java.lang.String namespace, java.lang.String attName,
            java.lang.String attValue,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }

            xmlWriter.writeAttribute(namespace, attName, attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,
            java.lang.String attName, java.lang.String attValue,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attValue);
            }
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeQNameAttribute(java.lang.String namespace,
            java.lang.String attName, javax.xml.namespace.QName qname,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String attributeNamespace = qname.getNamespaceURI();
            java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);

            if (attributePrefix == null) {
                attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
            }

            java.lang.String attributeValue;

            if (attributePrefix.trim().length() > 0) {
                attributeValue = attributePrefix + ":" + qname.getLocalPart();
            } else {
                attributeValue = qname.getLocalPart();
            }

            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attributeValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attributeValue);
            }
        }

        /**
         *  method to handle Qnames
         */
        private void writeQName(javax.xml.namespace.QName qname,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();

            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);

                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix, namespaceURI);
                }

                if (prefix.trim().length() > 0) {
                    xmlWriter.writeCharacters(prefix + ":" +
                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            qname));
                }
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }

                    namespaceURI = qnames[i].getNamespaceURI();

                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);

                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix, namespaceURI);
                        }

                        if (prefix.trim().length() > 0) {
                            stringToWrite.append(prefix).append(":")
                                         .append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                    qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                    qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                qnames[i]));
                    }
                }

                xmlWriter.writeCharacters(stringToWrite.toString());
            }
        }

        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(
            javax.xml.stream.XMLStreamWriter xmlWriter,
            java.lang.String namespace)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);

            if (prefix == null) {
                prefix = generatePrefix(namespace);

                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();

                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);

                    if ((uri == null) || (uri.length() == 0)) {
                        break;
                    }

                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }

                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }

            return prefix;
        }

        /**
         * databinding method to get an XML representation of this object
         *
         */
        public javax.xml.stream.XMLStreamReader getPullParser(
            javax.xml.namespace.QName qName)
            throws org.apache.axis2.databinding.ADBException {
            java.util.ArrayList elementList = new java.util.ArrayList();
            java.util.ArrayList attribList = new java.util.ArrayList();

            elementList.add(new javax.xml.namespace.QName("", "SequenceNumber"));

            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                    localSequenceNumber));

            if (localIcdDiagnosisCodeTracker) {
                elementList.add(new javax.xml.namespace.QName("",
                        "icdDiagnosisCode"));

                if (localIcdDiagnosisCode != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            localIcdDiagnosisCode));
                } else {
                    throw new org.apache.axis2.databinding.ADBException(
                        "icdDiagnosisCode cannot be null!!");
                }
            }

            return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName,
                elementList.toArray(), attribList.toArray());
        }

        /**
         *  Factory class that keeps the parse method
         */
        public static class Factory {
            /**
             * static method to create the object
             * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
             *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
             * Postcondition: If this object is an element, the reader is positioned at its end element
             *                If this object is a complex type, the reader is positioned at the end element of its outer element
             */
            public static DiagnosisDTO parse(
                javax.xml.stream.XMLStreamReader reader)
                throws java.lang.Exception {
                DiagnosisDTO object = new DiagnosisDTO();

                int event;
                java.lang.String nillableValue = null;
                java.lang.String prefix = "";
                java.lang.String namespaceuri = "";

                try {
                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.getAttributeValue(
                                "http://www.w3.org/2001/XMLSchema-instance",
                                "type") != null) {
                        java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                "type");

                        if (fullTypeName != null) {
                            java.lang.String nsPrefix = null;

                            if (fullTypeName.indexOf(":") > -1) {
                                nsPrefix = fullTypeName.substring(0,
                                        fullTypeName.indexOf(":"));
                            }

                            nsPrefix = (nsPrefix == null) ? "" : nsPrefix;

                            java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(
                                        ":") + 1);

                            if (!"diagnosisDTO".equals(type)) {
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext()
                                                               .getNamespaceURI(nsPrefix);

                                return (DiagnosisDTO) ExtensionMapper.getTypeObject(nsUri,
                                    type, reader);
                            }
                        }
                    }

                    // Note all attributes that were handled. Used to differ normal attributes
                    // from anyAttributes.
                    java.util.Vector handledAttributes = new java.util.Vector();

                    reader.next();

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                            new javax.xml.namespace.QName("", "SequenceNumber").equals(
                                reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                "nil");

                        if ("true".equals(nillableValue) ||
                                "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "SequenceNumber" +
                                "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setSequenceNumber(org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(
                                content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                        // A start element we are not expecting indicates an invalid parameter was passed
                        throw new org.apache.axis2.databinding.ADBException(
                            "Unexpected subelement " + reader.getName());
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                            new javax.xml.namespace.QName("", "icdDiagnosisCode").equals(
                                reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                "nil");

                        if ("true".equals(nillableValue) ||
                                "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "icdDiagnosisCode" +
                                "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setIcdDiagnosisCode(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()) {
                        // A start element we are not expecting indicates a trailing invalid property
                        throw new org.apache.axis2.databinding.ADBException(
                            "Unexpected subelement " + reader.getName());
                    }
                } catch (javax.xml.stream.XMLStreamException e) {
                    throw new java.lang.Exception(e);
                }

                return object;
            }
        } //end of factory class
    }

    public static class PriorAuthorizationSearchResponseE implements org.apache.axis2.databinding.ADBBean {
        public static final javax.xml.namespace.QName MY_QNAME = new javax.xml.namespace.QName("http://services.lbm.avalon.com/",
                "priorAuthorizationSearchResponse", "ns1");

        /**
         * field for PriorAuthorizationSearchResponse
         */
        protected PriorAuthorizationSearchResponse localPriorAuthorizationSearchResponse;

        /**
         * Auto generated getter method
         * @return PriorAuthorizationSearchResponse
         */
        public PriorAuthorizationSearchResponse getPriorAuthorizationSearchResponse() {
            return localPriorAuthorizationSearchResponse;
        }

        /**
         * Auto generated setter method
         * @param param PriorAuthorizationSearchResponse
         */
        public void setPriorAuthorizationSearchResponse(
            PriorAuthorizationSearchResponse param) {
            this.localPriorAuthorizationSearchResponse = param;
        }

        /**
         *
         * @param parentQName
         * @param factory
         * @return org.apache.axiom.om.OMElement
         */
        public org.apache.axiom.om.OMElement getOMElement(
            final javax.xml.namespace.QName parentQName,
            final org.apache.axiom.om.OMFactory factory)
            throws org.apache.axis2.databinding.ADBException {
            org.apache.axiom.om.OMDataSource dataSource = new org.apache.axis2.databinding.ADBDataSource(this,
                    MY_QNAME);

            return factory.createOMElement(dataSource, MY_QNAME);
        }

        public void serialize(final javax.xml.namespace.QName parentQName,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException,
                org.apache.axis2.databinding.ADBException {
            serialize(parentQName, xmlWriter, false);
        }

        public void serialize(final javax.xml.namespace.QName parentQName,
            javax.xml.stream.XMLStreamWriter xmlWriter, boolean serializeType)
            throws javax.xml.stream.XMLStreamException,
                org.apache.axis2.databinding.ADBException {
            //We can safely assume an element has only one type associated with it
            if (localPriorAuthorizationSearchResponse == null) {
                throw new org.apache.axis2.databinding.ADBException(
                    "priorAuthorizationSearchResponse cannot be null!");
            }

            localPriorAuthorizationSearchResponse.serialize(MY_QNAME, xmlWriter);
        }

        private static java.lang.String generatePrefix(
            java.lang.String namespace) {
            if (namespace.equals("http://services.lbm.avalon.com/")) {
                return "ns1";
            }

            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix,
            java.lang.String namespace, java.lang.String localPart,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

            if (writerPrefix != null) {
                xmlWriter.writeStartElement(namespace, localPart);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }

        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,
            java.lang.String namespace, java.lang.String attName,
            java.lang.String attValue,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }

            xmlWriter.writeAttribute(namespace, attName, attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,
            java.lang.String attName, java.lang.String attValue,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attValue);
            }
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeQNameAttribute(java.lang.String namespace,
            java.lang.String attName, javax.xml.namespace.QName qname,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String attributeNamespace = qname.getNamespaceURI();
            java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);

            if (attributePrefix == null) {
                attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
            }

            java.lang.String attributeValue;

            if (attributePrefix.trim().length() > 0) {
                attributeValue = attributePrefix + ":" + qname.getLocalPart();
            } else {
                attributeValue = qname.getLocalPart();
            }

            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attributeValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attributeValue);
            }
        }

        /**
         *  method to handle Qnames
         */
        private void writeQName(javax.xml.namespace.QName qname,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();

            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);

                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix, namespaceURI);
                }

                if (prefix.trim().length() > 0) {
                    xmlWriter.writeCharacters(prefix + ":" +
                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            qname));
                }
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }

                    namespaceURI = qnames[i].getNamespaceURI();

                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);

                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix, namespaceURI);
                        }

                        if (prefix.trim().length() > 0) {
                            stringToWrite.append(prefix).append(":")
                                         .append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                    qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                    qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                qnames[i]));
                    }
                }

                xmlWriter.writeCharacters(stringToWrite.toString());
            }
        }

        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(
            javax.xml.stream.XMLStreamWriter xmlWriter,
            java.lang.String namespace)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);

            if (prefix == null) {
                prefix = generatePrefix(namespace);

                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();

                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);

                    if ((uri == null) || (uri.length() == 0)) {
                        break;
                    }

                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }

                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }

            return prefix;
        }

        /**
         * databinding method to get an XML representation of this object
         *
         */
        public javax.xml.stream.XMLStreamReader getPullParser(
            javax.xml.namespace.QName qName)
            throws org.apache.axis2.databinding.ADBException {
            //We can safely assume an element has only one type associated with it
            return localPriorAuthorizationSearchResponse.getPullParser(MY_QNAME);
        }

        /**
         *  Factory class that keeps the parse method
         */
        public static class Factory {
            /**
             * static method to create the object
             * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
             *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
             * Postcondition: If this object is an element, the reader is positioned at its end element
             *                If this object is a complex type, the reader is positioned at the end element of its outer element
             */
            public static PriorAuthorizationSearchResponseE parse(
                javax.xml.stream.XMLStreamReader reader)
                throws java.lang.Exception {
                PriorAuthorizationSearchResponseE object = new PriorAuthorizationSearchResponseE();

                int event;
                java.lang.String nillableValue = null;
                java.lang.String prefix = "";
                java.lang.String namespaceuri = "";

                try {
                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    // Note all attributes that were handled. Used to differ normal attributes
                    // from anyAttributes.
                    java.util.Vector handledAttributes = new java.util.Vector();

                    while (!reader.isEndElement()) {
                        if (reader.isStartElement()) {
                            if (reader.isStartElement() &&
                                    new javax.xml.namespace.QName(
                                        "http://services.lbm.avalon.com/",
                                        "priorAuthorizationSearchResponse").equals(
                                        reader.getName())) {
                                object.setPriorAuthorizationSearchResponse(PriorAuthorizationSearchResponse.Factory.parse(
                                        reader));
                            } // End of if for expected property start element

                            else {
                                // A start element we are not expecting indicates an invalid parameter was passed
                                throw new org.apache.axis2.databinding.ADBException(
                                    "Unexpected subelement " +
                                    reader.getName());
                            }
                        } else {
                            reader.next();
                        }
                    } // end of while loop
                } catch (javax.xml.stream.XMLStreamException e) {
                    throw new java.lang.Exception(e);
                }

                return object;
            }
        } //end of factory class
    }

    public static class ProcedureDTO implements org.apache.axis2.databinding.ADBBean {
        /* This type was generated from the piece of schema that had
           name = procedureDTO
           Namespace URI = http://services.lbm.avalon.com/
           Namespace Prefix = ns1
         */

        /**
         * field for ProcedureFromCode
         */
        protected java.lang.String localProcedureFromCode;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localProcedureFromCodeTracker = false;

        /**
         * field for ProcedureToCode
         */
        protected java.lang.String localProcedureToCode;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localProcedureToCodeTracker = false;

        /**
         * field for ProcedureDecision
         */
        protected java.lang.String localProcedureDecision;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localProcedureDecisionTracker = false;

        /**
         * field for ProcedureDecisionReason
         */
        protected java.lang.String localProcedureDecisionReason;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localProcedureDecisionReasonTracker = false;

        /**
         * field for ProcedureUnits
         */
        protected float localProcedureUnits;

        /**
         * field for LabTestTypeName
         */
        protected java.lang.String localLabTestTypeName;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localLabTestTypeNameTracker = false;

        public boolean isProcedureFromCodeSpecified() {
            return localProcedureFromCodeTracker;
        }

        /**
         * Auto generated getter method
         * @return java.lang.String
         */
        public java.lang.String getProcedureFromCode() {
            return localProcedureFromCode;
        }

        /**
         * Auto generated setter method
         * @param param ProcedureFromCode
         */
        public void setProcedureFromCode(java.lang.String param) {
            localProcedureFromCodeTracker = param != null;

            this.localProcedureFromCode = param;
        }

        public boolean isProcedureToCodeSpecified() {
            return localProcedureToCodeTracker;
        }

        /**
         * Auto generated getter method
         * @return java.lang.String
         */
        public java.lang.String getProcedureToCode() {
            return localProcedureToCode;
        }

        /**
         * Auto generated setter method
         * @param param ProcedureToCode
         */
        public void setProcedureToCode(java.lang.String param) {
            localProcedureToCodeTracker = param != null;

            this.localProcedureToCode = param;
        }

        public boolean isProcedureDecisionSpecified() {
            return localProcedureDecisionTracker;
        }

        /**
         * Auto generated getter method
         * @return java.lang.String
         */
        public java.lang.String getProcedureDecision() {
            return localProcedureDecision;
        }

        /**
         * Auto generated setter method
         * @param param ProcedureDecision
         */
        public void setProcedureDecision(java.lang.String param) {
            localProcedureDecisionTracker = param != null;

            this.localProcedureDecision = param;
        }

        public boolean isProcedureDecisionReasonSpecified() {
            return localProcedureDecisionReasonTracker;
        }

        /**
         * Auto generated getter method
         * @return java.lang.String
         */
        public java.lang.String getProcedureDecisionReason() {
            return localProcedureDecisionReason;
        }

        /**
         * Auto generated setter method
         * @param param ProcedureDecisionReason
         */
        public void setProcedureDecisionReason(java.lang.String param) {
            localProcedureDecisionReasonTracker = param != null;

            this.localProcedureDecisionReason = param;
        }

        /**
         * Auto generated getter method
         * @return float
         */
        public float getProcedureUnits() {
            return localProcedureUnits;
        }

        /**
         * Auto generated setter method
         * @param param ProcedureUnits
         */
        public void setProcedureUnits(float param) {
            this.localProcedureUnits = param;
        }

		// fasttrack added Jan 19, 2017
        /**
        * field for ProcedureFastTrack
        */
		protected java.lang.String localProcedureFastTrack ;
                                
       /*  This tracker boolean wil be used to detect whether the user called the set method
        *   for this attribute. It will be used to determine whether to include this field
        *   in the serialized XML
        */
		protected boolean localProcedureFastTrackTracker = false ;
		
		public boolean isProcedureFastTrackSpecified(){
			return localProcedureFromCodeTracker;
		}

		/**
		 * Auto generated getter method
		 * @return java.lang.String
		 */
		public  java.lang.String getProcedureFastTrack(){
			return localProcedureFastTrack;
		}
        
		/**
		  * Auto generated setter method
		  * @param param ProcedureFromCode
		  */
		public void setProcedureFastTrack(java.lang.String param){
			localProcedureFastTrackTracker = param != null;

			this.localProcedureFastTrack=param;
		}
		// fasttrack added Jan 19, 2017
		
        public boolean isLabTestTypeNameSpecified() {
            return localLabTestTypeNameTracker;
        }

        /**
         * Auto generated getter method
         * @return java.lang.String
         */
        public java.lang.String getLabTestTypeName() {
            return localLabTestTypeName;
        }

        /**
         * Auto generated setter method
         * @param param LabTestTypeName
         */
        public void setLabTestTypeName(java.lang.String param) {
            localLabTestTypeNameTracker = param != null;

            this.localLabTestTypeName = param;
        }

        /**
         *
         * @param parentQName
         * @param factory
         * @return org.apache.axiom.om.OMElement
         */
        public org.apache.axiom.om.OMElement getOMElement(
            final javax.xml.namespace.QName parentQName,
            final org.apache.axiom.om.OMFactory factory)
            throws org.apache.axis2.databinding.ADBException {
            org.apache.axiom.om.OMDataSource dataSource = new org.apache.axis2.databinding.ADBDataSource(this,
                    parentQName);

            return factory.createOMElement(dataSource, parentQName);
        }

        public void serialize(final javax.xml.namespace.QName parentQName,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException,
                org.apache.axis2.databinding.ADBException {
            serialize(parentQName, xmlWriter, false);
        }

        public void serialize(final javax.xml.namespace.QName parentQName,
            javax.xml.stream.XMLStreamWriter xmlWriter, boolean serializeType)
            throws javax.xml.stream.XMLStreamException,
                org.apache.axis2.databinding.ADBException {
            java.lang.String prefix = null;
            java.lang.String namespace = null;

            prefix = parentQName.getPrefix();
            namespace = parentQName.getNamespaceURI();
            writeStartElement(prefix, namespace, parentQName.getLocalPart(),
                xmlWriter);

            if (serializeType) {
                java.lang.String namespacePrefix = registerPrefix(xmlWriter,
                        "http://services.lbm.avalon.com/");

                if ((namespacePrefix != null) &&
                        (namespacePrefix.trim().length() > 0)) {
                    writeAttribute("xsi",
                        "http://www.w3.org/2001/XMLSchema-instance", "type",
                        namespacePrefix + ":procedureDTO", xmlWriter);
                } else {
                    writeAttribute("xsi",
                        "http://www.w3.org/2001/XMLSchema-instance", "type",
                        "procedureDTO", xmlWriter);
                }
            }

            if (localProcedureFromCodeTracker) {
                namespace = "";
                writeStartElement(null, namespace, "procedureFromCode",
                    xmlWriter);

                if (localProcedureFromCode == null) {
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException(
                        "procedureFromCode cannot be null!!");
                } else {
                    xmlWriter.writeCharacters(localProcedureFromCode);
                }

                xmlWriter.writeEndElement();
            }

            if (localProcedureToCodeTracker) {
                namespace = "";
                writeStartElement(null, namespace, "procedureToCode", xmlWriter);

                if (localProcedureToCode == null) {
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException(
                        "procedureToCode cannot be null!!");
                } else {
                    xmlWriter.writeCharacters(localProcedureToCode);
                }

                xmlWriter.writeEndElement();
            }

            if (localProcedureDecisionTracker) {
                namespace = "";
                writeStartElement(null, namespace, "procedureDecision",
                    xmlWriter);

                if (localProcedureDecision == null) {
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException(
                        "procedureDecision cannot be null!!");
                } else {
                    xmlWriter.writeCharacters(localProcedureDecision);
                }

                xmlWriter.writeEndElement();
            }

            if (localProcedureDecisionReasonTracker) {
                namespace = "";
                writeStartElement(null, namespace, "procedureDecisionReason",
                    xmlWriter);

                if (localProcedureDecisionReason == null) {
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException(
                        "procedureDecisionReason cannot be null!!");
                } else {
                    xmlWriter.writeCharacters(localProcedureDecisionReason);
                }

                xmlWriter.writeEndElement();
            }

            namespace = "";
            writeStartElement(null, namespace, "procedureUnits", xmlWriter);

            if (java.lang.Float.isNaN(localProcedureUnits)) {
                throw new org.apache.axis2.databinding.ADBException(
                    "procedureUnits cannot be null!!");
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        localProcedureUnits));
            }

            xmlWriter.writeEndElement();

            // fasttrack added Jan 19, 2017
            if (localProcedureFastTrackTracker) {
                namespace = "";
                writeStartElement(null, namespace, "procedureFastTrack", xmlWriter);

                if (localProcedureFastTrack==null){
                	// write the nil attribute
                	throw new org.apache.axis2.databinding.ADBException("procedureFastTrack cannot be null!!");
                } else {
                	xmlWriter.writeCharacters(localProcedureFastTrack);
                }
                                    
                xmlWriter.writeEndElement();
            }
            // fasttrack added Jan 19, 2017

            if (localLabTestTypeNameTracker) {
                namespace = "";
                writeStartElement(null, namespace, "labTestTypeName", xmlWriter);

                if (localLabTestTypeName == null) {
                    // write the nil attribute
                    throw new org.apache.axis2.databinding.ADBException(
                        "labTestTypeName cannot be null!!");
                } else {
                    xmlWriter.writeCharacters(localLabTestTypeName);
                }

                xmlWriter.writeEndElement();
            }

            xmlWriter.writeEndElement();
        }

        private static java.lang.String generatePrefix(
            java.lang.String namespace) {
            if (namespace.equals("http://services.lbm.avalon.com/")) {
                return "ns1";
            }

            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix,
            java.lang.String namespace, java.lang.String localPart,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

            if (writerPrefix != null) {
                xmlWriter.writeStartElement(namespace, localPart);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }

        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,
            java.lang.String namespace, java.lang.String attName,
            java.lang.String attValue,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }

            xmlWriter.writeAttribute(namespace, attName, attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,
            java.lang.String attName, java.lang.String attValue,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attValue);
            }
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeQNameAttribute(java.lang.String namespace,
            java.lang.String attName, javax.xml.namespace.QName qname,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String attributeNamespace = qname.getNamespaceURI();
            java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);

            if (attributePrefix == null) {
                attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
            }

            java.lang.String attributeValue;

            if (attributePrefix.trim().length() > 0) {
                attributeValue = attributePrefix + ":" + qname.getLocalPart();
            } else {
                attributeValue = qname.getLocalPart();
            }

            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attributeValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attributeValue);
            }
        }

        /**
         *  method to handle Qnames
         */
        private void writeQName(javax.xml.namespace.QName qname,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();

            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);

                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix, namespaceURI);
                }

                if (prefix.trim().length() > 0) {
                    xmlWriter.writeCharacters(prefix + ":" +
                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            qname));
                }
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }

                    namespaceURI = qnames[i].getNamespaceURI();

                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);

                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix, namespaceURI);
                        }

                        if (prefix.trim().length() > 0) {
                            stringToWrite.append(prefix).append(":")
                                         .append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                    qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                    qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                qnames[i]));
                    }
                }

                xmlWriter.writeCharacters(stringToWrite.toString());
            }
        }

        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(
            javax.xml.stream.XMLStreamWriter xmlWriter,
            java.lang.String namespace)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);

            if (prefix == null) {
                prefix = generatePrefix(namespace);

                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();

                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);

                    if ((uri == null) || (uri.length() == 0)) {
                        break;
                    }

                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }

                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }

            return prefix;
        }

        /**
         * databinding method to get an XML representation of this object
         *
         */
        public javax.xml.stream.XMLStreamReader getPullParser(
            javax.xml.namespace.QName qName)
            throws org.apache.axis2.databinding.ADBException {
            java.util.ArrayList elementList = new java.util.ArrayList();
            java.util.ArrayList attribList = new java.util.ArrayList();

            if (localProcedureFromCodeTracker) {
                elementList.add(new javax.xml.namespace.QName("",
                        "procedureFromCode"));

                if (localProcedureFromCode != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            localProcedureFromCode));
                } else {
                    throw new org.apache.axis2.databinding.ADBException(
                        "procedureFromCode cannot be null!!");
                }
            }

            if (localProcedureToCodeTracker) {
                elementList.add(new javax.xml.namespace.QName("",
                        "procedureToCode"));

                if (localProcedureToCode != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            localProcedureToCode));
                } else {
                    throw new org.apache.axis2.databinding.ADBException(
                        "procedureToCode cannot be null!!");
                }
            }

            if (localProcedureDecisionTracker) {
                elementList.add(new javax.xml.namespace.QName("",
                        "procedureDecision"));

                if (localProcedureDecision != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            localProcedureDecision));
                } else {
                    throw new org.apache.axis2.databinding.ADBException(
                        "procedureDecision cannot be null!!");
                }
            }

            if (localProcedureDecisionReasonTracker) {
                elementList.add(new javax.xml.namespace.QName("",
                        "procedureDecisionReason"));

                if (localProcedureDecisionReason != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            localProcedureDecisionReason));
                } else {
                    throw new org.apache.axis2.databinding.ADBException(
                        "procedureDecisionReason cannot be null!!");
                }
            }

            elementList.add(new javax.xml.namespace.QName("", "procedureUnits"));

            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                    localProcedureUnits));

            // fasttrack added Jan 19, 2017
			if (localProcedureFastTrackTracker){
    			elementList.add(new javax.xml.namespace.QName("", "procedureFastTrack"));

	    		if (localProcedureFastTrack != null){
	    			elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localProcedureFastTrack));
	    		} else {
	    			throw new org.apache.axis2.databinding.ADBException("procedureFastTrack cannot be null!!");
	    		}
			}
            // fasttrack added Jan 19, 2017

            if (localLabTestTypeNameTracker) {
                elementList.add(new javax.xml.namespace.QName("",
                        "labTestTypeName"));

                if (localLabTestTypeName != null) {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            localLabTestTypeName));
                } else {
                    throw new org.apache.axis2.databinding.ADBException(
                        "labTestTypeName cannot be null!!");
                }
            }

            return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName,
                elementList.toArray(), attribList.toArray());
        }

        /**
         *  Factory class that keeps the parse method
         */
        public static class Factory {
            /**
             * static method to create the object
             * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
             *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
             * Postcondition: If this object is an element, the reader is positioned at its end element
             *                If this object is a complex type, the reader is positioned at the end element of its outer element
             */
            public static ProcedureDTO parse(
                javax.xml.stream.XMLStreamReader reader)
                throws java.lang.Exception {
                ProcedureDTO object = new ProcedureDTO();

                int event;
                java.lang.String nillableValue = null;
                java.lang.String prefix = "";
                java.lang.String namespaceuri = "";

                try {
                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.getAttributeValue(
                                "http://www.w3.org/2001/XMLSchema-instance",
                                "type") != null) {
                        java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                "type");

                        if (fullTypeName != null) {
                            java.lang.String nsPrefix = null;

                            if (fullTypeName.indexOf(":") > -1) {
                                nsPrefix = fullTypeName.substring(0,
                                        fullTypeName.indexOf(":"));
                            }

                            nsPrefix = (nsPrefix == null) ? "" : nsPrefix;

                            java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(
                                        ":") + 1);

                            if (!"procedureDTO".equals(type)) {
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext()
                                                               .getNamespaceURI(nsPrefix);

                                return (ProcedureDTO) ExtensionMapper.getTypeObject(nsUri,
                                    type, reader);
                            }
                        }
                    }

                    // Note all attributes that were handled. Used to differ normal attributes
                    // from anyAttributes.
                    java.util.Vector handledAttributes = new java.util.Vector();

                    reader.next();

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                            new javax.xml.namespace.QName("",
                                "procedureFromCode").equals(reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                "nil");

                        if ("true".equals(nillableValue) ||
                                "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "procedureFromCode" +
                                "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setProcedureFromCode(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                            new javax.xml.namespace.QName("", "procedureToCode").equals(
                                reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                "nil");

                        if ("true".equals(nillableValue) ||
                                "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "procedureToCode" +
                                "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setProcedureToCode(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                            new javax.xml.namespace.QName("",
                                "procedureDecision").equals(reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                "nil");

                        if ("true".equals(nillableValue) ||
                                "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "procedureDecision" +
                                "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setProcedureDecision(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                            new javax.xml.namespace.QName("",
                                "procedureDecisionReason").equals(
                                reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                "nil");

                        if ("true".equals(nillableValue) ||
                                "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "procedureDecisionReason" +
                                "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setProcedureDecisionReason(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                            new javax.xml.namespace.QName("", "procedureUnits").equals(
                                reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                "nil");

                        if ("true".equals(nillableValue) ||
                                "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "procedureUnits" +
                                "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setProcedureUnits(org.apache.axis2.databinding.utils.ConverterUtil.convertToFloat(
                                content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                        // A start element we are not expecting indicates an invalid parameter was passed
                        throw new org.apache.axis2.databinding.ADBException(
                            "Unexpected subelement " + reader.getName());
                    }

                    // fasttrack added Jan 19, 2017
            		while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

            		if (reader.isStartElement() && new javax.xml.namespace.QName("","procedureFastTrack").equals(reader.getName())){

            			nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
            			if ("true".equals(nillableValue) || "1".equals(nillableValue)){
            				throw new org.apache.axis2.databinding.ADBException("The element: "+"procedureFastTrack" +" cannot be null");
            			}

            			java.lang.String content = reader.getElementText();

            			object.setProcedureFastTrack(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

            			reader.next();

            		}  // End of if for expected property start element

            		else {

            		}
                    // fasttrack added Jan 19, 2017

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                            new javax.xml.namespace.QName("", "labTestTypeName").equals(
                                reader.getName())) {
                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                "nil");

                        if ("true".equals(nillableValue) ||
                                "1".equals(nillableValue)) {
                            throw new org.apache.axis2.databinding.ADBException(
                                "The element: " + "labTestTypeName" +
                                "  cannot be null");
                        }

                        java.lang.String content = reader.getElementText();

                        object.setLabTestTypeName(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                content));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()) {
                        // A start element we are not expecting indicates a trailing invalid property
                        throw new org.apache.axis2.databinding.ADBException(
                            "Unexpected subelement " + reader.getName());
                    }
                } catch (javax.xml.stream.XMLStreamException e) {
                    throw new java.lang.Exception(e);
                }

                return object;
            }
        } //end of factory class
    }

    public static class PriorAuthorizationSearch implements org.apache.axis2.databinding.ADBBean {
        /* This type was generated from the piece of schema that had
           name = priorAuthorizationSearch
           Namespace URI = http://services.lbm.avalon.com/
           Namespace Prefix = ns1
         */

        /**
         * field for PriorAuthorizationSearch
         */
        protected PriorAuthHeader localPriorAuthorizationSearch;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean localPriorAuthorizationSearchTracker = false;

        public boolean isPriorAuthorizationSearchSpecified() {
            return localPriorAuthorizationSearchTracker;
        }

        /**
         * Auto generated getter method
         * @return PriorAuthHeader
         */
        public PriorAuthHeader getPriorAuthorizationSearch() {
            return localPriorAuthorizationSearch;
        }

        /**
         * Auto generated setter method
         * @param param PriorAuthorizationSearch
         */
        public void setPriorAuthorizationSearch(PriorAuthHeader param) {
            localPriorAuthorizationSearchTracker = param != null;

            this.localPriorAuthorizationSearch = param;
        }

        /**
         *
         * @param parentQName
         * @param factory
         * @return org.apache.axiom.om.OMElement
         */
        public org.apache.axiom.om.OMElement getOMElement(
            final javax.xml.namespace.QName parentQName,
            final org.apache.axiom.om.OMFactory factory)
            throws org.apache.axis2.databinding.ADBException {
            org.apache.axiom.om.OMDataSource dataSource = new org.apache.axis2.databinding.ADBDataSource(this,
                    parentQName);

            return factory.createOMElement(dataSource, parentQName);
        }

        public void serialize(final javax.xml.namespace.QName parentQName,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException,
                org.apache.axis2.databinding.ADBException {
            serialize(parentQName, xmlWriter, false);
        }

        public void serialize(final javax.xml.namespace.QName parentQName,
            javax.xml.stream.XMLStreamWriter xmlWriter, boolean serializeType)
            throws javax.xml.stream.XMLStreamException,
                org.apache.axis2.databinding.ADBException {
            java.lang.String prefix = null;
            java.lang.String namespace = null;

            prefix = parentQName.getPrefix();
            namespace = parentQName.getNamespaceURI();
            writeStartElement(prefix, namespace, parentQName.getLocalPart(),
                xmlWriter);

            if (serializeType) {
                java.lang.String namespacePrefix = registerPrefix(xmlWriter,
                        "http://services.lbm.avalon.com/");

                if ((namespacePrefix != null) &&
                        (namespacePrefix.trim().length() > 0)) {
                    writeAttribute("xsi",
                        "http://www.w3.org/2001/XMLSchema-instance", "type",
                        namespacePrefix + ":priorAuthorizationSearch", xmlWriter);
                } else {
                    writeAttribute("xsi",
                        "http://www.w3.org/2001/XMLSchema-instance", "type",
                        "priorAuthorizationSearch", xmlWriter);
                }
            }

            if (localPriorAuthorizationSearchTracker) {
                if (localPriorAuthorizationSearch == null) {
                    throw new org.apache.axis2.databinding.ADBException(
                        "priorAuthorizationSearch cannot be null!!");
                }

                localPriorAuthorizationSearch.serialize(new javax.xml.namespace.QName(
                        "http://services.lbm.avalon.com/",
                        "priorAuthorizationSearch"), xmlWriter);
            }

            xmlWriter.writeEndElement();
        }

        private static java.lang.String generatePrefix(
            java.lang.String namespace) {
            if (namespace.equals("http://services.lbm.avalon.com/")) {
                return "ns1";
            }

            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix,
            java.lang.String namespace, java.lang.String localPart,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

            if (writerPrefix != null) {
                xmlWriter.writeStartElement(namespace, localPart);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }

        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,
            java.lang.String namespace, java.lang.String attName,
            java.lang.String attValue,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }

            xmlWriter.writeAttribute(namespace, attName, attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,
            java.lang.String attName, java.lang.String attValue,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attValue);
            }
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeQNameAttribute(java.lang.String namespace,
            java.lang.String attName, javax.xml.namespace.QName qname,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String attributeNamespace = qname.getNamespaceURI();
            java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);

            if (attributePrefix == null) {
                attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
            }

            java.lang.String attributeValue;

            if (attributePrefix.trim().length() > 0) {
                attributeValue = attributePrefix + ":" + qname.getLocalPart();
            } else {
                attributeValue = qname.getLocalPart();
            }

            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attributeValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attributeValue);
            }
        }

        /**
         *  method to handle Qnames
         */
        private void writeQName(javax.xml.namespace.QName qname,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();

            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);

                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix, namespaceURI);
                }

                if (prefix.trim().length() > 0) {
                    xmlWriter.writeCharacters(prefix + ":" +
                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            qname));
                }
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }

                    namespaceURI = qnames[i].getNamespaceURI();

                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);

                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix, namespaceURI);
                        }

                        if (prefix.trim().length() > 0) {
                            stringToWrite.append(prefix).append(":")
                                         .append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                    qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                    qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                qnames[i]));
                    }
                }

                xmlWriter.writeCharacters(stringToWrite.toString());
            }
        }

        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(
            javax.xml.stream.XMLStreamWriter xmlWriter,
            java.lang.String namespace)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);

            if (prefix == null) {
                prefix = generatePrefix(namespace);

                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();

                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);

                    if ((uri == null) || (uri.length() == 0)) {
                        break;
                    }

                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }

                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }

            return prefix;
        }

        /**
         * databinding method to get an XML representation of this object
         *
         */
        public javax.xml.stream.XMLStreamReader getPullParser(
            javax.xml.namespace.QName qName)
            throws org.apache.axis2.databinding.ADBException {
            java.util.ArrayList elementList = new java.util.ArrayList();
            java.util.ArrayList attribList = new java.util.ArrayList();

            if (localPriorAuthorizationSearchTracker) {
                elementList.add(new javax.xml.namespace.QName(
                        "http://services.lbm.avalon.com/",
                        "priorAuthorizationSearch"));

                if (localPriorAuthorizationSearch == null) {
                    throw new org.apache.axis2.databinding.ADBException(
                        "priorAuthorizationSearch cannot be null!!");
                }

                elementList.add(localPriorAuthorizationSearch);
            }

            return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName,
                elementList.toArray(), attribList.toArray());
        }

        /**
         *  Factory class that keeps the parse method
         */
        public static class Factory {
            /**
             * static method to create the object
             * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
             *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
             * Postcondition: If this object is an element, the reader is positioned at its end element
             *                If this object is a complex type, the reader is positioned at the end element of its outer element
             */
            public static PriorAuthorizationSearch parse(
                javax.xml.stream.XMLStreamReader reader)
                throws java.lang.Exception {
                PriorAuthorizationSearch object = new PriorAuthorizationSearch();

                int event;
                java.lang.String nillableValue = null;
                java.lang.String prefix = "";
                java.lang.String namespaceuri = "";

                try {
                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.getAttributeValue(
                                "http://www.w3.org/2001/XMLSchema-instance",
                                "type") != null) {
                        java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                "type");

                        if (fullTypeName != null) {
                            java.lang.String nsPrefix = null;

                            if (fullTypeName.indexOf(":") > -1) {
                                nsPrefix = fullTypeName.substring(0,
                                        fullTypeName.indexOf(":"));
                            }

                            nsPrefix = (nsPrefix == null) ? "" : nsPrefix;

                            java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(
                                        ":") + 1);

                            if (!"priorAuthorizationSearch".equals(type)) {
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext()
                                                               .getNamespaceURI(nsPrefix);

                                return (PriorAuthorizationSearch) ExtensionMapper.getTypeObject(nsUri,
                                    type, reader);
                            }
                        }
                    }

                    // Note all attributes that were handled. Used to differ normal attributes
                    // from anyAttributes.
                    java.util.Vector handledAttributes = new java.util.Vector();

                    reader.next();

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                            new javax.xml.namespace.QName(
                                "http://services.lbm.avalon.com/",
                                "priorAuthorizationSearch").equals(
                                reader.getName())) {
                        object.setPriorAuthorizationSearch(PriorAuthHeader.Factory.parse(
                                reader));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()) {
                        // A start element we are not expecting indicates a trailing invalid property
                        throw new org.apache.axis2.databinding.ADBException(
                            "Unexpected subelement " + reader.getName());
                    }
                } catch (javax.xml.stream.XMLStreamException e) {
                    throw new java.lang.Exception(e);
                }

                return object;
            }
        } //end of factory class
    }

    public static class DiagnosisDTOE implements org.apache.axis2.databinding.ADBBean {
        public static final javax.xml.namespace.QName MY_QNAME = new javax.xml.namespace.QName("http://services.lbm.avalon.com/",
                "diagnosisDTO", "ns1");

        /**
         * field for DiagnosisDTO
         */
        protected DiagnosisDTO localDiagnosisDTO;

        /**
         * Auto generated getter method
         * @return DiagnosisDTO
         */
        public DiagnosisDTO getDiagnosisDTO() {
            return localDiagnosisDTO;
        }

        /**
         * Auto generated setter method
         * @param param DiagnosisDTO
         */
        public void setDiagnosisDTO(DiagnosisDTO param) {
            this.localDiagnosisDTO = param;
        }

        /**
         *
         * @param parentQName
         * @param factory
         * @return org.apache.axiom.om.OMElement
         */
        public org.apache.axiom.om.OMElement getOMElement(
            final javax.xml.namespace.QName parentQName,
            final org.apache.axiom.om.OMFactory factory)
            throws org.apache.axis2.databinding.ADBException {
            org.apache.axiom.om.OMDataSource dataSource = new org.apache.axis2.databinding.ADBDataSource(this,
                    MY_QNAME);

            return factory.createOMElement(dataSource, MY_QNAME);
        }

        public void serialize(final javax.xml.namespace.QName parentQName,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException,
                org.apache.axis2.databinding.ADBException {
            serialize(parentQName, xmlWriter, false);
        }

        public void serialize(final javax.xml.namespace.QName parentQName,
            javax.xml.stream.XMLStreamWriter xmlWriter, boolean serializeType)
            throws javax.xml.stream.XMLStreamException,
                org.apache.axis2.databinding.ADBException {
            //We can safely assume an element has only one type associated with it
            if (localDiagnosisDTO == null) {
                throw new org.apache.axis2.databinding.ADBException(
                    "diagnosisDTO cannot be null!");
            }

            localDiagnosisDTO.serialize(MY_QNAME, xmlWriter);
        }

        private static java.lang.String generatePrefix(
            java.lang.String namespace) {
            if (namespace.equals("http://services.lbm.avalon.com/")) {
                return "ns1";
            }

            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix,
            java.lang.String namespace, java.lang.String localPart,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

            if (writerPrefix != null) {
                xmlWriter.writeStartElement(namespace, localPart);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }

        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,
            java.lang.String namespace, java.lang.String attName,
            java.lang.String attValue,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }

            xmlWriter.writeAttribute(namespace, attName, attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,
            java.lang.String attName, java.lang.String attValue,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attValue);
            }
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeQNameAttribute(java.lang.String namespace,
            java.lang.String attName, javax.xml.namespace.QName qname,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String attributeNamespace = qname.getNamespaceURI();
            java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);

            if (attributePrefix == null) {
                attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
            }

            java.lang.String attributeValue;

            if (attributePrefix.trim().length() > 0) {
                attributeValue = attributePrefix + ":" + qname.getLocalPart();
            } else {
                attributeValue = qname.getLocalPart();
            }

            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attributeValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attributeValue);
            }
        }

        /**
         *  method to handle Qnames
         */
        private void writeQName(javax.xml.namespace.QName qname,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();

            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);

                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix, namespaceURI);
                }

                if (prefix.trim().length() > 0) {
                    xmlWriter.writeCharacters(prefix + ":" +
                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            qname));
                }
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }

                    namespaceURI = qnames[i].getNamespaceURI();

                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);

                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix, namespaceURI);
                        }

                        if (prefix.trim().length() > 0) {
                            stringToWrite.append(prefix).append(":")
                                         .append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                    qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                    qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                qnames[i]));
                    }
                }

                xmlWriter.writeCharacters(stringToWrite.toString());
            }
        }

        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(
            javax.xml.stream.XMLStreamWriter xmlWriter,
            java.lang.String namespace)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);

            if (prefix == null) {
                prefix = generatePrefix(namespace);

                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();

                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);

                    if ((uri == null) || (uri.length() == 0)) {
                        break;
                    }

                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }

                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }

            return prefix;
        }

        /**
         * databinding method to get an XML representation of this object
         *
         */
        public javax.xml.stream.XMLStreamReader getPullParser(
            javax.xml.namespace.QName qName)
            throws org.apache.axis2.databinding.ADBException {
            //We can safely assume an element has only one type associated with it
            return localDiagnosisDTO.getPullParser(MY_QNAME);
        }

        /**
         *  Factory class that keeps the parse method
         */
        public static class Factory {
            /**
             * static method to create the object
             * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
             *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
             * Postcondition: If this object is an element, the reader is positioned at its end element
             *                If this object is a complex type, the reader is positioned at the end element of its outer element
             */
            public static DiagnosisDTOE parse(
                javax.xml.stream.XMLStreamReader reader)
                throws java.lang.Exception {
                DiagnosisDTOE object = new DiagnosisDTOE();

                int event;
                java.lang.String nillableValue = null;
                java.lang.String prefix = "";
                java.lang.String namespaceuri = "";

                try {
                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    // Note all attributes that were handled. Used to differ normal attributes
                    // from anyAttributes.
                    java.util.Vector handledAttributes = new java.util.Vector();

                    while (!reader.isEndElement()) {
                        if (reader.isStartElement()) {
                            if (reader.isStartElement() &&
                                    new javax.xml.namespace.QName(
                                        "http://services.lbm.avalon.com/",
                                        "diagnosisDTO").equals(reader.getName())) {
                                object.setDiagnosisDTO(DiagnosisDTO.Factory.parse(
                                        reader));
                            } // End of if for expected property start element

                            else {
                                // A start element we are not expecting indicates an invalid parameter was passed
                                throw new org.apache.axis2.databinding.ADBException(
                                    "Unexpected subelement " +
                                    reader.getName());
                            }
                        } else {
                            reader.next();
                        }
                    } // end of while loop
                } catch (javax.xml.stream.XMLStreamException e) {
                    throw new java.lang.Exception(e);
                }

                return object;
            }
        } //end of factory class
    }

    public static class GetPriorAuthHeaderE implements org.apache.axis2.databinding.ADBBean {
        public static final javax.xml.namespace.QName MY_QNAME = new javax.xml.namespace.QName("http://services.lbm.avalon.com/",
                "getPriorAuthHeader", "ns1");

        /**
         * field for GetPriorAuthHeader
         */
        protected GetPriorAuthHeader localGetPriorAuthHeader;

        /**
         * Auto generated getter method
         * @return GetPriorAuthHeader
         */
        public GetPriorAuthHeader getGetPriorAuthHeader() {
            return localGetPriorAuthHeader;
        }

        /**
         * Auto generated setter method
         * @param param GetPriorAuthHeader
         */
        public void setGetPriorAuthHeader(GetPriorAuthHeader param) {
            this.localGetPriorAuthHeader = param;
        }

        /**
         *
         * @param parentQName
         * @param factory
         * @return org.apache.axiom.om.OMElement
         */
        public org.apache.axiom.om.OMElement getOMElement(
            final javax.xml.namespace.QName parentQName,
            final org.apache.axiom.om.OMFactory factory)
            throws org.apache.axis2.databinding.ADBException {
            org.apache.axiom.om.OMDataSource dataSource = new org.apache.axis2.databinding.ADBDataSource(this,
                    MY_QNAME);

            return factory.createOMElement(dataSource, MY_QNAME);
        }

        public void serialize(final javax.xml.namespace.QName parentQName,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException,
                org.apache.axis2.databinding.ADBException {
            serialize(parentQName, xmlWriter, false);
        }

        public void serialize(final javax.xml.namespace.QName parentQName,
            javax.xml.stream.XMLStreamWriter xmlWriter, boolean serializeType)
            throws javax.xml.stream.XMLStreamException,
                org.apache.axis2.databinding.ADBException {
            //We can safely assume an element has only one type associated with it
            if (localGetPriorAuthHeader == null) {
                throw new org.apache.axis2.databinding.ADBException(
                    "getPriorAuthHeader cannot be null!");
            }

            localGetPriorAuthHeader.serialize(MY_QNAME, xmlWriter);
        }

        private static java.lang.String generatePrefix(
            java.lang.String namespace) {
            if (namespace.equals("http://services.lbm.avalon.com/")) {
                return "ns1";
            }

            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix,
            java.lang.String namespace, java.lang.String localPart,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

            if (writerPrefix != null) {
                xmlWriter.writeStartElement(namespace, localPart);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }

        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,
            java.lang.String namespace, java.lang.String attName,
            java.lang.String attValue,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }

            xmlWriter.writeAttribute(namespace, attName, attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,
            java.lang.String attName, java.lang.String attValue,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attValue);
            }
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeQNameAttribute(java.lang.String namespace,
            java.lang.String attName, javax.xml.namespace.QName qname,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String attributeNamespace = qname.getNamespaceURI();
            java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);

            if (attributePrefix == null) {
                attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
            }

            java.lang.String attributeValue;

            if (attributePrefix.trim().length() > 0) {
                attributeValue = attributePrefix + ":" + qname.getLocalPart();
            } else {
                attributeValue = qname.getLocalPart();
            }

            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attributeValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attributeValue);
            }
        }

        /**
         *  method to handle Qnames
         */
        private void writeQName(javax.xml.namespace.QName qname,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();

            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);

                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix, namespaceURI);
                }

                if (prefix.trim().length() > 0) {
                    xmlWriter.writeCharacters(prefix + ":" +
                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            qname));
                }
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }

                    namespaceURI = qnames[i].getNamespaceURI();

                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);

                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix, namespaceURI);
                        }

                        if (prefix.trim().length() > 0) {
                            stringToWrite.append(prefix).append(":")
                                         .append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                    qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                    qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                qnames[i]));
                    }
                }

                xmlWriter.writeCharacters(stringToWrite.toString());
            }
        }

        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(
            javax.xml.stream.XMLStreamWriter xmlWriter,
            java.lang.String namespace)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);

            if (prefix == null) {
                prefix = generatePrefix(namespace);

                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();

                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);

                    if ((uri == null) || (uri.length() == 0)) {
                        break;
                    }

                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }

                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }

            return prefix;
        }

        /**
         * databinding method to get an XML representation of this object
         *
         */
        public javax.xml.stream.XMLStreamReader getPullParser(
            javax.xml.namespace.QName qName)
            throws org.apache.axis2.databinding.ADBException {
            //We can safely assume an element has only one type associated with it
            return localGetPriorAuthHeader.getPullParser(MY_QNAME);
        }

        /**
         *  Factory class that keeps the parse method
         */
        public static class Factory {
            /**
             * static method to create the object
             * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
             *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
             * Postcondition: If this object is an element, the reader is positioned at its end element
             *                If this object is a complex type, the reader is positioned at the end element of its outer element
             */
            public static GetPriorAuthHeaderE parse(
                javax.xml.stream.XMLStreamReader reader)
                throws java.lang.Exception {
                GetPriorAuthHeaderE object = new GetPriorAuthHeaderE();

                int event;
                java.lang.String nillableValue = null;
                java.lang.String prefix = "";
                java.lang.String namespaceuri = "";

                try {
                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    // Note all attributes that were handled. Used to differ normal attributes
                    // from anyAttributes.
                    java.util.Vector handledAttributes = new java.util.Vector();

                    while (!reader.isEndElement()) {
                        if (reader.isStartElement()) {
                            if (reader.isStartElement() &&
                                    new javax.xml.namespace.QName(
                                        "http://services.lbm.avalon.com/",
                                        "getPriorAuthHeader").equals(
                                        reader.getName())) {
                                object.setGetPriorAuthHeader(GetPriorAuthHeader.Factory.parse(
                                        reader));
                            } // End of if for expected property start element

                            else {
                                // A start element we are not expecting indicates an invalid parameter was passed
                                throw new org.apache.axis2.databinding.ADBException(
                                    "Unexpected subelement " +
                                    reader.getName());
                            }
                        } else {
                            reader.next();
                        }
                    } // end of while loop
                } catch (javax.xml.stream.XMLStreamException e) {
                    throw new java.lang.Exception(e);
                }

                return object;
            }
        } //end of factory class
    }

    public static class GetPriorAuthHeaderResponse implements org.apache.axis2.databinding.ADBBean {
        /* This type was generated from the piece of schema that had
           name = getPriorAuthHeaderResponse
           Namespace URI = http://services.lbm.avalon.com/
           Namespace Prefix = ns1
         */

        /**
         * field for _return
         */
        protected PriorAuthHeader local_return;

        /*  This tracker boolean wil be used to detect whether the user called the set method
         *   for this attribute. It will be used to determine whether to include this field
         *   in the serialized XML
         */
        protected boolean local_returnTracker = false;

        public boolean is_returnSpecified() {
            return local_returnTracker;
        }

        /**
         * Auto generated getter method
         * @return PriorAuthHeader
         */
        public PriorAuthHeader get_return() {
            return local_return;
        }

        /**
         * Auto generated setter method
         * @param param _return
         */
        public void set_return(PriorAuthHeader param) {
            local_returnTracker = param != null;

            this.local_return = param;
        }

        /**
         *
         * @param parentQName
         * @param factory
         * @return org.apache.axiom.om.OMElement
         */
        public org.apache.axiom.om.OMElement getOMElement(
            final javax.xml.namespace.QName parentQName,
            final org.apache.axiom.om.OMFactory factory)
            throws org.apache.axis2.databinding.ADBException {
            org.apache.axiom.om.OMDataSource dataSource = new org.apache.axis2.databinding.ADBDataSource(this,
                    parentQName);

            return factory.createOMElement(dataSource, parentQName);
        }

        public void serialize(final javax.xml.namespace.QName parentQName,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException,
                org.apache.axis2.databinding.ADBException {
            serialize(parentQName, xmlWriter, false);
        }

        public void serialize(final javax.xml.namespace.QName parentQName,
            javax.xml.stream.XMLStreamWriter xmlWriter, boolean serializeType)
            throws javax.xml.stream.XMLStreamException,
                org.apache.axis2.databinding.ADBException {
            java.lang.String prefix = null;
            java.lang.String namespace = null;

            prefix = parentQName.getPrefix();
            namespace = parentQName.getNamespaceURI();
            writeStartElement(prefix, namespace, parentQName.getLocalPart(),
                xmlWriter);

            if (serializeType) {
                java.lang.String namespacePrefix = registerPrefix(xmlWriter,
                        "http://services.lbm.avalon.com/");

                if ((namespacePrefix != null) &&
                        (namespacePrefix.trim().length() > 0)) {
                    writeAttribute("xsi",
                        "http://www.w3.org/2001/XMLSchema-instance", "type",
                        namespacePrefix + ":getPriorAuthHeaderResponse",
                        xmlWriter);
                } else {
                    writeAttribute("xsi",
                        "http://www.w3.org/2001/XMLSchema-instance", "type",
                        "getPriorAuthHeaderResponse", xmlWriter);
                }
            }

            if (local_returnTracker) {
                if (local_return == null) {
                    throw new org.apache.axis2.databinding.ADBException(
                        "return cannot be null!!");
                }

                local_return.serialize(new javax.xml.namespace.QName("",
                        "return"), xmlWriter);
            }

            xmlWriter.writeEndElement();
        }

        private static java.lang.String generatePrefix(
            java.lang.String namespace) {
            if (namespace.equals("http://services.lbm.avalon.com/")) {
                return "ns1";
            }

            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix,
            java.lang.String namespace, java.lang.String localPart,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

            if (writerPrefix != null) {
                xmlWriter.writeStartElement(namespace, localPart);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }

        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,
            java.lang.String namespace, java.lang.String attName,
            java.lang.String attValue,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }

            xmlWriter.writeAttribute(namespace, attName, attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,
            java.lang.String attName, java.lang.String attValue,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attValue);
            }
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeQNameAttribute(java.lang.String namespace,
            java.lang.String attName, javax.xml.namespace.QName qname,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String attributeNamespace = qname.getNamespaceURI();
            java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);

            if (attributePrefix == null) {
                attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
            }

            java.lang.String attributeValue;

            if (attributePrefix.trim().length() > 0) {
                attributeValue = attributePrefix + ":" + qname.getLocalPart();
            } else {
                attributeValue = qname.getLocalPart();
            }

            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName, attributeValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attributeValue);
            }
        }

        /**
         *  method to handle Qnames
         */
        private void writeQName(javax.xml.namespace.QName qname,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();

            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);

                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix, namespaceURI);
                }

                if (prefix.trim().length() > 0) {
                    xmlWriter.writeCharacters(prefix + ":" +
                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                            qname));
                }
            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                        qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
            javax.xml.stream.XMLStreamWriter xmlWriter)
            throws javax.xml.stream.XMLStreamException {
            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }

                    namespaceURI = qnames[i].getNamespaceURI();

                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);

                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix, namespaceURI);
                        }

                        if (prefix.trim().length() > 0) {
                            stringToWrite.append(prefix).append(":")
                                         .append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                    qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                    qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                                qnames[i]));
                    }
                }

                xmlWriter.writeCharacters(stringToWrite.toString());
            }
        }

        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(
            javax.xml.stream.XMLStreamWriter xmlWriter,
            java.lang.String namespace)
            throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);

            if (prefix == null) {
                prefix = generatePrefix(namespace);

                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();

                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);

                    if ((uri == null) || (uri.length() == 0)) {
                        break;
                    }

                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }

                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }

            return prefix;
        }

        /**
         * databinding method to get an XML representation of this object
         *
         */
        public javax.xml.stream.XMLStreamReader getPullParser(
            javax.xml.namespace.QName qName)
            throws org.apache.axis2.databinding.ADBException {
            java.util.ArrayList elementList = new java.util.ArrayList();
            java.util.ArrayList attribList = new java.util.ArrayList();

            if (local_returnTracker) {
                elementList.add(new javax.xml.namespace.QName("", "return"));

                if (local_return == null) {
                    throw new org.apache.axis2.databinding.ADBException(
                        "return cannot be null!!");
                }

                elementList.add(local_return);
            }

            return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName,
                elementList.toArray(), attribList.toArray());
        }

        /**
         *  Factory class that keeps the parse method
         */
        public static class Factory {
            /**
             * static method to create the object
             * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
             *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
             * Postcondition: If this object is an element, the reader is positioned at its end element
             *                If this object is a complex type, the reader is positioned at the end element of its outer element
             */
            public static GetPriorAuthHeaderResponse parse(
                javax.xml.stream.XMLStreamReader reader)
                throws java.lang.Exception {
                GetPriorAuthHeaderResponse object = new GetPriorAuthHeaderResponse();

                int event;
                java.lang.String nillableValue = null;
                java.lang.String prefix = "";
                java.lang.String namespaceuri = "";

                try {
                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.getAttributeValue(
                                "http://www.w3.org/2001/XMLSchema-instance",
                                "type") != null) {
                        java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                "type");

                        if (fullTypeName != null) {
                            java.lang.String nsPrefix = null;

                            if (fullTypeName.indexOf(":") > -1) {
                                nsPrefix = fullTypeName.substring(0,
                                        fullTypeName.indexOf(":"));
                            }

                            nsPrefix = (nsPrefix == null) ? "" : nsPrefix;

                            java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(
                                        ":") + 1);

                            if (!"getPriorAuthHeaderResponse".equals(type)) {
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext()
                                                               .getNamespaceURI(nsPrefix);

                                return (GetPriorAuthHeaderResponse) ExtensionMapper.getTypeObject(nsUri,
                                    type, reader);
                            }
                        }
                    }

                    // Note all attributes that were handled. Used to differ normal attributes
                    // from anyAttributes.
                    java.util.Vector handledAttributes = new java.util.Vector();

                    reader.next();

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement() &&
                            new javax.xml.namespace.QName("", "return").equals(
                                reader.getName())) {
                        object.set_return(PriorAuthHeader.Factory.parse(reader));

                        reader.next();
                    } // End of if for expected property start element

                    else {
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement()) {
                        // A start element we are not expecting indicates a trailing invalid property
                        throw new org.apache.axis2.databinding.ADBException(
                            "Unexpected subelement " + reader.getName());
                    }
                } catch (javax.xml.stream.XMLStreamException e) {
                    throw new java.lang.Exception(e);
                }

                return object;
            }
        } //end of factory class
    }
}
