/**
 * Description
 *		This file methods to get values from the properties file for Prior Authirization Queue.
 *
 * CHANGE History
 * 		Version 1.0
 * 			Initial version
 *  	Version 1.1			03/08/2017
 *  		Use the constant for the properties file.
 *  
 */

package com.avalon.lbm.services;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;

import com.avalon.priorAuth.search.beans.QueueSearchConstants;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.StringPool;

public class EsbUrlDelegate {
	
	private static final Log log = LogFactoryUtil.getLog(EsbUrlDelegate.class.getName());

	private static EsbUrlDelegate esbUrlDelegate = null;
	private static Properties properties = null;

	/**
	 * @return the properties
	 */
	public static Properties getProperties() {
	    return properties;
	}

	/**
	 * @param properties the properties to set
	 */
	public static void setProperties(Properties properties) {
	    EsbUrlDelegate.properties = properties;
	}
	
	
	/**
	 * To load the properties file from the server bin directory and to fetch the esb-end point urls.
	 * @return
	 */
	
   static{
		if(properties==null || properties.size()==0){
		    properties = new Properties();
			esbUrlDelegate = new EsbUrlDelegate();
			String liferayHome = System.getProperty("LIFERAY_HOME");
			String propertyLocation = liferayHome +StringPool.SLASH + QueueSearchConstants.PROPERTIES_FILE;
		    try {
				properties.load(new FileInputStream(new File(propertyLocation)));
			    log.info("EsbUrlDelegate for PA Queue::propertyLocation: " + propertyLocation);
			    properties.load(new FileInputStream(new File(propertyLocation)));
			    log.info("properties ----> " + properties);
			} catch (FileNotFoundException e) {
				log.error("FileNotFoundException in static block",e);
			} catch (IOException e) {
				log.error("IOException in static block",e);
			}
		}
	}

   public static EsbUrlDelegate getEsbUrlDelegate(){
		return esbUrlDelegate;
	}
	
	 /**
	     * Fetching the esb end point urls from the properties object
	     * Setting to the esb end point urls in the map object
	     * 
	     */
	public Map<String,String> getEsbUrl(){
		Map<String,String> urls = new ConcurrentHashMap<String,String>();
		try {
			String serverInstance = System.getProperty("serverInstance");
			log.info("ServerInstanceAtEsbDelegate :" +serverInstance);
			log.info("EsbUrlMemberService :" +serverInstance+".serviceHeader.endpoint");
			urls.put("serviceHeaderUrl",properties.getProperty(serverInstance+".serviceHeader.endpoint"));
			urls.put("serviceReviewUrl",properties.getProperty(serverInstance+".reviewHeader.endpoint"));
		} catch (Exception e) {
			log.error("Exception in getEsbUrl method",e);
		}
	 return urls;
	}
	
	
}
