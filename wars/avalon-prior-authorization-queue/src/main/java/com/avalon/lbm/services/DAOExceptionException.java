
/**
 * DAOExceptionException.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.3  Built on : Jun 27, 2015 (11:17:49 BST)
 */

package com.avalon.lbm.services;

public class DAOExceptionException extends java.lang.Exception{

    private static final long serialVersionUID = 1454935204793L;
    
    private com.avalon.lbm.services.PriorAuthHeaderServiceStub.DAOExceptionE faultMessage;
        public DAOExceptionException() {
            super("DAOExceptionException");
        }
        public DAOExceptionException(java.lang.String s) {
           super(s);
        }
        public DAOExceptionException(java.lang.String s, java.lang.Throwable ex) {
          super(s, ex);
        }
        public DAOExceptionException(java.lang.Throwable cause) {
            super(cause);
        }
    public void setFaultMessage(com.avalon.lbm.services.PriorAuthHeaderServiceStub.DAOExceptionE msg){
       faultMessage = msg;
    }
    public com.avalon.lbm.services.PriorAuthHeaderServiceStub.DAOExceptionE getFaultMessage(){
       return faultMessage;
    }
}
    