package com.avalon.priorAuth.search.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletMode;
import javax.portlet.PortletModeException;
import javax.portlet.PortletRequest;
import javax.portlet.PortletSession;
import javax.portlet.PortletURL;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.portlet.WindowState;
import javax.portlet.WindowStateException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;

/**
 * Description
 *		This file is the controller for the Prior Authorization Queue portlet.
 *
 * CHANGE History
 * 		Version 1.0
 *			inital version.
 * 		Version 1.1  
 *			Send the signed in user to the search method.
 * 		Version 1.2  
 *			Added going to the procedure information page for a Pos Service Request record.
 *
 */

import com.avalon.priorAuth.search.beans.MemberDetailsFO;
import com.avalon.priorAuth.search.beans.QueueSearchConstants;
import com.avalon.priorAuth.search.beans.QueueSearchFO;
import com.avalon.priorAuth.search.businessdelegate.PriorAuthSearchBusinessDelegateVO;
import com.avalon.priorAuth.search.customexception.CustomException;
import com.google.gson.Gson;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Layout;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.model.UserGroup;
import com.liferay.portal.kernel.portlet.PortletURLFactoryUtil;
import com.liferay.portal.kernel.service.LayoutLocalServiceUtil;
import com.liferay.portal.kernel.service.UserGroupLocalServiceUtil;
import com.liferay.portal.kernel.service.UserLocalServiceUtil;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.WebKeys;

@Controller(value = "PriorAuthQueue")
@RequestMapping("VIEW")
public class PriorAuthQueue {
	
	private static final Log log = LogFactoryUtil.getLog(PriorAuthQueue.class.getName());
    
    /**
     * whenever the view page has been clicked the render method will be executed 
     * In this method we are removing the session variables which has been set in avalon-prior-authorization-portlet.
     * It redirects to the view page.
    */
    @RenderMapping
    public String handleRenderRequest(RenderRequest request, 
    		                          RenderResponse response, 
    		                          Model model) {
	
    	PortletSession portletSession = request.getPortletSession();
	    log.info("Start handleRenderRequest Method for Prior Auth Queue");

        // Get a list of the users in the PA Team.  An exception in this block should not stop the remaining code from executing.
        List<String> userList = null;
        try {
        	userList = new ArrayList<String>();
         	
        	User currentUser = PortalUtil.getUser(request);
        	long companyId = currentUser.getCompanyId();
        	
        	// Get the users assigned to a user group
        	UserGroup userGroup = UserGroupLocalServiceUtil.getUserGroup(companyId, "PATeam");
        	long userGroupId = userGroup.getUserGroupId();
        	
        	List<User> users = UserLocalServiceUtil.getUserGroupUsers(userGroupId);
        	for(User usr:users) {
        	    String nextName = usr.getFullName();
        		userList.add(nextName);
        	}
        } catch (SystemException e) {
        	log.error("SystemException in handleRenderRequest method",e);
        } catch (PortalException e) {
        	log.error("PortalException in handleRenderRequest method",e);
        }
        request.setAttribute("userList", userList);

    	portletSession.removeAttribute("authorizationAge", PortletSession.APPLICATION_SCOPE);  
        portletSession.removeAttribute("authorizationStatus", PortletSession.APPLICATION_SCOPE);
        portletSession.removeAttribute("enteredNotes", PortletSession.APPLICATION_SCOPE);
        portletSession.removeAttribute("peerEnteredNotes", PortletSession.APPLICATION_SCOPE);
        portletSession.removeAttribute("appCreateDateTime", PortletSession.APPLICATION_SCOPE);
        portletSession.removeAttribute("originalUserId", PortletSession.APPLICATION_SCOPE);
        portletSession.removeAttribute("authDecisionDate", PortletSession.APPLICATION_SCOPE);
        portletSession.removeAttribute("providerAuthStatus", PortletSession.APPLICATION_SCOPE);
	    
	    // Remove the attributes shown in the prior authorization pages
	    portletSession.removeAttribute("memberDob", PortletSession.APPLICATION_SCOPE);
	    portletSession.removeAttribute("memberId", PortletSession.APPLICATION_SCOPE);
	    portletSession.removeAttribute("memberNumber", PortletSession.APPLICATION_SCOPE);
	    portletSession.removeAttribute("healthPlan", PortletSession.APPLICATION_SCOPE);
	    portletSession.removeAttribute("mpi", PortletSession.APPLICATION_SCOPE);
	    portletSession.removeAttribute("membergroupId", PortletSession.APPLICATION_SCOPE);
	    portletSession.removeAttribute("memberName", PortletSession.APPLICATION_SCOPE);
	    portletSession.removeAttribute("memberGender", PortletSession.APPLICATION_SCOPE);
	    portletSession.removeAttribute("memberAddressLine1", PortletSession.APPLICATION_SCOPE);
	    portletSession.removeAttribute("memberAddressLine2",PortletSession.APPLICATION_SCOPE);
	    portletSession.removeAttribute("memberCity", PortletSession.APPLICATION_SCOPE);
	    portletSession.removeAttribute("memberStateCode", PortletSession.APPLICATION_SCOPE);
	    portletSession.removeAttribute("memberZipcode", PortletSession.APPLICATION_SCOPE);
	    portletSession.removeAttribute("memberPhoneNumber", PortletSession.APPLICATION_SCOPE);

		// Remove the additional attributes saved by the prior authorization pages
		portletSession.removeAttribute("memberFirstName", PortletSession.APPLICATION_SCOPE);
		portletSession.removeAttribute("memberMiddleName", PortletSession.APPLICATION_SCOPE);
		portletSession.removeAttribute("memberLastName", PortletSession.APPLICATION_SCOPE);
		portletSession.removeAttribute("memberSuffixName", PortletSession.APPLICATION_SCOPE);
		portletSession.removeAttribute("businessSectorCode", PortletSession.APPLICATION_SCOPE);
		portletSession.removeAttribute("businessSectorDescription", PortletSession.APPLICATION_SCOPE);
		portletSession.removeAttribute("businessSegmentCode", PortletSession.APPLICATION_SCOPE);
		portletSession.removeAttribute("businessSegmentDescription", PortletSession.APPLICATION_SCOPE);

		portletSession.removeAttribute("subscriberName", PortletSession.APPLICATION_SCOPE);

		log.info("End handleRenderRequest Method for Prior Auth Queue");

	    return "view";
    }
    

    /**
     * 
     * whenever the search button  has been clicked the authSearchDetails method will be executed 
     * In this method we will fetch information for the queue table 
	 * This method is for a resource URL.
	 * 
     */
    @ResourceMapping(value="queueSearchAction")
    public void queueSearchDetails(ResourceRequest request,
    		                       ResourceResponse response,
    		                       @ModelAttribute("queueSearchFO") QueueSearchFO queueSearchFO) throws IOException {

	    log.info("Processing queueSearchDetails Method for Prior Auth Queue Entry");
		String queueInfo = null;
		Gson gson = new Gson();  
		List list;
		try {
			String inprocessView = ParamUtil.getString(request, "hiddenInprocessView");
	        log.info("inprocessView : " + inprocessView);
	    	
			String searchType = ParamUtil.getString(request, "searchType");
	        log.info("searchType : " + searchType);
	
	        String authType = ParamUtil.getString(request, "hiddenAuthType");
	        log.info("authType : " + authType);

	        if (searchType.equals(QueueSearchConstants.AUTH_NUMBER)) {
	            log.info("Authorization Number : " + queueSearchFO.getAuthorizationNumber());
	        } else if (searchType.equals(QueueSearchConstants.WORKED_BY)) {
	        	String workedBy = ParamUtil.getString(request, "authWorkedBy");
	        	queueSearchFO.setWorkedBy(workedBy);
	            log.info("Worked By : " + queueSearchFO.getWorkedBy());
	        } else if (searchType.equals(QueueSearchConstants.PRIORITY_LEVEL)) {
	        	String priorityLevel = ParamUtil.getString(request, "authPriority");
	        	queueSearchFO.setPriorityLevel(priorityLevel);
	            log.info("Priority Leval : " + queueSearchFO.getPriorityLevel());
	        } else if (searchType.equals(QueueSearchConstants.DATE_RECEIVED)) {
	        	String dateReceived = ParamUtil.getString(request, "authDateReceived");
	        	queueSearchFO.setDateReceived(dateReceived);
	            log.info("Date Received : " + queueSearchFO.getDateReceived());
	        } else if (searchType.equals(QueueSearchConstants.CARD_NUMBER)) {
	            log.info("ID Card Number : " + queueSearchFO.getMemberId());
	        } else {
	        	log.warn("Invalid search type");
	        }

 	        // Get the records that match the search type 
	        PriorAuthSearchBusinessDelegateVO priorAuthSearchBusinessDelegateVO = PriorAuthSearchBusinessDelegateVO.getPriorAuthSearchBusinessDelegateVO();
	        list = priorAuthSearchBusinessDelegateVO.findPriorAuths(searchType, inprocessView, authType, queueSearchFO);

			// Covert the list into Json Response 
			queueInfo = gson.toJson(list); 
			
			// Set the coverted json response into response object
			response.getWriter().write(queueInfo.toString());

			log.info("Processing queueSearchDetails Method for Prior Auth Queue Exit");
		} catch (Exception e) {
			log.error("Exception in queueSearchDetails method",e);
        }
    }  
 
    /**
     * Whenever user click on an auth number hyperlink the PriorAuth ProcedureInfo page will be opened
     */
    @ActionMapping(params = "action=getAuthorizationDetails")
    public void getAuthorizationDetails(ActionRequest actionRequest,
	                                    ActionResponse actionResponse) {

    	log.info("Processing getAuthorizationDetails Method for Prior Auth Queue Entry");
    	
		String idCardNumber = ParamUtil.getString(actionRequest, "idCardNumberLink");
		log.info("ID Card Number : " + idCardNumber);
    	String authorizationNumber = ParamUtil.getString(actionRequest, "authorizationNumberLink");
	    log.info("Authorization Number : " + authorizationNumber);
        
    	String systemAdminError = "Error while processing the request. Please contact system administrator";
        String noRecordsError = "No authorizations were found that match your search criteria. Please update the search and try again.";
        String invalidMemberError = "Invalid Member Data";
        String mvcPathStr = "/html/jsps/view.jsp";

        // Get the member data 
        Map<Integer, MemberDetailsFO> memberList = null;
        PriorAuthSearchBusinessDelegateVO priorAuthSearchBusinessDelegateVO = PriorAuthSearchBusinessDelegateVO.getPriorAuthSearchBusinessDelegateVO();
        try {
        	memberList = priorAuthSearchBusinessDelegateVO.findMember(idCardNumber, authorizationNumber);

        	// Redirect to the avalon-prior-authorization portlet 
    		int sz = memberList.size();
    		log.info("Member List Size : " + sz);
			if (sz == 1) {
				MemberDetailsFO memberDetailsFo = memberList.get(0);

				HttpServletRequest httpRequest = PortalUtil.getHttpServletRequest(actionRequest);
				HttpSession httpSession = PortalUtil.getHttpServletRequest(actionRequest).getSession();
				ThemeDisplay themeDisplay1 = (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
				Layout layout;
				PortletSession portletSession = actionRequest.getPortletSession();
				
				log.info("member : " + memberDetailsFo);
				log.info("member setMemberNumber : " + memberDetailsFo.getMemberNumber());
				
				  //TODO: Remove mock service code and condition. Don't remove else condition code
				if(GetterUtil.getBoolean(com.liferay.portal.kernel.util.PropsUtil.get(QueueSearchConstants.ENABLED_MOCK_SERVICE))){
					 //Success Mock Code
					memberDetailsFo.setMemberId("22");
				}
				if ((memberDetailsFo.getMemberNumber() != null) && !(memberDetailsFo.getMemberNumber().equals(""))) {
				    httpSession.setAttribute("member", memberDetailsFo);
					
				    log.info("Member ID : " + memberDetailsFo.getMemberId());
                    log.info("Member First Name : " + memberDetailsFo.getMemberFirstName());
                    log.info("Member Middle Name : " + memberDetailsFo.getMemberMiddleName());
                    log.info("Member Last Name : " + memberDetailsFo.getMemberLastName());
                    log.info("Member Suffix Name : " + memberDetailsFo.getMemberSuffixName());
                    log.info("Member DOB : " + memberDetailsFo.getMemberDob());
                    log.info("Member Gender : " + memberDetailsFo.getGender());
                    log.info("Member Address Line 1 : " + memberDetailsFo.getMemberAddressLine1());
                    log.info("Member Address Line 2 : " + memberDetailsFo.getMemberAddressLine2());
                    log.info("Member City : " + memberDetailsFo.getMemberCity());
                    log.info("Member State : " + memberDetailsFo.getMemberStateCode());
                    log.info("Member Zip Code : " + memberDetailsFo.getMemberZipcode());
                    log.info("Member Phone Number : " + memberDetailsFo.getMemberPhoneNumber());
                    
				    log.info("Business Sector Code : " + memberDetailsFo.getBusinessSectorCode());
					log.info("Business Sector Description : " + memberDetailsFo.getBusinessSectorDescription());
					log.info("Business Segment Code : " + memberDetailsFo.getBusinessSegmentCode());
					log.info("Business Segment Description : " + memberDetailsFo.getBusinessSegmentDescription());
				
				    // Setting the member details in session variables that are used by the prior auth procedure page
				    portletSession.setAttribute("memberDob", memberDetailsFo.getMemberDob(), PortletSession.APPLICATION_SCOPE);
				    portletSession.setAttribute("memberId", memberDetailsFo.getMemberId(), PortletSession.APPLICATION_SCOPE);
				    portletSession.setAttribute("authorizationNumber", memberDetailsFo.getPriorAuthNumber(), PortletSession.APPLICATION_SCOPE);
				    portletSession.setAttribute("authorizationKey",Long.valueOf(memberDetailsFo.getAuthorizationKeyResponse()), PortletSession.APPLICATION_SCOPE);
				    portletSession.setAttribute("healthPlan", memberDetailsFo.getHealthPlan(), PortletSession.APPLICATION_SCOPE);
				    portletSession.setAttribute("mpi", memberDetailsFo.getMpi(), PortletSession.APPLICATION_SCOPE);
				    portletSession.setAttribute("memberName", memberDetailsFo.getMemberFirstName() + " "+ memberDetailsFo.getMemberLastName(), PortletSession.APPLICATION_SCOPE);
				    portletSession.setAttribute("membergroupId", memberDetailsFo.getGroupId(), PortletSession.APPLICATION_SCOPE);
				    portletSession.setAttribute("authSubmissionStatus", memberDetailsFo.getAuthSubmissionStatusCode(), PortletSession.APPLICATION_SCOPE);
				    portletSession.setAttribute("memberGender", memberDetailsFo.getDisplayGender(), PortletSession.APPLICATION_SCOPE);
				    portletSession.setAttribute("memberAddressLine1", memberDetailsFo.getMemberAddressLine1(), PortletSession.APPLICATION_SCOPE);
				    portletSession.setAttribute("memberAddressLine2", memberDetailsFo.getMemberAddressLine2(), PortletSession.APPLICATION_SCOPE);
				    portletSession.setAttribute("memberCity", memberDetailsFo.getMemberCity(), PortletSession.APPLICATION_SCOPE);
				    portletSession.setAttribute("memberStateCode", memberDetailsFo.getMemberStateCode(), PortletSession.APPLICATION_SCOPE);
				    portletSession.setAttribute("memberZipcode", memberDetailsFo.getMemberZipcode(), PortletSession.APPLICATION_SCOPE);
				    portletSession.setAttribute("memberPhoneNumber", memberDetailsFo.getMemberPhoneNumber(), PortletSession.APPLICATION_SCOPE);

    				// Set the additional attributes saved by the prior authorization queue page
    				portletSession.setAttribute("memberFirstName", memberDetailsFo.getMemberFirstName(), PortletSession.APPLICATION_SCOPE);
    				portletSession.setAttribute("memberMiddleName", memberDetailsFo.getMemberMiddleName(), PortletSession.APPLICATION_SCOPE);
    				portletSession.setAttribute("memberLastName", memberDetailsFo.getMemberLastName(), PortletSession.APPLICATION_SCOPE);
    				portletSession.setAttribute("memberSuffixName", memberDetailsFo.getMemberSuffixName(), PortletSession.APPLICATION_SCOPE);
    				portletSession.setAttribute("businessSectorCode", memberDetailsFo.getBusinessSectorCode(), PortletSession.APPLICATION_SCOPE);
    				portletSession.setAttribute("businessSectorDescription", memberDetailsFo.getBusinessSectorDescription(), PortletSession.APPLICATION_SCOPE);
    				portletSession.setAttribute("businessSegmentCode", memberDetailsFo.getBusinessSegmentCode(), PortletSession.APPLICATION_SCOPE);
    				portletSession.setAttribute("businessSegmentDescription", memberDetailsFo.getBusinessSegmentDescription(), PortletSession.APPLICATION_SCOPE);
   	        	
    				// Set the current user to the logged in user.
					ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
					String currentUser = themeDisplay.getUser().getEmailAddress();
					log.info("Current User : " + currentUser);
					
    				// Include the logged in user id in the operation type.  The procedure page will use this to log the PA viewed.
    				portletSession.setAttribute("operationType", QueueSearchConstants.QUEUE + currentUser, PortletSession.APPLICATION_SCOPE);

					boolean privateFlag = true;   // True for a private page

					// Set the page for PA or PSR
					String pageUrl = "prior-auth-page-url";
					String procedureInfoPage = "procedure-info-page";
					if (memberDetailsFo.getPriorAuthNumber().contains("PR")) {
						// Set to PSR
						pageUrl = "post-service-page-url";
						procedureInfoPage = "psr_procedure-info-page";
					}
					
				    layout = LayoutLocalServiceUtil.getFriendlyURLLayout(themeDisplay1.getLayout().getGroupId(), privateFlag, com.liferay.util.portlet.PortletProps.get(pageUrl));
				    PortletURL portletURL = PortletURLFactoryUtil.create(httpRequest, com.liferay.util.portlet.PortletProps.get(procedureInfoPage), layout.getPlid(), PortletRequest.RENDER_PHASE);
				    portletURL.setWindowState(WindowState.MAXIMIZED);
				    portletURL.setPortletMode(PortletMode.VIEW);
				    log.info("To String :" + portletURL.toString());
				    actionResponse.sendRedirect(portletURL.toString());
				} else {
				    actionRequest.setAttribute("message", invalidMemberError);
				    actionResponse.setRenderParameter("mvcPath", mvcPathStr);
				}
			} else {
            	actionResponse.setRenderParameter("message", noRecordsError);
            	actionResponse.setRenderParameter("mvcPath", mvcPathStr);
			}
    		log.info("Processing getAuthorizationDetails Method for Prior Auth Queue Exit");
        } catch (IOException ex) {
            log.info("Prior Auth Queue IOException");
            if (ex.getMessage().equals("No Records")) {
            	log.error("IOException getAuthorizationDetails method when Records are not found", ex);
            	log.info("Prior Auth Queue No Records");
                
                // Re-directing to the view page
            	actionResponse.setRenderParameter("message", noRecordsError);
            	actionResponse.setRenderParameter("mvcPath", mvcPathStr);
            } else {
                log.error("IOException during Prior Auth Queue Socket Time Out in getAuthorizationDetails method", ex);
                
                // Re-directing to the view page
                actionResponse.setRenderParameter("message", systemAdminError);
            	actionResponse.setRenderParameter("mvcPath", mvcPathStr);
            }
        } catch (CustomException ex) {
        	log.error("CustomException in getAuthorizationDetails method", ex);
		   
	        // Re-directing to the view page
		    actionResponse.setRenderParameter("message", ex.getMessage());
        	actionResponse.setRenderParameter("mvcPath", mvcPathStr);
        } catch (PortalException ex) {
        	log.error("PortalException in getAuthorizationDetails method", ex);
		  
        	// Re-directing to the view page
		    actionResponse.setRenderParameter("message", ex.getMessage());
        	actionResponse.setRenderParameter("mvcPath", mvcPathStr);
        } catch (PortletModeException ex) {
        	log.error("PortletModeException in getAuthorizationDetails method", ex);
		   
	        // Re-directing to the view page
		    actionResponse.setRenderParameter("message", ex.getMessage());
        	actionResponse.setRenderParameter("mvcPath", mvcPathStr);
        } catch (NullPointerException ex) {
        	log.error("NullPointerException in getAuthorizationDetails method", ex);
           
            // Re-directing to the view page
            actionResponse.setRenderParameter("message", noRecordsError);
        	actionResponse.setRenderParameter("mvcPath", mvcPathStr);
    	} catch (SystemException ex) {
    		log.error("SystemException in getAuthorizationDetails method", ex);
		    
	        // Re-directing to the view page
		    actionResponse.setRenderParameter("message", ex.getMessage());
        	actionResponse.setRenderParameter("mvcPath", mvcPathStr);
    	} catch (WindowStateException ex) {
    		log.error("WindowStateException in getAuthorizationDetails method", ex);
		    
	        // Re-directing to the view page
		    actionResponse.setRenderParameter("message", ex.getMessage());
        	actionResponse.setRenderParameter("mvcPath", mvcPathStr);
        }
    }

    
    /**
     * Whenever user click on cancel button cancelPage method will be executed
     */
    @ActionMapping(params = "action=cancelPage")
    public void cancelPage(ActionRequest actionRequest,
	                       ActionResponse actionResponse) {

    	log.info("Processing Cancel Action for Prior Auth Queue Entry");
	    String userName = null;
	    List usersList = null;
		try {
		    User user = PortalUtil.getUser((PortletRequest) actionRequest);
		    if (user != null) {
			    usersList = user.getUserGroups();
		    }
		} catch (PortalException e) {
			log.error("PortalException in cancelPage method",e);
		} catch (SystemException e) {
			log.error("SystemException in cancelPage method",e);
		}
	    ArrayList<String> groupNames = new ArrayList<String>();
	    if (usersList != null) {
	    	int length = usersList.size();
		    for (int i = 0; i < length; ++i) {
		    	
		    	// Ignore the groups that do not have a home page
				if (!((UserGroup)usersList.get(i)).getName().equalsIgnoreCase("CDSUsers") &&
					!((UserGroup)usersList.get(i)).getName().equalsIgnoreCase("Everyone") &&
					!((UserGroup)usersList.get(i)).getName().equalsIgnoreCase("ManagedUsers") &&
					!((UserGroup)usersList.get(i)).getName().equalsIgnoreCase("Multi-Factor") &&
					!((UserGroup)usersList.get(i)).getName().equalsIgnoreCase("PortalAdmin") &&
					!((UserGroup)usersList.get(i)).getName().equalsIgnoreCase("UMGroup")) {
					userName = ((UserGroup) usersList.get(i)).getName();
					groupNames.add(userName);
				}
		    }
	    }
    	
    	HttpServletRequest request = PortalUtil.getHttpServletRequest((PortletRequest) actionRequest);
	    String path = PortalUtil.getCurrentCompleteURL((HttpServletRequest) request);
	    StringBuilder output = new StringBuilder();
	    int count = 0;
	    char[] ch = path.toCharArray();
	    for (int i2 = 0; i2 < ch.length; ++i2) {
			if (ch[i2] == '/') {
			    ++count;
			}
			if (count >= 3)
			    continue;
			output = output.append(ch[i2]);
	    }

	    String landingPage = null;
	    if (usersList == null) {
			
			// Set to the default landing page since the user does not belong to any groups
			landingPage = "/web/guest/home";
	    } else {
	    	

	    	// Get the landing page based on the user's group
			landingPage = com.liferay.portal.kernel.util.PropsUtil.get("default.landing.page.path[" + userName  + "]"); 
			
			// If the user's group is not found use the defaule landing page
			if (landingPage == null) {
				landingPage = com.liferay.portal.kernel.util.PropsUtil.get("default.landing.page.path");
				if (landingPage == null) {
					
					// Set to the default landing page since it is not defined in the properties file
					landingPage = "/web/guest/home";
				}
			}
	    }
	    String pathRedirect = null;
		pathRedirect = output + landingPage;
		log.info(userName + " " + pathRedirect);
		try {
		    actionResponse.sendRedirect(pathRedirect);
		} catch (IOException e) {
			log.error("IOException during redirection in cancelPage method", e);
		}
    	log.info("Processing Cancel Action for Prior Auth Queue Exit");
    }

    /**
     * Default method
     */
    @ActionMapping
    public void providerHandler() {
        log.info("search handler");
    }
    
}
