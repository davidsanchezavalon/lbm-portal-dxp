package com.avalon.priorAuth.search.beans;

/**
 * Description
 *		This file contain the variables for the Prior Authorization Queue table.
 *
 * CHANGE History
 * 	Version 1.0
 *			Initial version.
 *
 */

import java.io.Serializable;

public class QueueDetailsFO implements Serializable {
	
	/**
     * The columns in the Prior Authorization Queue table
	 */
	private static final long serialVersionUID = -6258273394902792568L;
	private String authorizationNumber;
	private String authorizationAge;

	/**
	 * @return the authorizationNumber
	 */
	public String getAuthorizationNumber() {
		return authorizationNumber;
	}
	/**
	 * @param authorizationNumber the authorizationNumber to set
	 */
	public void setAuthorizationNumber(String authorizationNumber) {
		this.authorizationNumber = authorizationNumber;
	}

	/**
	 * @return the authorizationAge
	 */
	public String getAuthorizationAge() {
		return authorizationAge;
	}
	/**
	 * @param authorizationAge the authorizationAge to set
	 */
	public void setAuthorizationAge(String authorizationAge) {
		this.authorizationAge = authorizationAge;
	}
}
