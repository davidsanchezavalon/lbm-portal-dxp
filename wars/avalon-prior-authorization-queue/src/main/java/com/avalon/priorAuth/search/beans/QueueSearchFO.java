package com.avalon.priorAuth.search.beans;

/**
 * Description
 *		This file contain the screen variables for the Prior Authorization Queue.
 *
 * CHANGE History
 * 	Version 1.0
 *			Initial version.
 *
 */

import java.io.Serializable;

public class QueueSearchFO implements Serializable {
	
    /**
     * To store the Prior Auth Queue fields
     */
    private static final long serialVersionUID = 8246097910683363742L;
	private String authorizationNumber;
	private String workedBy;
	private String priorityLevel;
	private String dateReceived;
	private String memberId;
	private String healthPlanLob;
	private String submissionStatus;
	private String authorizationAge;

	/**
	 * @return the authorizationNumber
	 */
	public String getAuthorizationNumber() {
		return authorizationNumber;
	}
	/**
	 * @param authorizationNumber the authorizationNumber to set
	 */
	public void setAuthorizationNumber(String authorizationNumber) {
		this.authorizationNumber = authorizationNumber;
	}

	/**
	 * @return the dateReceived
	 */
	public String getDateReceived() {
		return dateReceived;
	}
	/**
	 * @param dateReceived the dateReceived to set
	 */
	public void setDateReceived(String dateReceived) {
		this.dateReceived = dateReceived;
	}

	/**
	 * @return the priorityLevel
	 */
	public String getPriorityLevel() {
		return priorityLevel;
	}
	/**
	 * @param priorityLevel the priorityLevel to set
	 */
	public void setPriorityLevel(String priorityLevel) {
		this.priorityLevel = priorityLevel;
	}

	/**
	 * @return the workedBy
	 */
	public String getWorkedBy() {
		return workedBy;
	}
	/**
	 * @param workedBy the workedBy to set
	 */
	public void setWorkedBy(String workedBy) {
		this.workedBy = workedBy;
	}

	/**
	 * @return the memberId
	 */
	public String getMemberId() {
		return memberId;
	}
	/**
	 * @param memberId the memberId to set
	 */
	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}

	/**
	 * @return the healthPlanLob
	 */
	public String getHealthPlanLob() {
		return healthPlanLob;
	}
	/**
	 * @param healthPlanLob the healthPlanLob to set
	 */
	public void setHealthPlanLob(String healthPlanLob) {
		this.healthPlanLob = healthPlanLob;
	}

	/**
	 * @return the submissionStatus
	 */
	public String getSubmissionStatus() {
		return submissionStatus;
	}
	/**
	 * @param submissionStatus the submissionStatus to set
	 */
	public void setSubmissionStatus(String submissionStatus) {
		this.submissionStatus = submissionStatus;
	}

	/**
	 * @return the authorizationAge
	 */
	public String getAuthorizationAge() {
		return authorizationAge;
	}
	/**
	 * @param authorizationAge the authorizationAge to set
	 */
	public void setAuthorizationAge(String authorizationAge) {
		this.authorizationAge = authorizationAge;
	}
}
