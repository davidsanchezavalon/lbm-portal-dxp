package com.avalon.priorAuth.search.utils;

import java.io.FileInputStream;
import java.io.InputStream;
import java.rmi.RemoteException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.axis2.AxisFault;

import com.avalon.lbm.services.DAOExceptionException;
import com.avalon.lbm.services.PriorAuthHeaderServiceStub;
import com.avalon.lbm.services.PriorAuthHeaderServiceStub.PriorAuthHeader;
import com.avalon.lbm.services.PriorAuthHeaderServiceStub.PriorAuthHeaderDTO;
import com.avalon.lbm.services.PriorAuthReviewServiceStub;
import com.avalon.lbm.services.PriorAuthReviewServiceStub.PriorAuthReview;
import com.avalon.priorAuth.search.beans.QueueSearchConstants;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.StringPool;

/**
 * Description
 *		This file contains the aging methods used by the queue controller.
 *
 * CHANGE History
 * 		Version 1.0
 * 			Initial version
 * 		Version 1.1
 * 			For a Completed - Withdrawn record with the date letter recieved not set, use the AppMainDatetime as the end time.
 *  	Version 1.1			03/08/2017
 *  		Use the constant for the properties file.
 *  
 */

public class AgeCalculation {

	private static final Log log = LogFactoryUtil.getLog(AgeCalculation.class.getName());

	private static AgeCalculation ageCaLCulation = null;

	private AgeCalculation() {
	}

	public static AgeCalculation getAgeCalculation() {
		return ageCaLCulation;
	}

	static {
		if (ageCaLCulation == null) {
			ageCaLCulation = new AgeCalculation();
		}
	}

	
	/**
	 * Convert the Saved Date and Time to the correct format
	 */
	public String getProcedureSavedDate(String dateStr) { 
		String input = dateStr;
		SimpleDateFormat inputformat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
		SimpleDateFormat outputformat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		Date date = null;
		String output = null;

		try {
			date = inputformat.parse(input);
			output = outputformat.format(date);
		}
		catch (ParseException e) {
			log.error("ParseException in getProcedureSavedDate method",e);
		}
		return output;
	}
	

	/**
	 * Get the requested date and time.  Return the creation date and time if the requested date/time is not set.
	 */
	private String getAuthRequestDateAndTime(String savedDateTime, Long authorizationKey) {
		String returnVal = null;
		
		try {
            PriorAuthHeaderServiceStub priorAuthHeaderServiceStub = new PriorAuthHeaderServiceStub();
            PriorAuthHeaderServiceStub.GetPriorAuthHeaderE getPriorAuthHeaderE = new PriorAuthHeaderServiceStub.GetPriorAuthHeaderE();
            PriorAuthHeaderServiceStub.GetPriorAuthHeader GetPriorAuthHeader2 = new PriorAuthHeaderServiceStub.GetPriorAuthHeader();
            PriorAuthHeader priorAuthHeader = new PriorAuthHeader();
            PriorAuthHeaderDTO[] authHeaderDTO = new PriorAuthHeaderDTO[1];
            authHeaderDTO[0] = new PriorAuthHeaderDTO();
            priorAuthHeader.setPriorAuthHeaderDTO(authHeaderDTO);
            priorAuthHeader.setSourceTab(QueueUtil.getQueueUtil().procedureTab);
			priorAuthHeader.setAuthorizationKey(authorizationKey.longValue());
			GetPriorAuthHeader2.setSearchCriteriaRequest(priorAuthHeader);
			getPriorAuthHeaderE.setGetPriorAuthHeader(GetPriorAuthHeader2);
			
			PriorAuthHeaderServiceStub.GetPriorAuthHeaderResponseE getPriorAuthHeaderResponseE =null;
			PriorAuthHeaderServiceStub.GetPriorAuthHeaderResponse getPriorAuthHeaderResponse = null;
			
			//TODO: Remove mock service code and condition. Don't remove else condition code
			if(GetterUtil.getBoolean(com.liferay.portal.kernel.util.PropsUtil.get(QueueSearchConstants.ENABLED_MOCK_SERVICE))){
				 //Success Mock Code
				 getPriorAuthHeaderResponseE = new PriorAuthHeaderServiceStub.GetPriorAuthHeaderResponseE();
				 getPriorAuthHeaderResponse = new PriorAuthHeaderServiceStub.GetPriorAuthHeaderResponse();
				 PriorAuthHeader priorAuthHeaderObj = new PriorAuthHeader();
				 PriorAuthHeaderDTO priorAuthHeaderDTO = new PriorAuthHeaderDTO();
				 priorAuthHeaderDTO.setAuthRequestedDate("2018-04-26 01:02:00.1");
				 priorAuthHeaderObj.setPriorAuthHeaderDTO(new PriorAuthHeaderDTO[] {priorAuthHeaderDTO});
				 getPriorAuthHeaderResponse.set_return(priorAuthHeaderObj);
				 getPriorAuthHeaderResponseE.setGetPriorAuthHeaderResponse(getPriorAuthHeaderResponse);
				
			}else{
				getPriorAuthHeaderResponseE = priorAuthHeaderServiceStub.getPriorAuthHeader(getPriorAuthHeaderE);
				getPriorAuthHeaderResponse = getPriorAuthHeaderResponseE.getGetPriorAuthHeaderResponse();
				
			}
			 
			if (getPriorAuthHeaderResponse.get_return().getPriorAuthHeaderDTO() != null) {
				PriorAuthHeaderServiceStub.PriorAuthHeaderDTO[] priorAuthHeaderDTO = getPriorAuthHeaderResponse.get_return().getPriorAuthHeaderDTO();

				returnVal = priorAuthHeaderDTO[0].getAuthRequestedDate();
			}

			if ((returnVal == null) || ((returnVal != null) && returnVal.equals("null"))) {
				
				// Return the save date/time
				returnVal = savedDateTime;
			}
				
			// Convert the returned date/time to the correct format
			returnVal = QueueUtil.getQueueUtil().getStandardDateTime(returnVal);
		} catch (AxisFault e) {
			log.error("AxisFault Exception in getAuthRequestDateAndTime method",e);
        } catch (RemoteException e) {
        	log.error("RemoteException in getAuthRequestDateAndTime method",e);
		} catch (DAOExceptionException e) {
			log.error("DAOExceptionException in getAuthRequestDateAndTime method",e);
        }

		log.info("auth requested date/time: " + returnVal);

		return returnVal;
	}


	/**
	 * Get the Notification date and time the letter was sent.  Return the current date and time if the letter sent date/time is not set.
	 * For a VOID record, return the appMaint date and time if the letter sent date/time is not set.
	 */
	private String getLetterSentDateAndTime(Long authorizationKey) {
		String returnVal = null;
		
		try {
			PriorAuthReviewServiceStub priorAuthReviewServiceStub = new PriorAuthReviewServiceStub();
			PriorAuthReviewServiceStub.SearchPriorAuthReviewDetailsE searchPriorAuthReviewDetailsE = new PriorAuthReviewServiceStub.SearchPriorAuthReviewDetailsE();
			PriorAuthReviewServiceStub.SearchPriorAuthReviewDetails searchPriorAuthReviewDetails = new PriorAuthReviewServiceStub.SearchPriorAuthReviewDetails();
			
			PriorAuthReviewServiceStub.ReviewSearchCriteria reviewSearchCriteria = new PriorAuthReviewServiceStub.ReviewSearchCriteria();
			reviewSearchCriteria.setAuthorizationKey(authorizationKey.longValue());
            reviewSearchCriteria.setSourceTab(QueueUtil.getQueueUtil().notificationTab);
			searchPriorAuthReviewDetails.setSearchCriteriaRequest(reviewSearchCriteria);
			searchPriorAuthReviewDetailsE.setSearchPriorAuthReviewDetails(searchPriorAuthReviewDetails);
			
			PriorAuthReviewServiceStub.SearchPriorAuthReviewDetailsResponseE searchPriorAuthReviewDetailsResponseE =null;
			PriorAuthReviewServiceStub.SearchPriorAuthReviewDetailsResponse searchPriorAuthReviewDetailsResponse = null;
			
			 //TODO: Remove mock service code and condition. Don't remove else condition code
			if(GetterUtil.getBoolean(com.liferay.portal.kernel.util.PropsUtil.get(QueueSearchConstants.ENABLED_MOCK_SERVICE))){
				 //Success Mock Code 
				searchPriorAuthReviewDetailsResponseE = new PriorAuthReviewServiceStub.SearchPriorAuthReviewDetailsResponseE();
				searchPriorAuthReviewDetailsResponse = new PriorAuthReviewServiceStub.SearchPriorAuthReviewDetailsResponse();
				
				PriorAuthReview priorAuthReview = new PriorAuthReview();
				priorAuthReview.setAuthorizationKey(100L);
				Calendar cal = Calendar.getInstance(); 
				priorAuthReview.setNotificationSentDate(cal);
				searchPriorAuthReviewDetailsResponse.set_return(priorAuthReview);
				searchPriorAuthReviewDetailsResponseE.setSearchPriorAuthReviewDetailsResponse(searchPriorAuthReviewDetailsResponse);
			}else{
				searchPriorAuthReviewDetailsResponseE = priorAuthReviewServiceStub.searchPriorAuthReviewDetails(searchPriorAuthReviewDetailsE);
				searchPriorAuthReviewDetailsResponse = searchPriorAuthReviewDetailsResponseE.getSearchPriorAuthReviewDetailsResponse();
			}
			PriorAuthReviewServiceStub.PriorAuthReview review = searchPriorAuthReviewDetailsResponse.get_return();

			String usedDate = null;
			String usedTime = null;
            if (review.getNotificationSentDate() != null && !review.getNotificationSentDate().equals("")) {

            	// Use the IE date format
            	usedDate = QueueUtil.getQueueUtil().getNotificationDateFromCalender(review.getNotificationSentDate(), "IE");
            	usedTime = QueueUtil.getQueueUtil().getNotificationTimeFromCalender(review.getNotificationSentDate());
            } else {

				// Use the appMaint date and time for a VOID record
                PriorAuthHeaderServiceStub priorAuthHeaderServiceStub = new PriorAuthHeaderServiceStub();
                PriorAuthHeaderServiceStub.GetPriorAuthHeaderE getPriorAuthHeaderE = new PriorAuthHeaderServiceStub.GetPriorAuthHeaderE();
                PriorAuthHeaderServiceStub.GetPriorAuthHeader GetPriorAuthHeader2 = new PriorAuthHeaderServiceStub.GetPriorAuthHeader();
                PriorAuthHeader priorAuthHeader = new PriorAuthHeader();
                PriorAuthHeaderDTO[] authHeaderDTO = new PriorAuthHeaderDTO[1];
                authHeaderDTO[0] = new PriorAuthHeaderDTO();
                priorAuthHeader.setPriorAuthHeaderDTO(authHeaderDTO);
                priorAuthHeader.setSourceTab(QueueUtil.getQueueUtil().procedureTab);
    			priorAuthHeader.setAuthorizationKey(authorizationKey.longValue());
    			GetPriorAuthHeader2.setSearchCriteriaRequest(priorAuthHeader);
    			getPriorAuthHeaderE.setGetPriorAuthHeader(GetPriorAuthHeader2);
    			
    			PriorAuthHeaderServiceStub.GetPriorAuthHeaderResponseE getPriorAuthHeaderResponseE = null;
    			PriorAuthHeaderServiceStub.GetPriorAuthHeaderResponse getPriorAuthHeaderResponse = null;
				
    			 //TODO: Remove mock service code and condition. Don't remove else condition code
    			if(GetterUtil.getBoolean(com.liferay.portal.kernel.util.PropsUtil.get(QueueSearchConstants.ENABLED_MOCK_SERVICE))){
    				 //Success Mock Code
    				 getPriorAuthHeaderResponseE =new PriorAuthHeaderServiceStub.GetPriorAuthHeaderResponseE();
        			 getPriorAuthHeaderResponse = new PriorAuthHeaderServiceStub.GetPriorAuthHeaderResponse();
        			 
        			 PriorAuthHeader priorAuthHeaderObj = new PriorAuthHeader();
        			 PriorAuthHeaderDTO priorAuthHeaderDTO = new PriorAuthHeaderDTO();
        			 priorAuthHeaderDTO.setPriorAuthSubmissionStatusCode("35");
        			 priorAuthHeaderDTO.setAppMaintDatetime("2017-12-11 11:11:22.1");
        			 priorAuthHeaderObj.setPriorAuthHeaderDTO(new PriorAuthHeaderDTO[]{priorAuthHeaderDTO});
        			 getPriorAuthHeaderResponse.set_return(priorAuthHeaderObj);
        			 getPriorAuthHeaderResponseE.setGetPriorAuthHeaderResponse(getPriorAuthHeaderResponse);
    			}else{
    				getPriorAuthHeaderResponseE = priorAuthHeaderServiceStub.getPriorAuthHeader(getPriorAuthHeaderE);
        			getPriorAuthHeaderResponse = getPriorAuthHeaderResponseE.getGetPriorAuthHeaderResponse();
    			}
    			    			 
				if (getPriorAuthHeaderResponse.get_return().getPriorAuthHeaderDTO() != null) {
					PriorAuthHeaderServiceStub.PriorAuthHeaderDTO[] priorAuthHeaderDTO = getPriorAuthHeaderResponse.get_return().getPriorAuthHeaderDTO();
					
					String submissionStatusCode = priorAuthHeaderDTO[0].getPriorAuthSubmissionStatusCode();
					if (submissionStatusCode.equals("35") || submissionStatusCode.equals("70") || submissionStatusCode.equals("80")) {

				        if (priorAuthHeaderDTO[0].getAppMaintDatetime() != null && !priorAuthHeaderDTO[0].getAppMaintDatetime().equals("")) {
				
				        	// Use the IE date format
			            	usedDate = QueueUtil.getQueueUtil().getAgingDateFromString(priorAuthHeaderDTO[0].getAppMaintDatetime(), "IE");
			            	usedTime = QueueUtil.getQueueUtil().getAgingTimeFromString(priorAuthHeaderDTO[0].getAppMaintDatetime());

				        }
					}
				}
            }
				
			if ((usedDate == null) || (usedTime == null)) {

            	SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
    			Date date = new Date();
    			usedDate = dateFormat.format(date);
    			SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mm a ");
    			Date time = new Date();
    			usedTime = timeFormat.format(time);
            }
			
        	// Put the date and time in the returned string
			returnVal = usedDate + " " + usedTime;
		} catch (AxisFault e) {
			log.error("AxisFault Exception in getLetterSentDateAndTime method",e);

        } catch (RemoteException e) {
        	log.error("RemoteException in getLetterSentDateAndTime method",e);

		} catch (DAOExceptionException e) {
			log.error("DAOExceptionException in getLetterSentDateAndTime method",e);

        }

		log.info("letter sent date/time: " + returnVal);

		return returnVal;
	}

	
	/**
	 * Calculate Authorization Age with both Saved Date and Time and the date the notification letter was sent or the Current Date and Time.
	 */
	public String getAuthorizationAge(String startDateTime, Long authorizationKey) { 
		int diffDays = 0;
		int diffHours = 0;
		int diffMins = 0;
		String age = null;  // days : hours : minutes;
		String format = "MM/dd/yyyy hh:mm a";

		try {

			List<Date> holidays = new ArrayList<Date>();

			// Get the holidays from common.properties
			String liferayHome = System.getProperty("LIFERAY_HOME");
			String propertyLocation = liferayHome +StringPool.SLASH + QueueSearchConstants.PROPERTIES_FILE;
		    log.info("getAuthorizationAge for PA Queue::propertyLocation: " + propertyLocation);
			Properties prop = new Properties();
			InputStream input = new FileInputStream(propertyLocation);
			prop.load(input);
			String holidayStr = prop.getProperty("AvalonHolidayCalendar");

			if (holidayStr != null) {

				// Split holidays into individual days
				String[] datesStr = holidayStr.split("\\s+");
				for (int cnt = 0; cnt < datesStr.length; cnt++) {
					Date nextDate = new SimpleDateFormat("MM/dd/yyyy").parse(datesStr[cnt]);
					holidays.add(nextDate);
				}
			}
			
			// Get the start time
			startDateTime = AgeCalculation.getAgeCalculation().getAuthRequestDateAndTime(startDateTime, authorizationKey);
			log.info("Age Start Date/Time Used : " + startDateTime);

			// Get the end time
			String endDateTime = AgeCalculation.getAgeCalculation().getLetterSentDateAndTime(authorizationKey);
			log.info("Age End Date/Time Used : " + endDateTime);

			SimpleDateFormat sdf = new SimpleDateFormat(format);
			Date startDate = sdf.parse(startDateTime);
			Date endDate = sdf.parse(endDateTime);

			Calendar startCal = Calendar.getInstance();
			startCal.setTime(startDate);
			Calendar endCal = Calendar.getInstance();
			endCal.setTime(endDate);

			// Move the starting date to the start of the first working day
			Calendar holidayCal = (Calendar) startCal.clone();
			holidayCal.set(Calendar.HOUR_OF_DAY, 0);
			holidayCal.set(Calendar.MINUTE, 0);
			holidayCal.set(Calendar.SECOND, 0);
			holidayCal.set(Calendar.MILLISECOND, 0);
			boolean nonWorkingFlag = false;
			while ((startCal.compareTo(endCal) <= 0) &&
					(Calendar.SATURDAY == startCal.get(Calendar.DAY_OF_WEEK) || Calendar.SUNDAY == startCal.get(Calendar.DAY_OF_WEEK) || holidays.contains(holidayCal.getTime()))) {
				nonWorkingFlag = true;
				
				// Increment start date past all non-working days
				startCal.add(Calendar.DATE, 1);
				holidayCal.add(Calendar.DATE, 1);
			}
			if (nonWorkingFlag) {
				startCal.set(Calendar.HOUR_OF_DAY, 0);
				startCal.set(Calendar.MINUTE, 0);
				startCal.set(Calendar.SECOND, 0);
				startCal.set(Calendar.MILLISECOND, 0);
			}
			
			// Move the ending date to the end of the last working day
			holidayCal = (Calendar) endCal.clone();
			holidayCal.set(Calendar.HOUR_OF_DAY, 0);
			holidayCal.set(Calendar.MINUTE, 0);
			holidayCal.set(Calendar.SECOND, 0);
			holidayCal.set(Calendar.MILLISECOND, 0);
			nonWorkingFlag = false;
			while ((startCal.compareTo(endCal) <= 0) &&
					(Calendar.SATURDAY == endCal.get(Calendar.DAY_OF_WEEK) || Calendar.SUNDAY == endCal.get(Calendar.DAY_OF_WEEK) || holidays.contains(holidayCal.getTime()))) {
				nonWorkingFlag = true;
				
				// Move to the previous day
				endCal.add(Calendar.DATE, -1);
				holidayCal.add(Calendar.DATE, -1);
			}
			if (nonWorkingFlag) {
				endCal.add(Calendar.DATE, 1);
				endCal.set(Calendar.HOUR_OF_DAY, 0);
				endCal.set(Calendar.MINUTE, 0);
				endCal.set(Calendar.SECOND, 0);
				endCal.set(Calendar.MILLISECOND, 0);
			}
			
			// Set a holiday date to the start date with only month, day, and year for the holiday comparison
			holidayCal = (Calendar) startCal.clone();
			holidayCal.set(Calendar.HOUR_OF_DAY, 0);
			holidayCal.set(Calendar.MINUTE, 0);
			holidayCal.set(Calendar.SECOND, 0);
			holidayCal.set(Calendar.MILLISECOND, 0);

			// Get number of full work days between the two dates
			boolean dayAddedFlag = false;
			while (startCal.compareTo(endCal) <= 0) {

				// Determine if this is a valid working day
				if (Calendar.SATURDAY != startCal.get(Calendar.DAY_OF_WEEK) && Calendar.SUNDAY != startCal.get(Calendar.DAY_OF_WEEK) && !holidays.contains(holidayCal.getTime())) {
			    	diffDays++;
			    	dayAddedFlag = true;
			    }
			    startCal.add(Calendar.DATE, 1);
			    holidayCal.add(Calendar.DATE, 1);

			    // If the start date has been moved past the end date, move the start date back a day, remove any day added, and exit the loop
				if (startCal.compareTo(endCal) > 0) {
					startCal.add(Calendar.DATE, -1);
					if (dayAddedFlag) {
						diffDays--;
					}
					break;
				}
				
				// Reset the day added flag for the next working day
				if (Calendar.SATURDAY != startCal.get(Calendar.DAY_OF_WEEK) && Calendar.SUNDAY != startCal.get(Calendar.DAY_OF_WEEK) && !holidays.contains(holidayCal.getTime())) {
					dayAddedFlag = false;
				}
			}

			if (startCal.compareTo(endCal) >= 0) {
				diffHours = 0;
				diffMins = 0;
			} else {
				
				// Get number of hours and minutes in the last day
				diffHours = (int) TimeUnit.MILLISECONDS.toHours(endCal.getTimeInMillis() - startCal.getTimeInMillis());
				
				// Determine if adding time for the missing time on Friday or a holiday moveed past a day
				if (diffHours == 24) {
					diffDays++;
					diffHours = 0;
				}
				diffMins = ((int) TimeUnit.MILLISECONDS.toMinutes(endCal.getTimeInMillis() - startCal.getTimeInMillis())) % 60;
			}
		}
		catch (Exception e) {
			log.error("Exception in getAuthorizationAge method",e);
		}
		
		age = String.valueOf(diffDays) + ":" + String.valueOf(diffHours) + ":" + String.valueOf(diffMins);
		return age;
	}
	
}
