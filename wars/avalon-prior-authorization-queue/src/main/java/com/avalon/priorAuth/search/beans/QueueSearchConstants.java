package com.avalon.priorAuth.search.beans;

/**
 * Description
 *		This file contain the Prior Authorization Queue constants.
 *
 * CHANGE History
 *		Version 1.0
 *			Initial version.
 *		Version 1.1				02/07/2018
 *     		Changed Reason for Inquiry to P.
 *  	Version 1.2				03/08/2017
 *  		Added a comstant for the properties file.
 *
 */

public interface QueueSearchConstants {
	public static final String PROPERTIES_FILE ="lbm-common.properties";

    public static final String SYSTEMADMINERROR = "Error while processing the request. Please contact system administrator";
    public static final String NODATAERROR = "No Authorizations were found that match your search criteria. Please update the search and try again.";
	public static final String ERROR = "error";

	// Authorization search types
	public static final String AUTH_ALL = "All";
	public static final String AUTH_PA  = "PA";
	public static final String AUTH_PSR = "PSR";
	public static final String AUTH_PA_DESC  = "Prior Authorization";
	public static final String AUTH_PSR_DESC = "Post Service Review";

	// Search Types and column names
	public static final String AUTH_NUMBER = "authNumber";
	public static final String CARD_NUMBER = "idCardNumber";
	public static final String HEALTH_PLAN_LOB = "healthPlanLob";
	public static final String SUBMISSION_STATUS = "submissionStatus";
	public static final String PRIORITY_LEVEL = "priorityLeval";
	public static final String AUTH_STATUS = "authStatus";
	public static final String DATE_RECEIVED = "dateReceived";
	public static final String AUTH_AGE = "authAge";
	public static final String DUE_DATE = "dueDate";
	public static final String WORKED_BY = "workedBy";

	// Member search constants
    public static final String REASON_FOR_INQUIRY = "P"; 
    public static final String HEALTH_PLAN_ID = "0010";
    public static final String HEALTH_PLAN_ID_TYPE = "PI";
    
    // Authorization search types
    public static final String INPROCESSSEARCH = "inProcessRecords";
    public static final String COMPLETEDSEARCH = "completedRecords";
    public static final String ALLSEARCH = "allRecords";
     
    // Queue search types
    public static final String QUEUE = "queue ";
    public static final String INPROCESSRECORDS = "queueInProcess";
    public static final String COMPLETEDRECORDS = "queueCompleted";
    public static final String ALLRECORDS = "queueAll";
    
    //TODO: Remove below code
    public static final String ENABLED_MOCK_SERVICE = "enabled.mock.service";
}
