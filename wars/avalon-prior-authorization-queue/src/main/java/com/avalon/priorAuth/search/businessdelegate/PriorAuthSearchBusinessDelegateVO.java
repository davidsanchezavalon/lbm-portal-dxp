package com.avalon.priorAuth.search.businessdelegate;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.rmi.RemoteException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.axis2.AxisFault;

/**
 * Description
 *		The methods in this file are called by the controller for the Prior Authorization Queue portlet.
 *
 * CHANGE History
 * 		Version 1.0
 *			inital version.
 * 		Version 1.1  
 *			Set the passed in user as the AppCreateUserId in the search method.  This is used in the access log message.
 *
 */

import com.avalon.esb.servicehelpers.PriorAuthImplServiceStub;
import com.avalon.esb.servicehelpers.PriorAuthImplServiceStub.Address;
import com.avalon.esb.servicehelpers.PriorAuthImplServiceStub.BusinessSectorSegment;
import com.avalon.esb.servicehelpers.PriorAuthImplServiceStub.ContactData;
import com.avalon.esb.servicehelpers.PriorAuthImplServiceStub.Coverage;
import com.avalon.esb.servicehelpers.PriorAuthImplServiceStub.CoverageList;
import com.avalon.esb.servicehelpers.PriorAuthImplServiceStub.GetMemberData;
import com.avalon.esb.servicehelpers.PriorAuthImplServiceStub.GetMemberDataResponse;
import com.avalon.esb.servicehelpers.PriorAuthImplServiceStub.Member;
import com.avalon.esb.servicehelpers.PriorAuthImplServiceStub.MemberDataRequest;
import com.avalon.esb.servicehelpers.PriorAuthImplServiceStub.MemberDataResponse;
import com.avalon.esb.servicehelpers.PriorAuthImplServiceStub.MemberDemographicsOutput;
import com.avalon.esb.servicehelpers.PriorAuthImplServiceStub.MemberList;
import com.avalon.esb.servicehelpers.PriorAuthImplServiceStub.MoreDataOptions;
import com.avalon.esb.servicehelpers.PriorAuthImplServiceStub.SearchCriteria;
import com.avalon.lbm.services.DAOExceptionException;
import com.avalon.lbm.services.PriorAuthHeaderServiceStub;
import com.avalon.lbm.services.PriorAuthHeaderServiceStub.PriorAuthHeader;
import com.avalon.lbm.services.PriorAuthHeaderServiceStub.PriorAuthHeaderDTO;
import com.avalon.lbm.services.PriorAuthHeaderServiceStub.PriorAuthorizationSearch;
import com.avalon.lbm.services.PriorAuthHeaderServiceStub.PriorAuthorizationSearchE;
import com.avalon.lbm.services.PriorAuthHeaderServiceStub.PriorAuthorizationSearchResponse;
import com.avalon.lbm.services.PriorAuthHeaderServiceStub.PriorAuthorizationSearchResponseE;
import com.avalon.priorAuth.search.beans.MemberDetailsFO;
import com.avalon.priorAuth.search.beans.QueueSearchConstants;
import com.avalon.priorAuth.search.beans.QueueSearchFO;
import com.avalon.priorAuth.search.customexception.CustomException;
import com.avalon.priorAuth.search.utils.QueueUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;


public class PriorAuthSearchBusinessDelegateVO implements QueueSearchConstants {

    private static PriorAuthSearchBusinessDelegateVO priorAuthSearchBusinessDelegateVO = null;
   
    private static final Log log = LogFactoryUtil.getLog(PriorAuthSearchBusinessDelegateVO.class.getName());
    
    private static Properties properties = null;
    private PriorAuthSearchBusinessDelegateVO() {
    }

    static {

		if (properties == null) {
		    properties = new Properties();
		    priorAuthSearchBusinessDelegateVO = new PriorAuthSearchBusinessDelegateVO();
		    String responseCodePropertyPath = System.getProperty("LIFERAY_HOME");
		    String propertyLocation = responseCodePropertyPath + "/PAMessages_en_US.properties";
		    log.info("responseCodePropertyPath: " + responseCodePropertyPath);
			try {
			    properties.load(new FileInputStream(new File(propertyLocation)));
			} catch (FileNotFoundException e) {
				log.error("FileNotFoundException in static block",e);
			} catch (IOException e) {
				log.error("IOException in static block",e);
			}
	    }
	}
    
    
    public static PriorAuthSearchBusinessDelegateVO getPriorAuthSearchBusinessDelegateVO() {
    	return priorAuthSearchBusinessDelegateVO;
    }


    /**
     * 
     * @param searchType
     * @param inprocessView
     * @param queueSearchFO
     * @param currentUser
     * @return
     * Find the Prior Auths that match the search type.  If the prior auths are available it fetches the results returning the results in the form of map object to the controller
     */
    public List findPriorAuths(String searchType, String inprocessView, String authType, QueueSearchFO queueSearchFO) {

    	log.info("PriorAuthSearchBusinessDelegateVO start findPriorAuths");
		List queueList = new ArrayList();

		try{
		
			// Creating the stub object to invoke the queue search service
	    	PriorAuthHeaderServiceStub priorAuthHeaderServiceStub = new PriorAuthHeaderServiceStub();
	        PriorAuthorizationSearchE priorAuthorizationSearchE = new PriorAuthorizationSearchE();
	
	        PriorAuthorizationSearch priorAuthorizationSearch = new PriorAuthorizationSearch();
	        PriorAuthHeader priorAuthHeader = new PriorAuthHeader();
	        PriorAuthHeaderDTO[] priorAuthHeaderDTO = new PriorAuthHeaderDTO[1];
	        priorAuthHeaderDTO[0] = new PriorAuthHeaderDTO();
	        
	        if (inprocessView.equals(QueueSearchConstants.INPROCESSSEARCH)) {
	        	
	            // Set the operation type for the prior auth queue (in process records)
	        	priorAuthHeader.setOperationType(QueueSearchConstants.INPROCESSRECORDS);
	        } else if (inprocessView.equals(QueueSearchConstants.COMPLETEDSEARCH)) {
		        	
		            // Set the operation type for the prior auth queue (completed records)
		        	priorAuthHeader.setOperationType(QueueSearchConstants.COMPLETEDRECORDS);
	        } else {
	        	
	            // Set the operation type for the prior auth search (all records)
	        	priorAuthHeader.setOperationType(QueueSearchConstants.ALLRECORDS);
	        }

        	
        	// Set the auth type
        	priorAuthHeader.setAuthType(authType);
        	log.info("  Search for auth type : " + authType);

	        if (searchType.equals(QueueSearchConstants.AUTH_NUMBER)) {
	        	priorAuthHeaderDTO[0].setPriorAuthNumber(queueSearchFO.getAuthorizationNumber());
				log.info("  Search Authorization Number : " + queueSearchFO.getAuthorizationNumber());
	        } else if (searchType.equals(QueueSearchConstants.WORKED_BY)) {
	        	priorAuthHeaderDTO[0].setPriorAuthWorkedBy(queueSearchFO.getWorkedBy());
				log.info("  Search Worked By : " + queueSearchFO.getWorkedBy());
	        } else if (searchType.equals(QueueSearchConstants.PRIORITY_LEVEL)) {
	        	priorAuthHeaderDTO[0].setPriorAuthPriorityCode(queueSearchFO.getPriorityLevel());
				log.info("  Search Priority Level : " + queueSearchFO.getPriorityLevel());
	        } else if (searchType.equals(QueueSearchConstants.DATE_RECEIVED)) {
	        	priorAuthHeaderDTO[0].setPriorAuthContactDatetime(queueSearchFO.getDateReceived());
				log.info("  Search Date Received : " + queueSearchFO.getDateReceived());
	        } else if (searchType.equals(QueueSearchConstants.CARD_NUMBER)) {
	        	priorAuthHeaderDTO[0].setIdCardNumber(queueSearchFO.getMemberId());
				log.info("  Search ID Card Number : " + queueSearchFO.getMemberId());
	        } else {
	        	log.warn("  Search for in process records");
	        }

	        priorAuthHeader.setPriorAuthHeaderDTO(priorAuthHeaderDTO);
	        priorAuthorizationSearch.setPriorAuthorizationSearch(priorAuthHeader);
	
	        priorAuthorizationSearchE.setPriorAuthorizationSearch(priorAuthorizationSearch);
	        PriorAuthorizationSearchResponseE priorAuthorizationSearchResponseE;
	
	        PriorAuthorizationSearchResponse priorAuthorizationSearchResponse;
	
	        //TODO: Remove mock service code and condition. Don't remove else condition code
			if(GetterUtil.getBoolean(com.liferay.portal.kernel.util.PropsUtil.get(ENABLED_MOCK_SERVICE))){
				 //Success Mock Code
				priorAuthorizationSearchResponse = priorAuthorizationSearchResponseSuccessMock();
			}else{
				// Invoke the service
		        priorAuthorizationSearchResponseE = priorAuthHeaderServiceStub.priorAuthorizationSearch(priorAuthorizationSearchE);
		        priorAuthorizationSearchResponse = priorAuthorizationSearchResponseE.getPriorAuthorizationSearchResponse();
			}
	        
	        // Fetching the data from the response object
	        PriorAuthHeaderDTO[] priorAuthHeaderDTOResponse = priorAuthorizationSearchResponse.get_return().getPriorAuthHeaderDTO();
	        if (priorAuthHeaderDTOResponse == null) {
	    		Map <String, Object> queueSearch = new HashMap<String, Object>();
				String errorMsg = NODATAERROR;

				queueSearch.put(ERROR, errorMsg);
				queueList.add(queueSearch);
	        } else {
		        int resultSize = priorAuthHeaderDTOResponse.length;
				for(int paCount = 0; paCount < resultSize; paCount++) {
			        if ((priorAuthHeaderDTOResponse[0].getMemberNumber() != null) && !(priorAuthHeaderDTOResponse[0].getMemberNumber().equals(""))) {
			    		Map <String, Object> queueSearch = new HashMap<String, Object>();
	
						log.info("Prior Authorization " + paCount + " : ");
	
			            // Set the authorization number
						String priorAuthNumber = priorAuthHeaderDTOResponse[paCount].getPriorAuthNumber();
						if (priorAuthNumber == null) {
	
							// Datatables does not allow NULL values
							priorAuthNumber =  " ";
						}
			        	queueSearch.put(QueueSearchConstants.AUTH_NUMBER, priorAuthNumber);
						log.info("  Authorization Number : " + priorAuthNumber);
			
			            // Set the authorization key
						int authorizationKeyResponse = priorAuthHeaderDTOResponse[paCount].getPriorAuthHeaderKey();
						log.info("  Auth Header Key: " + priorAuthHeaderDTOResponse[paCount].getPriorAuthHeaderKey());
						
			            // Set the ID card number
						String idCardNumber = priorAuthHeaderDTOResponse[paCount].getIdCardNumber().trim();
						if (idCardNumber == null) {
							
							// Datatables does not allow NULL values
							idCardNumber =  " ";
						}
			        	queueSearch.put(QueueSearchConstants.CARD_NUMBER, idCardNumber);
						log.info("  ID Card Number : " + idCardNumber);

		                // Set the Health Plan LOB
						String lob = priorAuthHeaderDTOResponse[paCount].getPriorAuthBusinessSegmentDescription();
						queueSearch.put(QueueSearchConstants.HEALTH_PLAN_LOB, lob);
						log.info("  Health Plan Lob : " + lob);
		
		                // Set the Submission Status
						String submissionStatus = "Saved";
						String submissionStatusCode = priorAuthHeaderDTOResponse[paCount].getPriorAuthSubmissionStatusCode();
		                if (submissionStatusCode != null) {
		                    if (submissionStatusCode.equalsIgnoreCase("10")) {
		                        submissionStatus = "Saved";
		                    } else if (submissionStatusCode.equalsIgnoreCase("11")) {
		                        submissionStatus = "Updated";
		                    } else if (submissionStatusCode.equalsIgnoreCase("40")) {
		                        submissionStatus = "Submitted";
		                    } else if (submissionStatusCode.equalsIgnoreCase("41")) {
		                        submissionStatus = "Updates Submitted";
		                    } else if (submissionStatusCode.equalsIgnoreCase("50")) {
		                        submissionStatus = "Sent";
		                    } else if (submissionStatusCode.equalsIgnoreCase("35")) {
		                        submissionStatus = "Void";
		                    } else if (submissionStatusCode.equalsIgnoreCase("70")) {
		                        submissionStatus = "Void HP - Submitted";
		                    } else if (submissionStatusCode.equalsIgnoreCase("80")) {
		                        submissionStatus = "Void HP";
		                    }
		                }
						queueSearch.put(QueueSearchConstants.SUBMISSION_STATUS, submissionStatus);
						log.info("  Submission Status : " + submissionStatus);
						
		                // Set the Priority Leval
						String priorityCode = priorAuthHeaderDTOResponse[paCount].getPriorAuthPriorityCode();
						if (priorityCode == null) {
	
							// Datatables does not allow NULL values
							priorityCode =  " ";
						}
			            queueSearch.put(QueueSearchConstants.PRIORITY_LEVEL, priorityCode);
						log.info("  Priority Level : " + priorityCode);
	
						// Set the Authorization Status
						String priorAuthStatus = priorAuthHeaderDTOResponse[paCount].getPriorAuthStatusDesc();
						if (priorAuthStatus == null) {
	
							// Datatables does not allow NULL values
							priorAuthStatus =  " ";
						}
						queueSearch.put(QueueSearchConstants.AUTH_STATUS, priorAuthStatus);
						log.info("  Authorization Status : " + priorAuthStatus);
	
						// Set the Date Received to the date from Contact date/time
						String contactDatetime = priorAuthHeaderDTOResponse[paCount].getPriorAuthContactDatetime();
						if (contactDatetime == null) {
	
							// Datatables does not allow NULL values
							contactDatetime =  " ";
						} else {
							contactDatetime = contactDatetime.substring(0, Math.min(contactDatetime.length(), "mm/dd/yyyy".length()));
						}
						queueSearch.put(QueueSearchConstants.DATE_RECEIVED, contactDatetime);
						log.info("  Date Received : " + contactDatetime);
	
			            // Set the Authorization Age
			            String appDate = priorAuthHeaderDTOResponse[paCount].getAppCreateDatetime();
			            Long authorizationKey = new Long(authorizationKeyResponse);
						String authorizationAge = QueueUtil.getQueueUtil().setAuthorizationAgeQueue(appDate, authorizationKey);
						queueSearch.put(QueueSearchConstants.AUTH_AGE, authorizationAge);
						log.info("  Authorization Age : "+ authorizationAge);
	
						// Set the Due Date and Time
						String dueDatetime = priorAuthHeaderDTOResponse[paCount].getPriorAuthDueDatetime();
						if (dueDatetime == null) {
	
							// Datatables does not allow NULL values
							dueDatetime =  " ";
						}
						queueSearch.put(QueueSearchConstants.DUE_DATE, dueDatetime);
						log.info("  Due Date and Time : " + dueDatetime);
	
						// Set the Worked By
						String workedBy = priorAuthHeaderDTOResponse[paCount].getPriorAuthWorkedBy();
						if (workedBy == null) {
	
							// Datatables does not allow NULL values
							workedBy =  " ";
						}
						queueSearch.put(QueueSearchConstants.WORKED_BY, workedBy);
						log.info("  Worked By : " + workedBy);

						queueList.add(queueSearch);
			        }
				}
	        }
		} catch (AxisFault ex) {
    		Map <String, Object> queueSearch = new HashMap<String, Object>();
			String errorMsg = ex.getMessage();

			queueSearch.put(ERROR, errorMsg);
			queueList.add(queueSearch);
		} catch (DAOExceptionException ex) {
    		Map <String, Object> queueSearch = new HashMap<String, Object>();
			String errorMsg = ex.getMessage();

			if (errorMsg.contains("Index: 0, Size: 0")) {
				errorMsg = NODATAERROR;
			}
			queueSearch.put(ERROR, errorMsg);
			queueList.add(queueSearch);
		} catch (RemoteException ex) {
    		Map <String, Object> queueSearch = new HashMap<String, Object>();
			String errorMsg = ex.getMessage();

			queueSearch.put(ERROR, errorMsg);
			queueList.add(queueSearch);
		}

    	log.info("PriorAuthSearchBusinessDelegateVO end findPriorAuths");
		return queueList;
    }

    
    /**
     * 
     * @param idCardNumber
     * @param authorizationNumber
     * @return
     * @throws IOException
     * @throws NullPointerException
     * @throws CustomException
     * Find member service will be invoked based on the memberId. If the member is available it fetches the results in the form of map object to the controller.
     */
   public Map<Integer, MemberDetailsFO> findMember(String idCardNumber, String authorizationNumber)
           throws IOException, NullPointerException, CustomException {

    	log.info("PriorAuthSearchBusinessDelegateVO start findMember");
		Map<Integer, MemberDetailsFO> mapList = new HashMap<Integer, MemberDetailsFO>();
		try {
				
			// Creating the member service stub object
			PriorAuthImplServiceStub priorAuthImplServiceStub = new PriorAuthImplServiceStub();
			GetMemberData getMemberData = new GetMemberData();
			MemberDataRequest memberDataRequest = new MemberDataRequest();
			SearchCriteria searchCriteria = new SearchCriteria();
			searchCriteria.setReasonForInquiry(QueueSearchConstants.REASON_FOR_INQUIRY); // C
			searchCriteria.setRequestDate(null);
			searchCriteria.setRpn(" ");
			searchCriteria.setProductCode(" ");
			searchCriteria.setPlanCode(" ");
			searchCriteria.setHealthPlanId(QueueSearchConstants.HEALTH_PLAN_ID); // 0010
			searchCriteria.setHealthPlanGroupId(" ");
			searchCriteria.setIdCardNumber(idCardNumber);
			searchCriteria.setHealthPlanIdType(QueueSearchConstants.HEALTH_PLAN_ID_TYPE); // PI
			searchCriteria.setSubscriberNumber(" ");
			memberDataRequest.setSearchCriteria(searchCriteria);    // Setting the required objects in the service request
			MoreDataOptions moreDataOptions = new MoreDataOptions();
			moreDataOptions.setRetrieveMemberList(true);
			moreDataOptions.setRetrieveCoverageData(false);
			memberDataRequest.setMoreDataOptions(moreDataOptions);
			getMemberData.setGetMemberData(memberDataRequest);
			log.info("Request Data : " + getMemberData.toString());
			
			GetMemberDataResponse getMemberDataResponse = null;
			//TODO: Remove mock service code and condition. Don't remove else condition code
			if(GetterUtil.getBoolean(com.liferay.portal.kernel.util.PropsUtil.get(ENABLED_MOCK_SERVICE))){
				 //Success Mock Code
				 getMemberDataResponse = getSuccessMockMemberData(getMemberData);
			}else{
				 getMemberDataResponse = priorAuthImplServiceStub.getMemberData(getMemberData);
			}
			
			boolean searchMemberFlag = true;
			String[] responseCodes = getMemberDataResponse.getGetMemberDataResponse().getResponseCode();
			for(String responseCode:responseCodes){
				if (responseCode.matches("000|952|953|954|955|956|957|958|959|960|961|962|963|964|990|991|992|993|994|995|996")) {
					if (searchMemberFlag) {
						
						// Set the flag to indicate the member search has been done
						searchMemberFlag = false;

						mapList = processMemberDataResponse(getMemberDataResponse, authorizationNumber);
						String coverageList = getMemberDataResponse.getGetMemberDataResponse().getCoverageList().getCoverageCount();
						log.info("coverageListCount: " + coverageList);
						Coverage[] coverage = getMemberDataResponse.getGetMemberDataResponse().getCoverageList().getCoverage();
						for (int j = 0; j < coverage.length; j++) {
							log.info("HealthPlanGroupId: "+ coverage[j].getHealthPlanGroupId());
							log.info("Coverage Begin date: "+ coverage[j].getCoverageBeginDate());
							log.info("Client Number: " + coverage[j].getClientNumber());
							log.info("getCesGroupNumber: "+ coverage[j].getCesGroupNumber());
							log.info("getCesGroupNumberDescription: "+ coverage[j].getCesGroupNumberDescription());
							log.info("getAmmsGroupNumber: "+ coverage[j].getAmmsGroupNumber());
							log.info("getBusinessSectorCd: "+ coverage[j].getBusinessSectorSegment().getBusinessSectorCd());
							log.info("getBusinessSectorDesc: "+ coverage[j].getBusinessSectorSegment().getBusinessSectorDesc());
							log.info("getBusinessSegmentCd: "+ coverage[j].getBusinessSectorSegment().getBusinessSegmentCd());
							log.info("getBusinessSegmentDesc: "+ coverage[j].getBusinessSectorSegment().getBusinessSegmentDesc());
						}
						log.info("PriorAuthBussinessDelegateVo End findMember");
					}
				} else {
											
					// Set the flag to indicate the a error that stops the member search
					searchMemberFlag = false;

					String errorMessage = getResponseCodeMessages(responseCodes);
					throw new CustomException(errorMessage);
				}
			}
		}
		// Exception thrown when general network I/O error occurs
		catch (IOException ioe) {
			log.info("Check: " + (ioe instanceof org.apache.axis2.AxisFault));
			log.info("Network I/O error - " + ioe);
			if (ioe instanceof org.apache.axis2.AxisFault) {
			org.apache.axis2.AxisFault exception = (org.apache.axis2.AxisFault) ioe;
			if (exception.getMessage().equals("Read timed out")) {
				log.info("IOException Timed Out");

				// Throwing the exception to the controller when the socket time out exception has been occured
				throw new IOException("Your Request is Timed Out");
			} else {
				log.info("IOException No Records");
				
				// Throwing the exception to the controller when the no records exception has been occured
				throw new IOException("No Records");
			}

			} else {
			log.info("IOException else block");
			throw new IOException("Your Request is Timed Out");
			}
		} catch (CustomException exception) {
			log.info("MemberSearchBussinessDelegateVo Response Code Exception " + exception.getMessage());
			throw exception;
		} catch (Exception e) {
			log.error("Exception in findMember method",e);
		}
		return mapList;
    }

    
    /**
     * 
     * @param getMemberDataResponse
     * @param idCardNumber
     * @param authorizationNumber
     * @param firstName
     * @param lastName
     * @param dob
     * @param authSearch
     * @return 
     * @throws AxisFault 
     * @throws DAOExceptionException 
     * @throws ParseException 
     * @throws RemoteException 
     * Process the member data response from the database and return the member list.
     */
    private Map<Integer, MemberDetailsFO> processMemberDataResponse(GetMemberDataResponse getMemberDataResponse, String searchNumber)
    		throws AxisFault, DAOExceptionException, ParseException, RemoteException {
		Map<Integer, MemberDetailsFO> mapList = new HashMap<Integer, MemberDetailsFO>();
		String memberId = null;
		
		// Get the addition member data (there is only one of these)
		String addressLine1 = getMemberDataResponse.getGetMemberDataResponse().getAddress().getAddressLine1();
		String addressLine2 = getMemberDataResponse.getGetMemberDataResponse().getAddress().getAddressLine2();
		String city = getMemberDataResponse.getGetMemberDataResponse().getAddress().getCity();
		String state = getMemberDataResponse.getGetMemberDataResponse().getAddress().getStateCode();
		String zipCode = getMemberDataResponse.getGetMemberDataResponse().getAddress().getZipCode();
		String phoneNumber = getMemberDataResponse.getGetMemberDataResponse().getContactData().getPhoneNumber();

		log.info("Health Plan Group Id : "+ getMemberDataResponse.getGetMemberDataResponse().getHealthPlanGroupId());
		log.info("Lab Benefit Id : " + getMemberDataResponse.getGetMemberDataResponse().getLabBenefitId());
		log.info("Lab Benefit Id Description : "+ getMemberDataResponse.getGetMemberDataResponse().getLabBenefitIdDescription());
		log.info("Subscriber Number : "+ getMemberDataResponse.getGetMemberDataResponse().getSubscriberNumber());
		log.info("First Name : "+ getMemberDataResponse.getGetMemberDataResponse().getMemberDemographicsOutput().getFirstName());
		log.info("Last Name : "+ getMemberDataResponse.getGetMemberDataResponse().getMemberDemographicsOutput().getLastName());
		log.info("Middle Name : "+ getMemberDataResponse.getGetMemberDataResponse().getMemberDemographicsOutput().getMiddleName());
		log.info("Gender Code: "+ getMemberDataResponse.getGetMemberDataResponse().getMemberDemographicsOutput().getGenderCode());
		log.info("Member Id : "+ getMemberDataResponse.getGetMemberDataResponse().getMemberDemographicsOutput().getMemberId());
		log.info("Date Of Birth: "+ getMemberDataResponse.getGetMemberDataResponse().getMemberDemographicsOutput().getDateOfBirth());
		log.info("Address Line 1: " + addressLine1);
		log.info("Address Line 2: " + addressLine2);
		log.info("City: " + city);
		log.info("State: " + state);
		log.info("Zip Code: " + zipCode);
		log.info("Phone Number: " + phoneNumber);
		String memberListCount = getMemberDataResponse.getGetMemberDataResponse().getMemberList().getMemberListCount();
		log.info("memeberListCount: " + memberListCount);
		Member[] member = getMemberDataResponse.getGetMemberDataResponse().getMemberList().getMember();

		int memberIdx = 0;
		for (int i = 0; i < member.length; i++) {
			
			memberId = member[i].getMemberId();
			log.info("Member Number " + memberId + " for " + member[i].getFirstName() + " " + member[i].getLastName());

			// Get the data from the Prior Auth Header table
        	// Creating the stub object to invoke the authorization search service
			PriorAuthHeaderServiceStub priorAuthHeaderServiceStub = new PriorAuthHeaderServiceStub();
            PriorAuthorizationSearchE priorAuthorizationSearchE = new PriorAuthorizationSearchE();
            PriorAuthorizationSearch priorAuthorizationSearch = new PriorAuthorizationSearch();

            PriorAuthHeader priorAuthHeader = new PriorAuthHeader();

            // Set the operation type for the prior auth search
            priorAuthHeader.setOperationType(QueueSearchConstants.ALLRECORDS);
            
            PriorAuthHeaderDTO[] priorAuthHeaderDTO = new PriorAuthHeaderDTO[1];
            priorAuthHeaderDTO[0] = new PriorAuthHeaderDTO();

			priorAuthHeaderDTO[0].setPriorAuthNumber(searchNumber);
			priorAuthHeaderDTO[0].setIdCardNumber("0");
			priorAuthHeaderDTO[0].setExtractPerformedQuantity((short)0);
			priorAuthHeaderDTO[0].setPriorAuthHeaderKey(0);

            priorAuthHeader.setPriorAuthHeaderDTO(priorAuthHeaderDTO);
            priorAuthorizationSearch.setPriorAuthorizationSearch(priorAuthHeader);
            priorAuthorizationSearchE.setPriorAuthorizationSearch(priorAuthorizationSearch);
            PriorAuthorizationSearchResponseE priorAuthorizationSearchResponseE;

            PriorAuthorizationSearchResponse priorAuthorizationSearchResponse =null;
           
          //TODO: Remove mock service code and condition. Don't remove else condition code
			if(GetterUtil.getBoolean(com.liferay.portal.kernel.util.PropsUtil.get(ENABLED_MOCK_SERVICE))){
				 //Success Mock Code
			    priorAuthorizationSearchResponseE = new PriorAuthorizationSearchResponseE();
	            priorAuthorizationSearchResponse =new PriorAuthorizationSearchResponse();
	            priorAuthorizationSearchResponse.set_return(priorAuthHeader);
	            priorAuthorizationSearchResponseE.setPriorAuthorizationSearchResponse(priorAuthorizationSearchResponse);
			}else{
				 // Invoke the service
	            priorAuthorizationSearchResponseE = priorAuthHeaderServiceStub.priorAuthorizationSearch(priorAuthorizationSearchE);
	            priorAuthorizationSearchResponse = priorAuthorizationSearchResponseE.getPriorAuthorizationSearchResponse();
			}
            
            // Fetching the data from the response object
            PriorAuthHeaderDTO[] priorAuthHeaderDTOResponse = priorAuthorizationSearchResponse.get_return().getPriorAuthHeaderDTO();
			
			// Get the LOB data
			String businessSectorCode = getMemberDataResponse.getGetMemberDataResponse().getBusinessSectorSegment().getBusinessSectorCd();
			String businessSectorDescription = getMemberDataResponse.getGetMemberDataResponse().getBusinessSectorSegment().getBusinessSectorDesc();
			String businessSegmentCode = getMemberDataResponse.getGetMemberDataResponse().getBusinessSectorSegment().getBusinessSegmentCd();
			String businessSegmentDescription = getMemberDataResponse.getGetMemberDataResponse().getBusinessSectorSegment().getBusinessSegmentDesc();

			for (int j = 0; j < priorAuthHeaderDTOResponse.length; j++) {

				MemberDetailsFO members = new MemberDetailsFO();
				// Set the member data
				members.setSubscriberId(memberId);
				log.info("Subscriber Id: " + members.getSubscriberId());
				members.setMemberNumber(member[i].getMemberId());
				log.info("Health Plan Group Id: "+ member[i].getHealthPlanGroupId());
				members.setHealthPlan(member[i].getHealthPlanGroupId());
				log.info("First Name: " + member[i].getFirstName());
				members.setMemberFirstName(member[i].getFirstName());
				log.info("Middle Name: " + member[i].getMiddleName());
				members.setMemberMiddleName(member[i].getMiddleName());
				log.info("Last Name: " + member[i].getLastName());
				members.setMemberLastName(member[i].getLastName());
				log.info("Suffix Name: " + member[i].getSuffix());
				members.setMemberSuffixName(member[i].getSuffix());
				log.info("Date Of Birth: " + member[i].getDateOfBirth());
				members.setMemberDob(convertStringToDate(member[i].getDateOfBirth()));
				log.info("Gender Code: " + member[i].getGenderCode());
				members.setGender(member[i].getGenderCode());
				log.info("Patient Id: " + member[i].getPatientId());
				members.setPatientId(member[i].getPatientId()); // patient Id
				log.info("Unique Member Id: " + member[i].getUniqueMemberId());
				members.setMpi(member[i].getUniqueMemberId());
				log.info("Relationship Code Description: "+ member[i].getRelationshipCodeDescription());
				members.setMemberRelationship(member[i].getRelationshipCodeDescription());
				log.info("Member Group Id: "+ member[i].getHealthPlanGroupId());
				members.setGroupId(member[i].getHealthPlanGroupId());
				
				// Set the addition member data (there is only one of these)
				members.setMemberAddressLine1(addressLine1);
				log.info("Member Address Line1: "+ addressLine1);
				members.setMemberAddressLine2(addressLine2);
				log.info("Member Address Line2: "+ addressLine2);
				members.setMemberCity(city);
				log.info("Member City: "+ city);
				members.setMemberStateCode(state);
				log.info("Member State: "+ state);
				members.setMemberZipcode(zipCode);
				log.info("Member Zipcode: "+ zipCode);
				members.setMemberPhoneNumber(phoneNumber);
				log.info("Member Phone Number: "+ phoneNumber);
				
				members.setBusinessSectorCode(businessSectorCode);
				log.info("businessSectorCode: "+ businessSectorCode);
				members.setBusinessSectorDescription(businessSectorDescription);
				log.info("businessSectorDescription: "+ businessSectorDescription);
				members.setBusinessSegmentCode(businessSegmentCode);
				log.info("businessSegmentCode: "+ businessSegmentCode);
				members.setBusinessSegmentDescription(businessSegmentDescription);
				log.info("businessSegmentDescription: "+ businessSegmentDescription);

				if ((priorAuthHeaderDTOResponse[j].getMemberNumber() != null)&& !(priorAuthHeaderDTOResponse[j].getMemberNumber().equals(""))) {
	                
	                // Get the submission status code
					String authSubmissionStatusCode = priorAuthHeaderDTOResponse[j].getPriorAuthSubmissionStatusCode();
  	 	            log.info("Auth Submission Status Code :" + authSubmissionStatusCode);
  	 	            members.setAuthSubmissionStatusCode(authSubmissionStatusCode);
	                
	                // Get the authorization key response
  		            int authorizationKeyResponse = priorAuthHeaderDTOResponse[j].getPriorAuthHeaderKey();
  	 	            log.info("Authorization Key Response :" + authorizationKeyResponse);
  	 	            members.setAuthorizationKeyResponse(authorizationKeyResponse);

	                // Set the ID Card Number
	                String memberIdCardNumber = priorAuthHeaderDTOResponse[j].getIdCardNumber().trim();
	                log.info("Member Id Card Number :" + memberIdCardNumber);
	                members.setMemberId(memberIdCardNumber);
	                
	                // Set the member number
	                String memberNumber = priorAuthHeaderDTOResponse[j].getMemberNumber().trim();
	                log.info("Member Number :" + memberNumber);
	                members.setMemberNumber(memberNumber);
	                
	                // Set the prior auth number
	                String authNumber = priorAuthHeaderDTOResponse[j].getPriorAuthNumber();
	                log.info("Authorization Number : " + authNumber);
	                members.setPriorAuthNumber(authNumber);
	                
	                // Set the create date
	                String createDatetime = priorAuthHeaderDTOResponse[j].getAppCreateDatetime();
	                log.info("Creation date : " + createDatetime);
	                members.setAppCreateDatetime(createDatetime);
	                
	                // Set the create user ID
	                String createUser = priorAuthHeaderDTOResponse[j].getAppCreateUserId();
	                log.info("Creation user : " + createUser);
	                members.setAppCreateUserId(createUser);
	                
	                // Set the prior auth status code
	                String authStatus = priorAuthHeaderDTOResponse[j].getPriorAuthStatusCode();
	                log.info("Authorization Status : " + authStatus);
	                members.setPriorAuthStatusCode(authStatus);
	                
	                // Set the prior auth service begin service date (must be saved with procedure info to get this date)
	                String serviceBeginDate = convertDateStringFormat(priorAuthHeaderDTOResponse[j].getPriorAuthBeginServiceDate(), "dd-MM-yyyy", "MM/dd/yyyy");
	                log.info("Service Begin Date : " + serviceBeginDate);
	                members.setPriorAuthBeginServiceDate(serviceBeginDate);
	            }
				
				if ((memberId != null) && memberId.equals(members.getMemberNumber())) {

		            // Setting the member details data into details map object
					mapList.put(memberIdx, members);
					memberIdx++;
				}
			}
    	}
		
		return mapList;
    }

    
    /**
     * 
     * @param responseCodes
     * @param idCardNumber
     * @return To display error message based on the response code from the the cds response
     */
    
    private String getResponseCodeMessages(String responseCodes[]) {
		StringBuilder errorMessage = new StringBuilder();
		
		for(String responseCode:responseCodes){
			log.info("responseCode ---> " +responseCode);
			StringBuilder propertyValue = new StringBuilder();
			if (responseCode.matches("950|951")) {
				propertyValue.append("authSearch_");
				propertyValue.append(responseCode);
				log.info("propertyValue" +propertyValue);
				errorMessage.append(properties.getProperty(propertyValue.toString()));
				errorMessage.append("</br>");
				propertyValue = null;
			} else {
				log.info(responseCode + " is not a Valid Response Code");
			}
		}
		return errorMessage.toString();
    }

    
    /**
     * 
     * @param indate
     * @return
     * To get the date in MM/dd/yyyy format for findMember response object
     */
    private String convertStringToDate(Date indate) {
		String dateString = null; 
		SimpleDateFormat formatDate = new SimpleDateFormat("MM/dd/yyyy");

		try {
		    dateString = formatDate.format(indate);
		} catch (Exception ex) {
			log.error("Exception in convertStringToDate method",ex);
		}
		return dateString;
    }

    
    /**
     * 
     * @param dt
     * @param fromFmt
     * @param toFmt
     * @return 
     * Convert the date string to a different format.
     */
   private String convertDateStringFormat(String dt, String fromFmt, String toFmt) {
    	String returnDt = null;

    	if (dt == null) {
    		dt = "";
    	}

    	try {
    		if (!dt.equals("")) {
    			
    			// Return the input string if it is in the requested format
    			if (toFmt.equals("MM/dd/yyyy") && (dt.matches("^(1[0-2]|0[1-9])/(3[01]|[12][0-9]|0[1-9])/[0-9]{4}$"))) {
    				returnDt = dt;
    			} else if (toFmt.equals("yyyy-MM-dd") && (dt.matches("^[0-9]{4}-(1[0-2]|0[1-9])-(3[01]|[12][0-9]|0[1-9])$"))) {
    				returnDt = dt;
    			} else {
					SimpleDateFormat inputFormat = new SimpleDateFormat(fromFmt);
					SimpleDateFormat outputFormat = new SimpleDateFormat(toFmt);
					Date tmpDate = inputFormat.parse(dt);
					returnDt = outputFormat.format(tmpDate);
    			}
    		}
    	} catch (ParseException e) {
    		log.error("ParseException in convertDateStringFormat method",e);
		}
		
		return returnDt;
    }

   /**
    * TODO: Remove below success mock code  
    * @param getMemberData
    * @return
    */
   public GetMemberDataResponse getSuccessMockMemberData(GetMemberData getMemberData){
	   
	    GetMemberDataResponse getMemberDataResponse = new GetMemberDataResponse();
	    MemberDataResponse memberDataResponse = new MemberDataResponse();
	    CoverageList coverageList = new CoverageList();
	   
		BusinessSectorSegment businessSectorSegment = new BusinessSectorSegment();
    	businessSectorSegment.setBusinessSectorCd("SectorCd");
    	businessSectorSegment.setBusinessSectorDesc("SectorDesc");
    	businessSectorSegment.setBusinessSegmentCd("SegmentCd");
    	businessSectorSegment.setBusinessSegmentDesc("SegmentDesc");
    	
	    Coverage coverage = new Coverage();
   		coverage.setHealthPlanGroupId("HPGID1");
   		coverage.setCoverageBeginDate(new Date());
   		coverage.setClientNumber("CNo");
   		coverage.setCesGroupNumber("CESGrpNo");
   		coverage.setCesGroupNumberDescription("CGND");
   		coverage.setAmmsGroupNumber("AMGRPNo");
   		coverage.setBusinessSectorSegment(businessSectorSegment);
   		coverageList.setCoverage(new Coverage[] {coverage});
	    memberDataResponse.setCoverageList(coverageList);
	    memberDataResponse.setResponseCode(new String[] {"000"});
	    
	    Address address = new Address();
	    address.setAddressLine1("ADDLine1");
	    address.setAddressLine2("AddLine2");
	    address.setCity("Ahbd");
	    address.setStateCode("01");
	    address.setZipCode("322345");
	   
	    memberDataResponse.setAddress(address);
	    
	    ContactData contactData = new ContactData();
	    contactData.setPhoneNumber("23823239");
	    memberDataResponse.setContactData(contactData);
	    memberDataResponse.setBusinessSectorSegment(businessSectorSegment);
	    
	    memberDataResponse.setHealthPlanGroupId("HPG2");
	    memberDataResponse.setLabBenefitId("LABBID");
	    memberDataResponse.setLabBenefitIdDescription("LABDesc");
	    memberDataResponse.setSubscriberNumber("SUBNo");
	    
	    MemberDemographicsOutput memberDemographicsOutput = new MemberDemographicsOutput();
	    memberDemographicsOutput.setDateOfBirth(new Date());
	    memberDemographicsOutput.setFirstName("First");
	    memberDemographicsOutput.setGenderCode("01");
	    memberDemographicsOutput.setLastName("Last");
	   // memberDemographicsOutput.setMemberId(getMemberData.getGetMemberData().getSearchCriteria().getIdCardNumber());
	    memberDemographicsOutput.setMemberId("22");
	    memberDemographicsOutput.setMiddleName("Middle");
	    memberDemographicsOutput.setSuffix("SUF");
	    memberDataResponse.setMemberDemographicsOutput(memberDemographicsOutput);
	    
		MemberList memberList = new MemberList();
    	memberList.setMemberListCount("1");
    	Member member = new Member();
    	member.setMemberId("22");
    	member.setFirstName("MeMFirstName");
    	member.setDateOfBirth(new Date());
    	member.setHealthPlanGroupId("MemberHPGroupId");
    	member.setLastName("MemLastName");
    	member.setMiddleName("MemMiddleName");
    	member.setSuffix("TestMem");
    	member.setGenderCode("MGEC");
    	member.setPatientId("MPID");
    	member.setUniqueMemberId("UMID");
    	member.setRelationshipCodeDescription("RCD");
    	member.setBusinessSectorSegment(businessSectorSegment);
    	member.setDependentVerificationIndicator(true);
    	member.setGenderDescription("GDES");
    	member.setLabBenefitId("LID");
    	member.setLabBenefitIdDescription("LBID");
    	member.setPrivacyIndicator(false);
    
    	memberList.setMember(new Member[] {member});
    	memberDataResponse.setMemberList(memberList);
	    
	    getMemberDataResponse.setGetMemberDataResponse(memberDataResponse);
	    
	   return getMemberDataResponse;
   }
   
   /**
    * TODO: Remove below success mock code 
    * @return
    */
   private PriorAuthorizationSearchResponse priorAuthorizationSearchResponseSuccessMock(){
	   PriorAuthorizationSearchResponse priorAuthorizationSearchResponse = new PriorAuthorizationSearchResponse();
       PriorAuthHeaderDTO priorAuthHeaderDTOObj = new PriorAuthHeaderDTO();
		priorAuthHeaderDTOObj.setAppCreateDatetime("11/11/2018 11:11:11");
		priorAuthHeaderDTOObj.setPriorAuthSubmissionStatusCode("10");
		priorAuthHeaderDTOObj.setPriorAuthPatientFirstName("PatientFirstName");
		priorAuthHeaderDTOObj.setPriorAuthPatientLastName("PatientLastName");
		priorAuthHeaderDTOObj.setPriorAuthPatientBirthDate("1999-04-26 01:02:00");
		priorAuthHeaderDTOObj.setPriorAuthBusinessSegmentDescription("SegDesc");
		priorAuthHeaderDTOObj.setPriorAuthStatusDesc("StatusDesc");
		priorAuthHeaderDTOObj.setMemberNumber("22");
		priorAuthHeaderDTOObj.setPriorAuthWorkedBy("Test");
		priorAuthHeaderDTOObj.setPriorAuthPriorityCode("Low");
		priorAuthHeaderDTOObj.setAppCreateUserId("22");
		priorAuthHeaderDTOObj.setAuthInboundChannel("Channel");
		priorAuthHeaderDTOObj.setAuthNumberSearched("22");
		priorAuthHeaderDTOObj.setAuthRequestedDate("2018-04-26 01:02:00");
		priorAuthHeaderDTOObj.setExtractPerformedQuantity((short)1);
		priorAuthHeaderDTOObj.setHealthPlanGroupId("HPGID");
		priorAuthHeaderDTOObj.setHealthPlanId("HID");
		priorAuthHeaderDTOObj.setIcdRevisionNumber("IRNo");
		priorAuthHeaderDTOObj.setIdCardNumber("22");
		priorAuthHeaderDTOObj.setMasterPatientId("MPId");
		priorAuthHeaderDTOObj.setPriorAuthNumber("22");
		priorAuthHeaderDTOObj.setPriorAuthHeaderKey(22);
		priorAuthHeaderDTOObj.setPriorAuthDueDatetime("2018-04-26 01:02:00");
		priorAuthHeaderDTOObj.setPriorAuthEndServiceDate("2018-04-25 01:02:00");
		
		PriorAuthHeaderServiceStub.ProviderDTO providerDTO = new PriorAuthHeaderServiceStub.ProviderDTO();
		providerDTO.setAppCreatedDateTime("2018-04-26 01:02:00");
		providerDTO.setAppCreatedUserId("CreatedUserId");
		providerDTO.setAppMantDateTime("2018-04-26 01:02:00f");
		providerDTO.setAppMantUserId("AppMantUserId");
		providerDTO.setOrderingAddressLine1("Add1");
		providerDTO.setOrderingAddressLine2("Add2");
		providerDTO.setOrderingCity("TestCity");
		providerDTO.setOrderingFaxNumber("1234567890");
		providerDTO.setOrderingFirstName("OFirst");
		providerDTO.setOrderingLastName("OLast");
		providerDTO.setOrderingNpi("ONpi");
		providerDTO.setOrderingPhoneNumber("0987654321");
		providerDTO.setOrderingState("OState");
		providerDTO.setOrderingTinEin("OTEin");
		providerDTO.setOrderingZip("32001");
		providerDTO.setPriorAuthHeaderKey(01);
		providerDTO.setPriorAuthProviderTypeCode("ProviderCode");
		providerDTO.setRenderingAddressLine1("RenAdd1");
		providerDTO.setRenderingAddressLine2("RenAdd2");
		providerDTO.setRenderingCity("RedCity");
		providerDTO.setRenderingFaxNumber("9199129191");
		providerDTO.setRenderingFirstName("Rfirst");
		providerDTO.setRenderingLabName("RLab");
		providerDTO.setRenderingLastName("RLast");
		providerDTO.setRenderingNpi("Rnpi");
		providerDTO.setRenderingPhoneNumber("9191919191");
		providerDTO.setRenderingState("RState");
		providerDTO.setRenderingTinEin("RTinEin");
		providerDTO.setRenderingZip("90800");
		
		PriorAuthHeader priorAuthHeaderObj = new PriorAuthHeader();
		priorAuthHeaderObj.setPriorAuthHeaderDTO(new PriorAuthHeaderDTO[] { priorAuthHeaderDTOObj });
		priorAuthHeaderObj.setProvider(providerDTO);
		priorAuthorizationSearchResponse.set_return(priorAuthHeaderObj);
		return priorAuthorizationSearchResponse;
   }
}
