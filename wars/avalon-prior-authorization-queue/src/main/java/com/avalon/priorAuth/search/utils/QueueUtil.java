package com.avalon.priorAuth.search.utils;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Properties;

import com.avalon.priorAuth.search.beans.QueueSearchConstants;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.StringPool;

/**
 * Description
 *		This file contains the utilities use by the queue controller.
 *
 * CHANGE History
 * 		Version 1.0
 * 			Initial version
 * 		Version 1.1
 * 			Add a method to get a value from the properties file.
 *			Added methods to return the App Maintenance Datetime in the correct format.  
 *  	Version 1.1			03/08/2017
 *  		Use the constant for the properties file.
 *  
 */

public class QueueUtil {

	private static final Log log = LogFactoryUtil.getLog(QueueUtil.class.getName());

	private static QueueUtil queueUtil = null;
	
	// Define the prior authorization source tables
	public String procedureTab = "Procedure";
	public String notificationTab = "Notification";


	static {
		if (queueUtil == null) {
			queueUtil = new QueueUtil();
		}
	}
	
	public QueueUtil() {
	}
	
	public static QueueUtil getQueueUtil() {
		return queueUtil;
	}


	/**
	 * Get the notification date
	 */
	public String getNotificationDateFromCalender(Calendar cal, String browser) {
		String toDateStr = null;
		SimpleDateFormat toFormat = null;

		if (browser.equals("IE") || browser.equals("Firefox") || browser.equals("unknown")) {
			
			// Convert to the displayed format
			toFormat = new SimpleDateFormat("MM/dd/yyyy");
		} else {
			toFormat = new SimpleDateFormat("yyyy-MM-dd");
		}

		// Convert to the correct format
		toDateStr = toFormat.format(cal.getTime());
		log.info("Notification Date From Calender : " + toDateStr);
		return toDateStr;
	}

	/**
	 * Get the notification time
	 */
	public String getNotificationTimeFromCalender(Calendar cal) {
		String toTimeStr = null;
		SimpleDateFormat toFormat = null;

		// Get just the time
		toFormat = new SimpleDateFormat("hh:mm aa");

		// Convert to the correct format
		toTimeStr = toFormat.format(cal.getTime());
		log.info("Notification Time From Calender : " + toTimeStr);
		return toTimeStr;
	}

	/**
	 * Convert the creation date/time
	 */
	public String getStandardDateTime(String requestedDate) {
		String input = requestedDate;
		String inputFormat = "yyyy-MM-dd HH:mm";
		
		// Remove any seconds
		input = input.substring(0, Math.min(input.length(), inputFormat.length()));
		
		SimpleDateFormat inputformat = new SimpleDateFormat(inputFormat);
		SimpleDateFormat outputformat = new SimpleDateFormat("MM/dd/yyyy hh:mm aa");
		Date date = null;
		String output = null;

		try {
			date = inputformat.parse(input);
			output = outputformat.format(date);
		}
		catch (ParseException e) {
			log.error("ParseException in getStandardDateTime method",e);
		}
		return output;
	}

	

	/**
	 * For Aging Date format
	 */
	public String getAgingDateFromString(String datetime, String browser) {
		String input = datetime;
		SimpleDateFormat inputformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
		SimpleDateFormat outputformat = null;
		Date date = null;
		String output = null;

		try {
			if (browser.equals("IE") || browser.equals("Firefox") || browser.equals("unknown")) {
				
				// Convert to the displayed format
				outputformat = new SimpleDateFormat("MM/dd/yyyy");
			} else {
				outputformat = new SimpleDateFormat("yyyy-MM-dd");
			}
			date = inputformat.parse(input);
			output = outputformat.format(date);
		}
		catch (ParseException e) {
			log.error("ParseException in getAgingDateFromString method",e);
		}
		log.info("Ageing Date From String : " + output);
		return output;
	}

	/**
	 * For Aging Time format
	 */
	public String getAgingTimeFromString(String datetime) {
		String input = datetime;
		SimpleDateFormat inputformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
		SimpleDateFormat outputformat = new SimpleDateFormat("hh:mm aa");
		Date date = null;
		String output = null;

		try {
			date = inputformat.parse(input);
			output = outputformat.format(date);
		}
		catch (ParseException e) {
			log.error("ParseException in getAgingTimeFromString method",e);
		}
		log.info("Ageing Time From String : " + output);
		return output;
	}


	/**
	 * Get the authorization age for the PA queue list.
	 */

	public String setAuthorizationAgeQueue(String appDate, Long authorizationKey) {
		String authorizationAge = null;

	    log.info("appCreateDateTime Value: " + appDate);
	    if (appDate != null && !appDate.equals("")) {
	        String savedDate = AgeCalculation.getAgeCalculation().getProcedureSavedDate(appDate);
	        authorizationAge = AgeCalculation.getAgeCalculation().getAuthorizationAge(savedDate, authorizationKey);
	        log.info("Authorization Age: " + authorizationAge);
	    }
	    return authorizationAge;
	}
	
	/**
	 * Return the interger value of a variable from the properties file.
	 */

	public int getIntFromPropertiesFile(String propName, int defaultValue) {
		int retValue = defaultValue;

/*System.out.println("====> Printing stack trace:");
StackTraceElement[] elements = Thread.currentThread().getStackTrace();
for (int i = 1; i < elements.length; i++) {
	StackTraceElement s = elements[i];
	System.out.println("\tat " + s.getClassName() + "." + s.getMethodName() + "(" + s.getFileName() + ":" + s.getLineNumber() + ")");
}*/

		// Get the refresh rate from properties file
		String liferayHome = System.getProperty("LIFERAY_HOME");
		String propertyLocation = liferayHome +StringPool.SLASH + QueueSearchConstants.PROPERTIES_FILE;
		try {
		    log.info("getIntFromPropertiesFile for PA Queue::propertyLocation: " + propertyLocation);
			Properties prop = new Properties();
			InputStream input = new FileInputStream(propertyLocation);
			prop.load(input);
			String propStr = prop.getProperty(propName);
			
			// Convert the rate to an integer
			retValue = Integer.parseInt(propStr);

			if (retValue <= 0) {
				retValue = defaultValue;
			}
		} catch (NumberFormatException e) {
			retValue = defaultValue;
		} catch (FileNotFoundException e) {
			retValue = defaultValue;
		} catch (IOException e) {
			retValue = defaultValue;
		}
		
		return retValue;
	}

}
