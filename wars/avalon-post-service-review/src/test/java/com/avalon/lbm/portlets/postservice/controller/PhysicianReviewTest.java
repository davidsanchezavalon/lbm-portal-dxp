package com.avalon.lbm.portlets.postservice.controller;

import static org.mockito.Mockito.when;

import java.rmi.RemoteException;
import java.util.Map;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletRequest;
import javax.portlet.PortletSession;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.ui.Model;

import com.avalon.lbm.portlets.postservice.model.PhysicianReviewFO;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;

@RunWith(MockitoJUnitRunner.class)
public class PhysicianReviewTest {

    @Test
    public void testHandleRenderRequest() throws RemoteException {
	PhysicianReview physicianReview = new PhysicianReview();
	Model model = Mockito.mock(Model.class);
	RenderRequest request = Mockito.mock(RenderRequest.class);
	RenderResponse response = Mockito.mock(RenderResponse.class);
	PortletSession portletSession = Mockito.mock(PortletSession.class);
	when(request.getPortletSession()).thenReturn(portletSession);

	when((String) portletSession.getAttribute("authorizationStatus", 1))
		.thenReturn("approved");
	when((String) portletSession.getAttribute("memberDob", 1)).thenReturn(
		"08/28/1983");
	when((String) portletSession.getAttribute("memberName", 1)).thenReturn(
		"DRUE DEVOST");
	when((String) portletSession.getAttribute("memberId", 1)).thenReturn(
		"ZCL06440277");
	when((String) portletSession.getAttribute("authorizationAge", 1))
		.thenReturn("2016-02-08 10:25:49.0");
	when((String) portletSession.getAttribute("authorizationNumber", 1))
		.thenReturn("00100116020800001");
	when((Long) portletSession.getAttribute("authorizationKey", 1))
		.thenReturn(16174L);
	physicianReview.handleRenderRequest(request, response, model);

    }

    @Test
    public void testPhysicianReviewDetails() throws RemoteException, PortalException, SystemException {
	PhysicianReview physicianReview = new PhysicianReview();

	ActionRequest request = Mockito.mock(ActionRequest.class);
	ActionResponse response = Mockito.mock(ActionResponse.class);
	Map map = Mockito.mock(Map.class);
	ThemeDisplay themeDisplay = Mockito.mock(ThemeDisplay.class);
	User user = Mockito.mock(User.class);
	when(
		(ThemeDisplay) request
			.getAttribute("LIFERAY_SHARED_THEME_DISPLAY"))
		.thenReturn(themeDisplay);
	when(themeDisplay.getUser()).thenReturn(user);
	when(themeDisplay.getUser().getEmailAddress()).thenReturn(
		"test@liferay.com");
	PortletSession portletSession = Mockito.mock(PortletSession.class);
	when(request.getPortletSession()).thenReturn(portletSession);

	System.out.println("portletSession" + portletSession);
	when((Long) portletSession.getAttribute("authorizationKey", 1))
		.thenReturn(16174L);
	PhysicianReviewFO physicianReviewFO = new PhysicianReviewFO();
	physicianReviewFO.setPhysReviewReviewerName("Phyreviewrnameradha");
	physicianReviewFO.setPhysReviewDecision("approved");
	physicianReviewFO.setPhysReviewDecisionReason("Medically Necessary");

	ParamUtil paramUtil = Mockito.mock(ParamUtil.class);
	PortletRequest portletRequest = Mockito.mock(PortletRequest.class);
	when(
		ParamUtil.getString((PortletRequest) request,
			(String) "physReviewRequestedDate")).thenReturn(
		"2016-02-12");
	when(
		ParamUtil.getString((PortletRequest) request,
			(String) "physReviewReturnedDate")).thenReturn(
		"2016-02-27");
	when(
		ParamUtil.getString((PortletRequest) request,
			(String) "physReviewRequestedTime")).thenReturn(
		"02:30 AM");
	when(
		ParamUtil.getString((PortletRequest) request,
			(String) "physReviewReturnedTime")).thenReturn(
		"09:00 AM");
	when(
		ParamUtil.getString((PortletRequest) request,
			(String) "physReviewRationaleCreate")).thenReturn(
		"physiciannotesmock");

	physicianReview.physicianReviewDetails(request, response,
		physicianReviewFO, map);

    }

}
