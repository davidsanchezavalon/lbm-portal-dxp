package com.avalon.lbm.portlets.postservice.controller;

import static org.mockito.Mockito.when;

import java.rmi.RemoteException;
import java.util.Map;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletRequest;
import javax.portlet.PortletSession;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.ui.Model;

import com.avalon.lbm.portlets.postservice.model.NurseReviewFO;
import com.avalon.lbm.portlets.postservice.util.PostServiceUtil;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;

public class AdditionalInformationTest {

    @Test
    public void testHandleRenderRequest() {
	AdditionalInformation additionalInformation = new AdditionalInformation();

	Model model = Mockito.mock(Model.class);
	RenderRequest request = Mockito.mock(RenderRequest.class);
	RenderResponse response = Mockito.mock(RenderResponse.class);
	PortletSession portletSession = Mockito.mock(PortletSession.class);
	when(request.getPortletSession()).thenReturn(portletSession);

	when((String) portletSession.getAttribute("authorizationStatus", 1))
		.thenReturn("approved");
	when((String) portletSession.getAttribute("memberDob", 1)).thenReturn(
		"08/28/1983");
	when((String) portletSession.getAttribute("memberName", 1)).thenReturn(
		"DRUE DEVOST");
	when((String) portletSession.getAttribute("memberId", 1)).thenReturn(
		"ZCL06440277");
	when((String) portletSession.getAttribute("authorizationAge", 1))
		.thenReturn("2016-02-08 10:25:49.0");
	when((String) portletSession.getAttribute("authorizationNumber", 1))
		.thenReturn("00100116020800001");
	when((Long) portletSession.getAttribute("authorizationKey", 1))
		.thenReturn(16174L);
	additionalInformation.handleRenderRequest(request, response, model);
    }

    @Test
    public void testIntakeReviewDetails() throws RemoteException {
	AdditionalInformation additionalInformation = new AdditionalInformation();
	ActionRequest request = Mockito.mock(ActionRequest.class);
	ActionResponse response = Mockito.mock(ActionResponse.class);
	Map map = Mockito.mock(Map.class);
	NurseReviewFO nurseReviewFO = new NurseReviewFO();

	nurseReviewFO.setRequestedInfo("mock additionla info entered");

	nurseReviewFO.setRequestedDate("02/09/2016 08:40 AM");
	nurseReviewFO.setRequestedBy("test@liferay.com");
	nurseReviewFO.setIsReceived(true);
	ThemeDisplay themeDisplay = Mockito.mock(ThemeDisplay.class);
	User user = Mockito.mock(User.class);
	when(
		(ThemeDisplay) request
			.getAttribute("LIFERAY_SHARED_THEME_DISPLAY"))
		.thenReturn(themeDisplay);
	when(themeDisplay.getUser()).thenReturn(user);
	when(themeDisplay.getUser().getEmailAddress()).thenReturn(
		"test@liferay.com");
	PortletSession portletSession = Mockito.mock(PortletSession.class);
	when(request.getPortletSession()).thenReturn(portletSession);

	System.out.println("portletSession" + portletSession);
	when((Long) portletSession.getAttribute("authorizationKey", 1))
		.thenReturn(16174L);
	ParamUtil paramUtil = Mockito.mock(ParamUtil.class);
	PortletRequest portletRequest = Mockito.mock(PortletRequest.class);
	PostServiceUtil PostServiceUtil = Mockito.mock(PostServiceUtil.class);

	when(
		ParamUtil.getString((PortletRequest) request,
			(String) "requestedDate")).thenReturn(
		"02/09/2016 08:40 AM");
	when(
		ParamUtil.getString((PortletRequest) request,
			(String) "returnedDate")).thenReturn("2016-02-09");

	

	when(
		ParamUtil.getString((PortletRequest) request,
			(String) "saveAddInfoSection")).thenReturn(
		"saveRequested");
	additionalInformation.intakeReviewDetails(request, response,
		nurseReviewFO);
	when(
		ParamUtil.getString((PortletRequest) request,
			(String) "saveAddInfoSection")).thenReturn(
		"saveReturned");
	additionalInformation.intakeReviewDetails(request, response,
		nurseReviewFO);
	when(
		ParamUtil.getString((PortletRequest) request,
			(String) "saveAddInfoSection")).thenReturn("submit");
	additionalInformation.intakeReviewDetails(request, response,
		nurseReviewFO);

    }

    @Test
    public void testAdditionalInformationHandler() {
	AdditionalInformation additionalInformation = new AdditionalInformation();
	additionalInformation.AdditionalInformationHandler();
    }
}
