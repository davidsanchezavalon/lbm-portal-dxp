package com.avalon.lbm.portlets.postservice.controller;

import static org.mockito.Mockito.when;

import java.rmi.RemoteException;
import java.util.Map;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletSession;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.apache.axis.AxisFault;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.ui.Model;

import com.avalon.lbm.portlets.postservice.model.NotificationFO;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.theme.ThemeDisplay;

@RunWith(MockitoJUnitRunner.class)
public class NotificationTest {

    @Test
    public void testHandleRenderRequest() throws AxisFault, RemoteException {
		Notification Notification = new Notification();
		Model model = Mockito.mock(Model.class);
		RenderRequest request = Mockito.mock(RenderRequest.class);
		RenderResponse response = Mockito.mock(RenderResponse.class);

		PortletSession portletSession = Mockito.mock(PortletSession.class);
		when(request.getPortletSession()).thenReturn(portletSession);

		when((String) portletSession.getAttribute("authorizationStatus", 1))
			.thenReturn("approved");
		when((String) portletSession.getAttribute("memberDob", 1)).thenReturn(
			"08/28/1983");
		when((String) portletSession.getAttribute("memberName", 1)).thenReturn(
			"DRUE DEVOST");
		when((String) portletSession.getAttribute("memberId", 1)).thenReturn(
			"ZCL06440277");
		when((String) portletSession.getAttribute("authorizationAge", 1))
			.thenReturn("2016-02-08 10:25:49.0");
		when((String) portletSession.getAttribute("authorizationNumber", 1))
			.thenReturn("00100116020800001");
		when((Long) portletSession.getAttribute("authorizationKey", 1))
			.thenReturn(16174L);
		when((String) portletSession.getAttribute("membergroupId", 1))
			.thenReturn("002130030PSC");
		when(
			(String) portletSession.getAttribute("providerAuthStatus",
				PortletSession.APPLICATION_SCOPE)).thenReturn(
			"Approved");

		Notification.handleRenderRequest(request, response, model);

    }

    @Test
    public void testNotificationDetails() {

		Notification Notification = new Notification();
		ActionRequest request = Mockito.mock(ActionRequest.class);
		ActionResponse response = Mockito.mock(ActionResponse.class);
		Map map = Mockito.mock(Map.class);
		PortletSession portletSession = Mockito.mock(PortletSession.class);
		when(request.getPortletSession()).thenReturn(portletSession);
		when((Long) portletSession.getAttribute("authorizationKey", 1))
			.thenReturn(16174L);

		NotificationFO notificationFO = new NotificationFO();

		notificationFO.setNotificationDate("2016-02-12");
		notificationFO.setNotificationContactName("Radha");
		notificationFO.setNotificationSentDate("2016-02-19");
		notificationFO.setNotificationContactPhone("8008190490");
		ThemeDisplay themeDisplay = Mockito.mock(ThemeDisplay.class);
		User user = Mockito.mock(User.class);
		when(
			(ThemeDisplay) request
				.getAttribute("LIFERAY_SHARED_THEME_DISPLAY"))
			.thenReturn(themeDisplay);
		when(themeDisplay.getUser()).thenReturn(user);
		when(themeDisplay.getUser().getEmailAddress()).thenReturn(
			"test@liferay.com");

		Notification.notificationDetails(request, response, notificationFO, map);

    }

    @Test
    public void testProviderHandler() {

		Notification Notification = new Notification();
		Notification.NotificationHandler();
    }

}
