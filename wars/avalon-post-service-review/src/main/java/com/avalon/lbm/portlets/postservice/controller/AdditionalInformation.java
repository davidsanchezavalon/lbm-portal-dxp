/**
 * Description
 *		This file contain the controller methods for the Post Service Review Addition Information Page.
 *
 * CHANGE History
 * 		Version 1.0
 * 			Initial version copied from avalon-prior-authorization.
 * 		Version 1.1					08/27/2018
 * 			Do not display a blank note.
 *  
 */

package com.avalon.lbm.portlets.postservice.controller;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletRequest;
import javax.portlet.PortletSession;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.apache.axis2.AxisFault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import com.avalon.lbm.portlets.postservice.model.CurrentAuthorizationDetailsFO;
import com.avalon.lbm.portlets.postservice.model.NurseReviewFO;
import com.avalon.lbm.portlets.postservice.model.PostServiceConstants;
import com.avalon.lbm.portlets.postservice.util.AgeCalculation;
import com.avalon.lbm.portlets.postservice.util.PostServiceUtil;
import com.avalon.lbm.services.PriorAuthReviewServiceStub;
import com.avalon.lbm.services.PriorAuthReviewServiceStub.PriorAuthHeader;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.ParamUtil;

@Controller(value = "AdditionalInformation")
@RequestMapping(value = { "VIEW" })
public class AdditionalInformation {
	
	private static final Log log = LogFactoryUtil.getLog(AdditionalInformation.class.getName());

    /**** Render method which gets called during initial load of JSP *************************/
    @RenderMapping
    public String handleRenderRequest(RenderRequest request, 
    		                          RenderResponse response, 
    		                          Model model) {
        log.info("Rendering Additional Information Data");
        String sourceTab = PostServiceUtil.additionalTab;
        PortletSession portletSession = request.getPortletSession();

        Long authorizationKey = (Long) portletSession.getAttribute("authorizationKey", PortletSession.APPLICATION_SCOPE);
        log.info("authorizationKey" + authorizationKey);

        CurrentAuthorizationDetailsFO currentAuthorizationDetails = PostServiceUtil.getHeaderDetails(portletSession, sourceTab);

        // Set the authorization age
        ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute("LIFERAY_SHARED_THEME_DISPLAY");
        String appDate = (String) portletSession.getAttribute("CreateDatetime", PortletSession.APPLICATION_SCOPE);
        currentAuthorizationDetails.setAuthorizationAge(PostServiceUtil.setAuthorizationAge(portletSession, appDate, authorizationKey, themeDisplay));

        request.setAttribute("currentAuthorizationDetailsFO", (Object) currentAuthorizationDetails);

        NurseReviewFO nurseReviewFO = new NurseReviewFO();
        if (authorizationKey == null) {
            log.info("authorization key is null");
        } else {
            log.info("authorization key is not null");
            try {
                PriorAuthReviewServiceStub priorAuthReviewServiceStub = new PriorAuthReviewServiceStub();
                PriorAuthReviewServiceStub.SearchPriorAuthNoteDetails searchPriorAuthNoteDetails = new PriorAuthReviewServiceStub.SearchPriorAuthNoteDetails();
                PriorAuthReviewServiceStub.NoteSearchCriteria noteSearchCriteria = new PriorAuthReviewServiceStub.NoteSearchCriteria();
                noteSearchCriteria.setAuthorizationKey(authorizationKey.longValue());
                noteSearchCriteria.setSourceTab(sourceTab);
                searchPriorAuthNoteDetails.setSearchCriteriaNoteRequest(noteSearchCriteria);
                PriorAuthReviewServiceStub.SearchPriorAuthNoteDetailsE searchPriorAuthNoteDetailsE = new PriorAuthReviewServiceStub.SearchPriorAuthNoteDetailsE();
                searchPriorAuthNoteDetailsE.setSearchPriorAuthNoteDetails(searchPriorAuthNoteDetails);
                
                PriorAuthReviewServiceStub.SearchPriorAuthNoteDetailsResponseE searchPriorAuthNoteDetailsResponseE =null;
                PriorAuthReviewServiceStub.SearchPriorAuthNoteDetailsResponse searchPriorAuthNoteDetailsResponse = null;
                
                //TODO: Remove mock service code and condition. Don't remove else condition code
    			if(GetterUtil.getBoolean(com.liferay.portal.kernel.util.PropsUtil.get(PostServiceConstants.ENABLED_MOCK_SERVICE))){
    				 //Success Mock Code
    				searchPriorAuthNoteDetailsResponseE = new PriorAuthReviewServiceStub.SearchPriorAuthNoteDetailsResponseE();
                    searchPriorAuthNoteDetailsResponse = new PriorAuthReviewServiceStub.SearchPriorAuthNoteDetailsResponse();
                    PriorAuthHeader priorAuthHeader = new PriorAuthHeader();
                    priorAuthHeader.setAuthorizationKey(100L);
   			     	priorAuthHeader.setNoteCreateDatetime("2015-12-12 12:12:22.222");
   			     	priorAuthHeader.setNoteCreateUserId("UserId");
   			     	priorAuthHeader.setPriorAuthNoteText("Notetext");
   			     	priorAuthHeader.setPriorAuthNoteTypeCode("AuthType");
   			     	priorAuthHeader.setSourceTab("STab");
                    searchPriorAuthNoteDetailsResponse.set_return(new PriorAuthHeader[]{ priorAuthHeader });
                    searchPriorAuthNoteDetailsResponseE.setSearchPriorAuthNoteDetailsResponse(searchPriorAuthNoteDetailsResponse);
                }else{
    				searchPriorAuthNoteDetailsResponseE = priorAuthReviewServiceStub.searchPriorAuthNoteDetails(searchPriorAuthNoteDetailsE);
                    searchPriorAuthNoteDetailsResponse = searchPriorAuthNoteDetailsResponseE.getSearchPriorAuthNoteDetailsResponse();
                }
                if (searchPriorAuthNoteDetailsResponse.get_return() != null) {
                    log.info("Additional Information Notes Text : " + searchPriorAuthNoteDetailsResponse.get_return()[0].getPriorAuthNoteText());
                }
                ArrayList<String> nurseEnNotes = new ArrayList<String>();
                if (searchPriorAuthNoteDetailsResponse.get_return() != null) {
                    for (int i = 0; i < searchPriorAuthNoteDetailsResponse.get_return().length; ++i) {
                        String creationDateTime = searchPriorAuthNoteDetailsResponse.get_return()[i].getNoteCreateDatetime();
                        log.info("Creation Date Time of Additional Notes : " + creationDateTime);
                        String frmDateStr = AgeCalculation.getAgeCalculation().getTimeForNote(creationDateTime);

                        // Do not add a blank note
                        if (!searchPriorAuthNoteDetailsResponse.get_return()[i].getPriorAuthNoteText().equals("")){
                        	nurseEnNotes.add(searchPriorAuthNoteDetailsResponse.get_return()[i].getNoteCreateUserId() + "(" + frmDateStr + ")" + " : " +
                                searchPriorAuthNoteDetailsResponse.get_return()[i].getPriorAuthNoteText());
                        	log.info("Additional Information note " + (i + 1) + ": " + searchPriorAuthNoteDetailsResponse.get_return()[i].getNoteCreateUserId() +
                        		"(" + frmDateStr + ")" + " : " + searchPriorAuthNoteDetailsResponse.get_return()[i].getPriorAuthNoteText());
                        }
                    }
                    portletSession.setAttribute("nurseEnNotes", nurseEnNotes, PortletSession.APPLICATION_SCOPE);
                    nurseReviewFO.setNurseNotesRead(nurseEnNotes);
                    List readData = nurseReviewFO.getNurseNotesRead();
                    request.setAttribute("readData", (Object) readData);
                }
            } catch (AxisFault e) {
            	log.error("AxisFault in handleRenderRequest method",e);
            } catch (RemoteException e) {
            	log.error("RemoteException in handleRenderRequest method",e);
            }
        }
        request.setAttribute("nurseReviewFO", (Object) nurseReviewFO);
        return "view";
    }
    
    @ActionMapping
    public void AdditionalInformationHandler() {
        log.info("notification handler");
    }
    
    /************** Action method which gets called on click of SAVE button ***************************************/
    @ActionMapping(params = { "action=additionalInformationAction" })
    public void intakeReviewDetails(ActionRequest request, 
    		                        ActionResponse response, 
    		                        @ModelAttribute(value = "nurseReviewFO") NurseReviewFO nurseReviewFO) throws RemoteException {
 
    	String buttonClicked = ParamUtil.getString((PortletRequest) request, (String) "saveAdditionalNotes");
        log.info("Saving Additional Information :" + buttonClicked);
        
        String sourceTab = PostServiceUtil.nurseReviewTab;
        String noteType = PostServiceUtil.additionalNote;
        ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute("LIFERAY_SHARED_THEME_DISPLAY");
        String creationDate = AgeCalculation.getAgeCalculation().createDate();
        PortletSession portletSession = request.getPortletSession();
        Long authorizationKey = (Long) portletSession.getAttribute("authorizationKey", PortletSession.APPLICATION_SCOPE);
        PriorAuthReviewServiceStub priorAuthReviewServiceStub = new PriorAuthReviewServiceStub();
        PriorAuthReviewServiceStub.CreatePriorAuthNoteDetails createPriorAuthNoteDetails = new PriorAuthReviewServiceStub.CreatePriorAuthNoteDetails();
        PriorAuthReviewServiceStub.CreatePriorAuthNoteDetailsE CreatePriorAuthNoteDetailsE2 = new PriorAuthReviewServiceStub.CreatePriorAuthNoteDetailsE();
        PriorAuthReviewServiceStub.PriorAuthHeader priorAuthHeader = new PriorAuthReviewServiceStub.PriorAuthHeader();
        String additionalNotesText = ParamUtil.getString((PortletRequest) request, (String) "additionalNotesCreate");
        if (additionalNotesText != null && !additionalNotesText.equals("")) {
            priorAuthHeader.setPriorAuthNoteText(additionalNotesText.toString());
        }
        priorAuthHeader.setPriorAuthNoteTypeCode(noteType);
        priorAuthHeader.setSourceTab(sourceTab);
        priorAuthHeader.setAuthorizationKey(authorizationKey.longValue());
        priorAuthHeader.setNoteCreateUserId(themeDisplay.getUser().getEmailAddress());
        priorAuthHeader.setNoteCreateDatetime(creationDate);
        createPriorAuthNoteDetails.setPriorAuthNoteRequest(priorAuthHeader);
        CreatePriorAuthNoteDetailsE2.setCreatePriorAuthNoteDetails(createPriorAuthNoteDetails);
        //TODO: Remove below condition. Don't remove else condition code
		if(GetterUtil.getBoolean(com.liferay.portal.kernel.util.PropsUtil.get(PostServiceConstants.ENABLED_MOCK_SERVICE))){
		}else{
			PriorAuthReviewServiceStub.CreatePriorAuthNoteDetailsResponseE createPriorAuthNoteDetailsResponseE = priorAuthReviewServiceStub.createPriorAuthNoteDetails(CreatePriorAuthNoteDetailsE2);
	        Long longKey = createPriorAuthNoteDetailsResponseE.getCreatePriorAuthNoteDetailsResponse().get_return().getAuthorizationKey();
	    }
        request.setAttribute("nurseReviewFO", (Object) nurseReviewFO);
    }
}