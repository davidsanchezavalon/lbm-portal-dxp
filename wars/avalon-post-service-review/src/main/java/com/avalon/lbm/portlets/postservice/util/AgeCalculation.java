/**
 * Description
 *		This file contain the controller methods for the Post Service Review aging calculation methods.
 *
 * CHANGE History
 * 		Version 1.0		
 * 			Initial version copied from avalon-prior-authorization.
 *  	Version 1.1			03/07/2017
 *  		Use the constant for the properties file.
 *  
 */

package com.avalon.lbm.portlets.postservice.util;

import java.io.FileInputStream;
import java.io.InputStream;
import java.rmi.RemoteException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.axis2.AxisFault;

import com.avalon.lbm.portlets.postservice.model.PostServiceConstants;
import com.avalon.lbm.services.DAOExceptionException;
import com.avalon.lbm.services.PriorAuthHeaderServiceStub;
import com.avalon.lbm.services.PriorAuthHeaderServiceStub.PriorAuthHeader;
import com.avalon.lbm.services.PriorAuthHeaderServiceStub.PriorAuthHeaderDTO;
import com.avalon.lbm.services.PriorAuthReviewServiceStub;
import com.avalon.lbm.services.PriorAuthReviewServiceStub.PriorAuthReview;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.GetterUtil;

public class AgeCalculation {
	
	private static final Log log = LogFactoryUtil.getLog(AgeCalculation.class.getName());

	private static AgeCalculation ageCaLCulation = null;
	private AgeCalculation() {
	}
	public static AgeCalculation getAgeCalculation() {
		return ageCaLCulation;
	}

	static {
		if (ageCaLCulation == null) {
			ageCaLCulation = new AgeCalculation();
		}
	}

	/**
	 * To Create Current Date and Time for  AppCreateDatetime
	 */
	public String getProcedureCreationDate() { 
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();
		String str = dateFormat.format(date);
		return str;
	}

	/**
	 * To Create Current Date and Time 
	 */
	public String getCurrentDate() {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm");
		Date date = new Date();
		String str = dateFormat.format(date);
		StringBuilder year = new StringBuilder();
		StringBuilder month = new StringBuilder();
		StringBuilder days = new StringBuilder();
		StringBuilder hours = new StringBuilder();
		StringBuilder minutes = new StringBuilder();
		StringBuilder currentDate = new StringBuilder();
		char[] ch = str.toCharArray();
		year.append(ch[0]);
		year.append(ch[1]);
		year.append(ch[2]);
		year.append(ch[3]);
		month.append(ch[5]);
		month.append(ch[6]);
		days.append(ch[8]);
		days.append(ch[9]);
		hours.append(ch[11]);
		hours.append(ch[12]);
		minutes.append(ch[14]);
		minutes.append(ch[15]);
		currentDate.append(year);
		currentDate.append("-");
		currentDate.append(month);
		currentDate.append("-");
		currentDate.append(days);
		currentDate.append(" ");
		currentDate.append(hours);
		currentDate.append(":");
		currentDate.append(minutes);
		return currentDate.toString();
	}
	
	
	/**
	 * To get the  Saved Date and Time
	 */
	public String getProcedureSavedDate(String str) { 
		StringBuilder year = new StringBuilder();
		StringBuilder month = new StringBuilder();
		StringBuilder days = new StringBuilder();
		StringBuilder hours = new StringBuilder();
		StringBuilder minutes = new StringBuilder();
		StringBuilder savedDate = new StringBuilder();
		char[] ch = str.toCharArray();
		year.append(ch[0]);
		year.append(ch[1]);
		year.append(ch[2]);
		year.append(ch[3]);
		month.append(ch[5]);
		month.append(ch[6]);
		days.append(ch[8]);
		days.append(ch[9]);
		hours.append(ch[11]);
		hours.append(ch[12]);
		minutes.append(ch[14]);
		minutes.append(ch[15]);
		savedDate.append(year);
		savedDate.append("-");
		savedDate.append(month);
		savedDate.append("-");
		savedDate.append(days);
		savedDate.append(" ");
		savedDate.append(hours);
		savedDate.append(":");
		savedDate.append(minutes);
		return savedDate.toString();
	}

	/**
	 * To Create Current Date and Time
	 */
	public String createDate() { 
		SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
		Date date = new Date();
		String creationDate = dateFormat.format(date);
		log.info("creationDate " + creationDate);
		return creationDate;
	}

	/**
	 * To display date in MM/dd/yyyy HH:mm:ss
	 */
	public String getConversationdate(String requestedDate) {
		String frmDateStr = null;
		try {
			SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
			Date date = formatter.parse(requestedDate);
			SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
			frmDateStr = dateFormat.format(date);
			log.info(frmDateStr);
		}
		catch (ParseException e) {
			log.error("ParseException in getConversationdate method",e);
		}
		return frmDateStr;
	}

	/**
	 * To Display Current TimeStamp in Enter Notes
	 */
	public String getTimeForNote(String creationDateTime) { 
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy , hh:mm a");
		String frmDateStr = null;
		try {
			Date date = formatter.parse(creationDateTime);
			frmDateStr = dateFormat.format(date);
			log.info("frmDateStr" + frmDateStr);
		}
		catch (ParseException e) {
			log.error("ParseException in getTimeForNote method",e);
		}
		return frmDateStr;
	}

	/**
	 * Get the requested date and time.  Return the creation date and time if the requested date/time is not set.
	 */

	private String getAuthRequestDateAndTime(String savedDateTime, Long authorizationKey, ThemeDisplay themeDisplay) {
		String returnVal = null;
		
		try {
            PriorAuthHeaderServiceStub priorAuthHeaderServiceStub = new PriorAuthHeaderServiceStub();
            PriorAuthHeaderServiceStub.GetPriorAuthHeaderE getPriorAuthHeaderE = new PriorAuthHeaderServiceStub.GetPriorAuthHeaderE();
            PriorAuthHeaderServiceStub.GetPriorAuthHeader GetPriorAuthHeader2 = new PriorAuthHeaderServiceStub.GetPriorAuthHeader();
            PriorAuthHeaderServiceStub.PriorAuthHeader priorAuthHeader = new PriorAuthHeaderServiceStub.PriorAuthHeader();
            PriorAuthHeaderServiceStub.PriorAuthHeaderDTO authHeaderDTO = new PriorAuthHeaderServiceStub.PriorAuthHeaderDTO();
            priorAuthHeader.setPriorAuthHeaderDTO(authHeaderDTO);
            priorAuthHeader.setSourceTab(PostServiceUtil.procedureTab);
			priorAuthHeader.setAuthorizationKey(authorizationKey.longValue());
			priorAuthHeader.setAppCreateUserId(themeDisplay.getUser().getEmailAddress());
			priorAuthHeader.setAppMaintUserId(themeDisplay.getUser().getEmailAddress());
			GetPriorAuthHeader2.setSearchCriteriaRequest(priorAuthHeader);
			getPriorAuthHeaderE.setGetPriorAuthHeader(GetPriorAuthHeader2);
			
			PriorAuthHeaderServiceStub.GetPriorAuthHeaderResponseE getPriorAuthHeaderResponseE = null;
			PriorAuthHeaderServiceStub.GetPriorAuthHeaderResponse getPriorAuthHeaderResponse = null;
			
			//TODO: Remove mock service code and condition. Don't remove else condition code
			if(GetterUtil.getBoolean(com.liferay.portal.kernel.util.PropsUtil.get(PostServiceConstants.ENABLED_MOCK_SERVICE))){
				 //Success Mock Code
				getPriorAuthHeaderResponseE = new PriorAuthHeaderServiceStub.GetPriorAuthHeaderResponseE();
				getPriorAuthHeaderResponse = new PriorAuthHeaderServiceStub.GetPriorAuthHeaderResponse();
				PriorAuthHeader priorAuthHeaderObj = new PriorAuthHeader();
				 
				 PriorAuthHeaderDTO priorAuthHeaderDTOObj = new PriorAuthHeaderDTO();
				 priorAuthHeaderDTOObj.setAppMaintDatetime("2018-04-26 01:02:00.1");
				 priorAuthHeaderDTOObj.setPriorAuthSubmissionStatusCode("ASub");
				 priorAuthHeaderDTOObj.setAuthRequestedDate("2018-04-26 01:02:00.1");
				 priorAuthHeaderObj.setPriorAuthHeaderDTO(priorAuthHeaderDTOObj);
				 getPriorAuthHeaderResponse.set_return(priorAuthHeaderObj);
				
				getPriorAuthHeaderResponseE.setGetPriorAuthHeaderResponse(getPriorAuthHeaderResponse);
			}else{
				getPriorAuthHeaderResponseE = priorAuthHeaderServiceStub.getPriorAuthHeader(getPriorAuthHeaderE);
				getPriorAuthHeaderResponse = getPriorAuthHeaderResponseE.getGetPriorAuthHeaderResponse();
			}
			 
			if (getPriorAuthHeaderResponse.get_return().getPriorAuthHeaderDTO() != null) {
				PriorAuthHeaderServiceStub.PriorAuthHeaderDTO priorAuthHeaderDTO = getPriorAuthHeaderResponse.get_return().getPriorAuthHeaderDTO();

				returnVal = priorAuthHeaderDTO.getAuthRequestedDate();
			}

			if ((returnVal == null) || ((returnVal != null) && returnVal.equals("null"))) {
				
				// Return the save date/time
				returnVal = savedDateTime;
			} else {
				
				// Convert the request date/time to the correct format
				returnVal = PostServiceUtil.getPriorAuthUtil().getStandardDateTime(returnVal);
			}
		} catch (AxisFault e) {
			log.error("AxisFault Exception in getAuthRequestDateAndTime method",e);
        } catch (RemoteException e) {
        	log.error("RemoteException in getAuthRequestDateAndTime method",e);
		} catch (DAOExceptionException e) {
			log.error("DAOExceptionException in getAuthRequestDateAndTime method",e);
        }

		log.info("auth requested date/time: " + returnVal);

		return returnVal;
	}

	/**
	 * Get the Notification date and time the letter was sent.  Return the current date and time if the letter sent date/time is not set.
	 */

	private String getLetterSentDateAndTime(Long authorizationKey) {
		String returnVal = null;
		
		try {
			PriorAuthReviewServiceStub priorAuthReviewServiceStub = new PriorAuthReviewServiceStub();
			PriorAuthReviewServiceStub.SearchPriorAuthReviewDetailsE searchPriorAuthReviewDetailsE = new PriorAuthReviewServiceStub.SearchPriorAuthReviewDetailsE();
			PriorAuthReviewServiceStub.SearchPriorAuthReviewDetails searchPriorAuthReviewDetails = new PriorAuthReviewServiceStub.SearchPriorAuthReviewDetails();
			
			PriorAuthReviewServiceStub.ReviewSearchCriteria reviewSearchCriteria = new PriorAuthReviewServiceStub.ReviewSearchCriteria();
			reviewSearchCriteria.setAuthorizationKey(authorizationKey.longValue());
            reviewSearchCriteria.setSourceTab(PostServiceUtil.notificationTab);
			searchPriorAuthReviewDetails.setSearchCriteriaRequest(reviewSearchCriteria);
			searchPriorAuthReviewDetailsE.setSearchPriorAuthReviewDetails(searchPriorAuthReviewDetails);
			
			PriorAuthReviewServiceStub.SearchPriorAuthReviewDetailsResponseE searchPriorAuthReviewDetailsResponseE = null;
			PriorAuthReviewServiceStub.SearchPriorAuthReviewDetailsResponse searchPriorAuthReviewDetailsResponse = null;
			
			//TODO: Remove mock service code and condition. Don't remove else condition code
			if(GetterUtil.getBoolean(com.liferay.portal.kernel.util.PropsUtil.get(PostServiceConstants.ENABLED_MOCK_SERVICE))){
				 //Success Mock Code
				searchPriorAuthReviewDetailsResponse = searchPriorAuthResponseSuccessMock();
			}else{
				searchPriorAuthReviewDetailsResponseE = priorAuthReviewServiceStub.searchPriorAuthReviewDetails(searchPriorAuthReviewDetailsE);
				searchPriorAuthReviewDetailsResponse = searchPriorAuthReviewDetailsResponseE.getSearchPriorAuthReviewDetailsResponse();
				
			}
			PriorAuthReviewServiceStub.PriorAuthReview review = searchPriorAuthReviewDetailsResponse.get_return();

			String usedDate = null;
			String usedTime = null;
            if (review.getNotificationSentDate() != null && !review.getNotificationSentDate().equals("")) {

            	// Use the IE date format
            	usedDate = PostServiceUtil.getPriorAuthUtil().getNotificationDateFromCalender(review.getNotificationSentDate(), "IE");
            	usedTime = PostServiceUtil.getPriorAuthUtil().getNotificationTimeFromCalender(review.getNotificationSentDate());
            } else {
    			SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
    			Date date = new Date();
    			usedDate = dateFormat.format(date);
    			SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mm a ");
    			Date time = new Date();
    			usedTime = timeFormat.format(time);
            }
			
        	// Put the date and time in the returned string
			returnVal = usedDate + " " + usedTime;
		} catch (AxisFault e) {
			log.error("AxisFault Exception in getLetterSentDateAndTime method",e);
        } catch (RemoteException e) {
        	log.error("RemoteException in getLetterSentDateAndTime method",e);
        }

		log.info("letter sent date/time: " + returnVal);

		return returnVal;
	}
	
	/**
	 * Calculate Authorization Age with both Saved Date and Time and Current Date and Time.
	 */

	public String getAuthorizationAge(String savedDate, String savedTime) { 
		int diffDays = 0;
		int diffhours = 0;
		int diffmin = 0;
		String age = null;
		try {
			log.info("savedDate: " + savedDate);
			log.info("savedTime : " + savedTime);
			SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
			Date date = new Date();
			String currentDate = dateFormat.format(date);
			SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mm a ");
			Date time = new Date();
			String currentTime = timeFormat.format(time);
			log.info("Current Date : " + currentDate);
			log.info("Current Time : " + currentTime);
			String date1 = savedDate;
			String time1 = savedTime;
			String date2 = currentDate;
			String time2 = currentTime;
			String format = "MM/dd/yyyy hh:mm a";
			SimpleDateFormat sdf = new SimpleDateFormat(format);
			Date dateObj1 = sdf.parse(date1 + " " + time1);
			Date dateObj2 = sdf.parse(date2 + " " + time2);

			log.info(dateObj2 + "\n");
			long diff = dateObj2.getTime() - dateObj1.getTime();
			diffDays = (int)(diff / 86400000);
			log.info("difference between days: " + String.valueOf(diffDays).replace("-", ""));
			diffhours = (int)(diff / 3600000);
			log.info("difference between hours: " + String.valueOf(diffhours).replace("-", ""));
			diffmin = (int)(diff / 60000);
			log.info("Miutes :" + diffmin);
			log.info("difference between minutes: " + String.valueOf(diffmin).replace("-", ""));
			int diffsec = (int)(diff / 1000);
			log.info("difference between seconds: " + String.valueOf(diffsec).replace("-", ""));
			log.info("difference between milliseconds: " + String.valueOf(diff).replace("-", ""));
			log.info("difference between milliseconds: " + String.valueOf(diffDays).replace("-", "") + ":" + String.valueOf(diffhours % 24).replace("-", "") + ":" + String.valueOf(diffmin % 60).replace("-", ""));
			age = String.valueOf(diffDays).replace("-", "") + ":" + String.valueOf(diffhours % 24).replace("-", "") + ":" + String.valueOf(diffmin % 60).replace("-", "");
		}
		catch (Exception e) {
			log.error("Exception in getAuthorizationAge method",e);
		}
		return age;
	}
	
	/**
	 * Calculate Authorization Age with both Saved Date and Time and the date the notification letter was sent or the Current Date and Time.
	 */

	public String getAuthorizationAge(String savedDate, String savedTime, Long authorizationKey, ThemeDisplay themeDisplay) { 
		int diffDays = 0;
		int diffHours = 0;
		int diffMins = 0;
		String age = null;  // days : hours : minutes;
		String format = "MM/dd/yyyy hh:mm a";

		try {

			List<Date> holidays = new ArrayList<Date>();

			// Get the holidays from properties file
			String liferay_Home = System.getProperty("LIFERAY_HOME");
			String propertyLocation = liferay_Home +"/" + PostServiceConstants.PROPERTIES_FILE;
			log.info("getAuthorizationAge for PSR::propertyLocation: " + propertyLocation);
			Properties prop = new Properties();
			InputStream input = new FileInputStream(propertyLocation);
			prop.load(input);
			String holidayStr = prop.getProperty("AvalonHolidayCalendar");
			
			if (holidayStr != null) {

				// Split holidays into individual days
				String[] datesStr = holidayStr.split("\\s+");
				for (int cnt = 0; cnt < datesStr.length; cnt++) {
					Date nextDate = new SimpleDateFormat("MM/dd/yyyy").parse(datesStr[cnt]);
					holidays.add(nextDate);
				}
			}
			
			// Get the start time
			String startDateTime = savedDate + " " + savedTime;
			startDateTime = getAuthRequestDateAndTime(startDateTime, authorizationKey, themeDisplay);
			log.info("Age Start Date/Time Used : " + startDateTime);

			// Get the end time
			String endDateTime = getLetterSentDateAndTime(authorizationKey);
			log.info("Age End Date/Time Used : " + endDateTime);

			SimpleDateFormat sdf = new SimpleDateFormat(format);
			Date startDate = sdf.parse(startDateTime);
			Date endDate = sdf.parse(endDateTime);

			Calendar startCal = Calendar.getInstance();
			startCal.setTime(startDate);
			Calendar endCal = Calendar.getInstance();
			endCal.setTime(endDate);

			// Move the starting date to the start of the first working day
			Calendar holidayCal = (Calendar) startCal.clone();
			holidayCal.set(Calendar.HOUR_OF_DAY, 0);
			holidayCal.set(Calendar.MINUTE, 0);
			holidayCal.set(Calendar.SECOND, 0);
			holidayCal.set(Calendar.MILLISECOND, 0);
			boolean nonWorkingFlag = false;
			while ((startCal.compareTo(endCal) <= 0) &&
					(Calendar.SATURDAY == startCal.get(Calendar.DAY_OF_WEEK) || Calendar.SUNDAY == startCal.get(Calendar.DAY_OF_WEEK) || holidays.contains(holidayCal.getTime()))) {
				nonWorkingFlag = true;
				
				// Increment start date past all non-working days
				startCal.add(Calendar.DATE, 1);
				holidayCal.add(Calendar.DATE, 1);
			}
			if (nonWorkingFlag) {
				startCal.set(Calendar.HOUR_OF_DAY, 0);
				startCal.set(Calendar.MINUTE, 0);
				startCal.set(Calendar.SECOND, 0);
				startCal.set(Calendar.MILLISECOND, 0);
			}
			
			// Move the ending date to the end of the last working day
			holidayCal = (Calendar) endCal.clone();
			holidayCal.set(Calendar.HOUR_OF_DAY, 0);
			holidayCal.set(Calendar.MINUTE, 0);
			holidayCal.set(Calendar.SECOND, 0);
			holidayCal.set(Calendar.MILLISECOND, 0);
			nonWorkingFlag = false;
			while ((startCal.compareTo(endCal) <= 0) &&
					(Calendar.SATURDAY == endCal.get(Calendar.DAY_OF_WEEK) || Calendar.SUNDAY == endCal.get(Calendar.DAY_OF_WEEK) || holidays.contains(holidayCal.getTime()))) {
				nonWorkingFlag = true;
				
				// Move to the previous day
				endCal.add(Calendar.DATE, -1);
				holidayCal.add(Calendar.DATE, -1);
			}
			if (nonWorkingFlag) {
				endCal.add(Calendar.DATE, 1);
				endCal.set(Calendar.HOUR_OF_DAY, 0);
				endCal.set(Calendar.MINUTE, 0);
				endCal.set(Calendar.SECOND, 0);
				endCal.set(Calendar.MILLISECOND, 0);
			}

			// Set a holiday date to the start date with only month, day, and year for the holiday comparison
			holidayCal = (Calendar) startCal.clone();
			holidayCal.set(Calendar.HOUR_OF_DAY, 0);
			holidayCal.set(Calendar.MINUTE, 0);
			holidayCal.set(Calendar.SECOND, 0);
			holidayCal.set(Calendar.MILLISECOND, 0);

			// Get number of full work days between the two dates
			boolean dayAddedFlag = false;
			while (startCal.compareTo(endCal) <= 0) {

				// Determine if this is a valid working day
				if (Calendar.SATURDAY != startCal.get(Calendar.DAY_OF_WEEK) && Calendar.SUNDAY != startCal.get(Calendar.DAY_OF_WEEK) && !holidays.contains(holidayCal.getTime())) {
			    	diffDays++;
			    	dayAddedFlag = true;
			    }
			    startCal.add(Calendar.DATE, 1);
			    holidayCal.add(Calendar.DATE, 1);
			    
			    // If the start date has been moved past the end date, move the start date back a day, remove any day added, and exit the loop
				if (startCal.compareTo(endCal) > 0) {
					startCal.add(Calendar.DATE, -1);
					if (dayAddedFlag) {
						diffDays--;
					}
					break;
				}
				
				// Reset the day added flag for the next working day
				if (Calendar.SATURDAY != startCal.get(Calendar.DAY_OF_WEEK) && Calendar.SUNDAY != startCal.get(Calendar.DAY_OF_WEEK) && !holidays.contains(holidayCal.getTime())) {
					dayAddedFlag = false;
				}
			}

			if (startCal.compareTo(endCal) >= 0) {
				diffHours = 0;
				diffMins = 0;
			} else {
				
				// Get number of hours and minutes in the last day
				diffHours = (int) TimeUnit.MILLISECONDS.toHours(endCal.getTimeInMillis() - startCal.getTimeInMillis());
				
				// Determine if adding time for the missing time on Friday or a holiday moveed past a day
				if (diffHours == 24) {
					diffDays++;
					diffHours = 0;
				}
				diffMins = ((int) TimeUnit.MILLISECONDS.toMinutes(endCal.getTimeInMillis() - startCal.getTimeInMillis())) % 60;
			}
		}
		catch (Exception e) {
			log.error("Exception in getAuthorizationAge method",e);
		}
		
		age = String.valueOf(diffDays) + ":" + String.valueOf(diffHours) + ":" + String.valueOf(diffMins);
		return age;
	}
	
	/**
	 * TODO: Remove below mock code 
	 * @return
	 */
	private PriorAuthReviewServiceStub.SearchPriorAuthReviewDetailsResponse searchPriorAuthResponseSuccessMock(){
		PriorAuthReviewServiceStub.SearchPriorAuthReviewDetailsResponse searchPriorAuthReviewDetailsResponse = new PriorAuthReviewServiceStub.SearchPriorAuthReviewDetailsResponse();
		 PriorAuthReview priorAuthReview = new PriorAuthReview();
		 Calendar cal = Calendar.getInstance(); 
	         priorAuthReview.setAuthorizationKey(100L);
	         priorAuthReview.setDateTimeReturned("2018-04-26 01:02:00");
	         priorAuthReview.setDateTimeSent("2018-04-26 01:02:00");
	         priorAuthReview.setDecisionKey("DKey");
	         priorAuthReview.setDecisionValue("DValue");
	         priorAuthReview.setEnterPhysicianRationale("EPR");
	         priorAuthReview.setIntakeNotesCreate("INC");
	         priorAuthReview.setIntakeNotesRead("INR");
	         priorAuthReview.setIsReceived(true);
	         priorAuthReview.setNotificationBy("NY");
	         priorAuthReview.setNotificationContactFax("NCF");
	         priorAuthReview.setNotificationContactName("NCN");
	         priorAuthReview.setNotificationContactPhone("NCP");
	         priorAuthReview.setNotificationSentDate(cal);
	         priorAuthReview.setNurseNotesCreate("NCreate");
	         priorAuthReview.setNurseNotesRead("Nread");
	         priorAuthReview.setPhysicianName("PName");
	         priorAuthReview.setPhysicianRationaleMessage("RMsg");
	         priorAuthReview.setReasonKey("RKey");
	         priorAuthReview.setReasonValue("RValue");
	         priorAuthReview.setReconDecision("RD");
	         priorAuthReview.setReconDecisionDate("2018-04-26 01:02:00");
	         priorAuthReview.setReconNotesCreate("RNC");
	         priorAuthReview.setReconDecisionReviewer("RDR");
	         priorAuthReview.setReconNotesRead("RNR");
	         priorAuthReview.setRequestedBy("RB");
	         priorAuthReview.setRequestedDate("2018-04-26 01:02:00");
	         priorAuthReview.setRequestedInfo("RInfo");
	         priorAuthReview.setSourceTab("STab");
		 searchPriorAuthReviewDetailsResponse.set_return(priorAuthReview);
		 return searchPriorAuthReviewDetailsResponse;
	}
}