/**
 * Description
 *		This file methods to get values from the properties file for Post Service Review utility.
 *
 * CHANGE History
 * 		Version 1.0
 * 			Initial version copied from avalon-prior-authorization.
 *  	Version 1.1			03/07/2017
 *  		Use the constant for the properties file.
 *  
 */

package com.avalon.lbm.services;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;

import com.avalon.lbm.portlets.postservice.model.PostServiceConstants;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.StringPool;

public class EsbUrlDelegate {
	
	private static final Log log = LogFactoryUtil.getLog(EsbUrlDelegate.class.getName());

    private static EsbUrlDelegate esbUrlDelegate = null;
    private static Properties properties = null;
    
    
    /**
	 * To load the common.properties files from the server bin directory and to fetch the esb-end point urls.
	 * @return
	 */
    
    static {
        if (properties == null) {
            properties = new Properties();
            esbUrlDelegate = new EsbUrlDelegate();
            String liferayHome = System.getProperty("LIFERAY_HOME");
			String propertyLocation = liferayHome +StringPool.SLASH + PostServiceConstants.PROPERTIES_FILE;
            try {
                log.info("EsbUrlDelegate for PSR::propertyLocation: " + propertyLocation);
                properties.load(new FileInputStream(new File(propertyLocation)));
                log.info("reading properties file " + properties);
            } catch (FileNotFoundException e) {
            	log.error("FileNotFoundException in static block",e);
            } catch (IOException e) {
            	log.error("IOException in static block",e);
            }
        }
    }
    
    public static EsbUrlDelegate getEsbUrlDelegate() {
        return esbUrlDelegate;
    }
    
    /**
     * Fetching the esb end point urls from the properties object
     * Setting to the esb end point urls in the map object
     */
    public Map<String, String> getEsbUrl() {
        Map<String, String> urls = new ConcurrentHashMap<String, String>();
        try {
            String serverInstance = System.getProperty("serverInstance");
            log.info("ServerInstanceAtEsbDelegate :" + serverInstance);
            log.info("EsbUrlMemberService :" + serverInstance + ".serviceHeader.endpoint");
            urls.put("serviceHeaderUrl", properties.getProperty(serverInstance + ".serviceHeader.endpoint"));// To
                                                                                                             // fetch
                                                                                                             // the
            // current evironment
            // esb url
            urls.put("serviceReviewUrl", properties.getProperty(serverInstance + ".reviewHeader.endpoint"));// To
                                                                                                            // fetch
                                                                                                            // the
                                                                                                            // current
            // evironment esb url
        } catch (Exception e) {
        	log.error("Exception in getEsbUrl method",e);

        }
        return urls;
    }
}
