/**
 * Description
 *		This file contain the getter and setter methods for the Post Service Review Common Member Display.
 *
 * CHANGE History
 * 		Version 1.0
 * 			Initial version copied from avalon-prior-authorization.
 * 		Version 1.1				08/08/2018
 *			Added HP LOB and Subscriber Name.
 *  
 */

package com.avalon.lbm.portlets.postservice.model;

import java.io.Serializable;

public class CurrentAuthorizationDetailsFO
implements Serializable {
    
    /**
     * To store the member details
     */
    
    private static final long serialVersionUID = 8650631274033552371L;
    private String idCardNumber;
    private String authorizationNumber;
    private String memberName;
    private String authorizationStatus;
    private String memberDOB;
    private String authorizationAge;
    private String submissionStatus;
    private String authSubmissionStatus;
    private String memberGroupId;
    private String gender;
    private String memberAddressLine1;
    private String memberAddressLine2;
    private String memberCity;
    private String memberStateCode;
    private String memberZipcode;
    private String memberPhoneNumber;
    private String hpLob;
    private String subscriberName;

    public String getMemberGroupId() {
        return memberGroupId;
    }

    public String getAuthSubmissionStatus() {
        return authSubmissionStatus;
    }

    public void setAuthSubmissionStatus(String authSubmissionStatus) {
        this.authSubmissionStatus = authSubmissionStatus;
    }

    public String getIdCardNumber() {
        return idCardNumber;
    }

    public void setIdCardNumber(String idCardNumber) {
        this.idCardNumber = idCardNumber;
    }

    public String getAuthorizationNumber() {
        return authorizationNumber;
    }

    public void setAuthorizationNumber(String authorizationNumber) {
        this.authorizationNumber = authorizationNumber;
    }

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    public String getAuthorizationStatus() {
        return authorizationStatus;
    }

    public void setAuthorizationStatus(String authorizationStatus) {
        this.authorizationStatus = authorizationStatus;
    }

    public String getMemberDOB() {
        return memberDOB;
    }

    public void setMemberDOB(String memberDOB) {
        this.memberDOB = memberDOB;
    }

    public String getAuthorizationAge() {
        return authorizationAge;
    }

    public void setAuthorizationAge(String authorizationAge) {
        this.authorizationAge = authorizationAge;
    }

    public String getSubmissionStatus() {
        return submissionStatus;
    }

    public void setSubmissionStatus(String submissionStatus) {
        this.submissionStatus = submissionStatus;
    }

    public void setMemberGroupId(String memberGroupId) {
        this.memberGroupId = memberGroupId;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getMemberAddressLine1() {
        return memberAddressLine1;
    }

    public void setMemberAddressLine1(String memberAddressLine1) {
        this.memberAddressLine1 = memberAddressLine1;
    }

    public String getMemberAddressLine2() {
        return memberAddressLine2;
    }

    public void setMemberAddressLine2(String memberAddressLine2) {
        this.memberAddressLine2 = memberAddressLine2;
    }

    public String getMemberCity() {
        return memberCity;
    }

    public void setMemberCity(String memberCity) {
        this.memberCity = memberCity;
    }

    public String getMemberStateCode() {
        return memberStateCode;
    }

    public void setMemberStateCode(String memberStateCode) {
        this.memberStateCode = memberStateCode;
    }

    public String getMemberZipcode() {
		String returnValue = this.memberZipcode;
	
		// Trim the last four digits if they are all zeros.
        if ((returnValue != null) && !(returnValue.equals(""))) {
			if ((returnValue != null) && returnValue.endsWith("0000")) {
				returnValue = returnValue.substring(0, 5);
			}
		}
        return returnValue;
    }

    public void setMemberZipcode(String memberZipcode) {
        this.memberZipcode = memberZipcode;
    }

    public String getMemberPhoneNumber() {
        return memberPhoneNumber;
    }

    public void setMemberPhoneNumber(String memberPhoneNumber) {
        this.memberPhoneNumber = memberPhoneNumber;
    }
    
    public String getHpLob() {
        return hpLob;
    }

    public void setHpLob(String hpLob) {
        this.hpLob = hpLob;
    }
    
    public String getSubscriberName() {
        return subscriberName;
    }

    public void setSubscriberName(String subscriberName) {
        this.subscriberName = subscriberName;
    }
}
