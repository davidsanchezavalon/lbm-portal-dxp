/**
 * Description
 *		This file contain the controller methods for the Post Service Review Procedure Information Page.
 *
 * CHANGE History
 * 		Version 1.0
 * 			Initial version copied from avalon-prior-authorization.
 * 		Version 1.1
 *			Added changes for the initial version of Post Service Review.
 *  		Use the Service Begin Date (from Prior Auth) as the Service Date (in Post Service Review).
 *  	Version 1.2			12/12/2017
 *  		Added In Process - Intake Ready for Completion to in process test
 *  	Version 1.3			03/07/2017
 *  		Use the constant for the properties file.
 *  	Version 1.4			05/29/2018
 *  		Switch getting the descriptions from JAX-WS RI to Apache Axis2.
 *  
 */

package com.avalon.lbm.portlets.postservice.controller;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Properties;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.PortletRequest;
import javax.portlet.PortletSession;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.servlet.http.HttpServletRequest;

import org.apache.axis2.AxisFault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;

import com.avalon.lbm.portlets.postservice.model.CurrentAuthorizationDetailsFO;
import com.avalon.lbm.portlets.postservice.model.DiagnosisDisplayOrder;
import com.avalon.lbm.portlets.postservice.model.PostServiceConstants;
import com.avalon.lbm.portlets.postservice.model.ProcedureInformationFO;
import com.avalon.lbm.portlets.postservice.util.AgeCalculation;
import com.avalon.lbm.portlets.postservice.util.PostServiceUtil;
import com.avalon.lbm.portlets.servicehandler.ServiceHandler;
import com.avalon.lbm.services.DAOExceptionException;
import com.avalon.lbm.services.DAOException_Exception;
import com.avalon.lbm.services.PriorAuthHeaderServiceStub;
import com.avalon.lbm.services.PriorAuthTrialClaimsServiceStub;
import com.avalon.lbm.services.PriorAuthHeaderServiceStub.DiagnosisDTO;
import com.avalon.lbm.services.PriorAuthHeaderServiceStub.PriorAuthHeader;
import com.avalon.lbm.services.PriorAuthHeaderServiceStub.PriorAuthHeaderDTO;
import com.avalon.lbm.services.PriorAuthHeaderServiceStub.ProcedureDTO;
import com.avalon.lbm.services.PriorAuthTrialClaimsServiceStub.GetEnumShrtDescription;
import com.avalon.lbm.services.PriorAuthTrialClaimsServiceStub.GetEnumShrtDescriptionE;
import com.avalon.lbm.services.PriorAuthTrialClaimsServiceStub.GetEnumShrtDescriptionResponse;
import com.avalon.lbm.services.PriorAuthTrialClaimsServiceStub.GetEnumShrtDescriptionResponseE;
import com.avalon.lbm.services.PriorAuthTrialClaimsServiceStub.GetIcdDiagnosisCode;
import com.avalon.lbm.services.PriorAuthTrialClaimsServiceStub.GetIcdDiagnosisCodeE;
import com.avalon.lbm.services.PriorAuthTrialClaimsServiceStub.GetIcdDiagnosisCodeResponse;
import com.avalon.lbm.services.PriorAuthTrialClaimsServiceStub.GetIcdDiagnosisCodeResponseE;
import com.avalon.lbm.services.PriorAuthTrialClaimsServiceStub.TrialClaimsDiagnosisReqHdr;
import com.avalon.lbm.services.PriorAuthTrialClaimsServiceStub.TrialClaimsProcedureReqHdr;
//NEED TO CHANGE import com.avalon.lbm.services.PriorAuthTrialClaimsService;
//NEED TO CHANGE import com.avalon.lbm.services.TrialClaimsDiagnosisReqHdr;
//NEED TO CHANGE import com.avalon.lbm.services.TrialClaimsProcedureReqHdr;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.model.UserGroup;
import com.liferay.portal.kernel.service.UserGroupLocalServiceUtil;
import com.liferay.portal.kernel.service.UserLocalServiceUtil;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.StringPool;

@Controller(value = "ProcedureInformation")
@RequestMapping(value = { "VIEW" })
public class ProcedureInformation implements PostServiceConstants {
	
	private static final Log log = LogFactoryUtil.getLog(ProcedureInformation.class.getName());

    private static final int dateLen = 19;
    
    /**** Render method which gets called during initial load of JSP *************************/
    @RenderMapping
    public String handleRenderRequest(RenderRequest request,
    		                          RenderResponse response, 
    		                          Model model,
    		                          @ModelAttribute(value = "procedureInformationFO") ProcedureInformationFO procedureInformationFO) {
 
    	log.info("Processing Render Action for Procedure Information Enter");

    	HttpServletRequest httpRequest = PortalUtil.getHttpServletRequest(request);
    	String browserStr = getBrowser(httpRequest);
        PortletSession portletSession = request.getPortletSession();
        String sourceTab = PostServiceUtil.procedureTab;

        CurrentAuthorizationDetailsFO currentAuthorizationDetails = PostServiceUtil.getHeaderDetails(portletSession, sourceTab);

        request.setAttribute("enableTabs", (Object) "false");
        try {
			ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute("LIFERAY_SHARED_THEME_DISPLAY");
			Long authorizationKey = (Long) portletSession.getAttribute("authorizationKey", PortletSession.APPLICATION_SCOPE);
            PriorAuthHeaderServiceStub.PriorAuthHeaderDTO priorAuthHeaderDTO = null;
 
            HttpServletRequest httprequest = PortalUtil.getHttpServletRequest((PortletRequest) request);
            HttpServletRequest originalRequest = PortalUtil.getOriginalServletRequest((HttpServletRequest) httprequest);
            originalRequest.setAttribute("enableTabs", (Object) "true");

            PriorAuthHeaderServiceStub priorAuthHeaderServiceStub = new PriorAuthHeaderServiceStub();
            PriorAuthHeaderServiceStub.GetPriorAuthHeaderE getPriorAuthHeaderE = new PriorAuthHeaderServiceStub.GetPriorAuthHeaderE();
            PriorAuthHeaderServiceStub.GetPriorAuthHeader GetPriorAuthHeader2 = new PriorAuthHeaderServiceStub.GetPriorAuthHeader();
            PriorAuthHeaderServiceStub.PriorAuthHeader priorAuthHeader = new PriorAuthHeaderServiceStub.PriorAuthHeader();
            PriorAuthHeaderServiceStub.PriorAuthHeaderDTO authHeaderDTO = new PriorAuthHeaderServiceStub.PriorAuthHeaderDTO();
            String authorizationNumber = (String) portletSession.getAttribute("authorizationNumber", PortletSession.APPLICATION_SCOPE);
            if (authorizationNumber == null) {
// LOCAL TEST
//authorizationNumber = "12345";
                log.info("Procedure Information - RENDER: Procedure Authorization Number --> " + authorizationNumber);
                authHeaderDTO.setPriorAuthNumber(authorizationNumber);
            }
            priorAuthHeader.setPriorAuthHeaderDTO(authHeaderDTO);
            priorAuthHeader.setSourceTab(sourceTab);
// LOCAL TEST
//authorizationKey = new Long("11111");
            priorAuthHeader.setAuthorizationKey(authorizationKey.longValue());
            
            // Set the operation type
			String operationType = (String) portletSession.getAttribute("operationType", PortletSession.APPLICATION_SCOPE);
			if (operationType != null) {
				priorAuthHeader.setOperationType(operationType);
				portletSession.removeAttribute("operationType", PortletSession.APPLICATION_SCOPE);
			}
			
            priorAuthHeader.setAppCreateUserId(themeDisplay.getUser().getEmailAddress());
            priorAuthHeader.setAppMaintUserId(themeDisplay.getUser().getEmailAddress());
            GetPriorAuthHeader2.setSearchCriteriaRequest(priorAuthHeader);
            getPriorAuthHeaderE.setGetPriorAuthHeader(GetPriorAuthHeader2);
            
            PriorAuthHeaderServiceStub.GetPriorAuthHeaderResponseE getPriorAuthHeaderResponseE =null;
            PriorAuthHeaderServiceStub.GetPriorAuthHeaderResponse getPriorAuthHeaderResponse = null;
            
            //TODO: Remove mock service code and condition. Don't remove else condition code
			if(GetterUtil.getBoolean(com.liferay.portal.kernel.util.PropsUtil.get(PostServiceConstants.ENABLED_MOCK_SERVICE))){
				 //Success Mock Code
				getPriorAuthHeaderResponse = getPriorAuthHeaderResponseSuccessMock(authorizationNumber);
			}else{
				 getPriorAuthHeaderResponseE = priorAuthHeaderServiceStub.getPriorAuthHeader(getPriorAuthHeaderE);
		         getPriorAuthHeaderResponse = getPriorAuthHeaderResponseE.getGetPriorAuthHeaderResponse();
		    }
           if (getPriorAuthHeaderResponse.get_return() != null) {
                PriorAuthHeaderServiceStub.DiagnosisDTO[] diagnosis = getPriorAuthHeaderResponse.get_return().getDiagnosis();
                PriorAuthHeaderServiceStub.ProcedureDTO[] Procedures = getPriorAuthHeaderResponse.get_return().getProcedures();
                log.info("Procedure Information - RENDER: Diagnosis Rows --> " + diagnosis);
                log.info("Procedure Information - RENDER: Procedure Rows --> " + Procedures);
                ArrayList<PriorAuthHeaderServiceStub.ProcedureDTO> pdtoList = new ArrayList<PriorAuthHeaderServiceStub.ProcedureDTO>();
                if (Procedures != null) {
                    log.info("Procedure Information - RENDER: Number of procedure rows inserted from portal --> " + Procedures.length);
                    for (int i = 0; i < Procedures.length; ++i) {
                        PriorAuthHeaderServiceStub.ProcedureDTO pdt = Procedures[i];

                        log.info("Procedure Information - RENDER: From Code for " + i + " --> " + pdt.getProcedureFromCode());
                        log.info("Procedure Information - RENDER: Decision for " + i + " --> " + pdt.getProcedureDecision());
                        log.info("Procedure Information - RENDER: Decision Reason for " + i + " --> " + pdt.getProcedureDecisionReason());
                        log.info("Procedure Information - RENDER: Units for " + i + " --> " + pdt.getProcedureUnits());
                        pdtoList.add(pdt);
                    }
                    request.setAttribute("pdtoListreReq", pdtoList);
                    originalRequest.setAttribute("pdtoListreorgReq", pdtoList);
                } else {
                    request.setAttribute("pdtoListreReq", new ArrayList());
                }

                ArrayList<DiagnosisDisplayOrder> diagnosisListComaparator = new ArrayList<DiagnosisDisplayOrder>();
                if (diagnosis != null) {
                    log.info("Procedure Information - RENDER: DiagnosisDTO length --> " + diagnosis.length);
                    int i = 0;
                    while (i < diagnosis.length) {
                        PriorAuthHeaderServiceStub.DiagnosisDTO diadt = diagnosis[i];
                        DiagnosisDisplayOrder diagnosisDisplayOrder = new DiagnosisDisplayOrder();
                        diagnosisDisplayOrder.setSequenceNumber(diadt.getSequenceNumber());
                        diagnosisDisplayOrder.setDiagnosisCode(diadt.getIcdDiagnosisCode());
                        diagnosisListComaparator.add(diagnosisDisplayOrder);
                        Collections.sort(diagnosisListComaparator, new DiagnosisDisplayOrder());
                        ++i;
                    }
                    request.setAttribute("diadtoListreReq", diagnosisListComaparator);
                    originalRequest.setAttribute("diadtoListreReq", diagnosisListComaparator);
                } else {
                    request.setAttribute("diadtoListreReq", new ArrayList());
                }
            }
            String authPatientRelation = "";
            if (getPriorAuthHeaderResponse.get_return().getPriorAuthHeaderDTO() != null) {
                priorAuthHeaderDTO = getPriorAuthHeaderResponse.get_return().getPriorAuthHeaderDTO();
                authPatientRelation = priorAuthHeaderDTO.getPatientGenderRelationshipCode();
                currentAuthorizationDetails.setAuthorizationNumber(priorAuthHeaderDTO.getPriorAuthNumber());

                if (priorAuthHeaderDTO.getPriorAuthStatusDesc() != null) {
                    currentAuthorizationDetails.setAuthorizationStatus(priorAuthHeaderDTO.getPriorAuthStatusDesc());
                }
                
                // Set the Submission Status
                portletSession.setAttribute("subMissionStatus", (Object) priorAuthHeaderDTO.getPriorAuthSubmissionStatusCode(), PortletSession.APPLICATION_SCOPE);
                if (priorAuthHeaderDTO.getPriorAuthSubmissionStatusCode() != null) {
                    if (priorAuthHeaderDTO.getPriorAuthSubmissionStatusCode().equalsIgnoreCase("10")) {
                        procedureInformationFO.setAuthSubmissionStatus("Saved");
                    } else if (priorAuthHeaderDTO.getPriorAuthSubmissionStatusCode().equalsIgnoreCase("11")) {
                        procedureInformationFO.setAuthSubmissionStatus("Updated");
                    } else if (priorAuthHeaderDTO.getPriorAuthSubmissionStatusCode().equalsIgnoreCase("40")) {
                        procedureInformationFO.setAuthSubmissionStatus("Submitted");
                    } else if (priorAuthHeaderDTO.getPriorAuthSubmissionStatusCode().equalsIgnoreCase("41")) {
                        procedureInformationFO.setAuthSubmissionStatus("Updates Submitted");
                    } else if (priorAuthHeaderDTO.getPriorAuthSubmissionStatusCode().equalsIgnoreCase("50")) {
                        procedureInformationFO.setAuthSubmissionStatus("Sent");
                    } else if (priorAuthHeaderDTO.getPriorAuthSubmissionStatusCode().equalsIgnoreCase("35")) {
                        procedureInformationFO.setAuthSubmissionStatus("Void");
                        /****** Changed from Cancelled to Void ***********/
                    } else if (priorAuthHeaderDTO.getPriorAuthSubmissionStatusCode().equalsIgnoreCase("70")) {
                        procedureInformationFO.setAuthSubmissionStatus("Void HP - Submitted");
                        /****** Changed from void submitted to void HP submitted ***********/
                    } else if (priorAuthHeaderDTO.getPriorAuthSubmissionStatusCode().equalsIgnoreCase("80")) {
                        procedureInformationFO.setAuthSubmissionStatus("Void HP");
                        /****** Changed from void to void HP ***********/
                    }
                } else {
                    procedureInformationFO.setAuthSubmissionStatus("Saved");
                }
                portletSession.setAttribute("authSubmissionStatus", (Object) procedureInformationFO.getAuthSubmissionStatus(), PortletSession.APPLICATION_SCOPE);
                request.setAttribute("authSubmissionStatus", (Object) procedureInformationFO.getAuthSubmissionStatus());

                // Set the authorization age
                String appDate = priorAuthHeaderDTO.getAppCreateDatetime();
    	        portletSession.setAttribute("CreateDatetime", (Object) appDate, PortletSession.APPLICATION_SCOPE);
                currentAuthorizationDetails.setAuthorizationAge(PostServiceUtil.setAuthorizationAge(portletSession, appDate, authorizationKey, themeDisplay));
                
                // Put the creation date/time in a request attribute
                request.setAttribute("paCreationDate", appDate);

				// Set the Status
				procedureInformationFO.setAuthStatusCode(priorAuthHeaderDTO.getPriorAuthStatusCode());
				procedureInformationFO.setAuthStatusDesc(priorAuthHeaderDTO.getPriorAuthStatusDesc());

                // Set the Due Date
                String priorAuthDueDate = priorAuthHeaderDTO.getPriorAuthDueDatetime();
                procedureInformationFO.setAuthDueDate(priorAuthDueDate);

				// Set the Worked By
				String priorAuthWorkedBy = priorAuthHeaderDTO.getPriorAuthWorkedBy();
				procedureInformationFO.setAuthWorkedBy(priorAuthWorkedBy);

				// Get the Worked By popup flag from the properties file
				String workedByPopupFlag = "true";
				try {
					String liferayHome = System.getProperty("LIFERAY_HOME");
					String propertyLocation = liferayHome +StringPool.SLASH + PostServiceConstants.PROPERTIES_FILE;
					log.info("ProcedureInformation for PSR::propertyLocation: " + propertyLocation);
					Properties prop = new Properties();
					InputStream input = new FileInputStream(propertyLocation);
					prop.load(input);
					workedByPopupFlag = prop.getProperty("procedureInfo.workedByPopupFlag");
		        } catch (FileNotFoundException e) {
		        	log.error("FileNotFoundException in handleRenderRequest method",e);
		            workedByPopupFlag = "true";
		        } catch (IOException e) {
		        	log.error("IOException in handleRenderRequest method",e);
		           workedByPopupFlag = "true";
		        }
 				if (workedByPopupFlag == null) {
 					workedByPopupFlag = "true";
 				}
                request.setAttribute("workedByPopupFlag", (Object) workedByPopupFlag);

				// Set the Withdrawn Reason
				procedureInformationFO.setAuthWithdrawnReasonCode(priorAuthHeaderDTO.getPriorAuthWithdrawnReasonCode());
				procedureInformationFO.setAuthWithdrawnReasonDesc(priorAuthHeaderDTO.getPriorAuthWithdrawnReasonDesc());

                log.info("Procedure Information - RENDER: AuthInboundChannel --> " + priorAuthHeaderDTO.getAuthInboundChannel());
                log.info("Procedure Information - RENDER: AuthRequestedDate --> " + priorAuthHeaderDTO.getAuthRequestedDate());
                log.info("Procedure Information - RENDER: AppCreateUserId --> " + priorAuthHeaderDTO.getAppCreateUserId());
                log.info("Procedure Information - RENDER: AppCreateDatetime --> " + priorAuthHeaderDTO.getAppCreateDatetime());
                log.info("Procedure Information - RENDER: PriorAuthPriorityCode --> " + priorAuthHeaderDTO.getPriorAuthPriorityCode());
                log.info("Procedure Information - RENDER: PriorAuthBeginServiceDate --> " + priorAuthHeaderDTO.getPriorAuthBeginServiceDate());
                log.info("Procedure Information - RENDER: PriorAuthEndServiceDate --> " + priorAuthHeaderDTO.getPriorAuthEndServiceDate());
                log.info("Procedure Information - RENDER: PriorAuthStatusCode --> " + priorAuthHeaderDTO.getPriorAuthStatusCode());
                log.info("Procedure Information - RENDER: PriorAuthStatusDesc --> " + priorAuthHeaderDTO.getPriorAuthStatusDesc());
                log.info("Procedure Information - RENDER: PriorAuthOriginalStatusDatetime --> " + priorAuthHeaderDTO.getPriorAuthOriginalStatusDatetime());
                log.info("Procedure Information - RENDER: PriorAuthDueDatetime --> " + priorAuthHeaderDTO.getPriorAuthDueDatetime());
                log.info("Procedure Information - RENDER: PriorAuthWorkedBy --> " + priorAuthHeaderDTO.getPriorAuthWorkedBy());
                log.info("Procedure Information - RENDER: PriorAuthWithdrawnReasonCode --> " + priorAuthHeaderDTO.getPriorAuthWithdrawnReasonCode());
                log.info("Procedure Information - RENDER: PriorAuthWithdrawnReasonDesc --> " + priorAuthHeaderDTO.getPriorAuthWithdrawnReasonDesc());

                log.info("Procedure Information - RENDER:  memberFirstName --> " + priorAuthHeaderDTO.getPriorAuthPatientFirstName());
                log.info("Procedure Information - RENDER:  memberMiddleName --> " + priorAuthHeaderDTO.getPriorAuthPatientMiddleName());
                log.info("Procedure Information - RENDER:  memberLastName --> " + priorAuthHeaderDTO.getPriorAuthPatientLastName());
                log.info("Procedure Information - RENDER:  memberDob --> " + priorAuthHeaderDTO.getPriorAuthPatientBirthDate());
                log.info("Procedure Information - RENDER:  memberGender --> " + priorAuthHeaderDTO.getPriorAuthPatientGenderCode());
                log.info("Procedure Information - RENDER:  businessSectorCode --> " + priorAuthHeaderDTO.getPriorAuthBusinessSectorCode());
                log.info("Procedure Information - RENDER:  businessSectorDescription --> " + priorAuthHeaderDTO.getPriorAuthBusinessSectorDescription());
                log.info("Procedure Information - RENDER:  businessSegmentCode --> " + priorAuthHeaderDTO.getPriorAuthBusinessSegmentCode());
                log.info("Procedure Information - RENDER:  businessSegmentDescription --> " + priorAuthHeaderDTO.getPriorAuthBusinessSegmentDescription());

                if (priorAuthHeaderDTO.getPriorAuthStatusCode() != null) {
                    procedureInformationFO.setAuthStatusDesc(priorAuthHeaderDTO.getPriorAuthStatusDesc());
                } else {
                    procedureInformationFO.setAuthStatusDesc(PostServiceConstants.IN_PROCESS_INTAKE_REVIEW_DESC);
                }
                procedureInformationFO.setAuthRecordCreator(priorAuthHeaderDTO.getAppCreateUserId());
                String appCreateDateTime = priorAuthHeaderDTO.getAppCreateDatetime();
                if (appCreateDateTime != null && appCreateDateTime != "" && !appCreateDateTime.equalsIgnoreCase("null")) {
                    procedureInformationFO.setAuthCreatedDate(PostServiceUtil.getPriorAuthUtil().getAppCreateDateSaveFormat(priorAuthHeaderDTO.getAppCreateDatetime()));
                    procedureInformationFO.setAuthCreatedDate(PostServiceUtil.getPriorAuthUtil().getStandardDateTime(priorAuthHeaderDTO.getAppCreateDatetime()));
                }
                procedureInformationFO.setAuthInboundChannel(priorAuthHeaderDTO.getAuthInboundChannel());
            	
            	// Use the Service Begin Date as the Service Date
                String serviceBeginDate = priorAuthHeaderDTO.getPriorAuthBeginServiceDate();
                if (serviceBeginDate != null && serviceBeginDate != "" && !serviceBeginDate.equalsIgnoreCase("null")) {
                    procedureInformationFO.setServiceDate(PostServiceUtil.getPriorAuthUtil().getProcedureResBeginDate(priorAuthHeaderDTO.getPriorAuthBeginServiceDate(), browserStr));
                    
                    // Make sure the requested date is not "null".  If it is you will get a ParseException for an Unparseable date
                    String requestedDate = priorAuthHeaderDTO.getAuthRequestedDate();
                    if (!requestedDate.equalsIgnoreCase("null")) {
	                    procedureInformationFO.setAuthRequestedDate(PostServiceUtil.getPriorAuthUtil().getProcedureResDate(priorAuthHeaderDTO.getAuthRequestedDate(), browserStr));
	                    procedureInformationFO.setAuthRequestedTime(PostServiceUtil.getPriorAuthUtil().getProcedureResTime(priorAuthHeaderDTO.getAuthRequestedDate()));
                    } else {
                    	procedureInformationFO.setAuthRequestedDate("");
                    	procedureInformationFO.setAuthRequestedTime("");
                    }
                } else {
                    SimpleDateFormat dateFormat = null;
        			if (browserStr.equals("IE") || browserStr.equals("Firefox") || browserStr.equals("unknown")) {
        				dateFormat = new SimpleDateFormat("MM/dd/yyyy");
        			} else {
        				dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        			}
                    Date date = new Date();
                    procedureInformationFO.setAuthRequestedDate(dateFormat.format(date));
                    procedureInformationFO.setAuthRequestedTime("");
                    procedureInformationFO.setServiceDate(dateFormat.format(date));
                }

                procedureInformationFO.setAuthorizationStatus(priorAuthHeaderDTO.getPriorAuthStatusDesc());
                portletSession.setAttribute("authorizationStatus", (Object) priorAuthHeaderDTO.getPriorAuthStatusDesc(), PortletSession.APPLICATION_SCOPE);
                portletSession.setAttribute("originalUserId", (Object) priorAuthHeaderDTO.getAppCreateUserId(), PortletSession.APPLICATION_SCOPE);
                request.setAttribute("authSubmissionStatus", (Object) procedureInformationFO.getAuthSubmissionStatus());
                request.setAttribute("recordCreator", (Object) procedureInformationFO.getAuthRecordCreator());
                String authDecisionDate = priorAuthHeaderDTO.getPriorAuthOriginalStatusDatetime();
                log.info("Procedure Information - RENDER: Date on which authorization decision is changed --> " + authDecisionDate);
                if (authDecisionDate.equals("1900-01-01 00:00:00.0") || authDecisionDate == null) {
                    procedureInformationFO.setAuthDecisionDate(null);
                    portletSession.setAttribute("authDecisionDate", (Object) null, PortletSession.APPLICATION_SCOPE);
                }
                if (authDecisionDate != null && authDecisionDate != "" && !authDecisionDate.equalsIgnoreCase("null")) {
                    procedureInformationFO.setAuthDecisionDate(PostServiceUtil.getPriorAuthUtil().getStandardDecisionDateTime(authDecisionDate.substring(0, dateLen)));
                    procedureInformationFO.setAuthDecisionTime(PostServiceUtil.getPriorAuthUtil().getProcedureDecisionTime(procedureInformationFO.getAuthDecisionDate()));
                    procedureInformationFO.setAuthDecisionDate(PostServiceUtil.getPriorAuthUtil().getProcedureDecisionDate(procedureInformationFO.getAuthDecisionDate(), browserStr));
                    log.info("Procedure Information - RENDER: AuthDecisionDate --> " + procedureInformationFO.getAuthDecisionDate());
                    log.info("Procedure Information - RENDER: AuthDecisionTime --> " + procedureInformationFO.getAuthDecisionTime());
                    portletSession.setAttribute("authDecisionDate", (Object) authDecisionDate, PortletSession.APPLICATION_SCOPE);
                }
                
                // Set the Due Date and Time
                String priorAuthDueDatetime = priorAuthHeaderDTO.getPriorAuthDueDatetime();
                if ((priorAuthDueDatetime != null) && priorAuthDueDatetime.equals("null")) {
                	procedureInformationFO.setAuthDueDate(null);
                	procedureInformationFO.setAuthDueTime(null);
                } else {
	                procedureInformationFO.setAuthDueDate(PostServiceUtil.getPriorAuthUtil().getProcedureResDate(priorAuthDueDatetime, browserStr));
	                procedureInformationFO.setAuthDueTime(PostServiceUtil.getPriorAuthUtil().getProcedureResTime(priorAuthDueDatetime));
                }

                String authStatus = (String) portletSession.getAttribute("providerAuthStatus", PortletSession.APPLICATION_SCOPE);
                if (authStatus != null) {
                    log.info("Procedure Information - RENDER: Authorization Status in process action --> " + authStatus);
                    procedureInformationFO.setAuthorizationStatus(authStatus);
                } else {
                    procedureInformationFO.setAuthorizationStatus(priorAuthHeaderDTO.getPriorAuthStatusDesc());
                    portletSession.setAttribute("providerAuthStatus", (Object) priorAuthHeaderDTO.getPriorAuthStatusDesc(), PortletSession.APPLICATION_SCOPE);
                }

	            // Get a list of the users in the PA Team.  An exception in this block should not stop the remaining code from executing.
	            List<String> userList = null;
	            try {
	            	userList = new ArrayList<String>();
	             	
	            	User currentUser = PortalUtil.getUser(request);
	            	long companyId = currentUser.getCompanyId();
	            	
	            	// Get the users assigned to a user group
	            	UserGroup userGroup = UserGroupLocalServiceUtil.getUserGroup(companyId, "PATeam");
	            	long userGroupId = userGroup.getUserGroupId();

	            	List<User> users = UserLocalServiceUtil.getUserGroupUsers(userGroupId);
	            	for(User usr:users) {
	            	    String nextName = usr.getFullName();
	            		userList.add(nextName);
	            	}
	            } catch (SystemException e) {
	            	log.error("IOException in handleRenderRequest method during get userlist",e);
	            } catch (PortalException e) {
	            	log.error("IOException in handleRenderRequest method during get userlist",e);
	            }
	        	
	            if ((priorAuthHeaderDTO.getAuthRequestedDate() != null) && !priorAuthHeaderDTO.getAuthRequestedDate().equalsIgnoreCase("null") && (procedureInformationFO.getAuthDueDate() == null)) {

	            	// Convert the date to the IE format and add 30 business days
	            	String requestDate = priorAuthHeaderDTO.getAuthRequestedDate();
	    			if (browserStr.equals("IE") || browserStr.equals("Firefox") || browserStr.equals("unknown")) {
	    				requestDate = PostServiceUtil.convertDateTime(priorAuthHeaderDTO.getAuthRequestedDate());
	    			}
	            	// Set the due date to the request date plus 30 business days
		            String newDueDate = PostServiceUtil.addBusinessDays(requestDate, 30, browserStr);
		            
					procedureInformationFO.setAuthDueDate(newDueDate);
					procedureInformationFO.setAuthDueTime("08:00 PM");
	            }

                portletSession.setAttribute("RequestedDatetime", (Object) priorAuthHeaderDTO.getAuthRequestedDate(), PortletSession.APPLICATION_SCOPE);
                portletSession.setAttribute("authorizationStatus", (Object) priorAuthHeaderDTO.getPriorAuthStatusDesc(), PortletSession.APPLICATION_SCOPE);
                request.setAttribute("userList", userList);
                request.setAttribute("procedureInformationFO", (Object) procedureInformationFO);
                request.setAttribute("enableTabs", (Object) "true");
                request.setAttribute("currentAuthorizationDetailsFO", (Object) currentAuthorizationDetails);
                request.setAttribute("authPatientRelation", (Object) authPatientRelation);

                log.info("Processing Render Action for Procedure Information Exit");
            }
        } catch (AxisFault e) {
        	log.error("AxisFault in handleRenderRequest method",(Throwable) e);
        } catch (RemoteException e) {
        	log.error("RemoteException in handleRenderRequest method",(Throwable) e);
        } catch (DAOExceptionException e) {
        	log.error("DAOExceptionException in handleRenderRequest method",(Throwable) e);
        }
        return "view";
    }
    
    /**************Action method which gets called on click of SAVE button***************************************/
    @ActionMapping(params = { "action=procedureInformationAction" })
    public void procedureInformationDetails(ActionRequest request, 
    		                                ActionResponse response, 
    		                                @ModelAttribute(value = "procedureInformationFO") ProcedureInformationFO procedureInformationFO) {

    	log.info("Processing SAVE Action for Procedure Information Enter");

        String authDecisionDate = null;

    	HttpServletRequest httpRequest = PortalUtil.getHttpServletRequest(request);
        String sourceTab = PostServiceUtil.procedureTab;
        PortletSession portletSession = request.getPortletSession();
    	String browserStr = getBrowser(httpRequest);

        portletSession.removeAttribute("providerAuthStatus", PortletSession.APPLICATION_SCOPE);
        String originalDecisionDate = "";
        String appCreateDateTime = "";
        ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute("LIFERAY_SHARED_THEME_DISPLAY");
        try {
            String authPatientRelation = null;
            
			procedureInformationFO.setAuthStatusCode(ParamUtil.getString((PortletRequest) request, (String) ("authStatus")));
			procedureInformationFO.setAuthWithdrawnReasonCode(ParamUtil.getString((PortletRequest) request, (String) ("authWithdrawnReason")));
			
			log.info("Procedure Information - SAVE: Auth Status Code --> " + procedureInformationFO.getAuthStatusCode());
			log.info("Procedure Information - SAVE: Requested Date --> " + procedureInformationFO.getAuthRequestedDate());
            log.info("Procedure Information - SAVE: Requested Time --> " + procedureInformationFO.getAuthRequestedTime());
            log.info("Procedure Information - SAVE: Service Date --> " + procedureInformationFO.getServiceDate());
            log.info("Procedure Information - SAVE: Due Date --> " + procedureInformationFO.getAuthDueDate());
            log.info("Procedure Information - SAVE: Due Time --> " + procedureInformationFO.getAuthDueTime());
            log.info("Procedure Information - SAVE: Worked By --> " + procedureInformationFO.getAuthWorkedBy());
            log.info("Procedure Information - SAVE: Withdrawn Reason Code --> " + procedureInformationFO.getAuthWithdrawnReasonCode());

            String memberId = (String) portletSession.getAttribute("memberId", PortletSession.APPLICATION_SCOPE);
            String memberNumber = (String) portletSession.getAttribute("memberNumber", PortletSession.APPLICATION_SCOPE);
            String healthPlan = (String) portletSession.getAttribute("healthPlan", PortletSession.APPLICATION_SCOPE);
            String mpi = (String) portletSession.getAttribute("mpi", PortletSession.APPLICATION_SCOPE);

            PriorAuthHeaderServiceStub priorAuthHeaderServiceStub = new PriorAuthHeaderServiceStub();
            PriorAuthHeaderServiceStub.SavePriorAuthHeaderE savePriorAuthHeader = new PriorAuthHeaderServiceStub.SavePriorAuthHeaderE();
            PriorAuthHeaderServiceStub.SavePriorAuthHeader savePriorAuthHead = new PriorAuthHeaderServiceStub.SavePriorAuthHeader();
            PriorAuthHeaderServiceStub.PriorAuthHeader priorAuthHeader = new PriorAuthHeaderServiceStub.PriorAuthHeader();
            priorAuthHeader.setAppCreateUserId(themeDisplay.getUser().getEmailAddress());
            priorAuthHeader.setAppMaintUserId(themeDisplay.getUser().getEmailAddress());
            LinkedHashMap procedureDetails = new LinkedHashMap();
            LinkedHashMap procedureToCodeDetails = new LinkedHashMap();
            LinkedHashMap procedureDecisionDetails = new LinkedHashMap();
            LinkedHashMap procedureReasonDetails = new LinkedHashMap();
            LinkedHashMap procedureUnitsDetails = new LinkedHashMap();
            LinkedHashMap<Integer, String> diagnosisCodeAdditional = new LinkedHashMap<Integer, String>();
            ArrayList<PriorAuthHeaderServiceStub.ProcedureDTO> procedureList = new ArrayList<PriorAuthHeaderServiceStub.ProcedureDTO>();
            ArrayList<PriorAuthHeaderServiceStub.DiagnosisDTO> diagnosisDTOList = new ArrayList<PriorAuthHeaderServiceStub.DiagnosisDTO>();
            PriorAuthHeaderServiceStub.DiagnosisDTO primaryDiagnosis = new PriorAuthHeaderServiceStub.DiagnosisDTO();
            primaryDiagnosis.setIcdDiagnosisCode(procedureInformationFO.getDiagnosisCodePrimary());
            log.info("Procedure Information - SAVE: Primary Diagnosis Code --> " + primaryDiagnosis.getIcdDiagnosisCode());
            primaryDiagnosis.setSequenceNumber(1);
            diagnosisDTOList.add(primaryDiagnosis);
            for (int j = 1; j <= 11; ++j) {
                request.setAttribute("pdtoListreReq", new ArrayList());
                PriorAuthHeaderServiceStub.DiagnosisDTO diagnosisDTO = new PriorAuthHeaderServiceStub.DiagnosisDTO();
                String additionalFields = ParamUtil.getString((PortletRequest) request, (String) ("diagnosisCodeAdditional" + j));
                if (additionalFields != null && !additionalFields.trim().isEmpty()) {
                    diagnosisDTO.setIcdDiagnosisCode(additionalFields);
                    diagnosisDTO.setSequenceNumber(j + 1);
                    diagnosisDTOList.add(diagnosisDTO);
                }
                diagnosisCodeAdditional.put(j, ParamUtil.getString((PortletRequest) request, (String) ("diagnosisCodeAdditional" + j)));
                log.info("Procedure Information - SAVE: Additional Diagnosis Code " + j + " --> " + (String) diagnosisCodeAdditional.get(j));
            }
            for (int i = 1; i <= 25; ++i) {
                request.setAttribute("pdtoListreReq", new ArrayList());
                PriorAuthHeaderServiceStub.ProcedureDTO procedureDTO = new PriorAuthHeaderServiceStub.ProcedureDTO();
                String units = ParamUtil.getString((PortletRequest) request, (String) ("procedureUnits" + i));
                String procedureFromCode = ParamUtil.getString((PortletRequest) request, (String) ("procedureFromCode" + i));
                String procedureToCode = ParamUtil.getString((PortletRequest) request, (String) ("procedureToCode" + i));
                String procedureDecision = ParamUtil.getString((PortletRequest) request, (String) ("procedureDecision" + i));
                String procedureReason = ParamUtil.getString((PortletRequest) request, (String) ("procedureDecisionReason" + i));
                if (!(units.equals("") || procedureReason.equals("") || procedureFromCode.equals("") || procedureDecision.equals(""))) {
                    procedureDTO.setProcedureFromCode(procedureFromCode);
                    if (!procedureToCode.equals("")) {
                        procedureDTO.setProcedureToCode(procedureToCode);
                    } else {
                        procedureDTO.setProcedureToCode(null);
                    }
                    procedureDTO.setProcedureDecision(procedureDecision);
                    procedureDTO.setProcedureDecisionReason(procedureReason);
                    procedureDTO.setProcedureUnits(Float.parseFloat(units));
                    procedureList.add(procedureDTO);
                }
                log.info("Procedure Information - SAVE: procedure From Code " + i + " --> " + (String) procedureDetails.get(i));
                log.info("Procedure Information - SAVE: procedure To Code " + i + " --> " + (String) procedureToCodeDetails.get(i));
                log.info("Procedure Information - SAVE: procedure Decision " + i + " --> " + (String) procedureDecisionDetails.get(i));
                log.info("Procedure Information - SAVE: procedure Reason " + i + " --> " + (String) procedureReasonDetails.get(i));
                log.info("Procedure Information - SAVE: procedure Units " + i + " --> " + (String) procedureUnitsDetails.get(i));
            }
            log.info("Procedure Information - SAVE: Procedure List Size --> " + procedureList);
            priorAuthHeader.setProcedures(procedureList.toArray(new PriorAuthHeaderServiceStub.ProcedureDTO[procedureList.size()]));
            priorAuthHeader.setDiagnosis(diagnosisDTOList.toArray(new PriorAuthHeaderServiceStub.DiagnosisDTO[diagnosisDTOList.size()]));
            String authorizationNumber = (String) portletSession.getAttribute("authorizationNumber", PortletSession.APPLICATION_SCOPE);
            
			String memberFirstName = (String) portletSession.getAttribute("memberFirstName", PortletSession.APPLICATION_SCOPE);
			String memberMiddleName = (String) portletSession.getAttribute("memberMiddleName", PortletSession.APPLICATION_SCOPE);
			String memberLastName = (String) portletSession.getAttribute("memberLastName", PortletSession.APPLICATION_SCOPE);
			String memberSuffixName = (String) portletSession.getAttribute("memberSuffixName", PortletSession.APPLICATION_SCOPE);
			String memberDob = (String) portletSession.getAttribute("memberDob", PortletSession.APPLICATION_SCOPE);
			String memberGender = (String) portletSession.getAttribute("memberGender", PortletSession.APPLICATION_SCOPE);
			String memberGenderCode = "U";
            if ((memberGender != null) && !(memberGender.equals(""))) {
				memberGender.substring(0, 1);
            }
			String businessSectorCode = (String) portletSession.getAttribute("businessSectorCode", PortletSession.APPLICATION_SCOPE);
			String businessSectorDescription = (String) portletSession.getAttribute("businessSectorDescription", PortletSession.APPLICATION_SCOPE);
			String businessSegmentCode = (String) portletSession.getAttribute("businessSegmentCode", PortletSession.APPLICATION_SCOPE);
			String businessSegmentDescription = (String) portletSession.getAttribute("businessSegmentDescription", PortletSession.APPLICATION_SCOPE);
            log.info("Procedure Information - SAVE: memberFirstName is " + memberFirstName);
            log.info("Procedure Information - SAVE: memberMiddleName is " + memberMiddleName);
            log.info("Procedure Information - SAVE: memberLastName is " + memberLastName);
            log.info("Procedure Information - SAVE: memberSuffixName is " + memberSuffixName);
            log.info("Procedure Information - SAVE: memberDob is " + memberDob);
            log.info("Procedure Information - SAVE: memberGenderCode is " + memberGenderCode);
            log.info("Procedure Information - SAVE: businessSectorCode is " + businessSectorCode);
            log.info("Procedure Information - SAVE: businessSectorDescription is " + businessSectorDescription);
            log.info("Procedure Information - SAVE: businessSegmentCode is " + businessSegmentCode);
            log.info("Procedure Information - SAVE: businessSegmentDescription is " + businessSegmentDescription);

			PriorAuthHeaderServiceStub.PriorAuthHeaderDTO priorAuthHeaderDTO = new PriorAuthHeaderServiceStub.PriorAuthHeaderDTO();
            if (authorizationNumber != null && authorizationNumber != "") {
                priorAuthHeaderDTO.setPriorAuthNumber(authorizationNumber);
            }

			// Set the member data returned from Member Lab Benefits
			priorAuthHeaderDTO.setPriorAuthPatientFirstName(memberFirstName);
			priorAuthHeaderDTO.setPriorAuthPatientMiddleName(memberMiddleName);
			priorAuthHeaderDTO.setPriorAuthPatientLastName(memberLastName);
			priorAuthHeaderDTO.setPriorAuthPatientSuffixName(memberSuffixName);
			priorAuthHeaderDTO.setPriorAuthPatientBirthDate(memberDob);
			priorAuthHeaderDTO.setPriorAuthPatientGenderCode(memberGenderCode);
			
			// Set the Line of Business data returned from Member Lab Benefits
			priorAuthHeaderDTO.setPriorAuthBusinessSectorCode(businessSectorCode);
			priorAuthHeaderDTO.setPriorAuthBusinessSectorDescription(businessSectorDescription);
			priorAuthHeaderDTO.setPriorAuthBusinessSegmentCode(businessSegmentCode);
			priorAuthHeaderDTO.setPriorAuthBusinessSegmentDescription(businessSegmentDescription);
			
			// Convert the date to the format needed to save 
            String authRequestedDate = procedureInformationFO.getAuthRequestedDate(); 
            authRequestedDate = PostServiceUtil.getPriorAuthUtil().setProcedureResDate(authRequestedDate, browserStr);
            String serviceDate = procedureInformationFO.getServiceDate();
            
            priorAuthHeaderDTO.setAuthInboundChannel(procedureInformationFO.getAuthInboundChannel());
            log.info("Procedure Information - SAVE: Authorization Request Date --> " + procedureInformationFO.getAuthRequestedDate() + " " + procedureInformationFO.getAuthRequestedTime());

	        priorAuthHeaderDTO.setAppCreateUserId(themeDisplay.getUser().getEmailAddress());
            priorAuthHeader.setAppMaintUserId(themeDisplay.getUser().getEmailAddress());
            appCreateDateTime = PostServiceUtil.getPriorAuthUtil().getOriginalDate();
            log.info("Procedure Information - SAVE: appCreateDateTime Value --> " + appCreateDateTime);
            priorAuthHeaderDTO.setPatientGenderRelationshipCode("0");
            priorAuthHeaderDTO.setPriorAuthSubmissionStatusCode("10");
            priorAuthHeaderDTO.setIdCardNumber(memberId);
            priorAuthHeaderDTO.setMemberNumber(memberNumber);
            priorAuthHeaderDTO.setMasterPatientId(mpi);
            priorAuthHeaderDTO.setHealthPlanGroupId(healthPlan);
            priorAuthHeaderDTO.setHealthPlanId("01");
            log.info("Procedure Information - SAVE: Requested Date --> " + authRequestedDate);
            log.info("Procedure Information - SAVE: Service Date --> " + serviceDate);
            priorAuthHeaderDTO.setAuthRequestedDate(PostServiceUtil.getPriorAuthUtil().getAuthRequestedDate(authRequestedDate + " " + procedureInformationFO.getAuthRequestedTime()));
            
            // Set the Service Begin Date and Service End Date to the Service Date
            priorAuthHeaderDTO.setPriorAuthBeginServiceDate(PostServiceUtil.getPriorAuthUtil().getProcedureReqBeginDate(serviceDate, browserStr));
            priorAuthHeaderDTO.setPriorAuthEndServiceDate(priorAuthHeaderDTO.getPriorAuthBeginServiceDate());
            authDecisionDate = (String) portletSession.getAttribute("authDecisionDate", PortletSession.APPLICATION_SCOPE);
            log.info("Procedure Information - SAVE: Authorization Decision Date --> " + authDecisionDate);
            String originalCreatedUser = (String) portletSession.getAttribute("originalUserId", PortletSession.APPLICATION_SCOPE);
			boolean inprocessFlag = this.isInProcess(procedureInformationFO.getAuthStatusCode());
            if (!inprocessFlag && authDecisionDate != null) {
                priorAuthHeaderDTO.setPriorAuthOriginalStatusUserId(originalCreatedUser);
                priorAuthHeaderDTO.setPriorAuthCurrentStatusUserId(themeDisplay.getUser().getEmailAddress());
                priorAuthHeaderDTO.setPriorAuthCurrentStatusDatetime(PostServiceUtil.getPriorAuthUtil().getOriginalDate());
                priorAuthHeaderDTO.setPriorAuthStatusCode(procedureInformationFO.getAuthStatusCode());
                priorAuthHeaderDTO.setPriorAuthStatusDesc(null);
				priorAuthHeaderDTO.setPriorAuthOriginalStatusDatetime(originalDecisionDate);
            }
            if (!inprocessFlag && authDecisionDate == null) {
                priorAuthHeaderDTO.setPriorAuthOriginalStatusUserId(themeDisplay.getUser().getEmailAddress());
                priorAuthHeaderDTO.setPriorAuthCurrentStatusUserId(themeDisplay.getUser().getEmailAddress());
                priorAuthHeaderDTO.setPriorAuthCurrentStatusDatetime(PostServiceUtil.getPriorAuthUtil().getOriginalDate());
                priorAuthHeaderDTO.setPriorAuthStatusCode(procedureInformationFO.getAuthStatusCode());
                priorAuthHeaderDTO.setPriorAuthStatusDesc(null);
                originalDecisionDate = PostServiceUtil.getPriorAuthUtil().getOriginalDate();
                priorAuthHeaderDTO.setPriorAuthOriginalStatusDatetime(originalDecisionDate);
            }
            if (inprocessFlag) {
                priorAuthHeaderDTO.setPriorAuthOriginalStatusUserId(null);
                priorAuthHeaderDTO.setPriorAuthCurrentStatusUserId(null);
                priorAuthHeaderDTO.setPriorAuthCurrentStatusDatetime(null);
                
                // Save the In Process Auth Status
                priorAuthHeaderDTO.setPriorAuthStatusCode(procedureInformationFO.getAuthStatusCode());
                priorAuthHeaderDTO.setPriorAuthStatusDesc(null);
                priorAuthHeaderDTO.setPriorAuthOriginalStatusDatetime(null);
            } else {

            	// Do not change the Original status date/time for a withdrawn status
            	if (!procedureInformationFO.getAuthStatusCode().equals(PostServiceConstants.COMPLETED_WITHDRAWN_CODE)) {
                    
                    // Save the In Process Auth Status
                    priorAuthHeaderDTO.setPriorAuthStatusCode(procedureInformationFO.getAuthStatusCode());
                    priorAuthHeaderDTO.setPriorAuthStatusDesc(null);

            		// Save any new Original status date/time
					String decisionDate = procedureInformationFO.getAuthDecisionDate();
					String decisionTime = procedureInformationFO.getAuthDecisionTime();
					if (!decisionDate.equals("") && !decisionTime.equals("") && !decisionTime.equals("hh:mm AM/PM")) {
						String newAuthOriginalStatusDatetime = decisionDate + " " + decisionTime;
	
						newAuthOriginalStatusDatetime = PostServiceUtil.getPriorAuthUtil().getDecisionDateTimeSaveFormat(newAuthOriginalStatusDatetime, browserStr);
						log.info("Procedure Information - SAVE: New AuthOriginalStatusDatetime --> " + newAuthOriginalStatusDatetime);
						priorAuthHeaderDTO.setPriorAuthOriginalStatusDatetime(newAuthOriginalStatusDatetime);
					} else {
						if (authDecisionDate == null) {
							
							// Use the current date/time for a new prior auth
							Date today = new Date();
							SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
							authDecisionDate = df.format(today);
						}
						log.info("Procedure Information - SAVE: New AuthOriginalStatusDatetime --> " + authDecisionDate);
						priorAuthHeaderDTO.setPriorAuthOriginalStatusDatetime(PostServiceUtil.getPriorAuthUtil().getSavedDecisionDate(authDecisionDate));
					}
            	}
            }

            // Save the Due Date/Time
            String dueDate = procedureInformationFO.getAuthDueDate();
            String dueTime = procedureInformationFO.getAuthDueTime();
			if (!dueDate.equals("") && !dueTime.equals("") && !dueTime.equals("hh:mm AM/PM")) {
				String newDueDatetime = dueDate + " " + dueTime;

				newDueDatetime = PostServiceUtil.getPriorAuthUtil().getDecisionDateTimeSaveFormat(newDueDatetime, browserStr);
				priorAuthHeaderDTO.setPriorAuthDueDatetime(newDueDatetime);
				log.info("Procedure Information - SAVE: Due Datetime --> " + newDueDatetime);
			}
            
            // Save the Worked By
			String workedBy = procedureInformationFO.getAuthWorkedBy();
            priorAuthHeaderDTO.setPriorAuthWorkedBy(workedBy);
			log.info("Procedure Information - SAVE: Worked By --> " + workedBy);
            
            // Save the Withdrawn Status Code
			String withdrawnReasonCode = procedureInformationFO.getAuthWithdrawnReasonCode();
            priorAuthHeaderDTO.setPriorAuthWithdrawnReasonCode(withdrawnReasonCode);
			log.info("Withdrawn Status --> " + withdrawnReasonCode);
            
            if (!(authPatientRelation = ParamUtil.getString((PortletRequest) request, (String) "authPatientRelation")).equalsIgnoreCase("")) {
                priorAuthHeaderDTO.setPatientGenderRelationshipCode(authPatientRelation);
            } else {
                priorAuthHeaderDTO.setPatientGenderRelationshipCode(null);
            }
            String authSubmissionStatus = ParamUtil.getString((PortletRequest) request, (String) "authSubmissionStatus");
        		
    		// Set the not void submission status
            if (authSubmissionStatus.equalsIgnoreCase("Saved")) {
                priorAuthHeaderDTO.setPriorAuthSubmissionStatusCode("10");
                portletSession.setAttribute("authSubmissionStatus", (Object) "Saved", PortletSession.APPLICATION_SCOPE);
            } else if (authSubmissionStatus.equalsIgnoreCase("Sent") || authSubmissionStatus.equalsIgnoreCase("Updated")) {
                priorAuthHeaderDTO.setPriorAuthSubmissionStatusCode("11");
                portletSession.setAttribute("authSubmissionStatus", (Object) "Updated", PortletSession.APPLICATION_SCOPE);
            } else if (authSubmissionStatus.equalsIgnoreCase("Submitted")) {
                priorAuthHeaderDTO.setPriorAuthSubmissionStatusCode("40");
                portletSession.setAttribute("authSubmissionStatus", (Object) "Submitted", PortletSession.APPLICATION_SCOPE);
            } else if (authSubmissionStatus.equalsIgnoreCase("Updates Submitted")) {
                priorAuthHeaderDTO.setPriorAuthSubmissionStatusCode("41");
                portletSession.setAttribute("authSubmissionStatus", (Object) "Updates Submitted", PortletSession.APPLICATION_SCOPE);
            }
            log.info("Procedure Information - SAVE: Submission Status --> " + authSubmissionStatus);

            portletSession.setAttribute("authorizationStatus", (Object) procedureInformationFO.getAuthStatusDesc(), PortletSession.APPLICATION_SCOPE);
            priorAuthHeader.setPriorAuthHeaderDTO(priorAuthHeaderDTO);
            Long authorizationKey = (Long) portletSession.getAttribute("authorizationKey", PortletSession.APPLICATION_SCOPE);
            log.info("Authorization Key : " + authorizationKey);
            if (authorizationKey != null) {
                priorAuthHeader.setAuthorizationKey(authorizationKey.longValue());
            }
            priorAuthHeader.setSourceTab(sourceTab);
            savePriorAuthHead.setSavePriorAuthHeaderRequest(priorAuthHeader);
            savePriorAuthHeader.setSavePriorAuthHeader(savePriorAuthHead);
            try {
            	PriorAuthHeaderServiceStub.SavePriorAuthHeaderResponseE savePriorAuthHeaderResponseE =null;
                PriorAuthHeaderServiceStub.SavePriorAuthHeaderResponse savePriorAuthHeaderResponse =null;
                //TODO: Remove mock service code and condition. Don't remove else condition code
    			if(GetterUtil.getBoolean(com.liferay.portal.kernel.util.PropsUtil.get(PostServiceConstants.ENABLED_MOCK_SERVICE))){
    				
    				savePriorAuthHeaderResponse = savePriorAuthHeaderResponseSuccessMock();
    			}else{
    				savePriorAuthHeaderResponseE = priorAuthHeaderServiceStub.savePriorAuthHeader(savePriorAuthHeader);
                    savePriorAuthHeaderResponse = savePriorAuthHeaderResponseE.getSavePriorAuthHeaderResponse();
                }
                PriorAuthHeaderServiceStub.PriorAuthHeader priorAuthHeaderResponse = savePriorAuthHeaderResponse.get_return();
                if (priorAuthHeaderResponse != null) {
                    procedureInformationFO.setAuthRecordCreator(priorAuthHeaderResponse.getPriorAuthHeaderDTO().getAppCreateUserId());
                    String appCreateDatetime = priorAuthHeaderResponse.getPriorAuthHeaderDTO().getAppCreateDatetime();
                    log.info("appCreateDatetime value: " + appCreateDatetime);
                    procedureInformationFO.setAuthInboundChannel(priorAuthHeaderResponse.getPriorAuthHeaderDTO().getAuthInboundChannel());
                    authDecisionDate = priorAuthHeaderResponse.getPriorAuthHeaderDTO().getPriorAuthOriginalStatusDatetime();
                 
                    if (authDecisionDate != null && authDecisionDate != "" && !authDecisionDate.equalsIgnoreCase("null")) {
                        procedureInformationFO.setAuthDecisionDate(authDecisionDate.substring(0, dateLen));
                        procedureInformationFO.setAuthDecisionTime(PostServiceUtil.getPriorAuthUtil().getProcedureDecisionTime(procedureInformationFO.getAuthDecisionDate()));
                        procedureInformationFO.setAuthDecisionDate(PostServiceUtil.getPriorAuthUtil().getProcedureDecisionDate(procedureInformationFO.getAuthDecisionDate(), browserStr));
                        log.info("Procedure Information - SAVE: AuthDecisionDate --> " + procedureInformationFO.getAuthDecisionDate());
                        log.info("Procedure Information - SAVE: AuthDecisionTime --> " + procedureInformationFO.getAuthDecisionTime());
                    } else {
                        procedureInformationFO.setAuthDecisionDate(" ");
                        procedureInformationFO.setAuthDecisionTime(" ");
                    }                   
                    procedureInformationFO.setAuthStatusDesc(priorAuthHeaderResponse.getPriorAuthHeaderDTO().getPriorAuthStatusDesc());
                    
                    // Use the Service Begin Date as the Service Date
                    procedureInformationFO.setServiceDate(PostServiceUtil.getPriorAuthUtil().getServiceDates(priorAuthHeaderResponse.getPriorAuthHeaderDTO().getPriorAuthBeginServiceDate(), browserStr));
                    procedureInformationFO.setAuthStatusDesc(priorAuthHeaderResponse.getPriorAuthHeaderDTO().getPriorAuthStatusCode());

                    PriorAuthHeaderServiceStub.GetPriorAuthHeaderE getPriorAuthHeaderE = new PriorAuthHeaderServiceStub.GetPriorAuthHeaderE();
                    PriorAuthHeaderServiceStub.GetPriorAuthHeader GetPriorAuthHeader2 = new PriorAuthHeaderServiceStub.GetPriorAuthHeader();
                    PriorAuthHeaderServiceStub.PriorAuthHeader priorAuthHeaderGet = new PriorAuthHeaderServiceStub.PriorAuthHeader();
                    PriorAuthHeaderServiceStub.PriorAuthHeaderDTO authHeaderDTO = new PriorAuthHeaderServiceStub.PriorAuthHeaderDTO();
                    authorizationNumber = (String) portletSession.getAttribute("authorizationNumber", PortletSession.APPLICATION_SCOPE);
                    if (authorizationNumber != null && authorizationNumber != "") {
                    	log.info("Procedure Information - SAVE: Procedure Authorization Number --> " + authorizationNumber);
                        authHeaderDTO.setPriorAuthNumber(authorizationNumber);
                    }

                    priorAuthHeaderGet.setPriorAuthHeaderDTO(authHeaderDTO);
                    priorAuthHeaderGet.setSourceTab(sourceTab);
                    priorAuthHeaderGet.setAuthorizationKey(authorizationKey.longValue());
                    priorAuthHeaderGet.setAppCreateUserId(themeDisplay.getUser().getEmailAddress());
                    priorAuthHeaderGet.setAppMaintUserId(themeDisplay.getUser().getEmailAddress());
                    GetPriorAuthHeader2.setSearchCriteriaRequest(priorAuthHeaderGet);
                    getPriorAuthHeaderE.setGetPriorAuthHeader(GetPriorAuthHeader2);
                    
                    PriorAuthHeaderServiceStub.GetPriorAuthHeaderResponseE getPriorAuthHeaderResponseE =null;
                    PriorAuthHeaderServiceStub.GetPriorAuthHeaderResponse getPriorAuthHeaderResponse =null;
                   
                    //TODO: Remove mock service code and condition. Don't remove else condition code
        			if(GetterUtil.getBoolean(com.liferay.portal.kernel.util.PropsUtil.get(PostServiceConstants.ENABLED_MOCK_SERVICE))){
        				 getPriorAuthHeaderResponseE = new PriorAuthHeaderServiceStub.GetPriorAuthHeaderResponseE();
        				 getPriorAuthHeaderResponse = new PriorAuthHeaderServiceStub.GetPriorAuthHeaderResponse();
        				 getPriorAuthHeaderResponse.set_return(priorAuthHeader);
                         getPriorAuthHeaderResponseE.setGetPriorAuthHeaderResponse(getPriorAuthHeaderResponse);
                    }else{
        				 getPriorAuthHeaderResponseE = priorAuthHeaderServiceStub.getPriorAuthHeader(getPriorAuthHeaderE);
                         getPriorAuthHeaderResponse = getPriorAuthHeaderResponseE.getGetPriorAuthHeaderResponse();
                    }
                    
                    if (getPriorAuthHeaderResponse.get_return().getPriorAuthHeaderDTO() != null) {
                        priorAuthHeaderDTO = getPriorAuthHeaderResponse.get_return().getPriorAuthHeaderDTO();
                        String createDateTime = priorAuthHeaderDTO.getAppCreateDatetime();
                        log.info("Procedure Information - SAVE: createDateTime --> " + createDateTime);
                        if (createDateTime != null && !createDateTime.equals("") && !createDateTime.equalsIgnoreCase("null")) {
                            procedureInformationFO.setAuthCreatedDate(PostServiceUtil.getPriorAuthUtil().getAppCreateDateSaveFormat(priorAuthHeaderDTO.getAppCreateDatetime()));
                            procedureInformationFO.setAuthCreatedDate(PostServiceUtil.getPriorAuthUtil().getStandardDateTime(priorAuthHeaderDTO.getAppCreateDatetime()));
                        }
                    }
                }
            } catch (RemoteException e) {
            	log.error("IOException during get response in procedureInformationDetails method",e);
            } catch (DAOExceptionException e) {
            	log.error("DAOExceptionException during get response in procedureInformationDetails method",e);
            }
	        portletSession.setAttribute("RequestedDatetime", (Object) authRequestedDate, PortletSession.APPLICATION_SCOPE);
            request.setAttribute("authPatientRelation", (Object) authPatientRelation);
            request.setAttribute("procedureInformationFO", (Object) procedureInformationFO);
            request.setAttribute("procedureDetails", procedureDetails);
            request.setAttribute("procedureToCodeDetails", procedureToCodeDetails);
            request.setAttribute("procedureDecisionDetails", procedureDecisionDetails);
            request.setAttribute("procedureReasonDetails", procedureReasonDetails);
            request.setAttribute("procedureUnitsDetails", procedureUnitsDetails);
        } catch (AxisFault e) {
         	log.error("AxisFault in procedureInformationDetails method",e);
        }

        log.info("Processing Save Action for Procedure Information Exit");
    }
    
    /**************Action method which gets called on click of SUBMIT button***************************************/
    @ActionMapping(params = { "action=procedureSubmitAction" })
    public void procedureSubmit(ActionRequest request, 
    		                    ActionResponse actionResponse, 
    		                    @ModelAttribute(value = "procedureInformationFO") ProcedureInformationFO procedureInformationFO) {

        log.info("Processing Submit Action for Procedure Information Enter");

        String authDecisionDate = null;
    	HttpServletRequest httpRequest = PortalUtil.getHttpServletRequest(request);
        String sourceTab = PostServiceUtil.procedureTab;
        PortletSession portletSession = request.getPortletSession();
    	String browserStr = getBrowser(httpRequest);

    	portletSession.removeAttribute("providerAuthStatus", PortletSession.APPLICATION_SCOPE);
        String submitButtonStatus = ParamUtil.getString((PortletRequest) request, (String) "submitButtonStatus");
        log.info("submitButtonStatus : " + submitButtonStatus);
        String originalDecisionDate = "";
        ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute("LIFERAY_SHARED_THEME_DISPLAY");
        try {
            
			procedureInformationFO.setAuthStatusCode(ParamUtil.getString((PortletRequest) request, (String) ("authStatus")));
			procedureInformationFO.setAuthWithdrawnReasonCode(ParamUtil.getString((PortletRequest) request, (String) ("authWithdrawnReason")));

			log.info("Procedure Information - SUBMIT: Auth Status Code --> " + procedureInformationFO.getAuthStatusCode());
            log.info("Procedure Information - SUBMIT: Requested Date --> " + procedureInformationFO.getAuthRequestedDate());
            log.info("Procedure Information - SUBMIT: Requested Time --> " + procedureInformationFO.getAuthRequestedTime());
            log.info("Procedure Information - SUBMIT: Service Date --> " + procedureInformationFO.getServiceDate());
            log.info("Procedure Information - SUBMIT: Due Date --> " + procedureInformationFO.getAuthDueDate());
            log.info("Procedure Information - SUBMIT: Due Time --> " + procedureInformationFO.getAuthDueTime());
            log.info("Procedure Information - SUBMIT: Worked By --> " + procedureInformationFO.getAuthWorkedBy());
            log.info("Procedure Information - SUBMIT: Withdrawn Reason Code --> " + procedureInformationFO.getAuthWithdrawnReasonCode());

            String memberId = (String) portletSession.getAttribute("memberId", PortletSession.APPLICATION_SCOPE);
            String memberNumber = (String) portletSession.getAttribute("memberNumber", PortletSession.APPLICATION_SCOPE);
            String healthPlan = (String) portletSession.getAttribute("healthPlan", PortletSession.APPLICATION_SCOPE);
            String mpi = (String) portletSession.getAttribute("mpi", PortletSession.APPLICATION_SCOPE);
            PriorAuthHeaderServiceStub priorAuthHeaderServiceStub = new PriorAuthHeaderServiceStub();
            PriorAuthHeaderServiceStub.SavePriorAuthHeaderE savePriorAuthHeader = new PriorAuthHeaderServiceStub.SavePriorAuthHeaderE();
            PriorAuthHeaderServiceStub.SavePriorAuthHeader savePriorAuthHead = new PriorAuthHeaderServiceStub.SavePriorAuthHeader();
            PriorAuthHeaderServiceStub.PriorAuthHeader priorAuthHeader = new PriorAuthHeaderServiceStub.PriorAuthHeader();
            priorAuthHeader.setAppCreateUserId(themeDisplay.getUser().getEmailAddress());
            priorAuthHeader.setAppMaintUserId(themeDisplay.getUser().getEmailAddress());
            LinkedHashMap procedureDetails = new LinkedHashMap();
            LinkedHashMap procedureToCodeDetails = new LinkedHashMap();
            LinkedHashMap procedureDecisionDetails = new LinkedHashMap();
            LinkedHashMap procedureReasonDetails = new LinkedHashMap();
            LinkedHashMap procedureUnitsDetails = new LinkedHashMap();
            LinkedHashMap<Integer, String> diagnosisCodeAdditional = new LinkedHashMap<Integer, String>();
            ArrayList<PriorAuthHeaderServiceStub.ProcedureDTO> procedureList = new ArrayList<PriorAuthHeaderServiceStub.ProcedureDTO>();
            ArrayList<PriorAuthHeaderServiceStub.DiagnosisDTO> diagnosisDTOList = new ArrayList<PriorAuthHeaderServiceStub.DiagnosisDTO>();
            PriorAuthHeaderServiceStub.DiagnosisDTO primaryDiagnosis = new PriorAuthHeaderServiceStub.DiagnosisDTO();
            
            // A withdrawn PA can be submitted with no primary diagnosis code.  If this occurs, including diagnosis in the save causes attempt to insert a null
            // into IcdDiagnosisCode.  Since this is not allowed, do not include the diagnosis in the save.
            if ((procedureInformationFO.getDiagnosisCodePrimary().length() >= 0) || !procedureInformationFO.getAuthStatusCode().equals(PostServiceConstants.COMPLETED_WITHDRAWN_CODE)) {
            	
            	// Do not set diagnosis if the first one is blank
                primaryDiagnosis.setIcdDiagnosisCode(procedureInformationFO.getDiagnosisCodePrimary());
                log.info("Procedure Information - SUBMIT: Primary Diagnosis Code --> " + primaryDiagnosis.getIcdDiagnosisCode());

                primaryDiagnosis.setSequenceNumber(1);
	            diagnosisDTOList.add(primaryDiagnosis);
	            for (int j = 1; j <= 11; ++j) {
	                request.setAttribute("pdtoListreReq", new ArrayList());
	                PriorAuthHeaderServiceStub.DiagnosisDTO diagnosisDTO = new PriorAuthHeaderServiceStub.DiagnosisDTO();
	                String additionalFields = ParamUtil.getString((PortletRequest) request, (String) ("diagnosisCodeAdditional" + j));
	                if (additionalFields != null && !additionalFields.trim().isEmpty()) {
	                    diagnosisDTO.setIcdDiagnosisCode(additionalFields);
	                    diagnosisDTO.setSequenceNumber(j + 1);
	                    diagnosisDTOList.add(diagnosisDTO);
	                }
	                diagnosisCodeAdditional.put(j, ParamUtil.getString((PortletRequest) request, (String) ("diagnosisCodeAdditional" + j)));
	                log.info("Procedure Information - SUBMIT: Additional Diagnosis Code " + j + " --> " + (String) diagnosisCodeAdditional.get(j));
	            }
	            priorAuthHeader.setDiagnosis(diagnosisDTOList.toArray(new PriorAuthHeaderServiceStub.DiagnosisDTO[diagnosisDTOList.size()]));
            }

            for (int i = 1; i <= 25; ++i) {
                request.setAttribute("pdtoListreReq", new ArrayList());
                PriorAuthHeaderServiceStub.ProcedureDTO procedureDTO = new PriorAuthHeaderServiceStub.ProcedureDTO();
                String units = ParamUtil.getString((PortletRequest) request, (String) ("procedureUnits" + i));
                String procedureFromCode = ParamUtil.getString((PortletRequest) request, (String) ("procedureFromCode" + i));
                String procedureToCode = ParamUtil.getString((PortletRequest) request, (String) ("procedureToCode" + i));
                String procedureDecision = ParamUtil.getString((PortletRequest) request, (String) ("procedureDecision" + i));
                String procedureReason = ParamUtil.getString((PortletRequest) request, (String) ("procedureDecisionReason" + i));
                if (!(units.equals("") || procedureReason.equals("") || procedureFromCode.equals("") || procedureDecision.equals(""))) {
                    procedureDTO.setProcedureFromCode(procedureFromCode);
                    if (!procedureToCode.equals("")) {
                        procedureDTO.setProcedureToCode(procedureToCode);
                    } else {
                        procedureDTO.setProcedureToCode(null);
                    }
                    procedureDTO.setProcedureDecision(procedureDecision);
                    procedureDTO.setProcedureDecisionReason(procedureReason);
                    procedureDTO.setProcedureUnits(Float.parseFloat(units));
                    procedureList.add(procedureDTO);
                }
                log.info("Procedure Information - SUBMIT: procedure From Code " + i + " --> " + (String) procedureDetails.get(i));
                log.info("Procedure Information - SUBMIT: procedure To Code " + i + " --> " + (String) procedureToCodeDetails.get(i));
                log.info("Procedure Information - SUBMIT: procedure Decision " + i + " --> " + (String) procedureDecisionDetails.get(i));
                log.info("Procedure Information - SUBMIT: procedure Reason " + i + " --> " + (String) procedureReasonDetails.get(i));
                log.info("Procedure Information - SUBMIT: procedure Units " + i + " --> " + (String) procedureUnitsDetails.get(i));
            }
            log.info("Procedure Information - SUBMIT: procedureList Size --> " + procedureList);
            priorAuthHeader.setProcedures(procedureList.toArray(new PriorAuthHeaderServiceStub.ProcedureDTO[procedureList.size()]));

            String authorizationNumber = (String) portletSession.getAttribute("authorizationNumber", PortletSession.APPLICATION_SCOPE);
            
			String memberFirstName = (String) portletSession.getAttribute("memberFirstName", PortletSession.APPLICATION_SCOPE);
			String memberMiddleName = (String) portletSession.getAttribute("memberMiddleName", PortletSession.APPLICATION_SCOPE);
			String memberLastName = (String) portletSession.getAttribute("memberLastName", PortletSession.APPLICATION_SCOPE);
			String memberSuffixName = (String) portletSession.getAttribute("memberSuffixName", PortletSession.APPLICATION_SCOPE);
			String memberDob = (String) portletSession.getAttribute("memberDob", PortletSession.APPLICATION_SCOPE);
			String memberGender = (String) portletSession.getAttribute("memberGender", PortletSession.APPLICATION_SCOPE);
			String memberGenderCode = memberGender.substring(0, 1);
			String businessSectorCode = (String) portletSession.getAttribute("businessSectorCode", PortletSession.APPLICATION_SCOPE);
			String businessSectorDescription = (String) portletSession.getAttribute("businessSectorDescription", PortletSession.APPLICATION_SCOPE);
			String businessSegmentCode = (String) portletSession.getAttribute("businessSegmentCode", PortletSession.APPLICATION_SCOPE);
			String businessSegmentDescription = (String) portletSession.getAttribute("businessSegmentDescription", PortletSession.APPLICATION_SCOPE);
            log.info("Procedure Information - SUBMIT: memberFirstName is " + memberFirstName);
            log.info("Procedure Information - SUBMIT: memberMiddleName is " + memberMiddleName);
            log.info("Procedure Information - SUBMIT: memberLastName is " + memberLastName);
            log.info("Procedure Information - SUBMIT: memberSuffixName is " + memberSuffixName);
            log.info("Procedure Information - SUBMIT: memberDob is " + memberDob);
            log.info("Procedure Information - SUBMIT: memberGenderCode is " + memberGenderCode);
            log.info("Procedure Information - SUBMIT: businessSectorCode is " + businessSectorCode);
            log.info("Procedure Information - SUBMIT: businessSectorDescription is " + businessSectorDescription);
            log.info("Procedure Information - SUBMIT: businessSegmentCode is " + businessSegmentCode);
            log.info("Procedure Information - SUBMIT: businessSegmentDescription is " + businessSegmentDescription);
            PriorAuthHeaderServiceStub.PriorAuthHeaderDTO priorAuthHeaderDTO = new PriorAuthHeaderServiceStub.PriorAuthHeaderDTO();
            if (authorizationNumber != null && authorizationNumber != "") {
                priorAuthHeaderDTO.setPriorAuthNumber(authorizationNumber);
            }

			// Set the member data returned from Member Lab Benefits
			priorAuthHeaderDTO.setPriorAuthPatientFirstName(memberFirstName);
			priorAuthHeaderDTO.setPriorAuthPatientMiddleName(memberMiddleName);
			priorAuthHeaderDTO.setPriorAuthPatientLastName(memberLastName);
			priorAuthHeaderDTO.setPriorAuthPatientSuffixName(memberSuffixName);
			priorAuthHeaderDTO.setPriorAuthPatientBirthDate(memberDob);
			priorAuthHeaderDTO.setPriorAuthPatientGenderCode(memberGenderCode);
			
			// Set the Line of Business data returned from Member Lab Benefits
			priorAuthHeaderDTO.setPriorAuthBusinessSectorCode(businessSectorCode);
			priorAuthHeaderDTO.setPriorAuthBusinessSectorDescription(businessSectorDescription);
			priorAuthHeaderDTO.setPriorAuthBusinessSegmentCode(businessSegmentCode);
			priorAuthHeaderDTO.setPriorAuthBusinessSegmentDescription(businessSegmentDescription);
			
			priorAuthHeaderDTO.setAuthInboundChannel(procedureInformationFO.getAuthInboundChannel());
            log.info("Procedure Information - SUBMIT: Authorization RequestedDate --> " + procedureInformationFO.getAuthRequestedDate() + " " + procedureInformationFO.getAuthRequestedTime());
            priorAuthHeaderDTO.setAppCreateUserId(themeDisplay.getUser().getEmailAddress());
            priorAuthHeader.setAppMaintUserId(themeDisplay.getUser().getEmailAddress());
            priorAuthHeaderDTO.setAppCreateDatetime(AgeCalculation.getAgeCalculation().getProcedureCreationDate());
            
            String authPatientRelation = ParamUtil.getString((PortletRequest) request, (String) "authPatientRelation");
            if (!authPatientRelation.equalsIgnoreCase("")) {
                priorAuthHeaderDTO.setPatientGenderRelationshipCode(authPatientRelation);
            } else {
                priorAuthHeaderDTO.setPatientGenderRelationshipCode(null);
            }

            priorAuthHeaderDTO.setAppCreateUserId(themeDisplay.getUser().getEmailAddress());
            priorAuthHeaderDTO.setIdCardNumber(memberId);
            priorAuthHeaderDTO.setMemberNumber(memberNumber);
            priorAuthHeaderDTO.setMasterPatientId(mpi);
            priorAuthHeaderDTO.setHealthPlanGroupId(healthPlan);
            priorAuthHeaderDTO.setHealthPlanId("01");
            
            // Convert the date to the format needed to save 
            String authRequestedDate = procedureInformationFO.getAuthRequestedDate();
            authRequestedDate = PostServiceUtil.getPriorAuthUtil().setProcedureResDate(authRequestedDate, browserStr);
            log.info("Procedure Information - SUBMIT: Requested Date --> " + authRequestedDate);
 
            String serviceDate = procedureInformationFO.getServiceDate();
            priorAuthHeaderDTO.setAuthRequestedDate(PostServiceUtil.getPriorAuthUtil().getAuthRequestedDate(authRequestedDate + " " + procedureInformationFO.getAuthRequestedTime()));
            
            // Set the Service Begin Date and Service End Date to the Service Date
            if (browserStr.equals("IE") || browserStr.equals("Firefox") || browserStr.equals("unknown")) {
                priorAuthHeaderDTO.setPriorAuthBeginServiceDate(serviceDate);
            } else {
                serviceDate = PostServiceUtil.getPriorAuthUtil().setProcedureResBeginDate(serviceDate, browserStr);
                priorAuthHeaderDTO.setPriorAuthBeginServiceDate(PostServiceUtil.getPriorAuthUtil().getProcedureReqBeginDate(serviceDate, browserStr));
            }
            priorAuthHeaderDTO.setPriorAuthEndServiceDate(priorAuthHeaderDTO.getPriorAuthBeginServiceDate());
            log.info("Procedure Information - SUBMIT: Service Begin Date --> " + serviceDate);

            authDecisionDate = (String) portletSession.getAttribute("authDecisionDate", PortletSession.APPLICATION_SCOPE);
            log.info("Procedure Information - SUBMIT: Authorization Decision Date --> " + authDecisionDate);

            String originalCreatedUser = (String) portletSession.getAttribute("originalUserId", PortletSession.APPLICATION_SCOPE);
			boolean inprocessFlag = this.isInProcess(procedureInformationFO.getAuthStatusCode());
            if (!inprocessFlag && authDecisionDate != null) {
                priorAuthHeaderDTO.setPriorAuthOriginalStatusUserId(originalCreatedUser);
                priorAuthHeaderDTO.setPriorAuthCurrentStatusUserId(themeDisplay.getUser().getEmailAddress());
                priorAuthHeaderDTO.setPriorAuthCurrentStatusDatetime(PostServiceUtil.getPriorAuthUtil().getOriginalDate());
                priorAuthHeaderDTO.setPriorAuthStatusCode(procedureInformationFO.getAuthStatusCode());
                priorAuthHeaderDTO.setPriorAuthStatusDesc(null);
                priorAuthHeaderDTO.setPriorAuthOriginalStatusDatetime(PostServiceUtil.getPriorAuthUtil().getSavedDecisionDate(authDecisionDate));
            }
            if (!inprocessFlag && authDecisionDate == null) {
                priorAuthHeaderDTO.setPriorAuthOriginalStatusUserId(originalCreatedUser);
                priorAuthHeaderDTO.setPriorAuthCurrentStatusUserId(themeDisplay.getUser().getEmailAddress());
                priorAuthHeaderDTO.setPriorAuthCurrentStatusDatetime(PostServiceUtil.getPriorAuthUtil().getOriginalDate());
                priorAuthHeaderDTO.setPriorAuthStatusCode(procedureInformationFO.getAuthStatusCode());
                priorAuthHeaderDTO.setPriorAuthStatusDesc(null);
                originalDecisionDate = PostServiceUtil.getPriorAuthUtil().getOriginalDate();
                priorAuthHeaderDTO.setPriorAuthOriginalStatusDatetime(originalDecisionDate);
            }
            if (inprocessFlag) {
                priorAuthHeaderDTO.setPriorAuthOriginalStatusUserId(null);
                priorAuthHeaderDTO.setPriorAuthCurrentStatusUserId(null);
                priorAuthHeaderDTO.setPriorAuthCurrentStatusDatetime(null);
                priorAuthHeaderDTO.setPriorAuthStatusCode(null);
                priorAuthHeaderDTO.setPriorAuthStatusDesc(null);
                priorAuthHeaderDTO.setPriorAuthOriginalStatusDatetime(null);
            } else {

            	// Do not change the Original status date/time for a withdrawn status
            	if (!procedureInformationFO.getAuthStatusCode().equals(PostServiceConstants.COMPLETED_WITHDRAWN_CODE)) {
                    
                    // Save the In Process Auth Status
                    priorAuthHeaderDTO.setPriorAuthStatusCode(procedureInformationFO.getAuthStatusCode());
                    priorAuthHeaderDTO.setPriorAuthStatusDesc(null);

            		// Save any new Original status date/time
					String decisionDate = procedureInformationFO.getAuthDecisionDate();
					String decisionTime = procedureInformationFO.getAuthDecisionTime();
					if (!decisionDate.equals("") && !decisionTime.equals("") && !decisionTime.equals("hh:mm AM/PM")) {
						String newAuthOriginalStatusDatetime = decisionDate + " " + decisionTime;
	
						newAuthOriginalStatusDatetime = PostServiceUtil.getPriorAuthUtil().getDecisionDateTimeSaveFormat(newAuthOriginalStatusDatetime, browserStr);
						log.info("Procedure Information - SUBMIT: New AuthOriginalStatusDatetime --> " + newAuthOriginalStatusDatetime);
						priorAuthHeaderDTO.setPriorAuthOriginalStatusDatetime(newAuthOriginalStatusDatetime);
					} else {
						if (authDecisionDate == null) {
							
							// Use the current date/time for a new prior auth
							Date today = new Date();
							SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
							authDecisionDate = df.format(today);
						}
						log.info("Procedure Information - SUBMIT: New AuthOriginalStatusDatetime --> " + authDecisionDate);
						priorAuthHeaderDTO.setPriorAuthOriginalStatusDatetime(PostServiceUtil.getPriorAuthUtil().getSavedDecisionDate(authDecisionDate));
					}
            	}
            }

            // Save the Due Date/Time
            String dueDate = procedureInformationFO.getAuthDueDate();
            String dueTime = procedureInformationFO.getAuthDueTime();
			if (!dueDate.equals("") && !dueTime.equals("") && !dueTime.equals("hh:mm AM/PM")) {
				String newDueDatetime = dueDate + " " + dueTime;

				newDueDatetime = PostServiceUtil.getPriorAuthUtil().getDecisionDateTimeSaveFormat(newDueDatetime, browserStr);
				priorAuthHeaderDTO.setPriorAuthDueDatetime(newDueDatetime);
				log.info("Procedure Information - SUBMIT: Due Datetime --> " + newDueDatetime);
			}
            
            // Save the Worked By
			String workedBy = procedureInformationFO.getAuthWorkedBy();
            priorAuthHeaderDTO.setPriorAuthWorkedBy(workedBy);
			log.info("Procedure Information - SUBMIT: Worked By --> " + workedBy);
            
            // Save the Withdrawn Status Code
			String withdrawnReasonCode = procedureInformationFO.getAuthWithdrawnReasonCode();
            priorAuthHeaderDTO.setPriorAuthWithdrawnReasonCode(withdrawnReasonCode);
			log.info("Procedure Information - SUBMIT: Withdrawn Status --> " + withdrawnReasonCode);

            String authSubmissionStatus = ParamUtil.getString((PortletRequest) request, (String) "authSubmissionStatus");
            
        	if (procedureInformationFO.getAuthStatusCode().equals(PostServiceConstants.COMPLETED_WITHDRAWN_CODE)) {
        		
        		// Set the void submission status
				if (authSubmissionStatus.equalsIgnoreCase("Saved") || authSubmissionStatus.equalsIgnoreCase("Submitted")) {
				    priorAuthHeaderDTO.setPriorAuthSubmissionStatusCode("35");
				    portletSession.setAttribute("authSubmissionStatus", (Object) "Void", PortletSession.APPLICATION_SCOPE);
				} else if (authSubmissionStatus.equalsIgnoreCase("Sent") || authSubmissionStatus.equalsIgnoreCase("Updated") || authSubmissionStatus.equalsIgnoreCase("Updates Submitted")) {
				    priorAuthHeaderDTO.setPriorAuthSubmissionStatusCode("70");
				    portletSession.setAttribute("authSubmissionStatus", (Object) "Void HP - Submitted", PortletSession.APPLICATION_SCOPE);
				}
        	} else {
        		
        		// Set the not void submission status
	            if (authSubmissionStatus.equalsIgnoreCase("Saved") || authSubmissionStatus.equalsIgnoreCase("Submitted")) {
	                priorAuthHeaderDTO.setPriorAuthSubmissionStatusCode("40");
	                portletSession.setAttribute("authSubmissionStatus", (Object) "Submitted", PortletSession.APPLICATION_SCOPE);
	            } else if (authSubmissionStatus.equalsIgnoreCase("Sent") || authSubmissionStatus.equalsIgnoreCase("Updated")) {
	                priorAuthHeaderDTO.setPriorAuthSubmissionStatusCode("41");
	                portletSession.setAttribute("authSubmissionStatus", (Object) "Updates Submitted", PortletSession.APPLICATION_SCOPE);
	            }
        	}
            log.info("Procedure Information - SUBMIT: Submission Status --> " + authSubmissionStatus);

            portletSession.setAttribute("authorizationStatus", (Object) procedureInformationFO.getAuthStatusDesc(), PortletSession.APPLICATION_SCOPE);
            priorAuthHeader.setPriorAuthHeaderDTO(priorAuthHeaderDTO);
            Long authorizationKey = (Long) portletSession.getAttribute("authorizationKey", PortletSession.APPLICATION_SCOPE);
            log.info("Procedure Information - SUBMIT: Authorization Key --> " + authorizationKey);
            if (authorizationKey != null) {
                priorAuthHeader.setAuthorizationKey(authorizationKey.longValue());
            }
            priorAuthHeader.setSourceTab(sourceTab);
            savePriorAuthHead.setSavePriorAuthHeaderRequest(priorAuthHeader);
            savePriorAuthHeader.setSavePriorAuthHeader(savePriorAuthHead);
            try {
            	
            	PriorAuthHeaderServiceStub.SavePriorAuthHeaderResponseE savePriorAuthHeaderResponseE = null;
                PriorAuthHeaderServiceStub.SavePriorAuthHeaderResponse savePriorAuthHeaderResponse = null;
                
                //TODO: Remove mock service code and condition. Don't remove else condition code
    			if(GetterUtil.getBoolean(com.liferay.portal.kernel.util.PropsUtil.get(PostServiceConstants.ENABLED_MOCK_SERVICE))){
    				 //Success Mock Code
    				savePriorAuthHeaderResponseE =new PriorAuthHeaderServiceStub.SavePriorAuthHeaderResponseE();
                    savePriorAuthHeaderResponse = new PriorAuthHeaderServiceStub.SavePriorAuthHeaderResponse();
                    savePriorAuthHeaderResponse.set_return(priorAuthHeader);
                    savePriorAuthHeaderResponseE.setSavePriorAuthHeaderResponse(savePriorAuthHeaderResponse);
                } else{
    				savePriorAuthHeaderResponseE = priorAuthHeaderServiceStub.savePriorAuthHeader(savePriorAuthHeader);
                    savePriorAuthHeaderResponse = savePriorAuthHeaderResponseE.getSavePriorAuthHeaderResponse();
                }
            	PriorAuthHeaderServiceStub.PriorAuthHeader priorAuthHeaderResponse = savePriorAuthHeaderResponse.get_return();
                procedureInformationFO.setAuthRecordCreator(priorAuthHeaderResponse.getPriorAuthHeaderDTO().getAppCreateUserId());
                String appCreateDatetime = priorAuthHeaderResponse.getPriorAuthHeaderDTO().getAppCreateDatetime();
                log.info("Procedure Information - SUBMIT: appCreateDatetime value --> " + appCreateDatetime);
                
                appCreateDatetime = PostServiceUtil.getPriorAuthUtil().getAppCreateDateSaveRetrieve(appCreateDatetime);
                procedureInformationFO.setAuthCreatedDate(PostServiceUtil.getPriorAuthUtil().displayStandardDateTime(appCreateDatetime));
				procedureInformationFO.setAuthInboundChannel(priorAuthHeaderResponse.getPriorAuthHeaderDTO().getAuthInboundChannel());
                authDecisionDate = priorAuthHeaderResponse.getPriorAuthHeaderDTO().getPriorAuthOriginalStatusDatetime();
                if (authDecisionDate != null && authDecisionDate != "" && !authDecisionDate.equalsIgnoreCase("null")) {
                    procedureInformationFO.setAuthDecisionDate(authDecisionDate.substring(0, dateLen));
                    procedureInformationFO.setAuthDecisionTime(PostServiceUtil.getPriorAuthUtil().getProcedureDecisionTime(procedureInformationFO.getAuthDecisionDate()));
                    procedureInformationFO.setAuthDecisionDate(PostServiceUtil.getPriorAuthUtil().getProcedureDecisionDate(procedureInformationFO.getAuthDecisionDate(), browserStr));
                    log.info("Procedure Information - SUBMIT: AuthDecisionDate --> " + procedureInformationFO.getAuthDecisionDate());
                    log.info("Procedure Information - SUBMIT: AuthDecisionTime --> " + procedureInformationFO.getAuthDecisionTime());
                } else {
                    procedureInformationFO.setAuthDecisionDate(" ");
                    procedureInformationFO.setAuthDecisionTime(" ");
                }
                procedureInformationFO.setAuthStatusDesc(priorAuthHeaderResponse.getPriorAuthHeaderDTO().getPriorAuthStatusDesc());
                
                // Use the Service Begin Date as the Service Date
                procedureInformationFO.setServiceDate(PostServiceUtil.getPriorAuthUtil().getServiceDates(priorAuthHeaderResponse.getPriorAuthHeaderDTO().getPriorAuthBeginServiceDate(), browserStr));
                log.info("Procedure Information - SUBMIT: Service Date --> " + procedureInformationFO.getServiceDate());
                procedureInformationFO.setAuthStatusDesc(priorAuthHeaderResponse.getPriorAuthHeaderDTO().getPriorAuthStatusDesc());
                log.info("Procedure Information - SUBMIT: Status Description --> " + procedureInformationFO.getAuthStatusDesc());

                PriorAuthHeaderServiceStub.GetPriorAuthHeaderE getPriorAuthHeaderE = new PriorAuthHeaderServiceStub.GetPriorAuthHeaderE();
                PriorAuthHeaderServiceStub.GetPriorAuthHeader GetPriorAuthHeader2 = new PriorAuthHeaderServiceStub.GetPriorAuthHeader();
                PriorAuthHeaderServiceStub.PriorAuthHeader priorAuthHeaderGet = new PriorAuthHeaderServiceStub.PriorAuthHeader();
                PriorAuthHeaderServiceStub.PriorAuthHeaderDTO authHeaderDTO = new PriorAuthHeaderServiceStub.PriorAuthHeaderDTO();
                 authorizationNumber = (String) portletSession.getAttribute("authorizationNumber", PortletSession.APPLICATION_SCOPE);
                 if (authorizationNumber != null && authorizationNumber != "") {
                    log.info("Procedure Information - SUBMIT: Procedure Authorization Number --> " + authorizationNumber);
                    authHeaderDTO.setPriorAuthNumber(authorizationNumber);
                }
                 
                priorAuthHeaderGet.setPriorAuthHeaderDTO(authHeaderDTO);
                priorAuthHeaderGet.setSourceTab(sourceTab);
                priorAuthHeaderGet.setAuthorizationKey(authorizationKey.longValue());
                priorAuthHeaderGet.setAppCreateUserId(themeDisplay.getUser().getEmailAddress());
                priorAuthHeaderGet.setAppMaintUserId(themeDisplay.getUser().getEmailAddress());
                GetPriorAuthHeader2.setSearchCriteriaRequest(priorAuthHeaderGet);
                getPriorAuthHeaderE.setGetPriorAuthHeader(GetPriorAuthHeader2);
                
                PriorAuthHeaderServiceStub.GetPriorAuthHeaderResponseE getPriorAuthHeaderResponseE = null;
                PriorAuthHeaderServiceStub.GetPriorAuthHeaderResponse getPriorAuthHeaderResponse = null;
                
                //TODO: Remove mock service code and condition. Don't remove else condition code
        		if(GetterUtil.getBoolean(com.liferay.portal.kernel.util.PropsUtil.get(PostServiceConstants.ENABLED_MOCK_SERVICE))){
        			 getPriorAuthHeaderResponseE = new PriorAuthHeaderServiceStub.GetPriorAuthHeaderResponseE();
                     getPriorAuthHeaderResponse = new PriorAuthHeaderServiceStub.GetPriorAuthHeaderResponse();
                     PriorAuthHeader priorAuthHeaderObj = new PriorAuthHeader();
                     
                     PriorAuthHeaderDTO priorAuthHeaderDTOObj = new PriorAuthHeaderDTO();
                     priorAuthHeaderDTOObj.setAppCreateDatetime("2018-04-30 13:23:46");
                     priorAuthHeaderObj.setPriorAuthHeaderDTO(priorAuthHeaderDTOObj);
                     getPriorAuthHeaderResponse.set_return(priorAuthHeaderObj);
                     getPriorAuthHeaderResponseE.setGetPriorAuthHeaderResponse(getPriorAuthHeaderResponse);
                }else{
        			 getPriorAuthHeaderResponseE = priorAuthHeaderServiceStub.getPriorAuthHeader(getPriorAuthHeaderE);
                     getPriorAuthHeaderResponse = getPriorAuthHeaderResponseE.getGetPriorAuthHeaderResponse();
                     
        		}
               
                if (getPriorAuthHeaderResponse.get_return().getPriorAuthHeaderDTO() != null) {
                    priorAuthHeaderDTO = getPriorAuthHeaderResponse.get_return().getPriorAuthHeaderDTO();
                    String createDateTime = priorAuthHeaderDTO.getAppCreateDatetime();
                    log.info("Procedure Information - SUBMIT: createDateTime --> " + createDateTime);
                    if (createDateTime != null && !createDateTime.equals("") && !createDateTime.equalsIgnoreCase("null")) {
                        procedureInformationFO.setAuthCreatedDate(PostServiceUtil.getPriorAuthUtil().getAppCreateDateSaveFormat(priorAuthHeaderDTO.getAppCreateDatetime()));
                      //TODO: Comment below code because same value is setting above
                      //  procedureInformationFO.setAuthCreatedDate(PostServiceUtil.getPriorAuthUtil().getStandardDateTime(priorAuthHeaderDTO.getAppCreateDatetime()));
                    }
                }
                
            } catch (RemoteException e) {
            	log.error("RemoteException during getting response in procedureSubmit method",(Throwable) e);
            } catch (DAOExceptionException e) {
            	log.error("DAOExceptionException during getting response in procedureSubmit method",(Throwable) e);
            }
	        portletSession.setAttribute("RequestedDatetime", (Object) authRequestedDate, PortletSession.APPLICATION_SCOPE);
            request.setAttribute("authPatientRelation", (Object) authPatientRelation);
            request.setAttribute("procedureInformationFO", (Object) procedureInformationFO);
            request.setAttribute("procedureDetails", procedureDetails);
            request.setAttribute("procedureToCodeDetails", procedureToCodeDetails);
            request.setAttribute("procedureDecisionDetails", procedureDecisionDetails);
            request.setAttribute("procedureReasonDetails", procedureReasonDetails);
            request.setAttribute("procedureUnitsDetails", procedureUnitsDetails);
        } catch (AxisFault e) {
        	log.error("AxisFault Exception in procedureSubmit method",(Throwable) e);
        }

        log.info("Processing Submit Action for Procedure Information Exit");
    }
    
    private boolean isInProcess(String status) {
    
    	return (status.equals(PostServiceConstants.IN_PROCESS_INTAKE_REVIEW_CODE) || 
    			status.equals(PostServiceConstants.IN_PROCESS_READY_FOR_COMPL_CODE) || 
    			status.equals(PostServiceConstants.IN_PROCESS_NURSE_REVIEW_CODE) || 
    			status.equals(PostServiceConstants.IN_PROCESS_PHYSICIAN_REVIEW_CODE));
    }

    /**
     * Method invocation is ajax-call
     * Based on the diagnosis code 
     * Diagnosis description will be fetched from the database through the  esb service .
     */
    @ResourceMapping(value = "getDiagnosisDescription")
    private void getDiagnosisDescription(ResourceRequest resourceRequest,
	                                     ResourceResponse resourceResponse, 
	                                     String diagcode,
	                                     String fromDate)
	             throws IOException, PortletException, SystemException, PortalException {

		String description = null;
		
// NEED TO CHANGE
		/* Make sure the diagnosis header is null so it does use the previous code
		ServiceHandler.setTrialClaimsDiagnosisReqHdr(null);
		TrialClaimsDiagnosisReqHdr trialClaimsDiagnosisReqHdr = ServiceHandler.getTrialClaimsDiagnosisReqHdr();
		trialClaimsDiagnosisReqHdr.setIcdDiagnosisCode(diagcode);
		trialClaimsDiagnosisReqHdr.setServiceDate(fromDate);*/

		//TODO: Remove mock service code and condition. Don't remove else condition code
		if(GetterUtil.getBoolean(com.liferay.portal.kernel.util.PropsUtil.get(PostServiceConstants.ENABLED_MOCK_SERVICE))){
			
			//Success Mock Code
			description = "DiagnosisDesc";
		}else{
			/* PriorAuthTrialClaimsService priorAuthTrialClaimsService = ServiceHandler.getPriorAuthTrialClaimsPort();
			
			try {
				trialClaimsDiagnosisReqHdr = priorAuthTrialClaimsService.getIcdDiagnosisCode(trialClaimsDiagnosisReqHdr);
		
			    description = trialClaimsDiagnosisReqHdr.getIcdShortDescription();
			    log.info("diagnosis description ---> " + description + " for code " + diagcode);
			} catch (DAOException_Exception e) {
				log.error("DAOException_Exception in getDiagnosisDescription method",e);
			}*/

	    	// Creating the stub object to invoke the description service
		    PriorAuthTrialClaimsServiceStub priorAuthTrialClaimsServiceStub = new PriorAuthTrialClaimsServiceStub();
		    GetIcdDiagnosisCodeE getIcdDiagnosisCodeE = new GetIcdDiagnosisCodeE();

		    // Add the diagnosis code to the service
		    TrialClaimsDiagnosisReqHdr trialClaimsDiagnosisReqHdr = new TrialClaimsDiagnosisReqHdr();
		    trialClaimsDiagnosisReqHdr.setIcdDiagnosisCode(diagcode);
		    trialClaimsDiagnosisReqHdr.setServiceDate(fromDate);

		    GetIcdDiagnosisCode getIcdDiagnosisCode = new GetIcdDiagnosisCode();
		    getIcdDiagnosisCode.setGetIcdDiagnosisCodeRequest(trialClaimsDiagnosisReqHdr);
		    getIcdDiagnosisCodeE.setGetIcdDiagnosisCode(getIcdDiagnosisCode);
		    
		    // Initialize the return variables
		    GetIcdDiagnosisCodeResponseE getIcdDiagnosisCodeResponseE;
		    GetIcdDiagnosisCodeResponse getIcdDiagnosisCodeResponse = null;

		    try {
		    	
			    // Invoke the service
			    getIcdDiagnosisCodeResponseE = priorAuthTrialClaimsServiceStub.getIcdDiagnosisCode(getIcdDiagnosisCodeE);
			    getIcdDiagnosisCodeResponse = getIcdDiagnosisCodeResponseE.getGetIcdDiagnosisCodeResponse();
		    } catch (DAOExceptionException e) {
				log.error("DAOExceptionException in getUsers method",e);
		    }
        
	        // Fetching the data from the response object
	        description = getIcdDiagnosisCodeResponse.get_return().getIcdShortDescription();
		}
		
		PrintWriter out = resourceResponse.getWriter();
		
		if ((description != null) && !(description.equals(""))) {
		    out.println(description);
	        log.info("icdShortDescription : " + description);
		} else {
		    String des = "Please enter a valid diagnosis code";
		    out.println(des);
		}

    }

    
    /** Method invocation is ajax-call
     * Based on the procedureCode  
     * Procedure description will be fetched from the database through the esb-service.
     */
    @ResourceMapping(value = "getProcedureDescription")
    private void getProcedureDescription(ResourceRequest resourceRequest,
	                                     ResourceResponse resourceResponse, 
	                                     String procedureCode)
	        throws IOException, PortletException, SystemException, PortalException {

		String description = null;

// NEED TO CHANGE 
		/* Make sure the procedure header is null so it does use the previous code
		ServiceHandler.setTrialClaimsProcedureReqHdr(null);
		TrialClaimsProcedureReqHdr trialClaimsProcedureReqHdr = ServiceHandler.getTrialClaimsProcedureReqHdr();
		trialClaimsProcedureReqHdr.setType("Proc");
		trialClaimsProcedureReqHdr.setEnumerationCode(procedureCode);*/
	
		 //TODO: Remove mock service code and condition. Don't remove else condition code
		if(GetterUtil.getBoolean(com.liferay.portal.kernel.util.PropsUtil.get(PostServiceConstants.ENABLED_MOCK_SERVICE))){
			
			//Success Mock Code
			description = "ProcedureDesc";
		}else{
			/* PriorAuthTrialClaimsService priorAuthTrialClaimsService = ServiceHandler.getPriorAuthTrialClaimsPort();
			try {
			    trialClaimsProcedureReqHdr = priorAuthTrialClaimsService.getEnumShrtDescription(trialClaimsProcedureReqHdr);
		
			    description = trialClaimsProcedureReqHdr.getEnumerationShortDescription();
			    log.info("procedure description --------> " + description + " for code " + procedureCode);
			} catch (DAOException_Exception e) {
				log.error("DAOException_Exception in getProcedureDescription method",e);
			}*/

	    	// Creating the stub object to invoke the description service
		    PriorAuthTrialClaimsServiceStub priorAuthTrialClaimsServiceStub = new PriorAuthTrialClaimsServiceStub();
		    GetEnumShrtDescriptionE getEnumShrtDescriptionE = new GetEnumShrtDescriptionE();

		    // Add the procedure code to the service
		    TrialClaimsProcedureReqHdr trialClaimsProcedureReqHdr = new TrialClaimsProcedureReqHdr();
		    trialClaimsProcedureReqHdr.setType("Proc");
		    trialClaimsProcedureReqHdr.setEnumerationCode(procedureCode);
		    
		    GetEnumShrtDescription getEnumShrtDescription = new GetEnumShrtDescription();
		    getEnumShrtDescription.setGetEnumShrtDescriptionRequest(trialClaimsProcedureReqHdr);
		    getEnumShrtDescriptionE.setGetEnumShrtDescription(getEnumShrtDescription);

		    // Initialize the return variables
		    GetEnumShrtDescriptionResponseE getEnumShrtDescriptionResponseE;
		    GetEnumShrtDescriptionResponse getEnumShrtDescriptionResponse = null;
		    
		    try {
		    	
			    // Invoke the service
		    	getEnumShrtDescriptionResponseE = priorAuthTrialClaimsServiceStub.getEnumShrtDescription(getEnumShrtDescriptionE);
		    	getEnumShrtDescriptionResponse = getEnumShrtDescriptionResponseE.getGetEnumShrtDescriptionResponse();
		    } catch (DAOExceptionException e) {
				log.error("DAOExceptionException in getUsers method",e);
		    };
	        
	        // Fetching the data from the response object
		    description = getEnumShrtDescriptionResponse.get_return().getEnumerationShortDescription();
		}

		PrintWriter out = resourceResponse.getWriter();
	
		if ((description != null) && !(description.equals(""))) {
		    out.println(description);
	        log.info("procedureShortDescription : " + description);
		} else {
		    String des = "Please enter a valid procedure code";
		    out.println(des);
		}

    }

    /**************Action method which gets called on click of CANCEL button***************************************/
    @ActionMapping(params = { "cancelaction=procedureInfoCancelAction" })
    public void cancelPage(ActionRequest actionRequest, 
    		               ActionResponse actionResponse) {

    	log.info("Processing Cancel Action for Procedure Information Enter");
	    String userName = null;
	    List usersList = null;
		try {
		    User user = PortalUtil.getUser((PortletRequest) actionRequest);
		    if (user != null) {
			    usersList = user.getUserGroups();
		    }
		} catch (PortalException e) {
			log.error("PortalException during getting userlist in cancelPage method",e);
		} catch (SystemException e) {
			log.error("SystemException during getting userlist in cancelPage method",e);
		}
	    ArrayList<String> groupNames = new ArrayList<String>();
	    if (usersList != null) {
	    	int length = usersList.size();
		    for (int i = 0; i < length; ++i) {
		    	
		    	// Ignore the groups that do not have a home page
				if (!((UserGroup)usersList.get(i)).getName().equalsIgnoreCase("CDSUsers") &&
					!((UserGroup)usersList.get(i)).getName().equalsIgnoreCase("Everyone") &&
					!((UserGroup)usersList.get(i)).getName().equalsIgnoreCase("ManagedUsers") &&
					!((UserGroup)usersList.get(i)).getName().equalsIgnoreCase("Multi-Factor") &&
					!((UserGroup)usersList.get(i)).getName().equalsIgnoreCase("PortalAdmin") &&
					!((UserGroup)usersList.get(i)).getName().equalsIgnoreCase("UMGroup")) {
					userName = ((UserGroup) usersList.get(i)).getName();
					groupNames.add(userName);
				}
		    }
	    }
	    HttpServletRequest request = PortalUtil.getHttpServletRequest((PortletRequest) actionRequest);
	    String path = PortalUtil.getCurrentCompleteURL((HttpServletRequest) request);
	    StringBuilder output = new StringBuilder();
	    int count = 0;
	    char[] ch = path.toCharArray();
	    for (int i2 = 0; i2 < ch.length; ++i2) {
			if (ch[i2] == '/') {
			    ++count;
			}
			if (count >= 3)
			    continue;
			output = output.append(ch[i2]);
	    }

	    String landingPage = null;
	    if (usersList == null) {
			
			// Set to the default landing page since the user does not belong to any groups
			landingPage = "/web/guest/home";
	    } else {
	    	

	    	// Get the landing page based on the user's group
			landingPage = com.liferay.portal.kernel.util.PropsUtil.get("default.landing.page.path[" + userName  + "]"); 
			
			// If the user's group is not found use the defaule landing page
			if (landingPage == null) {
				landingPage = com.liferay.portal.kernel.util.PropsUtil.get("default.landing.page.path");
				if (landingPage == null) {
					
					// Set to the default landing page since it is not defined in the properties file
					landingPage = "/web/guest/home";
				}
			}
	    }
	    String pathRedirect = null;
		pathRedirect = output + landingPage;
		log.info(userName + " " + pathRedirect);
		try {
		    actionResponse.sendRedirect(pathRedirect);
		} catch (IOException e) {
			log.error("IOException during redirection in cancelPage method",e);
		}

        log.info("Processing Cancel Action for Procedure Information Exit");
    }
    
	private String getBrowser(HttpServletRequest httpRequest) {
		String userAgent = httpRequest.getHeader("User-Agent");
		String rtnValue = "unknown";
		
		if (userAgent.contains("Opera") || userAgent.contains("OPR")) {
			rtnValue = "Opera";
		} else if (userAgent.contains("Chrome")) {
			rtnValue = "Chrome";
		} else if (userAgent.contains("Safari")) {
			rtnValue = "Safari";
		} else if (userAgent.contains("MSIE") || (userAgent.contains("Trident") && userAgent.contains("rv:"))) { 
			rtnValue = "IE";
		} else if (userAgent.contains("Firefox"))  {
			rtnValue = "Firefox";
		} else {
			rtnValue = "unknown";
		}

		return rtnValue;
	}

    @ActionMapping
    public void ProcedureHandler() {
        log.info("Procedure Information Handler");
    }
    
    /**
     * TODO: Remove below success mock
     * @return
     */
    private PriorAuthHeaderServiceStub.GetPriorAuthHeaderResponse getPriorAuthHeaderResponseSuccessMock(String priorAuthNo){
    	
    	 PriorAuthHeaderServiceStub.GetPriorAuthHeaderResponse getPriorAuthHeaderResponse = new PriorAuthHeaderServiceStub.GetPriorAuthHeaderResponse();
    	 PriorAuthHeader priorAuthHeaderObj = new PriorAuthHeader();
		 DiagnosisDTO diagnosisDTO = new DiagnosisDTO();
		 diagnosisDTO.setIcdDiagnosisCode("LCDDCode");
		 diagnosisDTO.setSequenceNumber(1);
		 priorAuthHeaderObj.setDiagnosis(new DiagnosisDTO[]{diagnosisDTO});
		 
		 ProcedureDTO procedureDTO = new ProcedureDTO();
		 procedureDTO.setLabTestTypeName("LabTestType");
		 procedureDTO.setProcedureDecision("ProDecision");
		 procedureDTO.setProcedureDecisionReason("ProReason");
		 procedureDTO.setProcedureFastTrack("FastTrack");
		 procedureDTO.setProcedureFromCode("PFromCode");
		 procedureDTO.setProcedureToCode("PTocode");
		 procedureDTO.setProcedureUnits(0.1f);
		 priorAuthHeaderObj.setProcedures(new ProcedureDTO[]{procedureDTO});
		 PriorAuthHeaderDTO priorAuthHeaderDTOObj = new PriorAuthHeaderDTO();
		 priorAuthHeaderDTOObj.setAppCreateDatetime("2018-04-26 01:02:00.1");
		 priorAuthHeaderDTOObj.setAppCreateUserId("UserId");
		 priorAuthHeaderDTOObj.setAppMaintDatetime("2018-04-26 01:02:00.1");
		 priorAuthHeaderDTOObj.setAuthInboundChannel("IChannel");
		 priorAuthHeaderDTOObj.setAuthNumberSearched("22");
		 priorAuthHeaderDTOObj.setAuthRequestedDate("2018-04-26 01:02:00.1");
		 priorAuthHeaderDTOObj.setExtractPerformedQuantity((short)100);
		 priorAuthHeaderDTOObj.setHealthPlanGroupId("HPGid");
		 priorAuthHeaderDTOObj.setHealthPlanId("HPId");
		 priorAuthHeaderDTOObj.setIcdRevisionNumber("RevNo");
		 priorAuthHeaderDTOObj.setIdCardNumber("22");
		 priorAuthHeaderDTOObj.setMasterPatientId("MPId");
		 priorAuthHeaderDTOObj.setMemberNumber("22");
		 priorAuthHeaderDTOObj.setPatientGenderRelationshipCode("CO1");
		 priorAuthHeaderDTOObj.setPriorAuthBeginServiceDate("2018-04-26 01:02:00.1");
		 priorAuthHeaderDTOObj.setPriorAuthBusinessSectorCode("SecCode");
		 priorAuthHeaderDTOObj.setPriorAuthBusinessSectorDescription("SecDesc");
		 priorAuthHeaderDTOObj.setPriorAuthBusinessSegmentCode("Segcode");
		 priorAuthHeaderDTOObj.setPriorAuthBusinessSegmentDescription("SegDesc");
		 priorAuthHeaderDTOObj.setPriorAuthCurrentStatusDatetime("2018-04-26 01:02:00.1");
		 priorAuthHeaderDTOObj.setPriorAuthCurrentStatusUserId("UId");
		 priorAuthHeaderDTOObj.setPriorAuthDueDatetime("2018-04-26 01:02:00.1");
		 priorAuthHeaderDTOObj.setPriorAuthEndServiceDate("2018-04-26 01:02:00.1");
		 priorAuthHeaderDTOObj.setPriorAuthHeaderKey(22);
		 priorAuthHeaderDTOObj.setPriorAuthNumber(priorAuthNo);
		 priorAuthHeaderDTOObj.setPriorAuthOriginalStatusDatetime("2018-04-26 01:02:00.1");
		 priorAuthHeaderDTOObj.setPriorAuthOriginalStatusUserId("StatusUserId");
		 priorAuthHeaderDTOObj.setPriorAuthPatientBirthDate("1992-04-26 01:02:00.1");
		 priorAuthHeaderDTOObj.setPriorAuthPatientFirstName("PFirst");
		 priorAuthHeaderDTOObj.setPriorAuthPatientGenderCode("M");
		 priorAuthHeaderDTOObj.setPriorAuthPatientLastName("PLast");
		 priorAuthHeaderDTOObj.setPriorAuthPatientMiddleName("PMiddle");
		 priorAuthHeaderDTOObj.setPriorAuthPatientSuffixName("PSuffix");
		 priorAuthHeaderDTOObj.setPriorAuthPriorityCode("PC");
		 priorAuthHeaderDTOObj.setPriorAuthStatusCode("ASC");
		 priorAuthHeaderDTOObj.setPriorAuthStatusDesc("ASD");
		 priorAuthHeaderDTOObj.setPriorAuthSubmissionStatusCode("ASub");
		 priorAuthHeaderDTOObj.setPriorAuthWithdrawnReasonCode("RECo");
		 priorAuthHeaderDTOObj.setPriorAuthWithdrawnReasonDesc("REDesc");
		 priorAuthHeaderDTOObj.setPriorAuthWorkedBy("Test");
		 priorAuthHeaderDTOObj.setExtractPerformedQuantity((short)100);
		
		 priorAuthHeaderObj.setPriorAuthHeaderDTO(priorAuthHeaderDTOObj);
		 getPriorAuthHeaderResponse.set_return(priorAuthHeaderObj);
		 return getPriorAuthHeaderResponse;  
    }
    
    /**
     * TODO: Remove below mock code
     * @return
     */
    private  PriorAuthHeaderServiceStub.SavePriorAuthHeaderResponse savePriorAuthHeaderResponseSuccessMock(){
    	 PriorAuthHeaderServiceStub.SavePriorAuthHeaderResponse savePriorAuthHeaderResponse = new PriorAuthHeaderServiceStub.SavePriorAuthHeaderResponse();
         
         PriorAuthHeader priorAuthHeaderObj = new PriorAuthHeader();
         DiagnosisDTO diagnosisDTO = new DiagnosisDTO();
		 diagnosisDTO.setIcdDiagnosisCode("LCDDCode");
		 diagnosisDTO.setSequenceNumber(1);
		 priorAuthHeaderObj.setDiagnosis(new DiagnosisDTO[]{diagnosisDTO});
		 
		 ProcedureDTO procedureDTO = new ProcedureDTO();
		 procedureDTO.setLabTestTypeName("LabTestType");
		 procedureDTO.setProcedureDecision("ProDecision");
		 procedureDTO.setProcedureDecisionReason("ProReason");
		 procedureDTO.setProcedureFastTrack("FastTrack");
		 procedureDTO.setProcedureFromCode("PFromCode");
		 procedureDTO.setProcedureToCode("PTocode");
		 procedureDTO.setProcedureUnits(0.1f);
		 
		 priorAuthHeaderObj.setProcedures(new ProcedureDTO[]{procedureDTO});
		 
		 PriorAuthHeaderDTO priorAuthHeaderDTOObj = new PriorAuthHeaderDTO();
		 priorAuthHeaderDTOObj.setAppCreateDatetime("2018-04-26 01:02:00.1");
		 priorAuthHeaderDTOObj.setAppCreateUserId("UserId");
		 priorAuthHeaderDTOObj.setAppMaintDatetime("2018-04-26 01:02:00.1");
		 priorAuthHeaderDTOObj.setAuthInboundChannel("IChannel");
		 priorAuthHeaderDTOObj.setAuthNumberSearched("22");
		 priorAuthHeaderDTOObj.setAuthRequestedDate("2018-04-26 01:02:00.1");
		 priorAuthHeaderDTOObj.setExtractPerformedQuantity((short)100);
		 priorAuthHeaderDTOObj.setHealthPlanGroupId("HPGid");
		 priorAuthHeaderDTOObj.setHealthPlanId("HPId");
		 priorAuthHeaderDTOObj.setIcdRevisionNumber("RevNo");
		 priorAuthHeaderDTOObj.setIdCardNumber("22");
		 priorAuthHeaderDTOObj.setMasterPatientId("MPId");
		 priorAuthHeaderDTOObj.setMemberNumber("22");
		 priorAuthHeaderDTOObj.setPatientGenderRelationshipCode("CO1");
		 priorAuthHeaderDTOObj.setPriorAuthBeginServiceDate("11/11/2018");
		 priorAuthHeaderDTOObj.setPriorAuthBusinessSectorCode("SecCode");
		 priorAuthHeaderDTOObj.setPriorAuthBusinessSectorDescription("SecDesc");
		 priorAuthHeaderDTOObj.setPriorAuthBusinessSegmentCode("Segcode");
		 priorAuthHeaderDTOObj.setPriorAuthBusinessSegmentDescription("SegDesc");
		 priorAuthHeaderDTOObj.setPriorAuthCurrentStatusDatetime("2018-04-26 01:02:00.1");
		 priorAuthHeaderDTOObj.setPriorAuthCurrentStatusUserId("UId");
		 priorAuthHeaderDTOObj.setPriorAuthDueDatetime("2018-04-26 01:02:00.1");
		 priorAuthHeaderDTOObj.setPriorAuthEndServiceDate("2018-04-26 01:02:00.1");
		 priorAuthHeaderDTOObj.setPriorAuthHeaderKey(22);
		 priorAuthHeaderDTOObj.setPriorAuthNumber("22");
		 priorAuthHeaderDTOObj.setPriorAuthOriginalStatusDatetime("12/12/2012 11:11:11");
		 priorAuthHeaderDTOObj.setPriorAuthOriginalStatusUserId("StatusUserId");
		 priorAuthHeaderDTOObj.setPriorAuthPatientBirthDate("1992-04-26 01:02:00.1");
		 priorAuthHeaderDTOObj.setPriorAuthPatientFirstName("PFirst");
		 priorAuthHeaderDTOObj.setPriorAuthPatientGenderCode("M");
		 priorAuthHeaderDTOObj.setPriorAuthPatientLastName("PLast");
		 priorAuthHeaderDTOObj.setPriorAuthPatientMiddleName("PMiddle");
		 priorAuthHeaderDTOObj.setPriorAuthPatientSuffixName("PSuffix");
		 priorAuthHeaderDTOObj.setPriorAuthPriorityCode("PC");
		 priorAuthHeaderDTOObj.setPriorAuthStatusCode("ASC");
		 priorAuthHeaderDTOObj.setPriorAuthStatusDesc("ASD");
		 priorAuthHeaderDTOObj.setPriorAuthSubmissionStatusCode("ASub");
		 priorAuthHeaderDTOObj.setPriorAuthWithdrawnReasonCode("RECo");
		 priorAuthHeaderDTOObj.setPriorAuthWithdrawnReasonDesc("REDesc");
		 priorAuthHeaderDTOObj.setPriorAuthWorkedBy("Test");
		 priorAuthHeaderDTOObj.setExtractPerformedQuantity((short)100);
		 priorAuthHeaderObj.setPriorAuthHeaderDTO(priorAuthHeaderDTOObj);
		 savePriorAuthHeaderResponse.set_return(priorAuthHeaderObj);
		 return savePriorAuthHeaderResponse;
    }
}