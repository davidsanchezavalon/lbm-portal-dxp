/**
 * Description
 *		This file contain the getter and setter methods for the Post Service Review Provider Information.
 *
 * CHANGE History
 * 		Version 1.0
 * 			Initial version copied from avalon-prior-authorization.
 * 		Version 1.1
 *			Added changes for the initial version of Post Service Review.
 *  
 */

package com.avalon.lbm.portlets.postservice.model;

import java.io.Serializable;

public class ProviderInformationFO implements Serializable {
	/**
	 * To store the provider information fields
	 */
	private static final long serialVersionUID = 1L;
	public String requestingProviderType;
	public String requestingProviderName;
	public String requestingNpi;
	public String requestingTinEin;
	public String requestingPhoneNumber;
	public String requestingFaxNumber;
	public String requestingAddressLine1;
	public String requestingAddressLine2;
	public String requestingCity;
	public String requestingState;
	public String requestingZip;


	public String getRequestingProviderType() {
		return requestingProviderType;
	}
	public void setRequestingProviderType(String requestingProviderType) {
		this.requestingProviderType = requestingProviderType;
	}

	public String getRequestingProviderName() {
		return requestingProviderName;
	}
	public void setRequestingProviderName(String requestingProviderName) {
		this.requestingProviderName = requestingProviderName;
	}
	
	public String getRequestingNpi() {
		return requestingNpi;
	}
	public void setRequestingNpi(String requestingNpi) {
		this.requestingNpi = requestingNpi;
	}
	
	public String getRequestingTinEin() {
		return requestingTinEin;
	}
	public void setRequestingTinEin(String requestingTinEin) {
		this.requestingTinEin = requestingTinEin;
	}

	public String getRequestingPhoneNumber() {
		return requestingPhoneNumber;
	}
	public void setRequestingPhoneNumber(String requestingPhoneNumber) {
		this.requestingPhoneNumber = requestingPhoneNumber;
	}
	
	public String getRequestingFaxNumber() {
		return requestingFaxNumber;
	}
	public void setRequestingFaxNumber(String requestingFaxNumber) {
		this.requestingFaxNumber = requestingFaxNumber;
	}
	
	public String getRequestingAddressLine1() {
		return requestingAddressLine1;
	}
	public void setRequestingAddressLine1(String requestingAddressLine1) {
		this.requestingAddressLine1 = requestingAddressLine1;
	}
	
	public String getRequestingAddressLine2() {
		return requestingAddressLine2;
	}
	public void setRequestingAddressLine2(String requestingAddressLine2) {
		this.requestingAddressLine2 = requestingAddressLine2;
	}
	
	public String getRequestingCity() {
		return requestingCity;
	}
	public void setRequestingCity(String requestingCity) {
		this.requestingCity = requestingCity;
	}
	
	public String getRequestingState() {
		return requestingState;
	}
	public void setRequestingState(String requestingState) {
		this.requestingState = requestingState;
	}
	
	public String getRequestingZip() {
		return requestingZip;
	}
	public void setRequestingZip(String requestingZip) {
		this.requestingZip = requestingZip;
	}
}
