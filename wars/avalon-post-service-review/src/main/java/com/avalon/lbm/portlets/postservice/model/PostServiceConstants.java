package com.avalon.lbm.portlets.postservice.model;

/**
 * Description
 *		This file contain the getter and setter methods for the Post Service Review Constants.
 *
 * CHANGE History
 * 		Version 1.0
 * 			Initial version copied from avalon-prior-authorization.
 * 		Version 1.1
 *			Added changes for the initial version of Post Service Review.
 *  	Version 1.2			12/04/2017
 * 			Removed constant for email address "AvalonPSPPR@avalonhcs.com"
 *			Added constants for "Provider-Advocate@Avalonhcs.com" and "BCBSSC Email Request"
 *  	Version 1.3			03/07/2017
 *  		Added a comstant for the properties file.
 *
 */

public interface PostServiceConstants {
	public static final String PROPERTIES_FILE ="lbm-common.properties";
	
    public static final String INTAKEREVIEW = "Intake Review";
    public static final String INTAKETYPECODE = "INTAKE";
    public static final String NOTIFICATION = "Notification";
    public static final String NURSEADD = "Nurse Add";
    public static final String NURSEREVIEW = "Nurse Review";
    public static final String NURSETYPECODE = "NURSE";
    public static final String NURSEADDTYPECODE = "NURSE-ADDL";
    public static final String PEERREVIEW = "Peer Review";
    public static final String PEERTYPECODE = "PEER";
    public static final String PHYSICIANREVIEW = "Physician Review";
    public static final String PHYSICIANTYPECODE = "PHYSICIAN";
    public static final String PROCEDURE = "Procedure";
    public static final String PROCEDURESUBMISSION = "ProcedureSubmission";
    public static final String Provider = "Provider";
    public static final String RECONSIDERATION = "Reconsideration";
    public static final String RECONSIDERATIONTYPECODE = "RECONSIDERATION";

    public static final String ADVOCATE_DESC_STR="Provider-Advocate@Avalonhcs.com";
    public static final String ADVOCATE_STR="Provider-Advocate";
    public static final String EMAIL_STR="BCBSSC Email Request";
    public static final String PHONE_STR="Phone";
    public static final String FAX_STR="Fax";
    public static final String WEB_STR="Web";
    
    public static final String ROUTINE_STR="Routine";
    public static final String URGENT_STR="Urgent";

    public static final String IN_PROCESS_INTAKE_REVIEW_DESC			= "In Process - Intake Review";
    public static final String IN_PROCESS_READY_FOR_COMPL_DESC			= "In Process - Intake Ready for Completion";
    public static final String IN_PROCESS_NURSE_REVIEW_DESC				= "In Process - Nurse Review";
    public static final String IN_PROCESS_PHYSICIAN_REVIEW_DESC			= "In Process - Physician Review";
    public static final String COMPLETED_OVERTURNED_DESC				= "Completed - Overturned";
    public static final String COMPLETED_PARTIALLY_OVERTURNED_DESC		= "Completed - Partially Overturned";
    public static final String COMPLETED_DENIED_DESC					= "Completed - Denied";
    public static final String COMPLETED_ADMINISTRATIVE_DENIAL_DESC		= "Completed - Administrative Denial";
    public static final String COMPLETED_NO_ADDL_CLINICAL_DENIAL_DESC	= "Completed - No Addl Clinical Information Denial";
    public static final String COMPLETED_CLINICAL_DENIAL_DESC			= "Completed - Clinical Denial";
    public static final String COMPLETED_WITHDRAWN_DESC					= "Completed - Withdrawn";

    public static final String IN_PROCESS_INTAKE_REVIEW_CODE			= "IP - Intk Rev";
    public static final String IN_PROCESS_READY_FOR_COMPL_CODE			= "IP - Intk Rdy for Cmpl";
    public static final String IN_PROCESS_NURSE_REVIEW_CODE				= "IP - Nrse Rev";
    public static final String IN_PROCESS_PHYSICIAN_REVIEW_CODE			= "IP - Phys Rev";
    public static final String COMPLETED_OVERTURNED_CODE				= "Cmpl - Overt";
    public static final String COMPLETED_PARTIALLY_OVERTURNED_CODE		= "Cmpl - Prtl Overt";
    public static final String COMPLETED_DENIED_CODE					= "Cmpl - Denial";
    public static final String COMPLETED_ADMINISTRATIVE_DENIAL_CODE		= "Cmpl - Adm Denial";
    public static final String COMPLETED_NO_ADDL_CLINICAL_DENIAL_CODE	= "Cmpl - No Add Cl Info Den";
    public static final String COMPLETED_CLINICAL_DENIAL_CODE			= "Cmpl - Cl Denial";
    public static final String COMPLETED_WITHDRAWN_CODE					= "Cmpl - Withdrawn";

    public static final String DUPLICATE_DESC					= "Duplicate";
    public static final String PROVIDER_WITHDRAWN_DESC			= "Provider Withdrawn";
    public static final String AUTH_ENTERED_IN_ERROR_DESC		= "Auth Entered In Error";
    public static final String NON_PARTICIPATING_MEMBER_DESC	= "Non-Participating Member";

    public static final String DUPLICATE_CODE					= "Dupe";
    public static final String PROVIDER_WITHDRAWN_CODE			= "Pvd Withdrawn";
    public static final String AUTH_ENTERED_IN_ERROR_CODE		= "Enter Error";
    public static final String NON_PARTICIPATING_MEMBER_CODE	= "Non-Par Mbr";
    
    // Provider Types
    public static final String ORDERING							= "Ordering";
    public static final String RENDERING_INNETWORK				= "Rendering In-Network";
    public static final String RENDERING_OUTOFNETWORK			= "Rendering Out-of-Network";
    //TODO: Remove below code
    public static final String ENABLED_MOCK_SERVICE = "enabled.mock.service";
  }
