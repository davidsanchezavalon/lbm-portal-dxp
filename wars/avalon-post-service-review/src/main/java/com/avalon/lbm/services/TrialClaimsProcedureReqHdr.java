
package com.avalon.lbm.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TrialClaimsProcedureReqHdr complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TrialClaimsProcedureReqHdr">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="enumerationShortDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="enumerationSetCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="logicalDeleteIndicator" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="enumerationCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="type" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TrialClaimsProcedureReqHdr", propOrder = {
    "enumerationShortDescription",
    "enumerationSetCode",
    "logicalDeleteIndicator",
    "enumerationCode",
    "type"
})
public class TrialClaimsProcedureReqHdr {

    protected String enumerationShortDescription;
    protected String enumerationSetCode;
    protected String logicalDeleteIndicator;
    protected String enumerationCode;
    protected String type;

    /**
     * Gets the value of the enumerationShortDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEnumerationShortDescription() {
        return enumerationShortDescription;
    }

    /**
     * Sets the value of the enumerationShortDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEnumerationShortDescription(String value) {
        this.enumerationShortDescription = value;
    }

    /**
     * Gets the value of the enumerationSetCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEnumerationSetCode() {
        return enumerationSetCode;
    }

    /**
     * Sets the value of the enumerationSetCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEnumerationSetCode(String value) {
        this.enumerationSetCode = value;
    }

    /**
     * Gets the value of the logicalDeleteIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLogicalDeleteIndicator() {
        return logicalDeleteIndicator;
    }

    /**
     * Sets the value of the logicalDeleteIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLogicalDeleteIndicator(String value) {
        this.logicalDeleteIndicator = value;
    }

    /**
     * Gets the value of the enumerationCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEnumerationCode() {
        return enumerationCode;
    }

    /**
     * Sets the value of the enumerationCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEnumerationCode(String value) {
        this.enumerationCode = value;
    }

    /**
     * Gets the value of the type property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getType() {
        return type;
    }

    /**
     * Sets the value of the type property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setType(String value) {
        this.type = value;
    }

}
