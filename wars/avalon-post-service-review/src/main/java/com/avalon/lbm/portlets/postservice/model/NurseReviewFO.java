/**
 * Description
 *		This file contain the getter and setter methods for the Post Service Review Nurse Review.
 *
 * CHANGE History
 * 		Version 1.0
 * 			Initial version copied from avalon-prior-authorization.
 *  
 */

package com.avalon.lbm.portlets.postservice.model;

import java.io.Serializable;

import java.util.List;

public class NurseReviewFO  implements Serializable{
	
	/**
	 * To store the NurseReviewFO fields
	 */
	private static final long serialVersionUID = 1695494364583835166L;
	private List<String> nurseNotesRead;
	private String nurseNotesCreate;
	private String requestedDate;
	private String returnedDate;
	private String requestedBy;
	private String returnedBy;
	
	private String requestedInfo;
	public String getRequestedInfo() {
		return requestedInfo;
	}
	public void setRequestedInfo(String requestedInfo) {
		this.requestedInfo = requestedInfo;
	}
	private String saveNotes;
	private Boolean isReceived;
	private String requestedDateAfter;
	
	public String getRequestedDateAfter() {
		return requestedDateAfter;
	}
	public void setRequestedDateAfter(String requestedDateAfter) {
		this.requestedDateAfter = requestedDateAfter;
	}
	private String requestedByAfter;
	
	public String getRequestedByAfter() {
		return requestedByAfter;
	}
	public void setRequestedByAfter(String requestedByAfter) {
		this.requestedByAfter = requestedByAfter;
	}
	
	public Boolean getIsReceived() {
		return isReceived;
	}
	public void setIsReceived(Boolean isReceived) {
		this.isReceived = isReceived;
	}
	public String getSaveNotes() {
		return saveNotes;
	}
	public void setSaveNotes(String saveNotes) {
		this.saveNotes = saveNotes;
	}
	public List<String> getNurseNotesRead() {
		return nurseNotesRead;
	}
	public void setNurseNotesRead(List<String> nurseNotesRead) {
		this.nurseNotesRead = nurseNotesRead;
	}
	public String getNurseNotesCreate() {
		return nurseNotesCreate;
	}
	public void setNurseNotesCreate(String nurseNotesCreate) {
		this.nurseNotesCreate = nurseNotesCreate;
	}
	public String getRequestedDate() {
		return requestedDate;
	}
	public void setRequestedDate(String requestedDate) {
		this.requestedDate = requestedDate;
	}
	public String getReturnedDate() {
		return returnedDate;
	}
	public void setReturnedDate(String returnedDate) {
		this.returnedDate = returnedDate;
	}
	public String getRequestedBy() {
		return requestedBy;
	}
	public void setRequestedBy(String requestedBy) {
		this.requestedBy = requestedBy;
	}
	public String getReturnedBy() {
		return returnedBy;
	}
	public void setReturnedBy(String returnedBy) {
		this.returnedBy = returnedBy;
	}
	

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("NurseReviewFO [nurseNotesRead=");
		builder.append(nurseNotesRead);
		builder.append(", nurseNotesCreate=");
		builder.append(nurseNotesCreate);
		builder.append(", requestedDate=");
		builder.append(requestedDate);
		builder.append(", returnedDate=");
		builder.append(returnedDate);
		builder.append(", requestedBy=");
		builder.append(requestedBy);
		builder.append(", returnedBy=");
		builder.append(returnedBy);
		builder.append(", requestedInfo=");
		builder.append(requestedInfo);
		builder.append(", saveNotes=");
		builder.append(saveNotes);
		builder.append(", isReceived=");
		builder.append(isReceived);
		builder.append(", requestedDateAfter=");
		builder.append(requestedDateAfter);
		builder.append(", requestedByAfter=");
		builder.append(requestedByAfter);
		builder.append("]");
		return builder.toString();
	}
	
	
	
	
}
