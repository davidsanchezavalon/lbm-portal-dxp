
package com.avalon.lbm.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getEnumShrtDescription complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getEnumShrtDescription">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="getEnumShrtDescriptionRequest" type="{http://services.lbm.avalon.com/}TrialClaimsProcedureReqHdr" minOccurs="0" form="qualified"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getEnumShrtDescription", propOrder = {
    "getEnumShrtDescriptionRequest"
})
public class GetEnumShrtDescription {

    @XmlElement(namespace = "http://services.lbm.avalon.com/")
    protected TrialClaimsProcedureReqHdr getEnumShrtDescriptionRequest;

    /**
     * Gets the value of the getEnumShrtDescriptionRequest property.
     * 
     * @return
     *     possible object is
     *     {@link TrialClaimsProcedureReqHdr }
     *     
     */
    public TrialClaimsProcedureReqHdr getGetEnumShrtDescriptionRequest() {
        return getEnumShrtDescriptionRequest;
    }

    /**
     * Sets the value of the getEnumShrtDescriptionRequest property.
     * 
     * @param value
     *     allowed object is
     *     {@link TrialClaimsProcedureReqHdr }
     *     
     */
    public void setGetEnumShrtDescriptionRequest(TrialClaimsProcedureReqHdr value) {
        this.getEnumShrtDescriptionRequest = value;
    }

}
