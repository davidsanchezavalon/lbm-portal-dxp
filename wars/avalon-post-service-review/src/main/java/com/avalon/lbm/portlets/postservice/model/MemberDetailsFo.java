/**
 * Description
 *		This file contain the getter and setter methods for the Post Service Member Information.
 *
 * CHANGE History
 * 		Version 1.0
 * 			Initial version copied from avalon-prior-authorization.
 *  
 */

package com.avalon.lbm.portlets.postservice.model;

import java.io.Serializable;

public class MemberDetailsFo implements Serializable{
	
	/**
	 *  To store the MemberDetails fields
	 *  
	 */
    
	private static final long serialVersionUID = -4732267506513048283L;
	private String memberId;
	private String memberFirstName;
	private String memberLastName;
	private String patientId;
	private String healthPlan;
	private String healthPlanName;
	private String memberNumber;
	private String healthPlanGroupValue;	
	private String memberDob;
	private String groupId;
	private String gender;
	private String subscriberId;
	private String memberRelationship;
	private String mpi;
	/**
	 * @return the memberNumber
	 */
	public String getMemberNumber() {
		return memberNumber;
	}

	/**
	 * @param memberNumber the memberNumber to set
	 */
	public void setMemberNumber(String memberNumber) {
		this.memberNumber = memberNumber;
	}

	/**
	 * @return the healthPlanName
	 */
	public String getHealthPlanName() {
		return healthPlanName;
	}

	/**
	 * @param healthPlanName the healthPlanName to set
	 */
	public void setHealthPlanName(String healthPlanName) {
		this.healthPlanName = healthPlanName;
	}
	
	
	
	public MemberDetailsFo(){
		
	}
	
	/**
	 * @return the mpi
	 */
	public String getMpi() {
		return mpi;
	}
	
	
	/**
	 * @return the healthPlanGroupValue
	 */
	public String getHealthPlanGroupValue() {
		return healthPlanGroupValue;
	}

	/**
	 * @param healthPlanGroupValue the healthPlanGroupValue to set
	 */
	public void setHealthPlanGroupValue(String healthPlanGroupValue) {
		this.healthPlanGroupValue = healthPlanGroupValue;
	}
	/**
	 * @param mpi the mpi to set
	 */
	public void setMpi(String mpi) {
		this.mpi = mpi;
	}
	/**
	 * @return the patientId
	 */
	public String getPatientId() {
		return patientId;
	}
	/**
	 * @param patientId the patientId to set
	 */
	public void setPatientId(String patientId) {
		this.patientId = patientId;
	}
	
	public String getSubscriberId() {
		return subscriberId;
	}
	public void setSubscriberId(String subscriberId) {
		this.subscriberId = subscriberId;
	}
	public String getMemberRelationship() {
		return memberRelationship;
	}
	public void setMemberRelationship(String memberRelationship) {
		this.memberRelationship = memberRelationship;
	}
	

	public String getMemberId() {
		return memberId;
	}
	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}
	public String getMemberFirstName() {
		return memberFirstName;
	}
	public void setMemberFirstName(String memberFirstName) {
		this.memberFirstName = memberFirstName;
	}
	public String getMemberLastName() {
		return memberLastName;
	}
	public void setMemberLastName(String memberLastName) {
		this.memberLastName = memberLastName;
	}
	public String getHealthPlan() {
		return healthPlan;
	}
	public void setHealthPlan(String healthPlan) {
		this.healthPlan = healthPlan;
	}
	public String getMemberDob() {
		return memberDob;
	}
	public void setMemberDob(String memberDob) {
		this.memberDob = memberDob;
	}
	
	public String getGroupId() {
		return groupId;
	}
	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	
}
