/**
 * Description
 *		This file contain the getter and setter methods for the Post Service Review Intake Review.
 *
 * CHANGE History
 * 		Version 1.0
 * 			Initial version copied from avalon-prior-authorization.
 *  
 */

package com.avalon.lbm.portlets.postservice.model;

import java.io.Serializable;
import java.util.List;

public class IntakeReviewFO implements Serializable{

	/**
	 *  To store the IntakeReview fields
	 */
	private static final long serialVersionUID = -1447617076770499452L;
	private List<String> intakeNotesRead;
	private String intakeNotesCreate;
	
	public List<String> getIntakeNotesRead() {
		return intakeNotesRead;
	}
	public void setIntakeNotesRead(List<String> intakeEnNotes) {
		this.intakeNotesRead = intakeEnNotes;
	}
	public String getIntakeNotesCreate() {
		return intakeNotesCreate;
	}
	public void setIntakeNotesCreate(String intakeNotesCreate) {
		this.intakeNotesCreate = intakeNotesCreate;
	}
	
}
