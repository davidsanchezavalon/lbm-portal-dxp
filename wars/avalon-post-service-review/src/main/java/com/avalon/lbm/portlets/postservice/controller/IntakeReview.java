/**
 * Description
 *		This file contain the controller methods for the Post Service Review Intake Review Page.
 *
 * CHANGE History
 * 		Version 1.0
 * 			Initial version copied from avalon-prior-authorization.
 * 		Version 1.1					11/08/2017
 *			Added changes for related PAs.
 *			Added three number fields.
 * 		Version 1.2					08/27/2018
 * 			Do not display a blank note.
 *  
 */

package com.avalon.lbm.portlets.postservice.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletRequest;
import javax.portlet.PortletSession;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.apache.axis2.AxisFault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;

import com.avalon.lbm.portlets.postservice.model.CurrentAuthorizationDetailsFO;
import com.avalon.lbm.portlets.postservice.model.IntakeReviewFO;
import com.avalon.lbm.portlets.postservice.model.PostServiceConstants;
import com.avalon.lbm.portlets.postservice.util.AgeCalculation;
import com.avalon.lbm.portlets.postservice.util.PostServiceUtil;
import com.avalon.lbm.services.DAOExceptionException;
import com.avalon.lbm.services.PriorAuthHeaderServiceStub;
import com.avalon.lbm.services.PriorAuthHeaderServiceStub.PriorAuthHeaderDTO;
import com.avalon.lbm.services.PriorAuthReviewServiceStub;
import com.avalon.lbm.services.PriorAuthReviewServiceStub.PriorAuthHeader;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.ParamUtil;

@Controller(value = "IntakeReview")
@RequestMapping(value = { "VIEW" })
public class IntakeReview {
	
	private static final Log log = LogFactoryUtil.getLog(IntakeReview.class.getName());

    /**** Render method which gets called during initial load of JSP *************************/
    @RenderMapping
    public String handleRenderRequest(RenderRequest request, 
    		                          RenderResponse response, 
    		                          Model model) throws RemoteException {

	    log.info("Processing Render Action for Intake Review Enter");
	   	String sourceTab = PostServiceUtil.intakeReviewTab;
        PortletSession portletSession = request.getPortletSession();

        Long authorizationKey = (Long) portletSession.getAttribute("authorizationKey", PortletSession.APPLICATION_SCOPE);
        log.info("(render) Authorization Key " + authorizationKey);

        CurrentAuthorizationDetailsFO currentAuthorizationDetails = PostServiceUtil.getHeaderDetails(portletSession, sourceTab);

        // Set the authorization age
        ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute("LIFERAY_SHARED_THEME_DISPLAY");
        String appDate = (String) portletSession.getAttribute("CreateDatetime", PortletSession.APPLICATION_SCOPE);
        currentAuthorizationDetails.setAuthorizationAge(PostServiceUtil.setAuthorizationAge(portletSession, appDate, authorizationKey, themeDisplay));

        request.setAttribute("currentAuthorizationDetailsFO", (Object) currentAuthorizationDetails);

        if (authorizationKey == null) {
            log.info("(render) Authorization key is null");
        } else {
            PriorAuthReviewServiceStub priorAuthReviewServiceStub = new PriorAuthReviewServiceStub();
            PriorAuthReviewServiceStub.SearchPriorAuthNoteDetails searchPriorAuthNoteDetails = new PriorAuthReviewServiceStub.SearchPriorAuthNoteDetails();
            PriorAuthReviewServiceStub.NoteSearchCriteria noteSearchCriteria = new PriorAuthReviewServiceStub.NoteSearchCriteria();
            noteSearchCriteria.setAuthorizationKey(authorizationKey.longValue());
            noteSearchCriteria.setSourceTab(sourceTab);
            searchPriorAuthNoteDetails.setSearchCriteriaNoteRequest(noteSearchCriteria);
            PriorAuthReviewServiceStub.SearchPriorAuthNoteDetailsE searchPriorAuthNoteDetailsE = new PriorAuthReviewServiceStub.SearchPriorAuthNoteDetailsE();
            searchPriorAuthNoteDetailsE.setSearchPriorAuthNoteDetails(searchPriorAuthNoteDetails);
            
            PriorAuthReviewServiceStub.SearchPriorAuthNoteDetailsResponseE searchPriorAuthNoteDetailsResponseE =null;
            PriorAuthReviewServiceStub.SearchPriorAuthNoteDetailsResponse searchPriorAuthNoteDetailsResponse = null;
            
            //TODO: Remove mock service code and condition. Don't remove else condition code
			if(GetterUtil.getBoolean(com.liferay.portal.kernel.util.PropsUtil.get(PostServiceConstants.ENABLED_MOCK_SERVICE))){
				 //Success Mock Code
				searchPriorAuthNoteDetailsResponseE = new PriorAuthReviewServiceStub.SearchPriorAuthNoteDetailsResponseE();
	            searchPriorAuthNoteDetailsResponse = new PriorAuthReviewServiceStub.SearchPriorAuthNoteDetailsResponse();
	            PriorAuthHeader priorAuthHeader = new PriorAuthHeader();
		        priorAuthHeader.setAuthorizationKey(100L);
			    priorAuthHeader.setNoteCreateDatetime("2015-12-12 12:12:22.222");
			    priorAuthHeader.setNoteCreateUserId("UserId");
			    priorAuthHeader.setPriorAuthNoteText("Notetext");
			    priorAuthHeader.setPriorAuthNoteTypeCode("AuthType");
			    priorAuthHeader.setSourceTab("STab");
		        searchPriorAuthNoteDetailsResponse.set_return(new PriorAuthHeader[]{priorAuthHeader});
	            searchPriorAuthNoteDetailsResponseE.setSearchPriorAuthNoteDetailsResponse(searchPriorAuthNoteDetailsResponse);
	        }else{
				searchPriorAuthNoteDetailsResponseE = priorAuthReviewServiceStub.searchPriorAuthNoteDetails(searchPriorAuthNoteDetailsE);
	            searchPriorAuthNoteDetailsResponse = searchPriorAuthNoteDetailsResponseE.getSearchPriorAuthNoteDetailsResponse();
			}
            IntakeReviewFO intakeReviewFO = new IntakeReviewFO();
            ArrayList<String> intakeEnNotes = new ArrayList<String>();
            if (searchPriorAuthNoteDetailsResponse.get_return() != null) {
            	for (int i = 0; i < searchPriorAuthNoteDetailsResponse.get_return().length; ++i) {
                    String creationDateTimeIn = searchPriorAuthNoteDetailsResponse.get_return()[i].getNoteCreateDatetime();
                    log.info("(render) Creation Date Time of Intake: " + creationDateTimeIn);
	                String frmDateStr = AgeCalculation.getAgeCalculation().getTimeForNote(creationDateTimeIn);

	                // Do not add a blank note
                    if (!searchPriorAuthNoteDetailsResponse.get_return()[i].getPriorAuthNoteText().equals("")){
	                    intakeEnNotes.add(searchPriorAuthNoteDetailsResponse.get_return()[i].getNoteCreateUserId() + "(" + frmDateStr + ")" + " : " +
	                    	searchPriorAuthNoteDetailsResponse.get_return()[i].getPriorAuthNoteText());
	                    log.info("(render) Intake note " + (i + 1) + ": " + searchPriorAuthNoteDetailsResponse.get_return()[i].getNoteCreateUserId() +
	                    	"(" + frmDateStr + ")" + " : " + searchPriorAuthNoteDetailsResponse.get_return()[i].getPriorAuthNoteText());
	                }
                }

                // Get the claim and document numbers
                Object priorAuthClaimNumber = (Object) searchPriorAuthNoteDetailsResponse.get_return()[0].getPriorAuthClaimNumber();
                Object priorAuthHpClaimNumber = (Object) searchPriorAuthNoteDetailsResponse.get_return()[0].getPriorAuthHpClaimNumber();
                Object priorAuthDocumentNumber = (Object) searchPriorAuthNoteDetailsResponse.get_return()[0].getPriorAuthSubDocumentNumber();

                request.setAttribute("priorAuthClaimNumber", priorAuthClaimNumber);
				log.info("(render) priorAuthClaimNumber: " + (String) priorAuthClaimNumber);
				request.setAttribute("priorAuthHpClaimNumber", priorAuthHpClaimNumber);
				log.info("(render) priorAuthHpClaimNumber: " + (String) priorAuthHpClaimNumber);
				request.setAttribute("priorAuthDocumentNumber", priorAuthDocumentNumber);
				log.info("(render) priorAuthDocumentNumber: " + (String) priorAuthDocumentNumber);
                
                // Get the related PAs from the first note (they are the same for all notes)
                Object relatedPa1  = (Object) searchPriorAuthNoteDetailsResponse.get_return()[0].getPriorAuthCode1();
                Object relatedPa2  = (Object) searchPriorAuthNoteDetailsResponse.get_return()[0].getPriorAuthCode2();
                Object relatedPa3  = (Object) searchPriorAuthNoteDetailsResponse.get_return()[0].getPriorAuthCode3();
                Object relatedPa4  = (Object) searchPriorAuthNoteDetailsResponse.get_return()[0].getPriorAuthCode4();
                Object relatedPa5  = (Object) searchPriorAuthNoteDetailsResponse.get_return()[0].getPriorAuthCode5();
                Object relatedPa6  = (Object) searchPriorAuthNoteDetailsResponse.get_return()[0].getPriorAuthCode6();
                Object relatedPa7  = (Object) searchPriorAuthNoteDetailsResponse.get_return()[0].getPriorAuthCode7();
                Object relatedPa8  = (Object) searchPriorAuthNoteDetailsResponse.get_return()[0].getPriorAuthCode8();
                Object relatedPa9  = (Object) searchPriorAuthNoteDetailsResponse.get_return()[0].getPriorAuthCode9();
                Object relatedPa10 = (Object) searchPriorAuthNoteDetailsResponse.get_return()[0].getPriorAuthCode10();
                Object relatedPa11 = (Object) searchPriorAuthNoteDetailsResponse.get_return()[0].getPriorAuthCode11();
                Object relatedPa12 = (Object) searchPriorAuthNoteDetailsResponse.get_return()[0].getPriorAuthCode12();

                request.setAttribute("relatedPa1", relatedPa1);
				log.info("(render) relatedPa1: " + (String) relatedPa1);
				request.setAttribute("relatedPa2", relatedPa2);
				log.info("(render) relatedPa2: " + (String) relatedPa2);
				request.setAttribute("relatedPa3", relatedPa3);
				log.info("(render) relatedPa3: " + (String) relatedPa3);
				request.setAttribute("relatedPa4", relatedPa4);
				log.info("(render) relatedPa4: " + (String) relatedPa4);
				request.setAttribute("relatedPa5", relatedPa5);
				log.info("(render) relatedPa5: " + (String) relatedPa5);
				request.setAttribute("relatedPa6", relatedPa6);
				log.info("(render) relatedPa6: " + (String) relatedPa6);
				request.setAttribute("relatedPa7",  relatedPa7);
				log.info("(render) relatedPa7: " + (String) relatedPa7);
				request.setAttribute("relatedPa8", relatedPa8);
				log.info("(render) relatedPa8: " + (String) relatedPa5);
				request.setAttribute("relatedPa9", relatedPa9);
				log.info("(render) relatedPa9: " + (String) relatedPa9);
				request.setAttribute("relatedPa10", relatedPa10);
				log.info("(render) relatedPa10: " + (String) relatedPa10);
				request.setAttribute("relatedPa11", relatedPa11);
				log.info("(render) relatedPa11: " + (String) relatedPa11);
				request.setAttribute("relatedPa12", relatedPa12);
				log.info("(render) relatedPa12: " + (String) relatedPa12);

                portletSession.setAttribute("intakeEnNotes", intakeEnNotes, PortletSession.APPLICATION_SCOPE);
                intakeReviewFO.setIntakeNotesRead(intakeEnNotes);
                List readData = intakeReviewFO.getIntakeNotesRead();
                request.setAttribute("readData", (Object) readData);
				log.info("(render) readData: " + readData);
				
                request.setAttribute("intakeReviewFO", (Object) intakeReviewFO);
            }
        }
	    log.info("Processing Render Action for Intake Review Exit");
        return "view";
    }

    /**************Action method which gets called on click of SAVE button***************************************/
    /**
     * @param request
     * @param response
     * @param intakeReviewFO
     * @throws RemoteException
     */
    @ActionMapping(params = { "action=intakeReviewAction" })
    public void intakeReviewDetails(ActionRequest request, 
    		                        ActionResponse response, 
    		                        @ModelAttribute(value = "intakeReviewFO") IntakeReviewFO intakeReviewFO) throws RemoteException {

	    log.info("Processing Save Action for Intake Review Enter");
		
	   	String sourceTab = PostServiceUtil.intakeReviewTab;
	   	String noteType = PostServiceUtil.intakeReviewNote;
        ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute("LIFERAY_SHARED_THEME_DISPLAY");
        PortletSession portletSession = request.getPortletSession();
        Long authorizationKey = (Long) portletSession.getAttribute("authorizationKey", PortletSession.APPLICATION_SCOPE);
        String creationDate = AgeCalculation.getAgeCalculation().createDate();

        PriorAuthReviewServiceStub priorAuthReviewServiceStub = new PriorAuthReviewServiceStub();
        PriorAuthReviewServiceStub.CreatePriorAuthNoteDetails createPriorAuthNoteDetails = new PriorAuthReviewServiceStub.CreatePriorAuthNoteDetails();
        PriorAuthReviewServiceStub.CreatePriorAuthNoteDetailsE CreatePriorAuthNoteDetailsE2 = new PriorAuthReviewServiceStub.CreatePriorAuthNoteDetailsE();
        PriorAuthReviewServiceStub.PriorAuthHeader priorAuthHeader = new PriorAuthReviewServiceStub.PriorAuthHeader();
        
        // Get the claim and document numbers
        String claimNumber = ParamUtil.getString((PortletRequest) request, (String) "avalonClaimNumber");
        String hpClaimNumber = ParamUtil.getString((PortletRequest) request, (String) "hpClaimNumber");
        String documentControlNumber = ParamUtil.getString((PortletRequest) request, (String) "documentControlNumber");
        
    	priorAuthHeader.setPriorAuthClaimNumber(claimNumber);
        log.info("Avalon Claim Number: " + claimNumber);

    	priorAuthHeader.setPriorAuthHpClaimNumber(hpClaimNumber);
        log.info("Health Plan Claim Number: " + hpClaimNumber);

    	priorAuthHeader.setPriorAuthSubDocumentNumber(documentControlNumber);
        log.info("Document Control Number: " + documentControlNumber);

        
        // Get the relate PAs
        String[] relatedPaCodes = {ParamUtil.getString((PortletRequest) request, (String) "releatePa0"),  ParamUtil.getString((PortletRequest) request, (String) "releatePa1"),
        		                   ParamUtil.getString((PortletRequest) request, (String) "releatePa2"),  ParamUtil.getString((PortletRequest) request, (String) "releatePa3"),
                                   ParamUtil.getString((PortletRequest) request, (String) "releatePa4"),  ParamUtil.getString((PortletRequest) request, (String) "releatePa5"),
                                   ParamUtil.getString((PortletRequest) request, (String) "releatePa6"),  ParamUtil.getString((PortletRequest) request, (String) "releatePa7"),
                                   ParamUtil.getString((PortletRequest) request, (String) "releatePa8"),  ParamUtil.getString((PortletRequest) request, (String) "releatePa9"),
                                   ParamUtil.getString((PortletRequest) request, (String) "releatePa10"), ParamUtil.getString((PortletRequest) request, (String) "releatePa11")};

        //Set the related PAs even it they are all blank.  All blank will clear any existing releate PAs.
    	priorAuthHeader.setPriorAuthCode1(relatedPaCodes[0]);
        log.info("related PA 1: "  + relatedPaCodes[0]);

        priorAuthHeader.setPriorAuthCode2(relatedPaCodes[1]);
        log.info("related PA 2: "  + relatedPaCodes[1]);

        priorAuthHeader.setPriorAuthCode3(relatedPaCodes[2]);
        log.info("related PA 3: "  + relatedPaCodes[2]);

        priorAuthHeader.setPriorAuthCode4(relatedPaCodes[3]);
        log.info("related PA 4: "  + relatedPaCodes[3]);

        priorAuthHeader.setPriorAuthCode5(relatedPaCodes[4]);
        log.info("related PA 5: "  + relatedPaCodes[4]);

        priorAuthHeader.setPriorAuthCode6(relatedPaCodes[5]);
        log.info("related PA 6: "  + relatedPaCodes[5]);

        priorAuthHeader.setPriorAuthCode7(relatedPaCodes[6]);
        log.info("related PA 7: "  + relatedPaCodes[6]);

        priorAuthHeader.setPriorAuthCode8(relatedPaCodes[7]);
        log.info("related PA 8: "  + relatedPaCodes[7]);

        priorAuthHeader.setPriorAuthCode9(relatedPaCodes[8]);
        log.info("related PA 9: "  + relatedPaCodes[8]);

        priorAuthHeader.setPriorAuthCode10(relatedPaCodes[9]);
        log.info("related PA 10: " + relatedPaCodes[9]);

        priorAuthHeader.setPriorAuthCode11(relatedPaCodes[10]);
        log.info("related PA 11: " + relatedPaCodes[10]);

        priorAuthHeader.setPriorAuthCode12(relatedPaCodes[11]);
        log.info("related PA 12: " + relatedPaCodes[11]);
        
        // Get the notes
        String inEnNotes = ParamUtil.getString((PortletRequest) request, (String) "intakeNotesCreate");
        priorAuthHeader.setPriorAuthNoteText(inEnNotes.toString());
        log.info("Intake Notes: " + inEnNotes);

        priorAuthHeader.setPriorAuthNoteTypeCode(noteType);
        priorAuthHeader.setSourceTab(sourceTab);
        priorAuthHeader.setAuthorizationKey(authorizationKey.longValue());
        priorAuthHeader.setNoteCreateUserId(themeDisplay.getUser().getEmailAddress());
        priorAuthHeader.setNoteCreateDatetime(creationDate);
        createPriorAuthNoteDetails.setPriorAuthNoteRequest(priorAuthHeader);
        CreatePriorAuthNoteDetailsE2.setCreatePriorAuthNoteDetails(createPriorAuthNoteDetails);
        
        //TODO: Remove below condition. Don't remove else condition code
		if(GetterUtil.getBoolean(com.liferay.portal.kernel.util.PropsUtil.get(PostServiceConstants.ENABLED_MOCK_SERVICE))){
		}else{
			PriorAuthReviewServiceStub.CreatePriorAuthNoteDetailsResponseE createPriorAuthNoteDetailsResponseE = priorAuthReviewServiceStub.createPriorAuthNoteDetails(CreatePriorAuthNoteDetailsE2);
	        Long longKey = createPriorAuthNoteDetailsResponseE.getCreatePriorAuthNoteDetailsResponse().get_return().getAuthorizationKey();
		}
        log.info("Processing Render Action for Intake Review Exit");
    }
    
    /**
     * Method invocation is ajax-call
     * Search for PA with the authorization number parameter using an esb service.
     */
    @ResourceMapping(value = "validateAuthorizationNumber")
    private void validateAuthorizationNumber(ResourceRequest resourceRequest,
	                                     ResourceResponse resourceResponse, 
	                                     String priorAuthNumber) throws IOException {

    	log.info("Validate the Authorization Number");
    	log.info("Authorization Number: " + priorAuthNumber);

    	String validFlag = "No";
    	String priorAuthNum = "";
    	try{
    		
    		// Set the operation type for the get prior auth header for procedure
    		PriorAuthHeaderServiceStub priorAuthHeaderServiceStub = new PriorAuthHeaderServiceStub();
    		PriorAuthHeaderServiceStub.GetPriorAuthHeaderE getPriorAuthHeaderE = new PriorAuthHeaderServiceStub.GetPriorAuthHeaderE();
    		PriorAuthHeaderServiceStub.GetPriorAuthHeader GetPriorAuthHeader2 = new PriorAuthHeaderServiceStub.GetPriorAuthHeader();
    		PriorAuthHeaderServiceStub.PriorAuthHeader priorAuthHeader = new PriorAuthHeaderServiceStub.PriorAuthHeader();

    		// Set the source tab and the email information
    		priorAuthHeader.setSourceTab("Procedure");
    		ThemeDisplay themeDisplay = (ThemeDisplay) resourceRequest.getAttribute("LIFERAY_SHARED_THEME_DISPLAY");
    		priorAuthHeader.setAppCreateUserId(themeDisplay.getUser().getEmailAddress());
    		priorAuthHeader.setAppMaintUserId(themeDisplay.getUser().getEmailAddress());

    		// Set the auth number
    		PriorAuthHeaderDTO priorAuthHeaderDTO = new PriorAuthHeaderDTO();
    		priorAuthHeaderDTO.setPriorAuthNumber(priorAuthNumber);
    		priorAuthHeader.setPriorAuthHeaderDTO(priorAuthHeaderDTO);

    		GetPriorAuthHeader2.setSearchCriteriaRequest(priorAuthHeader);
    		getPriorAuthHeaderE.setGetPriorAuthHeader(GetPriorAuthHeader2);

    		// Invoke the service
    		PriorAuthHeaderServiceStub.GetPriorAuthHeaderResponseE getPriorAuthHeaderResponseE =null;
    		PriorAuthHeaderServiceStub.GetPriorAuthHeaderResponse getPriorAuthHeaderResponse =null;

    		//TODO: Remove mock service code and condition. Don't remove else condition code
			if(GetterUtil.getBoolean(com.liferay.portal.kernel.util.PropsUtil.get(PostServiceConstants.ENABLED_MOCK_SERVICE))){
				 //Success Mock Code
				 getPriorAuthHeaderResponseE =new PriorAuthHeaderServiceStub.GetPriorAuthHeaderResponseE();
	    		 getPriorAuthHeaderResponse =new PriorAuthHeaderServiceStub.GetPriorAuthHeaderResponse();
	    		 getPriorAuthHeaderResponse.set_return(priorAuthHeader);
	    		 getPriorAuthHeaderResponseE.setGetPriorAuthHeaderResponse(getPriorAuthHeaderResponse);
			}else{
				// Invoke the service
	    		getPriorAuthHeaderResponseE = priorAuthHeaderServiceStub.getPriorAuthHeader(getPriorAuthHeaderE);
	    		getPriorAuthHeaderResponse = getPriorAuthHeaderResponseE.getGetPriorAuthHeaderResponse();
			}
    		
    		if (getPriorAuthHeaderResponse != null) {
	    		// Fetching the data from the response object
	    		PriorAuthHeaderServiceStub.PriorAuthHeaderDTO priorAuthHeaderDTOResponse = getPriorAuthHeaderResponse.get_return().getPriorAuthHeaderDTO();
	
	    		if (priorAuthHeaderDTOResponse != null) {
	    			priorAuthNum = priorAuthHeaderDTOResponse.getPriorAuthNumber();
	    		}
    		}
    		
			if (priorAuthNum == null) {
				validFlag = "No";
			} else {
				if (priorAuthNum.equals(priorAuthNumber)) {
					validFlag = "Yes";
				} else {
					validFlag = "No";
				}
			}
	    } catch (AxisFault e) {
	    	log.error("AxisFault in validateAuthorizationNumber method",(Throwable) e);
	    } catch (DAOExceptionException e) {
	    	log.error("DAOExceptionException in validateAuthorizationNumber method",(Throwable) e);
	    } catch (RemoteException e) {
	    	log.error("RemoteException in validateAuthorizationNumber method",(Throwable) e);
	    }
   		
    	// Set the response
		PrintWriter out = resourceResponse.getWriter();
		out.println(validFlag);
		log.info("validFlag: " + validFlag);
    }


    @ActionMapping
    public void IntakeHandler() {
        log.info("Intake Review Handler");
    }
}