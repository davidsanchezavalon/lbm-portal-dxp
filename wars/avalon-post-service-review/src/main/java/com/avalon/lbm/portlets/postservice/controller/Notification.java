/**
 * Description
 *		This file contain the controller methods for the Post Service Review Notification Page.
 *
 * CHANGE History
 * 		Version 1.0
 * 			Initial version copied from avalon-prior-authorization.
 * 		Version 1.1			12/14/2017  
 *			Added Notification Fax Number.
 *  
 */

package com.avalon.lbm.portlets.postservice.controller;

import java.rmi.RemoteException;
import java.util.Calendar;
import java.util.Map;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletRequest;
import javax.portlet.PortletSession;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.servlet.http.HttpServletRequest;

import org.apache.axis2.AxisFault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import com.avalon.lbm.portlets.postservice.model.CurrentAuthorizationDetailsFO;
import com.avalon.lbm.portlets.postservice.model.NotificationFO;
import com.avalon.lbm.portlets.postservice.model.PostServiceConstants;
import com.avalon.lbm.portlets.postservice.util.PostServiceUtil;
import com.avalon.lbm.services.PriorAuthReviewServiceStub;
import com.avalon.lbm.services.PriorAuthReviewServiceStub.PriorAuthReview;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PortalUtil;

@Controller(value = "Notification")
@RequestMapping(value = { "VIEW" })
public class Notification {
	
	private static final Log log = LogFactoryUtil.getLog(Notification.class.getName());
  
    /**** Render method which gets called during initial load of JSP *************************/
    @RenderMapping
    public String handleRenderRequest(RenderRequest request, 
    		                          RenderResponse response, 
    		                          Model model) {
    	
	    log.info("Rendering Notification Data");
	   	HttpServletRequest httpRequest = PortalUtil.getHttpServletRequest(request);
        String sourceTab = PostServiceUtil.notificationTab;
        PortletSession portletSession = request.getPortletSession();
    	String browserStr = getBrowser(httpRequest);

    	try {
            Long authorizationKey = (Long) portletSession.getAttribute("authorizationKey", PortletSession.APPLICATION_SCOPE);
            String authorizationNumber = (String) portletSession.getAttribute("authorizationNumber", PortletSession.APPLICATION_SCOPE);
// LOCAL TEST
//authorizationKey = new Long("11111");

            CurrentAuthorizationDetailsFO currentAuthorizationDetails = PostServiceUtil.getHeaderDetails(portletSession, sourceTab);

            // Set the authorization age
            ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute("LIFERAY_SHARED_THEME_DISPLAY");
            String appDate = (String) portletSession.getAttribute("CreateDatetime", PortletSession.APPLICATION_SCOPE);
            currentAuthorizationDetails.setAuthorizationAge(PostServiceUtil.setAuthorizationAge(portletSession, appDate, authorizationKey, themeDisplay));
            
            // Put the creation date/time in a request attribute
            request.setAttribute("paCreationDate", appDate);
            
            // Put the requested date/time in a request attribute
            String requestedDate = (String) portletSession.getAttribute("RequestedDatetime", PortletSession.APPLICATION_SCOPE);
            request.setAttribute("paRequestedDate", requestedDate);
           
    		request.setAttribute("currentAuthorizationDetailsFO", (Object) currentAuthorizationDetails);
            PriorAuthReviewServiceStub priorAuthReviewServiceStub = new PriorAuthReviewServiceStub();
            
            PriorAuthReviewServiceStub.SearchPriorAuthReviewDetailsE searchPriorAuthReviewDetailsE = new PriorAuthReviewServiceStub.SearchPriorAuthReviewDetailsE();
            PriorAuthReviewServiceStub.SearchPriorAuthReviewDetails searchPriorAuthReviewDetails = new PriorAuthReviewServiceStub.SearchPriorAuthReviewDetails();
            PriorAuthReviewServiceStub.ReviewSearchCriteria reviewSearchCriteria = new PriorAuthReviewServiceStub.ReviewSearchCriteria();
            reviewSearchCriteria.setAuthorizationKey(authorizationKey.longValue());
            reviewSearchCriteria.setSourceTab(sourceTab);
            reviewSearchCriteria.setAuthorizationNumber(authorizationNumber);
            searchPriorAuthReviewDetails.setSearchCriteriaRequest(reviewSearchCriteria);
            searchPriorAuthReviewDetailsE.setSearchPriorAuthReviewDetails(searchPriorAuthReviewDetails);
            
            PriorAuthReviewServiceStub.SearchPriorAuthReviewDetailsResponseE searchPriorAuthReviewDetailsResponseE =null;
            PriorAuthReviewServiceStub.SearchPriorAuthReviewDetailsResponse searchPriorAuthReviewDetailsResponse =null;
            
            //TODO: Remove mock service code and condition. Don't remove else condition code
			if(GetterUtil.getBoolean(com.liferay.portal.kernel.util.PropsUtil.get(PostServiceConstants.ENABLED_MOCK_SERVICE))){
				 //Success Mock Code
				searchPriorAuthReviewDetailsResponse = searchPriorAuthResponseSuccessMock();
			}else{
				searchPriorAuthReviewDetailsResponseE = priorAuthReviewServiceStub.searchPriorAuthReviewDetails(searchPriorAuthReviewDetailsE);
	            searchPriorAuthReviewDetailsResponse = searchPriorAuthReviewDetailsResponseE.getSearchPriorAuthReviewDetailsResponse();
	        }
            NotificationFO notificationFO = new NotificationFO();
            PriorAuthReviewServiceStub.PriorAuthReview review = searchPriorAuthReviewDetailsResponse.get_return();
            if (review.getNotificationSentDate() != null && !review.getNotificationSentDate().equals("")) {
            	notificationFO.setNotificationSentDate(PostServiceUtil.getPriorAuthUtil().getNotificationDateFromCalender(review.getNotificationSentDate(), browserStr));
            	notificationFO.setNotificationSentTime(PostServiceUtil.getPriorAuthUtil().getNotificationTimeFromCalender(review.getNotificationSentDate()));
            } else {
            	notificationFO.setNotificationSentDate(null);
            	notificationFO.setNotificationSentTime(null);
            }
            if (review.getNotificationDate() != null && !review.getNotificationDate().equals("")) {
                notificationFO.setNotificationDate(PostServiceUtil.getPriorAuthUtil().getNotificationDateFromCalender(review.getNotificationDate(), browserStr));
            } else {
                notificationFO.setNotificationDate(null);
            }
            notificationFO.setNotificationBy(review.getNotificationBy());
            notificationFO.setNotificationContactPhone(review.getNotificationContactPhone());
            notificationFO.setNotificationContactFax(review.getNotificationContactFax());
            notificationFO.setNotificationContactName(review.getNotificationContactName());
            request.setAttribute("notificationBy", (Object) notificationFO.getNotificationBy());
            request.setAttribute("notificationFO", (Object) notificationFO);
        } catch (AxisFault e) {
        	log.error("AxisFault Exception in handleRenderRequest method",e);
        } catch (RemoteException e) {
        	log.error("RemoteException in handleRenderRequest method",e);
        }
        return "view";
    }
    /**************Action method which gets called on click of SAVE button***************************************/
    @ActionMapping(params = { "action=notificationAction" })
    public void notificationDetails(ActionRequest request, 
    		                        ActionResponse response, 
    		                        @ModelAttribute(value = "notificationFO") NotificationFO notificationFO, Map map) {

    	log.info("Save Notification Action");
        ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute("LIFERAY_SHARED_THEME_DISPLAY");
        PostServiceUtil postServiceUtil = PostServiceUtil.getPriorAuthUtil();
        PortletSession session = request.getPortletSession();
    	
    	HttpServletRequest httpRequest = PortalUtil.getHttpServletRequest(request);
    	String browserStr = getBrowser(httpRequest);
        try {
    	   	String sourceTab = PostServiceUtil.notificationTab;
    	   	
            PriorAuthReviewServiceStub priorAuthReviewServiceStub = new PriorAuthReviewServiceStub();
            PriorAuthReviewServiceStub.CreatePriorAuthReviewDetailsE createPriorAuthReviewDetailsE = new PriorAuthReviewServiceStub.CreatePriorAuthReviewDetailsE();
            PriorAuthReviewServiceStub.CreatePriorAuthReviewDetails createPriorAuthReviewDetails = new PriorAuthReviewServiceStub.CreatePriorAuthReviewDetails();
            PriorAuthReviewServiceStub.PriorAuthReview priorAuthReview = new PriorAuthReviewServiceStub.PriorAuthReview();
            
			if (browserStr.equals("IE") || browserStr.equals("Firefox") || browserStr.equals("unknown")) {
	            if (notificationFO.getNotificationDate() == null) {
	            	notificationFO.setNotificationDate(ParamUtil.getString((PortletRequest) request, (String) "hiddenNotificationDate"));
	            }
			}
            if (notificationFO.getNotificationDate() != null && !notificationFO.getNotificationDate().toString().equals("")) {
				String notificationDate = notificationFO.getNotificationDate();
				notificationDate = PostServiceUtil.getPriorAuthUtil().setNotificationDate(notificationDate, browserStr);
            	priorAuthReview.setNotificationDate(postServiceUtil.getNotificationDate(notificationDate));
            } else {
                priorAuthReview.setNotificationDate(null);
            }
        	log.info("Verbal Notification Date ---> " + priorAuthReview.getNotificationDate());
            priorAuthReview.setNotificationBy(themeDisplay.getUser().getEmailAddress());
            priorAuthReview.setNotificationContactName(notificationFO.getNotificationContactName());

			if (browserStr.equals("IE") || browserStr.equals("Firefox") || browserStr.equals("unknown")) {
	            if (notificationFO.getNotificationSentDate() == null) {
	            	notificationFO.setNotificationSentDate(ParamUtil.getString((PortletRequest) request, (String) "hiddenNotificationSentDate"));
	            }
			}
            if (notificationFO.getNotificationSentDate() != null && !notificationFO.getNotificationSentDate().toString().equals("")) {
				String notificationSentDatetime = notificationFO.getNotificationSentDate() + " " + notificationFO.getNotificationSentTime();
				notificationSentDatetime = PostServiceUtil.getPriorAuthUtil().setNotificationDatetime(notificationSentDatetime, browserStr);
                priorAuthReview.setNotificationSentDate(postServiceUtil.getNotificationDatetime(notificationSentDatetime));
            } else {
                priorAuthReview.setNotificationSentDate(null);
            }
            priorAuthReview.setNotificationContactPhone(notificationFO.getNotificationContactPhone());
            priorAuthReview.setNotificationContactFax(notificationFO.getNotificationContactFax());
            priorAuthReview.setSourceTab(sourceTab);
            Long authorizationKey = (Long) session.getAttribute("authorizationKey", PortletSession.APPLICATION_SCOPE);
            priorAuthReview.setAuthorizationKey(authorizationKey.longValue());
            createPriorAuthReviewDetails.setPriorAuthReviewRequest(priorAuthReview);
            createPriorAuthReviewDetailsE.setCreatePriorAuthReviewDetails(createPriorAuthReviewDetails);
            //TODO: Remove below condition. Don't remove else condition code
			if(GetterUtil.getBoolean(com.liferay.portal.kernel.util.PropsUtil.get(PostServiceConstants.ENABLED_MOCK_SERVICE))){
			}else{
				PriorAuthReviewServiceStub.CreatePriorAuthReviewDetailsResponseE createPriorAuthReviewDetailsResponseE = priorAuthReviewServiceStub.createPriorAuthReviewDetails(createPriorAuthReviewDetailsE);
	        }
            request.setAttribute("notificationFO", (Object) notificationFO);
        } catch (AxisFault e) {
        	log.error("AxisFault Exception in notificationDetails method",e);
        } catch (RemoteException e) {
        	log.error("RemoteException in notificationDetails method",e);
        }
    }
    
	private String getBrowser(HttpServletRequest httpRequest) {
		String userAgent = httpRequest.getHeader("User-Agent");
		String rtnValue = "unknown";
		
		if (userAgent.contains("Opera") || userAgent.contains("OPR")) {
			rtnValue = "Opera";
		} else if (userAgent.contains("Chrome")) {
			rtnValue = "Chrome";
		} else if (userAgent.contains("Safari")) {
			rtnValue = "Safari";
		} else if (userAgent.contains("MSIE") || (userAgent.contains("Trident") && userAgent.contains("rv:"))) { 
			rtnValue = "IE";
		} else if (userAgent.contains("Firefox"))  {
			rtnValue = "Firefox";
		} else {
			rtnValue = "unknown";
		}
		return rtnValue;
	}
	
    @ActionMapping
    public void NotificationHandler() {
        log.info("Notification Handler");
    }
    
    /**
     * TODO: Remove below mock code
     * @return
     */
    private PriorAuthReviewServiceStub.SearchPriorAuthReviewDetailsResponse searchPriorAuthResponseSuccessMock(){
    	
    	PriorAuthReviewServiceStub.SearchPriorAuthReviewDetailsResponse searchPriorAuthReviewDetailsResponse = new PriorAuthReviewServiceStub.SearchPriorAuthReviewDetailsResponse();
             PriorAuthReview priorAuthReview = new PriorAuthReview();
         	 Calendar cal = Calendar.getInstance(); 
	         priorAuthReview.setAuthorizationKey(100L);
	         priorAuthReview.setDateTimeReturned("2018-04-26 01:02:00");
	         priorAuthReview.setDateTimeSent("2018-04-26 01:02:00");
	         priorAuthReview.setDecisionKey("DKey");
	         priorAuthReview.setDecisionValue("DValue");
	         priorAuthReview.setEnterPhysicianRationale("EPR");
	         priorAuthReview.setIntakeNotesCreate("INC");
	         priorAuthReview.setIntakeNotesRead("INR");
	         priorAuthReview.setIsReceived(true);
	         priorAuthReview.setNotificationBy("NY");
	         priorAuthReview.setNotificationContactFax("9912199121");
	         priorAuthReview.setNotificationContactName("NCN");
	         priorAuthReview.setNotificationContactPhone("9912199121");
	         priorAuthReview.setNotificationSentDate(cal);
	         priorAuthReview.setNurseNotesCreate("NCreate");
	         priorAuthReview.setNurseNotesRead("Nread");
	         priorAuthReview.setPhysicianName("PName");
	         priorAuthReview.setPhysicianRationaleMessage("RMsg");
	         priorAuthReview.setReasonKey("RKey");
	         priorAuthReview.setReasonValue("RValue");
	         priorAuthReview.setReconDecision("RD");
	         priorAuthReview.setReconDecisionDate("2018-04-26 01:02:00");
	         priorAuthReview.setReconNotesCreate("RNC");
	         priorAuthReview.setReconDecisionReviewer("RDR");
	         priorAuthReview.setReconNotesRead("RNR");
	         priorAuthReview.setRequestedBy("RB");
	         priorAuthReview.setRequestedDate("2018-04-26 01:02:00");
	         priorAuthReview.setRequestedInfo("RInfo");
	         priorAuthReview.setSourceTab("STab");
	         searchPriorAuthReviewDetailsResponse.set_return(priorAuthReview);
             return searchPriorAuthReviewDetailsResponse;
    }
}