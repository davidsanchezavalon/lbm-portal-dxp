/**
 * Description
 *		This file contain the controller methods for the Post Service Review Nurse Review Page.
 *
 * CHANGE History
 * 		Version 1.0
 * 			Initial version copied from avalon-prior-authorization.
 * 		Version 1.1					08/27/2018
 * 			Do not display a blank note.
 *  
 */

package com.avalon.lbm.portlets.postservice.controller;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletRequest;
import javax.portlet.PortletSession;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import com.avalon.lbm.portlets.postservice.model.CurrentAuthorizationDetailsFO;
import com.avalon.lbm.portlets.postservice.model.NurseReviewFO;
import com.avalon.lbm.portlets.postservice.model.PostServiceConstants;
import com.avalon.lbm.portlets.postservice.util.AgeCalculation;
import com.avalon.lbm.portlets.postservice.util.PostServiceUtil;
import com.avalon.lbm.services.PriorAuthReviewServiceStub;
import com.avalon.lbm.services.PriorAuthReviewServiceStub.PriorAuthHeader;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.ParamUtil;

@Controller(value = "NurseReview")
@RequestMapping(value = { "VIEW" })
public class NurseReview {
	
	private static final Log log = LogFactoryUtil.getLog(NurseReview.class.getName());
  
    /**** Render method which gets called during initial load of JSP *************************/
    @RenderMapping
    public String handleRenderRequest(RenderRequest request, 
    		                          RenderResponse response, 
    		                          Model model) throws RemoteException {
 
    	log.info("Rendering Nurse Review Data");
        String sourceTab = PostServiceUtil.nurseReviewTab;
        PortletSession portletSession = request.getPortletSession();
 
        Long authorizationKey = (Long) portletSession.getAttribute("authorizationKey", PortletSession.APPLICATION_SCOPE);
        log.info("Authorization Key" + authorizationKey);

        CurrentAuthorizationDetailsFO currentAuthorizationDetails = PostServiceUtil.getHeaderDetails(portletSession, sourceTab);

        // Set the authorization age
        ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute("LIFERAY_SHARED_THEME_DISPLAY");
        String appDate = (String) portletSession.getAttribute("CreateDatetime", PortletSession.APPLICATION_SCOPE);
        currentAuthorizationDetails.setAuthorizationAge(PostServiceUtil.setAuthorizationAge(portletSession, appDate, authorizationKey, themeDisplay));

        request.setAttribute("currentAuthorizationDetailsFO", (Object) currentAuthorizationDetails);

        NurseReviewFO nurseReviewFO = new NurseReviewFO();
        if (authorizationKey == null) {
            log.info("auth key null");
        } else {
            log.info("auth key not null");
            PriorAuthReviewServiceStub priorAuthReviewServiceStub1 = new PriorAuthReviewServiceStub();
            PriorAuthReviewServiceStub.SearchPriorAuthNoteDetails searchPriorAuthNoteDetails1 = new PriorAuthReviewServiceStub.SearchPriorAuthNoteDetails();
            PriorAuthReviewServiceStub.NoteSearchCriteria noteSearchCriteria1 = new PriorAuthReviewServiceStub.NoteSearchCriteria();
            noteSearchCriteria1.setAuthorizationKey(authorizationKey.longValue());
            noteSearchCriteria1.setSourceTab(sourceTab);
            searchPriorAuthNoteDetails1.setSearchCriteriaNoteRequest(noteSearchCriteria1);
            PriorAuthReviewServiceStub.SearchPriorAuthNoteDetailsE searchPriorAuthNoteDetailsE1 = new PriorAuthReviewServiceStub.SearchPriorAuthNoteDetailsE();
            searchPriorAuthNoteDetailsE1.setSearchPriorAuthNoteDetails(searchPriorAuthNoteDetails1);
            
            PriorAuthReviewServiceStub.SearchPriorAuthNoteDetailsResponseE searchPriorAuthNoteDetailsResponseE1 =null;
            PriorAuthReviewServiceStub.SearchPriorAuthNoteDetailsResponse searchPriorAuthNoteDetailsResponse1 =null;
           
            //TODO: Remove mock service code and condition. Don't remove else condition code
			if(GetterUtil.getBoolean(com.liferay.portal.kernel.util.PropsUtil.get(PostServiceConstants.ENABLED_MOCK_SERVICE))){
				 //Success Mock Code
				 searchPriorAuthNoteDetailsResponseE1 = new PriorAuthReviewServiceStub.SearchPriorAuthNoteDetailsResponseE();
				 searchPriorAuthNoteDetailsResponse1 = new PriorAuthReviewServiceStub.SearchPriorAuthNoteDetailsResponse();
				 PriorAuthHeader priorAuthHeader = new PriorAuthHeader();
		         priorAuthHeader.setAuthorizationKey(100L);
			     priorAuthHeader.setNoteCreateDatetime("2015-12-12 12:12:22.222");
			     priorAuthHeader.setNoteCreateUserId("UserId");
			     priorAuthHeader.setPriorAuthNoteText("Notetext");
			     priorAuthHeader.setPriorAuthNoteTypeCode("AuthType");
			     priorAuthHeader.setSourceTab("STab");
			     searchPriorAuthNoteDetailsResponse1.set_return(new PriorAuthHeader[] { priorAuthHeader} );
		         searchPriorAuthNoteDetailsResponseE1.setSearchPriorAuthNoteDetailsResponse(searchPriorAuthNoteDetailsResponse1);
		    }else{
				 searchPriorAuthNoteDetailsResponseE1 = priorAuthReviewServiceStub1.searchPriorAuthNoteDetails(searchPriorAuthNoteDetailsE1);
		         searchPriorAuthNoteDetailsResponse1 = searchPriorAuthNoteDetailsResponseE1.getSearchPriorAuthNoteDetailsResponse();
		    }
            if (searchPriorAuthNoteDetailsResponse1.get_return() != null) {
                log.info("Nurse Review Notes Text : " + searchPriorAuthNoteDetailsResponse1.get_return()[0].getPriorAuthNoteText());
            }
            ArrayList<String> nurseEnNotes = new ArrayList<String>();
            if (searchPriorAuthNoteDetailsResponse1.get_return() != null) {
                for (int i = 0; i < searchPriorAuthNoteDetailsResponse1.get_return().length; ++i) {
                    String creationDateTime = searchPriorAuthNoteDetailsResponse1.get_return()[i].getNoteCreateDatetime();
                    log.info("Creation Date Time of Nurse Notes :" + creationDateTime);
                    String frmDateStr = AgeCalculation.getAgeCalculation().getTimeForNote(creationDateTime);

                    
                    // Do not add a blank note
                    if (!searchPriorAuthNoteDetailsResponse1.get_return()[i].getPriorAuthNoteText().equals("")){
                    	nurseEnNotes.add(searchPriorAuthNoteDetailsResponse1.get_return()[i].getNoteCreateUserId() + "(" + frmDateStr + ")" + " : " +
                            searchPriorAuthNoteDetailsResponse1.get_return()[i].getPriorAuthNoteText());
                    	log.info("Nurse Review note " + (i + 1) + ": " + searchPriorAuthNoteDetailsResponse1.get_return()[i].getNoteCreateUserId() +
                    		"(" + frmDateStr + ")" + " : " + searchPriorAuthNoteDetailsResponse1.get_return()[i].getPriorAuthNoteText());
                    }
                }
                portletSession.setAttribute("nurseEnNotes", nurseEnNotes, PortletSession.APPLICATION_SCOPE);
                nurseReviewFO.setNurseNotesRead(nurseEnNotes);
                List readData = nurseReviewFO.getNurseNotesRead();
                request.setAttribute("readData", (Object) readData);
            }
        }
        request.setAttribute("nurseReviewFO", (Object) nurseReviewFO);
        return "view";
    }
    /**************Action method which gets called on click of SAVE button***************************************/
    @ActionMapping(params = { "action=nurseReviewAction" })
    public void nurseReviewDetails(ActionRequest request, 
    		                       ActionResponse response, 
    		                       @ModelAttribute(value = "nurseReviewFO") NurseReviewFO nurseReviewFO, 
    		                       Map map) throws RemoteException {

    	log.info("Save Nurse Review Action");
        String sourceTab = PostServiceUtil.nurseReviewTab;
        String noteType = PostServiceUtil.nurseReviewNote;

    	ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute("LIFERAY_SHARED_THEME_DISPLAY");
        String creationDate = AgeCalculation.getAgeCalculation().createDate();
        PortletSession portletSession = request.getPortletSession();
        Long authorizationKey = (Long) portletSession.getAttribute("authorizationKey", PortletSession.APPLICATION_SCOPE);
        PriorAuthReviewServiceStub priorAuthReviewServiceStub = new PriorAuthReviewServiceStub();
        PriorAuthReviewServiceStub.CreatePriorAuthNoteDetails createPriorAuthNoteDetails = new PriorAuthReviewServiceStub.CreatePriorAuthNoteDetails();
        PriorAuthReviewServiceStub.CreatePriorAuthNoteDetailsE CreatePriorAuthNoteDetailsE2 = new PriorAuthReviewServiceStub.CreatePriorAuthNoteDetailsE();
        PriorAuthReviewServiceStub.PriorAuthHeader priorAuthHeader = new PriorAuthReviewServiceStub.PriorAuthHeader();
        String nurseNotesText = ParamUtil.getString((PortletRequest) request, (String) "nurseNotesCreate");
        if (nurseNotesText != null && !nurseNotesText.equals("")) {
            priorAuthHeader.setPriorAuthNoteText(nurseNotesText.toString());
        }
        priorAuthHeader.setPriorAuthNoteTypeCode(noteType);
        priorAuthHeader.setSourceTab(sourceTab);
        priorAuthHeader.setAuthorizationKey(authorizationKey.longValue());
        priorAuthHeader.setNoteCreateUserId(themeDisplay.getUser().getEmailAddress());
        priorAuthHeader.setNoteCreateDatetime(creationDate);
        createPriorAuthNoteDetails.setPriorAuthNoteRequest(priorAuthHeader);
        CreatePriorAuthNoteDetailsE2.setCreatePriorAuthNoteDetails(createPriorAuthNoteDetails);
        
        //TODO: Remove below condition. Don't remove else condition code
		if(GetterUtil.getBoolean(com.liferay.portal.kernel.util.PropsUtil.get(PostServiceConstants.ENABLED_MOCK_SERVICE))){
		}else{
			 PriorAuthReviewServiceStub.CreatePriorAuthNoteDetailsResponseE createPriorAuthNoteDetailsResponseE = priorAuthReviewServiceStub.createPriorAuthNoteDetails(CreatePriorAuthNoteDetailsE2);
		     Long longKey = createPriorAuthNoteDetailsResponseE.getCreatePriorAuthNoteDetailsResponse().get_return().getAuthorizationKey();
		}
        request.setAttribute("nurseReviewFO", (Object) nurseReviewFO);
    }
    
    @ActionMapping
    public void NurseHandler() {
        log.info("Nurse Review Handler");
    }
}