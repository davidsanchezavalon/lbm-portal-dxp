/**
 * Description
 *		This file contain the javascript for Post Service Review.
 *
 * CHANGE History
 * 		Version 1.0  
 *			Initial version copied from avalon-prior-authorization.
 * 		Version 1.1
 *			Added changes for the initial version of Post Service Review.
 *		Version 1.2				06/19/2018
 *			Align the AUI fields in the member data section.
 * 		Version 1.3				07/16/2018
 *			Move the label to the left of the field.
 *
 */

YUI().ready('aui-node','event',
  function(Y) {
	if(Y.one('.authnumber')){
		 var statusnode = Y.one('.authnumber');
		 var status=statusnode.val();
		 
		if(status!=null && status!="" && status!='undefined'){
			 Y.all('.disabletab').removeClass('inactiveLink');
		}
	}
});


YUI().use('aui-node','event',
  function(Y) {
	if(Y.one('.authnumber')){
		 var statusnode = Y.one('.authnumber');
		 var status=statusnode.val();
		 
		if(status!=null && status!="" && status!='undefined'){
			 Y.all('.disabletab').removeClass('inactiveLink');
		}else{
			 Y.all('.disabletab').on('click',function(e){
				 e.preventDefault();
			 });
		}
	}
});  


var exitString = 'You have made updates to this page that have not been saved.  If you continue the updates will be lost.';

disableNavigationToggle();
setLayoutColumns();
makeFormResponsive();
formatRequiredLabels('*');

function disableNavigationToggle() {
	var leftNav = document.getElementsByClassName('toggler-header')[0];
	leftNav.setAttribute('class', leftNav.getAttribute('class').replace(' toggler-header',''));
}

function makeFormResponsive() {
	var groups = document.getElementById('main-content').getElementsByClassName('control-group');
	
	for (var i = 0, n = groups.length; i < n; i++ ) {
		groups[i].setAttribute('class', groups[i].getAttribute('class') + ' row-fluid');
		
		var label = groups[i].getElementsByTagName('label')[0];
		
		if (label) {
			label.setAttribute('class', label.getAttribute('class') + ' span5');
		}
		
		var input = groups[i].getElementsByTagName('input')[0];
		
		if (input) {
			input.setAttribute('class', input.getAttribute('class') + ' span7');
		}
	}
}

function formatRequiredLabels(text) {
	var reqLabels = document.getElementById('main-content').getElementsByClassName('label-required');
	
	for ( var i = 0, n = reqLabels.length; i < n; i++ ) {
		reqLabels[i].innerHTML = text;
	}
}

function setLayoutColumns() {
	var menu = document.getElementsByClassName('panel-page-menu');
	var body = document.getElementsByClassName('panel-page-body');
	
	if ( (menu && menu.length > 0) && (body && body.length > 0) ) {
		menu[0].setAttribute('class', menu[0].getAttribute('class').replace('span2','span3'));
		body[0].setAttribute('class', body[0].getAttribute('class').replace('span10','span9'));
	}
}

function removeSpaces(str) {
	 return str.split(' ').join('');
}

function showBusySign(frm) {
	
	// The form must be submitted before the animated GIF will move in IE
	$('#' + getNamespace() + frm).submit();

	document.getElementById('busy_indicator').style.display = 'block';
}

function checkValidForm(frm) {
	if (isValidForm()) {
		showBusySign(frm);
	}
}

function closePortlet() {
    var url=cancelUrl;
    window.location.href = url;
}

/*
 *  Return the time portion of a Java Date as a 12 hour clock.
 */
function convertTimeToString(dt) {

	var hour = dt.getHours() - (dt.getHours() > 12 ? 12 : 0);
	if (hour < 10) {
		hour = "0" + hour;
	}
	
	var minute = dt.getMinutes();
	if (minute < 10) {
		minute = "0" + minute;
	}

	var period = dt.getHours() >= 12 ? 'PM' : 'AM';

	return (hour + ':' + minute + ' ' + period);
}

/*
 *  Convert the Java Date to a string in the correct format for the browser.
 */
function convertDateToString(dt) {
	var retValue = null;
	var month = dt.getMonth() + 1;
	var day = dt.getDate();
	var year = dt.getFullYear();
	
	// Convert month and day to two digits
	month = month < 10 ? '0' + month : '' + month;
	day = day < 10 ? '0' + day : '' + day;
	
	if ((browserStr == "unknown") || (browserStr == "IE") || (browserStr == "Firefox")) {
		retValue = month + "/" + day + "/" + year;
	} else {
		retValue = year + "-" + month + "-" + day;
	}
	return retValue;
}

/*
 *  Convert the date to yyyy-MM-dd from MM/dd/yyyy.
 */
function convertDateChrome(_dat) {
	var rtnValue = "";
	
	// Parse the first date to individual parts
	var day = _dat.substring(3, 5);
	var month = _dat.substring(0, 2);
	var year = _dat.substring(6, 10);
	
	rtnValue = year + "-" + month + "-" + day;
	
	return rtnValue;
}

/*
 *  Convert the date to MM/dd/yyyy from yyyy-MM-dd.
 */
function convertDateIE(_dat) {
	var rtnValue = "";
	
	// Parse the first date to individual parts
	var day = _dat.substring(5, 7);
	var month = _dat.substring(8, 10);
	var year = _dat.substring(0, 4);
	
	rtnValue = day + "/" + month + "/" + year;
	
	return rtnValue;
}

function isValid(_dat, _len, _requiredFlag) {
	var rtnValue = false;
	var length = _dat.length;

	if (length == 0) {
		if (!_requiredFlag) {
			rtnValue = true;
		}
	} else if (length <= _len) {
		rtnValue = true;
	}
	
	return rtnValue;
}

function isValidAlpha(_dat, _len, _requiredFlag) {
	var rtnValue = false;
	var length = _dat.length;

	if (length == 0) {
		if (!_requiredFlag) {
			rtnValue = true;
		}
	} else {
		var isAlpha =  /^[a-zA-Z]+$/.test(_dat);
		if (isAlpha && (length <= _len)) {
			rtnValue = true;
		}
	}
	
	return rtnValue;
}

// name accept all
function isValidAlphaProviderName(_dat, _len, _requiredFlag) {
	var rtnValue = false;
	var length = _dat.length;

	if (length == 0) {
		if (!_requiredFlag) {
			rtnValue = true;
		}
	} else {
		if ((length <= _len)) {
			rtnValue = true;
		}
	}
	
	return rtnValue;
} 

function isValidAlphaOrder(_dat, _len, _requiredFlag) {
	var rtnValue = false;
	var length = _dat.length;

	if (length == 0) {
		if (!_requiredFlag) {
			rtnValue = true;
		}
	} else {
		
		if ( (length <= _len)) {
			rtnValue = true;
		}
	}
	
	return rtnValue;
}

function isValidDigit(_dat, _len, _requiredFlag, equalFlag) {
	var rtnValue = false;
	var length = _dat.length;

	if (length == 0) {
		if (!_requiredFlag) {
			rtnValue = true;
		}
	} else {
		var isNumber =  /^\d+$/.test(_dat);
		if (equalFlag) {
			if (isNumber && (_dat.length == _len)) {
				rtnValue = true;
			}
		} else if (isNumber && (_dat.length <= _len)) {
			rtnValue = true;
		}
	}
	
	return rtnValue;
}

function isValidAlphanum(_dat, _len, _requiredFlag) {
	var rtnValue = false;
	var length = _dat.length;

	if (length == 0) {
		if (!_requiredFlag) {
			rtnValue = true;
		}
	} else {
		var isAlphanumeric =  /^[a-zA-Z0-9]+$/.test(_dat);
		if (isAlphanumeric && (length <= _len)) {
			rtnValue = true;
		}
	}
	
	return rtnValue;
}

function isValidDiagnosis(_dat, _len, _requiredFlag) {
	var rtnValue = false;
	var stripDat = _dat.replace(/\./g, '');
	
	rtnValue = isValidAlphanum(stripDat, _len, _requiredFlag);
	
	return rtnValue;
}

function isValidDate(_dateString, _requiredFlag) {
	var rtnValue = true;
	var browserStr = getBrowser();
	var parts;
 	var day;
 	var month;
	var year;

	if (_dateString.length > 0) {

		// First check for the pattern
		if ((browserStr == "IE") || (browserStr == "Firefox") || (browserStr == "unknown")) {

			// Check format mm/dd/yyyy
			if (!/^\d{2}\/\d{2}\/\d{4}$/.test(_dateString)) {
				rtnValue = false;
			}
			
			if (rtnValue) {
	
				// Parse the date parts to integers
				parts = _dateString.split("/");
				day = parseInt(parts[1], 10);
				month = parseInt(parts[0], 10);
				year = parseInt(parts[2], 10);
			}
		} else {
			
			// Check format yyyy-mm-dd
			if (!/^\d{4}-\d{2}-\d{2}$/.test(_dateString)) {
				rtnValue = false;
			}
		
			
			if (rtnValue) {
				// Parse the date parts to integers
				parts = _dateString.split("-");
				day = parseInt(parts[2], 10);
				month = parseInt(parts[1], 10);
				year = parseInt(parts[0], 10);
			}
		}
	
		if (rtnValue) {
	
			// Check the ranges of month and year
			if ((year < 1000) || (year > 3000) || (month == 0) || (month > 12)) {
				rtnValue = false;
			}
		
			var monthLength = [ 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 ];
		
			// Adjust for leap years
		 	if ((year % 400 == 0) || ((year % 100 != 0) && (year % 4 == 0))) {
				monthLength[1] = 29;
			}

			// Check the range of the day 
			rtnValue = ((day > 0) && (day <= monthLength[month - 1]));
		}
	} else {
		if (_requiredFlag){
			rtnValue = false;
		}
	}

	return rtnValue;
}

function isValidTime(_timeString, _requiredFlag) {
	var rtnValue = false;
	var foundAM = false;
	var foundPM = false;
	var foundAM_PM = false;
	var len = _timeString.length;

	if (len == 8) {
		if (_timeString.indexOf(" AM/PM") != -1) {
			foundAM_PM = true;
		}
		if (_timeString.indexOf(" AM") != -1) {
			foundAM = true;
		}
		if (_timeString.indexOf(" PM") != -1) {
			foundPM = true;
		}

		if (foundAM_PM) {
			if (!_requiredFlag) {
				rtnValue = true;
			}
		} else if (foundAM || foundPM) {
			var dividerIdx = _timeString.indexOf(":");
			if (dividerIdx !== -1) {
				var hours = _timeString.substring(0, dividerIdx);
				var spIdx = _timeString.indexOf(" ");
				if (spIdx != -1) {
					var mins = _timeString.substring(dividerIdx + 1, spIdx);
					if ((hours > 0) && (hours < 13) && (mins > -1) && (mins < 60)) {
						rtnValue = true;
					}
				}
			}
		}
	} else {
		if (!_requiredFlag) {
			if ((len == 0 ) || (len == 8) || (_timeString == "hh:mm AM/PM")) {
				rtnValue = true;
			}
		}
	}
	return rtnValue;
}

function addWatermark(elem, dateFlag) {
	var browserStr = getBrowser();
	
	if (dateFlag) {
		
		// Add a watermark to the date field
		if ((browserStr == "IE") || (browserStr == "Firefox") || (browserStr == "unknown")) {
			$('#' + elem).Watermark("mm/dd/yyyy");
		}
	} else {
		
		// Add a watermark to the time field
		$('#' + elem).Watermark("hh:mm AM/PM");
	}
}

// Toggle between ascending and descending note order and change the icon
function changeSortOrder(elem) {
	if (elem.className == "icon-chevron-sign-up") {
		elem.className = "icon-chevron-sign-down";
	} else {
		elem.className = "icon-chevron-sign-up";
	}

	$(".oldNotes").toggle();
}

function processItem(nextId, currentId) {

	// Save the current active element
	var activeId = document.activeElement.id;

	// Force validation of currentId
	$("#" + nextId).focus();
	$("#" + nextId).blur();
	$("#" + currentId).focus();
	$("#" + currentId).blur();
		
	// Set focus to the saved field
	$("#" + activeId).focus();
}

function getBrowser() {
	var rtnValue = "unknown";
	
	if ((navigator.userAgent.indexOf("Opera") || navigator.userAgent.indexOf('OPR')) != -1 ) {
		rtnValue = "Opera";
	} if (navigator.userAgent.indexOf("Chrome") != -1 ) {
		rtnValue = "Chrome";
	} if (navigator.userAgent.indexOf("Safari") != -1) {
		rtnValue = "Safari";
	} else if ((navigator.userAgent.indexOf("MSIE") != -1 ) || (!!document.documentMode == true )) {  //IF IE > 10
		rtnValue = "IE";
	} else if (navigator.userAgent.indexOf("Firefox") != -1 )  {
		rtnValue = "Firefox";
	} else {
		rtnValue = "unknown";
	}

	return rtnValue;
}

function setField(fld, val, cls) {

	if (fld.val() == "") {
		
		// Test for a field that is pre-set
		if (val.length > 0) {
			fld.val(val);
		}

		// Make field editable
		fld.removeClass(cls);
	} else {
		
		// Make field read only
		fld.addClass(cls);
		
	}
}

function addToToolTip(str, len) {
	var toolTip = "";
	
	// Test for a new line needed
	if (len != 0) {
		toolTip += "\n";
	}
	toolTip += str;
	
	return toolTip;
}

//Highlight the item displayed in the menu.
function boldSelection() {
	var loc = window.location.href;
	var currentIdx = 10;
	
	if (loc.indexOf("providerinformation") != -1) {
		currentIdx = 0;
	} else if (loc.indexOf("procedureinformation") != -1) {
		currentIdx = 1;
	} else if (loc.indexOf("additionalinformation") != -1) {
		currentIdx = 2;
	} else if (loc.indexOf("intakereview") != -1) {
		currentIdx = 3;
	} else if (loc.indexOf("nursereview") != -1) {
		currentIdx = 4;
	} else if (loc.indexOf("physicianreview") != -1) {
		currentIdx = 5;
	} else if (loc.indexOf("notification") != -1) {
		currentIdx = 6;
	} else if (loc.indexOf("peertopeer") != -1) {
		currentIdx = 7;
	}
	
	for (var idx = 0; idx < 8; idx++) {
		if (currentIdx == idx) {
			$(".pta" + idx).css("font-weight", "bold");
		} else {
			$(".pta" + idx).css("font-weight", "normal");
		}
	}
}

function adjustMemberDataWidth() {
	var memberDetailsSmallSz = (window.innerWidth - parseInt($('.panel-heading').css('width'))) / 15;
	
	// Set the size for the State
	$("#" + getNamespace() + "memberState").css("cssText", "width:" + memberDetailsSmallSz + "px !important");
	$("#" + getNamespace() + "memberZip").css("cssText", "width:" + memberDetailsSmallSz + "px !important");
	
	// Set the size of the zipcode so that it aligned on the right side
//	var address2EndLoc = $("#" + getNamespace() + "memberAddressLine2").offset().left + parseInt($("#" + getNamespace() + "memberAddressLine2").css("width"));
//	var zipcodeEndLoc = $("#" + getNamespace() + "memberZip").offset().left + memberDetailsSmallSz;
//	memberDetailsSmallSz = memberDetailsSmallSz - (zipcodeEndLoc - address2EndLoc);
//	$("#" + getNamespace() + "memberZip").css("cssText", "width:" + memberDetailsSmallSz + "px !important");
}
