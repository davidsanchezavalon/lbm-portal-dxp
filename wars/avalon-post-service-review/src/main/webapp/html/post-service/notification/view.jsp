<%--
/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
 
 /**
  * Description
  *		This file contain the Notification portlet for Post Service Review
  *
  * CHANGE History
  * 	Version 1.0
  *			Initial version copied from avalon-prior-authorization.
  * 	Version 1.1
  *			Added changes for the initial version of Post Service Review.
  *		Version 1.2						11/08/2017
  *			Added the AvalonEmployeeROPA group to read-only for PA.
  * 	Version 1.3						12/14/2017  
  *			Added Notification Fax Number.
  *		Version 1.4						01/31/2018
  *			When the Save button is disabled show a mouse over tooltip with what is missing.
  *		Version 1.5						06/18/2018
  *			Align the AUI fields.
  * 	Version 1.6						07/16/2018
  *			Move the label to the left of the field.
  *		Version 1.7						09/20/2018
  *			Fixed the read-only fields.
  *
  */
--%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="theme" %>

<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@page import="com.liferay.portal.kernel.service.UserGroupLocalServiceUtil"%>
<%@page import="com.liferay.portal.kernel.model.User"%>

<%@ include file="/html/post-service/init.jsp"%>
<%@ include file="/html/post-service/post-service-details.jsp"%>

<theme:defineObjects />
<script>
	<%
		int NAME_LEN = 70;
		int PHONE_NUMBER_LEN = 10;
		int NOTIFY_BY = 256;
	%>
</script>

<aui:script>
	var toolTip = "";
	var browserStr = getBrowser();

	// Get the minimum date for the letter sent date
	var minDateStr = getMinDate();

	// Get the maximum date for the letter sent date
	var maxDateStr = convertDateToString(new Date());

	$(function() {
		
		// Highlight the Notification selection
		boldSelection();

		$('form').areYouSure( {'message':exitString} );

		// Add the date limits (only the week prior to the current day)
		if ((browserStr == "unknown") || (browserStr == "IE") || (browserStr == "Firefox")) {

			// Show date picker for IE and Firefox
			$("#<portlet:namespace/>notificationDate").datepicker({
				showOn: "button",
				buttonImage: "<%=request.getContextPath()%>	/images/icondatepicker.png",
				buttonImageOnly : true,
				dateFormat: 'mm/dd/yy',
				changeMonth: true,
				changeYear: true,
				onSelect: function(selected) {
					$("#<portlet:namespace/>hiddenNotificationDate").val(selected);
					
					// Cause the validation on the date field
					processItem("<portlet:namespace/>notificationDate", "<portlet:namespace/>notificationDate");

					// Set the save button status
					setSaveButtonStatus();
				}
			});
			$("#<portlet:namespace/>notificationDate").attr("placeholder", "mm/dd/yyyy");

			$("#<portlet:namespace/>notificationSentDate").datepicker({
				showOn: "button",
				buttonImage: "<%=request.getContextPath()%>	/images/icondatepicker.png",
				buttonImageOnly : true,
				dateFormat: 'mm/dd/yy',
				changeMonth: true,
				changeYear: true,
		        minDate: new Date(minDateStr),
		        maxDate: new Date(maxDateStr),
				onSelect: function(selected) {
					$("#<portlet:namespace/>hiddenNotificationSentDate").val(selected);
					setNotificationSentTime();
					
					// Cause the validation on the letter sent fields
					processItem("<portlet:namespace/>notificationSentTime", "<portlet:namespace/>notificationSentDate");
					processItem("<portlet:namespace/>notificationSentDate", "<portlet:namespace/>notificationSentTime");

					// Set the save button status
					setSaveButtonStatus();
				}
			});

			$(".ui-datepicker-trigger").css("float", "right");
		} else {
			$('#<portlet:namespace/>notificationSentDate').attr("min", minDateStr);
			$('#<portlet:namespace/>notificationSentDate').attr("max", maxDateStr);
		}
		addWatermark('<portlet:namespace/>notificationSentTime', false);
		
		// Set the initial status of the save button
		setSaveButtonStatus();
		
		// Adjust the field widths
		adjustMemberDataWidth();
	});

	$(window).resize(function () {
		
		// Reset the fields widths
		adjustMemberDataWidth();
	});

	function getNamespace() {
		return('<portlet:namespace/>');
	}

	function getCurrentTime() {
		var date = new Date();
		var currentTime = null;
		var hour = date.getHours();
		var minute = date.getMinutes();
		var amFlag = true;
		
		if (hour > 12) {
			hour -= 12;
			amFlag = false;
		}

		
		// Convert hour and minute to two digits
		hour = hour < 10 ? '0' + hour : '' + hour;
		minute = minute < 10 ? '0' + minute : '' + minute;
		
		currentTime = hour + ":" + minute + " ";
		if (amFlag) {
			currentTime += "AM";
		} else {
			currentTime += "PM";
		}

		return currentTime;
	}
	
	// For disabling tabbing when the Auth submission status are related to Void
	YUI().ready('aui-node', 'event', function(Y) {
		/*
		   Set the page to read only if the user is in the HealthPlanEmployee or AvalonEmployeeROPA group and not in 
		   AvalonAdmin, AvalonEmployee, AvalonProvider, PortalApprover, or PortalCreator groups.
		*/
		<% 
			boolean readOnlyFlag = false;
			boolean hpEmployeeFlag = false;
			boolean avalonEmployeeRoPaFlag = false;
			boolean notReadOnlyGroupFlag = false;
			User userU = themeDisplay.getUser();
			long[] groups = userU.getUserGroupIds();
			int length = groups.length;
			for (int i = 0; i < length; ++i) {
				String nextGroup = UserGroupLocalServiceUtil.getUserGroup(groups[i]).getName();
				if (nextGroup.equalsIgnoreCase("HealthPlanEmployee")) {
					hpEmployeeFlag = true;
				} else if (nextGroup.equalsIgnoreCase("AvalonEmployeeROPA")) {
					avalonEmployeeRoPaFlag = true;
				} else if (nextGroup.equalsIgnoreCase("AvalonAdmin") || nextGroup.equalsIgnoreCase("AvalonEmployee") || 
						   nextGroup.equalsIgnoreCase("AvalonProvider") || nextGroup.equalsIgnoreCase("PortalApprover") || 
						   nextGroup.equalsIgnoreCase("PortalCreator")) {
					notReadOnlyGroupFlag = true;
				}
			}
			if (!notReadOnlyGroupFlag && (hpEmployeeFlag || avalonEmployeeRoPaFlag)) {
				readOnlyFlag = true;
			}
		%>
		if (<%= readOnlyFlag %>) {
			
			// Disable the tabbing and make read only
			disableTabbing();
			makeReadonly();
		} else if(Y.one('#authSubmitStatusdisableId')){
			var authSubmitStatusdisable = Y.one('#authSubmitStatusdisableId').val();
			
			if(authSubmitStatusdisable!=null && authSubmitStatusdisable!="" && authSubmitStatusdisable!='undefined'){
				if(authSubmitStatusdisable=="Sent" || authSubmitStatusdisable=="Void HP - Submitted" || authSubmitStatusdisable=="Void-Submitted" || authSubmitStatusdisable=="Void"||authSubmitStatusdisable=="Void HP"){
					if(Y.one('.avalon_portlet')){
						// Opening all fields
					}
				}
			}
		}
	});
	// making field readonly and disabling tabindex when the field contains data already
	YUI().ready(
			'aui-node',
			'event',
			function(Y) {
				if (Y.one('#<portlet:namespace/>notificationBy')) {
					var notificationDate = Y.one('#<portlet:namespace/>notificationDate').val();
					var notificationContactName = Y.one('#<portlet:namespace/>notificationContactName').val();
					var notificationContactPhone = Y.one('#<portlet:namespace/>notificationContactPhone').val();
					var notificationContactFax = Y.one('#<portlet:namespace/>notificationContactFax').val();
					var notificationSentDate = Y.one('#<portlet:namespace/>notificationSentDate').val();
					var notificationSentTime = Y.one('#<portlet:namespace/>notificationSentTime').val();
					
					// Test for watermark in IE and Firefox
					if ((browserStr == "IE") || (browserStr == "Firefox") || (browserStr == "unknown")) {
						if (notificationDate != null && notificationDate == "") {

							// clear the watermark
							notificationDate = null;
						}
						if (notificationSentDate != null && notificationSentDate == "") {

							// clear the watermark
							notificationSentDate = null;
						}
					}
					if (notificationSentTime != null && notificationSentTime == "hh:mm AM/PM") {

						// clear the watermark
						notificationSentTime = null;
					}

					if (notificationDate != null && notificationDate != "" && notificationDate != 'undefined') {
						Y.all('.notifydate').addClass('optdisable');
						Y.all('.notifydate').setAttribute("tabindex","-1");
						$('.notifydate').prop("readonly", true);
						if ((browserStr == "unknown") || (browserStr == "IE") || (browserStr == "Firefox")) {
							$(".notifydate").datepicker('destroy');
						}
					}
					if (notificationContactName != null && notificationContactName != "" && notificationContactName != 'undefined') {
						Y.all('.notifyconName').addClass('optdisable');
						Y.all('.notifyconName').setAttribute("tabindex","-1");
						$('.notifyconName').prop("readonly", true);
					}
					if (notificationContactPhone != null && notificationContactPhone != "" && notificationContactPhone != 'undefined') {
						Y.all('.notifyconPhn').addClass('optdisable');
						Y.all('.notifyconPhn').setAttribute("tabindex","-1");
						$('.notifyconPhn').prop("readonly", true);
					}
					if (notificationContactFax != null && notificationContactFax != "" && notificationContactFax != 'undefined') {
						Y.all('.notifyconFax').addClass('optdisable');
						Y.all('.notifyconFax').setAttribute("tabindex","-1");
						$('.notifyconFax').prop("readonly", true);
					}
					if (notificationSentDate != null && notificationSentDate != "" && notificationSentDate != 'undefined') {
						Y.all('.notifysntDate').addClass('optdisable');
						Y.all('.notifysntDate').setAttribute("tabindex","-1");
						$('.notifysntDate').prop("readonly", true);
						if ((browserStr == "unknown") || (browserStr == "IE") || (browserStr == "Firefox")) {
							$(".notifysntDate").datepicker('destroy');
						}
					}
					
					// Set the status of the save button
					setSaveButtonStatus();

					if (notificationSentTime != null && notificationSentTime != "" && notificationSentTime != 'undefined') {
						Y.all('.notifysntTime').addClass('optdisable');
						Y.all('.notifysntTime').setAttribute("tabindex","-1");
						$('.notifysntTime').prop("readonly", true);
					}
				}
			}
	);

	function setSaveButtonStatus() {
		var disableFlag = true;
		var notificationDate = $("#<portlet:namespace/>notificationDate").val().trim();
		var notificationContactName = $("#<portlet:namespace/>notificationContactName").val().trim();
		var notificationSentDate = $("#<portlet:namespace/>notificationSentDate").val().trim();
		var notificationSentTime = $("#<portlet:namespace/>notificationSentTime").val().trim();
		var notificationContactPhone = $("#<portlet:namespace/>notificationContactPhone").val().trim();
		var notificationContactFax = $("#<portlet:namespace/>notificationContactFax").val().trim();
		
		var notificationSentDate_readonlyFlag = $("#<portlet:namespace/>notificationSentDate").prop("readonly");
		var notificationDate_readonlyFlag = $("#<portlet:namespace/>notificationDate").prop("readonly");
		var notificationContactName_readonlyFlag = $("#<portlet:namespace/>notificationContactName").prop("readonly");
		var notificationContactPhone_readonlyFlag = $("#<portlet:namespace/>notificationContactPhone").prop("readonly");
		var notificationContactFax_readonlyFlag = $("#<portlet:namespace/>notificationContactFax").prop("readonly");

		if (notificationSentTime == "hh:mm AM/PM") {
			notificationSentTime = "";
		}
		
		// Test for data in each field
		var notificationDateFlag = false;
		var notificationContactNameFlag = false;
		var notificationContactPhoneFlag = false;
		var notificationContactFaxFlag = false;
		if (!notificationDate_readonlyFlag && (notificationDate.length > 0)) {
			notificationDateFlag = true;
		}
		if (!notificationContactName_readonlyFlag && (notificationContactName.length > 0)) {
			notificationContactNameFlag = true;
		}
		if (!notificationContactPhone_readonlyFlag && (notificationContactPhone.length > 0)) {
			notificationContactPhoneFlag = true;
		}
		if (!notificationContactFax_readonlyFlag && (notificationContactFax.length > 0)) {
			notificationContactFaxFlag = true;
		}

		// Enable the SAVE button if data exists in any field
		if (notificationSentDate_readonlyFlag) {
			if (notificationDateFlag || notificationContactNameFlag || notificationContactPhoneFlag || notificationContactFaxFlag) {
				disableFlag = false;
			}
		} else if (notificationDateFlag || notificationContactNameFlag || notificationContactPhoneFlag || notificationContactFaxFlag || (notificationSentDate.length > 0) || (notificationSentTime.length > 0)) {
			disableFlag = false;
 		}

		if (isValidForm(disableFlag)) {
			if (disableFlag) {
				$("#<portlet:namespace/>saveNotification").attr("disabled", "disabled");
			} else {
				$("#<portlet:namespace/>saveNotification").removeAttr("disabled");
			}
		} else {
			$("#<portlet:namespace/>saveNotification").attr("disabled", "disabled");
		}

		// Add the mouse over text for the save button
		$("#<portlet:namespace/>saveNotification").prop("title", toolTip);
	}
	
	function getMinDate() {
		var retValue = "";
		var creationDate = $("#<portlet:namespace/>hiddenCreationDate").val().substring(0, 10);
		var requestedDate = $("#<portlet:namespace/>hiddenRequestedDate").val().substring(0, 10);
		
		// Get seven date before the current date (always use Chrome format for compare)
		var minDate = new Date();
		minDate.setDate(minDate.getDate() - 7);
		retValue = convertDateToString(minDate);

		if ((browserStr == "unknown") || (browserStr == "IE") || (browserStr == "Firefox")) {
			
			// Convert the date to Chrome format
			retValue = convertDateChrome(retValue);
		}
		
		// minDate cannot be before the request date.  If the requested date is not set, minDate cannot be before the creation date.
		if ((requestedDate == null) || ((requestedDate != null) && ((requestedDate == "null") || (requestedDate == "")))) {
			if (retValue < creationDate) {
				retValue = creationDate;
			}
		} else {
			if (retValue < requestedDate) {
				retValue = requestedDate;
			}
		}
		
		if ((browserStr == "unknown") || (browserStr == "IE") || (browserStr == "Firefox")) {
		
			// Convert the date to IE format
			retValue = convertDateIE(retValue);
		}

		return retValue;
	}
	
	function setNotificationSentTime() {
		var notificationSentDate = $("#<portlet:namespace/>notificationSentDate").val();
		var notificationSentTime = $("#<portlet:namespace/>notificationSentTime").val();
		var notificationSentDate_readonlyFlag = $("#<portlet:namespace/>notificationSentDate").prop("readonly");

		// Make sure the time is not set
		if (notificationSentTime == "hh:mm AM/PM") {

			// Make sure the date was not previously set
			if (!notificationSentDate_readonlyFlag && (notificationSentDate != "")) {

				// Make sure the date is within the valid range
				if ((notificationSentDate >= minDateStr) || (notificationSentDate <= maxDateStr)) {
					
					// Make sure the date is valid
					if (isValidDate(notificationSentDate, true)) {
						$("#<portlet:namespace/>notificationSentTime").val(getCurrentTime());
						notificationSentTime = $("#<portlet:namespace/>notificationSentTime").val();
					}
				}
			}
		}
	}
	
	function isValidForm(saveDisabled) {
		var formValid = false;
		var notificationDate = $("#<portlet:namespace/>notificationDate").val();
		var notificationBy = $("#<portlet:namespace/>notificationBy").val();
		var notificationContactName = $("#<portlet:namespace/>notificationContactName").val();
		var notificationSentDate = $("#<portlet:namespace/>notificationSentDate").val();
		var notificationSentTime = $("#<portlet:namespace/>notificationSentTime").val();
		var notificationContactPhone = $("#<portlet:namespace/>notificationContactPhone").val();
		var notificationContactFax = $("#<portlet:namespace/>notificationContactFax").val();
		
		var notificationDate_html5 = document.getElementById("<portlet:namespace/>notificationDate");
		var notificationSentDate_html5 = document.getElementById("<portlet:namespace/>notificationSentDate");
		var notificationSentDate_readonlyFlag = $("#<portlet:namespace/>notificationSentDate").prop("readonly");

		// Initialize the tool tip for the save button
		toolTip = "";
		
		var dateSentRequired = false;
		if (notificationSentTime != "hh:mm AM/PM") {
			dateSentRequired = true;
		}
		
		// Test for the notificationSentDate is within the valid range or the date is set (Notified By is set)
		validSentDate = true;
		if (!notificationSentDate_readonlyFlag && (notificationSentDate != "")) {
			if ((notificationSentDate < minDateStr) || (notificationSentDate > maxDateStr)) {
				validSentDate = false;
			}
		}

		if (validSentDate) {
			setNotificationSentTime();
		}

		// Test for HTML5 errors
		var dateFormatValid = true;
		if ((browserStr != "unknown") && (browserStr != "IE") && (browserStr != "Firefox")) {
			if (notificationDate_html5.validity.badInput || notificationSentDate_html5.validity.badInput) {
				dateFormatValid = false;
			}
		}

		if (dateFormatValid && validSentDate && isValidDate(notificationDate, false) && isValid(notificationBy, <%= NOTIFY_BY %>, false) && isValid(notificationContactName, <%= NAME_LEN %>, false) && 
			isValidDate(notificationSentDate, dateSentRequired) && isValidTime(notificationSentTime, false) && isValidDigit(notificationContactPhone, <%= PHONE_NUMBER_LEN %>, false, true) && 
			isValidDigit(notificationContactFax, <%= PHONE_NUMBER_LEN %>, false, true)) {
			formValid = true;
		} else {

			// Build the toolTip for the Save button
			// Test the date of verbal notification
			if (notificationDate_html5.validity.badInput || !isValidDate(notificationDate, false)) {
				toolTip += addToToolTip("The Date of Verbal Notification is not valid", toolTip.length);
			}
			
			// Test the contact name
			if (!isValid(notificationContactName, <%= NAME_LEN %>, false)) {
				toolTip += addToToolTip("The Contact Name is not valid", toolTip.length);
			}

			// Test the date letter sent
			if (notificationSentDate_html5.validity.badInput || !validSentDate || !isValidDate(notificationSentDate, dateSentRequired)) {
				toolTip += addToToolTip("The Date Letter Sent is not valid", toolTip.length);
			}
			
			// Test the time letter sent
			if (!isValidTime(notificationSentTime, false)) {
				toolTip += addToToolTip("The Time Letter Sent is not valid", toolTip.length);
			}
			
			// Test the phone number
			if (!isValidDigit(notificationContactPhone, <%= PHONE_NUMBER_LEN %>, false, true)) {
				toolTip += addToToolTip("The Phone Number is not valid", toolTip.length);
			}
			
			// Test the fax number
			if (!isValidDigit(notificationContactFax, <%= PHONE_NUMBER_LEN %>, false, true)) {
				toolTip += addToToolTip("The Fax Number is not valid", toolTip.length);
			}
		}

		if ((toolTip.length == 0) && saveDisabled) {

			// Add a toolTip for enabling the Save button by entering a field.
			toolTip = "There is no new data to save";
		}

		return formValid;
	}

	function makeReadonly() {

		// Make all fields readonly
		$('#<portlet:namespace/>notificationDate').addClass("optdisable");
		$('#<portlet:namespace/>notificationBy').addClass("optdisable");
		$('#<portlet:namespace/>notificationContactName').addClass("optdisable");
		$('#<portlet:namespace/>notificationSentDate').addClass("optdisable");
		$('#<portlet:namespace/>notificationSentTime').addClass("optdisable");
		$('#<portlet:namespace/>notificationContactPhone').addClass("optdisable");
		$('#<portlet:namespace/>notificationContactFax').addClass("optdisable");

		$('#<portlet:namespace/>notificationDate').prop("readonly", true);
		$('#<portlet:namespace/>notificationBy').prop("readonly", true);
		$('#<portlet:namespace/>notificationContactName').prop("readonly", true);
		$('#<portlet:namespace/>notificationSentDate').prop("readonly", true);
		$('#<portlet:namespace/>notificationSentTime').prop("readonly", true);
		$('#<portlet:namespace/>notificationContactPhone').prop("readonly", true);
		$('#<portlet:namespace/>notificationContactFax').prop("readonly", true);
		
		// Disable date pickers fro IE
		if ((browserStr == "unknown") || (browserStr == "IE") || (browserStr == "Firefox")) {
			$(".notifydate").datepicker('destroy');
			$(".notifysntDate").datepicker('destroy');
		}
		
		// Make the buttons readonly
		$('#<portlet:namespace/>saveNotification').addClass("optdisable");
		$('#<portlet:namespace/>saveNotification').prop("readonly", true);
	}

	function disableTabbing() {

		// Disable tabbing to all fields
		$('#<portlet:namespace/>notificationDate').prop('tabindex', '-1'); 
		$('#<portlet:namespace/>notificationBy').prop('tabindex', '-1'); 
		$('#<portlet:namespace/>notificationContactName').prop('tabindex', '-1'); 
		$('#<portlet:namespace/>notificationSentDate').prop('tabindex', '-1'); 
		$('#<portlet:namespace/>notificationSentTime').prop('tabindex', '-1'); 
		$('#<portlet:namespace/>notificationContactPhone').prop('tabindex', '-1'); 
		$('#<portlet:namespace/>notificationContactFax').prop('tabindex', '-1'); 

		// Disable tabbing to all buttons
		$('#<portlet:namespace/>saveNotification').prop('tabindex', '-1');
	}
</aui:script>
	

<portlet:actionURL var="notificationpageURL">
	<portlet:param name="action" value="notificationAction" />
</portlet:actionURL>

<%
    String notificationBy = "";
    if (request.getAttribute("notificationBy") != null) {
        notificationBy = (String) request.getAttribute("notificationBy");
    }
%>

<div id="busy_indicator" style="display: none">
	<img src="<%=request.getContextPath()%>/images/busy-spinner.gif" alt="Busy indicator">
</div>

<liferay-ui:panel title="psr.notification" collapsible="false">
	<aui:form name="notificationDetails" action="${notificationpageURL}" method="post" commandname="notificationFO" onChange="javascript:setSaveButtonStatus()" >
		<aui:fieldset label="psr.notification.info">
			<aui:input type="hidden" name="hiddenNotificationDate" id="hiddenNotificationDate" label="" value="${notificationFO.notificationDate}" />
			<aui:input type="hidden" name="hiddenNotificationSentDate" id="hiddenNotificationSentDate" label="" value="${notificationFO.notificationSentDate}" />
			<aui:input type="hidden" name="hiddenCreationDate" id="hiddenCreationDate" label="" value='<%= request.getAttribute("paCreationDate") %>' />
			<aui:input type="hidden" name="hiddenRequestedDate" id="hiddenRequestedDate" label="" value='<%= request.getAttribute("paRequestedDate") %>' />
			
			<aui:container>
				<aui:row>
					<aui:col span="6">
						<aui:input type="date" inlineLabel="true" name="notificationDate" id="notificationDate" cssClass="notifydate" label="psr.notification.label.notified.date" value="${notificationFO.notificationDate}" >
							<aui:validator name="custom" errorMessage="Please enter a valid date (format mm/dd/yyyy).">
								function (val, fieldNode, ruleValue) {
									var retValue = false;
									
									// Test for invalid date
									if ((browserStr == "IE") || (browserStr == "Firefox") || (browserStr == "unknown")) {
										retValue = isValidDate(val, true);
									} else {
										var notificationDate = document.getElementById("<portlet:namespace/>notificationDate");
										retValue = !notificationDate.validity.badInput;
									}
									
									return retValue;
								}
							</aui:validator>
						</aui:input>
					</aui:col>
					<aui:col span="6">
						<aui:input type="text" inlineLabel="true" name="notificationBy" id="notificationBy" cssClass="optdisable notify" tabindex="-1" readonly="true" label="psr.notification.label.notified.by" value="<%=notificationBy%>" maxLength="<%= NOTIFY_BY %>" />
					</aui:col>
				</aui:row>
				<aui:row>
					<aui:col span="6">
						<aui:input type="text" inlineLabel="true" name="notificationContactName" id="notificationContactName" label="psr.notification.label.contact.name" cssClass="notifyconName" value="${notificationFO.notificationContactName}" maxLength="<%= NAME_LEN %>" />
					</aui:col>
					<aui:col span="6">
						<aui:input type="date" inlineLabel="true" name="notificationSentDate" id="notificationSentDate" cssClass="notifysntDate" label="psr.notification.label.letter.sent.date" value="${notificationFO.notificationSentDate}" >
							<aui:validator name="custom" errorMessage="Please enter date letter sent.">
								function (val, fieldNode, ruleValue) {
									var retValue = false;
								
									if (!isValidDate(val, true)) {
										
										// Do not cause this error message if the date is invalid
										retValue = true;
									}
										
									if (!retValue) {
												    	
										var notificationSentTime = $('#<portlet:namespace/>notificationSentTime').val();
								
										if (notificationSentTime == "hh:mm AM/PM") {
											notificationSentTime = "";
										}
								
										// if a value is entered in the time or date, validate the time
										if ((val == "") && (notificationSentTime == "")) {
											retValue = true;
										} else {
											retValue = isValidDate(val, true);
										}
									}
								
									return retValue;
								}
							</aui:validator>
							<aui:validator name="custom" errorMessage="Please enter a valid date (format mm/dd/yyyy).">
								function (val, fieldNode, ruleValue) {
									var retValue = false;
									
									// Test for invalid date
									if ((browserStr == "IE") || (browserStr == "Firefox") || (browserStr == "unknown")) {
										retValue = isValidDate(val, true);
									} else {
										var notificationSentDate = document.getElementById("<portlet:namespace/>notificationSentDate");
										retValue = !notificationSentDate.validity.badInput;
									}
									
									return retValue;
								}
							</aui:validator>
							<aui:validator name="custom" errorMessage="Date cannot be prior to the request date or 7 days before today.">
								function (val, fieldNode, ruleValue) {
									var retValue = false;
									
									if (!isValidDate(val, true)) {
										
										// Do not cause this error message if the date format is invalid
										retValue = true;
									}
										
									if (!retValue) {
										var notificationSentTime = $('#<portlet:namespace/>notificationSentTime').val();
										
										// Check for watermark in returned time
										if (notificationSentTime == "hh:mm AM/PM") {
											notificationSentTime = "";
										}
										
										// If a time is entered, validate the date
										if (val == "") {
											retValue = true;
										} else {
											
											// Return true if the date letter sent is readonly
											if ($('#<portlet:namespace/>notificationSentDate').prop("readonly")) {
												retValue = true;
											} else {
												retValue = (val >= minDateStr);
											}
										}
									}
										
									return retValue;
								}
							</aui:validator>
							<aui:validator name="custom" errorMessage="Date cannot be greater than today.">
								function (val, fieldNode, ruleValue) {
									var retValue = false;
									
									if (!isValidDate(val, true)) {
										
										// Do not cause this error message if the date format is invalid
										retValue = true;
									}
										
									if (!retValue) {
													    	
										// Check for watermark in returned time
										var notificationSentTime = $('#<portlet:namespace/>notificationSentTime').val();
										if (notificationSentTime == "hh:mm AM/PM") {
											notificationSentTime = "";
										}
										
										// If a time is entered, validate the date
										if (val == "") {
											retValue = true;
										} else {
											retValue = (val <= maxDateStr);
										}
									}
									 	
									return retValue;
								}
							</aui:validator>
						</aui:input>
					</aui:col>
				</aui:row>
				<aui:row>
					<aui:col span="6">
						<aui:input type="text" inlineLabel="true" name="notificationContactPhone" id="notificationContactPhone" cssClass="notifyconPhn" label="psr.notification.phone.number" value="${notificationFO.notificationContactPhone}" maxlength="<%= PHONE_NUMBER_LEN %>" >
							<aui:validator name="digits" />
							<aui:validator name="minLength"><%= PHONE_NUMBER_LEN %></aui:validator>
						</aui:input>
					</aui:col>
					<aui:col span="6">
						<aui:input type="text" inlineLabel="true" name="notificationSentTime" id="notificationSentTime" cssClass="notifysntTime" label="psr.notification.label.letter.sent.time" value="${notificationFO.notificationSentTime}" >
							<aui:validator name="custom" errorMessage="Please enter a valid time (format: hh:mm AM/PM).">
								function (val, fieldNode, ruleValue) {
									var retValue;
									var notificationSentDate = $('#<portlet:namespace/>notificationSentDate').val();
									
									if (val == "hh:mm AM/PM") {
										val = "";
									}
									
									// If a date is entered, validate the time
									if ((val == "") && (notificationSentDate == "")) {
										retValue = true;
									} else {
										retValue = isValidTime(val, true);
									}
									
									return retValue;
								}
							</aui:validator>
						</aui:input>
					</aui:col>
				</aui:row>
				<aui:row>
					<aui:col span="6">
					</aui:col>
					<aui:col span="6">
						<aui:input type="text" inlineLabel="true" name="notificationContactFax" id="notificationContactFax" cssClass="notifyconFax" label="psr.notification.fax.number" value="${notificationFO.notificationContactFax}" maxlength="<%= PHONE_NUMBER_LEN %>" >
							<aui:validator name="digits" />
							<aui:validator name="minLength"><%= PHONE_NUMBER_LEN %></aui:validator>
						</aui:input>
					</aui:col>
				</aui:row>
			</aui:container>
		</aui:fieldset>
		
		<aui:fieldset>
			<aui:container>
				<aui:row>
					<aui:button-row cssClass="btn-divider">
						<aui:button type="button" primary="true" name="saveNotification" cssClass="pull-right" value="psr.label.save" onclick="javascript:checkValidForm('notificationDetails')" />
					</aui:button-row>
				</aui:row>
			</aui:container>
		</aui:fieldset>
 	</aui:form>
</liferay-ui:panel>
