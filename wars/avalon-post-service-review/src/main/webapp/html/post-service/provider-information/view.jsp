<%--
/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
 
 /**
  * Description
  *		This file contain the Provider Information portlet for Post Service Review
  *
  * CHANGE History
  * 	Version 1.0
  *			Initial version copied from avalon-prior-authorization.
  * 	Version 1.1
  *			Added changes for the initial version of Post Service Review.
  *		Version 1.2						11/08/2017
  *			Added the AvalonEmployeeROPA group to read-only for PA.
  *		Version 1.3						01/25/2018
  *			When the Save button is disabled show a mouse over tooltip with what is missing.
  *		Version 1.4						06/06/2018
  *			Change the cancel button from a hyperlink to a button.
  *		Version 1.5						06/19/2018
  *			Align the AUI fields.
  * 	Version 1.6						07/16/2018
  *			Move the label to the left of the field.
  *		Version 1.7						09/20/2018
  *			Fixed the read-only fields.
  *
  */
--%>

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="theme" %>

<%@page import="com.liferay.portal.kernel.util.PortalUtil"%>
<%@page import="com.liferay.portal.kernel.service.UserGroupLocalServiceUtil"%>
<%@page import="com.liferay.portal.kernel.model.User"%>
<%@ page import="com.avalon.lbm.portlets.postservice.model.PostServiceConstants" %>

<%@ include file="/html/post-service/init.jsp"%>
<%@ include file="/html/post-service/post-service-details.jsp"%>

<portlet:defineObjects />
<theme:defineObjects />

<portlet:actionURL var='providerInformationPageURL'>
 <portlet:param name="action" value="providerInformationAction" />
</portlet:actionURL>
<portlet:actionURL var='providerInformationCancelURL'>
 <portlet:param name="cancelaction" value="providerInfoCancelAction" />
</portlet:actionURL>
<portlet:actionURL var='providerAuthRedirectURL'>
 <portlet:param name="authRaction" value="authRedirectAction" />
</portlet:actionURL>
<style>
	.avalon__proc_portlet .control-group label {
		pointer-events: none;
	}
</style>

<script>
	<%
		int PROVIDER_NAME_LEN = 70;
		int NPI_LEN = 10;
		int TIN_EIN_LEN = 9;
		int PHONE_NUMBER_LEN = 10;
		int ADDRESS_LEN = 55;
		int CITY_LEN = 40;
		int ZIP_LEN = 5;
	%>
</script>

<aui:script>
	var toolTip = "";
	
	$(function() {
		
		// Highlight the Provider Information selection
		boldSelection();

		$('form').areYouSure({
			'message' : exitString
		});
		
		setSaveButtonStatus();
		
		// Adjust the width of zip code input so it ends with the other inputs.
		$("#<portlet:namespace/>requestingZip").width("40%");
		
		// Adjust the field widths
		adjustMemberDataWidth();
		adjustProviderDataWidth();
	});

	$(window).resize(function () {
		
		// Reset the fields widths
		adjustMemberDataWidth();
		adjustProviderDataWidth();
	});

	function adjustProviderDataWidth() {
		var memberDetailsSmallSz = (window.innerWidth - parseInt($('.panel-heading').css('width'))) / 15;
		
		// Set the size for the Provider State and zipcode
		$("#<portlet:namespace/>requestingState").css("cssText", "width:" + memberDetailsSmallSz + "px !important");
		$("#" + getNamespace() + "requestingZip").css("cssText", "width:" + memberDetailsSmallSz + "px !important");

		// Set the size of the zipcode so that it aligned on the right side
//		var address2EndLoc = $("#<portlet:namespace/>requestingAddressLine2").offset().left + parseInt($("#<portlet:namespace/>requestingAddressLine2").css("width"));
//		var zipcodeEndLoc = $("#<portlet:namespace/>requestingZip").offset().left + memberDetailsSmallSz;
//		memberDetailsSmallSz = memberDetailsSmallSz - (zipcodeEndLoc - address2EndLoc);
//		$("#<portlet:namespace/>requestingZip").css("cssText", "width:" + memberDetailsSmallSz + "px !important");
	}
	
	function getNamespace() {
		return('<portlet:namespace/>');
	}

	/* preventing save button functionality when any validation fails for form */
	YUI().use('aui-node','aui-modal','event',function(Y) {
		Y.one('.saveAction').on('click',function(e){
			if(Y.one('.error-field')){
				Y.one('.error-field').focus();
				e.preventDefault();
			} else{
		         window.onbeforeunload = function(e) {
		                e = e || window.event;
		                e.preventDefault = false;
		                e.cancelBubble = false;
		         }
				checkValidForm("providerInformationDetails");
			}
		});
	});

	var cancelUrl = '<%=providerInformationCancelURL.toString()%>';
	var authCreated = "<c:out value='${authCreated}'/>";
	YUI().use('aui-modal', function(Y) {

		if (authCreated != "") {
			var modal = new Y.Modal({
				bodyContent : 'Authorization Number:' + authCreated,
				centered : true,
				destroyOnHide : false,
				headerContent : 'The authorization has been saved.',
				modal : true,
				render : '#modal',
				resizable : {
					handles : 'b, r'
				},

				visible : true,
				width : 450
			}).render();

			modal.addToolbar([ {
				label : 'OK',
				on : {
					click : function() {
						var url = '<%=providerAuthRedirectURL.toString()%>';
						window.location.href = url;
					}
				}
			} ]);
		}
	});

	// For disabling tabbing when the Auth submission status are related to Void
	YUI().ready('aui-node', 'event', function(Y) {
		/*
		   Set the page to read only if the user is in the HealthPlanEmployee or AvalonEmployeeROPA group and not in 
		   AvalonAdmin, AvalonEmployee, AvalonProvider, PortalApprover, or PortalCreator groups.
		*/
		<% 
			boolean readOnlyFlag = false;
			boolean hpEmployeeFlag = false;
			boolean avalonEmployeeRoPaFlag = false;
			boolean notReadOnlyGroupFlag = false;
			User userU = themeDisplay.getUser();
			long[] groups = userU.getUserGroupIds();
			int length = groups.length;
			for (int i = 0; i < length; ++i) {
				String nextGroup = UserGroupLocalServiceUtil.getUserGroup(groups[i]).getName();
				if (nextGroup.equalsIgnoreCase("HealthPlanEmployee")) {
					hpEmployeeFlag = true;
				} else if (nextGroup.equalsIgnoreCase("AvalonEmployeeROPA")) {
					avalonEmployeeRoPaFlag = true;
				} else if (nextGroup.equalsIgnoreCase("AvalonAdmin") || nextGroup.equalsIgnoreCase("AvalonEmployee") || 
						   nextGroup.equalsIgnoreCase("AvalonProvider") || nextGroup.equalsIgnoreCase("PortalApprover") || 
						   nextGroup.equalsIgnoreCase("PortalCreator")) {
					notReadOnlyGroupFlag = true;
				}
			}
			if (!notReadOnlyGroupFlag && (hpEmployeeFlag || avalonEmployeeRoPaFlag)) {
				readOnlyFlag = true;
			}
		%>
		if (<%= readOnlyFlag %>) {

	  		// Make the page readonly
  		 	Y.one('.avalon__proc_portlet').addClass('optdisable');
			
			// Disable the tabbing for read only
			disableTabbing();
			
			// Disable the dropdowns in IE and Firefox
			disableDropdowns();
		} else if(Y.one('#authSubmitStatusdisableId')){
			var authSubmitStatusdisable = Y.one('#authSubmitStatusdisableId').val();
			
			if(authSubmitStatusdisable!=null && authSubmitStatusdisable!="" && authSubmitStatusdisable!='undefined'){
				if(authSubmitStatusdisable=="Void HP - Submitted" || authSubmitStatusdisable=="Void-Submitted" || authSubmitStatusdisable=="Void"||authSubmitStatusdisable=="Void HP"){

			  		// Make the page readonly
		  		 	Y.one('.avalon__proc_portlet').addClass('optdisable');

					disableTabbing();
					
					// Disable the dropdowns in IE and Firefox
					disableDropdowns();
				}
			}
		}
	});

	function isValidForm() {
		var requestingValid = false;
		var requestingProviderType = $("#<portlet:namespace/>requestingProviderType").prop("selectedIndex");
		var requestingProviderName = $("#<portlet:namespace/>requestingProviderName").val();
		var requestingNpi = $("#<portlet:namespace/>requestingNpi").val();
		var requestingTinEin = $("#<portlet:namespace/>requestingTinEin").val();
		var requestingPhoneNumber = $("#<portlet:namespace/>requestingPhoneNumber").val();
		var requestingFaxNumber = $("#<portlet:namespace/>requestingFaxNumber").val();
		var requestingAddressLine1 = $("#<portlet:namespace/>requestingAddressLine1").val();
		var requestingAddressLine2 = $("#<portlet:namespace/>requestingAddressLine2").val();
		var requestingCity = $("#<portlet:namespace/>requestingCity").val();
		var requestingState = $("#<portlet:namespace/>requestingState").prop("selectedIndex");
		var requestingZip = $("#<portlet:namespace/>requestingZip").val();
		
		// Initialize the tool tip for the save button
		toolTip = "";
		
		// Validate the requesting values
		if ((requestingProviderType > 0) && isValidAlphaProviderName(requestingProviderName, <%= PROVIDER_NAME_LEN %>, true) &&  isValidDigit(requestingNpi, <%= NPI_LEN %>, true) && 
			isValidDigit(requestingTinEin, <%= TIN_EIN_LEN %>, false) && isValidDigit(requestingPhoneNumber, <%= PHONE_NUMBER_LEN %>, false, true) &&  isValidDigit(requestingFaxNumber, <%= PHONE_NUMBER_LEN %>, false, true) && 
			isValid(requestingAddressLine1, <%= ADDRESS_LEN %>, false) &&  isValid(requestingAddressLine2, <%= ADDRESS_LEN %>, false) && isValidAlphaProviderName(requestingCity, <%= CITY_LEN %>, false) && 
			isValidDigit(requestingZip, <%= ZIP_LEN %>, false, true)) {
			requestingValid = true;
		} else {

			// Build the toolTip for the Save and Submit buttons
			// Test the provider type
			if (requestingProviderType == 0) {
				toolTip += addToToolTip("The Provider Type is not valid", toolTip.length);
			}
			
			// Test the provider name
			if (!isValidAlphaProviderName(requestingProviderName, <%= PROVIDER_NAME_LEN %>, true)) {
				toolTip += addToToolTip("The Provider Name is not valid", toolTip.length);
			}
			
			// Test the npi
			if (!isValidDigit(requestingNpi, <%= NPI_LEN %>, true)) {
				toolTip += addToToolTip("The NPI is not valid", toolTip.length);
			}
			
			// Test the tin/ein
			if (!isValidDigit(requestingTinEin, <%= TIN_EIN_LEN %>, false)) {
				toolTip += addToToolTip("The TIN/EIN is not valid", toolTip.length);
			}
			
			// Test the phone number
			if (!isValidDigit(requestingPhoneNumber, <%= PHONE_NUMBER_LEN %>, false, true)) {
				toolTip += addToToolTip("The Phone Number is not valid", toolTip.length);
			}
			
			// Test the fax number
			if (!isValidDigit(requestingFaxNumber, <%= PHONE_NUMBER_LEN %>, false, true)) {
				toolTip += addToToolTip("The Fax Number is not valid", toolTip.length);
			}
			
			// Test the address line 1
			if (!isValid(requestingAddressLine1, <%= ADDRESS_LEN %>, false)) {
				toolTip += addToToolTip("The Address Line 1 is not valid", toolTip.length);
			}
			
			// Test the address line 2
			if (!isValid(requestingAddressLine2, <%= ADDRESS_LEN %>, false)) {
				toolTip += addToToolTip("The Address Line 2 is not valid", toolTip.length);
			}
			
			// Test the city
			if (!isValidAlphaProviderName(requestingCity, <%= CITY_LEN %>, false)) {
				toolTip += addToToolTip("The City is not valid", toolTip.length);
			}
			
			// Test the zip
			if (!isValidDigit(requestingZip, <%= ZIP_LEN %>, false, true)) {
				toolTip += addToToolTip("The ZIP is not valid", toolTip.length);
			}
		}
		
		return requestingValid;
	}
	
	function setSaveButtonStatus() {
		
		// Disable the save button until all the fields are valid
	    if (isValidForm()) {
			$("#<portlet:namespace/>saveProviderInfo").removeAttr("disabled");
		} else {
			$("#<portlet:namespace/>saveProviderInfo").attr("disabled", "disabled");
		}
		
		// Add the mouse over text for the save button
		$("#<portlet:namespace/>saveProviderInfo").prop("title", toolTip);
	}

	function disableTabbing() {

		// Disable tabbing to all fields
		$('#<portlet:namespace/>requestingProviderType').prop('tabIndex', '-1'); 
		$('#<portlet:namespace/>requestingProviderName').prop('tabIndex', '-1'); 
		$('#<portlet:namespace/>requestingNpi').prop('tabIndex', '-1'); 
		$('#<portlet:namespace/>requestingTinEin').prop('tabIndex', '-1'); 
		$('#<portlet:namespace/>requestingPhoneNumber').prop('tabIndex', '-1'); 
		$('#<portlet:namespace/>requestingFaxNumber').prop('tabIndex', '-1'); 
		$('#<portlet:namespace/>requestingAddressLine1').prop('tabIndex', '-1'); 
		$('#<portlet:namespace/>requestingAddressLine2').prop('tabIndex', '-1'); 
		$('#<portlet:namespace/>requestingCity').prop('tabIndex', '-1'); 
		$('#<portlet:namespace/>requestingState').prop('tabIndex', '-1'); 
		$('#<portlet:namespace/>requestingZip').prop('tabIndex', '-1'); 

		// Disable tabbing to all buttons
		$('#<portlet:namespace/>saveProviderInfo').prop('tabIndex', '-1');
		$('#<portlet:namespace/>cancelProviderInfo').prop('tabIndex', '-1');
	}

	function disableDropdowns() {
		var browserStr = getBrowser();

		// Disable all of the dropdowns for IE
		if ((browserStr == "IE") || (browserStr == "Firefox") || (browserStr == "unknown")) {
			$('#<portlet:namespace/>requestingProviderType').attr('disabled', true);
			$('#<portlet:namespace/>requestingState').attr('disabled', true);
		}
	}
</aui:script>

<% // Provider Type constants %>
<c:set var="ordering" value="<%= PostServiceConstants.ORDERING %>" />
<c:set var="renderingInNetwork" value="<%= PostServiceConstants.RENDERING_INNETWORK %>" />
<c:set var="renderingOutOfNetwork" value="<%= PostServiceConstants.RENDERING_OUTOFNETWORK %>" />

<div id="busy_indicator" style="display: none">
 <img src="<%=request.getContextPath()%>/images/busy-spinner.gif" alt="Busy indicator">
</div>

<div class="avalon__proc_portlet">
	<liferay-ui:panel title="psr.provider.info" collapsible="false">
		<aui:form name="providerInformationDetails" cssClass="form-inline" action="${providerInformationPageURL}" method="post" commandName="providerInformationFO"  onChange="setSaveButtonStatus()">
			<aui:fieldset label="psr.requesting.provider.info" id="requesting-fields">
				<aui:container>
 					<aui:row>
						<aui:col span="6">
							<aui:select inlineLabel="true" name="requestingProviderType" label="psr.label.provider.type" cssClass="span7" required="true" showRequiredLabel="true">
								<aui:option value="">Make a Selection</aui:option>
								<aui:option value="<%= PostServiceConstants.ORDERING %>" selected="${providerInformationFO.requestingProviderType eq ordering}"><%= PostServiceConstants.ORDERING %></aui:option>
								<aui:option value="<%= PostServiceConstants.RENDERING_INNETWORK %>"    selected="${providerInformationFO.requestingProviderType eq renderingInNetwork}"><%= PostServiceConstants.RENDERING_INNETWORK %></aui:option>
								<aui:option value="<%= PostServiceConstants.RENDERING_OUTOFNETWORK %>" selected="${providerInformationFO.requestingProviderType eq renderingOutOfNetwork}"><%= PostServiceConstants.RENDERING_OUTOFNETWORK %></aui:option>
 							</aui:select>
						</aui:col>
					</aui:row>
 					<aui:row>
						<aui:col span="12">
							<aui:input type="text" inlineLabel="true" name="requestingProviderName" id="requestingProviderName" label="psr.label.provider.name" class="requestingProviderName" value="${providerInformationFO.requestingProviderName}" maxlength="<%= PROVIDER_NAME_LEN %>">
								<aui:validator name="required" />
							</aui:input>
						</aui:col>
					</aui:row>
					<aui:row>
						<aui:col span="6">
							<aui:input type="text" inlineLabel="true" name="requestingNpi" id="requestingNpi" label="psr.label.npi" value="${providerInformationFO.requestingNpi}" maxlength="<%= NPI_LEN %>" >
								<aui:validator name="required" />
								<aui:validator name="digits" />
								<aui:validator name="minLength"><%= NPI_LEN %></aui:validator>
							</aui:input>
						</aui:col>
						<aui:col span="6">
							<aui:input type="text" inlineLabel="true" name="requestingTinEin" id="requestingTinEin" label="psr.label.tin.ein" value="${providerInformationFO.requestingTinEin}" maxlength="<%= TIN_EIN_LEN %>" >
								<aui:validator name="digits" />
								<aui:validator name="minLength"><%= TIN_EIN_LEN %></aui:validator>
							</aui:input>
						</aui:col>
					</aui:row>
					<aui:row>
						<aui:col span="6">
							<aui:input type="text" inlineLabel="true" name="requestingPhoneNumber" id="requestingPhoneNumber" label="psr.label.phone.number" value="${providerInformationFO.requestingPhoneNumber}" maxlength="<%= PHONE_NUMBER_LEN %>" >
								<aui:validator name="digits" />
								<aui:validator name="minLength"><%= PHONE_NUMBER_LEN %></aui:validator>
							</aui:input>
						</aui:col>
						<aui:col span="6">
							<aui:input type="text" inlineLabel="true" name="requestingFaxNumber" id="requestingFaxNumber" label="psr.label.fax.number" value="${providerInformationFO.requestingFaxNumber}" maxlength="<%= PHONE_NUMBER_LEN %>" >
								<aui:validator name="digits" />
								<aui:validator name="minLength"><%= PHONE_NUMBER_LEN %></aui:validator>
							</aui:input>
						</aui:col>
					</aui:row>
					<aui:row>
						<aui:col span="6">
							<aui:input type="text" inlineLabel="true" name="requestingAddressLine1" id="requestingAddressLine1" label="psr.label.address" value="${providerInformationFO.requestingAddressLine1}" maxlength="<%= ADDRESS_LEN %>" />
						</aui:col>
						<aui:col span="6">
							<aui:input type="text" inlineLabel="true" name="requestingAddressLine2" id="requestingAddressLine2" label="psr.label.address.additional" value="${providerInformationFO.requestingAddressLine2}" maxlength="<%= ADDRESS_LEN %>" />
						</aui:col>
					</aui:row>
					<aui:row>
						<aui:col span="6">
							<aui:input type="text" inlineLabel="true" name="requestingCity" id="requestingCity" label="psr.label.city" value="${providerInformationFO.requestingCity}" maxlength="<%= CITY_LEN %>" />
						</aui:col>
						<aui:col span="3">
							<aui:select inlineLabel="true" name="requestingState" id="requestingState" label="psr.label.state" cssClass="span7" >
								<aui:option value="">Selection</aui:option>
								<aui:option value="AL" selected="${providerInformationFO.requestingState eq 'AL'}">Alabama</aui:option>
								<aui:option value="AK" selected="${providerInformationFO.requestingState eq 'AK'}">Alaska</aui:option>
								<aui:option value="AZ" selected="${providerInformationFO.requestingState eq 'AZ'}">Arizona</aui:option>
								<aui:option value="AR" selected="${providerInformationFO.requestingState eq 'AR'}">Arkansas</aui:option>
								<aui:option value="CA" selected="${providerInformationFO.requestingState eq 'CA'}">California</aui:option>
								<aui:option value="CO" selected="${providerInformationFO.requestingState eq 'CO'}">Colorado</aui:option>
								<aui:option value="CT" selected="${providerInformationFO.requestingState eq 'CT'}">Connecticut</aui:option>
								<aui:option value="DE" selected="${providerInformationFO.requestingState eq 'DE'}">Delaware</aui:option>
								<aui:option value="FL" selected="${providerInformationFO.requestingState eq 'FL'}">Florida</aui:option>
								<aui:option value="GA" selected="${providerInformationFO.requestingState eq 'GA'}">Georgia</aui:option>
								<aui:option value="HI" selected="${providerInformationFO.requestingState eq 'HI'}">Hawaii</aui:option>
								<aui:option value="ID" selected="${providerInformationFO.requestingState eq 'ID'}">Idaho</aui:option>
								<aui:option value="IL" selected="${providerInformationFO.requestingState eq 'IL'}">Illinois</aui:option>
								<aui:option value="IN" selected="${providerInformationFO.requestingState eq 'IN'}">Indiana</aui:option>
								<aui:option value="IA" selected="${providerInformationFO.requestingState eq 'IA'}">Iowa</aui:option>
								<aui:option value="KS" selected="${providerInformationFO.requestingState eq 'KS'}">Kansas</aui:option>
								<aui:option value="KY" selected="${providerInformationFO.requestingState eq 'KY'}">Kentucky</aui:option>
								<aui:option value="LA" selected="${providerInformationFO.requestingState eq 'LA'}">Louisiana</aui:option>
								<aui:option value="ME" selected="${providerInformationFO.requestingState eq 'ME'}">Maine</aui:option>
								<aui:option value="MD" selected="${providerInformationFO.requestingState eq 'MD'}">Maryland</aui:option>
								<aui:option value="MA" selected="${providerInformationFO.requestingState eq 'MA'}">Massachusetts</aui:option>
								<aui:option value="MI" selected="${providerInformationFO.requestingState eq 'MI'}">Michigan</aui:option>
								<aui:option value="MN" selected="${providerInformationFO.requestingState eq 'MN'}">Minnesota</aui:option>
								<aui:option value="MS" selected="${providerInformationFO.requestingState eq 'MS'}">Mississippi</aui:option>
								<aui:option value="MO" selected="${providerInformationFO.requestingState eq 'MO'}">Missouri</aui:option>
								<aui:option value="MT" selected="${providerInformationFO.requestingState eq 'MT'}">Montana</aui:option>
								<aui:option value="NE" selected="${providerInformationFO.requestingState eq 'NE'}">Nebraska</aui:option>
								<aui:option value="NV" selected="${providerInformationFO.requestingState eq 'NV'}">Nevada</aui:option>
								<aui:option value="NH" selected="${providerInformationFO.requestingState eq 'NH'}">New Hampshire</aui:option>
								<aui:option value="NJ" selected="${providerInformationFO.requestingState eq 'NJ'}">New Jersey</aui:option>
								<aui:option value="NM" selected="${providerInformationFO.requestingState eq 'NM'}">New Mexico</aui:option>
								<aui:option value="NY" selected="${providerInformationFO.requestingState eq 'NY'}">New York</aui:option>
								<aui:option value="NC" selected="${providerInformationFO.requestingState eq 'NC'}">North Carolina</aui:option>
								<aui:option value="ND" selected="${providerInformationFO.requestingState eq 'ND'}">North Dakota</aui:option>
								<aui:option value="OH" selected="${providerInformationFO.requestingState eq 'OH'}">Ohio</aui:option>
								<aui:option value="OK" selected="${providerInformationFO.requestingState eq 'OK'}">Oklahoma</aui:option>
								<aui:option value="OR" selected="${providerInformationFO.requestingState eq 'OR'}">Oregon</aui:option>
								<aui:option value="PA" selected="${providerInformationFO.requestingState eq 'PA'}">Pennsylvania</aui:option>
								<aui:option value="RI" selected="${providerInformationFO.requestingState eq 'RI'}">Rhode Island</aui:option>
								<aui:option value="SC" selected="${providerInformationFO.requestingState eq 'SC'}">South Carolina</aui:option>
								<aui:option value="SD" selected="${providerInformationFO.requestingState eq 'SD'}">South Dakota</aui:option>
								<aui:option value="TN" selected="${providerInformationFO.requestingState eq 'TN'}">Tennessee</aui:option>
								<aui:option value="TX" selected="${providerInformationFO.requestingState eq 'TX'}">Texas</aui:option>
								<aui:option value="UT" selected="${providerInformationFO.requestingState eq 'UT'}">Utah</aui:option>
								<aui:option value="VT" selected="${providerInformationFO.requestingState eq 'VT'}">Vermont</aui:option>
								<aui:option value="VA" selected="${providerInformationFO.requestingState eq 'VA'}">Virginia</aui:option>
								<aui:option value="WA" selected="${providerInformationFO.requestingState eq 'WA'}">Washington</aui:option>
								<aui:option value="DC" selected="${providerInformationFO.requestingState eq 'DC'}">Washington, D.C.</aui:option>
								<aui:option value="WV" selected="${providerInformationFO.requestingState eq 'WV'}">West Virginia</aui:option>
								<aui:option value="WI" selected="${providerInformationFO.requestingState eq 'WI'}">Wisconsin</aui:option>
								<aui:option value="WY" selected="${providerInformationFO.requestingState eq 'WY'}">Wyoming</aui:option>
							</aui:select>
						</aui:col>
						<aui:col span="3">
					  <aui:input type="text" inlineLabel="true" name="requestingZip" id="requestingZip" label="psr.label.zip.code" value="${providerInformationFO.requestingZip}" maxlength="<%= ZIP_LEN %>" >
							<aui:validator name="digits" />
								<aui:validator name="minLength"><%= ZIP_LEN %></aui:validator>
							</aui:input>
						</aui:col>
					</aui:row>
				</aui:container>
			</aui:fieldset>

			<aui:fieldset>
				<aui:row>
					<aui:button-row cssClass="btn-divider">
						<div onmouseenter="setSaveButtonStatus()">
							<aui:button name="saveProviderInfo" type="button" primary="true" cssClass="pull-right saveAction" value="psr.label.save" onclick="javascript:checkValidForm('providerInformationDetails')" />
						</div>
						<aui:button name="cancelProviderInfo" id="cancelProviderInfo" cssClass="btn-gray pull-left" type="button" value="psr.label.cancel" onclick="javascript:closePortlet()" />
					</aui:button-row>
				</aui:row>
			</aui:fieldset>
		</aui:form>
	</liferay-ui:panel>
</div>
<div class="yui3-skin-sam">
	<div id="modal"></div>
</div>
