<%--
/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
 
/**
  * Description
  *		This file contain the Physician Review portlet for Post Service Review
  *
  * CHANGE History
  * 	Version 1.0
  *			Initial version copied from avalon-prior-authorization.
  * 	Version 1.1
  *			Added changes for the initial version of Post Service Review.
  *		Version 1.2						11/08/2017
  *			Added the AvalonEmployeeROPA group to read-only for PA.
  *		Version 1.3						01/31/2018
  *			When the Save button is disabled show a mouse over tooltip with what is missing.
  *		Version 1.4						06/19/2018
  *			Align the AUI fields.
  * 	Version 1.5						07/16/2018
  *			Move the label to the left of the field.
  *		Version 1.6						09/20/2018
  *			Fixed the read-only fields.
  *
  */
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="theme" %>

<%@ page import="com.avalon.lbm.portlets.postservice.model.PhysicianReviewFO"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.util.List"%>
<%@ page import="java.util.*"%>
<%@page import="com.liferay.portal.kernel.service.UserGroupLocalServiceUtil"%>
<%@page import="com.liferay.portal.kernel.model.User"%>

<%@ include file="/html/post-service/init.jsp"%>
<%@ include file="/html/post-service/post-service-details.jsp"%>

<portlet:defineObjects />
<theme:defineObjects />

<portlet:actionURL var="physicianReviewpageURL">
	<portlet:param name="action" value="physicianReviewAction" />
</portlet:actionURL>

<style>
	.ui-datepicker-trigger {
		position: relative;
		top: 4px;
		height: 18px;
		float: right;
	}
	
	.avalon_portlet .control-group label {
		pointer-events: none;
	}
</style>

<script>
	<%
		int NAME_LEN = 70;
		int REQUESTED_BY = 256;
		int NOTE_LEN = 4000;
	%>
</script>

<aui:script>
	var toolTip = "";
	var browserStr = getBrowser();
	var formChangedFlag = false;

	$(function() {
		
		// Highlight the Physician Review selection
		boldSelection();

		$('form').areYouSure({'message' : exitString});

		//  Hide one of the sorted notes text areas
		$(".hide_text").hide();
		
		// Set the date and time fields to the current date and time if they are blank
		var today = new Date();
		var currentDate = convertDateToString(today);
		var currentTime = convertTimeToString(today);
		setField($("#<portlet:namespace/>physReviewReviewerName"), "", "phyy");
		setField($("#<portlet:namespace/>physReviewRequestedDate"), currentDate, "phyy");
		setField($("#<portlet:namespace/>physReviewRequestedTime"), currentTime, "phyy");
		setField($("#<portlet:namespace/>physReviewReturnedDate"), "", "phyy");
		setField($("#<portlet:namespace/>physReviewReturnedTime"), "", "phyy");

		// Show date picker for IE and Firefox
		if ((browserStr == "IE") || (browserStr == "Firefox") || (browserStr == "unknown")) {
			$("#<portlet:namespace/>physReviewRequestedDate").datepicker({
				showOn: "button",
				buttonImage: "<%=request.getContextPath()%>	/images/icondatepicker.png",
				buttonImageOnly : true,
				dateFormat: 'mm/dd/yy',
				changeMonth: true,
				changeYear: true,
				onSelect: function(selected) {
					
					// Set the time if it is not set
					setTime(true);
					
					// Cause the validation on the date field
					processItem("<portlet:namespace/>physReviewRequestedDate", "<portlet:namespace/>physReviewRequestedDate");
				
					// Set the status of the Save button
					setSaveButtonStatus();
				}
			});
			$("#<portlet:namespace/>physReviewRequestedDate").attr("placeholder", "mm/dd/yyyy");

			$("#<portlet:namespace/>physReviewReturnedDate").datepicker({
				showOn: "button",
				buttonImage: "<%=request.getContextPath()%>	/images/icondatepicker.png",
				buttonImageOnly : true,
				dateFormat: 'mm/dd/yy',
				changeMonth: true,
				changeYear: true,
				onSelect: function(selected) {
					
					// Set the time if it is not set
					setTime(false);
					
					// Cause the validation on the date field
					processItem("<portlet:namespace/>physReviewReturnedDate", "<portlet:namespace/>physReviewReturnedDate");
					
					// Set the status of the Save button
					setSaveButtonStatus();
				}
			});
			$("#<portlet:namespace/>physReviewReturnedDate").attr("placeholder", "mm/dd/yyyy");
			
			$(".ui-datepicker-trigger").css("float", "right");
		}
		
		// Add watermarks to the time fields
		addWatermark('<portlet:namespace/>physReviewRequestedTime', false);
		addWatermark('<portlet:namespace/>physReviewReturnedTime', false);
		
		// Set the initial status of the Save button
		setSaveButtonStatus();
		
		// Adjust the field widths
		adjustMemberDataWidth();
	});

	$(window).resize(function () {
		
		// Reset the fields widths
		adjustMemberDataWidth();
	});

	function getNamespace() {
		return('<portlet:namespace/>');
	}

	/****For disabling tabbing when the Auth submission status are related to Void****/
	YUI().ready('aui-node', 'event', function(Y) {
		/*
		   Set the page to read only if the user is in the HealthPlanEmployee or AvalonEmployeeROPA group and not in 
		   AvalonAdmin, AvalonEmployee, AvalonProvider, PortalApprover, or PortalCreator groups.
		*/
		<% 
			boolean readOnlyFlag = false;
			boolean hpEmployeeFlag = false;
			boolean avalonEmployeeRoPaFlag = false;
			boolean notReadOnlyGroupFlag = false;
			User userU = themeDisplay.getUser();
			long[] groups = userU.getUserGroupIds();
			int length = groups.length;
			for (int i = 0; i < length; ++i) {
				String nextGroup = UserGroupLocalServiceUtil.getUserGroup(groups[i]).getName();
				if (nextGroup.equalsIgnoreCase("HealthPlanEmployee")) {
					hpEmployeeFlag = true;
				} else if (nextGroup.equalsIgnoreCase("AvalonEmployeeROPA")) {
					avalonEmployeeRoPaFlag = true;
				} else if (nextGroup.equalsIgnoreCase("AvalonAdmin") || nextGroup.equalsIgnoreCase("AvalonEmployee") || 
						   nextGroup.equalsIgnoreCase("AvalonProvider") || nextGroup.equalsIgnoreCase("PortalApprover") || 
						   nextGroup.equalsIgnoreCase("PortalCreator")) {
					notReadOnlyGroupFlag = true;
				}
			}
			if (!notReadOnlyGroupFlag && (hpEmployeeFlag || avalonEmployeeRoPaFlag)) {
				readOnlyFlag = true;
			}
		%>
		if (<%= readOnlyFlag %>) {
			
			// Disable the tabbing and make read only
			disableTabbing();
			makeReadonly();
		} else if(Y.one('#authSubmitStatusdisableId')){
			var authSubmitStatusdisable = Y.one('#authSubmitStatusdisableId').val();
				
			if(authSubmitStatusdisable!=null && authSubmitStatusdisable!="" && authSubmitStatusdisable!='undefined'){
				if(authSubmitStatusdisable=="Sent" ||authSubmitStatusdisable=="Void HP - Submitted" || authSubmitStatusdisable=="Void-Submitted" || authSubmitStatusdisable=="Void"||authSubmitStatusdisable=="Void HP"){
					if(Y.one('.avalon_portlet')){
						// Opening all fields
					}
				}
			}
		}
	});

	/* preventing mouse click action on field which are readonly */
	YUI().use('aui-node', function(Y) {
		if (Y.one('.optdisable')) {
			Y.all('.optdisable').on('click', function(e) {
				e.preventDefault();
			});
			$('.optdisable').prop("readonly", true);
		}
	});

	
	/* making field readonly and disabling tabindex when the field contains data already */
	YUI().ready('aui-node', 'event', function(Y) {

		if (Y.one('#<portlet:namespace/>physReviewRequestedDate')) {
			var physReviewRequestedDate = Y.one('#<portlet:namespace/>physReviewRequestedDate').val();
			
			if (physReviewRequestedDate != null && physReviewRequestedDate != "" && physReviewRequestedDate != 'undefined') {
				Y.all('.phyy').addClass('optdisable');
				Y.all('.phyy').setAttribute("tabindex", "-1");
				$('.phyy').prop("readonly", true);
				if ((browserStr == "unknown") || (browserStr == "IE") || (browserStr == "Firefox")) {
					if ($("#<portlet:namespace/>physReviewRequestedDate").hasClass('phyy')) {
						$("#<portlet:namespace/>physReviewRequestedDate").datepicker('destroy');
					}
				}
			}
		};

		if (Y.one('#<portlet:namespace/>physReviewReturnedDate')) {
			var physReviewReturnedDate = Y.one('#<portlet:namespace/>physReviewReturnedDate').val();
			
			if (physReviewReturnedDate != null && physReviewReturnedDate != "" && physReviewReturnedDate != 'undefined') {
				Y.all('.phyy').addClass('optdisable');
				Y.all('.phyy').setAttribute("tabindex", "-1");
				$('.phyy').prop("readonly", true);
				if ((browserStr == "unknown") || (browserStr == "IE") || (browserStr == "Firefox")) {
					if ($("#<portlet:namespace/>physReviewRequestedDate").hasClass('phyy')) {
						$("#<portlet:namespace/>physReviewReturnedDate").datepicker('destroy');
					}
				}
			}
		};

		YUI().use('aui-timepicker', function(Y) {
			new Y.TimePicker({
				trigger : 'input.timepicker',
				popover : {
					zIndex : 1
				},
				on : {
					selectionChange : function(event) {
						console.log(event.newSelection)
					}
				}
			});
		});

		var dtTimeSentCheck = "<%=renderRequest.getAttribute("datetime") %>";

		var dtTimeRetCheck = "<%=renderRequest.getAttribute("datetimereturned") %>";

		var decisionCheck = "<%=renderRequest.getAttribute("decision") %>";

		var phyNameCheck = "<%=renderRequest.getAttribute("physicianName") %>";

		var dtTimeSent = Y.one('#<portlet:namespace/>physReviewRequestedDate');
		var dtTimeRet = Y.one('#<portlet:namespace/>physReviewReturnedDate');
		var decision = Y.one('#<portlet:namespace/>physReviewDecision');
		var phyName = Y.one('#<portlet:namespace/>physReviewReviewerName');

		if (dtTimeSentCheck === "true") {
			dtTimeSent.setAttribute("readonly", "true");
		}
		if (dtTimeRetCheck === "true") {
			dtTimeRet.setAttribute("readonly", "true");
		}
		if (decisionCheck === "true") {
			decision.setAttribute("readonly", "true");
		}
		if (phyNameCheck === "true") {
			phyName.setAttribute("readonly", "true");
		}
	});
	
	function setSaveButtonStatus() {

		if (isValidForm() && formChangedFlag) {
			
			// Enable SAVE button
			$("#<portlet:namespace/>savePhysReview").removeAttr("disabled");
		} else {
			
			// Disable SAVE button
			$("#<portlet:namespace/>savePhysReview").attr("disabled", "disabled");
		}

		// Add the mouse over text for the save button
		$("#<portlet:namespace/>savePhysReview").prop("title", toolTip);
	}
	
	function setTime(requestedFlag) {
		var dateField = null;
		var timeField = null;
		
		// Set the date and time field
		if (requestedFlag) {
			dateField = "<portlet:namespace/>physReviewRequestedDate";
			timeField = "<portlet:namespace/>physReviewRequestedTime";
		} else {
			dateField = "<portlet:namespace/>physReviewReturnedDate";
			timeField = "<portlet:namespace/>physReviewReturnedTime";
		}

		// Remove the watermark
		var timeValue = $("#" + timeField).val();
		if (timeValue == "hh:mm AM/PM") {
			timeValue = "";
		}

		// Only set a blank time field
		if (timeValue == "") {
			// Test for HTML5 errors
			var dateFormatValid = true;
			if ((browserStr != "unknown") && (browserStr != "IE") && (browserStr != "Firefox")) {
				var date_html5 = document.getElementById(dateField);
				if (date_html5.validity.badInput) {
					dateFormatValid = false;
				}
			}
			
			// Test for a valid date
			if (dateFormatValid && isValidDate($("#" + dateField).val(), true)) {
				var today = new Date();
				var currentTime = convertTimeToString(today);
				// Set the time field to the current time
				$("#" + timeField).val(currentTime);
			}
		}
	}

	var returnedFlag = false;

	function isValidForm() {
		var physPhysicianName = $("#<portlet:namespace/>physReviewReviewerName").val();
		var physReviewRequestedDate = $("#<portlet:namespace/>physReviewRequestedDate").val();
		var physReviewRequestedTime = $("#<portlet:namespace/>physReviewRequestedTime").val();
		var physReviewReturnedDate = $("#<portlet:namespace/>physReviewReturnedDate").val();
		var physReviewReturnedTime = $("#<portlet:namespace/>physReviewReturnedTime").val();
		var physReviewRationaleCreate = $("#<portlet:namespace/>physReviewRationaleCreate").val();

		var physReviewRequestedDate_html5 = document.getElementById("<portlet:namespace/>physReviewRequestedDate");
		var physReviewReturnedDate_html5 = document.getElementById("<portlet:namespace/>physReviewReturnedDate");

		// Initialize the tool tip for the save button
		toolTip = "";
		
		// Remove white space
		physReviewRationaleCreate = physReviewRationaleCreate.trim();

		// Test for HTML5 errors
		var dateFormatValid = true;
		if ((browserStr != "unknown") && (browserStr != "IE") && (browserStr != "Firefox")) {
			if (physReviewRequestedDate_html5.validity.badInput) {
				dateFormatValid = false;
			}
		}

		// Set the times if they are blank and the date is valid
		setTime(true);
		setTime(false);
		
		var returnFlag = false;
		
		// If returned date or time is set both are required
		var returnedDateFlag = false;
		if ((physReviewReturnedDate.length > 0) || ((physReviewReturnedTime.length > 0) && (physReviewReturnedTime != "hh:mm AM/PM"))) {
			returnedDateFlag = true;
		}

		if (dateFormatValid && isValid(physPhysicianName, <%= NAME_LEN %>, true) && isValidDate(physReviewRequestedDate, true) && isValidTime(physReviewRequestedTime, true) && 
			isValidDate(physReviewReturnedDate, returnedDateFlag) && isValidTime(physReviewReturnedTime, returnedDateFlag) &&  isValid(physReviewRationaleCreate, <%= NOTE_LEN %>, false)) {
			returnFlag = true;
		} else {

			// Build the toolTip for the Save button
			// Test the physician name
			if (!isValid(physPhysicianName, <%= NAME_LEN %>, true)) {
				toolTip += addToToolTip("The Physician Name is not valid", toolTip.length);
			}

			// Test the date sent
			if (physReviewRequestedDate_html5.validity.badInput || !isValidDate(physReviewRequestedDate, true)) {
				toolTip += addToToolTip("The Date Sent is not valid", toolTip.length);
			}

			// Test the time sent
			if (!isValidTime(physReviewRequestedTime, true)) {
				toolTip += addToToolTip("The Time Sent is not valid", toolTip.length);
			}

			// Test the date returned
			if (physReviewReturnedDate_html5.validity.badInput || !isValidDate(physReviewReturnedDate, returnedDateFlag)) {
				toolTip += addToToolTip("The Date Returned is not valid", toolTip.length);
			}

			// Test the time returned
			if (!isValidTime(physReviewReturnedTime, returnedDateFlag)) {
				toolTip += addToToolTip("The Time Returned is not valid", toolTip.length);
			}

			// Test the physician rationale
			if (!isValidTime(physReviewReturnedTime, returnedDateFlag)) {
				toolTip += addToToolTip("The Physician Rationale is not valid", toolTip.length);
			}
		}
		
		if ((toolTip.length == 0) && !formChangedFlag) {
			
			// Add a toolTip for enabling the Save button by entering a field.
			toolTip = "There is no new data to save";
		}

		return returnFlag;
	}
	
	function makeReadonly() {
	
		// Make all fields readonly
		$('#<portlet:namespace/>physReviewReviewerName').addClass("optdisable");
		$('#<portlet:namespace/>physReviewRequestedBy').addClass("optdisable");
		$('#<portlet:namespace/>physReviewRequestedDate').addClass("optdisable");
		$('#<portlet:namespace/>physReviewRequestedTime').addClass("optdisable");
		$('#<portlet:namespace/>physReviewReturnedDate').addClass("optdisable");
		$('#<portlet:namespace/>physReviewReturnedTime').addClass("optdisable");
		$('#<portlet:namespace/>physReviewDecision').addClass("optdisable");
		$('#<portlet:namespace/>physReviewDecisionReason').addClass("optdisable");
		$('#<portlet:namespace/>physReviewRationaleCreate').addClass("optdisable");
		
		$('#<portlet:namespace/>physReviewReviewerName').prop("readonly", true);
		$('#<portlet:namespace/>physReviewRequestedBy').prop("readonly", true);
		$('#<portlet:namespace/>physReviewRequestedDate').prop("readonly", true);
		$('#<portlet:namespace/>physReviewRequestedTime').prop("readonly", true);
		$('#<portlet:namespace/>physReviewReturnedDate').prop("readonly", true);
		$('#<portlet:namespace/>physReviewReturnedTime').prop("readonly", true);
		$('#<portlet:namespace/>physReviewDecision').prop("readonly", true);
		$('#<portlet:namespace/>physReviewDecisionReason').prop("readonly", true);
		$('#<portlet:namespace/>physReviewRationaleCreate').prop("readonly", true);

		// Disable the date pickers for IE
		if ((browserStr == "unknown") || (browserStr == "IE") || (browserStr == "Firefox")) {
			$("#<portlet:namespace/>physReviewRequestedDate").datepicker('destroy');
			$("#<portlet:namespace/>physReviewReturnedDate").datepicker('destroy');
		}

		// Make the buttons readonly
		$('#<portlet:namespace/>savePhysReview').addClass("optdisable");

		$('#<portlet:namespace/>savePhysReview').prop("readonly", true);

		// Disable all of the dropdowns for IE
		$('#<portlet:namespace/>physReviewDecision').attr('disabled', true);
		$('#<portlet:namespace/>physReviewDecisionReason').attr('disabled', true);
	}
	
	function disableTabbing() {
	
		// Disable tabbing to all fields
		$('#<portlet:namespace/>physReviewReviewerName').prop('tabindex', '-1'); 
		$('#<portlet:namespace/>physReviewRequestedBy').prop('tabindex', '-1'); 
		$('#<portlet:namespace/>physReviewRequestedDate').prop('tabindex', '-1'); 
		$('#<portlet:namespace/>physReviewRequestedTime').prop('tabindex', '-1'); 
		$('#<portlet:namespace/>physReviewReturnedDate').prop('tabindex', '-1'); 
		$('#<portlet:namespace/>physReviewReturnedTime').prop('tabindex', '-1'); 
		$('#<portlet:namespace/>physReviewDecision').prop('tabindex', '-1'); 
		$('#<portlet:namespace/>physReviewDecisionReason').prop('tabindex', '-1'); 
		$('#<portlet:namespace/>physReviewRationaleCreate').prop('tabindex', '-1'); 
	
		// Disable tabbing to all buttons
		$('#<portlet:namespace/>savePhysReview').prop('tabindex', '-1');
	}
</aui:script>

<%
	List readFdata = new ArrayList();
	String fdata_desc = "";
	String fdata_asc = "";
	
	if ((request.getAttribute("readData") != null)) {
	    readFdata = (List) request.getAttribute("readData");
			
		// Create the ascending list
		for (int i = 0; i < readFdata.size(); i++) {
	        fdata_asc = fdata_asc + readFdata.get(i) + System.getProperty("line.separator");
	    }
			
		// Create the descending list
		for (int i = readFdata.size() - 1; i >= 0; i--) {
			fdata_desc = fdata_desc + readFdata.get(i) + System.getProperty("line.separator");
		}
	}
	
	String physReviewDecisionReason = "", physReviewDecision = "", requestedBy = "", physReviewReviewerName = "", physReviewRequestedDate = "", physReviewRequestedTime = "", physReviewReturnedDate = "", physReviewReturnedTime = "";
	if ((request.getAttribute("physicianReviewFO1") != null)) {
	    PhysicianReviewFO physicianReviewFO = (PhysicianReviewFO) request.getAttribute("physicianReviewFO1");
	    requestedBy = (String) request.getAttribute("requestedBy");
	    physReviewReviewerName = physicianReviewFO.getPhysReviewReviewerName();
	    physReviewRequestedTime = physicianReviewFO.getPhysReviewRequestedTime();
	    physReviewRequestedDate = physicianReviewFO.getPhysReviewRequestedDate();
	    physReviewReturnedDate = physicianReviewFO.getPhysReviewReturnedDate();
	    physReviewReturnedTime = physicianReviewFO.getPhysReviewReturnedTime();
	    physReviewDecisionReason = physicianReviewFO.getPhysReviewDecisionReason();
	    physReviewDecision = physicianReviewFO.getPhysReviewDecision();
	}
%>

<div id="busy_indicator" style="display: none">
	<img src="<%=request.getContextPath()%>/images/busy-spinner.gif" alt="Busy indicator">
</div>

<liferay-ui:panel title="psr.physician.review" collapsible="false">
	<aui:form name="physicianReviewDetails" action="${physicianReviewpageURL}" method="post" commandname="physicianReviewFO" onChange="formChangedFlag = true; javascript:setSaveButtonStatus()">
		<aui:fieldset label="psr.physician.review.physician.information">
			<aui:container>
				<aui:row>
					<aui:col span="6">
						<aui:input type="text" inlineLabel="true" name="physReviewReviewerName" id="physReviewReviewerName" label="psr.physician.review.label.physician.name" value='<%=physReviewReviewerName%>' >
							<aui:validator name="required" />
							<aui:validator name="maxLength">70</aui:validator>
						</aui:input>
					</aui:col>
					<aui:col span="6">
						<aui:input type="text" inlineLabel="true" name="physReviewRequestedBy" label="psr.physician.review.label.requested.by" value='<%=requestedBy%>' disabled="true" maxlength="<%= REQUESTED_BY %>" />
					</aui:col>
				</aui:row>
				<aui:row>
					<aui:col span="6">
						<aui:input type="date" inlineLabel="true" name="physReviewRequestedDate" id="physReviewRequestedDate" cssClass="phyy" label="psr.physician.review.label.date.sent" value="<%=physReviewRequestedDate%>"
						           onChange="javascript:setTime(true)" >
							<aui:validator name="required" />
							<aui:validator name="custom" errorMessage="Please enter a valid date (format: mm/dd/yyyy).">
								function (val, fieldNode, ruleValue) {
									var retValue = false;
									
									// Test for invalid date
									if ((browserStr == "IE") || (browserStr == "Firefox") || (browserStr == "unknown")) {
										retValue = isValidDate(val, true);
									} else {
										var physReviewRequestedDate = document.getElementById("<portlet:namespace/>physReviewRequestedDate");
										retValue = !physReviewRequestedDate.validity.badInput;
									}
									
									return retValue;
								}
							</aui:validator>
						</aui:input>
					</aui:col>
					<aui:col span="6">
						<aui:input type="text" inlineLabel="true" name="physReviewRequestedTime" id="physReviewRequestedTime" label="psr.physician.review.label.time.sent" value="<%=physReviewRequestedTime%>" >
							<aui:validator name="required" />
							<aui:validator  name="custom" errorMessage="Please enter a valid time (format: hh:mm AM/PM).">
								function (val, fieldNode, ruleValue) {
									var retValue = false;
								
									retValue = isValidTime(val, true);
								
									return retValue;
								}
							</aui:validator>
						</aui:input>
					</aui:col>
				</aui:row>
				<aui:row>
					<aui:col span="6">
						<aui:input type="date" inlineLabel="true" name="physReviewReturnedDate" id="physReviewReturnedDate" label="psr.physician.review.label.date.returned" value="<%=physReviewReturnedDate%>"
						           onChange="javascript:setTime(false)" >
							<aui:validator name="custom" errorMessage="Date Returned is required when Time Returned has been entered.  Please enter a Date Returned as well.">
								function (val, fieldNode, ruleValue) {
									var retValue = false;
									
									if (!isValidDate(val, false)) {
										
										// Do not cause this error message if the date format is invalid
										retValue = true;
									}
									
									if (!retValue) {
										var physReviewReturnedTime = $("#<portlet:namespace />physReviewReturnedTime").val();
										
										retValue = isValidDate(val, false);
										
										// Check for watermark in returned time
										if (physReviewReturnedTime == "hh:mm AM/PM") {
											physReviewReturnedTime = "";
										}
											
										if (val.length == 0) {
										
											// Has the date been entered
											if (physReviewReturnedTime.length > 0) {
												retValue = false;
											}
										}							    	
									}
								
									if (!returnedFlag) {
									
										// Run the physReviewReturnedTime validation
										returnedFlag = true;
										processItem("<portlet:namespace />physReviewReturnedTime", "<portlet:namespace />physReviewReturnedDate")
										returnedFlag = false;
									}
								
								  	return retValue;
							 	}
							</aui:validator>
							<aui:validator name="custom" errorMessage="Please enter a valid date (format: mm/dd/yyyy).">
								function (val, fieldNode, ruleValue) {
									var retValue = false;
									
									// Test for invalid date
									if ((browserStr == "IE") || (browserStr == "Firefox") || (browserStr == "unknown")) {
										retValue = isValidDate(val, false);
									} else {
								  		    var physReviewReturnedDate = document.getElementById("<portlet:namespace/>physReviewReturnedDate");
								   		retValue = !physReviewReturnedDate.validity.badInput;
								   	}
								    	
									return retValue;
								}
							</aui:validator>
						</aui:input>
					</aui:col>
					<aui:col span="6">
						<aui:input type="text" inlineLabel="true" name="physReviewReturnedTime" id="physReviewReturnedTime" label="psr.physician.review.label.time.returned" value="<%=physReviewReturnedTime%>" >
							<aui:validator name="custom" errorMessage="Time Returned is required when Date Returned has been entered.  Please enter a Time Returned as well.">
								function (val, fieldNode, ruleValue) {
									var retValue = true;
									var physReviewReturnedDate = $("#<portlet:namespace />physReviewReturnedDate").val();
								
									// Check for watermark in returned date and time (IE problem)
									if ((browserStr == "IE") || (browserStr == "Firefox") || (browserStr == "unknown")) {
										if (val == "hh:mm AM/PM") {
											val = "";
										}
									}
								
								  	if ((val.length == 0) || (val.indexOf(" AM/PM") != -1)) {
								
										// Has the date been entered
								  		if (physReviewReturnedDate.length > 0) {
								  	  		retValue = false;
								  	  	}
									}							    	
								
									if (!returnedFlag) {
									
										// Run the ptpDecisionReturnedTime validation
										returnedFlag = true;
										processItem("<portlet:namespace />physReviewReturnedDate", "<portlet:namespace />physReviewReturnedTime")
										returnedFlag = false;
									}
								
									return retValue;
								}
							</aui:validator>
							<aui:validator name="custom" errorMessage="Please enter a valid time (format: hh:mm AM/PM).">
								function (val, fieldNode, ruleValue) {
									var retValue = false;
								
									retValue = isValidTime(val, false);
								
									return retValue;
								}
							</aui:validator>
						</aui:input>
					</aui:col>
				</aui:row>
				<aui:row>
					<% if (physReviewDecision != null && !physReviewDecision.equalsIgnoreCase("") && !physReviewDecision.equalsIgnoreCase("null")) { %>
						<aui:col span="6">
							<aui:select inlineLabel="true" name="physReviewDecision" cssClass="span7 phyyd" label="psr.physician.review.label.decision" id="physReviewDecision" >
								<aui:option value="">Make a Selection</aui:option>
								<aui:option value="approved" selected="<%=physReviewDecision.equals(\"approved\")%>">Approved</aui:option>
								<aui:option value="partiallyApproved" selected="<%=physReviewDecision.equals(\"partiallyApproved\")%>">Partially Approved</aui:option>
								<aui:option value="denied" selected="<%=physReviewDecision.equals(\"denied\")%>">Denied</aui:option>
							</aui:select>
						</aui:col>
					<% } else { %>
						<aui:col span="6">
							<aui:select inlineLabel="true" name="physReviewDecision" cssClass="span7 phyyd" label="psr.physician.review.label.decision" id="physReviewDecision" >
								<aui:option value="">Make a Selection</aui:option>
								<aui:option value="approved">Approved</aui:option>
								<aui:option value="partiallyApproved">Partially Approved</aui:option>
								<aui:option value="denied">Denied</aui:option>
							</aui:select>
						</aui:col>
					<% } %>
					<% if (physReviewDecisionReason != null && !physReviewDecisionReason.equalsIgnoreCase("") && !physReviewDecisionReason.equalsIgnoreCase("null")) { %>
						<aui:col span="6">
							<aui:select inlineLabel="true" name="physReviewDecisionReason" cssClass="span7 phyydr" label="psr.physician.review.label.reason" id="physReviewDecisionReason" >
								<aui:option value="">Make a Selection</aui:option>
								<aui:option value="medicallyNecessary" selected="<%=physReviewDecisionReason.equals(\"medicallyNecessary\")%>">Medically Necessary</aui:option>
								<aui:option value="notMedicallyNecessary" selected="<%=physReviewDecisionReason.equals(\"notMedicallyNecessary\")%>">Not Medically Necessary</aui:option>
								<aui:option value="notaCoveredBenefit" selected="<%=physReviewDecisionReason.equals(\"notaCoveredBenefit\")%>">Not a Covered Benefit</aui:option>
							</aui:select>
						</aui:col>
					<% } else { %>
						<aui:col span="6">
							<aui:select inlineLabel="true" name="physReviewDecisionReason" cssClass="span7 phyydr" label="psr.physician.review.label.reason" id="physReviewDecisionReason" >
								<aui:option value="">Make a Selection</aui:option>
								<aui:option value="medicallyNecessary">Medically Necessary</aui:option>
								<aui:option value="notMedicallyNecessary">Not Medically Necessary</aui:option>
								<aui:option value="notaCoveredBenefit">Not a Covered Benefit</aui:option>
							</aui:select>
						</aui:col>
						<% } %>
				</aui:row>
			</aui:container>
		</aui:fieldset>
  
		<aui:fieldset label="psr.physician.review.physician.rationale">
			<aui:container>
				<aui:row>
					<i class="icon-chevron-sign-up" title="Change Sort Order" onClick="javascript: changeSortOrder(this)" ></i>
					<aui:input name="physReviewRationaleRead" cssClass="oldNotes note-control" type="textarea" resizable="false" label="" readonly="true" rows="7" value="<%= fdata_desc %>" tabindex="-1" />
					<aui:input name="physReviewRationaleRead" cssClass="hide_text oldNotes note-control" type="textarea" resizable="false" label="" readonly="true" rows="7" value="<%= fdata_asc %>" tabindex="-1" />
					<aui:input name="physReviewRationaleCreate" cssClass="note-control" type="textarea" resizable="false" label="psr.physician.review.label.enter.rationale" value='' rows="3" >
						<aui:validator name="maxLength"><%= NOTE_LEN %></aui:validator>
					</aui:input>
				</aui:row>
			</aui:container>
		</aui:fieldset>
		
		<aui:fieldset>
			<aui:button-row cssClass="btn-divider">
				<div onmouseenter="setSaveButtonStatus()">
					<aui:button type="button" primary="true" name="savePhysReview" cssClass="pull-right saveAction" value="psr.label.save" onclick="javascript:checkValidForm('physicianReviewDetails')" />
				</div>
			</aui:button-row>
		</aui:fieldset>
	</aui:form>
</liferay-ui:panel>
