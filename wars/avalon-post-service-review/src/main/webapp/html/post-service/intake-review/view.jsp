<%--
/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
 
 /**
  * Description
  *		This file contain the Intake Review portlet for Post Service Review
  *
  * CHANGE History
  * 	Version 1.0
  *			Initial version copied from avalon-prior-authorization.
  * 	Version 1.1
  *			Added changes for the initial version of Post Service Review.
  * 	Version 1.2						11/6/2017
  *			Added changes for related PAs.
  *			Added three number fields.
  *		Version 1.3						11/08/2017
  *			Added the AvalonEmployeeROPA group to read-only for PA.
  *		Version 1.4						01/26/2018
  *			When the Save button is disabled show a mouse over tooltip with what is missing.
  *		Version 1.5						06/18/2018
  *			Align the AUI fields.
  * 	Version 1.6						07/16/2018
  *			Move the label to the left of the field.
  *		Version 1.7						09/20/2018
  *			Fixed the read-only fields.
  *
  */
--%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="theme" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ page import="java.util.ArrayList"%>
<%@ page import="java.util.List"%>
<%@page import="com.liferay.portal.kernel.service.UserGroupLocalServiceUtil"%>
<%@page import="com.liferay.portal.kernel.model.User"%>

<%@ include file="/html/post-service/init.jsp"%>
<%@ include file="/html/post-service/post-service-details.jsp"%>

<portlet:defineObjects />
<theme:defineObjects />

<portlet:actionURL var="intakeReviewpageURL">
	<portlet:param name="action" value="intakeReviewAction" />
</portlet:actionURL>

<portlet:resourceURL var='validateAuthorizationNumber'
                     id='validateAuthorizationNumber' />

<%
	String color_red = "rgb(181, 3, 3)";
	String color_green = "rgb(72, 143, 6)";

	final int MAX_NUM_PA = 12;
	final int NUM_LEN = 17;
	final int NOTE_LEN = 4000;

	List readFdata=new ArrayList();
	String fdata_desc = "";
	String fdata_asc = "";
	
	if ((request.getAttribute("readData") != null)) {
		readFdata=(List)request.getAttribute("readData");
	
		// Create the ascending list
		for(int i = 0;i < readFdata.size(); i++){
			fdata_asc = fdata_asc + readFdata.get(i) + System.getProperty("line.separator");
		}
		
		// Create the descending list
		for (int i = readFdata.size() - 1; i >= 0; i--) {
			fdata_desc = fdata_desc + readFdata.get(i) + System.getProperty("line.separator");
		}
	}
	
	// Get the related PAs
	String[] relatedPaCodes = {(String)request.getAttribute("relatedPa1"),  (String)request.getAttribute("relatedPa2"),  (String)request.getAttribute("relatedPa3"), 
			                   (String)request.getAttribute("relatedPa4"),  (String)request.getAttribute("relatedPa5"),  (String)request.getAttribute("relatedPa6"), 
			                   (String)request.getAttribute("relatedPa7"),  (String)request.getAttribute("relatedPa8"),  (String)request.getAttribute("relatedPa9"), 
			                   (String)request.getAttribute("relatedPa10"), (String)request.getAttribute("relatedPa11"), (String)request.getAttribute("relatedPa12")};
	
	int AVALON_CLAIM_NUMBER_LEN = 13;
	int HP_CLAIM_NUMBER_LEN = 20;
	int DOCUMENT_CONTROL_NUMBER_LEN = 20;
%>

<aui:script>
	var toolTip = "";
	var browserStr = getBrowser();
	var formChangedFlag = false;

	$(function() { 
		
		// Highlight the Intake Review selection
		boldSelection();

		$('form').areYouSure( {'message':exitString} );
	
		//  Hide one of the sorted notes text areas
		$(".hide_text").hide();
		
		// Set the initial save button status
		setSaveButtonStatus();
		
		// Test for related PAs
		if ($("#<portlet:namespace/>releatePa0").val().length > 0) {
			$("#radioSelectYes").prop("checked", "checked");

			// Show the first PA
			$("#divReleatePa0").show();
			
			// Show any other related PAs
			var paCnt = 1;
			for (; paCnt < <%= MAX_NUM_PA %>; paCnt++) {
				if ($("#<portlet:namespace/>releatePa" + paCnt).val().length > 0) {
					$("#divReleatePa" + paCnt).show();
				} else {
					break;
				}
			}

			// Test for show/hide add auth numbers link
			if ($("#divReleatePa11").is(":visible")) {
				$("#<portlet:namespace/>authNum-add").hide();
			} else {
				$("#<portlet:namespace/>authNum-add").show();
			}
		} else {
			$("#radioSelectNo").prop("checked", "checked");
			
			// Hide the add link
			$("#<portlet:namespace/>authNum-add").hide();
		}
		
		// Set the initial save button status
		setSaveButtonStatus();
		
		// Adjust the field widths
		adjustMemberDataWidth();
	});

	$(window).resize(function () {
		
		// Reset the fields widths
		adjustMemberDataWidth();
	});

	function getNamespace() {
		return('<portlet:namespace/>');
	}

	/****For disabling tabbing when the Auth submission status' are related to Void****/
	YUI().ready('aui-node', 'event', function(Y) {
		/*
		   Set the page to read only if the user is in the HealthPlanEmployee or AvalonEmployeeROPA group and not in 
		   AvalonAdmin, AvalonEmployee, AvalonProvider, PortalApprover, or PortalCreator groups.
		*/
		<% 
			boolean readOnlyFlag = false;
			boolean hpEmployeeFlag = false;
			boolean avalonEmployeeRoPaFlag = false;
			boolean notReadOnlyGroupFlag = false;
			User userU = themeDisplay.getUser();
			long[] groups = userU.getUserGroupIds();
			int length = groups.length;
			for (int i = 0; i < length; ++i) {
				String nextGroup = UserGroupLocalServiceUtil.getUserGroup(groups[i]).getName();
				if (nextGroup.equalsIgnoreCase("HealthPlanEmployee")) {
					hpEmployeeFlag = true;
				} else if (nextGroup.equalsIgnoreCase("AvalonEmployeeROPA")) {
					avalonEmployeeRoPaFlag = true;
				} else if (nextGroup.equalsIgnoreCase("AvalonAdmin") || nextGroup.equalsIgnoreCase("AvalonEmployee") || 
						   nextGroup.equalsIgnoreCase("AvalonProvider") || nextGroup.equalsIgnoreCase("PortalApprover") || 
						   nextGroup.equalsIgnoreCase("PortalCreator")) {
					notReadOnlyGroupFlag = true;
				}
			}
			if (!notReadOnlyGroupFlag && (hpEmployeeFlag || avalonEmployeeRoPaFlag)) {
				readOnlyFlag = true;
			}
		%>
		if (<%= readOnlyFlag %>) {

			// Disable the tabbing and make read only
			disableTabbing();
			makeReadonly();
		} else if(Y.one('#authSubmitStatusdisableId')){
			var authSubmitStatusdisable = Y.one('#authSubmitStatusdisableId').val();
			
			if(authSubmitStatusdisable!=null && authSubmitStatusdisable!="" && authSubmitStatusdisable!='undefined'){
				if(authSubmitStatusdisable=="Sent" || authSubmitStatusdisable=="Void HP - Submitted" || authSubmitStatusdisable=="Void-Submitted" || authSubmitStatusdisable=="Void"||authSubmitStatusdisable=="Void HP"){
				}
			}
		}
	});
	
	function validateRelatedPa() {
		var firstRow = 0;
		var cnt = 0;

		// find the last visible row
		for (; cnt < <%= MAX_NUM_PA %>; cnt++) {
			if (!$("#divReleatePa" + cnt).is(":visible")) {
				firstRow = cnt - 1;
				break;
			}
		}

		// Validate on row past the last visible for a deleted row
		if (firstRow < <%= MAX_NUM_PA %> - 1) {
			firstRow++;
		}
		
		// Force focus to each of the visible related PAs
		for (cnt = 0; cnt < firstRow + 1; cnt++) {
			validatePriorAuthNumber("<portlet:namespace/>releatePa" + cnt);
		}
	}
	
	function deleteRow(id) {
		var firstRow = 0;
		var cnt = 0;

		// find the last visible row
		for (; cnt < <%= MAX_NUM_PA %>; cnt++) {
			if (!$("#divReleatePa" + cnt).is(":visible")) {
				firstRow = cnt - 1;
				break;
			}
		}
		
		// Move all the visible values below the selected row up one
		var nextCount = id;
		var nextvalue = 0;
		for (cnt = id; cnt < firstRow; cnt++) {
			nextvalue = $("#<portlet:namespace/>releatePa" + ++nextCount).val();

			$("#<portlet:namespace/>releatePa" + cnt).val(nextvalue);
		}
		
		// Clear and hide the last visible row.  Do not hide the first row if it is the only visible one.
		$("#<portlet:namespace/>releatePa" + firstRow).val("");
		if (firstRow != "0") {
			$("#divReleatePa" + firstRow).hide();
		}
		
		// Show the add auth numbers link
		$("#<portlet:namespace/>authNum-add").show();
		
		// Test for any remaining problems
		validateRelatedPa();
		
		// Indicate the form has changed and enable the save button
		formChangedFlag = true;
		setSaveButtonStatus();
	}
	
	function showNextRow() {
		var cnt = 0;
		
		// Show the next hidden row
		for (; cnt < <%= MAX_NUM_PA %>; cnt++) {
			if (!$("#divReleatePa" + cnt).is(":visible")) {
				$("#divReleatePa" + cnt).show();
				
				// Force validation of the new blank field to clear any previous errors
				$("#<portlet:namespace/>releatePa" + cnt).focus();
				$("#<portlet:namespace/>releatePa" + cnt).blur();
				
				break;
			}
		}
			
		// Test for all 12 rows shown and hide the add auth numbers link
		if ($("#divReleatePa11").is(":visible")) {
			$("#<portlet:namespace/>authNum-add").hide();
		}
	}
	
	function setFormChanged(id) {
		
		// Indicate the form has changed
		formChangedFlag = true;
	}
	
	function relatedPriorAuthStatus(val) {
		
		// Indicate the form has changed
		formChangedFlag = true;

		if (val == "Yes") {

			// Show the first PA
			$("#divReleatePa0").show();
			
			// Show the add auth numbers link
			$("#<portlet:namespace/>authNum-add").show();
		} else {

			// Hide and clear all PAs
			for (var cnt = 0; cnt < <%= MAX_NUM_PA %>; cnt++) {
				$("#divReleatePa" + cnt).hide();
				$("#<portlet:namespace/>releatePa" + cnt).val("");
			}
			
			// Hide the add link
			$("#<portlet:namespace/>authNum-add").hide();

		}
	}
	
	function validatePriorAuthNumber(id) {
		var authNum = $("#" + id).val();
		var authIdx = id.substring(id.lastIndexOf("_")).replace( /^\D+/g, '');
		
		// Indicate the form has changed
		formChangedFlag = true;
		YUI().ready('autocomplete-list','aui-base','aui-io-request','autocomplete-filters','autocomplete-highlighters',function (A) {
			A.io.request('<%= validateAuthorizationNumber %>', {
				dataType: 'text',
				method: 'POST', 
				data: {
					<portlet:namespace/>priorAuthNumber : authNum,
				},
			    on: {
					success: function() {
						var authNumValidFlag = "Yes";
						if (authNum != "") {
							
							// Remove the new line at the end of the string
							authNumValidFlag = this.get('responseData').replace(/(\r\n|\n|\r)/gm,"")
						}

						// Change the color to green if the authNum is valid and not a duplicate
						if (authNumValidFlag == "Yes") {
							if (!checkDuplicateRelatedPA(authIdx, authNum)) {

								// Hide the error message
								$("#error" + authIdx).hide();
								
								$("#" + id).css("color", "<%= color_green %>");
								$("#" + id).css("border-color", "<%= color_green %>");
							}
						}

						// Change the color to red if the authNum is not valid
						if (authNumValidFlag == "No") {
							if ($("#" + id).css("color") != "<%= color_red %>") {
								
								// Show the error message
								$("#error" + authIdx).show();
							}
							
							$("#" + id).css("color", "<%= color_red %>");
							$("#" + id).css("border-color", "<%= color_red %>");
						}
					}
				}
			});
		});
	}
	
	function isValidForm() {
		var formValid = true;
		var intakeNotesCreate = $("#<portlet:namespace/>intakeNotesCreate").val().trim();
		var avalonClaimNumber = $("#<portlet:namespace/>avalonClaimNumber").val().trim();
		var hpClaimNumber = $("#<portlet:namespace/>hpClaimNumber").val().trim();
		var documentControlNumber = $("#<portlet:namespace/>documentControlNumber").val().trim();
		
		// Initialize the tool tip for the save button
		toolTip = "";
		
		// Validate the claim and control numbers
		if (!isValidAlphanum(avalonClaimNumber, <%= AVALON_CLAIM_NUMBER_LEN %>, false) || !isValidAlphanum(hpClaimNumber, <%= HP_CLAIM_NUMBER_LEN %>, false) || 
			!isValidAlphanum(documentControlNumber, <%= DOCUMENT_CONTROL_NUMBER_LEN %>, false)) {
			formValid = false;

			// Build the toolTip for the Save button
			// Test the avalon claim number
			if (!isValidAlphanum(avalonClaimNumber, <%= AVALON_CLAIM_NUMBER_LEN %>, false)) {
				toolTip += addToToolTip("The Avalon Claim Number is not valid", toolTip.length);
			}

			// Test the HP claim number
			if (!isValidAlphanum(hpClaimNumber, <%= HP_CLAIM_NUMBER_LEN %>, false)) {
				toolTip += addToToolTip("The HP Claim Number is not valid", toolTip.length);
			}

			// Test the document control number
			if (!isValidAlphanum(documentControlNumber, <%= DOCUMENT_CONTROL_NUMBER_LEN %>, false)) {
				toolTip += addToToolTip("The Document Control Number is not valid", toolTip.length);
			}
		}

		// Validate the related PAs
		for (var cnt = 0; cnt < <%= MAX_NUM_PA %>; cnt++) {
			
			// Test all 12 possible relate PAs.  Stop when an error is found.  There may be blanks that will be removed when saved.
			if ($("#divReleatePa" + cnt).is(":visible")) {
				var nextRelated = $("#<portlet:namespace/>releatePa" + cnt).val();
				if (nextRelated.trim() != "") {

					// Test for an invalid record (shown in red)
					if ($("#<portlet:namespace/>releatePa" + cnt).css("color") == "<%= color_red %>") {
						formValid = false;
						toolTip += addToToolTip("One of more of the Related Prior Authorizations are not valid", toolTip.length);
						break;
					}
				}
			}
		}
	
		// Validate the notes
		if (!isValid(intakeNotesCreate, <%= NOTE_LEN %>, false)) {
			formValid = false;
			toolTip += addToToolTip("The PSR Intake Note is not valid", toolTip.length);
		}
		
		if (!formChangedFlag) {
			
			// Add a toolTip for enabling the Save button by entering a field.
			toolTip = "There is no new data to save";
			
		}
		
		return formValid;
	}

	var validationInProcessFlag = false;
	function checkDuplicateRelatedPA(idx, val) {
		var retVal = false;
		var chkVal = val;
		var nextVal = null;

		// Do not test hidden or empty fields
		if (val != '') {

			// Compare to every PA except the selected one
			for (var cnt = 0; cnt <= <%= MAX_NUM_PA %>; cnt++) {
				if ($("#divReleatePa" + cnt).is(":visible") && (cnt != idx)) {
					nextVal = $("#<portlet:namespace/>releatePa" + cnt).val()
					
					if (nextVal != '') {
						if (chkVal == nextVal) {
							retVal = true;
							break;
						}
					}
				}
			}
		}
	
		// Validate the remaining PAs that are visible and not empty
		if (validationInProcessFlag == false) {
			validationInProcessFlag = true;

			for (var cnt = 0; cnt <= <%= MAX_NUM_PA %>; cnt++) {
				if ($("#divReleatePa" + cnt).is(":visible") && (cnt != idx)) {
					nextVal = $("#<portlet:namespace/>releatePa" + cnt).val()
					if (nextVal != '') {
	
						// Force validation of all the PAs except the selected one
						$("#<portlet:namespace/>releatePa" + cnt).focus();
						$("#<portlet:namespace/>releatePa" + cnt).blur();
					}
				}
			}
			validationInProcessFlag = false;
		}

		return retVal;
	}

	function setSaveButtonStatus() {
		
		// Disable the save button until all the fields are valid
		if (isValidForm() && formChangedFlag) {
			
			// Enable the save button
			$("#<portlet:namespace/>saveIntakeNotes").removeAttr("disabled");
		} else {
			
			// Disable the save button
			$("#<portlet:namespace/>saveIntakeNotes").attr("disabled", "disabled");
		}

		// Add the mouse over text for the save button
		$("#<portlet:namespace/>saveIntakeNotes").prop("title", toolTip);
	}
	
	function makeReadonly() {

		// Make all fields readonly
		$('#<portlet:namespace/>intakeNotesCreate').addClass("optdisable");
		$("#<portlet:namespace/>avalonClaimNumber").addClass("optdisable");
		$("#<portlet:namespace/>hpClaimNumber").addClass("optdisable");
		$("#<portlet:namespace/>documentControlNumber").addClass("optdisable");

		if ((browserStr == "IE") || (browserStr == "Firefox") || (browserStr == "unknown")) {
			$("#addRelatedPriorAuthQuestion").attr('disabled', true);
		} else {
			$("#addRelatedPriorAuthQuestion").addClass("optdisable");
		}
		for (var cnt = 0; cnt < <%= MAX_NUM_PA %>; cnt++) {
			$("#<portlet:namespace/>releatePa" + cnt).addClass("optdisable");
		}
		$("#<portlet:namespace/>authNum-add").addClass("optdisable");

		// Disable clicking on the labels
		$(".control-label").addClass('optdisable');

		// Disable the delete icons
		$(".icon-trash").addClass("optdisable");
		
		// Make the buttons readonly
		$('#<portlet:namespace/>saveIntakeNotes').addClass("optdisable");
	}
	
	function disableTabbing() {

		// Disable tabbing to all fields
		$('#<portlet:namespace/>intakeNotesCreate').prop('tabindex', '-1'); 
		$("#<portlet:namespace/>avalonClaimNumber").prop('tabindex', '-1'); 
		$("#<portlet:namespace/>hpClaimNumber").prop('tabindex', '-1'); 
		$("#<portlet:namespace/>documentControlNumber").prop('tabindex', '-1'); 

		$("#radioSelectYes").prop('tabindex', '-1');
		$("#radioSelectNo").prop('tabindex', '-1');
		for (var cnt = 0; cnt < <%= MAX_NUM_PA %>; cnt++) {
			$("#<portlet:namespace/>releatePa" + cnt).prop('tabindex', '-1'); 
		}
		$("#<portlet:namespace/>authNum-add").hide(); 	

		// Disable tabbing to all buttons
		$('#<portlet:namespace/>saveIntakeNotes').prop('tabindex', '-1');
	}
</aui:script>

<div id="busy_indicator" style="display: none">
	<img src="<%=request.getContextPath() %>/images/busy-spinner.gif" alt="Busy indicator">
</div>

<liferay-ui:panel title="psr.intake.review" collapsible="false">
	<aui:form name="intakeReviewDetails" action="${intakeReviewpageURL}" method="post" commandname="intakeReviewFO" onChange="javascript:setSaveButtonStatus()">

		 <aui:fieldset label="psr.intake.review.existing.pa">
		 	<aui:container>
				<aui:row>
					<aui:col span="6">
						<aui:input type="text" inlineLabel="true" name="avalonClaimNumber" id="avalonClaimNumber" label="psr.intake.review.label.avalon.claim.number" value='<%= request.getAttribute("priorAuthClaimNumber") %>'
						           maxlength="<%= AVALON_CLAIM_NUMBER_LEN %>" onChange="javascript:setFormChanged(this.id)" >
							<aui:validator name="alphanum" />
						</aui:input>
					</aui:col>
					<aui:col span="6">
						<aui:input type="text" inlineLabel="true" name="hpClaimNumber" id="hpClaimNumber" label="psr.intake.review.label.hp.claim.number" value='<%= request.getAttribute("priorAuthHpClaimNumber") %>'
						           maxlength="<%= HP_CLAIM_NUMBER_LEN %>" onChange="javascript:setFormChanged(this.id)" >
							<aui:validator name="alphanum" />
						</aui:input>
					</aui:col>
				</aui:row>
				<aui:row>
					<aui:col span="6">
						<aui:input type="text" inlineLabel="true" name="documentControlNumber" id="documentControlNumber" label="psr.intake.review.label.document.control.number" value='<%= request.getAttribute("priorAuthDocumentNumber") %>'
						           maxlength="<%= DOCUMENT_CONTROL_NUMBER_LEN %>" onChange="javascript:setFormChanged(this.id)" >
							<aui:validator name="alphanum" />
						</aui:input>
					</aui:col>
				</aui:row>
		 		<aui:row>
					<aui:col span="12">
			 			<div id="addRelatedPriorAuthQuestion">
							Are there any related Prior Authorization? <span style="color: blue"> *</span>
							<input style="width: 50px; margin-top: -1px;" type="radio" name="radioselect" id="radioSelectYes" value="Yes" onChange="javascript:relatedPriorAuthStatus(this.value)" >Yes
							<input style="width: 50px; margin-top: -1px;" type="radio" name="radioselect" id="radioSelectNo"  value="No"  onChange="javascript:relatedPriorAuthStatus(this.value)" >No
						</div>
   					</aui:col>
   				</aui:row>
    			<% for (int i = 0; i < MAX_NUM_PA; i++) { %>
   					<% String inputId = "releatePa" + i; %>
   					<% String devId = "divReleatePa" + i; %>
   					<% String errorId = "error" + i; %>
   					<div id="<%= devId %>" style="display: none">
 		   				<aui:row>
	  						<aui:col span="6">
								<aui:input type="text" inlineLabel="true" name="<%= inputId %>" id="<%= inputId %>" cssClass="toUppercase" style="text-transform:uppercase" label="psr.intake.review.label.number" maxlength="<%= NUM_LEN %>"
								           value='<%= relatedPaCodes[i] %>' onChange="validatePriorAuthNumber(this.id); setTimeout(function(){setSaveButtonStatus()}, 2000);">
									<aui:validator name="custom" errorMessage="Please enter only alphanumeric.">
										function (val, fieldNode, ruleValue) {
											var retValue = false;
											
											// Allow all numbers of blank
											if (val.length == 0) {
												retValue = true;
											} else if (val.match(/^[a-zA-Z0-9]+$/) != null) {
												retValue = true;
											}
											
											return retValue;
										}
									</aui:validator>
									<aui:validator name="custom" errorMessage="Please enter at least 17 characters.">
										function (val, fieldNode, ruleValue) {
											var retValue = false;

											if ((val.length == 17) || (val.length == 0)) {
												retValue = true;
											}
	
											return retValue;
										}
									</aui:validator>
									<aui:validator name="custom" errorMessage="Cannot enter duplicate related prior authorization.">
										function (val, fieldNode, ruleValue) {
											var retValue = false;
											
											retValue = !checkDuplicateRelatedPA(<%= i %>, val);
	
											return retValue;
										}
									</aui:validator>
								</aui:input>
							</aui:col>
	  						<aui:col span="1">
			   					<i class="icon-trash" title="Delete" onClick="javascript:deleteRow('<%= i %>')"></i>
			   				</aui:col>
		   				</aui:row>
   						<div id="<%= errorId %>" style="color: <%= color_red %>; display: none">
 							<label>Enter a valid prior authorization or post service review number</label>
 						</div>
	   				</div>
   				<% } %>
   				<aui:row id="authNum-add" style="display: none">
					<aui:col span="6">
						<aui:a href="javascript:showNextRow()">
							<aui:icon image="plus" cssClass="pull-right sntdis" label="psr.procedure.info.label.add.authNum" />
						</aui:a>
					</aui:col>
				</aui:row>
			</aui:container>
		</aui:fieldset>
		
		 <aui:fieldset label="psr.intake.review.notes">
		 	<aui:container>
		 		<aui:row>
					<i class="icon-chevron-sign-up" title="Change Sort Order" onClick="javascript: changeSortOrder(this)" ></i>
					<aui:input type="textarea" cssClass="oldNotes note-control" name="intakeNotesRead" resizable="false" label="" readonly="true" rows="7" value='<%= fdata_desc %>' />
					<aui:input type="textarea" cssClass="hide_text oldNotes note-control" name="intakeNotesRead" resizable="false" label="" readonly="true" rows="7" value='<%= fdata_asc %>' />
					<aui:input type="textarea" cssClass="note-control" name="intakeNotesCreate" resizable="false" label="psr.intake.review.label.enter.notes" rows="3" value="" onChange="javascript:setFormChanged(this.id)" >
						<aui:validator name="maxLength"><%= NOTE_LEN %></aui:validator>
					</aui:input>
				</aui:row>
			</aui:container>
		</aui:fieldset>

		<aui:fieldset>
			 <aui:container>
		 		<aui:row>
					<aui:button-row>
						<div onmouseenter="setSaveButtonStatus()">
							<aui:button type="button" primary="true" name="saveIntakeNotes" cssClass="pull-right" value="psr.label.save" onclick="javascript:checkValidForm('intakeReviewDetails')" />
						</div>
					</aui:button-row>
				</aui:row>
			</aui:container>
		</aui:fieldset>
	</aui:form>
</liferay-ui:panel>
