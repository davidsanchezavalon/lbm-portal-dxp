package com.avalon.lbm.portlets.priorauth.controller;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.PortalUtil;

import javax.portlet.PortletRequest;
import javax.portlet.PortletSession;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.ui.Model;

public class ProviderInformationTest {

    @Test
    public void testHandleRenderRequest() {
	ProviderInformation providerInformation = new ProviderInformation();
	RenderRequest request = Mockito.mock(RenderRequest.class);
	RenderResponse response = Mockito.mock(RenderResponse.class);
	Model model = Mockito.mock(Model.class);
	PortletSession portletSession = Mockito.mock(PortletSession.class);
	when(request.getPortletSession()).thenReturn(portletSession);
	when((String) portletSession.getAttribute("authorizationStatus", 1))
		.thenReturn("approved");
	when((String) portletSession.getAttribute("memberDob", 1)).thenReturn(
		"08/28/1983");
	when((String) portletSession.getAttribute("memberName", 1)).thenReturn(
		"DRUE DEVOST");
	when((String) portletSession.getAttribute("memberId", 1)).thenReturn(
		"ZCL06440277");
	when((String) portletSession.getAttribute("authSubmissionStatus", 1))
		.thenReturn("approved");
	when((String) portletSession.getAttribute("authorizationNumber", 1))
		.thenReturn("00100116020800001");
	when((Long) portletSession.getAttribute("authorizationKey", 1))
		.thenReturn(16174L);

	ThemeDisplay themeDisplay = Mockito.mock(ThemeDisplay.class);
	User user = Mockito.mock(User.class);
	when(
		(ThemeDisplay) request
			.getAttribute("LIFERAY_SHARED_THEME_DISPLAY"))
		.thenReturn(themeDisplay);
	when(themeDisplay.getUser()).thenReturn(user);
	when(themeDisplay.getUser().getEmailAddress()).thenReturn(
		"test@liferay.com");

	HttpServletRequest httprequest = Mockito.mock(HttpServletRequest.class);
	HttpServletRequest httpServletRequest2 = Mockito
		.mock(HttpServletRequest.class);
	PortalUtil portalUtil = new PortalUtil();
	Portal portal = Mockito.mock(Portal.class);
	portalUtil.setPortal(portal);
	PortletRequest portletRequest = Mockito.mock(PortletRequest.class);
	when(portalUtil.getHttpServletRequest(any(PortletRequest.class)))
		.thenReturn(httprequest);
	when(
		portalUtil
			.getOriginalServletRequest(any(HttpServletRequest.class)))
		.thenReturn(httpServletRequest2);
	ParamUtil paramUtil = Mockito.mock(ParamUtil.class);
	HttpSession httpSession = Mockito.mock(HttpSession.class);
	PortalUtil portautil = Mockito.mock(PortalUtil.class);
	when(request.getPortletSession()).thenReturn(portletSession);
	try {
	    providerInformation.handleRenderRequest(request, response, model);
	} catch (Exception e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}

    }

    @Test
    public void testHandleActionRequest() {
	ProviderInformation providerInformation = new ProviderInformation();
	RenderRequest request = Mockito.mock(RenderRequest.class);
	RenderResponse response = Mockito.mock(RenderResponse.class);
	Model model = Mockito.mock(Model.class);
	PortletSession portletSession = Mockito.mock(PortletSession.class);
	when(request.getPortletSession()).thenReturn(portletSession);
	when((String) portletSession.getAttribute("memberId", 1)).thenReturn(
		"ZCL06440277");

	when((String) portletSession.getAttribute("memberNumber", 1))
		.thenReturn("1234");
	when((String) portletSession.getAttribute("healthPlan", 1)).thenReturn(
		"approved");
	when((String) portletSession.getAttribute("mpi", 1)).thenReturn(
		"00100116020800001");

	ThemeDisplay themeDisplay = Mockito.mock(ThemeDisplay.class);
	User user = Mockito.mock(User.class);
	when(
		(ThemeDisplay) request
			.getAttribute("LIFERAY_SHARED_THEME_DISPLAY"))
		.thenReturn(themeDisplay);
	when(themeDisplay.getUser()).thenReturn(user);
	when(themeDisplay.getUser().getEmailAddress()).thenReturn(
		"test@liferay.com");

	HttpServletRequest httprequest = Mockito.mock(HttpServletRequest.class);
	HttpServletRequest httpServletRequest2 = Mockito
		.mock(HttpServletRequest.class);
	PortalUtil portalUtil = new PortalUtil();
	Portal portal = Mockito.mock(Portal.class);
	portalUtil.setPortal(portal);
	PortletRequest portletRequest = Mockito.mock(PortletRequest.class);
	when(portalUtil.getHttpServletRequest(any(PortletRequest.class)))
		.thenReturn(httprequest);
	when(
		portalUtil
			.getOriginalServletRequest(any(HttpServletRequest.class)))
		.thenReturn(httpServletRequest2);

	ParamUtil paramUtil = Mockito.mock(ParamUtil.class);
	HttpSession httpSession = Mockito.mock(HttpSession.class);
	PortalUtil portautil = Mockito.mock(PortalUtil.class);
	when(request.getPortletSession()).thenReturn(portletSession);
	// when(PortalUtil.getHttpServletRequest(any(PortletRequest.class))).thenReturn((HttpServletRequest)
	// httpSession);

	try {
	    providerInformation.handleRenderRequest(request, response, model);
	} catch (Exception e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
    }

    /*
     * @Test public void testAuthRedirectAction() { ProviderInformation
     * providerInformation = new ProviderInformation(); ActionRequest request =
     * Mockito.mock(ActionRequest.class); ActionResponse response =
     * Mockito.mock(ActionResponse.class);
     * 
     * ThemeDisplay themeDisplay =Mockito.mock(ThemeDisplay.class); User user =
     * Mockito.mock(User.class);
     * when((ThemeDisplay)request.getAttribute("LIFERAY_SHARED_THEME_DISPLAY"
     * )).thenReturn(themeDisplay); Layout layout = Mockito.mock(Layout.class);
     * LayoutLocalServiceUtil layoutLocalServiceUtil =
     * Mockito.mock(LayoutLocalServiceUtil.class);
     * 
     * 
     * 
     * LiferayPortletURL url = Mockito.mock(LiferayPortletURL.class);
     * System.out.println(layoutLocalServiceUtil);
     * 
     * 
     * PortletURL portletURL= Mockito.mock(PortletURL.class);
     * PortletURLFactoryUtil portletURLFactoryUtil
     * =Mockito.mock(PortletURLFactoryUtil.class); PortletRequest portletRequest
     * =Mockito.mock(PortletRequest.class);
     * 
     * when(themeDisplay.getPlid()).thenReturn(13982L);
     * when(themeDisplay.getCompanyId()).thenReturn((long) 10155);
     * when(themeDisplay.getScopeGroupId()).thenReturn((long) 10195);
     * when(themeDisplay.getLanguageId()).thenReturn("en_US");
     * 
     * 
     * 
     * providerInformation.authRedirectAction(request, response);
     * 
     * 
     * }
     */

}
