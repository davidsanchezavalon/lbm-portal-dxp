package com.avalon.lbm.portlets.priorauth.controller;

import static org.mockito.Mockito.when;

import com.avalon.lbm.portlets.priorauth.model.PeerToPeerFO;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;

import java.util.Map;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletRequest;
import javax.portlet.PortletSession;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.ui.Model;

public class PeerToPeerTest {

    @Test
    public void testHandleRenderRequest() {
	PeerToPeer peerToPeer = new PeerToPeer();

	Model model = Mockito.mock(Model.class);
	RenderRequest request = Mockito.mock(RenderRequest.class);
	RenderResponse response = Mockito.mock(RenderResponse.class);

	PortletSession portletSession = Mockito.mock(PortletSession.class);
	when(request.getPortletSession()).thenReturn(portletSession);

	when((String) portletSession.getAttribute("authorizationStatus", 1))
		.thenReturn("approved");
	when((String) portletSession.getAttribute("memberDob", 1)).thenReturn(
		"08/28/1983");
	when((String) portletSession.getAttribute("memberName", 1)).thenReturn(
		"DRUE DEVOST");
	when((String) portletSession.getAttribute("memberId", 1)).thenReturn(
		"ZCL06440277");
	when((String) portletSession.getAttribute("authorizationAge", 1))
		.thenReturn("2016-02-08 10:25:49.0");
	when((String) portletSession.getAttribute("authorizationNumber", 1))
		.thenReturn("00100116020800001");
	when((Long) portletSession.getAttribute("authorizationKey", 1))
		.thenReturn(16174L);

	when(
		(String) portletSession.getAttribute("providerAuthStatus",
			PortletSession.APPLICATION_SCOPE)).thenReturn(
		"Approved");

	peerToPeer.handleRenderRequest(request, response, model);

    }

    @Test
    public void testPeerToPeerDetails() {
	PeerToPeer peerToPeer = new PeerToPeer();
	ActionRequest request = Mockito.mock(ActionRequest.class);
	ActionResponse response = Mockito.mock(ActionResponse.class);
	Map map = Mockito.mock(Map.class);
	PeerToPeerFO peerToPeerFO = new PeerToPeerFO();

	peerToPeerFO.setPtpDecisionRequestedTime("02:30 AM");
	peerToPeerFO.setPtpDecisionReturnedTime("09:00 AM");

	ThemeDisplay themeDisplay = Mockito.mock(ThemeDisplay.class);
	User user = Mockito.mock(User.class);
	when(
		(ThemeDisplay) request
			.getAttribute("LIFERAY_SHARED_THEME_DISPLAY"))
		.thenReturn(themeDisplay);
	when(themeDisplay.getUser()).thenReturn(user);
	when(themeDisplay.getUser().getEmailAddress()).thenReturn(
		"test@liferay.com");
	PortletSession portletSession = Mockito.mock(PortletSession.class);
	when(request.getPortletSession()).thenReturn(portletSession);

	System.out.println("portletSession" + portletSession);
	when((Long) portletSession.getAttribute("authorizationKey", 1))
		.thenReturn(16174L);
	when((String) portletSession.getAttribute("authorizationNumber", 1))
		.thenReturn("00100116020800001");

	ParamUtil paramUtil = Mockito.mock(ParamUtil.class);
	PortletRequest portletRequest = Mockito.mock(PortletRequest.class);
	when(
		ParamUtil.getString((PortletRequest) request,
			(String) "ptpNotesCreate")).thenReturn(
		"peerToPeerMocknotes");
	when(
		ParamUtil.getString((PortletRequest) request,
			(String) "ptpDecisionRequestedDate")).thenReturn(
		"2016-02-12");
	when(
		ParamUtil.getString((PortletRequest) request,
			(String) "ptpDecisionReturnedDate")).thenReturn(
		"2016-02-27");
	when(
		ParamUtil.getString((PortletRequest) request,
			(String) "ptpPhyName")).thenReturn("test");
	when(
		ParamUtil.getString((PortletRequest) request,
			(String) "ptpDecision")).thenReturn("approved");

	peerToPeer.peerToPeerDetails(request, response, peerToPeerFO, map);

    }

    @Test
    public void testProviderHandler() {

	PeerToPeer peerToPeer = new PeerToPeer();
	peerToPeer.PtPHandler();
    }

}
