package com.avalon.lbm.portlets.priorauth.controller;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import com.avalon.lbm.portlets.priorauth.model.ProcedureInformationFO;
import com.avalon.lbm.portlets.priorauth.util.PriorAuthUtil;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.PortalUtil;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletRequest;
import javax.portlet.PortletSession;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.ui.Model;

public class ProcedureInformationTest {

    @Test
    public void testHandleRenderRequest() {
		ProcedureInformation procedureInformation = new ProcedureInformation();
		ProcedureInformationFO procedureInformationFO = new ProcedureInformationFO();
		RenderRequest request = Mockito.mock(RenderRequest.class);
		RenderResponse response = Mockito.mock(RenderResponse.class);
		Model model = Mockito.mock(Model.class);
	
		PortletSession portletSession = Mockito.mock(PortletSession.class);
		when(request.getPortletSession()).thenReturn(portletSession);
	
		when((String) portletSession.getAttribute("authorizationStatus", 1)).thenReturn("Approved");
		when((String) portletSession.getAttribute("memberDob", 1)).thenReturn("08/28/1983");
		when((String) portletSession.getAttribute("memberName", 1)).thenReturn("DRUE DEVOST");
		when((String) portletSession.getAttribute("memberId", 1)).thenReturn("X999999999");
		when((String) portletSession.getAttribute("authorizationAge", 1)).thenReturn("2016-02-08 10:25:49.0");
		when((String) portletSession.getAttribute("authorizationNumber", 1)).thenReturn("00100116020800001");
		when((Long) portletSession.getAttribute("authorizationKey", 1)).thenReturn(16174L);
		when((String) portletSession.getAttribute("membergroupId", 1)).thenReturn("002130030PSC");
		HttpServletRequest httprequest = Mockito.mock(HttpServletRequest.class);
		HttpServletRequest httpServletRequest2 = Mockito.mock(HttpServletRequest.class);
		HttpSession httpSession = Mockito.mock(HttpSession.class);
		PortalUtil portalUtil = new PortalUtil();
		Portal portal = Mockito.mock(Portal.class);
		portalUtil.setPortal(portal);
		PortletRequest portletRequest = Mockito.mock(PortletRequest.class);
		when(portalUtil.getHttpServletRequest(any(PortletRequest.class))).thenReturn(httprequest);
		when(portalUtil.getOriginalServletRequest(any(HttpServletRequest.class))).thenReturn(httpServletRequest2);
	
		when(PortalUtil.getHttpServletRequest(any(PortletRequest.class)).getSession()).thenReturn(httpSession);
		ThemeDisplay themeDisplay = Mockito.mock(ThemeDisplay.class);
		User user = Mockito.mock(User.class);
		when((ThemeDisplay) request.getAttribute("LIFERAY_SHARED_THEME_DISPLAY")).thenReturn(themeDisplay);
		when(themeDisplay.getUser()).thenReturn(user);
		when(themeDisplay.getUser().getEmailAddress()).thenReturn("test@liferay.com");
		procedureInformation.handleRenderRequest(request, response, model, procedureInformationFO);
	    }

    @Test
    public void testProcedureInformationDetails() {
		ActionRequest request = Mockito.mock(ActionRequest.class);
		ActionResponse response = Mockito.mock(ActionResponse.class);
		ProcedureInformationFO procedureInformationFO = new ProcedureInformationFO();
		ProcedureInformation procedureInformation = new ProcedureInformation();
		PortletSession session = Mockito.mock(PortletSession.class);
		when(request.getPortletSession()).thenReturn(session);
	
		PriorAuthUtil priorAuthUtil = Mockito.mock(PriorAuthUtil.class);
	
		ParamUtil paramUtil = Mockito.mock(ParamUtil.class);
		when(ParamUtil.getString((PortletRequest) request,(String) "submitButtonStatus")).thenReturn("saved");
	
		ThemeDisplay themeDisplay = Mockito.mock(ThemeDisplay.class);
		User user = Mockito.mock(User.class);
		when((ThemeDisplay) request.getAttribute("LIFERAY_SHARED_THEME_DISPLAY")).thenReturn(themeDisplay);
		when(themeDisplay.getUser()).thenReturn(user);
		when(themeDisplay.getUser().getEmailAddress()).thenReturn("radh@liferay.com");
	
		procedureInformationFO.setAuthRequestedDate("2016-02-09");
		procedureInformationFO.setAuthRequestedTime("12:00 AM");
		procedureInformationFO.setServiceBeginDate("2016-02-09");
		procedureInformationFO.setServiceEndDate("2016-02-24");
		procedureInformationFO.setAuthPriority("urgent");
	
		procedureInformationFO.setAuthStatusDesc("Approved");
		PortletRequest portletRequest = Mockito.mock(PortletRequest.class);
		int j = 1;
		when(ParamUtil.getString((PortletRequest) request,(String) ("diagnosisCodeAdditional" + j))).thenReturn("1");
		int i = 1;
		when(ParamUtil.getString((PortletRequest) request, (String) ("procedureUnits" + i))).thenReturn("13245");
	
		// when(ParamUtil.getString((PortletRequest)request,
		// (String)("procedureUnits" + i))).thenReturn("12345");
		when(ParamUtil.getString((PortletRequest) request, (String) ("procedureFromCode" + i))).thenReturn("123");
		when(ParamUtil.getString((PortletRequest) request, (String) ("procedureToCode" + i))).thenReturn("234");
	
		when(ParamUtil.getString((PortletRequest) request, (String) ("procedureDecision" + i))).thenReturn("A");
		when(ParamUtil.getString((PortletRequest) request, (String) ("procedureDecisionReason" + i))).thenReturn("A1");
		when((String) session.getAttribute("authorizationNumber", 1)).thenReturn("00100116020800001");
		when(ParamUtil.getString(request, "authPatientRelation")).thenReturn("mother");
		when(ParamUtil.getString((PortletRequest) request, (String) "authSubmissionStatus")).thenReturn("sent");
	
		when((String) session.getAttribute("memberId", 1)).thenReturn("X999999999");
	
		when((String) session.getAttribute("memberNumber", 1)).thenReturn("13");
		when((String) session.getAttribute("healthPlan", 1)).thenReturn("40");
		when((String) session.getAttribute("mpi", 1)).thenReturn("13");
	
		procedureInformationFO.setDiagnosisCodePrimary("diagnosis1");
		procedureInformationFO.setAuthInboundChannel("fax");
		when(ParamUtil.getString(request, "authPatientRelation")).thenReturn("2");
	
		when(ParamUtil.getString((PortletRequest) request,(String) "authSubmissionStatus")).thenReturn("Saved");
		when((Long) session.getAttribute("authorizationKey", 1)).thenReturn(16174L);
	
		// (String)session.getAttribute("authDecisionDate",
		// PortletSession.APPLICATION_SCOPE)
	
		procedureInformation.procedureInformationDetails(request, response, procedureInformationFO);
    }

    @Test
    public void testProcedureSubmit() {
		ProcedureInformation procedureInformation = new ProcedureInformation();
		ActionRequest request = Mockito.mock(ActionRequest.class);
		ActionResponse actionResponse = Mockito.mock(ActionResponse.class);
		ProcedureInformationFO procedureInformationFO = new ProcedureInformationFO();
	
		PortletSession session = Mockito.mock(PortletSession.class);
		when(request.getPortletSession()).thenReturn(session);
	
		ParamUtil paramUtil = Mockito.mock(ParamUtil.class);
		PortletRequest PortletRequest = Mockito.mock(PortletRequest.class);
	
		when(ParamUtil.getString((PortletRequest) request, (String) "submitButtonStatus")).thenReturn("Submitted");
	
		ThemeDisplay themeDisplay = Mockito.mock(ThemeDisplay.class);
		User user = Mockito.mock(User.class);
		when((ThemeDisplay) request.getAttribute("LIFERAY_SHARED_THEME_DISPLAY")).thenReturn(themeDisplay);
		when(themeDisplay.getUser()).thenReturn(user);
		when(themeDisplay.getUser().getEmailAddress()).thenReturn("submittest@liferay.com");
	
		procedureInformationFO.setAuthRequestedDate("2016-02-09");
		procedureInformationFO.setAuthRequestedTime("12:00 AM");
		procedureInformationFO.setServiceBeginDate("2016-02-09");
		procedureInformationFO.setServiceEndDate("2016-02-24");
		procedureInformationFO.setDiagnosisCodePrimary("1");
		procedureInformationFO.setAuthInboundChannel("phone");
		procedureInformationFO.setAuthPriority("urgent");
		procedureInformationFO.setAuthStatusDesc("Approved");
	
		when((String) session.getAttribute("memberId", 1)).thenReturn("X9999999999");
		when((String) session.getAttribute("memberNumber", 1)).thenReturn("13");
		when((String) session.getAttribute("healthPlan", 1)).thenReturn("40");
		when((String) session.getAttribute("mpi", 1)).thenReturn("123");
	
		int j = 1;
		int i = 1;
		when(ParamUtil.getString((PortletRequest) request, (String) ("diagnosisCodeAdditional" + j))).thenReturn("2");
		when(ParamUtil.getString((PortletRequest) request, (String) ("procedureUnits" + i))).thenReturn("67890");
		when(ParamUtil.getString((PortletRequest) request, (String) ("procedureFromCode" + i))).thenReturn("2344");
		when(ParamUtil.getString((PortletRequest) request, (String) ("procedureToCode" + i))).thenReturn("234");
		when(ParamUtil.getString((PortletRequest) request, (String) ("procedureDecision" + i))).thenReturn("A");
		when(ParamUtil.getString((PortletRequest) request, (String) ("procedureDecisionReason" + i))).thenReturn("A1");
	
		when((String) session.getAttribute("authorizationNumber", 1)).thenReturn("00100116020800001");
		when(ParamUtil.getString((PortletRequest) request, (String) "authPatientRelation")).thenReturn("2");
		when(ParamUtil.getString((PortletRequest) request, (String) "authSubmissionStatus")).thenReturn("Submitted");
		when((String) session.getAttribute("originalUserId", PortletSession.APPLICATION_SCOPE)).thenReturn("originakl@liferay.com");
		when((Long) session.getAttribute("authorizationKey", PortletSession.APPLICATION_SCOPE)).thenReturn(16174L);
	
		procedureInformation.procedureSubmit(request, actionResponse, procedureInformationFO);
	
    }

    @Test
    public void testProcedureVoid() {
		ProcedureInformationFO procedureInformationFO = new ProcedureInformationFO();
		ProcedureInformation procedureInformation = new ProcedureInformation();
	
		ActionRequest request = Mockito.mock(ActionRequest.class);
		ActionResponse actionResponse = Mockito.mock(ActionResponse.class);
		PriorAuthUtil priorAuthUtil = Mockito.mock(PriorAuthUtil.class);
		PortletSession session = Mockito.mock(PortletSession.class);
		when(request.getPortletSession()).thenReturn(session);
	
		when(ParamUtil.getString((PortletRequest) request, (String) "submitButtonStatus")).thenReturn("10");//
		when((String) session.getAttribute("subMissionStatus", 1)).thenReturn("sent");//
	
		procedureInformationFO.setAuthRequestedDate("2016-02-09");
		procedureInformationFO.setAuthRequestedTime("12:00 AM");
		procedureInformationFO.setServiceBeginDate("2016-02-09");
		procedureInformationFO.setServiceEndDate("2016-02-24");
		procedureInformationFO.setDiagnosisCodePrimary("1");
	
		procedureInformationFO.setAuthInboundChannel("phone");
		procedureInformationFO.setAuthDecisionDate("02/10/2016 16:27:39");//
		procedureInformationFO.setAuthCreatedDate("02/10/2016 16:27:39");//
	
		procedureInformationFO.setAuthStatusDesc("Approved");
	
		when((String) session.getAttribute("memberId", 1)).thenReturn("X9999999999");
		when((String) session.getAttribute("memberNumber", 1)).thenReturn("13");
		when((String) session.getAttribute("healthPlan", 1)).thenReturn("40");
		when((String) session.getAttribute("mpi", 1)).thenReturn("123");
	
		int j = 1;
		int i = 1;
		when(ParamUtil.getString((PortletRequest) request, (String) ("diagnosisCodeAdditional" + j))).thenReturn("2");
		when(ParamUtil.getString((PortletRequest) request, (String) ("procedureUnits" + i))).thenReturn("67890");
		when(ParamUtil.getString((PortletRequest) request, (String) ("procedureFromCode" + i))).thenReturn("2344");
		when(ParamUtil.getString((PortletRequest) request, (String) ("procedureToCode" + i))).thenReturn("234");
		when(ParamUtil.getString((PortletRequest) request, (String) ("procedureDecision" + i))).thenReturn("A");
		when(ParamUtil.getString((PortletRequest) request, (String) ("procedureDecisionReason" + i))).thenReturn("A1");
	
		when((String) session.getAttribute("authorizationNumber", 1)).thenReturn("00100116020800001");
	
		when(ParamUtil.getString(request, "authPatientRelation")).thenReturn("2");
		when(ParamUtil.getString((PortletRequest) request, (String) "authSubmissionStatus")).thenReturn("Submitted");
	
		when((Long) session.getAttribute("authorizationKey", 1)).thenReturn(16174L);
		when(ParamUtil.getString((PortletRequest) request, (String) "authCreatedDate")).thenReturn("02/10/2016 16:27:39");// error
		when((String) session.getAttribute("appCreateDateTime", 1)).thenReturn("02/10/2016 16:27:39");//
	
		ThemeDisplay themeDisplay = Mockito.mock(ThemeDisplay.class);
		User user = Mockito.mock(User.class);
		when((ThemeDisplay) request.getAttribute("LIFERAY_SHARED_THEME_DISPLAY")).thenReturn(themeDisplay);
		when(themeDisplay.getUser()).thenReturn(user);
		when(themeDisplay.getUser().getEmailAddress()).thenReturn("test000@liferay.com");
	
		when((String) session.getAttribute("originalUserId", PortletSession.APPLICATION_SCOPE)).thenReturn("original@liferay.com");
		procedureInformation.procedureSubmit(request, actionResponse, procedureInformationFO);

    }

}
