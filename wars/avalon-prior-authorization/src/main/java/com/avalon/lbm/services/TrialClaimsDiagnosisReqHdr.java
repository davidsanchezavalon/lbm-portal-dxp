
package com.avalon.lbm.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TrialClaimsDiagnosisReqHdr complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TrialClaimsDiagnosisReqHdr">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="icdDiagnosisCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="icdFormattedDiagnosisCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="icdHipaaSubmissionIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="icdShortDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="icdLongDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="icdRevisionNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="serviceDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TrialClaimsDiagnosisReqHdr", propOrder = {
    "icdDiagnosisCode",
    "icdFormattedDiagnosisCode",
    "icdHipaaSubmissionIndicator",
    "icdShortDescription",
    "icdLongDescription",
    "icdRevisionNumber",
    "serviceDate"
})
public class TrialClaimsDiagnosisReqHdr {

    protected String icdDiagnosisCode;
    protected String icdFormattedDiagnosisCode;
    protected boolean icdHipaaSubmissionIndicator;
    protected String icdShortDescription;
    protected String icdLongDescription;
    protected String icdRevisionNumber;
    protected String serviceDate;

    /**
     * Gets the value of the icdDiagnosisCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIcdDiagnosisCode() {
        return icdDiagnosisCode;
    }

    /**
     * Sets the value of the icdDiagnosisCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIcdDiagnosisCode(String value) {
        this.icdDiagnosisCode = value;
    }

    /**
     * Gets the value of the icdFormattedDiagnosisCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIcdFormattedDiagnosisCode() {
        return icdFormattedDiagnosisCode;
    }

    /**
     * Sets the value of the icdFormattedDiagnosisCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIcdFormattedDiagnosisCode(String value) {
        this.icdFormattedDiagnosisCode = value;
    }

    /**
     * Gets the value of the icdHipaaSubmissionIndicator property.
     * 
     */
    public boolean isIcdHipaaSubmissionIndicator() {
        return icdHipaaSubmissionIndicator;
    }

    /**
     * Sets the value of the icdHipaaSubmissionIndicator property.
     * 
     */
    public void setIcdHipaaSubmissionIndicator(boolean value) {
        this.icdHipaaSubmissionIndicator = value;
    }

    /**
     * Gets the value of the icdShortDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIcdShortDescription() {
        return icdShortDescription;
    }

    /**
     * Sets the value of the icdShortDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIcdShortDescription(String value) {
        this.icdShortDescription = value;
    }

    /**
     * Gets the value of the icdLongDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIcdLongDescription() {
        return icdLongDescription;
    }

    /**
     * Sets the value of the icdLongDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIcdLongDescription(String value) {
        this.icdLongDescription = value;
    }

    /**
     * Gets the value of the icdRevisionNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIcdRevisionNumber() {
        return icdRevisionNumber;
    }

    /**
     * Sets the value of the icdRevisionNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIcdRevisionNumber(String value) {
        this.icdRevisionNumber = value;
    }

    /**
     * Gets the value of the serviceDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceDate() {
        return serviceDate;
    }

    /**
     * Sets the value of the serviceDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceDate(String value) {
        this.serviceDate = value;
    }

}
