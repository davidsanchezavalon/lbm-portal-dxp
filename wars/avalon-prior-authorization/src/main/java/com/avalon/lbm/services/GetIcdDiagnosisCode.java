
package com.avalon.lbm.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getIcdDiagnosisCode complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getIcdDiagnosisCode">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="getIcdDiagnosisCodeRequest" type="{http://services.lbm.avalon.com/}TrialClaimsDiagnosisReqHdr" minOccurs="0" form="qualified"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getIcdDiagnosisCode", propOrder = {
    "getIcdDiagnosisCodeRequest"
})
public class GetIcdDiagnosisCode {

    @XmlElement(namespace = "http://services.lbm.avalon.com/")
    protected TrialClaimsDiagnosisReqHdr getIcdDiagnosisCodeRequest;

    /**
     * Gets the value of the getIcdDiagnosisCodeRequest property.
     * 
     * @return
     *     possible object is
     *     {@link TrialClaimsDiagnosisReqHdr }
     *     
     */
    public TrialClaimsDiagnosisReqHdr getGetIcdDiagnosisCodeRequest() {
        return getIcdDiagnosisCodeRequest;
    }

    /**
     * Sets the value of the getIcdDiagnosisCodeRequest property.
     * 
     * @param value
     *     allowed object is
     *     {@link TrialClaimsDiagnosisReqHdr }
     *     
     */
    public void setGetIcdDiagnosisCodeRequest(TrialClaimsDiagnosisReqHdr value) {
        this.getIcdDiagnosisCodeRequest = value;
    }

}
