package com.avalon.lbm.portlets.priorauth.model;

import java.io.Serializable;
import java.util.List;

public class PhysicianReviewFO implements Serializable {

    /**
     * To store the PhysicianReview fields
     */
    private static final long serialVersionUID = 1L;
	private String dateTimeSent;
	private String requestedBy;
	private String dateTimeReturned;
	private String decision;
	private String physicianName;
	private String reason;
	private String physReviewRequestedDate;
	private String physReviewReturnedDate;
	private String physReviewRequestedTime;
	private String physReviewReturnedTime;
	private List<String> physReviewRationaleRead;
	private String physReviewRationaleCreate;
	private String saveRevw;
	private String physReviewReviewerName;
	private String physReviewDecisionReason;
	private String physReviewDecisionReasonValue;

	private String physicianRationaleMessage;
	private String enterPhysicianRationale;

	public String getPhysReviewRequestedTime() {
		return physReviewRequestedTime;
	}
	public void setPhysReviewRequestedTime(String physReviewRequestedTime) {
		this.physReviewRequestedTime = physReviewRequestedTime;
	}
	public String getPhysReviewReturnedTime() {
		return physReviewReturnedTime;
	}
	public void setPhysReviewReturnedTime(String physReviewReturnedTime) {
		this.physReviewReturnedTime = physReviewReturnedTime;
	}
	private String physReviewDecision;
	private String physReviewDecisionValue;
	public String getPhysReviewDecisionValue() {
		return physReviewDecisionValue;
	}
	public void setPhysReviewDecisionValue(String physReviewDecisionValue) {
		this.physReviewDecisionValue = physReviewDecisionValue;
	}

	public String getPhysReviewDecisionReasonValue() {
		return physReviewDecisionReasonValue;
	}

	public void setPhysReviewDecisionReasonValue(
			String physReviewDecisionReasonValue) {
		this.physReviewDecisionReasonValue = physReviewDecisionReasonValue;
	}

	public String getSaveRevw() {
		return saveRevw;
	}

	public void setSaveRevw(String saveRevw) {
		this.saveRevw = saveRevw;
	}

	public String getPhysReviewRationaleCreate() {
		return physReviewRationaleCreate;
	}
	public void setPhysReviewRationaleCreate(String physReviewRationaleCreate) {
		this.physReviewRationaleCreate = physReviewRationaleCreate;
	}
	public List<String> getPhysReviewRationaleRead() {
		return physReviewRationaleRead;
	}
	public void setPhysReviewRationaleRead(List<String> physEnNotes) {
		this.physReviewRationaleRead = physEnNotes;
	}
	public String getPhysReviewDecisionReason() {
		return physReviewDecisionReason;
	}
	public void setPhysReviewDecisionReason(String physReviewDecisionReason) {
		this.physReviewDecisionReason = physReviewDecisionReason;
	}
	public String getPhysReviewReviewerName() {
		return physReviewReviewerName;
	}
	public void setPhysReviewReviewerName(String physReviewReviewerName) {
		this.physReviewReviewerName = physReviewReviewerName;
	}
	public String getPhysReviewDecision() {
		return physReviewDecision;
	}
	public void setPhysReviewDecision(String physReviewDecision) {
		this.physReviewDecision = physReviewDecision;
	}
	public String getPhysReviewReturnedDate() {
		return physReviewReturnedDate;
	}
	public void setPhysReviewReturnedDate(String physReviewReturnedDate) {
		this.physReviewReturnedDate = physReviewReturnedDate;
	}
	public String getPhysReviewRequestedBy() {
		return physReviewRequestedBy;
	}
	public void setPhysReviewRequestedBy(String physReviewRequestedBy) {
		this.physReviewRequestedBy = physReviewRequestedBy;
	}
	private String physReviewRequestedBy;
	public String getPhysReviewRequestedDate() {
		return physReviewRequestedDate;
	}
	public void setPhysReviewRequestedDate(String physReviewRequestedDate) {
		this.physReviewRequestedDate = physReviewRequestedDate;
	}

	public String getDateTimeSent() {
		return dateTimeSent;
	}
	public void setDateTimeSent(String dateTimeSent) {
		this.dateTimeSent = dateTimeSent;
	}
	public String getRequestedBy() {
		return requestedBy;
	}
	public void setRequestedBy(String requestedBy) {
		this.requestedBy = requestedBy;
	}
	public String getDateTimeReturned() {
		return dateTimeReturned;
	}
	public void setDateTimeReturned(String dateTimeReturned) {
		this.dateTimeReturned = dateTimeReturned;
	}
	public String getDecision() {
		return decision;
	}
	public void setDecision(String decision) {
		this.decision = decision;
	}
	public String getPhysicianName() {
		return physicianName;
	}
	public void setPhysicianName(String physicianName) {
		this.physicianName = physicianName;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public String getPhysicianRationaleMessage() {
		return physicianRationaleMessage;
	}
	public void setPhysicianRationaleMessage(String physicianRationaleMessage) {
		this.physicianRationaleMessage = physicianRationaleMessage;
	}
	public String getEnterPhysicianRationale() {
		return enterPhysicianRationale;
	}
	public void setEnterPhysicianRationale(String enterPhysicianRationale) {
		this.enterPhysicianRationale = enterPhysicianRationale;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("PhysicianReviewFO [dateTimeSent=");
		builder.append(dateTimeSent);
		builder.append(", requestedBy=");
		builder.append(requestedBy);
		builder.append(", dateTimeReturned=");
		builder.append(dateTimeReturned);
		builder.append(", decision=");
		builder.append(decision);
		builder.append(", physicianName=");
		builder.append(physicianName);
		builder.append(", reason=");
		builder.append(reason);
		builder.append(", physReviewRequestedDate=");
		builder.append(physReviewRequestedDate);
		builder.append(", physReviewReturnedDate=");
		builder.append(physReviewReturnedDate);
		builder.append(", physReviewDecision=");
		builder.append(physReviewDecision);
		builder.append(", physReviewDecisionValue=");
		builder.append(physReviewDecisionValue);
		builder.append(", physReviewReviewerName=");
		builder.append(physReviewReviewerName);
		builder.append(", physReviewDecisionReason=");
		builder.append(physReviewDecisionReason);
		builder.append(", physReviewDecisionReasonValue=");
		builder.append(physReviewDecisionReasonValue);
		builder.append(", physReviewRationaleRead=");
		builder.append(physReviewRationaleRead);
		builder.append(", physReviewRationaleCreate=");
		builder.append(physReviewRationaleCreate);
		builder.append(", saveRevw=");
		builder.append(saveRevw);
		builder.append(", physReviewRequestedBy=");
		builder.append(physReviewRequestedBy);
		builder.append(", physicianRationaleMessage=");
		builder.append(physicianRationaleMessage);
		builder.append(", enterPhysicianRationale=");
		builder.append(enterPhysicianRationale);
		builder.append("]");
		return builder.toString();
	}
	
	
	

}
