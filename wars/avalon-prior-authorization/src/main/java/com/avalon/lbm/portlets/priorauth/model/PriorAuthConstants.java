package com.avalon.lbm.portlets.priorauth.model;

/**
 * Description
 *		This file contain the constants for esb service request.
 *
 * CHANGE History
 * 		Version 1.0
 * 			Initial version
 * 		Version 1.1
 * 			Changed the aging start date to the request date.  If it is not set use the creation date.
 * 			Changed the aging end date to the date the letter was sent.  If it is not set use the current date.
 * 		Version 1.2
 *			Added new fields: Withdrawn Reason, Being Worked By, Due Date, and Due Time.
 *  	Version 1.3						03/07/2017
 *  		Added a constant for the properties file.
  *		Version 1.04					08/09/2018
  *			Added a new authorization status.
 *  
 */

public interface PriorAuthConstants {
	public static final String PROPERTIES_FILE ="lbm-common.properties";

	public static final String NEW = "00";
	public static final String IN_PROCESS = "10";
	public static final String SUBMITTED = "40";
	public static final String PA_REQUEST_EXTRACTED = "50";
	public static final String AVALONEMPLOYEE = "AvalonEmployee";
	public static final String EVERY_ONE = "Everyone";
    public static final String AVALONADMIN = "AvalonAdmin";
    public static final String AVALONPROVIDER = "AvalonProvider";
    public static final String BCBSSPROVIDER = "BCBSSCProvider";
    public static final String HEALTHPLANEMPLOYEE = "HealthPlanEmployee";
    public static final String HEALTHPLANPROVIDER = "HealthPlanProvider";
    public static final String PORTALAPPROVER = "PortalApprover";
    public static final String PORTALCREATOR = "PortalCreator";
    public static final String AVALON_EMP_HOME  = "avalonemphome";    
    public static final String AVALON_HOME   ="/web/guest/home"; 
    public static final String AVALON_PROVIDER_HOME = "avalonproviderhome";
    public static final String HP_PROVIDER_HOME = "hpproviderhome";
    public static final String HP_EMP_HOME = "hpemphome";
    public static final String PORTAL_CREATOR_HOME = "portalcreatorhome";
    public static final String PORTAL_APPROVER_HOME = "portalapproverhome";
    public static final String INTAKEREVIEW = "Intake Review";
    public static final String INTAKETYPECODE = "INTAKE";
    public static final String NOTIFICATION = "Notification";
    public static final String NURSEADD = "Nurse Add";
    public static final String NURSEREVIEW = "Nurse Review";
    public static final String NURSETYPECODE = "NURSE";
    public static final String NURSEADDTYPECODE = "NURSE-ADDL";
    public static final String PEERREVIEW = "Peer Review";
    public static final String PEERTYPECODE = "PEER";
    public static final String PHYSICIANREVIEW = "Physician Review";
    public static final String PHYSICIANTYPECODE = "PHYSICIAN";
    public static final String PROCEDURE = "Procedure";
    public static final String PROCEDURESUBMISSION = "ProcedureSubmission";
    public static final String Provider = "Provider";
    public static final String RECONSIDERATION = "Reconsideration";
    public static final String RECONSIDERATIONTYPECODE = "RECONSIDERATION";

    public static final String PHONE_STR="Phone";
    public static final String FAX_STR="Fax";
    public static final String WEB_STR="Web";
    
    public static final String ROUTINE_STR="Routine";
    public static final String URGENT_STR="Urgent";

    public static final String IN_PROCESS_INTAKE_REVIEW_DESC		= "In Process - Intake Review";
    public static final String IN_PROCESS_READY_FOR_COMPL_DESC		= "In Process - Intake Ready for Completion";
    public static final String IN_PROCESS_NURSE_REVIEW_DESC			= "In Process - Nurse Review";
    public static final String IN_PROCESS_NURSE_WAITING_DESC		= "In Process - Nurse Review Waiting on Clinical";
    public static final String IN_PROCESS_NURSE_READY_DESC			= "In Process - Nurse Review Ready for Denial Prep";
    public static final String IN_PROCESS_PHYSICIAN_REVIEW_DESC		= "In Process - Physician Review";
    public static final String COMPLETED_APPROVED_DESC				= "Completed - Approved";
    public static final String COMPLETED_PARTIALLY_APPROVED_DESC	= "Completed - Partially Approved";
    public static final String COMPLETED_DENIED_DESC				= "Completed - Denied";
    public static final String COMPLETED_ADMINISTRATIVE_DENIAL_DESC	= "Completed - Administrative Denial";
    public static final String COMPLETED_CLINICAL_DENIAL_DESC		= "Completed - Clinical Denial";
    public static final String COMPLETED_WITHDRAWN_DESC				= "Completed - Withdrawn";

    public static final String IN_PROCESS_INTAKE_REVIEW_CODE		= "IP - Intk Rev";
    public static final String IN_PROCESS_READY_FOR_COMPL_CODE		= "IP - Intk Rdy for Cmpl";
    public static final String IN_PROCESS_NURSE_REVIEW_CODE			= "IP - Nrse Rev";
    public static final String IN_PROCESS_NURSE_WAITING_CODE		= "IP - Nrse Rev Wait on Cln";
    public static final String IN_PROCESS_NURSE_READY_CODE			= "IP - Nrse Rev Denial Prep";
    public static final String IN_PROCESS_PHYSICIAN_REVIEW_CODE		= "IP - Phys Rev";
    public static final String COMPLETED_APPROVED_CODE				= "Cmpl - Appr";
    public static final String COMPLETED_PARTIALLY_APPROVED_CODE	= "Cmpl - Prtl Appr";
    public static final String COMPLETED_DENIED_CODE				= "Cmpl - Denial";
    public static final String COMPLETED_ADMINISTRATIVE_DENIAL_CODE	= "Cmpl - Adm Denial";
    public static final String COMPLETED_CLINICAL_DENIAL_CODE		= "Cmpl - Cl Denial";
    public static final String COMPLETED_WITHDRAWN_CODE				= "Cmpl - Withdrawn";

    public static final String DUPLICATE_DESC					= "Duplicate";
    public static final String PROVIDER_WITHDRAWN_DESC			= "Provider Withdrawn";
    public static final String AUTH_ENTERED_IN_ERROR_DESC		= "Auth Entered In Error";
    public static final String NON_PARTICIPATING_MEMBER_DESC	= "Non-Participating Member";

    public static final String DUPLICATE_CODE					= "Dupe";
    public static final String PROVIDER_WITHDRAWN_CODE			= "Pvd Withdrawn";
    public static final String AUTH_ENTERED_IN_ERROR_CODE		= "Enter Error";
    public static final String NON_PARTICIPATING_MEMBER_CODE	= "Non-Par Mbr";
    //TODO: Remove below code
    public static final String ENABLED_MOCK_SERVICE = "enabled.mock.service";
}
