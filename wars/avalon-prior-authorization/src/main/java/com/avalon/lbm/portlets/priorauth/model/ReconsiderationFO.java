package com.avalon.lbm.portlets.priorauth.model;

import java.io.Serializable;
import java.util.List;

public class ReconsiderationFO implements Serializable {
	
	/**
	 * To store the reconsideration fields
	 */
	private static final long serialVersionUID = 6774226444253955861L;
	private String reconDecisionDate;
	private String reconDecision;
	private String reconDecisionReviewer;
	private List<String>  reconNotesRead;
	private String reconNotesCreate;
	private String saveRevw;
	
	
	public String getSaveRevw() {
		return saveRevw;
	}
	public void setSaveRevw(String saveRevw) {
		this.saveRevw = saveRevw;
	}
	public String getReconDecisionDate() {
		return reconDecisionDate;
	}
	public void setReconDecisionDate(String reconDecisionDate) {
		this.reconDecisionDate = reconDecisionDate;
	}
	public String getReconDecision() {
		return reconDecision;
	}
	public void setReconDecision(String reconDecision) {
		this.reconDecision = reconDecision;
	}
	public String getReconDecisionReviewer() {
		return reconDecisionReviewer;
	}
	public void setReconDecisionReviewer(String reconDecisionReviewer) {
		this.reconDecisionReviewer = reconDecisionReviewer;
	}
	public List<String>  getReconNotesRead() {
		return reconNotesRead;
	}
	public void setReconNotesRead(List<String> enteredNotes) {
		this.reconNotesRead = enteredNotes;
	}
	public String getReconNotesCreate() {
		return reconNotesCreate;
	}
	public void setReconNotesCreate(String reconNotesCreate) {
		this.reconNotesCreate = reconNotesCreate;
	}
	
	
	
	
	
	
	

}
