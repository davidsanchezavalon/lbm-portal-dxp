package com.avalon.lbm.portlets.priorauth.model;

import java.io.Serializable;
import java.util.List;

public class PeerToPeerFO implements Serializable {

	/**
	 * To store the PeerToPeer fields
	 */
	private static final long serialVersionUID = 2450135409484738528L;
	private String ptpDecisionRequestedDate;
	private String ptpDecisionRequestedBy;
	private String ptpDecisionReturnedDate;
	private String ptpDecisionReturnedTime;
	private String ptpDecision;
	private String ptpPhyName;
	private List<String> ptpNotesRead;
	private String ptpNotesCreate;
	private String saveRevw;

	private String ptpDecisionRequestedTime;

	/**
	 * @return the ptpDecisionRequestedTime
	 */
	public String getPtpDecisionRequestedTime() {
		return ptpDecisionRequestedTime;
	}

	/**
	 * @param ptpDecisionRequestedTime
	 *            the ptpDecisionRequestedTime to set
	 */
	public void setPtpDecisionRequestedTime(String ptpDecisionRequestedTime) {
		this.ptpDecisionRequestedTime = ptpDecisionRequestedTime;
	}

	/**
	 * @return the ptpDecisionReturnedTime
	 */
	public String getPtpDecisionReturnedTime() {
		return ptpDecisionReturnedTime;
	}

	/**
	 * @param ptpDecisionReturnedTime
	 *            the ptpDecisionReturnedTime to set
	 */
	public void setPtpDecisionReturnedTime(String ptpDecisionReturnedTime) {
		this.ptpDecisionReturnedTime = ptpDecisionReturnedTime;
	}

	public String getSaveRevw() {
		return saveRevw;
	}
	public void setSaveRevw(String saveRevw) {
		this.saveRevw = saveRevw;
	}
	public String getPtpPhyName() {
		return ptpPhyName;
	}
	public void setPtpPhyName(String ptpPhyName) {
		this.ptpPhyName = ptpPhyName;
	}
	
	public String getPtpDecisionRequestedDate() {
		return ptpDecisionRequestedDate;
	}
	public void setPtpDecisionRequestedDate(String ptpDecisionRequestedDate) {
		this.ptpDecisionRequestedDate = ptpDecisionRequestedDate;
	}
	public String getPtpDecisionRequestedBy() {
		return ptpDecisionRequestedBy;
	}
	public void setPtpDecisionRequestedBy(String ptpDecisionRequestedBy) {
		this.ptpDecisionRequestedBy = ptpDecisionRequestedBy;
	}
	public String getPtpDecisionReturnedDate() {
		return ptpDecisionReturnedDate;
	}
	public void setPtpDecisionReturnedDate(String ptpDecisionReturnedDate) {
		this.ptpDecisionReturnedDate = ptpDecisionReturnedDate;
	}
	public String getPtpDecision() {
		return ptpDecision;
	}
	public void setPtpDecision(String ptpDecision) {
		this.ptpDecision = ptpDecision;
	}
	public List<String> getPtpNotesRead() {
		return ptpNotesRead;
	}
	public void setPtpNotesRead(List<String> ptpNotesRead) {
		this.ptpNotesRead = ptpNotesRead;
	}
	public String getPtpNotesCreate() {
		return ptpNotesCreate;
	}
	public void setPtpNotesCreate(String ptpNotesCreate) {
		this.ptpNotesCreate = ptpNotesCreate;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("PeerToPeerFO [ptpDecisionRequestedDate=");
		builder.append(ptpDecisionRequestedDate);
		builder.append(", ptpDecisionRequestedBy=");
		builder.append(ptpDecisionRequestedBy);
		builder.append(", ptpDecisionReturnedDate=");
		builder.append(ptpDecisionReturnedDate);
		builder.append(", ptpDecision=");
		builder.append(ptpDecision);
		builder.append(", ptpPhyName=");
		builder.append(ptpPhyName);
		builder.append(", ptpNotesRead=");
		builder.append(ptpNotesRead);
		builder.append(", ptpNotesCreate=");
		builder.append(ptpNotesCreate);
		builder.append(", saveRevw=");
		builder.append(saveRevw);
		builder.append("]");
		return builder.toString();
	}
	
	
	
	
}
