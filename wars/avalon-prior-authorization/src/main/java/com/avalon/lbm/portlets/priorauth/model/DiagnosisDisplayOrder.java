package com.avalon.lbm.portlets.priorauth.model;

import java.util.Comparator;

public class DiagnosisDisplayOrder
implements Comparator<DiagnosisDisplayOrder> {
    
    /**
     * To display the DiagnosisDisplayOrder
     */
    private int sequenceNumber;
    private String diagnosisCode;

    public int getSequenceNumber() {
        return this.sequenceNumber;
    }

    public void setSequenceNumber(int sequenceNumber) {
        this.sequenceNumber = sequenceNumber;
    }

    public String getDiagnosisCode() {
        return this.diagnosisCode;
    }

    public void setDiagnosisCode(String diagnosisCode) {
        this.diagnosisCode = diagnosisCode;
    }

    @Override
    public int compare(DiagnosisDisplayOrder diagnosisOne, DiagnosisDisplayOrder diagnosisTwo) {
        int sequenceTwoValue;
        int value = 0;
        int sequenceOneValue = diagnosisOne.getSequenceNumber();
        if (sequenceOneValue == (sequenceTwoValue = diagnosisTwo.getSequenceNumber())) {
            value = 0;
        }
        if (sequenceOneValue > sequenceTwoValue) {
            value = 1;
        }
        if (sequenceOneValue < sequenceTwoValue) {
            value = -1;
        }
        return value;
    }
}