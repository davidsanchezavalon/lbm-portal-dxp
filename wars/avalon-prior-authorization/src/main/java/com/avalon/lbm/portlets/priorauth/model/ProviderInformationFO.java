package com.avalon.lbm.portlets.priorauth.model;

import java.io.Serializable;

/**
 * Description
 *		This file contain the getter and setter methods for the PA Procedure Information Page.
 *
 * @author David Sanchez
 *
 * CHANGE History
 * 		Version 1.0
 * 			Initial version
 *  
 */

public class ProviderInformationFO implements Serializable {
	/**
	 * To store the provider information fields
	 */
	private static final long serialVersionUID = 1L;
	public String orderingFirstName;
	public String orderingLastName;
	public String orderingNpi;
	public String orderingPhoneNumber;
	public String orderingTinEin;
	public String orderingFaxNumber;
	public String orderingAddressLine1;
	public String orderingAddressLine2;
	public String orderingCity;
	public String orderingState;
	public String orderingZip;
	public String renderingLabName;
	public String renderingFirstName;
	public String renderingLastName;
	public String renderingNpi;
	public String renderingPhoneNumber;
	public String renderingTinEin;
	public String renderingFaxNumber;
	public String renderingAddressLine1;
	public String renderingAddressLine2;
	public String renderingCity;
	public String renderingZip;
	public String renderingState;
	
	private String memberFirstName;
	private String memberMiddleName;
	private String memberLastName;
	private String memberSuffixName;
	private String memberDob;
	private String gender;
	private String businessSectorCode;
	private String businessSectorDescription;
	private String businessSegmentCode;
	private String businessSegmentDescription;

	public String getRenderingState() {
		return renderingState;
	}
	public void setRenderingState(String renderingState) {
		this.renderingState = renderingState;
	}
	
	public String getOrderingFirstName() {
		return orderingFirstName;
	}
	public void setOrderingFirstName(String orderingFirstName) {
		this.orderingFirstName = orderingFirstName;
	}
	
	public String getOrderingLastName() {
		return orderingLastName;
	}
	public void setOrderingLastName(String orderingLastName) {
		this.orderingLastName = orderingLastName;
	}
	
	public String getOrderingNpi() {
		return orderingNpi;
	}
	public void setOrderingNpi(String orderingNpi) {
		this.orderingNpi = orderingNpi;
	}
	
	public String getOrderingPhoneNumber() {
		return orderingPhoneNumber;
	}
	public void setOrderingPhoneNumber(String orderingPhoneNumber) {
		this.orderingPhoneNumber = orderingPhoneNumber;
	}
	
	public String getOrderingTinEin() {
		return orderingTinEin;
	}
	public void setOrderingTinEin(String orderingTinEin) {
		this.orderingTinEin = orderingTinEin;
	}
	
	public String getOrderingFaxNumber() {
		return orderingFaxNumber;
	}
	public void setOrderingFaxNumber(String orderingFaxNumber) {
		this.orderingFaxNumber = orderingFaxNumber;
	}
	
	public String getOrderingAddressLine1() {
		return orderingAddressLine1;
	}
	public void setOrderingAddressLine1(String orderingAddressLine1) {
		this.orderingAddressLine1 = orderingAddressLine1;
	}
	
	public String getOrderingAddressLine2() {
		return orderingAddressLine2;
	}
	public void setOrderingAddressLine2(String orderingAddressLine2) {
		this.orderingAddressLine2 = orderingAddressLine2;
	}
	
	public String getOrderingCity() {
		return orderingCity;
	}
	public void setOrderingCity(String orderingCity) {
		this.orderingCity = orderingCity;
	}
	
	public String getOrderingState() {
		return orderingState;
	}
	public void setOrderingState(String orderingState) {
		this.orderingState = orderingState;
	}
	
	public String getOrderingZip() {
		return orderingZip;
	}
	public void setOrderingZip(String orderingZip) {
		this.orderingZip = orderingZip;
	}
	
	public String getRenderingLabName() {
		return renderingLabName;
	}
	public void setRenderingLabName(String renderingLabName) {
		this.renderingLabName = renderingLabName;
	}
	
	public String getRenderingFirstName() {
		return renderingFirstName;
	}
	public void setRenderingFirstName(String renderingFirstName) {
		this.renderingFirstName = renderingFirstName;
	}
	
	public String getRenderingLastName() {
		return renderingLastName;
	}
	public void setRenderingLastName(String renderingLastName) {
		this.renderingLastName = renderingLastName;
	}
	
	public String getRenderingNpi() {
		return renderingNpi;
	}
	public void setRenderingNpi(String renderingNpi) {
		this.renderingNpi = renderingNpi;
	}
	
	public String getRenderingPhoneNumber() {
		return renderingPhoneNumber;
	}
	public void setRenderingPhoneNumber(String renderingPhoneNumber) {
		this.renderingPhoneNumber = renderingPhoneNumber;
	}
	
	public String getRenderingTinEin() {
		return renderingTinEin;
	}
	public void setRenderingTinEin(String renderingTinEin) {
		this.renderingTinEin = renderingTinEin;
	}
	
	public String getRenderingFaxNumber() {
		return renderingFaxNumber;
	}
	public void setRenderingFaxNumber(String renderingFaxNumber) {
		this.renderingFaxNumber = renderingFaxNumber;
	}
	
	public String getRenderingAddressLine1() {
		return renderingAddressLine1;
	}
	public void setRenderingAddressLine1(String renderingAddressLine1) {
		this.renderingAddressLine1 = renderingAddressLine1;
	}
	
	public String getRenderingAddressLine2() {
		return renderingAddressLine2;
	}
	public void setRenderingAddressLine2(String renderingAddressLine2) {
		this.renderingAddressLine2 = renderingAddressLine2;
	}
	
	public String getRenderingCity() {
		return renderingCity;
	}
	public void setRenderingCity(String renderingCity) {
		this.renderingCity = renderingCity;
	}
	
	public String getRenderingZip() {
		return renderingZip;
	}
	public void setRenderingZip(String renderingZip) {
		this.renderingZip = renderingZip;
	}

	public String getMemberFirstName() {
	    return memberFirstName;
	}
	public void setMemberFirstName(String memberFirstName) {
	    this.memberFirstName = memberFirstName;
	}
	
	public String getMemberMiddleName() {
	    return memberMiddleName;
	}
	public void setMemberMiddleName(String memberMiddleName) {
	    this.memberMiddleName = memberMiddleName;
	}
	
	public String getMemberLastName() {
	    return memberLastName;
	}
	public void setMemberLastName(String memberLastName) {
	    this.memberLastName = memberLastName;
	}
	
	public String getMemberSuffixName() {
	    return memberSuffixName;
	}
	public void setMemberSuffixName(String memberSuffixName) {
	    this.memberSuffixName = memberSuffixName;
	}
	
	public String getMemberDob() {
	    return memberDob;
	}
	public void setMemberDob(String memberDob) {
	    this.memberDob = memberDob;
	}
	
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	
	public String getBusinessSectorCode() {
		return businessSectorCode;
	}
	public void setBusinessSectorCode(String businessSectorCode) {
		this.businessSectorCode = businessSectorCode;
	}
	
	public String getBusinessSectorDescription() {
		return businessSectorDescription;
	}
	public void setBusinessSectorDescription(String businessSectorDescription) {
		this.businessSectorDescription = businessSectorDescription;
	}
	
	public String getBusinessSegmentCode() {
		return businessSegmentCode;
	}
	public void setBusinessSegmentCode(String businessSegmentCode) {
		this.businessSegmentCode = businessSegmentCode;
	}
	
	public String getBusinessSegmentDescription() {
		return businessSegmentDescription;
	}
	public void setBusinessSegmentDescription(String businessSegmentDescription) {
		this.businessSegmentDescription = businessSegmentDescription;
	}
	
}
