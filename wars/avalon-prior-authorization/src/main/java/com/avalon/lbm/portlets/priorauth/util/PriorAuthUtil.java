/**
 * Description
 *		This file contain the controller methods for the PA utility methods.
 *
 * CHANGE History
 * 		Version 1.0		
 * 			Initial version
 * 		Version 1.1		
 * 			Changed the setAuthorizationAge method to stop the aging when the date the letter was sent is set.
 * 		Version 1.2  
 *			Added methods to return the App Maintenance Datetime in the correct format.  
 *		Version 1.3					08/08/2018
 *			Added HP LOB and Subscriber Name to header.
 *  
 */

package com.avalon.lbm.portlets.priorauth.util;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.portlet.PortletRequest;
import javax.portlet.PortletSession;

import com.avalon.lbm.portlets.priorauth.model.CurrentAuthorizationDetailsFO;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.theme.ThemeDisplay;

public class PriorAuthUtil {

	private static final Log log = LogFactoryUtil.getLog(PriorAuthUtil.class.getName());

	private static PriorAuthUtil priorAuthUtil = null;

	public static String userStamp;
	
	// Define the prior authorization source tables
	public static String providerTab = "Provider";
	public static String procedureTab = "Procedure";
	public static String procedureSubmissionTab = "ProcedureSubmission";
	public static String additionalTab = "Nurse Add";
	public static String intakeReviewTab = "Intake Review";
	public static String nurseReviewTab = "Nurse Review";
	public static String physicianReviewTab = "Physician Review";
	public static String notificationTab = "Notification";
	public static String peerToPeerTab = "Peer Review";
	
	// Define the prior authorization note types
	public static String additionalNote = "NURSE-ADDL";
	public static String intakeReviewNote = "INTAKE";
	public static String nurseReviewNote = "NURSE";
	public static String physicianReviewNote = "PHYSICIAN";
	public static String peerToPeerNote = "PEER";

	static {
		if (priorAuthUtil == null) {
			priorAuthUtil = new PriorAuthUtil();
		}
	}

	private PriorAuthUtil() {
	}

	public static PriorAuthUtil getPriorAuthUtil() {
		return priorAuthUtil;
	}
	
	/**
	 * To display the user stamp in notes field
	 */

	public static String getUserStamp(PortletRequest request) {
		ThemeDisplay themeDisplay = (ThemeDisplay)request.getAttribute("LIFERAY_SHARED_THEME_DISPLAY");
		User user = themeDisplay.getUser();
		String userEmail = user.getEmailAddress();

		SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy , hh:mm a");
		Date date = new Date();
		String currentDate = dateFormat.format(date);
		userStamp = userEmail + "(" + currentDate + ")" + ":";
		log.info("UserStamp : " + userStamp);
		return userStamp;
	}
	
	/**
	 * To display the current username
	 */

	public static String getUserName(PortletRequest request) {
		ThemeDisplay themeDisplay = (ThemeDisplay)request.getAttribute("LIFERAY_SHARED_THEME_DISPLAY");
		User user = themeDisplay.getUser();
		String userName = user.getFullName();
		return userName;
	}

	/**
	 * To display the current user date and time
	 */

	public String getCurrentUserDateTime() {
		SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		Date date = new Date();
		String currentDate = dateFormat.format(date);
		return currentDate;
	}
	
	/**
	 * To display the current user date and time through procedure date
	 */

	public String getCurrentUserDateTime(String procedureDate) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		Date date = new Date();
		String currentDate = dateFormat.format(date);
		return currentDate;
	}
	
	/**
	 * To display the conversion date
	 */

	public String getConversationdate(String requestedDate) {
		String frmDateStr = null;
		try {
			SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
			Date date = formatter.parse(requestedDate);

			SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
			frmDateStr = dateFormat.format(date);
			log.info("Conversationdate : " + frmDateStr);
		}
		catch (ParseException e) {
			log.error("ParseException in getConversationdate method",e);
		}
		return frmDateStr;
	}
	
	/**
	 * To Display  date and time in Physician,ProcedureInformation,PeerToPeer pages
	 */

	public String getPhydateReq(String requestedDate) { 
		String input = requestedDate;
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm aa");
		SimpleDateFormat outputformat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
		Date date = null;
		String output = null;
		try {
			date = df.parse(input);
			output = outputformat.format(date);
		}
		catch (ParseException e) {
			log.error("ParseException in getPhydateReq method",e);
		}
		return output;
	}
	
	/**
	 * To Display Original Decision Date date and time in Procedure Information pages
	 */

	public String getAuthRequestedDate(String requestedDate) { 
		String input = requestedDate;
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm aa");
		SimpleDateFormat outputformat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
		Date date = null;
		String output = null;
		try {
			date = df.parse(input);
			output = outputformat.format(date);
		}
		catch (ParseException e) {
			log.error("ParseException in getAuthRequestedDate method",e);
		}
		return output;
	}
	
	/**
	 * To Display Original Decision  Date
	 */

	public String getOriginalDate() { 
		String frmDateStr = null;
		try {
			Date date = new Date();
			SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
			frmDateStr = dateFormat.format(date);
		}
		catch (Exception e) {
			log.error("Exception in getOriginalDate method",e);
		}
		return frmDateStr;
	}

	/**
	 * To convert the creation date/time
	 */
	public String getStandardDateTime(String requestedDate) { 
		String input = requestedDate;
		SimpleDateFormat inputformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
		SimpleDateFormat outputformat = new SimpleDateFormat("MM/dd/yyyy hh:mm aa");
		Date date = null;
		String output = null;
		try {
			date = inputformat.parse(input);
			output = outputformat.format(date);
		}
		catch (ParseException e) {
			log.error("ParseException in getStandardDateTime method",e);
		}
		return output;
	}


	/**
	 * To Display Requested and Returned date/time
	 */
	public String displayStandardDateTime(String requestedDate) { 
		String input = requestedDate;
		SimpleDateFormat inputformat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
		SimpleDateFormat outputformat = new SimpleDateFormat("MM/dd/yyyy hh:mm aa");
		Date date = null;
		String output = null;
		try {
			date = inputformat.parse(input);
			output = outputformat.format(date);
		}
		catch (ParseException e) {
			log.error("ParseException in displayStandardDateTime method",e);
		}
		return output;
	}

	/**
	 * To Display Original Decision Time
	 */
	public String getStandardDecisionDateTime(String originalDate) { 
		String input = originalDate;
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		SimpleDateFormat outputformat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
		Date date = null;
		String output = null;
		try {
			date = df.parse(input);
			output = outputformat.format(date);
		}
		catch (ParseException e) {
			log.error("ParseException in getStandardDecisionDateTime method",e);
		}
		return output;
	}

	/**
	 * To Display Original Decision Time
	 */
	public String getDecisionDateTimeSaveFormat(String originalDate, String browser) { 
		String input = originalDate;
		SimpleDateFormat inputformat = null;
		SimpleDateFormat outputformat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
		Date date = null;
		String output = null;
		
		try {
			if (browser.equals("IE") || browser.equals("Firefox") || browser.equals("unknown")) {
				inputformat = new SimpleDateFormat("MM/dd/yyyy hh:mm aa");
			} else {
				inputformat = new SimpleDateFormat("yyyy-MM-dd hh:mm aa");
			}
			date = inputformat.parse(input);
			output = outputformat.format(date);
		}
		catch (ParseException e) {
			log.error("ParseException in getDecisionDateTimeSaveFormat method",e);
		}
		return output;
	}

	/**
	 * To display the procedure create date and time
	 */

	public String getStandardProcCreateDateTime(String requestedDate) {
		String input = requestedDate;
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		SimpleDateFormat outputformat = new SimpleDateFormat("MM/dd/yyyy hh:mm aa");
		Date date = null;
		String output = null;
		try {
			date = df.parse(input);
			output = outputformat.format(date);
		}
		catch (ParseException e) {
			log.error("ParseException in getStandardProcCreateDateTime method",e);
		}
		return output;
	}

	
	/**
	 * To display the procedure create date and time for request object
	 */
	
	public String getStandardProcCreateDateTimeReq(String requestedDate) {
		String input = requestedDate;
		SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy hh:mm aa");
		SimpleDateFormat outputformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date = null;
		String output = null;
		try {
			date = df.parse(input);
			output = outputformat.format(date);
		}
		catch (ParseException e) {
			log.error("ParseException in getStandardProcCreateDateTimeReq method",e);
		}
		return output;
	}
	
	
	/**
	 * To display the procedure response date
	 */

	public String getProdateResDate(String requestedDate) {
		String frmDateStr = null;
		try {
			SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
			Date date = formatter.parse(requestedDate);

			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			frmDateStr = dateFormat.format(date);
			log.info("ProdateResDate : " + frmDateStr);
		}
		catch (ParseException e) {
			log.error("ParseException in getProdateResDate method",e);
		}
		return frmDateStr;
	}
	
	/**
	 * To display the physician response date
	 */

	public String getPhydateResDate(String requestedDate, String browser) {// To Display Date in PeerToPeer and Physician Review
		String toDateStr = null;
		SimpleDateFormat frmFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
		SimpleDateFormat toFormat = null;

		try {
			if (browser.equals("IE") || browser.equals("Firefox") || browser.equals("unknown")) {
				
				// Convert to the displayed format
				toFormat = new SimpleDateFormat("MM/dd/yyyy");
			} else {
				toFormat = new SimpleDateFormat("yyyy-MM-dd");
			}
			
			// Convert to the correct format
			Date date = frmFormat.parse(requestedDate);

			toDateStr = toFormat.format(date);
			log.info("PhydateResDate : " + toDateStr);
		}
		catch (ParseException e) {
			log.error("ParseException in getPhydateResDate method",e);
		}
		
		return toDateStr;
	}

	/**
	 * To display the physician response time
	 */
	public String getPhydateResTime(String requestedDate) { // To Display Time in PeerToPeer and Physician Review
		String input = requestedDate;
		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		SimpleDateFormat outputformat = new SimpleDateFormat("hh:mm aa");
		Date date = null;
		String output = null;
		try {
			date = df.parse(input);
			output = outputformat.format(date);
		}
		catch (ParseException e) {
			log.error("ParseException in getPhydateResTime method",e);
		}
		return output;
	}
	
	/**
	 * To set the physician response date
	 */

	public String setPhydateResDate(String requestedDate, String browser) {// To save Date format in PeerToPeer and Physician Review
		String toDateStr = "";
		SimpleDateFormat frmFormat = null;
		SimpleDateFormat toFormat = new SimpleDateFormat("yyyy-MM-dd");

		try {
			if (browser.equals("IE") || browser.equals("Firefox") || browser.equals("unknown")) {
				
				// Test for watermark
				if (requestedDate.equals("mm/dd/yyyy")) {
					requestedDate = "";
				}
				
				// Convert to the displayed format
				frmFormat = new SimpleDateFormat("MM/dd/yyyy");
			} else {
				frmFormat = new SimpleDateFormat("yyyy-MM-dd");
			}
			
			// Test for empty date
			if ((requestedDate != null) && (requestedDate.length() > 0)) {

				// Convert to the correct format
				Date date = frmFormat.parse(requestedDate);

				toDateStr = toFormat.format(date);
			}
		}
		catch (ParseException e) {
			log.error("ParseException in setPhydateResDate method",e);
		}

		return toDateStr;
	}
	
	/**
	 * To display the current date
	 */

	public static String getCurrentDate(PortletRequest request) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm a");
		Date date = new Date();
		String currentDate = dateFormat.format(date);
		return currentDate;
	}

	/**
	 * coverting the String date object into Calender object for the service request.
	 */
	public Calendar getDate(String date) {
		String dateStr = date;
		SimpleDateFormat curFormater = new SimpleDateFormat("MM/dd/yyyy");
		Date dateObj = null;
		Calendar calendar = Calendar.getInstance();
		try {
			dateObj = curFormater.parse(dateStr);
			calendar.setTime(dateObj);
		}
		catch (ParseException e) {
			log.error("ParseException in getDate method",e);
		}
		return calendar;
	}

	/**
	 * To Display the Saved Date in Notification Page
	 */
	
	public Calendar getNotificationDate(String date) { 
		String dateStr = date;
		SimpleDateFormat curFormater = new SimpleDateFormat("yyyy-MM-dd");
		Date dateObj = null;
		Calendar calendar = Calendar.getInstance();
		try {
			dateObj = curFormater.parse(dateStr);
			calendar.setTime(dateObj);
		}
		catch (ParseException e) {
			log.error("ParseException in getNotificationDate method",e);
		}
		return calendar;
	}

	/**
	 * To Display the Saved Date and Time in Notification Page
	 */
	
	public Calendar getNotificationDatetime(String datetime) { 
		SimpleDateFormat curFormater = new SimpleDateFormat("yyyy-MM-dd hh:mm aa");
		Date dateObj = null;
		Calendar calendar = Calendar.getInstance();
		try {
			dateObj = curFormater.parse(datetime);
			calendar.setTime(dateObj);
		}
		catch (ParseException e) {
			log.error("ParseException in getNotificationDatetime method",e);
		}
		return calendar;
	}

	
	/**
	 * To covert the calender object into string object to display the date
	 */
	public String getDateFromCalender(Calendar cal) {
		cal.add(5, 1);
		SimpleDateFormat format1 = new SimpleDateFormat("MM/dd/yyyy");
		String formatted = format1.format(cal.getTime());
		log.info("Date from calendar : " + formatted);
		return formatted;
	}

	/**
	 * To display the physicia from date
	 */
	public String getPhyFromCalender(String cal) {
		String dateSample = cal;
		String oldFormat = "MM/dd/yyyy";
		String newFormat = "yyyy-MM-dd";
		SimpleDateFormat sdf1 = new SimpleDateFormat(oldFormat);
		SimpleDateFormat sdf2 = new SimpleDateFormat(newFormat);
		String s = "";
		try {
			if (dateSample != null && dateSample.equals("")) {
				dateSample = dateSample.substring(0, 10);
				s = sdf2.format(sdf1.parse(dateSample));
			}
		}
		catch (ParseException e) {
			log.error("ParseException in getPhyFromCalender method",e);
		}
		return s;
	}
	
	/**
	 * To display the standard  from date 
	 */

	public String getStndDateFromCalender(Calendar cal) {
		cal.add(5, 1);
		SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
		String formatted = format1.format(cal.getTime());
		log.info("Standard date from Calendar : " + formatted);
		return formatted;
	}

	/**
	 * To display the notification date
	 */
	public String getNotificationDateFromCalender(Calendar cal, String browser) {
		String toDateStr = null;
		SimpleDateFormat toFormat = null;

		if (browser.equals("IE") || browser.equals("Firefox") || browser.equals("unknown")) {
			
			// Convert to the displayed format
			toFormat = new SimpleDateFormat("MM/dd/yyyy");
		} else {
			toFormat = new SimpleDateFormat("yyyy-MM-dd");
		}

		// Convert to the correct format
		toDateStr = toFormat.format(cal.getTime());
		log.info("Notification Date From Calender : " + toDateStr);
		return toDateStr;
	}

	/**
	 * To convert the format of the notification date
	 */
	public String setNotificationDate(String notificationDate, String browser) {
		String toDateStr = null;
		SimpleDateFormat frmFormat = null;
		SimpleDateFormat toFormat = new SimpleDateFormat("yyyy-MM-dd");

		try {
			if (browser.equals("IE") || browser.equals("Firefox") || browser.equals("unknown")) {
				
				// Convert to the displayed format
				frmFormat = new SimpleDateFormat("MM/dd/yyyy");
			} else {
				frmFormat = new SimpleDateFormat("yyyy-MM-dd");
			}
			
			// Convert to the correct format
			Date date = frmFormat.parse(notificationDate);

			toDateStr = toFormat.format(date);
		}
		catch (ParseException e) {
			log.error("ParseException in setNotificationDate method",e);
		}
		
		return toDateStr;
	}

	/**
	 * To convert the format of the notification date
	 */
	public String setNotificationDatetime(String notificationDatetime, String browser) {
		String toDatetimeStr = null;
		SimpleDateFormat frmFormat = null;
		SimpleDateFormat toFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm aa");

		try {
			if (browser.equals("IE") || browser.equals("Firefox") || browser.equals("unknown")) {
				
				// Convert to the displayed format
				frmFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm aa");
			} else {
				frmFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm aa");
			}
			
			// Convert to the correct format
			Date date = frmFormat.parse(notificationDatetime);

			toDatetimeStr = toFormat.format(date);
			log.info("Notification letter sent datetime : " + toDatetimeStr);
		}
		catch (ParseException e) {
			log.error("ParseException in setNotificationDatetime method",e);
		}
		
		log.info("Notification Date and Time : " + toDatetimeStr);
		return toDatetimeStr;
	}
	
	/**
	 * To display the current time
	 */

	public String getCurrentTime() {
		SimpleDateFormat dateFormat = new SimpleDateFormat("hh:mm aa");
		Date date = new Date();
		String currentDate = dateFormat.format(date);
		return currentDate;
	}


	/**
	 * To display the notification time
	 */
	public String getNotificationTimeFromCalender(Calendar cal) {
		String toTimeStr = null;
		SimpleDateFormat toFormat = null;

		// Get just the time
		toFormat = new SimpleDateFormat("hh:mm aa");

		// Convert to the correct format
		toTimeStr = toFormat.format(cal.getTime());
		log.info("Notification Time From Calender : " + toTimeStr);
		return toTimeStr;
	}

	/**
	 * To display the physician date
	 */

	public String getPhysicianDate(String dates) {
		String frmDateStr = "";
		return frmDateStr;
	}

	/**
	 * To display the request date and time
	 */
	
	public String getRequestedDateTime(String reqDate) {
		SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-mm-dd HH:mm:ss");
		SimpleDateFormat outputFormat = new SimpleDateFormat("yyyy-MM-dd KK:mm a");
		String requestedDateTime = null;
		try {
			requestedDateTime = outputFormat.format(inputFormat.parse(reqDate));
		}
		catch (ParseException e) {
			log.error("ParseException in getRequestedDateTime method",e);
		}
		return requestedDateTime;
	}
	
	/**
	 * To display service dates
	 */

	public String getServiceDates(String reqDate, String browser) {
		SimpleDateFormat inputFormat = new SimpleDateFormat("MM/dd/yyyy");
		SimpleDateFormat outputFormat = new SimpleDateFormat("yyyy-MM-dd");
		String requestedDateTime = null;

		try {
			if (browser.equals("IE") || browser.equals("Firefox") || browser.equals("unknown")) {
				requestedDateTime = reqDate;
			} else {
				requestedDateTime = outputFormat.format(inputFormat.parse(reqDate));
			}
		}
		catch (ParseException e) { 
			log.error("ParseException in getServiceDates method",e);
		}
		return requestedDateTime;
	}
	
	/**
	 * To Display Retrieve Service Date
	 */

	public String getRetrieveServiceDate(String date) {
		SimpleDateFormat inputFormat = new SimpleDateFormat("dd-MM-yyyy");
		SimpleDateFormat outputFormat = new SimpleDateFormat("yyyy-MM-dd");
		String requestedDateTime = null;
		try {
			requestedDateTime = outputFormat.format(inputFormat.parse(date));
		}
		catch (ParseException e) {
			log.error("ParseException in getRetrieveServiceDate method",e);
		}
		return requestedDateTime;
	}
	
	/**
	 * To Display Procedure Response Date
	 */

	public String getProcedureResDate(String requestedDate, String browser) {
		String toDateStr = null;
		SimpleDateFormat frmFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		SimpleDateFormat toFormat = null;

		try {
			if (browser.equals("IE") || browser.equals("Firefox") || browser.equals("unknown")) {
				
				// Convert to the displayed format
				toFormat = new SimpleDateFormat("MM/dd/yyyy");
			} else {
				toFormat = new SimpleDateFormat("yyyy-MM-dd");
			}
			
			// Convert to the correct format
			Date date = frmFormat.parse(requestedDate);

			toDateStr = toFormat.format(date);
			log.info("ProcedureResDate : " + toDateStr);
		}
		catch (ParseException e) {
			log.error("ParseException in getProcedureResDate method",e);
		}
		
		return toDateStr;
	}
	
	/**
	 * To Set the format for the Procedure Response Date
	 */

	public String setProcedureResDate(String requestedDate, String browser) {
			String toDateStr = null;
			SimpleDateFormat frmFormat = null;
			SimpleDateFormat toFormat = new SimpleDateFormat("yyyy-MM-dd");

			try {
				if (browser.equals("IE") || browser.equals("Firefox") || browser.equals("unknown")) {
					
					// Convert to the displayed format
					frmFormat = new SimpleDateFormat("MM/dd/yyyy");
				} else {
					frmFormat = new SimpleDateFormat("yyyy-MM-dd");
				}
				
				// Convert to the correct format
				Date date = frmFormat.parse(requestedDate);

				toDateStr = toFormat.format(date);
			}
			catch (ParseException e) {
				log.error("ParseException in setProcedureResDate method",e);
			}
			
			return toDateStr;
	}
	
	/**
	 * To Display Procedure Request Time
	 */

	public String getProcedureResTime(String requestedDate) { 
		String input = requestedDate;
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		SimpleDateFormat outputformat = new SimpleDateFormat("hh:mm aa");
		Date date = null;
		String output = null;
		try {
			date = df.parse(input);
			output = outputformat.format(date);
		}
		catch (ParseException e) {
			log.error("ParseException in getProcedureResTime method",e);
		}
		return output;
	}

	/**
	 * To Display Procedure Request Begin Date
	 */
	public String getProcedureReqBeginDate(String beginDate) { 
		String dateSample = beginDate;
		log.info("ProcedureReqBeginDate : " + dateSample);
		String oldFormat = "yyyy-MM-dd";
		String newFormat = "MM/dd/yyyy";
		SimpleDateFormat sdf1 = new SimpleDateFormat(oldFormat);
		SimpleDateFormat sdf2 = new SimpleDateFormat(newFormat);
		String s = "";
		try {
			if (dateSample != null && !dateSample.equals("")) {
				dateSample = dateSample.substring(0, 10);
				s = sdf2.format(sdf1.parse(dateSample));
			}
		}
		catch (ParseException e) {
			log.error("ParseException in getProcedureReqBeginDate method",e);
		}
		return s;
	}
	
	/**
	 * To Display Procedure Begin/End Date
	 */

	public String getProcedureResBeginDate(String beginDate, String browser) { 
		String toDateStr = null;
		SimpleDateFormat frmFormat = new SimpleDateFormat("dd-MM-yyyy");
		SimpleDateFormat toFormat = null;

		try {
			if (browser.equals("IE") || browser.equals("Firefox") || browser.equals("unknown")) {
				
				// Convert to the displayed format
				toFormat = new SimpleDateFormat("MM/dd/yyyy");
			} else {
				toFormat = new SimpleDateFormat("yyyy-MM-dd");
			}
			
			// Convert to the correct format
			Date date = frmFormat.parse(beginDate);

			toDateStr = toFormat.format(date);
			log.info("ProcedureResBegin/EndDate : " + toDateStr);
		}
		catch (ParseException e) {
			log.error("ParseException in getProcedureResBeginDate method",e);
		}
		
		return toDateStr;
	}

	
	/**
	 * To set the format for the Procedure Begin/End Date
	 */

	public String setProcedureResBeginDate(String beginDate, String browser) { 
		String toDateStr = null;
		SimpleDateFormat frmFormat = null;
		SimpleDateFormat toFormat = new SimpleDateFormat("yyyy-MM-dd");
	
		try {
			if (browser.equals("IE") || browser.equals("Firefox") || browser.equals("unknown")) {
				
				// Convert to the displayed format
				frmFormat = new SimpleDateFormat("MM/dd/yyyy");
			} else {
				frmFormat = new SimpleDateFormat("yyyy-MM-dd");
			}
			
			// Convert to the correct format
			Date date = frmFormat.parse(beginDate);
	
			toDateStr = toFormat.format(date);
		}
		catch (ParseException e) {
			log.error("ParseException in setProcedureResBeginDate method",e);
		}
	
		return toDateStr;
	}

	/**
	 * To Display Procedure Original Decision Date
	 */

	public String getProcedureDecisionDate(String decisionDate, String browser) {
		String toDateStr = null;
		SimpleDateFormat frmFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
		SimpleDateFormat toFormat = null;

		try {
			if (browser.equals("IE") || browser.equals("Firefox") || browser.equals("unknown")) {
				
				// Convert to the displayed format
				toFormat = new SimpleDateFormat("MM/dd/yyyy");
			} else {
				toFormat = new SimpleDateFormat("yyyy-MM-dd");
			}
			
			// Convert to the correct format
			Date date = frmFormat.parse(decisionDate);

			toDateStr = toFormat.format(date);
			log.info("ProcedureResDate : " + toDateStr);
		}
		catch (ParseException e) {
			log.error("ParseException in getProcedureDecisionDate method",e);
		}
		
		return toDateStr;
	}

	/**
	 * To Display Procedure Original Decision Date
	 */

	public String setProcedureDecisionDate(String decisionDate, String browser) {
		String toDateStr = null;
		SimpleDateFormat frmFormat = null;
		SimpleDateFormat toFormat = new SimpleDateFormat("yyyy-MM-dd");
	
		try {
			if (browser.equals("IE") || browser.equals("Firefox") || browser.equals("unknown")) {

				// Test for watermark
				if (decisionDate.equals("mm/dd/yyyy")) {
					toDateStr = "";
				} else {

					// Convert to the displayed format
					frmFormat = new SimpleDateFormat("MM/dd/yyyy");
				}
			} else {
				frmFormat = new SimpleDateFormat("yyyy-MM-dd");
			}
			
			if (frmFormat != null) {

				// Convert to the correct format
				Date date = frmFormat.parse(decisionDate);
		
				toDateStr = toFormat.format(date);
			}
		}
		catch (ParseException e) {
			log.error("ParseException in setProcedureDecisionDate method",e);
		}
	
		if (toDateStr == null) {
			toDateStr = "";
		}
		return toDateStr;
	}
	
	/**
	 * To Display Procedure Original Decision Time
	 */

	public String getProcedureDecisionTime(String decisionDate) { 
		String input = decisionDate;
		SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
		SimpleDateFormat outputformat = new SimpleDateFormat("hh:mm aa");
		Date date = null;
		String output = null;
		try {
			date = df.parse(input);
			output = outputformat.format(date);
		}
		catch (ParseException e) {
			log.error("ParseException in getProcedureDecisionTime method",e);
		}
		return output;
	}

	/**
	 * To display the app create date format
	 */
	public String getAppCreateDateFormat(String date) {
		String appCreate = "";
		if (date == null) {
			appCreate = "";
		} else if (date != null && !date.equals("")) {
			date = date.substring(0, 19);
			log.info("AppCreateDateFormat (before) : " + date);
			String oldFormat = "yyyy-MM-dd HH:mm:ss";
			String newFormat = "MM/dd/yyyy HH:mm:ss";
			SimpleDateFormat sdf1 = new SimpleDateFormat(oldFormat);
			SimpleDateFormat sdf2 = new SimpleDateFormat(newFormat);
			try {
				appCreate = sdf2.format(sdf1.parse(date));
				log.info("AppCreateDateFormat  : ---> " + appCreate);
			}
			catch (ParseException e) {
				log.error("ParseException in getAppCreateDateFormat method",e);
			}
		}
		return appCreate;
	}
	
	

	/**
	 * For Aging Date format
	 */
	public String getAgingDateFromString(String datetime, String browser) {
		String input = datetime;
		SimpleDateFormat inputformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
		SimpleDateFormat outputformat = null;
		Date date = null;
		String output = null;

		try {
			if (browser.equals("IE") || browser.equals("Firefox") || browser.equals("unknown")) {
				
				// Convert to the displayed format
				outputformat = new SimpleDateFormat("MM/dd/yyyy");
			} else {
				outputformat = new SimpleDateFormat("yyyy-MM-dd");
			}
			date = inputformat.parse(input);
			output = outputformat.format(date);
		}
		catch (ParseException e) {
			log.error("ParseException in getAgingDateFromString method",e);
		}
		log.info("Aging Date From String : " + output);
		return output;
	}

	/**
	 * For Aging Time format
	 */
	public String getAgingTimeFromString(String datetime) {
		String input = datetime;
		SimpleDateFormat inputformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
		SimpleDateFormat outputformat = new SimpleDateFormat("hh:mm aa");
		Date date = null;
		String output = null;

		try {
			date = inputformat.parse(input);
			output = outputformat.format(date);
		}
		catch (ParseException e) {
			log.error("ParseException in getAgingTimeFromString method",e);
		}
		log.info("Aging Time From String : " + output);
		return output;
	}

	
	/**
	 * To Display the Authorization Number Creation Date
	 */

	public String getAppCreateDateSaveFormat(String date) { 
		String appCreate = "";
		if (date == null) {
			appCreate = "";
		} else if (date != null && !date.equals("")) {
			date = date.substring(0, 19);
			String oldFormat = "yyyy-MM-dd HH:mm:ss";
			String newFormat = "MM/dd/yyyy HH:mm:ss";
			SimpleDateFormat sdf1 = new SimpleDateFormat(oldFormat);
			SimpleDateFormat sdf2 = new SimpleDateFormat(newFormat);
			try {
				appCreate = sdf2.format(sdf1.parse(date));
				log.info("AppCreateDateFormat  : " + appCreate);
			}
			catch (ParseException e) {
				log.error("ParseException in getAppCreateDateSaveFormat method",e);
			}
		}
		return appCreate;
	}

	/**
	 * To display the app create date time
	 */
	public String getAppCreateDateSaveRetrieve(String date) {
		String appCreate = "";
		if (date == null) {
			appCreate = "";
		} else if (date != null && !date.equals("")) {
			date = date.substring(0, 19);
			String oldFormat = "yyyy/MM/dd HH:mm:ss";
			String newFormat = "MM/dd/yyyy HH:mm:ss";
			SimpleDateFormat sdf1 = new SimpleDateFormat(oldFormat);
			SimpleDateFormat sdf2 = new SimpleDateFormat(newFormat);
			try {
				appCreate = sdf2.format(sdf1.parse(date));
			}
			catch (ParseException e) {
				log.error("ParseException in getAppCreateDateSaveRetrieve method",e);
			}
		}
		return appCreate;
	}
	
	/**
	 * To display the standard date time 
	 */

	public String getStandardDateTimeRecon(String requestedDate) {
		String input = requestedDate;
		SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy hh:mm aa");
		SimpleDateFormat outputformat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
		Date date = null;
		String output = null;
		try {
			date = df.parse(input);
			output = outputformat.format(date);
		}
		catch (ParseException e) {
			log.error("ParseException in getStandardDateTimeRecon method",e);
		}
		return output;
	}
	
	/**
	 * To display the Saved Date in all pages
	 */

	public String getSavedDate(String saveDate) { 
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		SimpleDateFormat outputformat = new SimpleDateFormat("MM/dd/yyyy hh:mm aa");
		Date date = null;
		String output = null;
		try {
			date = df.parse(saveDate);
		}
		catch (ParseException e) {
			log.error("ParseException in getSavedDate method",e);
		}
		output = outputformat.format(date);
		output = output.substring(0, 10);
		return output;
	}

	/**
	 * To display the Saved Time in all pages
	 */
	public String getSavedTime(String saveDate) { 
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		SimpleDateFormat outputformat = new SimpleDateFormat("MM/dd/yyyy hh:mm aa");
		Date date = null;
		String output = null;
		try {
			date = df.parse(saveDate);
		}
		catch (ParseException e) {
			log.error("ParseException in getSavedTime method",e);
		}
		output = outputformat.format(date);
		output = output.substring(11, 19);
		return output;
	}
	
	/**
	 * To display the Requested Date in Additional Information Page
	 */

	public String getStandardDateTimeReq(String requestedDate) { 
		String input = requestedDate;
		SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy hh:mm aa");
		SimpleDateFormat outputformat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
		Date date = null;
		String output = null;
		try {
			date = df.parse(input);
			output = outputformat.format(date);
		}
		catch (ParseException e) {
			log.error("ParseException in getStandardDateTimeReq method",e);
		}
		return output;
	}

	/**
	 * To Display saved date and time in procedure page
	 */
	public String getSavedDecisionDate(String savedDecisionDate) {  
		SimpleDateFormat inputformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		SimpleDateFormat outputformat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
		Date date = null;
		String output = null;
		try {
			date = inputformat.parse(savedDecisionDate);
			output = outputformat.format(date);
		}
		catch (ParseException e) {
			log.error("ParseException in getSavedDecisionDate method",e);
		}
		return output;
	}
	
	/**
	 * To header fields for all of the prior auth screens
	 */

	public static CurrentAuthorizationDetailsFO getHeaderDetails(PortletSession portletSession, String sourceTab) {
		CurrentAuthorizationDetailsFO currentAuthorizationDetails = new CurrentAuthorizationDetailsFO();

        /************ Getting member header details from session *******************/
        String memberDob = (String) portletSession.getAttribute("memberDob", PortletSession.APPLICATION_SCOPE);
        String memberId = (String) portletSession.getAttribute("memberId", PortletSession.APPLICATION_SCOPE);
        String memberName = (String) portletSession.getAttribute("memberName", PortletSession.APPLICATION_SCOPE);
        String authSubmissionStatus = (String) portletSession.getAttribute("authSubmissionStatus", PortletSession.APPLICATION_SCOPE);
        String authStatus = (String) portletSession.getAttribute("authorizationStatus", PortletSession.APPLICATION_SCOPE);
        String authorizationNumber = (String) portletSession.getAttribute("authorizationNumber", PortletSession.APPLICATION_SCOPE);
        String membergroupId = (String) portletSession.getAttribute("membergroupId", PortletSession.APPLICATION_SCOPE);

        String memberGender = (String) portletSession.getAttribute("memberGender", PortletSession.APPLICATION_SCOPE);
	    String memberAddressLine1 = (String) portletSession.getAttribute("memberAddressLine1", PortletSession.APPLICATION_SCOPE);
	    String memberAddressLine2 = (String) portletSession.getAttribute("memberAddressLine2", PortletSession.APPLICATION_SCOPE);
	    String memberCity = (String) portletSession.getAttribute("memberCity", PortletSession.APPLICATION_SCOPE);
	    String memberStateCode = (String) portletSession.getAttribute("memberStateCode", PortletSession.APPLICATION_SCOPE);
	    String memberZipcode = (String) portletSession.getAttribute("memberZipcode", PortletSession.APPLICATION_SCOPE);
	    String memberPhoneNumber = (String) portletSession.getAttribute("memberPhoneNumber", PortletSession.APPLICATION_SCOPE);

	    String businessSectorDescription = (String) portletSession.getAttribute("businessSegmentDescription", PortletSession.APPLICATION_SCOPE);
	    String subscriberName = (String) portletSession.getAttribute("subscriberName", PortletSession.APPLICATION_SCOPE);
	    
        /************ Setting member header details from session *******************/
        currentAuthorizationDetails.setAuthorizationNumber(authorizationNumber);
        currentAuthorizationDetails.setAuthorizationStatus(authStatus);

        currentAuthorizationDetails.setMemberDOB(memberDob);
        currentAuthorizationDetails.setMemberName(memberName);
        currentAuthorizationDetails.setIdCardNumber(memberId);
        currentAuthorizationDetails.setMemberGroupId(membergroupId);

        currentAuthorizationDetails.setGender(memberGender);
        currentAuthorizationDetails.setMemberAddressLine1(memberAddressLine1);
        currentAuthorizationDetails.setMemberAddressLine2(memberAddressLine2);
        currentAuthorizationDetails.setMemberCity(memberCity);
        currentAuthorizationDetails.setMemberStateCode(memberStateCode);
        currentAuthorizationDetails.setMemberZipcode(memberZipcode);
        currentAuthorizationDetails.setMemberPhoneNumber(memberPhoneNumber);
        
        currentAuthorizationDetails.setHpLob(businessSectorDescription);
        currentAuthorizationDetails.setSubscriberName(subscriberName);

		if (sourceTab.equals(providerTab)) {
			Long authorizationKey = (Long) portletSession.getAttribute("authorizationKey", PortletSession.APPLICATION_SCOPE);
			if (authorizationKey == null) {
		        currentAuthorizationDetails.setAuthorizationStatus("");
			} else {
	            currentAuthorizationDetails.setAuthorizationStatus(authStatus);
	            currentAuthorizationDetails.setAuthSubmissionStatus(authSubmissionStatus);
			}
			
            if ((authorizationNumber != null) && !authorizationNumber.equals("")) {
                currentAuthorizationDetails.setAuthorizationNumber(authorizationNumber);
            }

		} else if (sourceTab.equals(procedureTab)) {
			String providerAuthStatus = (String) portletSession.getAttribute("providerAuthStatus", PortletSession.APPLICATION_SCOPE);
			
	        if (providerAuthStatus != null) {
	            log.info("Authorization Status : " + providerAuthStatus);
	            currentAuthorizationDetails.setAuthorizationStatus(providerAuthStatus);
	        }
		}
		
		return currentAuthorizationDetails;
	}
	
	/**
	 * Get the text description of thje submission status code
	 */

	public static String getSubmissionStatusDesc(String code) {
		String submissionStatusDesc = "";

		if (code != null) {
            if (code.equalsIgnoreCase("10")) {
            	submissionStatusDesc = "Saved";
            } else if (code.equalsIgnoreCase("11")) {
                submissionStatusDesc = "Updated";
            } else if (code.equalsIgnoreCase("40")) {
                submissionStatusDesc = "Submitted";
            } else if (code.equalsIgnoreCase("41")) {
                submissionStatusDesc = "Updates Submitted";
            } else if (code.equalsIgnoreCase("50")) {
                submissionStatusDesc = "Sent";
            } else if (code.equalsIgnoreCase("35")) {
                submissionStatusDesc = "Void";
            } else if (code.equalsIgnoreCase("70")) {
                submissionStatusDesc = "Void HP - Submitted";
            } else if (code.equalsIgnoreCase("80")) {
                submissionStatusDesc = "Void HP";
            }
        } else {
            submissionStatusDesc = "Saved";
        }
        log.info("Submission Status Description : " + submissionStatusDesc + " for code " + code);

        return submissionStatusDesc;
	}

	/**
	 * Get the authorization age (do not stop the aging)
	 */

	public static String setAuthorizationAge(PortletSession portletSession, String appDate) {
		String authorizationAge = null;

	    log.info("appCreateDateTime Value:" + appDate);
	    if (appDate != null && !appDate.equals("")) {
	        String savedDate = AgeCalculation.getAgeCalculation().getProcedureSavedDate(appDate);
	        String date = PriorAuthUtil.getPriorAuthUtil().getSavedDate(savedDate);
	        String time = PriorAuthUtil.getPriorAuthUtil().getSavedTime(savedDate);
	        authorizationAge = AgeCalculation.getAgeCalculation().getAuthorizationAge(date, time);
	        log.info("Authorization Age: " + authorizationAge);
	        portletSession.setAttribute("authorizationAge", (Object) appDate, PortletSession.APPLICATION_SCOPE);
	    }
	    return authorizationAge;
	}

	/**
	 * Get the authorization age (Stop the aging when the Notification Letter is sent)
	 */

	public static String setAuthorizationAge(PortletSession portletSession, String appDate, Long authorizationKey, ThemeDisplay themeDisplay) {
		String authorizationAge = null;

	    log.info("appCreateDateTime Value:" + appDate);
	    if (appDate != null && !appDate.equals("")) {
	        String savedDate = AgeCalculation.getAgeCalculation().getProcedureSavedDate(appDate);
	        String date = PriorAuthUtil.getPriorAuthUtil().getSavedDate(savedDate);
	        String time = PriorAuthUtil.getPriorAuthUtil().getSavedTime(savedDate);
	        authorizationAge = AgeCalculation.getAgeCalculation().getAuthorizationAge(date, time, authorizationKey, themeDisplay);
	        log.info("Authorization Age: " + authorizationAge);
	        portletSession.setAttribute("authorizationAge", (Object) appDate, PortletSession.APPLICATION_SCOPE);
	    }
	    return authorizationAge;
	}
}