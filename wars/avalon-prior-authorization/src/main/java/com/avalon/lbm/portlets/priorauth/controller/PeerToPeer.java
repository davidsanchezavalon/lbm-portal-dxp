/**
 * Description
 *		This file contain the controller methods for the PA Peer-to-Peer Page.
 *
 * CHANGE History
 * 		Version 1.0
 * 			Initial version
 * 		Version 1.1
 * 			Changed the aging start date to the request date.  If it is not set use the creation date.
 * 			Changed the aging end date to the date the letter was sent.  If it is not set use the current date.
 * 		Version 1.2					08/27/2018
 * 			Do not display a blank note.
 * 		Version 1.3					09/28/2018
 * 			Fixed the format of the returned date and time.
 *  
 */

package com.avalon.lbm.portlets.priorauth.controller;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletRequest;
import javax.portlet.PortletSession;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.servlet.http.HttpServletRequest;

import org.apache.axis2.AxisFault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import com.avalon.lbm.portlets.priorauth.model.CurrentAuthorizationDetailsFO;
import com.avalon.lbm.portlets.priorauth.model.PeerToPeerFO;
import com.avalon.lbm.portlets.priorauth.model.PriorAuthConstants;
import com.avalon.lbm.portlets.priorauth.util.AgeCalculation;
import com.avalon.lbm.portlets.priorauth.util.PriorAuthUtil;
import com.avalon.lbm.services.PriorAuthReviewServiceStub;
import com.avalon.lbm.services.PriorAuthReviewServiceStub.PriorAuthHeader;
import com.avalon.lbm.services.PriorAuthReviewServiceStub.PriorAuthReview;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PortalUtil;

@Controller(value = "PeerToPeer")
@RequestMapping(value = { "VIEW" })
public class PeerToPeer {
	
	private static final Log log = LogFactoryUtil.getLog(PeerToPeer.class.getName());
 
    /**** Render method which gets called during initial load of JSP *************************/
    @RenderMapping
    public String handleRenderRequest(RenderRequest request, RenderResponse response, Model model) {
    	
    	HttpServletRequest httpRequest = PortalUtil.getHttpServletRequest(request);
        PortletSession portletSession = request.getPortletSession();
    	String sourceTab = PriorAuthUtil.peerToPeerTab;
    	String browserStr = getBrowser(httpRequest);
 
        Long authorizationKey = (Long) portletSession.getAttribute("authorizationKey", PortletSession.APPLICATION_SCOPE);
        log.info("authorizationKey" + authorizationKey);

        CurrentAuthorizationDetailsFO currentAuthorizationDetails = PriorAuthUtil.getHeaderDetails(portletSession, sourceTab);

        // Set the authorization age
        ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute("LIFERAY_SHARED_THEME_DISPLAY");
        String appDate = (String) portletSession.getAttribute("CreateDatetime", PortletSession.APPLICATION_SCOPE);
        currentAuthorizationDetails.setAuthorizationAge(PriorAuthUtil.setAuthorizationAge(portletSession, appDate, authorizationKey, themeDisplay));

        request.setAttribute("currentAuthorizationDetailsFO", (Object) currentAuthorizationDetails);

        if (authorizationKey == null) {
            log.info("authorization key is null");
        } else {
            log.info("authorization key is not null");
            try {
                String authorizationNumber = (String) portletSession.getAttribute("authorizationNumber", PortletSession.APPLICATION_SCOPE);

                PriorAuthReviewServiceStub priorAuthReviewServiceStub = new PriorAuthReviewServiceStub();
                PriorAuthReviewServiceStub.SearchPriorAuthNoteDetails searchPriorAuthNoteDetails = new PriorAuthReviewServiceStub.SearchPriorAuthNoteDetails();
                PriorAuthReviewServiceStub.NoteSearchCriteria noteSearchCriteria = new PriorAuthReviewServiceStub.NoteSearchCriteria();
                noteSearchCriteria.setAuthorizationKey(authorizationKey.longValue());
                noteSearchCriteria.setSourceTab(sourceTab);
                searchPriorAuthNoteDetails.setSearchCriteriaNoteRequest(noteSearchCriteria);
                PriorAuthReviewServiceStub.SearchPriorAuthNoteDetailsE searchPriorAuthNoteDetailsE = new PriorAuthReviewServiceStub.SearchPriorAuthNoteDetailsE();
                searchPriorAuthNoteDetailsE.setSearchPriorAuthNoteDetails(searchPriorAuthNoteDetails);
                
                PriorAuthReviewServiceStub.SearchPriorAuthNoteDetailsResponseE searchPriorAuthNoteDetailsResponseE = null;
                PriorAuthReviewServiceStub.SearchPriorAuthNoteDetailsResponse searchPriorAuthNoteDetailsResponse = null;
               
                //TODO: Remove mock service code and condition. Don't remove else condition code
    			if(GetterUtil.getBoolean(com.liferay.portal.kernel.util.PropsUtil.get(PriorAuthConstants.ENABLED_MOCK_SERVICE))){
    				 //Success Mock Code
    				 searchPriorAuthNoteDetailsResponseE = new PriorAuthReviewServiceStub.SearchPriorAuthNoteDetailsResponseE();
    				 searchPriorAuthNoteDetailsResponse = new PriorAuthReviewServiceStub.SearchPriorAuthNoteDetailsResponse();
    	             PriorAuthHeader priorAuthHeader = new PriorAuthHeader();
    	             priorAuthHeader.setAuthorizationKey(100L);
    			     priorAuthHeader.setNoteCreateDatetime("2015-12-12 12:12:22.222");
    			     priorAuthHeader.setNoteCreateUserId("UserId");
    			     priorAuthHeader.setPriorAuthNoteText("Notetext");
    			     priorAuthHeader.setPriorAuthNoteTypeCode("AuthType");
    			     priorAuthHeader.setSourceTab("STab");
    	             searchPriorAuthNoteDetailsResponse.set_return(new PriorAuthHeader[] { priorAuthHeader} );
    	             searchPriorAuthNoteDetailsResponseE.setSearchPriorAuthNoteDetailsResponse(searchPriorAuthNoteDetailsResponse);
    	               
    			}else{
    				 searchPriorAuthNoteDetailsResponseE = priorAuthReviewServiceStub.searchPriorAuthNoteDetails(searchPriorAuthNoteDetailsE);
    	             searchPriorAuthNoteDetailsResponse = searchPriorAuthNoteDetailsResponseE.getSearchPriorAuthNoteDetailsResponse();
    	        }
                PeerToPeerFO peerToPeerFO = new PeerToPeerFO();
                ArrayList<String> peerNotes = new ArrayList<String>();
                if (searchPriorAuthNoteDetailsResponse.get_return() != null) {
                    for (int i = 0; i < searchPriorAuthNoteDetailsResponse.get_return().length; ++i) {
                        String creationDateTimePeer = searchPriorAuthNoteDetailsResponse.get_return()[i].getNoteCreateDatetime();
                        log.info("Creation Date Time of Peer to Peer Notes" + creationDateTimePeer);
                        String frmDateStr = AgeCalculation.getAgeCalculation().getTimeForNote(creationDateTimePeer);
                        
                        // Do not add a blank note
                        if (!searchPriorAuthNoteDetailsResponse.get_return()[i].getPriorAuthNoteText().equals("")){
                        	peerNotes.add(searchPriorAuthNoteDetailsResponse.get_return()[i].getNoteCreateUserId() + "(" + frmDateStr + ")" + " : " + 
                        		searchPriorAuthNoteDetailsResponse.get_return()[i].getPriorAuthNoteText());
                        	log.info("Peer to Peer note " + (i + 1) + ": " + searchPriorAuthNoteDetailsResponse.get_return()[i].getNoteCreateUserId() + 
                        		"(" + frmDateStr + ")" + " : " + searchPriorAuthNoteDetailsResponse.get_return()[i].getPriorAuthNoteText());
                        }
                    }
                    portletSession.setAttribute("peerNotes", peerNotes, PortletSession.APPLICATION_SCOPE);
                    peerToPeerFO.setPtpNotesRead(peerNotes);
                    List readData = peerToPeerFO.getPtpNotesRead();
                    request.setAttribute("readData", (Object) readData);
                    request.setAttribute("peerToPeerFO", (Object) peerToPeerFO);
                }
                log.info("peer to peer search");
                PriorAuthReviewServiceStub.SearchPriorAuthReviewDetailsE searchPriorAuthReviewDetailsE = new PriorAuthReviewServiceStub.SearchPriorAuthReviewDetailsE();
                PriorAuthReviewServiceStub.SearchPriorAuthReviewDetails searchPriorAuthReviewDetails = new PriorAuthReviewServiceStub.SearchPriorAuthReviewDetails();
                PriorAuthReviewServiceStub.ReviewSearchCriteria reviewSearchCriteria = new PriorAuthReviewServiceStub.ReviewSearchCriteria();
                reviewSearchCriteria.setAuthorizationKey(authorizationKey.longValue());
                reviewSearchCriteria.setSourceTab(sourceTab);
                reviewSearchCriteria.setAuthorizationNumber(authorizationNumber);
                searchPriorAuthReviewDetails.setSearchCriteriaRequest(reviewSearchCriteria);
                searchPriorAuthReviewDetailsE.setSearchPriorAuthReviewDetails(searchPriorAuthReviewDetails);
                
                PriorAuthReviewServiceStub.SearchPriorAuthReviewDetailsResponseE searchPriorAuthReviewDetailsResponseE = null;
                PriorAuthReviewServiceStub.SearchPriorAuthReviewDetailsResponse searchPriorAuthReviewDetailsResponse = null;
                
                //TODO: Remove mock service code and condition. Don't remove else condition code
    			if(GetterUtil.getBoolean(com.liferay.portal.kernel.util.PropsUtil.get(PriorAuthConstants.ENABLED_MOCK_SERVICE))){
    				 //Success Mock Code
    				searchPriorAuthReviewDetailsResponse = searchPriorAuthReviewSuccessMock();
    				} else {
    				searchPriorAuthReviewDetailsResponseE = priorAuthReviewServiceStub.searchPriorAuthReviewDetails(searchPriorAuthReviewDetailsE);
                    searchPriorAuthReviewDetailsResponse = searchPriorAuthReviewDetailsResponseE.getSearchPriorAuthReviewDetailsResponse();
                }
                log.info("getRequestedDate :" + searchPriorAuthReviewDetailsResponse.get_return().getDateTimeSent());
                log.info("getReturnedDate :" + searchPriorAuthReviewDetailsResponse.get_return().getReturnedDate());
                log.info("getPhysicianName :" + searchPriorAuthReviewDetailsResponse.get_return().getPhysicianName());
                log.info("getRequestedBy :" + searchPriorAuthReviewDetailsResponse.get_return().getRequestedBy());
                log.info("getDecisionValue :" + searchPriorAuthReviewDetailsResponse.get_return().getDecisionValue());
                if (searchPriorAuthReviewDetailsResponse.get_return().getRequestedDate() != null && !searchPriorAuthReviewDetailsResponse.get_return().getRequestedDate().equals("") && !searchPriorAuthReviewDetailsResponse.get_return().getRequestedDate().equalsIgnoreCase("null")) {
                    peerToPeerFO.setPtpDecisionRequestedDate(PriorAuthUtil.getPriorAuthUtil().getPhydateResDate(searchPriorAuthReviewDetailsResponse.get_return().getRequestedDate(), browserStr));
                    peerToPeerFO.setPtpDecisionRequestedTime(PriorAuthUtil.getPriorAuthUtil().getPhydateResTime(searchPriorAuthReviewDetailsResponse.get_return().getRequestedDate()));
                    if (searchPriorAuthReviewDetailsResponse.get_return().getReturnedDate() != null && !searchPriorAuthReviewDetailsResponse.get_return().getReturnedDate().equals("")) {
                        peerToPeerFO.setPtpDecisionReturnedDate(PriorAuthUtil.getPriorAuthUtil().getPhydateResDate(searchPriorAuthReviewDetailsResponse.get_return().getReturnedDate(), browserStr));
                        peerToPeerFO.setPtpDecisionReturnedTime(PriorAuthUtil.getPriorAuthUtil().getPhydateResTime(searchPriorAuthReviewDetailsResponse.get_return().getReturnedDate()));
                    }
                    peerToPeerFO.setPtpPhyName(searchPriorAuthReviewDetailsResponse.get_return().getPhysicianName());
                } else {
                    peerToPeerFO.setPtpDecisionRequestedDate(searchPriorAuthReviewDetailsResponse.get_return().getRequestedDate());
                    peerToPeerFO.setPtpDecisionRequestedTime(searchPriorAuthReviewDetailsResponse.get_return().getRequestedDate());
                    if (searchPriorAuthReviewDetailsResponse.get_return().getReturnedDate() != null && !searchPriorAuthReviewDetailsResponse.get_return().getReturnedDate().equals("")) {
                        peerToPeerFO.setPtpDecisionReturnedDate(PriorAuthUtil.getPriorAuthUtil().getPhydateResDate(searchPriorAuthReviewDetailsResponse.get_return().getReturnedDate(), browserStr));
                        peerToPeerFO.setPtpDecisionReturnedTime(PriorAuthUtil.getPriorAuthUtil().getPhydateResTime(searchPriorAuthReviewDetailsResponse.get_return().getReturnedDate()));
                    }
                }
                peerToPeerFO.setPtpPhyName(searchPriorAuthReviewDetailsResponse.get_return().getPhysicianName());
                peerToPeerFO.setPtpDecisionRequestedBy(searchPriorAuthReviewDetailsResponse.get_return().getRequestedBy());
                peerToPeerFO.setPtpDecision(searchPriorAuthReviewDetailsResponse.get_return().getDecisionValue());
                request.setAttribute("PtpDecisionRequestedBy", (Object) peerToPeerFO.getPtpDecisionRequestedBy());
                request.setAttribute("peerToPeerFO1", (Object) peerToPeerFO);
            } catch (AxisFault e) {
            	log.error("AxisFault Exception in handleRenderRequest method",e);
            } catch (RemoteException e) {
            	log.error("RemoteException in handleRenderRequest method",e);

            }
        }
        return "view";
    }
    /**************Action method which gets called on click of SAVE button***************************************/
    @ActionMapping(params = { "action=peerToPeerAction" })
    public void peerToPeerDetails(ActionRequest request, ActionResponse response, @ModelAttribute(value = "peerToPeerFO") PeerToPeerFO peerToPeerFO, Map map) {
        try {
        	
        	HttpServletRequest httpRequest = PortalUtil.getHttpServletRequest(request);
        	String sourceTab = PriorAuthUtil.peerToPeerTab;
        	String noteType = PriorAuthUtil.peerToPeerNote;
        	String browserStr = getBrowser(httpRequest);

            PortletSession portletSession = request.getPortletSession();
            ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute("LIFERAY_SHARED_THEME_DISPLAY");
            Long authorizationKey = (Long) portletSession.getAttribute("authorizationKey", PortletSession.APPLICATION_SCOPE);
            String creationDate = AgeCalculation.getAgeCalculation().createDate();
            PriorAuthReviewServiceStub priorAuthReviewServiceStub = new PriorAuthReviewServiceStub();
            String ptpNotesCreate = ParamUtil.getString((PortletRequest) request, (String) "ptpNotesCreate");
            if (!ptpNotesCreate.equals("") && ptpNotesCreate != null) {
                PriorAuthReviewServiceStub.CreatePriorAuthNoteDetails createPriorAuthNoteDetails = new PriorAuthReviewServiceStub.CreatePriorAuthNoteDetails();
                PriorAuthReviewServiceStub.CreatePriorAuthNoteDetailsE CreatePriorAuthNoteDetailsE2 = new PriorAuthReviewServiceStub.CreatePriorAuthNoteDetailsE();
                PriorAuthReviewServiceStub.PriorAuthHeader priorAuthHeader = new PriorAuthReviewServiceStub.PriorAuthHeader();
                priorAuthHeader.setPriorAuthNoteTypeCode(noteType);
                priorAuthHeader.setSourceTab(sourceTab);
                priorAuthHeader.setAuthorizationKey(authorizationKey.longValue());
                priorAuthHeader.setNoteCreateUserId(themeDisplay.getUser().getEmailAddress());
                priorAuthHeader.setNoteCreateDatetime(creationDate);
                priorAuthHeader.setPriorAuthNoteText(ptpNotesCreate);
                createPriorAuthNoteDetails.setPriorAuthNoteRequest(priorAuthHeader);
                CreatePriorAuthNoteDetailsE2.setCreatePriorAuthNoteDetails(createPriorAuthNoteDetails);
                
                //TODO: Remove condition. Don't remove else condition code
    			if(GetterUtil.getBoolean(com.liferay.portal.kernel.util.PropsUtil.get(PriorAuthConstants.ENABLED_MOCK_SERVICE))){
    			}else{
    			     PriorAuthReviewServiceStub.CreatePriorAuthNoteDetailsResponseE createPriorAuthNoteDetailsResponseE = 
    	                		priorAuthReviewServiceStub.createPriorAuthNoteDetails(CreatePriorAuthNoteDetailsE2);
    	             Long longKey = createPriorAuthNoteDetailsResponseE.getCreatePriorAuthNoteDetailsResponse().get_return().getAuthorizationKey();
    	        }
           }
            String authorizationNumber = (String) portletSession.getAttribute("authorizationNumber", PortletSession.APPLICATION_SCOPE);
            PriorAuthReviewServiceStub.SearchPriorAuthReviewDetailsE searchPriorAuthReviewDetailsE = new PriorAuthReviewServiceStub.SearchPriorAuthReviewDetailsE();
            PriorAuthReviewServiceStub.SearchPriorAuthReviewDetails searchPriorAuthReviewDetails = new PriorAuthReviewServiceStub.SearchPriorAuthReviewDetails();
            PriorAuthReviewServiceStub.ReviewSearchCriteria reviewSearchCriteria = new PriorAuthReviewServiceStub.ReviewSearchCriteria();
            reviewSearchCriteria.setAuthorizationKey(authorizationKey.longValue());
            reviewSearchCriteria.setSourceTab(sourceTab);
            reviewSearchCriteria.setAuthorizationNumber(authorizationNumber);
            searchPriorAuthReviewDetails.setSearchCriteriaRequest(reviewSearchCriteria);
            searchPriorAuthReviewDetailsE.setSearchPriorAuthReviewDetails(searchPriorAuthReviewDetails);
            
            PriorAuthReviewServiceStub.SearchPriorAuthReviewDetailsResponseE searchPriorAuthReviewDetailsResponseE = null; 
          
            //TODO: Remove mock service code and condition. Don't remove else condition code
			if(GetterUtil.getBoolean(com.liferay.portal.kernel.util.PropsUtil.get(PriorAuthConstants.ENABLED_MOCK_SERVICE))){
				 //Success Mock Code
				 searchPriorAuthReviewDetailsResponseE = new PriorAuthReviewServiceStub.SearchPriorAuthReviewDetailsResponseE();
				 searchPriorAuthReviewDetailsResponseE.setSearchPriorAuthReviewDetailsResponse(searchPriorAuthReviewSuccessMock());
			}else{
				  searchPriorAuthReviewDetailsResponseE = priorAuthReviewServiceStub.searchPriorAuthReviewDetails(searchPriorAuthReviewDetailsE);
		    }
            PriorAuthReviewServiceStub.SearchPriorAuthReviewDetailsResponse searchPriorAuthReviewDetailsResponse = searchPriorAuthReviewDetailsResponseE.getSearchPriorAuthReviewDetailsResponse();
            peerToPeerFO.setPtpDecisionRequestedDate(searchPriorAuthReviewDetailsResponse.get_return().getRequestedDate());
            peerToPeerFO.setPtpDecisionReturnedDate(searchPriorAuthReviewDetailsResponse.get_return().getReturnedDate());
            peerToPeerFO.setPtpPhyName(searchPriorAuthReviewDetailsResponse.get_return().getPhysicianName());
            peerToPeerFO.setPtpDecisionRequestedBy(searchPriorAuthReviewDetailsResponse.get_return().getRequestedBy());
            peerToPeerFO.setPtpDecision(searchPriorAuthReviewDetailsResponse.get_return().getDecisionKey());
            PriorAuthReviewServiceStub priorAuthReviewServiceStub1 = new PriorAuthReviewServiceStub();
            PriorAuthReviewServiceStub.CreatePriorAuthReviewDetails createPriorAuthReviewDetails = new PriorAuthReviewServiceStub.CreatePriorAuthReviewDetails();
            PriorAuthReviewServiceStub.CreatePriorAuthReviewDetailsE createPriorAuthReviewDetailsE = new PriorAuthReviewServiceStub.CreatePriorAuthReviewDetailsE();
            PriorAuthReviewServiceStub.PriorAuthReview PriorAuthReview2 = new PriorAuthReviewServiceStub.PriorAuthReview();
            PriorAuthReview2.setAuthorizationKey(authorizationKey.longValue());
            PriorAuthReview2.setSourceTab(sourceTab);
            String requestedDate = ParamUtil.getString((PortletRequest) request, (String) "ptpDecisionRequestedDate");
            requestedDate = PriorAuthUtil.getPriorAuthUtil().setPhydateResDate(requestedDate, browserStr);
            String returnedDate = ParamUtil.getString((PortletRequest) request, (String) "ptpDecisionReturnedDate");
            returnedDate = PriorAuthUtil.getPriorAuthUtil().setPhydateResDate(returnedDate, browserStr);
            String ptpName = ParamUtil.getString((PortletRequest) request, (String) "ptpPhyName");
            String ptpDecision = ParamUtil.getString((PortletRequest) request, (String) "ptpDecision");
            String reqDateTime = requestedDate + " " + peerToPeerFO.getPtpDecisionRequestedTime();
            String retrnDateTime = returnedDate + " " + peerToPeerFO.getPtpDecisionReturnedTime();
            log.info("Peer To Peer Review Requested Date/Time --->  " + reqDateTime);
            log.info("Peer To Peer Review Returned Date/Time --->  " + retrnDateTime);
            PriorAuthReview2.setRequestedDate(PriorAuthUtil.getPriorAuthUtil().getPhydateReq(reqDateTime));
            if (returnedDate != null && !returnedDate.equalsIgnoreCase("") && peerToPeerFO.getPtpDecisionReturnedTime() != null && !peerToPeerFO.getPtpDecisionReturnedTime().equalsIgnoreCase("")) {
                PriorAuthReview2.setReturnedDate(PriorAuthUtil.getPriorAuthUtil().getPhydateReq(retrnDateTime));
            } else {
                PriorAuthReview2.setReturnedDate(null);
            }
            PriorAuthReview2.setDateTimeSent(PriorAuthUtil.getPriorAuthUtil().getPhydateReq(reqDateTime));
            log.info("Peer to Peer Request Date Time" + reqDateTime);
            PriorAuthReview2.setRequestedBy(themeDisplay.getUser().getEmailAddress());
            PriorAuthReview2.setReturnedBy(themeDisplay.getUser().getEmailAddress());
            PriorAuthReview2.setIsReceived(true);
            PriorAuthReview2.setRequestedInfo("testInfo");
            log.info("Peer to Peer Decision Reason" + peerToPeerFO.getPtpDecision());
            PriorAuthReview2.setPhysicianName(ptpName);
            log.info("Peer to Peer Decision Value" + peerToPeerFO.getPtpDecision());
            PriorAuthReview2.setDecisionValue(ptpDecision);
            PriorAuthReview2.setDecisionKey(ptpDecision);
            createPriorAuthReviewDetails.setPriorAuthReviewRequest(PriorAuthReview2);
            createPriorAuthReviewDetailsE.setCreatePriorAuthReviewDetails(createPriorAuthReviewDetails);
            
            //TODO: Remove below condition. Don't remove else condition code
			if(GetterUtil.getBoolean(com.liferay.portal.kernel.util.PropsUtil.get(PriorAuthConstants.ENABLED_MOCK_SERVICE))){
			}else{
				 PriorAuthReviewServiceStub.CreatePriorAuthReviewDetailsResponseE createPriorAuthReviewDetailsResponseE = 
		            		priorAuthReviewServiceStub1.createPriorAuthReviewDetails(createPriorAuthReviewDetailsE);
		         Long longKey2 = createPriorAuthReviewDetailsResponseE.getCreatePriorAuthReviewDetailsResponse().get_return().getAuthorizationKey();
		    }
            request.setAttribute("peerToPeerFO", (Object) peerToPeerFO);
        } catch (RemoteException e) {
        	log.error("RemoteException in peerToPeerDetails method",e);
        }
    }
    
	private String getBrowser(HttpServletRequest httpRequest) {
		String userAgent = httpRequest.getHeader("User-Agent");
		String rtnValue = "unknown";
		
		if (userAgent.contains("Opera") || userAgent.contains("OPR")) {
			rtnValue = "Opera";
		} else if (userAgent.contains("Chrome")) {
			rtnValue = "Chrome";
		} else if (userAgent.contains("Safari")) {
			rtnValue = "Safari";
		} else if (userAgent.contains("MSIE") || (userAgent.contains("Trident") && userAgent.contains("rv:"))) { 
			rtnValue = "IE";
		} else if (userAgent.contains("Firefox"))  {
			rtnValue = "Firefox";
		} else {
			rtnValue = "unknown";
		}
		return rtnValue;
	}

    @ActionMapping
    public void PtPHandler() {
        log.info("Peer to Peer Review Handler");
    }
    
    /**
     * TODO: Remove below success mock code
     * @return
     */
    private PriorAuthReviewServiceStub.SearchPriorAuthReviewDetailsResponse searchPriorAuthReviewSuccessMock(){
    	 
    	PriorAuthReviewServiceStub.SearchPriorAuthReviewDetailsResponse searchPriorAuthReviewDetailsResponse = new PriorAuthReviewServiceStub.SearchPriorAuthReviewDetailsResponse();
         
    	     PriorAuthReview priorAuthReview = new PriorAuthReview();
         	 Calendar cal = Calendar.getInstance(); 
	         priorAuthReview.setAuthorizationKey(100L);
	         priorAuthReview.setDateTimeReturned("2018-04-26 01:02:00");
	         priorAuthReview.setDateTimeSent("2018-04-26 01:02:00");
	         priorAuthReview.setDecisionKey("DKey");
	         priorAuthReview.setDecisionValue("DValue");
	         priorAuthReview.setEnterPhysicianRationale("EPR");
	         priorAuthReview.setIntakeNotesCreate("INC");
	         priorAuthReview.setIntakeNotesRead("INR");
	         priorAuthReview.setIsReceived(true);
	         priorAuthReview.setNotificationBy("NY");
	         priorAuthReview.setNotificationContactFax("9912199121");
	         priorAuthReview.setNotificationContactName("NCN");
	         priorAuthReview.setNotificationContactPhone("9912199121");
	         priorAuthReview.setNotificationSentDate(cal);
	         priorAuthReview.setNurseNotesCreate("NCreate");
	         priorAuthReview.setNurseNotesRead("Nread");
	         priorAuthReview.setPhysicianName("PName");
	         priorAuthReview.setPhysicianRationaleMessage("RMsg");
	         priorAuthReview.setReasonKey("RKey");
	         priorAuthReview.setReasonValue("RValue");
	         priorAuthReview.setReconDecision("RD");
	         priorAuthReview.setReconDecisionDate("2018-04-26 01:02:00");
	         priorAuthReview.setReconNotesCreate("RNC");
	         priorAuthReview.setReconDecisionReviewer("RDR");
	         priorAuthReview.setReconNotesRead("RNR");
	         priorAuthReview.setRequestedBy("RB");
	         priorAuthReview.setRequestedDate("12/12/1993 11:11:11");
	         priorAuthReview.setRequestedInfo("RInfo");
	         priorAuthReview.setSourceTab("STab");
	         searchPriorAuthReviewDetailsResponse.set_return(priorAuthReview);
	         return searchPriorAuthReviewDetailsResponse;
    }
}
