package com.avalon.lbm.portlets.priorauth.model;

/**
 * Description
 *		This file contain the getter and setter methods for the Prior Authorization Notification.
 *
 * CHANGE History
 * 		Version 1.0
 * 			Initial version copied from avalon-prior-authorization.
 * 		Version 1.1			12/14/2017  
 *			Added Notification Fax Number.
 *  
 */

import java.io.Serializable;


public class NotificationFO implements Serializable {

	/**
	 * To store Notification fields
	 */
	private static final long serialVersionUID = -1125828568145284503L;
	private String notificationDate; 
	private String notificationBy;
	private String notificationContactName;
	private String notificationSentDate;
	private String notificationSentTime;
	private String notificationContactPhone;
	private String notificationContactFax;

	public String getNotificationDate() {
		return notificationDate;
	}
	public void setNotificationDate(String notificationDate) {
		this.notificationDate = notificationDate;
	}

	public String getNotificationBy() {
		return notificationBy;
	}
	public void setNotificationBy(String notificationBy) {
		this.notificationBy = notificationBy;
	}

	public String getNotificationContactName() {
		return notificationContactName;
	}
	public void setNotificationContactName(String notificationContactName) {
		this.notificationContactName = notificationContactName;
	}

	public String getNotificationSentDate() {
		return notificationSentDate;
	}
	public void setNotificationSentDate(String notificationSentDate) {
		this.notificationSentDate = notificationSentDate;
	}

	public String getNotificationSentTime() {
		return notificationSentTime;
	}
	public void setNotificationSentTime(String notificationSentTime) {
		this.notificationSentTime = notificationSentTime;
	}

	public String getNotificationContactPhone() {
		return notificationContactPhone;
	}
	public void setNotificationContactPhone(String notificationContactPhone) {
		this.notificationContactPhone = notificationContactPhone;
	}

	public String getNotificationContactFax() {
		return notificationContactFax;
	}
	public void setNotificationContactFax(String notificationContactFax) {
		this.notificationContactFax = notificationContactFax;
	}
}
