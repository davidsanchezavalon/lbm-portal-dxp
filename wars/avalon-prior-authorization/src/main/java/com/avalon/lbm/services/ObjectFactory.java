
package com.avalon.lbm.services;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.avalon.lbm.services package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _TrialClaimsProcedureReqHdr_QNAME = new QName("http://services.lbm.avalon.com/", "trialClaimsProcedureReqHdr");
    private final static QName _DAOException_QNAME = new QName("http://services.lbm.avalon.com/", "DAOException");
    private final static QName _GetEnumShrtDescription_QNAME = new QName("http://services.lbm.avalon.com/", "getEnumShrtDescription");
    private final static QName _GetIcdDiagnosisCode_QNAME = new QName("http://services.lbm.avalon.com/", "getIcdDiagnosisCode");
    private final static QName _GetEnumShrtDescriptionResponse_QNAME = new QName("http://services.lbm.avalon.com/", "getEnumShrtDescriptionResponse");
    private final static QName _GetIcdDiagnosisCodeResponse_QNAME = new QName("http://services.lbm.avalon.com/", "getIcdDiagnosisCodeResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.avalon.lbm.services
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetIcdDiagnosisCode }
     * 
     */
    public GetIcdDiagnosisCode createGetIcdDiagnosisCode() {
        return new GetIcdDiagnosisCode();
    }

    /**
     * Create an instance of {@link GetEnumShrtDescriptionResponse }
     * 
     */
    public GetEnumShrtDescriptionResponse createGetEnumShrtDescriptionResponse() {
        return new GetEnumShrtDescriptionResponse();
    }

    /**
     * Create an instance of {@link GetIcdDiagnosisCodeResponse }
     * 
     */
    public GetIcdDiagnosisCodeResponse createGetIcdDiagnosisCodeResponse() {
        return new GetIcdDiagnosisCodeResponse();
    }

    /**
     * Create an instance of {@link DAOException }
     * 
     */
    public DAOException createDAOException() {
        return new DAOException();
    }

    /**
     * Create an instance of {@link TrialClaimsProcedureReqHdr }
     * 
     */
    public TrialClaimsProcedureReqHdr createTrialClaimsProcedureReqHdr() {
        return new TrialClaimsProcedureReqHdr();
    }

    /**
     * Create an instance of {@link GetEnumShrtDescription }
     * 
     */
    public GetEnumShrtDescription createGetEnumShrtDescription() {
        return new GetEnumShrtDescription();
    }

    /**
     * Create an instance of {@link TrialClaimsDiagnosisReqHdr }
     * 
     */
    public TrialClaimsDiagnosisReqHdr createTrialClaimsDiagnosisReqHdr() {
        return new TrialClaimsDiagnosisReqHdr();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TrialClaimsProcedureReqHdr }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.lbm.avalon.com/", name = "trialClaimsProcedureReqHdr")
    public JAXBElement<TrialClaimsProcedureReqHdr> createTrialClaimsProcedureReqHdr(TrialClaimsProcedureReqHdr value) {
        return new JAXBElement<TrialClaimsProcedureReqHdr>(_TrialClaimsProcedureReqHdr_QNAME, TrialClaimsProcedureReqHdr.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DAOException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.lbm.avalon.com/", name = "DAOException")
    public JAXBElement<DAOException> createDAOException(DAOException value) {
        return new JAXBElement<DAOException>(_DAOException_QNAME, DAOException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetEnumShrtDescription }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.lbm.avalon.com/", name = "getEnumShrtDescription")
    public JAXBElement<GetEnumShrtDescription> createGetEnumShrtDescription(GetEnumShrtDescription value) {
        return new JAXBElement<GetEnumShrtDescription>(_GetEnumShrtDescription_QNAME, GetEnumShrtDescription.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetIcdDiagnosisCode }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.lbm.avalon.com/", name = "getIcdDiagnosisCode")
    public JAXBElement<GetIcdDiagnosisCode> createGetIcdDiagnosisCode(GetIcdDiagnosisCode value) {
        return new JAXBElement<GetIcdDiagnosisCode>(_GetIcdDiagnosisCode_QNAME, GetIcdDiagnosisCode.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetEnumShrtDescriptionResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.lbm.avalon.com/", name = "getEnumShrtDescriptionResponse")
    public JAXBElement<GetEnumShrtDescriptionResponse> createGetEnumShrtDescriptionResponse(GetEnumShrtDescriptionResponse value) {
        return new JAXBElement<GetEnumShrtDescriptionResponse>(_GetEnumShrtDescriptionResponse_QNAME, GetEnumShrtDescriptionResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetIcdDiagnosisCodeResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.lbm.avalon.com/", name = "getIcdDiagnosisCodeResponse")
    public JAXBElement<GetIcdDiagnosisCodeResponse> createGetIcdDiagnosisCodeResponse(GetIcdDiagnosisCodeResponse value) {
        return new JAXBElement<GetIcdDiagnosisCodeResponse>(_GetIcdDiagnosisCodeResponse_QNAME, GetIcdDiagnosisCodeResponse.class, null, value);
    }

}
