package com.avalon.lbm.portlets.servicehandler;

import com.avalon.lbm.services.PriorAuthTrialClaimsService;
import com.avalon.lbm.services.PriorAuthTrialClaimsService_Service;
import com.avalon.lbm.services.TrialClaimsDiagnosisReqHdr;
import com.avalon.lbm.services.TrialClaimsProcedureReqHdr;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

/**
 * Description
 *	This file contain the service methods for Procedure Information.
 *
 * @author David Sanchez
 *
 *	Version 1.0
 *		Initial version
 *	Version 1.1
 *		Remove the static initialization code.  Create the object the first time it is used.
 *
 */

public class ServiceHandler {
    
	private static final Log log = LogFactoryUtil.getLog(ServiceHandler.class.getName());
	
    private static PriorAuthTrialClaimsService_Service priorAuthTrialClaimsService_Service = null;
    private static TrialClaimsDiagnosisReqHdr trialClaimsDiagnosisReqHdr = null;
    private static TrialClaimsProcedureReqHdr trialClaimsProcedureReqHdr = null;

    private ServiceHandler() {

    }
    
    public static TrialClaimsDiagnosisReqHdr getTrialClaimsDiagnosisReqHdr() {
	
    	log.info(" In getTrialClaimsDiagnosisReqHdr method ");

    	if (trialClaimsDiagnosisReqHdr == null) {
    		trialClaimsDiagnosisReqHdr = new TrialClaimsDiagnosisReqHdr();
    	}
    	return trialClaimsDiagnosisReqHdr;
    }

    public static void setTrialClaimsDiagnosisReqHdr(TrialClaimsDiagnosisReqHdr trialClaimsDiagReqHdr) {
	
    	log.info(" In setTrialClaimsDiagnosisReqHdr method ");
 	
    	trialClaimsDiagnosisReqHdr = trialClaimsDiagReqHdr;
    }

    public static PriorAuthTrialClaimsService getPriorAuthTrialClaimsPort() {
	
    	log.info(" In getPriorAuthTrialClaimsPort method ");
	
    	if (priorAuthTrialClaimsService_Service == null) {
		    priorAuthTrialClaimsService_Service = new PriorAuthTrialClaimsService_Service();
    	}
    	return priorAuthTrialClaimsService_Service.getPriorAuthTrialClaimsPort();
    }

    public static TrialClaimsProcedureReqHdr getTrialClaimsProcedureReqHdr() {
	
    	log.info(" In getTrialClaimsProcedureReqHdr method ");
	
    	if (trialClaimsProcedureReqHdr == null) {
    		trialClaimsProcedureReqHdr = new TrialClaimsProcedureReqHdr();
    	}
    	return trialClaimsProcedureReqHdr;
    }

    public static void setTrialClaimsProcedureReqHdr(TrialClaimsProcedureReqHdr trialClaimsProcReqHdr) {
	
    	log.info(" In setTrialClaimsProcedureReqHdr method ");
 	
    	trialClaimsProcedureReqHdr = trialClaimsProcReqHdr;
    }
}
