/**
 * Description
 *		This file contain the controller methods for the PA Provider Information Page.
 *
 * @author David Sanchez
 *
 * CHANGE History
 * 		Version 1.0
 * 			Initial version
 * 		Version 1.1
 * 			Changed the aging start date to the request date.  If it is not set use the creation date.
 * 			Changed the aging end date to the date the letter was sent.  If it is not set use the current date.
 * 		Version 1.2
 * 			Set the provider type code for SAVE.
 *  
 */

package com.avalon.lbm.portlets.priorauth.controller;

import java.io.IOException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletMode;
import javax.portlet.PortletModeException;
import javax.portlet.PortletRequest;
import javax.portlet.PortletSession;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.WindowState;
import javax.portlet.WindowStateException;
import javax.servlet.http.HttpServletRequest;

import org.apache.axis2.AxisFault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import com.avalon.lbm.portlets.priorauth.model.CurrentAuthorizationDetailsFO;
import com.avalon.lbm.portlets.priorauth.model.PriorAuthConstants;
import com.avalon.lbm.portlets.priorauth.model.ProviderInformationFO;
import com.avalon.lbm.portlets.priorauth.util.PriorAuthUtil;
import com.avalon.lbm.services.DAOExceptionException;
import com.avalon.lbm.services.PriorAuthHeaderServiceStub;
import com.avalon.lbm.services.PriorAuthHeaderServiceStub.PriorAuthHeader;
import com.avalon.lbm.services.PriorAuthHeaderServiceStub.PriorAuthHeaderDTO;
import com.avalon.lbm.services.PriorAuthHeaderServiceStub.ProviderDTO;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Layout;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.model.UserGroup;
import com.liferay.portal.kernel.portlet.LiferayPortletURL;
import com.liferay.portal.kernel.portlet.PortletURLFactoryUtil;
import com.liferay.portal.kernel.service.LayoutLocalServiceUtil;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.util.portlet.PortletProps;

@Controller(value = "ProviderInformation")
@RequestMapping(value = { "VIEW" })
public class ProviderInformation implements PriorAuthConstants {
	
	private static final Log log = LogFactoryUtil.getLog(ProviderInformation.class.getName());

    /**** Render method which gets called during initial load of JSP *************************/
    @RenderMapping
    public String handleRenderRequest(RenderRequest request, 
    		                          RenderResponse response, 
    		                          Model model) {
    
    	log.info("Processing Render Action for Provider Information Enter");
        String sourceTab = PriorAuthUtil.providerTab;
        PortletSession portletSession = request.getPortletSession();

        CurrentAuthorizationDetailsFO currentAuthorizationDetails = PriorAuthUtil.getHeaderDetails(portletSession, sourceTab);
        Long authorizationKey = (Long) portletSession.getAttribute("authorizationKey", PortletSession.APPLICATION_SCOPE);
		
		if (authorizationKey == null) {
			request.setAttribute("currentAuthorizationDetailsFO", (Object) currentAuthorizationDetails);
            request.setAttribute("enableTabs", (Object) "false");
            String submissionStatusCode = "10";
			portletSession.setAttribute("submissionStatusCode", submissionStatusCode, PortletSession.PORTLET_SCOPE);

            log.info("Provider Information - RENDER: submissionStatusCode --> " + submissionStatusCode);
        } else {
        	
        	 try {
            	ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute("LIFERAY_SHARED_THEME_DISPLAY");

                HttpServletRequest httprequest = PortalUtil.getHttpServletRequest((PortletRequest) request);
                HttpServletRequest originalRequest = PortalUtil.getOriginalServletRequest((HttpServletRequest) httprequest);
                originalRequest.setAttribute("enableTabs", (Object) "true");
                PriorAuthHeaderServiceStub priorAuthHeaderServiceStub = new PriorAuthHeaderServiceStub();
                PriorAuthHeaderServiceStub.GetPriorAuthHeaderE getPriorAuthHeaderE = new PriorAuthHeaderServiceStub.GetPriorAuthHeaderE();
                PriorAuthHeaderServiceStub.GetPriorAuthHeader GetPriorAuthHeader2 = new PriorAuthHeaderServiceStub.GetPriorAuthHeader();
                PriorAuthHeaderServiceStub.PriorAuthHeader priorAuthHeader = new PriorAuthHeaderServiceStub.PriorAuthHeader();
                priorAuthHeader.setSourceTab(sourceTab);
                priorAuthHeader.setAuthorizationKey(authorizationKey.longValue());
                priorAuthHeader.setAppCreateUserId(themeDisplay.getUser().getEmailAddress());
                priorAuthHeader.setAppMaintUserId(themeDisplay.getUser().getEmailAddress());
                GetPriorAuthHeader2.setSearchCriteriaRequest(priorAuthHeader);
                getPriorAuthHeaderE.setGetPriorAuthHeader(GetPriorAuthHeader2);
                
                PriorAuthHeaderServiceStub.GetPriorAuthHeaderResponseE getPriorAuthHeaderResponseE = null;
				PriorAuthHeaderServiceStub.GetPriorAuthHeaderResponse getPriorAuthHeaderResponse = null;
			    PriorAuthHeaderServiceStub.PriorAuthHeaderDTO priorAuthHeaderDTO = null;
				PriorAuthHeaderServiceStub.ProviderDTO providerDTO =null;

                //TODO: Remove mock service code and condition. Don't remove else condition code
    			if(GetterUtil.getBoolean(com.liferay.portal.kernel.util.PropsUtil.get(PriorAuthConstants.ENABLED_MOCK_SERVICE))){
    				 //Success Mock Code
    				getPriorAuthHeaderResponse = getPriorAuthHeaderResponseSuccessMock();
    				currentAuthorizationDetails.setAuthorizationAge("22");
    			}else{
    				getPriorAuthHeaderResponseE = priorAuthHeaderServiceStub.getPriorAuthHeader(getPriorAuthHeaderE);
    				getPriorAuthHeaderResponse = getPriorAuthHeaderResponseE.getGetPriorAuthHeaderResponse();
                    priorAuthHeaderDTO = getPriorAuthHeaderResponse.get_return().getPriorAuthHeaderDTO();
                    providerDTO = getPriorAuthHeaderResponse.get_return().getProvider();
    				
    			    // Set the authorization age
                    String appDate = priorAuthHeaderDTO.getAppCreateDatetime();
    				currentAuthorizationDetails.setAuthorizationAge(PriorAuthUtil.setAuthorizationAge(portletSession, appDate, authorizationKey, themeDisplay));
    	        }
    			priorAuthHeaderDTO = getPriorAuthHeaderResponse.get_return().getPriorAuthHeaderDTO();
				providerDTO = getPriorAuthHeaderResponse.get_return().getProvider();
				
			    ProviderInformationFO providerInformationFO = new ProviderInformationFO();
                log.info("Provider Information - RENDER: OrderingAddressLine1 --> " + providerDTO.getOrderingAddressLine1());
                log.info("Provider Information - RENDER: OrderingAddressLine2 --> " + providerDTO.getOrderingAddressLine2());
                log.info("Provider Information - RENDER: OrderingCity --> " + providerDTO.getOrderingCity());
                log.info("Provider Information - RENDER: OrderingFaxNumber --> " + providerDTO.getOrderingFaxNumber());
                log.info("Provider Information - RENDER: OrderingFirstName --> " + providerDTO.getOrderingFirstName());
                log.info("Provider Information - RENDER: OrderingLastName --> " + providerDTO.getOrderingLastName());
                log.info("Provider Information - RENDER: OrderingNpi --> " + providerDTO.getOrderingNpi());
                log.info("Provider Information - RENDER: OrderingPhoneNumber --> " + providerDTO.getOrderingPhoneNumber());
                log.info("Provider Information - RENDER: OrderingState --> " + providerDTO.getOrderingState());
                log.info("Provider Information - RENDER: OrderingTinEin --> " + providerDTO.getOrderingTinEin());
                log.info("Provider Information - RENDER: PriorAuthHeaderKey --> " + providerDTO.getPriorAuthHeaderKey());
                log.info("Provider Information - RENDER: PriorAuthProviderTypeCode --> " + providerDTO.getPriorAuthProviderTypeCode());
                log.info("Provider Information - RENDER: RenderingAddressLine1 --> " + providerDTO.getRenderingAddressLine1());
                log.info("Provider Information - RENDER: RenderingAddressLine2 --> " + providerDTO.getRenderingAddressLine2());
                log.info("Provider Information - RENDER: RenderingCity --> " + providerDTO.getRenderingCity());
                log.info("Provider Information - RENDER: RenderingFaxNumber --> " + providerDTO.getRenderingFaxNumber());
                log.info("Provider Information - RENDER: RenderingFirstName --> " + providerDTO.getRenderingFirstName());
                log.info("Provider Information - RENDER: RenderingLastName --> " + providerDTO.getRenderingLastName());
                log.info("Provider Information - RENDER: RenderingNpi --> " + providerDTO.getRenderingNpi());
                log.info("Provider Information - RENDER: RenderingPhoneNumber --> " + providerDTO.getRenderingPhoneNumber());
                log.info("Provider Information - RENDER: RenderingState --> " + providerDTO.getRenderingState());
                log.info("Provider Information - RENDER: RenderingTinEin --> " + providerDTO.getRenderingTinEin());

                log.info("Provider Information - RENDER: IdCardNumber --> " + priorAuthHeaderDTO.getIdCardNumber());
                log.info("Provider Information - RENDER: PriorAuthHeaderKey --> " + providerDTO.getPriorAuthHeaderKey());
                log.info("Provider Information - RENDER: PriorAuthProviderTypeCode --> " + providerDTO.getPriorAuthProviderTypeCode());
                
                // Save the Submission Status Code, the LOB data and member data 
                String submissionStatusCode = getPriorAuthHeaderResponse.get_return().getPriorAuthHeaderDTO().getPriorAuthSubmissionStatusCode();
				portletSession.setAttribute("submissionStatusCode", submissionStatusCode, PortletSession.PORTLET_SCOPE);
				log.info("Provider Information - RENDER: submissionStatusCode --> " + submissionStatusCode);

                log.info("Provider Information - RENDER: memberFirstName --> " + priorAuthHeaderDTO.getPriorAuthPatientFirstName());
                log.info("Provider Information - RENDER: memberMiddleName --> " + priorAuthHeaderDTO.getPriorAuthPatientMiddleName());
                log.info("Provider Information - RENDER: memberLastName --> " + priorAuthHeaderDTO.getPriorAuthPatientLastName());
                log.info("Provider Information - RENDER: memberDob --> " + priorAuthHeaderDTO.getPriorAuthPatientBirthDate());
                log.info("Provider Information - RENDER: memberGender --> " + priorAuthHeaderDTO.getPriorAuthPatientGenderCode());
                log.info("Provider Information - RENDER: businessSectorCode --> " + priorAuthHeaderDTO.getPriorAuthBusinessSectorCode());
                log.info("Provider Information - RENDER: businessSectorDescription --> " + priorAuthHeaderDTO.getPriorAuthBusinessSectorDescription());
                log.info("Provider Information - RENDER: businessSegmentCode --> " + priorAuthHeaderDTO.getPriorAuthBusinessSegmentCode());
                log.info("Provider Information - RENDER: businessSegmentDescription --> " + priorAuthHeaderDTO.getPriorAuthBusinessSegmentDescription());
                if (priorAuthHeaderDTO != null) {
                    currentAuthorizationDetails.setAuthorizationNumber(priorAuthHeaderDTO.getPriorAuthNumber());
                }
                currentAuthorizationDetails.setAuthorizationStatus(priorAuthHeaderDTO.getPriorAuthStatusDesc());
 
                providerInformationFO.setOrderingAddressLine1(providerDTO.getOrderingAddressLine1());
                providerInformationFO.setOrderingAddressLine2(providerDTO.getOrderingAddressLine2());
                providerInformationFO.setOrderingCity(providerDTO.getOrderingCity());
                providerInformationFO.setOrderingFaxNumber(providerDTO.getOrderingFaxNumber());
                providerInformationFO.setOrderingFirstName(providerDTO.getOrderingFirstName());
                providerInformationFO.setOrderingLastName(providerDTO.getOrderingLastName());
                providerInformationFO.setOrderingNpi(providerDTO.getOrderingNpi());
                providerInformationFO.setOrderingPhoneNumber(providerDTO.getOrderingPhoneNumber());
                providerInformationFO.setOrderingState(providerDTO.getOrderingState());
                providerInformationFO.setOrderingTinEin(providerDTO.getOrderingTinEin());
                providerInformationFO.setOrderingZip(providerDTO.getOrderingZip());
                providerInformationFO.setRenderingAddressLine1(providerDTO.getRenderingAddressLine1());
                providerInformationFO.setRenderingAddressLine2(providerDTO.getRenderingAddressLine2());
                providerInformationFO.setRenderingCity(providerDTO.getRenderingCity());
                providerInformationFO.setRenderingFaxNumber(providerDTO.getRenderingFaxNumber());
                providerInformationFO.setRenderingFirstName(providerDTO.getRenderingFirstName());
                providerInformationFO.setRenderingLabName(providerDTO.getRenderingLabName());
                providerInformationFO.setRenderingLastName(providerDTO.getRenderingLastName());
                providerInformationFO.setRenderingNpi(providerDTO.getRenderingNpi());
                providerInformationFO.setRenderingPhoneNumber(providerDTO.getRenderingPhoneNumber());
                providerInformationFO.setRenderingState(providerDTO.getRenderingState());
                providerInformationFO.setRenderingTinEin(providerDTO.getRenderingTinEin());
                providerInformationFO.setRenderingZip(providerDTO.getRenderingZip());
                
                request.setAttribute("enableTabs", (Object) "true");
                request.setAttribute("providerInformationFO", (Object) providerInformationFO);
                request.setAttribute("currentAuthorizationDetailsFO", (Object) currentAuthorizationDetails);
           } catch (AxisFault e) {
        	    log.error("IOException in handleRenderRequest method",e);
           } catch (RemoteException e) {
            	log.error("RemoteException in handleRenderRequest method",e);
           } catch (DAOExceptionException e) {
            	 log.error("DAOExceptionException in handleRenderRequest method",e);
           }
        }
        log.info("Processing Render Action for Provider Information Exit");
        return "view";
    }
    
    private String convertHeaderDateFormat(String dt) {
		String returnDt = null;
   		
		if ((dt != null) && (dt.equals("null"))) {
			dt = "";
		}
		returnDt = dt;
    
    	// Convert the header date format from "dd-MM-yyyy" to "MM/dd/yyyy".
    	if ((returnDt != null) && (returnDt.length() > 0)) {
    		returnDt = dt.substring(3, 5) + "/" + dt.substring(0, 2) + "/" + dt.substring(6);
    	}
    	return returnDt;
    }
    
    private String convertHeaderDatetimeFormat(String dt) {
   		String returnDt = null;
   		
   		if ((dt != null) && (dt.equals("null"))) {
   			dt = "";
   		}
   		returnDt = dt;
    
    	// Convert the header date format from "yyyy-mm-dd HH:mm:ss.S" to "MM/dd/yyyy HH:mm:ss.S".
   		// Make sure the time is included
    	if ((returnDt != null ) && (returnDt.contains(":"))) {
    		returnDt = dt.substring(5, 7) + "/" + dt.substring(8, 10) + "/" + dt.substring(0, 4) + dt.substring(10);
    	}
    	return returnDt;
    }

    
    /**************Action method which gets called on click of SAVE button***************************************/
    @ActionMapping(params = { "action=providerInformationAction" })
    public void handleActionRequest(ActionRequest request,
    		                        ActionResponse response, 
    		                        @ModelAttribute(value = "providerInformationFO") ProviderInformationFO providerInformationFO) throws Exception {
    
        log.info("Processing SAVE Action for Provider Information Entry");
        HttpServletRequest httpRequest = PortalUtil.getHttpServletRequest((PortletRequest) request);
        ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute("LIFERAY_SHARED_THEME_DISPLAY");
        PortletSession portletSession = request.getPortletSession();
        String sourceTab = PriorAuthUtil.providerTab;
        String appCreateDateTime = "";

        try {
        	CurrentAuthorizationDetailsFO currentAuthorizationDetails = new CurrentAuthorizationDetailsFO();
        	
            String memberId = (String) portletSession.getAttribute("memberId", PortletSession.APPLICATION_SCOPE);
            String memberNumber = (String) portletSession.getAttribute("memberNumber", PortletSession.APPLICATION_SCOPE);
            String healthPlan = (String) portletSession.getAttribute("healthPlan", PortletSession.APPLICATION_SCOPE);
            String mpi = (String) portletSession.getAttribute("mpi", PortletSession.APPLICATION_SCOPE);

            // Only change the submission status code 50.
            String submissionStatusCode = (String) portletSession.getAttribute("submissionStatusCode", PortletSession.PORTLET_SCOPE);
            if (submissionStatusCode != null) {
            	if (submissionStatusCode.equals("50")) {
            		submissionStatusCode = "11";
            	}
            } else {
            	submissionStatusCode = "10";
            }
			portletSession.setAttribute("submissionStatusCode", submissionStatusCode, PortletSession.PORTLET_SCOPE);

			String memberFirstName = (String) portletSession.getAttribute("memberFirstName", PortletSession.APPLICATION_SCOPE);
			String memberMiddleName = (String) portletSession.getAttribute("memberMiddleName", PortletSession.APPLICATION_SCOPE);
			String memberLastName = (String) portletSession.getAttribute("memberLastName", PortletSession.APPLICATION_SCOPE);
			String memberSuffixName = (String) portletSession.getAttribute("memberSuffixName", PortletSession.APPLICATION_SCOPE);
			String memberDob = (String) portletSession.getAttribute("memberDob", PortletSession.APPLICATION_SCOPE);
			String memberGender = (String) portletSession.getAttribute("memberGender", PortletSession.APPLICATION_SCOPE);
			String memberGenderCode = memberGender.substring(0, 1);
			String businessSectorCode = (String) portletSession.getAttribute("businessSectorCode", PortletSession.APPLICATION_SCOPE);
			String businessSectorDescription = (String) portletSession.getAttribute("businessSectorDescription", PortletSession.APPLICATION_SCOPE);
			String businessSegmentCode = (String) portletSession.getAttribute("businessSegmentCode", PortletSession.APPLICATION_SCOPE);
			String businessSegmentDescription = (String) portletSession.getAttribute("businessSegmentDescription", PortletSession.APPLICATION_SCOPE);

			PriorAuthHeaderServiceStub.PriorAuthHeaderDTO priorAuthHeaderDTO = new PriorAuthHeaderServiceStub.PriorAuthHeaderDTO();
            appCreateDateTime = PriorAuthUtil.getPriorAuthUtil().getOriginalDate();
            log.info("Provider Information - SAVE: appCreateDateTime value --> " + appCreateDateTime);
            priorAuthHeaderDTO.setAppCreateDatetime(appCreateDateTime);

            PriorAuthHeaderServiceStub priorAuthHeaderServiceStub = new PriorAuthHeaderServiceStub();
            PriorAuthHeaderServiceStub.SavePriorAuthHeaderE savePriorAuthHeader = new PriorAuthHeaderServiceStub.SavePriorAuthHeaderE();
            PriorAuthHeaderServiceStub.SavePriorAuthHeader savePriorAuthHead = new PriorAuthHeaderServiceStub.SavePriorAuthHeader();
            PriorAuthHeaderServiceStub.PriorAuthHeader priorAuthHeader = new PriorAuthHeaderServiceStub.PriorAuthHeader();
            priorAuthHeader.setAppCreateUserId(themeDisplay.getUser().getEmailAddress());
            priorAuthHeader.setAppMaintUserId(themeDisplay.getUser().getEmailAddress());
            priorAuthHeaderDTO.setPriorAuthStatusCode(PriorAuthConstants.IN_PROCESS_INTAKE_REVIEW_CODE);
            portletSession.setAttribute("providerAuthStatus", (Object) priorAuthHeaderDTO.getPriorAuthStatusDesc(), PortletSession.APPLICATION_SCOPE);
            String authorizationNumber = (String) portletSession.getAttribute("authorizationNumber", PortletSession.APPLICATION_SCOPE);
            if (authorizationNumber != null && authorizationNumber != "") {
                log.info("Provider Information - SAVE: Procedure Authorization Number --> " + authorizationNumber);
                priorAuthHeaderDTO.setPriorAuthNumber(authorizationNumber);
            }
            Long authorizationKey = (Long) portletSession.getAttribute("authorizationKey", PortletSession.APPLICATION_SCOPE);
            log.info("Provider Information - SAVE: Authorization Key Value --> " + authorizationKey);
            if (authorizationKey != null) {
                priorAuthHeaderDTO.setPriorAuthHeaderKey(Integer.parseInt(String.valueOf(authorizationKey)));
            }
            priorAuthHeader.setPriorAuthHeaderDTO(priorAuthHeaderDTO);
            PriorAuthHeaderServiceStub.ProviderDTO providerDTO = new PriorAuthHeaderServiceStub.ProviderDTO();
            log.info("Provider Information - SAVE: Ordering Address Line1 --> " + providerInformationFO.getOrderingAddressLine1());
            log.info("Provider Information - SAVE: Ordering Address Line2 --> " + providerInformationFO.getOrderingAddressLine2());
            log.info("Provider Information - SAVE: Ordering City --> " + providerInformationFO.getOrderingCity());
            log.info("Provider Information - SAVE: Ordering Fax Number --> " + providerInformationFO.getOrderingFaxNumber());
            log.info("Provider Information - SAVE: Ordering First Name --> " + providerInformationFO.getOrderingFirstName());
            log.info("Provider Information - SAVE: Ordering Last Name --> " + providerInformationFO.getOrderingLastName());
            log.info("Provider Information - SAVE: Ordering NPI --> " + providerInformationFO.getOrderingNpi());
            log.info("Provider Information - SAVE: Ordering Phone Number --> " + providerInformationFO.getOrderingPhoneNumber());
            log.info("Provider Information - SAVE: Ordering State --> " + providerInformationFO.getOrderingState());
            log.info("Provider Information - SAVE: Ordering Zip --> " + providerInformationFO.getOrderingZip());
            log.info("Provider Information - SAVE: Ordering Tin Ein --> " + providerInformationFO.getOrderingTinEin());

            log.info("Provider Information - SAVE: Rendering Address Line1 --> " + providerInformationFO.getRenderingAddressLine1());
            log.info("Provider Information - SAVE: Rendering Address Line2 --> " + providerInformationFO.getRenderingAddressLine2());
            log.info("Provider Information - SAVE: Rendering City --> " + providerInformationFO.getRenderingCity());
            log.info("Provider Information - SAVE: Rendering Fax Number --> " + providerInformationFO.getRenderingFaxNumber());
            log.info("Provider Information - SAVE: Rendering Lab Name --> " + providerInformationFO.getRenderingLabName());
            log.info("Provider Information - SAVE: Rendering First Name --> " + providerInformationFO.getRenderingFirstName());
            log.info("Provider Information - SAVE: Rendering Last Name --> " + providerInformationFO.getRenderingLastName());
            log.info("Provider Information - SAVE: Rendering NPI --> " + providerInformationFO.getRenderingNpi());
            log.info("Provider Information - SAVE: Rendering Phone Number --> " + providerInformationFO.getRenderingPhoneNumber());
            log.info("Provider Information - SAVE: Rendering State --> " + providerInformationFO.getRenderingState());
            log.info("Provider Information - SAVE: Rendering Zip --> " + providerInformationFO.getRenderingZip());
            log.info("Provider Information - SAVE: Rendering Tin Ein --> " + providerInformationFO.getRenderingTinEin());

            log.info("Provider Information - SAVE: Id Card Number --> " + memberId);
            log.info("Provider Information - SAVE: Member Number --> " + memberNumber);
            log.info("Provider Information - SAVE: Master Patient Id --> " + mpi);
            log.info("Provider Information - SAVE: Health Plan Group Id --> " + healthPlan);
            log.info("Provider Information - SAVE: Health Plan Id --> " + "01");

            log.info("Provider Information - SAVE: Submission Status Code --> " + submissionStatusCode);

            log.info("Provider Information - SAVE: Member First Name --> " + memberFirstName);
            log.info("Provider Information - SAVE: Member Middle Name --> " + memberMiddleName);
            log.info("Provider Information - SAVE: Member Last Name --> " + memberLastName);
            log.info("Provider Information - SAVE: Member Suffix Name --> " + memberSuffixName);
            log.info("Provider Information - SAVE: Member Date of Birth --> " + memberDob);
            log.info("Provider Information - SAVE: Member Gender Code --> " + memberGenderCode);
            
            log.info("Provider Information - SAVE: Business Sector Code --> " + providerInformationFO.getBusinessSectorCode());
            log.info("Provider Information - SAVE: Business Sector Description --> " + providerInformationFO.getBusinessSectorDescription());
            log.info("Provider Information - SAVE: Business Segment Code --> " + providerInformationFO.getBusinessSegmentCode());
            log.info("Provider Information - SAVE: Business Segment Description --> " + providerInformationFO.getBusinessSegmentDescription());

            // Set the provider Information
            providerDTO.setPriorAuthProviderTypeCode("PA");
            providerDTO.setOrderingAddressLine1(providerInformationFO.getOrderingAddressLine1());
            providerDTO.setOrderingAddressLine2(providerInformationFO.getOrderingAddressLine2());
            providerDTO.setOrderingCity(providerInformationFO.getOrderingCity());
            providerDTO.setOrderingFaxNumber(providerInformationFO.getOrderingFaxNumber());
            providerDTO.setOrderingFirstName(providerInformationFO.getOrderingFirstName());
            providerDTO.setOrderingLastName(providerInformationFO.getOrderingLastName());
            providerDTO.setOrderingNpi(providerInformationFO.getOrderingNpi());
            providerDTO.setOrderingPhoneNumber(providerInformationFO.getOrderingPhoneNumber());
            providerDTO.setOrderingState(providerInformationFO.getOrderingState());
            providerDTO.setOrderingTinEin(providerInformationFO.getOrderingTinEin());
            providerDTO.setOrderingZip(providerInformationFO.getOrderingZip());

            providerDTO.setRenderingAddressLine1(providerInformationFO.getRenderingAddressLine1());
            providerDTO.setRenderingAddressLine2(providerInformationFO.getRenderingAddressLine2());
            providerDTO.setRenderingCity(providerInformationFO.getRenderingCity());
            providerDTO.setRenderingFaxNumber(providerInformationFO.getRenderingFaxNumber());
            providerDTO.setRenderingFirstName(providerInformationFO.getRenderingFirstName());
            providerDTO.setRenderingLabName(providerInformationFO.getRenderingLabName());
            providerDTO.setRenderingLastName(providerInformationFO.getRenderingLastName());
            providerDTO.setRenderingNpi(providerInformationFO.getRenderingNpi());
            providerDTO.setRenderingPhoneNumber(providerInformationFO.getRenderingPhoneNumber());
            providerDTO.setRenderingState(providerInformationFO.getRenderingState());
            providerDTO.setRenderingTinEin(providerInformationFO.getRenderingTinEin());
            providerDTO.setRenderingZip(providerInformationFO.getRenderingZip());
            
            // Set the member data returned from Member Lab Benefits
            priorAuthHeaderDTO.setPriorAuthPatientFirstName(memberFirstName);
            priorAuthHeaderDTO.setPriorAuthPatientMiddleName(memberMiddleName);
            priorAuthHeaderDTO.setPriorAuthPatientLastName(memberLastName);
            priorAuthHeaderDTO.setPriorAuthPatientSuffixName(memberSuffixName);
            priorAuthHeaderDTO.setPriorAuthPatientBirthDate(memberDob);
            priorAuthHeaderDTO.setPriorAuthPatientGenderCode(memberGenderCode);

            // Set the Line of Business data returned from Member Lab Benefits
            priorAuthHeaderDTO.setPriorAuthBusinessSectorCode(businessSectorCode);
            priorAuthHeaderDTO.setPriorAuthBusinessSectorDescription(businessSectorDescription);
            priorAuthHeaderDTO.setPriorAuthBusinessSegmentCode(businessSegmentCode);
            priorAuthHeaderDTO.setPriorAuthBusinessSegmentDescription(businessSegmentDescription);
            
            priorAuthHeaderDTO.setIdCardNumber(memberId);
            priorAuthHeaderDTO.setMemberNumber(memberNumber);
            priorAuthHeaderDTO.setMasterPatientId(mpi);
            priorAuthHeaderDTO.setHealthPlanGroupId(healthPlan);
            priorAuthHeaderDTO.setHealthPlanId("01");

            priorAuthHeaderDTO.setPriorAuthSubmissionStatusCode(submissionStatusCode);
            
            // Set the member data returned from Member Lab Benefits
            priorAuthHeaderDTO.setPriorAuthPatientFirstName(memberFirstName);
            priorAuthHeaderDTO.setPriorAuthPatientMiddleName(memberMiddleName);
            priorAuthHeaderDTO.setPriorAuthPatientLastName(memberLastName);
            priorAuthHeaderDTO.setPriorAuthPatientSuffixName(memberSuffixName);
            priorAuthHeaderDTO.setPriorAuthPatientBirthDate(memberDob);
            priorAuthHeaderDTO.setPriorAuthPatientGenderCode(memberGenderCode);

            // Set the Line of Business data returned from Member Lab Benefits
            priorAuthHeaderDTO.setPriorAuthBusinessSectorCode(businessSectorCode);
            priorAuthHeaderDTO.setPriorAuthBusinessSectorDescription(businessSectorDescription);
            priorAuthHeaderDTO.setPriorAuthBusinessSegmentCode(businessSegmentCode);
            priorAuthHeaderDTO.setPriorAuthBusinessSegmentDescription(businessSegmentDescription);
            
            priorAuthHeaderDTO.setIdCardNumber(memberId);
            priorAuthHeaderDTO.setMemberNumber(memberNumber);
            priorAuthHeaderDTO.setMasterPatientId(mpi);
            priorAuthHeaderDTO.setHealthPlanGroupId(healthPlan);
            priorAuthHeaderDTO.setHealthPlanId("01");

            priorAuthHeader.setProvider(providerDTO);
            priorAuthHeader.setSourceTab(sourceTab);
            savePriorAuthHead.setSavePriorAuthHeaderRequest(priorAuthHeader);
            savePriorAuthHeader.setSavePriorAuthHeader(savePriorAuthHead);
            
            PriorAuthHeaderServiceStub.PriorAuthHeader priorAuthHeaderResponse = null;
           //TODO: Remove mock service code and condition. Don't remove else condition code
			if(GetterUtil.getBoolean(com.liferay.portal.kernel.util.PropsUtil.get(PriorAuthConstants.ENABLED_MOCK_SERVICE))){
				 //Success Mock Code
				
				PriorAuthHeaderServiceStub.SavePriorAuthHeaderResponseE savePriorAuthHeaderResponseE = new PriorAuthHeaderServiceStub.SavePriorAuthHeaderResponseE();
				PriorAuthHeaderServiceStub.SavePriorAuthHeaderResponse savePriorAuthHeaderResponse = new PriorAuthHeaderServiceStub.SavePriorAuthHeaderResponse();;
				
				com.avalon.lbm.services.PriorAuthHeaderServiceStub.PriorAuthHeader priorAuthHeaderObj = new com.avalon.lbm.services.PriorAuthHeaderServiceStub.PriorAuthHeader();
				priorAuthHeaderObj.setAuthorizationKey(10L);
				PriorAuthHeaderDTO priorAuthHeaderDTOObj = new PriorAuthHeaderDTO();
				priorAuthHeaderDTOObj.setPriorAuthStatusCode("PAuthStatusCode");
				priorAuthHeaderDTOObj.setPriorAuthStatusDesc("PAuthStatusDesc");
				priorAuthHeaderDTOObj.setPriorAuthNumber("22");
				priorAuthHeaderObj.setPriorAuthHeaderDTO(priorAuthHeaderDTOObj);
				
				ProviderDTO providerDTOObj = new ProviderDTO();
				providerDTOObj.setAppCreatedDateTime(new Date().toString());
				priorAuthHeaderObj.setProvider(providerDTOObj);
				priorAuthHeaderObj.setPriorAuthHeaderDTO(priorAuthHeaderDTOObj);
				savePriorAuthHeaderResponse.set_return(priorAuthHeaderObj);
				savePriorAuthHeaderResponseE.setSavePriorAuthHeaderResponse(savePriorAuthHeaderResponse);
				
				priorAuthHeaderResponse = savePriorAuthHeaderResponse.get_return();
			}else{
				    PriorAuthHeaderServiceStub.SavePriorAuthHeaderResponseE savePriorAuthHeaderResponseE = priorAuthHeaderServiceStub.savePriorAuthHeader(savePriorAuthHeader);
		            PriorAuthHeaderServiceStub.SavePriorAuthHeaderResponse savePriorAuthHeaderResponse = savePriorAuthHeaderResponseE.getSavePriorAuthHeaderResponse();
		            priorAuthHeaderResponse = savePriorAuthHeaderResponse.get_return();
		    }
			portletSession.setAttribute("authorizationKey", (Object) priorAuthHeaderResponse.getAuthorizationKey(), PortletSession.APPLICATION_SCOPE);
            String authNumber = priorAuthHeaderResponse.getPriorAuthHeaderDTO().getPriorAuthNumber();
            request.setAttribute("authCreated", (Object) authNumber);
            portletSession.setAttribute("authorizationNumber", (Object) authNumber, PortletSession.APPLICATION_SCOPE);
            httpRequest.setAttribute("enableTabs", (Object) "true");
            request.setAttribute("enableTabs", (Object) "true");
            String appDate = priorAuthHeaderResponse.getProvider().getAppCreatedDateTime();
            
            log.info("Provider Information - SAVE: Authorization Status --> " + priorAuthHeaderResponse.getPriorAuthHeaderDTO().getPriorAuthStatusCode());
            portletSession.setAttribute("providerAuthStatus", (Object) priorAuthHeaderResponse.getPriorAuthHeaderDTO().getPriorAuthStatusCode(), PortletSession.APPLICATION_SCOPE);
            currentAuthorizationDetails.setAuthorizationStatus(priorAuthHeaderResponse.getPriorAuthHeaderDTO().getPriorAuthStatusDesc());
            currentAuthorizationDetails.setAuthorizationAge("0:0:0");
            request.setAttribute("providerInformationFO", (Object) providerInformationFO);
            request.setAttribute("currentAuthorizationDetailsFO", (Object) currentAuthorizationDetails);
            
            log.info("Processing SAVE Action for Provider Information Action Exit");
       } catch (RemoteException e) {
        	log.error("RemoteException in handleActionRequest method",e);
       } catch (DAOExceptionException e) {
        	log.error("DAOExceptionException in handleActionRequest method",e);
       }
    }
    
    /**************Action method which gets called on click of SAVE button for redirection of page from provider information to procedure information***************************************/
    @ActionMapping(params = { "authRaction=authRedirectAction" })
    public void authRedirectAction(ActionRequest request, 
    		                       ActionResponse response) {
    
    	log.info("Processing REDIRECT Action for Information Action Enter");
        ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute("LIFERAY_SHARED_THEME_DISPLAY");
        try {
            Layout layout = LayoutLocalServiceUtil.getFriendlyURLLayout((long) themeDisplay.getLayout().getGroupId(), (boolean) true, (String) PortletProps.get((String) "prior-auth-page-url"));
            LiferayPortletURL portletURL = PortletURLFactoryUtil.create((PortletRequest) request, (String) PortletProps.get((String) "procedure-info-page"), (long) layout.getPlid(),
                    (String) "RENDER_PHASE");
            portletURL.setWindowState(WindowState.MAXIMIZED);
            portletURL.setPortletMode(PortletMode.VIEW);
            log.info("URL for redirecting to procedure information page:" + portletURL.toString());
            response.sendRedirect(portletURL.toString());
        } catch (PortalException e) {
        	log.error("PortalException in authRedirectAction method",e);
        } catch (SystemException e) {
        	log.error("SystemException in authRedirectAction method",e);
        } catch (WindowStateException e) {
        	log.error("WindowStateException in authRedirectAction method",e);
        } catch (PortletModeException e) {
        	log.error("PortletModeException in authRedirectAction method",e);
        } catch (IOException e) {
        	log.error("IOException in authRedirectAction method",e);
        }
        log.info("Processing REDIRECT Action for Provider Information Action Exit");
    }

    
    @ActionMapping
    public void providerHandler() {
        log.info("prov handler");
    }
    
    /**************Action method which gets called on click of CANCEL button***************************************/
    @ActionMapping(params = { "cancelaction=providerInfoCancelAction" })
    public void cancelPage(ActionRequest actionRequest, 
    		               ActionResponse actionResponse) {

    	log.info("Processing Cancel Action for Provider Information Entry Enter");
	    String userName = null;
	    List usersList = null;
		try {
		    User user = PortalUtil.getUser((PortletRequest) actionRequest);
		    if (user != null) {
			    usersList = user.getUserGroups();
		    }
		} catch (PortalException e) {
			log.error("PortalException in cancelPage method",e);
		} catch (SystemException e) {
			log.error("SystemException in cancelPage method",e);
		}
	    ArrayList<String> groupNames = new ArrayList<String>();
	    if (usersList != null) {
	    	int length = usersList.size();
		    for (int i = 0; i < length; ++i) {
		    	
		    	// Ignore the groups that do not have a home page
				if (!((UserGroup)usersList.get(i)).getName().equalsIgnoreCase("CDSUsers") &&
					!((UserGroup)usersList.get(i)).getName().equalsIgnoreCase("Everyone") &&
					!((UserGroup)usersList.get(i)).getName().equalsIgnoreCase("ManagedUsers") &&
					!((UserGroup)usersList.get(i)).getName().equalsIgnoreCase("Multi-Factor") &&
					!((UserGroup)usersList.get(i)).getName().equalsIgnoreCase("PortalAdmin") &&
					!((UserGroup)usersList.get(i)).getName().equalsIgnoreCase("UMGroup")) {
					userName = ((UserGroup) usersList.get(i)).getName();
					groupNames.add(userName);
				}
		    }
	    }
	    HttpServletRequest request = PortalUtil.getHttpServletRequest((PortletRequest) actionRequest);
	    String path = PortalUtil.getCurrentCompleteURL((HttpServletRequest) request);
	    StringBuilder output = new StringBuilder();
	    int count = 0;
	    char[] ch = path.toCharArray();
	    for (int i2 = 0; i2 < ch.length; ++i2) {
			if (ch[i2] == '/') {
			    ++count;
			}
			if (count >= 3)
			    continue;
			output = output.append(ch[i2]);
	    }

	    String landingPage = null;
	    if (usersList == null) {
			
			// Set to the default landing page since the user does not belong to any groups
			landingPage = "/web/guest/home";
	    } else {
	    	

	    	// Get the landing page based on the user's group
			landingPage = com.liferay.portal.kernel.util.PropsUtil.get("default.landing.page.path[" + userName  + "]"); 
			
			// If the user's group is not found use the defaule landing page
			if (landingPage == null) {
				landingPage = com.liferay.portal.kernel.util.PropsUtil.get("default.landing.page.path");
				if (landingPage == null) {
					
					// Set to the default landing page since it is not defined in the properties file
					landingPage = "/web/guest/home";
				}
			}
	    }
	    String pathRedirect = null;
		pathRedirect = output + landingPage;
		log.info(userName + " " + pathRedirect);
    	log.info("Processing Cancel Action for Provider Information Exit");
		try {
		    actionResponse.sendRedirect(pathRedirect);
		} catch (IOException e) {
			log.error("IOException during redirection in cancelPage method", e);
		}
    }
    
    /**
     * TODO: Remove below success mock code
     * @return
     */
    private PriorAuthHeaderServiceStub.GetPriorAuthHeaderResponse getPriorAuthHeaderResponseSuccessMock(){
    	
    	PriorAuthHeaderServiceStub.GetPriorAuthHeaderResponse getPriorAuthHeaderResponse = new PriorAuthHeaderServiceStub.GetPriorAuthHeaderResponse();
		
		PriorAuthHeaderServiceStub.PriorAuthHeaderDTO priorAuthHeaderDTO = new PriorAuthHeaderServiceStub.PriorAuthHeaderDTO();
		priorAuthHeaderDTO.setAppCreateDatetime("2018-04-26 01:02:00");
		priorAuthHeaderDTO.setIdCardNumber("22");
		priorAuthHeaderDTO.setPriorAuthSubmissionStatusCode("SubCode");
		priorAuthHeaderDTO.setPriorAuthPatientFirstName("PatientFirstName");
		priorAuthHeaderDTO.setPriorAuthPatientMiddleName("PatientMiddleName");
		priorAuthHeaderDTO.setPriorAuthPatientLastName("PatientLastName");
		priorAuthHeaderDTO.setPriorAuthPatientBirthDate("1999-04-26 01:02:00");
		priorAuthHeaderDTO.setPriorAuthPatientGenderCode("01");
		priorAuthHeaderDTO.setPriorAuthBusinessSectorCode("SecCode");
		priorAuthHeaderDTO.setPriorAuthBusinessSectorDescription("SecDesc");
		priorAuthHeaderDTO.setPriorAuthBusinessSegmentCode("SegCode");
		priorAuthHeaderDTO.setPriorAuthBusinessSegmentDescription("SegDesc");
		priorAuthHeaderDTO.setPriorAuthNumber("1111011111");
		priorAuthHeaderDTO.setPriorAuthStatusDesc("StatusDesc");
		PriorAuthHeaderServiceStub.ProviderDTO providerDTO = new PriorAuthHeaderServiceStub.ProviderDTO();
		providerDTO.setAppCreatedDateTime("2018-04-26 01:02:00");
		providerDTO.setAppCreatedUserId("CreatedUserId");
		providerDTO.setAppMantDateTime("2018-04-26 01:02:00f");
		providerDTO.setAppMantUserId("AppMantUserId");
		providerDTO.setOrderingAddressLine1("Add1");
		providerDTO.setOrderingAddressLine2("Add2");
		providerDTO.setOrderingCity("TestCity");
		providerDTO.setOrderingFaxNumber("1234567890");
		providerDTO.setOrderingFirstName("OFirst");
		providerDTO.setOrderingLastName("OLast");
		providerDTO.setOrderingNpi("1111111111");
		providerDTO.setOrderingPhoneNumber("0987654321");
		providerDTO.setOrderingState("Kensas");
		providerDTO.setOrderingZip("32001");
		providerDTO.setPriorAuthHeaderKey(01);
		providerDTO.setPriorAuthProviderTypeCode("ProviderCode");
		providerDTO.setRenderingAddressLine1("RenAdd1");
		providerDTO.setRenderingAddressLine2("RenAdd2");
		providerDTO.setRenderingCity("RedCity");
		providerDTO.setRenderingFaxNumber("9199129191");
		providerDTO.setRenderingFirstName("Rfirst");
		providerDTO.setRenderingLabName("RLab");
		providerDTO.setRenderingLastName("RLast");
		providerDTO.setRenderingNpi("1111111111");
		providerDTO.setRenderingPhoneNumber("9191919191");
		providerDTO.setRenderingState("RState");
		providerDTO.setRenderingZip("90800");
		
		PriorAuthHeader priorAuthHeaderObj = new PriorAuthHeader();
		priorAuthHeaderObj.setPriorAuthHeaderDTO(priorAuthHeaderDTO);
		priorAuthHeaderObj.setProvider(providerDTO);
		getPriorAuthHeaderResponse.set_return(priorAuthHeaderObj);
		return getPriorAuthHeaderResponse;
    }
}
