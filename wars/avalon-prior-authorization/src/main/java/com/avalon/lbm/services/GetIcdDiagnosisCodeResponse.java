
package com.avalon.lbm.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getIcdDiagnosisCodeResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getIcdDiagnosisCodeResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="return" type="{http://services.lbm.avalon.com/}TrialClaimsDiagnosisReqHdr" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getIcdDiagnosisCodeResponse", propOrder = {
    "_return"
})
public class GetIcdDiagnosisCodeResponse {

    @XmlElement(name = "return")
    protected TrialClaimsDiagnosisReqHdr _return;

    /**
     * Gets the value of the return property.
     * 
     * @return
     *     possible object is
     *     {@link TrialClaimsDiagnosisReqHdr }
     *     
     */
    public TrialClaimsDiagnosisReqHdr getReturn() {
        return _return;
    }

    /**
     * Sets the value of the return property.
     * 
     * @param value
     *     allowed object is
     *     {@link TrialClaimsDiagnosisReqHdr }
     *     
     */
    public void setReturn(TrialClaimsDiagnosisReqHdr value) {
        this._return = value;
    }

}
