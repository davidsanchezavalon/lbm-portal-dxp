package com.avalon.lbm.portlets.priorauth.model;

/**
 * Description
 *		This file contain the getters and setters methods for the Procedure Information Page.
 *
 * CHANGE History
 * 		Version 1.0
 * 			Initial version
 * 		Version 1.1
 * 			Changed the aging start date to the request date.  If it is not set use the creation date.
 * 			Changed the aging end date to the date the letter was sent.  If it is not set use the current date.
 * 		Version 1.2
 *			Added new fields: Withdrawn Reason, Being Worked By, Due Date, and Due Time.
 *  
 */

import java.io.Serializable;
import java.util.List;

public class ProcedureInformationFO implements Serializable {
	
	
	/**
	 * To store the procedure information fields
	 */
	private static final long serialVersionUID = -2500508813945643931L;

	private String authInboundChannel;
	private String inboundChannel;
	private String requestDateTime;
	private String authRecordCreator;
	private String creationDate;
	private String authRequestedTime;
	private String submitButtonStatus;
	private String submissionStatus;
	private String priorityKey;
	private String priorityValue;
	private String serviceBeginDate;
	private String serviceEndDate;
	private String authorizationStatus;
	private String originalDecisionDate;
	private String primaryDiagnosis;
	private List<String> additionalDiagnosis;
	private String fromProcedureCode;
	private String toProcedureCode;
	private String procedureDecision;
	private String decisionReason;
	private String units;
	private String authRequestedDate;
	private String authCreatedDate;
	private String authPriority;
	private String authSubmissionStatus;
	private String serviceStartDate;
	private String authStatusDesc;
	private String authStatusCode;
	private String authWithdrawnReasonDesc;
	private String authWithdrawnReasonCode;
	private String authDecisionDate;
	private String authDecisionTime;
	private String diagnosisCodePrimary;
	private String authDueDate;
	private String authDueTime;
	private String authWorkedBy;

	public String getAuthInboundChannel() {
		return authInboundChannel;
	}
	public void setAuthInboundChannel(String authInboundChannel) {
		this.authInboundChannel = authInboundChannel;
	}

	public String getInboundChannel() {
		return inboundChannel;
	}
	public void setInboundChannel(String inboundChannel) {
		this.inboundChannel = inboundChannel;
	}

	public String getRequestDateTime() {
		return requestDateTime;
	}
	public void setRequestDateTime(String requestDateTime) {
		this.requestDateTime = requestDateTime;
	}

	public String getAuthRecordCreator() {
		return authRecordCreator;
	}
	public void setAuthRecordCreator(String authRecordCreator) {
		this.authRecordCreator = authRecordCreator;
	}

	public String getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}

	public String getAuthRequestedTime() {
		return authRequestedTime;
	}
	public void setAuthRequestedTime(String authRequestedTime) {
		this.authRequestedTime = authRequestedTime;
	}

	public String getSubmitButtonStatus() {
		return submitButtonStatus;
	}
	public void setSubmitButtonStatus(String submitButtonStatus) {
		this.submitButtonStatus = submitButtonStatus;
	}

	public String getSubmissionStatus() {
		return submissionStatus;
	}
	public void setSubmissionStatus(String submissionStatus) {
		this.submissionStatus = submissionStatus;
	}

	public String getPriorityKey() {
		return priorityKey;
	}
	public void setPriorityKey(String priorityKey) {
		this.priorityKey = priorityKey;
	}

	public String getPriorityValue() {
		return priorityValue;
	}
	public void setPriorityValue(String priorityValue) {
		this.priorityValue = priorityValue;
	}

	public String getServiceBeginDate() {
		return serviceBeginDate;
	}
	public void setServiceBeginDate(String serviceBeginDate) {
		this.serviceBeginDate = serviceBeginDate;
	}

	public String getServiceEndDate() {
		return serviceEndDate;
	}
	public void setServiceEndDate(String serviceEndDate) {
		this.serviceEndDate = serviceEndDate;
	}

	public String getAuthorizationStatus() {
		return authorizationStatus;
	}
	public void setAuthorizationStatus(String authorizationStatus) {
		this.authorizationStatus = authorizationStatus;
	}

	public String getOriginalDecisionDate() {
		return originalDecisionDate;
	}
	public void setOriginalDecisionDate(String originalDecisionDate) {
		this.originalDecisionDate = originalDecisionDate;
	}

	public String getPrimaryDiagnosis() {
		return primaryDiagnosis;
	}
	public void setPrimaryDiagnosis(String primaryDiagnosis) {
		this.primaryDiagnosis = primaryDiagnosis;
	}

	public List<String> getAdditionalDiagnosis() {
		return additionalDiagnosis;
	}
	public void setAdditionalDiagnosis(List<String> additionalDiagnosis) {
		this.additionalDiagnosis = additionalDiagnosis;
	}

	public String getFromProcedureCode() {
		return fromProcedureCode;
	}
	public void setFromProcedureCode(String fromProcedureCode) {
		this.fromProcedureCode = fromProcedureCode;
	}

	public String getToProcedureCode() {
		return toProcedureCode;
	}
	public void setToProcedureCode(String toProcedureCode) {
		this.toProcedureCode = toProcedureCode;
	}

	public String getProcedureDecision() {
		return procedureDecision;
	}
	public void setProcedureDecision(String procedureDecision) {
		this.procedureDecision = procedureDecision;
	}

	public String getDecisionReason() {
		return decisionReason;
	}
	public void setDecisionReason(String decisionReason) {
		this.decisionReason = decisionReason;
	}

	public String getUnits() {
		return units;
	}
	public void setUnits(String units) {
		this.units = units;
	}

	public String getAuthRequestedDate() {
		return authRequestedDate;
	}
	public void setAuthRequestedDate(String authRequestedDate) {
		this.authRequestedDate = authRequestedDate;
	}

	public String getAuthCreatedDate() {
		return authCreatedDate;
	}
	public void setAuthCreatedDate(String authCreatedDate) {
		this.authCreatedDate = authCreatedDate;
	}
	
	public String getAuthPriority() {
		return authPriority;
	}
	public void setAuthPriority(String authPriority) {
		this.authPriority = authPriority;
	}

	public String getAuthSubmissionStatus() {
		return authSubmissionStatus;
	}
	public void setAuthSubmissionStatus(String authSubmissionStatus) {
		this.authSubmissionStatus = authSubmissionStatus;
	}
	
	public String getServiceStartDate() {
		return serviceStartDate;
	}
	public void setServiceStartDate(String serviceStartDate) {
		this.serviceStartDate = serviceStartDate;
	}
	
	public String getAuthStatusDesc() {
		return authStatusDesc;
	}
	public void setAuthStatusDesc(String authStatusDesc) {
		this.authStatusDesc = authStatusDesc;
	}
	
	public String getAuthStatusCode() {
		return authStatusCode;
	}
	public void setAuthStatusCode(String authStatusCode) {
		this.authStatusCode = authStatusCode;
	}
	
	public String getAuthWithdrawnReasonDesc() {
		return authWithdrawnReasonDesc;
	}
	public void setAuthWithdrawnReasonDesc(String authWithdrawnReasonDesc) {
		this.authWithdrawnReasonDesc = authWithdrawnReasonDesc;
	}
	
	public String getAuthWithdrawnReasonCode() {
		return authWithdrawnReasonCode;
	}
	public void setAuthWithdrawnReasonCode(String authWithdrawnReasonCode) {
		this.authWithdrawnReasonCode = authWithdrawnReasonCode;
	}
	
	public String getAuthDecisionDate() {
		return authDecisionDate;
	}
	public void setAuthDecisionDate(String authDecisionDate) {
		this.authDecisionDate = authDecisionDate;
	}

	public String getAuthDecisionTime() {
		return authDecisionTime;
	}
	public void setAuthDecisionTime(String authDecisionTime) {
		this.authDecisionTime = authDecisionTime;
	}

	public String getDiagnosisCodePrimary() {
		return diagnosisCodePrimary;
	}
	public void setDiagnosisCodePrimary(String diagnosisCodePrimary) {
		this.diagnosisCodePrimary = diagnosisCodePrimary;
	}
	
	public String getAuthDueDate() {
		return authDueDate;
	}
	public void setAuthDueDate(String authDueDate) {
		this.authDueDate = authDueDate;
	}
	
	public String getAuthDueTime() {
		return authDueTime;
	}
	public void setAuthDueTime(String authDueTime) {
		this.authDueTime = authDueTime;
	}
	
	public String getAuthWorkedBy() {
		return authWorkedBy;
	}
	public void setAuthWorkedBy(String authWorkedBy) {
		this.authWorkedBy = authWorkedBy;
	}

}
