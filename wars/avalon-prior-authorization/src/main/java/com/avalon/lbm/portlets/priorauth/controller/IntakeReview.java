/**
 * Description
 *		This file contain the controller methods for the PA Intake Review Page.
 *
 * CHANGE History
 * 		Version 1.0
 * 			Initial version
 * 		Version 1.1
 * 			Changed the aging start date to the request date.  If it is not set use the creation date.
 * 			Changed the aging end date to the date the letter was sent.  If it is not set use the current date.
 * 		Version 1.2					08/15/2018
 * 			Added codify changes.
 *  
 */

package com.avalon.lbm.portlets.priorauth.controller;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletRequest;
import javax.portlet.PortletSession;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import com.avalon.lbm.portlets.priorauth.model.CurrentAuthorizationDetailsFO;
import com.avalon.lbm.portlets.priorauth.model.IntakeReviewFO;
import com.avalon.lbm.portlets.priorauth.model.PriorAuthConstants;
import com.avalon.lbm.portlets.priorauth.util.AgeCalculation;
import com.avalon.lbm.portlets.priorauth.util.PriorAuthUtil;
import com.avalon.lbm.services.PriorAuthReviewServiceStub;
import com.avalon.lbm.services.PriorAuthReviewServiceStub.PriorAuthHeader;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.ParamUtil;

@Controller(value = "IntakeReview")
@RequestMapping(value = { "VIEW" })
public class IntakeReview {
	
	private static final Log log = LogFactoryUtil.getLog(IntakeReview.class.getName());

    /**** Render method which gets called during initial load of JSP *************************/
    @RenderMapping
    public String handleRenderRequest(RenderRequest request, 
    		                          RenderResponse response, 
    		                          Model model) throws RemoteException {

	    log.info("Processing Render Action for Intake Review Enter");
	   	String sourceTab = PriorAuthUtil.intakeReviewTab;
        PortletSession portletSession = request.getPortletSession();

        Long authorizationKey = (Long) portletSession.getAttribute("authorizationKey", PortletSession.APPLICATION_SCOPE);
        log.info("(render) Authorization Key " + authorizationKey);

        CurrentAuthorizationDetailsFO currentAuthorizationDetails = PriorAuthUtil.getHeaderDetails(portletSession, sourceTab);

        // Set the authorization age
        ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute("LIFERAY_SHARED_THEME_DISPLAY");
        String appDate = (String) portletSession.getAttribute("CreateDatetime", PortletSession.APPLICATION_SCOPE);
        currentAuthorizationDetails.setAuthorizationAge(PriorAuthUtil.setAuthorizationAge(portletSession, appDate, authorizationKey, themeDisplay));

        request.setAttribute("currentAuthorizationDetailsFO", (Object) currentAuthorizationDetails);

        if (authorizationKey == null) {
            log.info("(render) Authorization key is null");
        } else {
            PriorAuthReviewServiceStub priorAuthReviewServiceStub = new PriorAuthReviewServiceStub();
            PriorAuthReviewServiceStub.SearchPriorAuthNoteDetails searchPriorAuthNoteDetails = new PriorAuthReviewServiceStub.SearchPriorAuthNoteDetails();
            PriorAuthReviewServiceStub.NoteSearchCriteria noteSearchCriteria = new PriorAuthReviewServiceStub.NoteSearchCriteria();
            noteSearchCriteria.setAuthorizationKey(authorizationKey.longValue());
            noteSearchCriteria.setSourceTab(sourceTab);
            searchPriorAuthNoteDetails.setSearchCriteriaNoteRequest(noteSearchCriteria);
            PriorAuthReviewServiceStub.SearchPriorAuthNoteDetailsE searchPriorAuthNoteDetailsE = new PriorAuthReviewServiceStub.SearchPriorAuthNoteDetailsE();
            searchPriorAuthNoteDetailsE.setSearchPriorAuthNoteDetails(searchPriorAuthNoteDetails);
            
            PriorAuthReviewServiceStub.SearchPriorAuthNoteDetailsResponseE searchPriorAuthNoteDetailsResponseE = null;
            PriorAuthReviewServiceStub.SearchPriorAuthNoteDetailsResponse searchPriorAuthNoteDetailsResponse = null;
           
            //TODO: Remove mock service code and condition. Don't remove else condition code
			if(GetterUtil.getBoolean(com.liferay.portal.kernel.util.PropsUtil.get(PriorAuthConstants.ENABLED_MOCK_SERVICE))){
				 //Success Mock Code
				 searchPriorAuthNoteDetailsResponseE = new PriorAuthReviewServiceStub.SearchPriorAuthNoteDetailsResponseE();
		         searchPriorAuthNoteDetailsResponse = new PriorAuthReviewServiceStub.SearchPriorAuthNoteDetailsResponse();
		         PriorAuthHeader priorAuthHeader = new PriorAuthHeader();
		         priorAuthHeader.setAuthorizationKey(100L);
			     priorAuthHeader.setNoteCreateDatetime("2015-12-12 12:12:22.222");
			     priorAuthHeader.setNoteCreateUserId("UserId");
			     priorAuthHeader.setPriorAuthNoteText("Notetext");
			     priorAuthHeader.setPriorAuthNoteTypeCode("AuthType");
			     priorAuthHeader.setSourceTab("STab");
		         searchPriorAuthNoteDetailsResponse.set_return(new PriorAuthHeader[]{priorAuthHeader});
		         searchPriorAuthNoteDetailsResponseE.setSearchPriorAuthNoteDetailsResponse(searchPriorAuthNoteDetailsResponse);
		    }else{
				 searchPriorAuthNoteDetailsResponseE = priorAuthReviewServiceStub.searchPriorAuthNoteDetails(searchPriorAuthNoteDetailsE);
		         searchPriorAuthNoteDetailsResponse = searchPriorAuthNoteDetailsResponseE.getSearchPriorAuthNoteDetailsResponse();
		    }
            IntakeReviewFO intakeReviewFO = new IntakeReviewFO();
            ArrayList<String> intakeEnNotes = new ArrayList<String>();
            if (searchPriorAuthNoteDetailsResponse.get_return() != null) {
                for (int i = 0; i < searchPriorAuthNoteDetailsResponse.get_return().length; ++i) {
                    String creationDateTimeIn = searchPriorAuthNoteDetailsResponse.get_return()[i].getNoteCreateDatetime();
                    log.info("(render) Creation Date Time of Intake: " + creationDateTimeIn);
                    String frmDateStr = AgeCalculation.getAgeCalculation().getTimeForNote(creationDateTimeIn);
                    
                    // Do not add a blank note
                    if (!searchPriorAuthNoteDetailsResponse.get_return()[i].getPriorAuthNoteText().equals("")){
	                    intakeEnNotes.add(searchPriorAuthNoteDetailsResponse.get_return()[i].getNoteCreateUserId() + "(" + frmDateStr + ")" + " : "
	                            + searchPriorAuthNoteDetailsResponse.get_return()[i].getPriorAuthNoteText());
	                    log.info("(render) Intake note " + (i + 1) + ": " + searchPriorAuthNoteDetailsResponse.get_return()[i].getPriorAuthNoteText());
                    }
                }

                // Get the checks and HP Representative
                Object priorAuthEligibilityBenefitCheck = (Object) searchPriorAuthNoteDetailsResponse.get_return()[0].getPriorAuthEligibilityBenefitCheck();
                Object priorAuthHpRepresentative = (Object) searchPriorAuthNoteDetailsResponse.get_return()[0].getPriorAuthHpRepresentative();
                Object priorAuthProviderCheck = (Object) searchPriorAuthNoteDetailsResponse.get_return()[0].getPriorAuthProviderCheck();
                Object priorAuthCompletedBy = (Object) searchPriorAuthNoteDetailsResponse.get_return()[0].getPriorAuthCompletedByUserId();

                // Test for null
                if (priorAuthEligibilityBenefitCheck == null) priorAuthEligibilityBenefitCheck = "";
                if (priorAuthHpRepresentative == null) priorAuthHpRepresentative = "";
                if (priorAuthProviderCheck == null) priorAuthProviderCheck = "";
                if (priorAuthCompletedBy == null) priorAuthCompletedBy = "";
                
                request.setAttribute("priorAuthEligibilityBenefitCheck", priorAuthEligibilityBenefitCheck);
				log.info("(render) priorAuthEligibilityBenefitCheck: " + (String) priorAuthEligibilityBenefitCheck);
                request.setAttribute("priorAuthHpRepresentative", priorAuthHpRepresentative);
				log.info("(render) priorAuthHpRepresentative: " + (String) priorAuthHpRepresentative);
                request.setAttribute("priorAuthProviderCheck", priorAuthProviderCheck);
				log.info("(render) priorAuthProviderCheck: " + (String) priorAuthProviderCheck);
                request.setAttribute("priorAuthCompletedBy", priorAuthCompletedBy);
				log.info("(render) priorAuthCompletedBy: " + (String) priorAuthCompletedBy);

                portletSession.setAttribute("intakeEnNotes", intakeEnNotes, PortletSession.APPLICATION_SCOPE);
                intakeReviewFO.setIntakeNotesRead(intakeEnNotes);
                List readData = intakeReviewFO.getIntakeNotesRead();
                request.setAttribute("readData", (Object) readData);
				log.info("(render) readData: " + readData);
				
                request.setAttribute("intakeReviewFO", (Object) intakeReviewFO);
            }
        }
	    log.info("Processing Render Action for Intake Review Exit");
        return "view";
    }

    /**************Action method which gets called on click of SAVE button***************************************/
    @ActionMapping(params = { "action=intakeReviewAction" })
    public void intakeReviewDetails(ActionRequest request, 
    		                        ActionResponse response, 
    		                        @ModelAttribute(value = "intakeReviewFO") IntakeReviewFO intakeReviewFO) throws RemoteException {

		log.info("Processing Save Action for Intake Review Enter");
		
	   	String sourceTab = PriorAuthUtil.intakeReviewTab;
	   	String noteType = PriorAuthUtil.intakeReviewNote;
        ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute("LIFERAY_SHARED_THEME_DISPLAY");
        PortletSession portletSession = request.getPortletSession();

		if(GetterUtil.getBoolean(com.liferay.portal.kernel.util.PropsUtil.get(PriorAuthConstants.ENABLED_MOCK_SERVICE))){
		}else{
	        Long authorizationKey = (Long) portletSession.getAttribute("authorizationKey", PortletSession.APPLICATION_SCOPE);
	        String creationDate = AgeCalculation.getAgeCalculation().createDate();
	        
	        PriorAuthReviewServiceStub priorAuthReviewServiceStub = new PriorAuthReviewServiceStub();
	        PriorAuthReviewServiceStub.CreatePriorAuthNoteDetails createPriorAuthNoteDetails = new PriorAuthReviewServiceStub.CreatePriorAuthNoteDetails();
	        PriorAuthReviewServiceStub.CreatePriorAuthNoteDetailsE CreatePriorAuthNoteDetailsE2 = new PriorAuthReviewServiceStub.CreatePriorAuthNoteDetailsE();
	        PriorAuthReviewServiceStub.PriorAuthHeader priorAuthHeader = new PriorAuthReviewServiceStub.PriorAuthHeader();
	        String inEnNotes = ParamUtil.getString((PortletRequest) request, (String) "intakeNotesCreate");
	        if (inEnNotes != null && !inEnNotes.equals("")) {
	            log.info("Intake Notes: " + inEnNotes);
	            priorAuthHeader.setPriorAuthNoteText(inEnNotes.toString());
	        }
	        
	        // Get the claim and document numbers
	        String eligibilityBenefitCheck = ParamUtil.getString((PortletRequest) request, (String) "eligibilityBenefitCheck");
	        String hpRepresentative = ParamUtil.getString((PortletRequest) request, (String) "hpRepresentative");
	        String providerCheck = ParamUtil.getString((PortletRequest) request, (String) "providerCheck");
	        String completedBy = ParamUtil.getString((PortletRequest) request, (String) "hiddencompletedBy");
	        log.info("Eligibility Benefit Check: " + eligibilityBenefitCheck);
	        log.info("HP Representative: " + hpRepresentative);
	        log.info("Provider Check: " + providerCheck);
	        log.info("Completed By: " + completedBy);
	        
	    	priorAuthHeader.setPriorAuthEligibilityBenefitCheck(eligibilityBenefitCheck);
	     	priorAuthHeader.setPriorAuthHpRepresentative(hpRepresentative);
	    	priorAuthHeader.setPriorAuthProviderCheck(providerCheck);
	    	priorAuthHeader.setPriorAuthCompletedByUserId(completedBy);
	    	
	        priorAuthHeader.setPriorAuthNoteTypeCode(noteType);
	        priorAuthHeader.setSourceTab(sourceTab);
	        priorAuthHeader.setAuthorizationKey(authorizationKey.longValue());
	        priorAuthHeader.setNoteCreateUserId(themeDisplay.getUser().getEmailAddress());
	        priorAuthHeader.setNoteCreateDatetime(creationDate);
	        createPriorAuthNoteDetails.setPriorAuthNoteRequest(priorAuthHeader);
	        CreatePriorAuthNoteDetailsE2.setCreatePriorAuthNoteDetails(createPriorAuthNoteDetails);
			PriorAuthReviewServiceStub.CreatePriorAuthNoteDetailsResponseE createPriorAuthNoteDetailsResponseE = priorAuthReviewServiceStub.createPriorAuthNoteDetails(CreatePriorAuthNoteDetailsE2);
			Long longKey = createPriorAuthNoteDetailsResponseE.getCreatePriorAuthNoteDetailsResponse().get_return().getAuthorizationKey();
		}
	    log.info("Processing Save Action for Intake Review Exit");
    }
    
    @ActionMapping
    public void IntakeHandler() {
        log.info("Intake Review Handler");
    }
}