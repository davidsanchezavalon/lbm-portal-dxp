/**
 * Description
 *		This file methods to get values from the properties file for Prior Authorization utility.
 *
 * CHANGE History
 * 		Version 1.0
 * 			Initial version copied from avalon-prior-authorization.
 *  	Version 1.1			03/07/2017
 *  		Use the constant for the properties file.
 *  
 */

package com.avalon.esb.servicehelpers;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;

import com.avalon.lbm.portlets.priorauth.model.PriorAuthConstants;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.StringPool;

public class EsbUrlDelegateTrialClaim {
	
	private static final Log log = LogFactoryUtil.getLog(EsbUrlDelegateTrialClaim.class.getName());

    private static EsbUrlDelegateTrialClaim esbUrlDelegate = null;
    private static Properties properties = null;

    public static EsbUrlDelegateTrialClaim getEsbUrlDelegate() {
        return esbUrlDelegate;
    }

    public Map<String, String> getEsbUrl() {
		ConcurrentHashMap<String, String> urls = new ConcurrentHashMap<String, String>();
		try {
		    String serverInstance = System.getProperty("serverInstance");
		    log.info("ServerInstanceAtEsbDelegate :" + serverInstance);
		    log.info("researchDecisionUrl :" + properties.getProperty(new StringBuilder().append(serverInstance).append(".getResearchDecision.endpoint").toString()));
		    log.info("claimEditorUrl :" + properties.getProperty(new StringBuilder().append(serverInstance).append(".getClaimEditor.endpoint").toString()));
		    urls.put("researchDecisionUrl", properties.getProperty(serverInstance + ".getResearchDecision.endpoint"));
		    urls.put("claimEditorUrl", properties.getProperty(serverInstance + ".getClaimEditor.endpoint"));
		    urls.put("trailClaimsPriorAuthUrl", properties.getProperty(serverInstance + ".getPriorAuth.endpoint"));
		} catch (Exception exception) {
			log.error("Exception in getEsbUrl method",exception);
		}
		return urls;
    }

    static {
		if (properties == null) {
		    properties = new Properties();
		    esbUrlDelegate = new EsbUrlDelegateTrialClaim();
		    String liferayHome = System.getProperty("LIFERAY_HOME");
		    String propertyLocation = liferayHome + StringPool.SLASH + PriorAuthConstants.PROPERTIES_FILE;
		    try {
				properties.load(new FileInputStream(new File(propertyLocation)));
				log.info("EsbUrlDelegateTrialClaim for PA::propertyLocation: " + propertyLocation);
		    } catch (FileNotFoundException exception) {
		    	log.error("FileNotFoundException in static block",exception);
		    } catch (IOException exception) {
		    	log.error("IOException in static block",exception);
		    }
		}
    }
}
