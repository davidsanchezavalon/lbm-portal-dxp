<%--
/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

/**
  * Description
  *		This file contain the Peer-to-Peer Review portlet for Prior Auth.
  *
  * CHANGE History
  * 	Version 1.0
  *			All of the changes have been merged.
  * 		This file includes all of the field validation, the busy icon, and the check for leaving the page without saving changes.
  * 	Version 1.2
  *			Fixed ticket LJR-374.
  *		version 1.3
  *			Splitting into two forms
  * 	Version 1.4  
  *			Fixed ticket LJR-383.
  *		Version 1.5  
  *			Two forms needs for this page.
  * 	Version 1.6  
  *			Fixed date fields for IE and Firefox.
  * 	Version 1.7  
  *			Revised to one save button.
  *			Added isValidForm function.
  * 	Version 1.8  
  *			Make Physician Name, Date Sent and Time Sent required fields.
  * 	Version 1.9  
  *			Treat Returned Data and Returned Time as a group. If one is set, both must be set.
  *	    Version 1.10
  *			Disallowing users to type any text in the datetime fields
  *	    Version 1.11
  *			Added validity of the html5 date before save.
  *	    Version 1.12
  *			Disabling clicking on Labels to fix LJR25-50
  *		Version 1.13 
  *			Allow tab in time and date fields.
  *			Added watermark with the format for the time and date fields.
  *			Moved all javascript to a common $(function().
  *		Version 1.14 
  *         Accout for wartermark in time and date field validations.
  *		Version 1.15 
  *         Added set tabindex to -1 for readonly fields.
  *		Version 1.16
  *         Fixed validation in isValidForm().
  *		Version 1.17
  *			Disable tabbing for VOID.
  *		Version 1.18
  *			Trim spaces from the notes field.
  *		Version 1.19
  *			Disable tabbing if the user is in the HP Employee group.
  *		Version 1.20
  *			Allow the scrolling of the notes in read only mode.
  *		Version 1.22
  *			Remove readonly for all but HP Employee group.
  *     Version 1.23
  *         Added functionality to sort the previous notes.
  *		Version 1.24
  *			The following fields should always be editable:
  *				Date Returned
  *				Time Returned
  *				Decision
  *     Version 1.25
  *         Changed initial notes sort to descending.
  *     Version 1.26
  *         Fixed date to work in IE.
  *			In the validations for not required fileds, add tests for the watermark in IE.
  *     Version 1.27
  *			Use the date picker for IE and firefox.
  *		Version 1.28
  *			Switch all HTML DOM to jQuery.
  *			Disable editing the fields that are readonly.
  *			Fixed busy spinner for IE.
  *		Version 1.29
  *			Remove the datepicker from the input when the field is readonly.
  *		Version 1.30
  *			Cause the invalid date format error message when an invalid date is entered.
  *		Version 1.31
  *			If the Date Sent and Time Sent are blank, set them to the current date or time.
  *			Set the Save button status.  It is enabled when something has changed and the required fields are filled in.
  *		Version 1.32					11/08/2017
  *			Added the AvalonEmployeeROPA to read-only for PA.
  *		Version 1.33					01/26/2018
  *			When the Save button is disabled show a mouse over tooltip with what is missing.
  *		Version 1.34					06/18/2018
  *			Align the AUI fields.
  *		Version 1.35					07/16/2018
  *			Move the label to the left of the field.
  *		Version 1.36					09/20/2018
  *			Fixed the read-only fields.
  *
  */
--%>
<%@page import="com.avalon.lbm.portlets.priorauth.model.PeerToPeerFO"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@ include file="/html/prior-auth/init.jsp"%>
<%@ include file="/html/prior-auth/auth-details.jsp"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>

<%@page import="com.liferay.portal.kernel.service.UserGroupLocalServiceUtil"%>
<%@page import="com.liferay.portal.kernel.model.User"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="theme" %>

<portlet:defineObjects />
<theme:defineObjects />

<portlet:actionURL var="peerToPeerpageURL">
 <portlet:param name="action" value="peerToPeerAction" />
</portlet:actionURL>

<style>
	.ui-datepicker-trigger {
		position: relative;
		top: 4px;
		height: 18px;
		float: right;
	}
	
	.avalon_portlet .control-group label {
		pointer-events: none;
	}
</style>

<script>
	<%
		int NAME_LEN = 70;
		int REQUESTED_BY_LEN = 256;
		int NOTE_LEN = 4000;
	%>
</script>

<aui:script>
	var toolTip = "";
	var browserStr = getBrowser();
	var formChangedFlag = false;

	$(function() {
		
		// Highlight the Peer-to-Peer Review selection
		boldSelection();

		$('form').areYouSure({'message' : exitString});

		//  Hide one of the sorted notes text areas
		$(".hide_text").hide();

		// Set the date and time fields to the current date and time if they are blank
		var today = new Date();
		var currentDate = convertDateToString(today);
		var currentTime = convertTimeToString(today);
		setField($("#<portlet:namespace/>ptpPhyName"), "", "ptp");
		setField($("#<portlet:namespace/>ptpDecisionRequestedDate"), currentDate, "ptp");
		setField($("#<portlet:namespace/>ptpDecisionRequestedTime"), currentTime, "ptp");
		setField($("#<portlet:namespace/>ptpDecisionReturnedDate"), "", "ptp");
		setField($("#<portlet:namespace/>ptpDecisionReturnedTime"), "", "ptp");

		// Show date picker for IE and Firefox
		if ((browserStr == "IE") || (browserStr == "Firefox") || (browserStr == "unknown")) {
			$("#<portlet:namespace/>ptpDecisionRequestedDate").datepicker({
				showOn: "button",
				buttonImage: "<%=request.getContextPath()%>	/images/icondatepicker.png",
				buttonImageOnly : true,
				dateFormat: 'mm/dd/yy',
				changeMonth: true,
				changeYear: true,
				onSelect: function(selected) {
					
					// Set the time if it is not set
					setTime(true);
					
					// Cause the validation on the date field
					processItem("<portlet:namespace/>ptpDecisionRequestedDate", "<portlet:namespace/>ptpDecisionRequestedDate");
					
					// Set the status of the Save button
					setSaveButtonStatus();
				}
			});
			$("#<portlet:namespace/>ptpDecisionRequestedDate").attr("placeholder", "mm/dd/yyyy");

			$("#<portlet:namespace/>ptpDecisionReturnedDate").datepicker({
				showOn: "button",
				buttonImage: "<%=request.getContextPath()%>	/images/icondatepicker.png",
				buttonImageOnly : true,
				dateFormat: 'mm/dd/yy',
				changeMonth: true,
				changeYear: true,
				onSelect: function(selected) {
					
					// Set the time if it is not set
					setTime(false);
					
					// Cause the validation on the date field
					processItem("<portlet:namespace/>ptpDecisionReturnedDate", "<portlet:namespace/>ptpDecisionReturnedDate");
					
					// Set the status of the Save button
					setSaveButtonStatus();
				}
			});
			$("#<portlet:namespace/>ptpDecisionReturnedDate").attr("placeholder", "mm/dd/yyyy");
			
			// Move the calendar icon ro the right endge of any error message
			$(".ui-datepicker-trigger").css("float", "right");
		}
		
		// Add watermarks to the time fields
		addWatermark('<portlet:namespace/>ptpDecisionRequestedTime', false);
		addWatermark('<portlet:namespace/>ptpDecisionReturnedTime', false);
		
		// Set the initial status of the Save button
		setSaveButtonStatus();
		
		// Adjust the field widths
		adjustMemberDataWidth();
	});

	$(window).resize(function () {
		
		// Reset the fields widths
		adjustMemberDataWidth();
	});

	function getNamespace() {
		return('<portlet:namespace/>');
	}

	/****For disabling tabbing when the Auth submission status are related to Void****/
	YUI().ready('aui-node', 'event', function(Y) {
		/*
		   Set the page to read only if the user is in the HealthPlanEmployee or AvalonEmployeeROPA group and not in 
		   AvalonAdmin, AvalonEmployee, AvalonProvider, PortalApprover, or PortalCreator groups.
		*/
		<% 
			boolean readOnlyFlag = false;
			boolean hpEmployeeFlag = false;
			boolean avalonEmployeeRoPaFlag = false;
			boolean notReadOnlyGroupFlag = false;
			User userU = themeDisplay.getUser();
			long[] groups = userU.getUserGroupIds();
			int length = groups.length;
			for (int i = 0; i < length; ++i) {
				String nextGroup = UserGroupLocalServiceUtil.getUserGroup(groups[i]).getName();
				if (nextGroup.equalsIgnoreCase("HealthPlanEmployee")) {
					hpEmployeeFlag = true;
				} else if (nextGroup.equalsIgnoreCase("AvalonEmployeeROPA")) {
					avalonEmployeeRoPaFlag = true;
				} else if (nextGroup.equalsIgnoreCase("AvalonAdmin") || nextGroup.equalsIgnoreCase("AvalonEmployee") || 
						   nextGroup.equalsIgnoreCase("AvalonProvider") || nextGroup.equalsIgnoreCase("PortalApprover") || 
						   nextGroup.equalsIgnoreCase("PortalCreator")) {
					notReadOnlyGroupFlag = true;
				}
			}
			if (!notReadOnlyGroupFlag && (hpEmployeeFlag || avalonEmployeeRoPaFlag)) {
				readOnlyFlag = true;
			}
		%>
		if (<%= readOnlyFlag %>) {
			
			// Disable the tabbing and make read only
			disableTabbing();
			makeReadonly();
		} else if(Y.one('#authSubmitStatusdisableId')){
			var authSubmitStatusdisable = Y.one('#authSubmitStatusdisableId').val();
				
			if(authSubmitStatusdisable!=null && authSubmitStatusdisable!="" && authSubmitStatusdisable!='undefined'){
				if(authSubmitStatusdisable=="Sent"||authSubmitStatusdisable=="Void HP - Submitted" || authSubmitStatusdisable=="Void-Submitted" || authSubmitStatusdisable=="Void"||authSubmitStatusdisable=="Void HP"){
					if(Y.one('.avalon_portlet')){
						// Opening all fields
					}
				}
			}
		}
	});

	YUI().use('aui-node', function(Y) {
						
		/* preventing mouse click action on field which are readonly */
		if (Y.one('.optdisable')) {
			Y.all('.optdisable').on('click', function(e) {
				e.preventDefault();
			});
			$('.optdisable').prop("readonly", true);
		}
	
		/* making field readonly and disabling tabindex when the field contains data already */
		YUI().ready('aui-node', 'event', function(Y) {
	
			if (Y.one('#<portlet:namespace/>ptpDecisionRequestedDate')) {
				var ptpDecisionRequestedDate = Y.one('#<portlet:namespace/>ptpDecisionRequestedDate').val();

				if (ptpDecisionRequestedDate != null && ptpDecisionRequestedDate != "" && ptpDecisionRequestedDate != 'undefined') {
					Y.all('.ptp').addClass('optdisable');
					Y.all('.ptp').setAttribute("tabindex","-1");
					$('.ptp').prop("readonly", true);
					if ((browserStr == "unknown") || (browserStr == "IE") || (browserStr == "Firefox")) {
						if ($("#<portlet:namespace/>ptpDecisionRequestedDate").hasClass('ptp')) {
							$("#<portlet:namespace/>ptpDecisionRequestedDate").datepicker('destroy');
						}
					}
				}
			}
			
			if (Y.one('#<portlet:namespace/>ptpDecisionReturnedDate')) {
				var ptpDecisionReturnedDate = Y.one('#<portlet:namespace/>ptpDecisionReturnedDate').val();

				if (ptpDecisionReturnedDate != null && ptpDecisionReturnedDate != "" && ptpDecisionReturnedDate != 'undefined') {
					Y.all('.ptp').addClass('optdisable');
					Y.all('.ptp').setAttribute("tabindex","-1");
					$('.ptp').prop("readonly", true);
					if ((browserStr == "unknown") || (browserStr == "IE") || (browserStr == "Firefox")) {
						if ($("#<portlet:namespace/>ptpDecisionReturnedDate").hasClass('ptp')) {
							$("#<portlet:namespace/>ptpDecisionReturnedDate").datepicker('destroy');
						}
					}
				}
			}
		});

		YUI().use('aui-timepicker', function(Y) {
			new Y.TimePicker({
				trigger : 'input.timepicker',
				popover : {
					zIndex : 1
				},
				on : {
					selectionChange : function(event) {
						console.log(event.newSelection)
					}
				}
			});
		});
	
		var dtTimeSentCheck = "<%=renderRequest.getAttribute("datetime") %>";
		var dtTimeRetCheck = "<%=renderRequest.getAttribute("datetimereturned") %>";
		var decisionCheck = "<%=renderRequest.getAttribute("decision") %>";
		var phyNameCheck = "<%=renderRequest.getAttribute("physicianName") %>";
		var dtTimeSent = Y.one('#<portlet:namespace/>ptpDecisionRequestedDate');
		var dtTimeRet = Y.one('#<portlet:namespace/>ptpDecisionReturnedDate');
		var decision = Y.one('#<portlet:namespace/>ptpDecision');
		var phyName = Y.one('#<portlet:namespace/>ptpDecisionReturnedBy');
	
		if (dtTimeSentCheck === "true") {
			dtTimeSent.setAttribute("readonly", "true");
		}
		if (dtTimeRetCheck === "true") {
			dtTimeRet.setAttribute("readonly", "true");
		}
		if (decisionCheck === "true") {
			decision.setAttribute("readonly", "true");
		}
		if (phyNameCheck === "true") {
			phyName.setAttribute("readonly", "true");
		}
	});
	
	function setSaveButtonStatus() {

		// Disable the save button until all the fields are valid
		if (isValidForm() && formChangedFlag) {
			
			// Enable SAVE button
			$("#<portlet:namespace/>savePtpDecision").removeAttr("disabled");
		} else {
			
			// Disable SAVE button
			$("#<portlet:namespace/>savePtpDecision").attr("disabled", "disabled");
		}

		// Add the mouse over text for the save button
		$("#<portlet:namespace/>savePtpDecision").prop("title", toolTip);
	}
	
	function setTime(requestedFlag) {
		var dateField = null;
		var timeField = null;
		
		// Set the date and time field
		if (requestedFlag) {
			dateField = "<portlet:namespace/>ptpDecisionRequestedDate";
			timeField = "<portlet:namespace/>ptpDecisionRequestedTime";
		} else {
			dateField = "<portlet:namespace/>ptpDecisionReturnedDate";
			timeField = "<portlet:namespace/>ptpDecisionReturnedTime";
		}

		// Remove the watermark
		var timeValue = $("#" + timeField).val();
		if (timeValue == "hh:mm AM/PM") {
			timeValue = "";
		}

		// Only set a blank time field
		if (timeValue == "") {

			// Test for HTML5 errors
			var dateFormatValid = true;
			if ((browserStr != "unknown") && (browserStr != "IE") && (browserStr != "Firefox")) {
				var date_html5 = document.getElementById(dateField);
				if (date_html5.validity.badInput) {
					dateFormatValid = false;
				}
			}
			
			// Test for a valid date
			if (dateFormatValid && isValidDate($("#" + dateField).val(), true)) {
				var today = new Date();
				var currentTime = convertTimeToString(today);

				// Set the time field to the current time
				$("#" + timeField).val(currentTime);
			}
		}
	}

	var returnedFlag = false;

	function isValidForm() {
		var returnFlag = false;
		var ptpPhysicianName = $("#<portlet:namespace/>ptpPhyName").val();
		var ptpDecisionRequestedDate = $("#<portlet:namespace/>ptpDecisionRequestedDate").val();
		var ptpDecisionRequestedTime = $("#<portlet:namespace/>ptpDecisionRequestedTime").val();
		var ptpDecisionReturnedDate = $("#<portlet:namespace/>ptpDecisionReturnedDate").val();
		var ptpDecisionReturnedTime = $("#<portlet:namespace/>ptpDecisionReturnedTime").val();
		var ptpNotesCreate = $("#<portlet:namespace/>ptpNotesCreate").val();

		var ptpDecisionRequestedDate_html5 = document.getElementById("<portlet:namespace/>ptpDecisionRequestedDate");
		var ptpDecisionReturnedDate_html5 = document.getElementById("<portlet:namespace/>ptpDecisionReturnedDate");

		// Initialize the tool tip for the save button
		toolTip = "";

		// Remove white space
		ptpNotesCreate = ptpNotesCreate.trim();

		// Test for HTML5 errors
		var dateFormatValid = true;
		if ((browserStr != "unknown") && (browserStr != "IE") && (browserStr != "Firefox")) {
			if (ptpDecisionRequestedDate_html5.validity.badInput || ptpDecisionReturnedDate_html5.validity.badInput) {
				dateFormatValid = false;
			}
		}

		// Set the times if they are blank and the date is valid
		setTime(true);
		setTime(false);
		
		// If returned date or time is set both are required
		var returnedDateFlag = false;
		if ((ptpDecisionReturnedDate.length > 0) || ((ptpDecisionReturnedTime.length > 0) && (ptpDecisionReturnedTime != "hh:mm AM/PM"))) {
			returnedDateFlag = true;
		}

		if (dateFormatValid && isValid(ptpPhysicianName, <%= NAME_LEN %>, true) &&  isValidDate(ptpDecisionRequestedDate, true) && isValidTime(ptpDecisionRequestedTime, true) && 
			isValidDate(ptpDecisionReturnedDate, returnedDateFlag) && isValidTime(ptpDecisionReturnedTime, returnedDateFlag) && isValid(ptpNotesCreate, <%= NOTE_LEN %>, false)) {
			returnFlag = true;
		} else {

			// Build the toolTip for the Save button
			// Test the physician name
			if (!isValid(ptpPhysicianName, <%= NAME_LEN %>, true)) {
				toolTip += addToToolTip("The Physician Name is not valid", toolTip.length);
			}

			// Test the date sent
			if (ptpDecisionRequestedDate_html5.validity.badInput || !isValidDate(ptpDecisionRequestedDate, true)) {
				toolTip += addToToolTip("The Date Sent is not valid", toolTip.length);
			}

			// Test the time sent
			if (!isValidTime(ptpDecisionRequestedTime, true)) {
				toolTip += addToToolTip("The Time Sent is not valid", toolTip.length);
			}

			// Test the decision date
			if (ptpDecisionReturnedDate_html5.validity.badInput || !isValidDate(ptpDecisionReturnedDate, returnedDateFlag)) {
				toolTip += addToToolTip("The Date Returned is not valid", toolTip.length);
			}

			// Test the decision time
			if (!isValidTime(ptpDecisionReturnedTime, returnedDateFlag)) {
				toolTip += addToToolTip("The Time Returned is not valid", toolTip.length);
			}

			// Test the new notes
			if (!isValid(ptpNotesCreate, <%= NOTE_LEN %>, false)) {
				toolTip += addToToolTip("The Physician Rationale is not valid", toolTip.length);
			}
		}
		
		if ((toolTip.length == 0) && !formChangedFlag) {
			
			// Add a toolTip for enabling the Save button by entering a field.
			toolTip = "There is no new data to save";
		}
		
		return returnFlag;
	}
	
	function disableTabbing() {
	
		// Disable tabbing to all fields
		$('#<portlet:namespace/>ptpPhyName').prop('tabindex', '-1'); 
		$('#<portlet:namespace/>ptpDecisionRequestedBy').prop('tabindex', '-1'); 
		$('#<portlet:namespace/>ptpDecisionRequestedDate').prop('tabindex', '-1'); 
		$('#<portlet:namespace/>ptpDecisionRequestedTime').prop('tabindex', '-1'); 
		$('#<portlet:namespace/>ptpDecisionReturnedDate').prop('tabindex', '-1'); 
		$('#<portlet:namespace/>ptpDecisionReturnedTime').prop('tabindex', '-1'); 
		$('#<portlet:namespace/>ptpDecision').prop('tabindex', '-1'); 
		//$('#<portlet:namespace/>ptpNotesRead').prop('tabindex', '-1'); 
		$('#<portlet:namespace/>ptpNotesCreate').prop('tabindex', '-1'); 
	
		// Disable tabbing to all buttons
		$('#<portlet:namespace/>savePtpDecision').prop('tabindex', '-1');
	}

	function makeReadonly() {
	
		// Make all fields readonly
		$('#<portlet:namespace/>ptpPhyName').addClass("optdisable");
		$('#<portlet:namespace/>ptpDecisionRequestedBy').addClass("optdisable");
		$('#<portlet:namespace/>ptpDecisionRequestedDate').addClass("optdisable");
		$('#<portlet:namespace/>ptpDecisionRequestedTime').addClass("optdisable");
		$('#<portlet:namespace/>ptpDecisionReturnedDate').addClass("optdisable");
		$('#<portlet:namespace/>ptpDecisionReturnedTime').addClass("optdisable");
		$('#<portlet:namespace/>ptpDecision').addClass("optdisable"); 
		$('#<portlet:namespace/>ptpNotesCreate').addClass("optdisable");

		$('#<portlet:namespace/>ptpPhyName').prop("readonly", true);
		$('#<portlet:namespace/>ptpDecisionRequestedBy').prop("readonly", true);
		$('#<portlet:namespace/>ptpDecisionRequestedDate').prop("readonly", true);
		$('#<portlet:namespace/>ptpDecisionRequestedTime').prop("readonly", true);
		$('#<portlet:namespace/>ptpDecisionReturnedDate').prop("readonly", true);
		$('#<portlet:namespace/>ptpDecisionReturnedTime').prop("readonly", true);
		$('#<portlet:namespace/>ptpDecision').prop("readonly", true); 
		$('#<portlet:namespace/>ptpNotesCreate').prop("readonly", true);

		// Disable the date pickers	for IE
		if ((browserStr == "unknown") || (browserStr == "IE") || (browserStr == "Firefox")) {
			$("#<portlet:namespace/>ptpDecisionRequestedDate").datepicker('destroy');
			$("#<portlet:namespace/>ptpDecisionReturnedDate").datepicker('destroy');
		}

		// Make the buttons readonly
		$('#<portlet:namespace/>savePtpDecision').addClass("optdisable");

		$('#<portlet:namespace/>savePtpDecision').prop("readonly", true);

		// Disable all of the dropdowns for IE
		$('#<portlet:namespace/>ptpDecision').attr('disabled', true);
	}
</aui:script>

<%
    List readFdata = new ArrayList();
	String fdata_desc = "";
	String fdata_asc = "";

    if ((request.getAttribute("readData") != null)) {
        readFdata = (List) request.getAttribute("readData");
 		
		// Create the ascending list
		for (int i = 0; i < readFdata.size(); i++) {
            fdata_asc = fdata_asc + readFdata.get(i) + System.getProperty("line.separator");
        }
		
		// Create the descending list
		for (int i = readFdata.size() - 1; i >= 0; i--) {
			fdata_desc = fdata_desc + readFdata.get(i) + System.getProperty("line.separator");
		}
    }

    String ptprequestedBy = "", ptpReviewReviewerName = "", ptpReviewRequestedDate = "", ptpReviewRequestedTime = "", ptpReviewReturnedDate = "", ptpReviewReturnedTime = "", ptpDecision = "";
    if ((request.getAttribute("peerToPeerFO1") != null)) {
        PeerToPeerFO peerToPeerFO = (PeerToPeerFO) request.getAttribute("peerToPeerFO1");
        ptpDecision = peerToPeerFO.getPtpDecision();
        ptprequestedBy = peerToPeerFO.getPtpDecisionRequestedBy();
        ptpReviewReviewerName = peerToPeerFO.getPtpPhyName();
        ptpReviewRequestedTime = peerToPeerFO.getPtpDecisionRequestedTime();
        ptpReviewRequestedDate = peerToPeerFO.getPtpDecisionRequestedDate();
        ptpReviewReturnedDate = peerToPeerFO.getPtpDecisionReturnedDate();
        ptpReviewReturnedTime = peerToPeerFO.getPtpDecisionReturnedTime();
    }
%>
<div id="busy_indicator" style="display: none">
	<img src="<%=request.getContextPath()%>/images/busy-spinner.gif" alt="Busy indicator">
</div>

<liferay-ui:panel title="pa.peer.to.peer" collapsible="false">
	<aui:form name="peerToPeerDetails" action="${peerToPeerpageURL}" method="post" commandname="physicianPeerReviewFO"  onChange="formChangedFlag = true; javascript:setSaveButtonStatus()">
		<aui:fieldset label="pa.peer.to.peer.physician.information">
			<aui:container>
				<aui:row>
					<aui:col span="6">
						<aui:input type="text" inlineLabel="true" name="ptpPhyName" id="ptpPhyName" label="pa.peer.to.peer.label.physician.name" value='<%=ptpReviewReviewerName%>' maxlength="<%= NAME_LEN %>" >
							<aui:validator name="required" />
						</aui:input>
					</aui:col>
					<aui:col span="6">
						<aui:input type="text" inlineLabel="true" name="ptpDecisionRequestedBy" cssClass="ptp" label="pa.peer.to.peer.label.requested.by" 
						           value='<%=ptprequestedBy%>' disabled="true" maxlength="<%= REQUESTED_BY_LEN %>" />
					</aui:col>
				</aui:row>
				<aui:row>
					<aui:col span="6">
						<aui:input type="date" inlineLabel="true" name="ptpDecisionRequestedDate" id="ptpDecisionRequestedDate" label="pa.peer.to.peer.label.date.sent" required="true" 
						           value="<%=ptpReviewRequestedDate%>" onChange="javascript:setTime(true)" >
							<aui:validator name="custom" errorMessage="Please enter a valid date (format mm/dd/yyyy).">
								function (val, fieldNode, ruleValue) {
									var retValue = false;
									
									// Test for invalid date
									if ((browserStr == "IE") || (browserStr == "Firefox") || (browserStr == "unknown")) {
										retValue = isValidDate(val, true);
									} else {
										var ptpDecisionRequestedDate = document.getElementById("<portlet:namespace/>ptpDecisionRequestedDate");
										retValue = !ptpDecisionRequestedDate.validity.badInput;
									}
									
									return retValue;
								}
							</aui:validator>
						</aui:input>
					</aui:col>
					<aui:col span="6">
						<aui:input type="text" inlineLabel="true" name="ptpDecisionRequestedTime" label="pa.peer.to.peer.label.time.sent" value='<%=ptpReviewRequestedTime%>' id="ptpDecisionRequestedTime" >
							<aui:validator name="required" />
							<aui:validator  name="custom" errorMessage="Please enter a valid time (format: hh:mm AM/PM).">
								function (val, fieldNode, ruleValue) {
									var retValue = false;
								
								 	retValue = isValidTime(val, true);
								
								 	return retValue;
								}
							</aui:validator>
						</aui:input>
					</aui:col>
				</aui:row>
				<aui:row>
					<aui:col span="6">
						<aui:input type="date" inlineLabel="true" name="ptpDecisionReturnedDate" id="ptpDecisionReturnedDate" label="pa.peer.to.peer.label.date.returned" 
						           value="<%=ptpReviewReturnedDate%>" onChange="javascript:setTime(false)" >
							<aui:validator name="custom" errorMessage="Date Returned is required when Time Returned has been entered.  Please enter a Date Returned as well.">
								function (val, fieldNode, ruleValue) {
									var retValue = false;
								
									if (!isValidDate(val, false)) {
									
										// Do not cause this error message if the date format is invalid
										retValue = true;
									}
								
									if (!retValue) {
										var ptpDecisionReturnedTime = $("#<portlet:namespace />ptpDecisionReturnedTime").val();
								
										retValue = isValidDate(val, false);
								
										// Check for watermark in returned time
										if (ptpDecisionReturnedTime == "hh:mm AM/PM") {
											ptpDecisionReturnedTime = "";
										}
									
										if (val.length == 0) {
								
											// Has the date been entered
											if (ptpDecisionReturnedTime.length > 0) {
												retValue = false;
											}
										}							    	
									}
								
									if (!returnedFlag) {
								
										// Run the ptpDecisionReturnedTime validation
										returnedFlag = true;
										processItem("<portlet:namespace />ptpDecisionReturnedTime", "<portlet:namespace />ptpDecisionReturnedDate")
										returnedFlag = false;
									}
								
									return retValue;
								}
							</aui:validator>
							<aui:validator name="custom" errorMessage="Please enter a valid date (format mm/dd/yyyy).">
								function (val, fieldNode, ruleValue) {
									var retValue = false;
									
									// Test for invalid date
									if ((browserStr == "IE") || (browserStr == "Firefox") || (browserStr == "unknown")) {
										retValue = isValidDate(val, false);
									} else {
										var ptpDecisionReturnedDate = document.getElementById("<portlet:namespace/>ptpDecisionReturnedDate");
										retValue = !ptpDecisionReturnedDate.validity.badInput;
									}
									
									return retValue;
								}
							</aui:validator>
						</aui:input>
					</aui:col>
					<aui:col span="6">
						<aui:input type="text" inlineLabel="true" name="ptpDecisionReturnedTime" id="ptpDecisionReturnedTime" label="pa.peer.to.peer.label.time.returned" 
						           value='<%=ptpReviewReturnedTime%>'>
							<aui:validator name="custom" errorMessage="Time Returned is required when Date Returned has been entered.  Please enter a Time Returned as well.">
								function (val, fieldNode, ruleValue) {
									var retValue = true;
									var ptpDecisionReturnedDate = $("#<portlet:namespace />ptpDecisionReturnedDate").val();
								
									// Check for watermark in returned time
									if (val == "hh:mm AM/PM") {
										val = "";
									}
								
									if (val.length == 0) {
								
										// Has the date been entered
										if (ptpDecisionReturnedDate.length > 0) {
											retValue = false;
										}
									}							    	
								
									if (!returnedFlag) {
								
										// Run the ptpDecisionReturnedTime validation
										returnedFlag = true;
										processItem("<portlet:namespace />ptpDecisionReturnedDate", "<portlet:namespace />ptpDecisionReturnedTime")
										returnedFlag = false;
									}
								
									return retValue;
								}
							</aui:validator>
							<aui:validator name="custom" errorMessage="Please enter a valid time (format: hh:mm AM/PM).">
								function (val, fieldNode, ruleValue) {
									var retValue = false;
								
									retValue = isValidTime(val, false);
								
									return retValue;
								}
							</aui:validator>
 						</aui:input>
					</aui:col>
				</aui:row>
				<aui:row>
					<c:choose>
					<%
						if (ptpDecision != null && !ptpDecision.equalsIgnoreCase("") && !ptpDecision.equalsIgnoreCase("null")) {
					%>
					<aui:col span="6">
						<aui:select inlineLabel="true" name="ptpDecision" label="pa.peer.to.peer.label.decision" cssClass="ptpd span7" id="ptpDecision" >
							<aui:option value="">Make a Selection</aui:option>
							<aui:option value="approved" selected="<%=ptpDecision.equals(\"approved\")%>">Approved</aui:option>
							<aui:option value="partiallyApproved" selected="<%=ptpDecision.equals(\"partiallyApproved\")%>">Partially Approved</aui:option>
							<aui:option value="denied" selected="<%=ptpDecision.equals(\"denied\")%>">Denied</aui:option>
						</aui:select>
					</aui:col>
					<%
						} else {
					%>
					<aui:col span="6">
						<aui:select inlineLabel="true" name="ptpDecision" label="pa.peer.to.peer.label.decision" cssClass="ptpd span7" id="ptpDecision" >
							<aui:option value="">Make a Selection</aui:option>
							<aui:option value="approved">Approved</aui:option>
							<aui:option value="partiallyApproved">Partially Approved</aui:option>
							<aui:option value="denied">Denied</aui:option>
						</aui:select>
					</aui:col>
					<%
						}
					%>
					</c:choose>
				</aui:row>
			</aui:container>
		</aui:fieldset>
   
		<aui:fieldset label="pa.peer.to.peer.physician.rationale">
			<aui:container>
				<aui:row>
					<i class="icon-chevron-sign-up" title="Change Sort Order" onClick="javascript: changeSortOrder(this)" ></i>
					<aui:input type="textarea" cssClass="oldNotes note-control" resizable="false" name="ptpNotesRead" label="" value='<%= fdata_desc %>' readonly="true" rows="7" tabindex="-1" />
					<aui:input type="textarea" cssClass="hide_text oldNotes note-control" resizable="false" name="ptpNotesRead" label="" value='<%= fdata_asc %>' readonly="true" rows="7" tabindex="-1" />
					<aui:input type="textarea" cssClass="note-control" resizable="false" name="ptpNotesCreate" label="pa.peer.to.peer.label.enter.rationale" rows="3" >
						<aui:validator name="maxLength"><%= NOTE_LEN %></aui:validator>
					</aui:input>
				</aui:row>
			</aui:container>
		</aui:fieldset>
		
		<aui:fieldset>
			<aui:button-row cssClass="btn-divider">
				<div onmouseenter="setSaveButtonStatus()">
					<aui:button type="button" primary="true" name="savePtpDecision" cssClass="pull-right saveAction" value="pa.label.save" onclick="javascript:checkValidForm('peerToPeerDetails')" />
				</div>
			</aui:button-row>
		</aui:fieldset>
	</aui:form>
</liferay-ui:panel>
