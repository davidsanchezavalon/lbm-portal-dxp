<%--
/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

/**
  * Description
  *		This file contain the Additional Information portlet for Prior Auth.
  *
  * CHANGE History
  * 	Version 1.0
  *			The initial version was split from Nurse Review.
  *		Version 1.1
  *			Disable tabbing for VOID.
  *		Version 1.2
  *			Make screens read only for Health Plan Employees.
  *		Version 1.3
  *			Allow the scrolling of requested info in read only mode.
  *		Version 1.4
  *			Remove readonly for all but HP Employee group.
  *		Version 1.5
  *			Switch all HTML DOM to jQuery.
  *			Fixed busy spinner for IE.
  *		Version 1.6						11/08/2017
  *			Added the AvalonEmployeeROPA group to read-only for PA.
  *		Version 1.7						01/12/2018
  *			Disable Save button until all required fields are valid.
  *			When the Save button is disabled show a mouse over tooltip with what is missing.
  *		Version 1.8						06/18/2018
  *			Align the AUI fields.
  *		Version 1.9						09/20/2018
  *			Fixed the read-only fields.
  *
  */
--%>
<%@page import="com.liferay.portal.kernel.service.UserGroupLocalServiceUtil"%>
<%@page import="com.liferay.portal.kernel.model.User"%>
<%@page import="com.avalon.lbm.portlets.priorauth.util.PriorAuthUtil"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@ include file="/html/prior-auth/init.jsp"%>
<%@ include file="/html/prior-auth/auth-details.jsp"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>

<%@ taglib uri="http://liferay.com/tld/theme" prefix="theme" %>

<portlet:defineObjects />
<theme:defineObjects />

<portlet:actionURL var="additionalInformationpageURL">
	<portlet:param name="action" value="additionalInformationAction" />
</portlet:actionURL>

<script>
	<%
		int NOTE_LEN = 4000;
	%>
</script>

<aui:script>
	var toolTip = "";

	$(function() {
		
		// Highlight the Additional Information selection
		boldSelection();

		$('form').areYouSure({ 'message' : exitString });
	
		//  Hide one of the sorted notes text areas
		$(".hide_text").hide();
		
		// Set the initial save button status
		setSaveButtonStatus();
		
		// Adjust the field widths
		adjustMemberDataWidth();
	});

	$(window).resize(function () {
		
		// Reset the fields widths
		adjustMemberDataWidth();
	});
	
	function getNamespace() {
		return('<portlet:namespace/>');
	}
	
	/****For disabling tabbing when the Auth submission status are related to Void****/
	YUI().ready('aui-node', 'event', function(Y) {
		/*
		   Set the page to read only if the user is in the HealthPlanEmployee or AvalonEmployeeROPA group and not in 
		   AvalonAdmin, AvalonEmployee, AvalonProvider, PortalApprover, or PortalCreator groups.
		*/
		<% 
			boolean readOnlyFlag = false;
			boolean hpEmployeeFlag = false;
			boolean avalonEmployeeRoPaFlag = false;
			boolean notReadOnlyGroupFlag = false;
			User userU = themeDisplay.getUser();
			long[] groups = userU.getUserGroupIds();
			int length = groups.length;
			for (int i = 0; i < length; ++i) {
				String nextGroup = UserGroupLocalServiceUtil.getUserGroup(groups[i]).getName();
				if (nextGroup.equalsIgnoreCase("HealthPlanEmployee")) {
					hpEmployeeFlag = true;
				} else if (nextGroup.equalsIgnoreCase("AvalonEmployeeROPA")) {
					avalonEmployeeRoPaFlag = true;
				} else if (nextGroup.equalsIgnoreCase("AvalonAdmin") || nextGroup.equalsIgnoreCase("AvalonEmployee") || 
						   nextGroup.equalsIgnoreCase("AvalonProvider") || nextGroup.equalsIgnoreCase("PortalApprover") || 
						   nextGroup.equalsIgnoreCase("PortalCreator")) {
					notReadOnlyGroupFlag = true;
				}
			}
			if (!notReadOnlyGroupFlag && (hpEmployeeFlag || avalonEmployeeRoPaFlag)) {
				readOnlyFlag = true;
			}
		%>
		if (<%= readOnlyFlag %>) {
			
			// Disable the tabbing and make read only
			disableTabbing();
			makeReadonly();
		} else if(Y.one('#authSubmitStatusdisableId')){
			var authSubmitStatusdisable = Y.one('#authSubmitStatusdisableId').val();
			
			if(authSubmitStatusdisable!=null && authSubmitStatusdisable!="" && authSubmitStatusdisable!='undefined'){
				if(authSubmitStatusdisable=="Sent"||authSubmitStatusdisable=="Void HP - Submitted" || authSubmitStatusdisable=="Void-Submitted" || authSubmitStatusdisable=="Void"||authSubmitStatusdisable=="Void HP"){
					if(Y.one('.avalon_portlet')){
						// Opening all fields
					}
				}
			}
		}
	});

	function isValidForm() {
		var formValid = false;
		var additionalNotesCreate = $("#<portlet:namespace/>additionalNotesCreate").val();
		
		// Initialize the tool tip for the save button
		toolTip = "";
		
		// Remove white space
		additionalNotesCreate = additionalNotesCreate.trim();
		
		// Validate the form
		if (isValid(additionalNotesCreate, <%= NOTE_LEN %>, true)) {
			formValid = true;
			toolTip = "";
		} else {
			toolTip = "Enter a Case Note";
		}
	
		return formValid;
	}
	
	function makeReadonly() {
	
		// Make all fields readonly
		$('#<portlet:namespace/>additionalNotesCreate').addClass("optdisable");
	
		// Disable tabbing to all buttons
		$('#<portlet:namespace/>saveAdditionalNotes').addClass("optdisable");
	}
	
	function disableTabbing() {
	
		// Disable tabbing to all fields
		$('#<portlet:namespace/>additionalNotesCreate').prop('tabindex', '-1'); 
	
		// Disable tabbing to all buttons
		$('#<portlet:namespace/>saveAdditionalNotes').prop('tabindex', '-1');
	}

	function setSaveButtonStatus() {
		
		// Disable the save button until all the fields are valid
		if (isValidForm()) {
			$("#<portlet:namespace/>saveAdditionalNotes").removeAttr("disabled");
		} else {
			$("#<portlet:namespace/>saveAdditionalNotes").attr("disabled", "disabled");
		}
		
		// Add the mouse over text for the save button
		$("#<portlet:namespace/>saveAdditionalNotes").prop("title", toolTip);
	}
</aui:script>

<%
	List readFdata = new ArrayList();
	String fdata_desc = "";
	String fdata_asc = "";
	
	if ((request.getAttribute("readData") != null)) {
		readFdata = (List) request.getAttribute("readData");
		
		// Create the ascending list
		for (int i = 0; i < readFdata.size(); i++) {
			fdata_asc = fdata_asc + readFdata.get(i) + System.getProperty("line.separator");
		}
		
		// Create the descending list
		for (int i = readFdata.size() - 1; i >= 0; i--) {
			fdata_desc = fdata_desc + readFdata.get(i) + System.getProperty("line.separator");
		}
	}
%>


<div id="busy_indicator" style="display: none">
	<img src="<%=request.getContextPath()%>/images/busy-spinner.gif" alt="Busy indicator">
</div>

<liferay-ui:panel title="pa.additional.info.subheader" collapsible="false">
	<aui:form name="additionalInformationDetails" action="${additionalInformationpageURL}" method="post" commandname="nurseReviewFO" onChange="javascript:setSaveButtonStatus()">
		<aui:fieldset label="pa.additional.info.subheader">
			<aui:container>
				<aui:row>
					<i class="icon-chevron-sign-up" title="Change Sort Order" onClick="javascript: changeSortOrder(this)" ></i>
					<aui:input type="textarea" cssClass="oldNotes note-control" resizable="false" name="additionalNotesRead" label="" readonly="true" rows="7" value='<%= fdata_desc %>' />
					<aui:input type="textarea" cssClass="hide_text oldNotes note-control" resizable="false" name="additionalNotesRead" label="" readonly="true" rows="7" value='<%= fdata_asc %>' />
					<aui:input type="textarea" cssClass="note-control" resizable="false" name="additionalNotesCreate" label="pa.additional.review.label.notes" rows="3" value='' >
						<aui:validator name="required" />
						<aui:validator name="maxLength"><%= NOTE_LEN %></aui:validator>
					</aui:input>
				</aui:row>
			</aui:container>
		</aui:fieldset>
		
		<aui:fieldset>
			<aui:button-row cssClass="btn-divider">
				<aui:button type="button" primary="true" name="saveAdditionalNotes" id="saveAdditionalNotes" cssClass="pull-right" value="pa.label.save" onclick="javascript:checkValidForm('additionalInformationDetails')" />
			</aui:button-row>
		</aui:fieldset>
	</aui:form>
</liferay-ui:panel>
