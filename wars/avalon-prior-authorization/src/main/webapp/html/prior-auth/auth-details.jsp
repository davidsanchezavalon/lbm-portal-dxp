<%--
/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

 /**
  * Description
  *		This file contain the header field for Prior Auth.
  *
  * CHANGE History
  * 	Version 1.0  
  *			All of the changes have been merged.
  * 		This file includes all of the field validations.
  *		Version 1.1
  *			Convert idCardNumber to upper case
  *		Version 1.2
  *			Make read only if the user is in the HP Employee group.
  *		Version 1.3					07/16/2018
  *			Move the label to the left of the field.
  *		Version 1.4					08/08/2018
  *			Added HP LOB and Subscriber Name.
  *
  */
--%>
<style>
.authSubmitStatusdisablehide{
	pointer-events: none;
	background-color: #eee !important;
	display:none !important;
}
.optdisable,senttdisable {
	pointer-events: none;
	background-color: #eee !important;
}
</style>

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<%@page import="com.liferay.portal.kernel.service.UserGroupLocalServiceUtil"%>
<%@page import="com.liferay.portal.kernel.model.User"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="theme" %>

<portlet:defineObjects />
<theme:defineObjects />

<%-- Header details having the member details and authorization details - common for every page --%>

<liferay-ui:panel title="pa.external.site.links" collapsible="true" defaultState = "closed">
	<p><a href="https://npiregistry.cms.hhs.gov/registry/" target="_blank">Search NPI Records</a></p>
	<p><a href="https://www.verifycomply.com/login.asp" target="_blank">Verify Comply</a></p>
	<p><a href="https://web.healthsparq.com/psearch/public/?insurerCode=SC&brandCode=SCSC&productCode=SC01" target="_blank">South Carolina - Provider Search</a></p>
	<p><a href="https://provider.bcbssc.com/wps/portal/hcp/providers/home" target="_blank">South Carolina - My Insurance Manager</a></p>
</liferay-ui:panel>

<liferay-ui:panel title="pa.details" collapsible="false">
<aui:form name="currentAuthorizationDetails"  method="post" commandname="currentAuthorizationDetailsFO">
	<input type="text" class="authSubmitStatusdisablehide" id="authSubmitStatusdisableId" name="authSubmissionStatus"  value='${currentAuthorizationDetailsFO.authSubmissionStatus}'/>
		
	<aui:fieldset>
		<aui:container>
			<aui:row>
				<aui:col span="6">
					<aui:input type="text" inlineLabel="true" name="idCardNumber" label="pa.label.id.card.number" value='${currentAuthorizationDetailsFO.idCardNumber}' style="text-transform:uppercase" disabled="true" />
				</aui:col>
				<aui:col span="6">
					<aui:input type="text" inlineLabel="true" name="authNumberSearched"  cssClass="authnumber" label="pa.label.auth.number" value='${currentAuthorizationDetailsFO.authorizationNumber}' disabled="true" />
				</aui:col>
			</aui:row>
			
			<aui:row>
				<aui:col span="6">
					<aui:input type="text" inlineLabel="true" name="authStatus" label="pa.label.auth.status" value='${currentAuthorizationDetailsFO.authorizationStatus}' disabled="true" />
				</aui:col>
				<aui:col span="6">
					<aui:input type="text" inlineLabel="true" name="authAge" label="pa.label.auth.age" value='${currentAuthorizationDetailsFO.authorizationAge}' disabled="true" />
				</aui:col>
			</aui:row>
			
			<aui:row>
				<aui:col span="6">
					<aui:input type="text" inlineLabel="true" name="groupId" label="pa.label.group.id" value='${currentAuthorizationDetailsFO.memberGroupId}' disabled="true" />
				</aui:col>
				<aui:col span="6">
					<aui:input type="text" inlineLabel="true" name="hpLob" label="pa.label.hp.lob" value='${currentAuthorizationDetailsFO.hpLob}' disabled="true" />
				</aui:col>
			</aui:row>

		</aui:container>
	</aui:fieldset>
				
	<aui:fieldset label="pa.details.subscriber.details">
		<aui:container>

			<aui:row>
				<aui:col span="6">
					<aui:input type="text" inlineLabel="true" name="subscriberName" label="pa.label.subscriber.name" value='${currentAuthorizationDetailsFO.subscriberName}' disabled="true" />
				</aui:col>
			</aui:row>

		</aui:container>
	</aui:fieldset>
			
	<aui:fieldset label="pa.details.member.details">
		<aui:container>

			<aui:row>
				<aui:col span="6">
					<aui:input type="text" inlineLabel="true" name="memberName" label="pa.label.member.name" value='${currentAuthorizationDetailsFO.memberName}' disabled="true" />
				</aui:col>
				<aui:col span="6">
					<aui:input type="text" inlineLabel="true" name="memberDob" label="pa.label.member.dob" value='${currentAuthorizationDetailsFO.memberDOB}' disabled="true" />
				</aui:col>
			</aui:row>

			<aui:row>
				<aui:col span="6">
					<aui:input type="text" inlineLabel="true" name="memberGender" label="pa.label.member.gender" value='${currentAuthorizationDetailsFO.gender}' disabled="true" />
				</aui:col>
				<aui:col span="6">
					<aui:input type="text" inlineLabel="true" name="memberPhoneNumber" label="pa.label.phone.number" value='${currentAuthorizationDetailsFO.memberPhoneNumber}' disabled="true" />
				</aui:col>
			</aui:row>

			<aui:row>
				<aui:col span="6">
					<aui:input type="text" inlineLabel="true" name="memberAddressLine1" label="pa.label.address" value='${currentAuthorizationDetailsFO.memberAddressLine1}' disabled="true" />
				</aui:col>
				<aui:col span="6">
					<aui:input type="text" inlineLabel="true" name="memberAddressLine2" label="pa.label.address.additional" value='${currentAuthorizationDetailsFO.memberAddressLine2}' disabled="true" />
				</aui:col>
			</aui:row>

 			<aui:row>
				<aui:col span="6">
					<aui:input type="text" inlineLabel="true" name="memberCity" label="pa.label.city" value="${currentAuthorizationDetailsFO.memberCity}" disabled="true" />
				</aui:col>
				<aui:col span="3">
					<aui:input type="text" inlineLabel="true" name="memberState" label="pa.label.state" cssClass="span7" value="${currentAuthorizationDetailsFO.memberStateCode}" disabled="true" />
				</aui:col>
				<aui:col span="3">
					<aui:input type="text" inlineLabel="true" name="memberZip" label="pa.label.zip.code" value="${currentAuthorizationDetailsFO.memberZipcode}" disabled="true" />
				</aui:col>
 			</aui:row>

		</aui:container>
	</aui:fieldset>

	</aui:form>
</liferay-ui:panel>
