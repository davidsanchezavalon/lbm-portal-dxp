<%--
/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
 
 /**
  * Description
  *		This file contain the Procedure Information portlet for Prior Auth.
  *
  * CHANGE History
  *		Version 1.0
  *			All of the changes have been merged.
  * 		This file includes all of the field validation, the busy icon, and the check for leaving the page without saving changes.
  *			The Request Date/Time datetime field has been split into two fields, on for the date and one for the time.
  *		Version 1.1
  *			Moved all javascript except 'areYouSure' and 'YUI' from aui:script block to script block.
  *		Version 1.2
  *			Add parameter to isValidDigit.
  *			Changed the name and id of authStatus to authStatus2.  Something is conflicting with authStatus.
  *			Convert diagnosis code and procedure code to upper case
  *			Fixed service start and end date validation.  Moved setStartDate and setEndDate to aui:script block.
  *			Added the busy indicator to the Save and Submit buttons if there are no invalid fields.  There is an issue with the submit button.
  *				You will get the message about saving your page because submit causes you to leave the page because it submits to a new url.
  *			Added the processing to the Cancel button to close the page.
  *		Version 1.3
  *			Allow period in diagnosis codes.
  *		Version 1.4
  *			Auth Status field name was authStatus2. Changed it to authStatus.
  *			Changed fieldnames of Inbound channel, created user ID, Requested Date for correct mapping
  *		Version 1.5
  *			Fixed ticket LJR-400.
  *			Added require * to the proper columns in the Procedure columns.
  *		Version 1.6  
  *			Fixed date fields for IE and Firefox.
  *		Version 1.7  
  *			Added pending to reason and decision drop downs.
  *			Make reason, decision, and units required for the first procedure row.
  *		Version 1.8  
  *			Fixed decision reason column heading.
  *		Version 1.9  
  *			Change validation order for dates.
  *		Version 1.10
  *			Fixed form retention when user selects Pending in decision dropdowns
  *		Version 1.11
  *			Pending should not be the default for decision and reason in the firat procedure rows.
  *			There were two options selected is some of the decision and reason.  Remove the invalid selection.
  *			For begin and end service date, do not show an error is the other date is blank.
  *		Version 1.12
  *			If procedures rows 2 - 25 have one value then from code, decision, reason, and units are require.
  *		Version 1.13
  *			procedure units field is made mandatory whenever user loads the page. This can be before or after SAVE/SUBMIT.
  *		Version 1.14
  *			Move Cancel button to left.
  *		Version 1.15
  *			Added the check for unsaved changes.  "areYouSure" was commented out.
  *		Version 1.16
  *			Validate the from and to procedure codes are 5 characters.
  *		Version 1.17
  *			Changed validation for service begin date and end date to allow till 1/1/2016
  *		Version 1.18
  *			Disallowing users to type any text in the datetime fields
  *		Version 1.19
  *			Change patient relationship to display numbers.
  *			Move void button to the left and change the background color to red.
  *		Version 1.20
  *			Fixed the validation for service begin date and end date to allow till 1/1/2016.  The change
  *			that was made in version 2.7 did not match the previous code.
  *		Version 1.21
  *			Added onSubmit to SAVE and VOID buttons also to silent exit string on save or void
  *		Version 1.22
  *			Added error message which gets displayed when duplicate procedure codes are inserted.
  *		Version 1.23
  *			Disabling diagnosis section when submission status is 'sent'
  *		Version 1.24
  *			Disabling SAVE button until any changes are made in any field
  *		Version 1.25
  *			Adding pop functionality when the Auth status is Updated 
  *		Version 1.26
  *			Disabling clicking on Labels to fix LJR25-50
  *		Version 1.27
  *			Removing "updates will not be submitted" popup on confirming submit pop up
  *		Version 1.28
  *			Disabling add procedure link when in sent status
  *		Version 1.29
  *			Fixed inconsistent error messages for diagnosis codes.
  *			Display a AUI validation error when a duplicate diagnosis or procedure codes are entered.
  *		Version 1.30
  *			Disabling SUBMIT button until any changes are made.
  *		Version 1.31
  *			If diagnosis length > 0 display the invalid length error.
  *			Allow tab in date and time fields.
  *			Remove time drop-down.
  *			Added watermark with the format for the time and date fields.
  *			Moved all javascript to a common $(function().
  *		Version 1.32
  *			Disable tabbing for VOID.
  *		Version 1.33
  *			Disable tabbing if the user is in the HP Employee group.
  *		Version 1.34
  *			Added a popup to remind user of submit when the save is pressed.
  *		Version 1.35
  *			When the Authorization Status is changed to "In Process" clear the Original Decision Date.
  *			When the Authorization Status is changed to "Approved/Partially Approved/Denied" change 
  *				Original Decision Date to the current date and time.
  *			Make Original Decision Date editable and split into to fields (date and time).
  *			Validate the Original Decision Date is 1/1/2016 or later.
  *		Version 1.36
  *			Added a cancel to the "submit popup".  When this is selected the save and submit button status
  *				will not be changed.
  *			If Authorization status is 'In Process', do not show the "submit popup" and disable the submit button. 
  *			If any procedure row is pending, do not show the "submit popup" and disable the submit button. 
  *			Enable the save button when any field has changed.
  *		Version 1.37
  *     	Fixed the busy spinner fo IE.
  *         Fixed date fields in IE.
  *		Version 1.38
  *     	Added validation of diagnosis and procedure codes.
  *		Version 1.39
  *			Fixed the tabbing order.
  *		Version 1.40
  *			Added diagnosis and procedure descriptions.
  *			Use the service begin date for the diagnosis and procedure descriptions.
  *		Version 1.41
  *			Switch all HTML DOM to jQuery.
  *			Use the date picker for IE and firefox.
  *			Fixed busy spinner for IE.
  *		Version 1.42
  *			Add fast track check box column to procedure table.
  *		Version 1.43
  *			Remove the datepicker from the input when the field is readonly.
  *		Version 1.44
  *			Set the status of the SAVE button after a datepicker is changed in IE.
  *			Fixed the service begin date and service end date to only run the validation once for each of them.
  *			Cause the invalid date format error message when an invalid date is entered.
  *		Version 1.45
  *			Added new fields: Withdrawn Reason, Being Worked By, Due Date, and Due Time.
  *			Added items to the dropdowns: Inbound Channel and Authorization Status.
  *			Remove the Void button and add the functionality to the Submit button when the status is withdrawn.
  *		Version 1.46
  *			If the due date is set and the due time is blank or not the correct format, set the time to 8 PM.
  *			For a SAVE, if the workedBy field has not been changed display a popup.  Added a flag to the properties file that controls if this is displayed.
  *			For the workedBy field, "Make a selection" is the default value.
  *		Version 1.47
  *			Disable the submit button if the form is not dirty.
  *		Version 1.48
  *			Added a new Authorization Status.
  *		Version 1.49					11/08/2017
  *			Added the AvalonEmployeeROPA group to read-only for PA.
  *		Version 1.50					12/04/2017
  *			Hide fast track.
  *		Version 1.51					01/22/2018
  *			When the Save button is disabled show a mouse over tooltip with what is missing.
  *			When the Submit button is disabled show a mouse over tooltip with what is missing.
  *		Version 1.51					02/12/2018
  *			Added and option to Decision Reason (D3 - Lack of Timely Filing Administrative Denial).
  *		Version 1.52					05/24/2018
  *			Set Submission Status to disabled to prevent edit of the field by clicking the label.
  *  	Version 1.53					05/30/2018
  *  		Removed disabled from Submission Status Field.  A disabled field cannot be read by the controller.
  *		Version 1.54					06/06/2018
  *			Change the cancel button from a hyperlink to a button.
  *			Hide the add link after 12 diagnosis codes 
  *			Hide the add link after 25 Procedure codes 
  *		Version 1.55					06/18/2018
  *			Align the AUI fields.
  *		Version 1.56					07/16/2018
  *			Move the label to the left of the field.
  *		Version 1.57					08/09/2018
  *			Added a new authorization status.
  *			Added a new Procedure Decision and Decision Reason.
  *		Version 1.58					09/20/2018
  *			Fixed the read-only fields.
  *
  */
--%>

<%@ page import="com.avalon.lbm.portlets.priorauth.model.PriorAuthConstants" %>
<%@ page import="com.avalon.lbm.portlets.priorauth.model.DiagnosisDisplayOrder" %>
<%@ page import="com.avalon.lbm.portlets.priorauth.util.PriorAuthUtil" %>
<%@ page import="com.avalon.lbm.portlets.priorauth.model.ProcedureInformationFO" %>
<%@ page import="com.avalon.lbm.services.PriorAuthHeaderServiceStub.DiagnosisDTO" %>
<%@ page import="com.avalon.lbm.services.PriorAuthHeaderServiceStub.ProcedureDTO" %>
<%@ page import="java.util.*" %>
<%@page import="com.liferay.portal.kernel.service.UserGroupLocalServiceUtil"%>
<%@page import="com.liferay.portal.kernel.model.User"%>

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="theme" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<%@ include file="/html/prior-auth/init.jsp" %>
<%@ include file="/html/prior-auth/auth-details.jsp" %>

<portlet:defineObjects />
<theme:defineObjects />

<portlet:actionURL var="procedureInformationPageURL">
 <portlet:param name="action" value="procedureInformationAction" />
</portlet:actionURL>

<portlet:actionURL var='procedureInformationCancelURL'>
 <portlet:param name="cancelaction" value="procedureInfoCancelAction" />
</portlet:actionURL>

<portlet:actionURL var='procedureSubmitURL'>
 <portlet:param name="action" value="procedureSubmitAction" />
</portlet:actionURL>

<portlet:resourceURL var='getDiagnosisDescription'
                     id='getDiagnosisDescription' />

<portlet:resourceURL var="getProcedureDescription"
	                 id="getProcedureDescription" />

<style>
	.authSubmitStatusdisable {
		pointer-events: none;
		background-color: #eee !important;
	}
	
	.updateSubmitdisable {
		pointer-events: none;
		opacity: .4;
	}
	
	.linDisable {
		display: none;
	}
		
	.avalon__proc_portlet .control-group label {
		pointer-events: none;
	}

	input[type=checkbox] {
		transform: scale(1.5);
		margin-left: 3.5% !important;
	}
</style>

<script>
	<%
		int DIAGNOSIS_MAX_ROWS = 11;
		int PROCEDURE_MAX_ROWS = 25;
		
		int DIAGNOSIS_CODE_LEN = 10;
		int PROCEDURE_CODE_LEN = 10;
		int PROCEDURE_UNIT_LEN = 5;
		int RECORD_CREATOR_LEN = 256;
	%>
</script>

<aui:script>
	var toolTipSave = "";
	var toolTipSubmit = "";

	var diagnosisError = "Please enter a valid diagnosis code";
	var procedureError = "Please enter a valid procedure code";
	
	var browserStr = getBrowser();
	var serviceDateFlag = false;
	var workedByChanged = false;
	var formChangedFlag = false;
	
	// Get the minimum date for the requested date (possible future change)
	var minRequestedDateStr = getMinRequestedDate();
	var minRequestedDate = new Date(convertDateIE(minRequestedDateStr));

	// Get the maximum date for the requested date
	var maxRequestedDate = new Date();
	var maxRequestedDateStr = convertDateToString(maxRequestedDate);
	
	$(function() {
		
		// Highlight the Procedure Information selection
		boldSelection();

		$('.toUppercase').focusout(function() {
			// Uppercase-ize contents
			this.value = this.value.toLocaleUpperCase();
		});

		// Show date picker for IE and Firefox
		if ((browserStr == "IE") || (browserStr == "Firefox") || (browserStr == "unknown")) {

			$("#<portlet:namespace/>authRequestedDate").datepicker({
				showOn: "button",
				buttonImage: "<%=request.getContextPath()%>	/images/icondatepicker.png",
				buttonImageOnly : true,
				dateFormat: 'mm/dd/yy',
				changeMonth: true,
				changeYear: true,
				onSelect: function(selected) {
					
					// Cause the validation on the date field
					processItem("<portlet:namespace/>authRequestedDate", "<portlet:namespace/>authRequestedDate");
				}
			});
			$("#<portlet:namespace/>authRequestedDate").attr("placeholder", "mm/dd/yyyy");

			$("#<portlet:namespace/>serviceBeginDate").datepicker({
				showOn: "button",
				buttonImage: "<%=request.getContextPath()%>	/images/icondatepicker.png",
				buttonImageOnly : true,
				dateFormat: 'mm/dd/yy',
				changeMonth: true,
				changeYear: true,
				onSelect: function(selected) {
					
					// Cause the validation on the date fields
					processItem("<portlet:namespace/>serviceEndDate", "<portlet:namespace/>serviceBeginDate");
				}
			});
			$("#<portlet:namespace/>serviceBeginDate").attr("placeholder", "mm/dd/yyyy");

			$("#<portlet:namespace/>serviceEndDate").datepicker({
				showOn: "button",
				buttonImage: "<%=request.getContextPath()%>	/images/icondatepicker.png",
				buttonImageOnly : true,
				dateFormat: 'mm/dd/yy',
				changeMonth: true,
				changeYear: true,
				onSelect: function(selected) {
					
					// Cause the validation on the date fields
					processItem("<portlet:namespace/>serviceBeginDate", "<portlet:namespace/>serviceEndDate");
				}
			});
			$("#<portlet:namespace/>serviceEndDate").attr("placeholder", "mm/dd/yyyy");

			$("#<portlet:namespace/>authDecisionDate").datepicker({
				showOn: "button",
				buttonImage: "<%=request.getContextPath()%>	/images/icondatepicker.png",
				buttonImageOnly : true,
				dateFormat: 'mm/dd/yy',
				changeMonth: true,
				changeYear: true,
				onSelect: function(selected) {
					
					// Cause the validation on the date field
					processItem("<portlet:namespace/>authDecisionDate", "<portlet:namespace/>authDecisionDate");
				}
			});
			$("#<portlet:namespace/>authDecisionDate").attr("placeholder", "mm/dd/yyyy");

			$("#<portlet:namespace/>authDueDate").datepicker({
				showOn: "button",
				buttonImage: "<%=request.getContextPath()%>	/images/icondatepicker.png",
				buttonImageOnly : true,
				dateFormat: 'mm/dd/yy',
				changeMonth: true,
				changeYear: true,
				onSelect: function(selected) {
					
					// Cause the validation on the date field
					processItem("<portlet:namespace/>authDueDate", "<portlet:namespace/>authDueDate");
				}
			});
			$("#<portlet:namespace/>authDueDate").attr("placeholder", "mm/dd/yyyy");

			// Add onChange or onBlur calls to the date picker fields (the AUI function does not fire in IE)
			$("#<portlet:namespace/>authRequestedDate").attr("onChange", "setFormDirty(); setSaveButtonStatus()");
			$("#<portlet:namespace/>authDecisionDate").attr("onChange", "setFormDirty(); setSaveButtonStatus()");
			$("#<portlet:namespace/>serviceBeginDate").attr("onBlur", "setFormDirty(); setSaveButtonStatus()");
			$("#<portlet:namespace/>serviceEndDate").attr("onBlur", "setFormDirty(); setSaveButtonStatus()");
			$("#<portlet:namespace/>authDueDate").attr("onChange", "setFormDirty(); setSaveButtonStatus()");
			
			$(".ui-datepicker-trigger").css("float", "right");
		}
		
		// Add watermarks to the time fields
		addWatermark('<portlet:namespace/>authRequestedTime', false);
		addWatermark('<portlet:namespace/>authDecisionTime', false);
		addWatermark('<portlet:namespace/>authDueTime', false);
	
		// Add the descriptions
		getAllDesc("2");

		// Do not allow tabbing into the submission status field
		$("#<portlet:namespace/>authSubmitStatusdisableId").prop('tabindex', '-1');
	
		// Do not allow tabbing into the diagnosis description fields
	 	for (var diagCnt = 0; diagCnt < <%= DIAGNOSIS_MAX_ROWS %> + 1; diagCnt++) {
			var diagCodeId = "<portlet:namespace />diagnosisCodeDesc" + diagCnt;
			$('#' + diagCodeId).prop('tabindex', '-1');
		}

		// Test for hide add diagnosis link
		if ($("#<portlet:namespace/>diagnosisCodeAdditional11").is(":visible")) {
			$('#<portlet:namespace/>diagnosis-table-add').hide();
		}
		
		// Do not allow tabbing into the procesure description fields
		for (var procCnt = 1; procCnt < <%= PROCEDURE_MAX_ROWS %> + 1; procCnt++) {
			var procCodeId = "<portlet:namespace />procedureDesc" + procCnt;
	
			$('#' + procCodeId).prop('tabindex', '-1');
		}

		// Test for hide add procedure link
		if ($("#<portlet:namespace/>procedureFromCode25").is(":visible")) {
			$('#<portlet:namespace/>procedure-table-add').hide();
		}

		// Disable Withdrawn Reason if Authorization Status is not "Completed - Withdrawn"
		setWithdrawnReasonStatus(false);
		
		// Set the initial save and submit button status
		setSaveButtonStatus();
		setSubmitButtonStatus();
		
		// Adjust the field widths
		adjustMemberDataWidth();
	    adjustDiagnosisLines(true);
	    adjustClaimLines(true);
	});

	$(window).resize(function () {
		
		// Reset the fields widths
	    adjustMemberDataWidth();
	    adjustDiagnosisLines(false);
	    adjustClaimLines(false);
	});
	
	function getNamespace() {
		return('<portlet:namespace/>');
	}
	
	function adjustDiagnosisLines(firstFlag) {
		
		// Set the default AUI input width and label height
		var inputWidth = parseInt($("#<portlet:namespace/>authInboundChannel").css("width"));
		var labelHeight = parseInt($("label[for=<portlet:namespace/>diagnosisCodePrimary]").css("height")) + 10;

		// Adjust diagnosis lines
		var diagCodeWidth = inputWidth / 2;
		var diagDescWidth = inputWidth;
		
		// Set the primary diagnosis code line
		$("#<portlet:namespace/>diagnosisCodePrimary").css("cssText", "width:" + diagCodeWidth + "px !important");
		if ($("#<portlet:namespace/>diagnosisCodeDesc0").val() == diagnosisError) {
			$("#<portlet:namespace/>diagnosisCodeDesc0").css("cssText", "width:" + diagDescWidth + "px !important;border-color:red; color:red");
		} else {
			$("#<portlet:namespace/>diagnosisCodeDesc0").css("cssText", "width:" + diagDescWidth + "px !important");
		}
		$("#<portlet:namespace/>diagnosisCodeDesc0").css('margin-top', labelHeight);
		
		// Line up the right side of the description with the other fields
		var address2EndLoc = $("#<portlet:namespace/>memberAddressLine2").offset().left + parseInt($("#<portlet:namespace/>memberAddressLine2").css("width"));
		var descEndLoc = $("#<portlet:namespace/>diagnosisCodeDesc0").offset().left + diagDescWidth;
		diagDescWidth = diagDescWidth - (descEndLoc - address2EndLoc);
		$("#<portlet:namespace/>diagnosisCodeDesc0").css("cssText", "width:" + diagDescWidth + "px !important");

		if (!firstFlag) {
			diagDescWidth = $("#<portlet:namespace/>diagnosisCodeDesc0").css("width");

			// Set the remaining diagnosis code lines
			for (var i = 1; i <= <%= DIAGNOSIS_MAX_ROWS %>; i++) {
				
				// Set the diagnosis code width
				$("#<portlet:namespace/>diagnosisCodeAdditional" + i).css("cssText", "width:" + diagCodeWidth + "px !important");
	
				// Set the diagnosis description width
				if ($("#<portlet:namespace/>diagnosisCodeDesc" + i).val() == diagnosisError) {
					$("#<portlet:namespace/>diagnosisCodeDesc" + i).css("cssText", "width:" + diagDescWidth + " !important;border-color:red; color:red");
				} else {
					$("#<portlet:namespace/>diagnosisCodeDesc" + i).css("cssText", "width:" + diagDescWidth + " !important");
				}
			}
		}
	}
		
	function adjustClaimLines(firstFlag) {

		// Get the column location
		var pad = 3;
		var procCodeLoc = $("#procCodeLabelId").offset().left;
		var procDescLoc = $("#procDescLabelId").offset().left;
		var procDecisionLoc = $("#procDecisionLabelId").offset().left;
		var procReasonLoc = $("#procReasonLabelId").offset().left;
		var procUnitLoc = $("#procUnitLabelId").offset().left;
		var endLoc = $("#<portlet:namespace/>authPatientRelation").offset().left + parseInt($("#<portlet:namespace/>authPatientRelation").css("width"));
		
		// Adjust the column widths
		var procCodeWidth = procDescLoc - procCodeLoc - pad;
		var procDescWidth = procDecisionLoc - procDescLoc - pad;
		var procDecisionWidth = procReasonLoc - procDecisionLoc - pad;
		var procReasonWidth = procUnitLoc - procReasonLoc - pad;
		var procUnitWidth = endLoc - procUnitLoc;
		
		var lastIdx = <%= PROCEDURE_MAX_ROWS %>;
		if (!firstFlag) {
			lastIdx = 1;
		}

		// Set the claim lines
		for (var i = 1; i <= lastIdx; i++) {
			$("#<portlet:namespace/>procedureFromCode" + i).css("cssText", "width:" + procCodeWidth + "px !important");
			if ($("#<portlet:namespace/>procedureDesc" + i).val() == procedureError) {
				$("#<portlet:namespace/>procedureDesc" + i).css("cssText", "width:" + diagDescWidth + "px !important;border-color:red; color:red");
			} else {
				$("#<portlet:namespace/>procedureDesc" + i).css("cssText", "width:" + procDescWidth + "px !important");
			}
			$("#<portlet:namespace/>procedureDecision" + i).css("cssText", "width:" + procDecisionWidth + "px !important");
			$("#<portlet:namespace/>procedureDecisionReason" + i).css("cssText", "width:" + procReasonWidth + "px !important");
			$("#<portlet:namespace/>procedureUnits" + i).css("cssText", "width:" + procUnitWidth + "px !important");
		}
	}

	function getMinRequestedDate() {
		var retValue = "";
		var creationDate = $("#<portlet:namespace/>hiddenCreationDate").val().substring(0, 10);

		// min requested date cannot be before the creation date.
		retValue = creationDate;
		
		return retValue;
	}


	YUI().use('aui-node','aui-modal','event',function(Y) {
		
		/**********************************script for pop up which appears when user makes a change to the procedure information again after initial submission*******************************************/
		var modalUpdate = new Y.Modal({
	        bodyContent: 'These updates will be sent to the health plan instead of the previous entered values',
	        centered: true,
	        destroyOnHide: false,
	        modal: true,
	        render: '#modal',
	        resizable: {
	        	handles: 'b, r'
	        },
	        visible: true,
	        width: 450
	    }).render();

		modalUpdate.addToolbar(
	        [
	          {
	            label: 'Continue',
	            on: {
	              click: function() {
	           	    Y.one('.procForm').set('action',"<%=procedureInformationPageURL%>");
	           	    Y.one('.procForm').submit();
	           		showBusySign('ProcedureInformationDetails');
	              }
	            }
	          },
	          {
	            label: 'Cancel',
	            on: {
	              click: function() {
	            	  
	            	// Hide this pop up
	                modalUpdate.hide();
	              }
	            }
	          }
	        ]
	    );
        modalUpdate.hide();

		/**********************************script for pop up which appears when user clicks on save button*******************************************/
		var modalSave = new Y.Modal({
            bodyContent: 'Your updates will be saved. Please click Submit button to submit the changes.',
            centered: true,
            destroyOnHide: false,
            modal: true,
            render: '#modal',
            resizable: {
            	handles: 'b, r'
	        },
	        visible: true,
	        width: 450
	    }).render();
	
		modalSave.addToolbar(
	      [
	        {
	          label: 'Continue',
	          on: {
	            click: function() {
	                
	        		// Remove the disabled property.  This property prevents the checkbox value being read by the backend.
	        		$('.fasttrack').removeProp("disabled");

					Y.one('.procForm').set('action',"<%=procedureInformationPageURL%>");
	         		Y.one('.procForm').submit();
	         		showBusySign('ProcedureInformationDetails');
	            }
	          }
	        },
            {
              label: 'Cancel',
              on: {
                click: function() {
            	  
                  // Hide this pop up
                  modalSave.hide();
                }
              }
            }
	      ]
	    );
        modalSave.hide();

        Y.one('.procForm').on('change',function(e) {
		
			/*************** Disabling SAVE button until user changes any of the fields in the form ************/
			setSaveButtonStatus();				
		});
	
		Y.one('.submitAction').on('click',function(e){
				
			/********* Preventing form submission when any validation fails in the form ************/
			if(Y.one('.error-field')){
				Y.one('.error-field').focus();
				e.preventDefault();
			} else{
				
				if (isValidForm() && isValidOptionalProcedures()) {
					var modal;
					e.preventDefault();
					
					var msg = "";
					if ($("#<portlet:namespace/>authStatus2").val() == "<%= PriorAuthConstants.COMPLETED_WITHDRAWN_CODE %>") {
						
						/********************************** script for pop up which appears when user clicks on submit button for a withdrawn status *******************************************/
						msg = "You are about to Void this authorization and it will no longer be valid. Are you sure you want to continue?";
					} else {

						/********************************** script for pop up which appears when user clicks on submit button *******************************************/
						msg = "This authorization has been indicated as ready to be sent to the Health Plan and will be sent in the next file transfer.";
					}
					
					modal = new Y.Modal({
						bodyContent: msg,
						centered: true,
						destroyOnHide: false,
						modal: true,
						render: '#modal',
						resizable: {
						  handles: 'b, r'
						},
						visible: true,
						width: 450
					}).render();
	
	                modal.addToolbar([
                      {
                        label: 'Continue',
                        on: {
                          click: function() {
                       	    Y.one('.procForm').set('action',"<%=procedureSubmitURL%>");
                       	    Y.one('.procForm').submit();
                       		showBusySign('ProcedureInformationDetails');
                          }
                        }
                      },
                      {
	                    label: 'Cancel',
                        on: {
                          click: function() {
                            modal.destroy();
                      	    modal.hide();

                         }
                        }
                      }
                  ]);
				} else if(!isValidOptionalProcedures()){

					/**********************************script for pop up which appears when user leaves out some fields in procedure table row and tries to submit. If one value is entered all the other mandatory columns are required*******************************************/
					var modal;
					e.preventDefault();
					
					 var modal = new Y.Modal(
						{
							bodyContent: 'A decision, decision reason, and units must be entered for each procedure code entered. Please fill in these fields prior to saving or submitting.',
							centered: true,
							headerContent: 'Error in procedures.',
							modal: true,
							render: '#modal',
							resizable: {
								handles: 'b, r'
							},
							visible: true,
							width: 450
						}).render();
	
		                modal.addToolbar(
		                  [
		                    {
		                      label: 'Continue',
		                      on: {
		                        click: function() {
		                      	  modal.destroy();
		                     	  modal.hide();
		                        }
		                      }
		                    },
		                    {
			                  label: 'Cancel',
		                      on: {
		                        click: function() {
		                         modal.destroy();
		                   	     modal.hide();
		                        }
		                      }
		                    }
		                  ]
		                );
		      		}
				}
			});
	
			/******** To process CANCEL button action ***************/
			Y.one('.cancelAction').on('click',function(e){
				Y.one('.procForm').set('action',"<%=procedureInformationCancelURL%>");
	            Y.one('.procForm').submit();
			});
 
			/********* Preventing form save when any validation fails in the form ************/
			Y.one('.saveAction').on('click',function(e){
		
				if(Y.one('.error-field')){
					Y.one('.error-field').focus();
					e.preventDefault();
				} else {
					if (isValidForm() && isValidOptionalProcedures()) {
						e.preventDefault();
						
						if (workedByChanged) {
							var authSubmitStatusdisable2 = Y.one('#<portlet:namespace/>authSubmitStatusdisableId').val();
							if(authSubmitStatusdisable2=="Submitted") {
								var modal1;
								e.preventDefault();

								/**********************************script for pop up which appears when user makes a change to the procedure information again after initial submission*******************************************/
								modal1 = new Y.Modal(
				                   {
				                      bodyContent: 'These updates will be sent to the health plan instead of the previous entered values',
				                      centered: true,
				                      destroyOnHide: false,
				                      modal: true,
				                      render: '#modal',
				                      resizable: {
				                        handles: 'b, r'
				                      },
				                     
				                      visible: true,
				                      width: 450
				                    }).render();
		
				                  modal1.addToolbar(
				                    [
				                      {
				                        label: 'Continue',
				                        on: {
				                          click: function() {
				                       	    Y.one('.procForm').set('action',"<%=procedureInformationPageURL%>");
				                       	    Y.one('.procForm').submit();
				                       		showBusySign('ProcedureInformationDetails');
				                          }
				                        }
				                      },
				                      {
				                        label: 'Cancel',
				                        on: {
				                          click: function() {
				                        	  
				                        	// Destroy and hide this pop up
						                    modal1.destroy();
				                            modal1.hide();
				                          }
				                        }
				                      }
					                ]
					              );
							} else {
								if (isPending() || isInProcess()) {
									Y.one('.procForm').set('action',"<%=procedureInformationPageURL%>");
					         		Y.one('.procForm').submit();
					         		showBusySign('ProcedureInformationDetails');
								} else {
									var modal2;
									e.preventDefault();
									/**********************************script for pop up which appears when user clicks on save button*******************************************/
									modal2 = new Y.Modal({
									            bodyContent: 'Your updates will be saved. Please click Submit button to submit the changes.',
									            centered: true,
									            destroyOnHide: false,
									            modal: true,
									            render: '#modal',
									            resizable: {
								                	handles: 'b, r'
								    	        },
								    	        visible: true,
								    	        width: 450
								    }).render();
								
								    modal2.addToolbar(
								      [
								        {
								          label: 'Continue',
								          on: {
								            click: function() {
								                
								        		// Remove the disabled property.  This property prevents the checkbox value being read by the backend.
								        		$('.fasttrack').removeProp("disabled");
	
												Y.one('.procForm').set('action',"<%=procedureInformationPageURL%>");
								         		Y.one('.procForm').submit();
								         		showBusySign('ProcedureInformationDetails');
								            }
								          }
								        },
					                    {
						                  label: 'Cancel',
					                      on: {
					                        click: function() {
				                        	  
					                          // Destroy and hide this pop up
					                          modal2.destroy();
					                          modal2.hide();
					                        }
					                      }
					                    }
								      ]
								    );
								}
							}
						} else {
							var modal;
							var workedByPopupFlag = $("#<portlet:namespace/>hiddenWorkedByPopupFlag").val();

							if (workedByPopupFlag == "true") {

								/**********************************script for pop up which appears when user has not changed the Being Worked By field *******************************************/
								modal = new Y.Modal(
						            {
						               bodyContent: 'Please remember to update the "Being Worked By" field to reflect the correct selection.  Click Cancel to update or Continue to Save.',
						               centered: true,
						               destroyOnHide: false,
						               modal: true,
						               render: '#modal',
						               resizable: {
						                 handles: 'b, r'
						               },
						               visible: true,
						               width: 450
						            }).render();
				
						        modal.addToolbar(
						            [
						               {
						                 label: 'Continue',
						                   on: {
						                     click: function() {

						                    	// Hide the first pop up
									            modal.hide();
									            
												var authSubmitStatusdisable2 = Y.one('#<portlet:namespace/>authSubmitStatusdisableId').val();
												if (authSubmitStatusdisable2=="Submitted") {
													e.preventDefault();
													modalUpdate.show();
												} else {
													if (isPending() || isInProcess()) {
														Y.one('.procForm').set('action',"<%=procedureInformationPageURL%>");
										         		Y.one('.procForm').submit();
										         		showBusySign('ProcedureInformationDetails');
													} else {
														e.preventDefault();
														modalSave.show();
													}
												}
						                     }
						                   }
						               },
						               {
						                 label: 'Cancel',
						                   on: {
						                     click: function() {
										                        	  
											   // Hide the first pop up
						                       modal.hide();
						                     }
						                   }
						               }
							    	]
							    );
							} else {
					            
								var authSubmitStatusdisable2 = Y.one('#<portlet:namespace/>authSubmitStatusdisableId').val();
								if (authSubmitStatusdisable2=="Submitted") {
									e.preventDefault();
									modalUpdate.show();
								} else {
									if (isPending() || isInProcess()) {
										Y.one('.procForm').set('action',"<%=procedureInformationPageURL%>");
						         		Y.one('.procForm').submit();
						         		showBusySign('ProcedureInformationDetails');
									} else {
										e.preventDefault();
										modalSave.show();
									}
								}
							}
						}
					/**********************************script for pop up which appears when user leaves out some fields in procedure table row and tries to submit. If one value is entered all the other mandatory columns are required*******************************************/
					} else if(!isValidOptionalProcedures()){
				
						e.preventDefault();
					
						var modal = new Y.Modal(
							{
								bodyContent: 'A decision, decision reason, and units must be entered for each procedure code entered. Please fill in these fields prior to saving or submitting.',
								centered: true,
								headerContent: 'Error in procedures.',
								modal: true,
								render: '#modal',
								resizable: {
									handles: 'b, r'
								},
								visible: true,
								width: 450
							}).render();
		
			                modal.addToolbar(
			                  [
			                    {
			                      label: 'Continue',
			                      on: {
			                        click: function() {
			                      	  modal.destroy();
			                   	  	  modal.hide();
			                        }
			                      }
			                    },
			                    {
				                  label: 'Cancel',
			                      on: {
			                        click: function() {
			                         modal.destroy();
			                   	     modal.hide();
			                        }
			                      }
			                    }
				              ]
				            );
						}
					}
			});
	});
	
	YUI().ready('aui-node','event',
		  function(Y) {

			/*
			   Set the page to read only if the user is in the HealthPlanEmployee or AvalonEmployeeROPA group and not in 
			   AvalonAdmin, AvalonEmployee, AvalonProvider, PortalApprover, or PortalCreator groups.
			*/
			<% 
				boolean readOnlyFlag = false;
				boolean hpEmployeeFlag = false;
				boolean avalonEmployeeRoPaFlag = false;
				boolean notReadOnlyGroupFlag = false;
				User userU = themeDisplay.getUser();
				long[] groups = userU.getUserGroupIds();
				int length = groups.length;
				for (int i = 0; i < length; ++i) {
					String nextGroup = UserGroupLocalServiceUtil.getUserGroup(groups[i]).getName();
					if (nextGroup.equalsIgnoreCase("HealthPlanEmployee")) {
						hpEmployeeFlag = true;
					} else if (nextGroup.equalsIgnoreCase("AvalonEmployeeROPA")) {
						avalonEmployeeRoPaFlag = true;
					} else if (nextGroup.equalsIgnoreCase("AvalonAdmin") || nextGroup.equalsIgnoreCase("AvalonEmployee") || 
							   nextGroup.equalsIgnoreCase("AvalonProvider") || nextGroup.equalsIgnoreCase("PortalApprover") || 
							   nextGroup.equalsIgnoreCase("PortalCreator")) {
						notReadOnlyGroupFlag = true;
					}
				}
				if (!notReadOnlyGroupFlag && (hpEmployeeFlag || avalonEmployeeRoPaFlag)) {
					readOnlyFlag = true;
				}
			%>
			if (<%= readOnlyFlag %>) {

		  		// Make the page readonly
	  		 	Y.one('.avalon__proc_portlet').addClass('optdisable');
				
				// Disable the tabbing for read only
				disableTabbing();
				
				// Disable the dropdowns in IE and Firefox
				disableDropdowns();
			} else if(Y.one('#<portlet:namespace/>authSubmitStatusdisableId')) {
		  	   var authSubmitStatusdisable = Y.one('#<portlet:namespace/>authSubmitStatusdisableId').val();
		  	   if(authSubmitStatusdisable!=null && authSubmitStatusdisable!="" && authSubmitStatusdisable!='undefined') {
		  			
		  		 if(authSubmitStatusdisable=="Void HP - Submitted" || authSubmitStatusdisable=="Void-Submitted" || authSubmitStatusdisable=="Void"||authSubmitStatusdisable=="Void HP") {

			  		// Make the page readonly
		  		 	Y.one('.avalon__proc_portlet').addClass('optdisable');
			  		
		  		 	disableTabbing();
					
					// Disable the dropdowns in IE and Firefox
					disableDropdowns();
		  		 } else if(authSubmitStatusdisable=="Sent") {
	  				Y.all('.sntdis').addClass('optdisable');
	  			
					//in sent status only certain fields are disabled. Not the entire page.
	  				Y.all('.sntdis').setAttribute("tabindex","-1");
					
					// Disable add diagnosis and add procedure links when in sent status
	  				disableTabbingInSentStatus();
					
	  				// fasttrack added Jan 19, 2017
					$('.fasttrack').prop("readonly", true);
					$('.fasttrack').prop("disabled", true);

	  			 }
			   }
			}
	});

	YUI().ready('aui-node','event',
	  function(Y) {
		setSubmitButtonStatus();
		setOriginalDecisionState();
	});

	var cancelUrl = '<%=procedureInformationCancelURL.toString()%>';
	YUI().ready('aui-node','event',
	  function(Y) {
		
		var readonlyCheck="<%=renderRequest.getAttribute("readonly") %>";
	
		var disCheck = Y.all('.checkForDisable');  
		var dislinkCheck = Y.all('.checkForLinkDisable');  

		if(readonlyCheck==="true"){
			disCheck.addClass('optdisable'); 
		   
			//A.one("#checkForLinkDisable").set('display',hidden);
			dislinkCheck.addClass('linDisable'); 
		}

		if ((browserStr == "IE") || (browserStr == "Firefox") || (browserStr == "unknown")) {

			// This is only called for date fields that are disabled.  Disable the calendar icon if a date field call this.
			if (Y.one("#<portlet:namespace/>authRequestedDate")) {
				if ($("#<portlet:namespace/>authRequestedDate").hasClass("optdisable")) {

					// Destroy the datepicker from the input field
					$("#<portlet:namespace/>authRequestedDate").datepicker("destroy");
				}
			} else if (Y.one("#<portlet:namespace/>serviceBeginDate")) {
				if ($("#<portlet:namespace/>serviceBeginDate").hasClass("optdisable")) {

					// Destroy the datepicker from the input field
					$("#<portlet:namespace/>serviceBeginDate").datepicker("destroy");
				}
			} else if (Y.one("#<portlet:namespace/>serviceEndDate")) {
				if ($("#<portlet:namespace/>serviceEndDate").hasClass("optdisable")) {

					// Destroy the datepicker from the input field
					$("#<portlet:namespace/>serviceEndDate").datepicker("destroy");
				}
			} else if (Y.one("#<portlet:namespace/>authDecisionDate")) {
				if ($("#<portlet:namespace/>authDecisionDate").hasClass("optdisable")) {

					// Destroy the datepicker from the input field
					$("#<portlet:namespace/>authDecisionDate").datepicker("destroy");
				}
			} else if (Y.one("#<portlet:namespace/>authDueDate")) {
				if ($("#<portlet:namespace/>authDueDate").hasClass("optdisable")) {

					// Destroy the datepicker from the input field
					$("#<portlet:namespace/>authDueDate").datepicker("destroy");
				}
			}
		}

	  }
	);

	YUI().use('aui-timepicker', function(Y) {
	    new Y.TimePicker(
	      {
	        trigger: 'input.timepicker',
	        popover: {
	          zIndex: 1
	        },
	        on: {
	          selectionChange: function(event) {
	            console.log(event.newSelection)
	          }
	        }
	      }
	    );
	  }
	);

	YUI().use('aui-node','aui-modal','event',function(Y) {
	     var authUpdated = $("#auSubStatid").val();
	     if (authUpdated == "Updated") {
	            if(!Y.one('.procForm').hasClass('dirty')) {
	    			if (browserStr == "IE") {
						window.onbeforeunload = function(e) {
							e = e || window.event;
						    e.preventDefault = true;
						    e.cancelBubble = true;
						    e.returnValue = 'You have made updates to this authorization that affect the file for the Health Plan but have not submitted them. If you do not submit the updates, the Health Plan authorization record will not contain these updates.';
					    }
	    			} else {
    					$('form').areYouSure( {'message': 'You have made updates to this authorization that affect the file for the Health Plan but have not submitted them. If you do not submit the updates, the Health Plan authorization record will not contain these updates.'} );
	    			}
	            }
	     } else {
   			$('form').areYouSure( {'message':exitString} );
	     }
	});
	
	function onSubmit() {
		window.onbeforeunload = function(e) {
                e = e || window.event;
                e.preventDefault = false;
                e.cancelBubble = false;
		}
		$('form').areYouSure( {'silent':true} );
	}

	var validationInProcessFlag = false;
	function checkDuplicateDiagnosis(idx, val) {
		var retVal = false;
		var chkVal = val;
		var nextVal = null;

		// Do not test empty fields
		if (val != '') {

			if (idx != 0) {
				
				// Since the primary diagnosis code is not selected make the comparison to the primary
				var priVal = $('#<portlet:namespace/>diagnosisCodePrimary').val();

				if (chkVal == priVal) {
					retVal = true;
				}
			}
	
			if (retVal == false) {
				
				// Compare to every diagnosis code except the selected one
				for (var d_cnt1 = 1; d_cnt1 <= <%= DIAGNOSIS_MAX_ROWS %>; d_cnt1++) {
					if (d_cnt1 != idx) {
						nextVal = $('#<portlet:namespace/>diagnosisCodeAdditional' + d_cnt1).val()
						if (nextVal != '') {
							if (chkVal == nextVal) {
								retVal = true;
								break;
							}
						}
					}
				}
			}
		}

		// Validate the remaining diagnosis codes that are not empty
		if (validationInProcessFlag == false) {
			validationInProcessFlag = true;
			if (idx != 0) {
				
				// Force validation of the primary diagnosis code
				$('#<portlet:namespace/>diagnosisCodePrimary').focus();
				$('#<portlet:namespace/>diagnosisCodePrimary').blur();
			}
	
			for (var d_cnt2 = 1; d_cnt2 <= <%= DIAGNOSIS_MAX_ROWS %>; d_cnt2++) {
				if (d_cnt2 != idx) {
					nextVal = $('#<portlet:namespace/>diagnosisCodeAdditional' + d_cnt2).val()
					if (nextVal != '') {

						// Force validation of the diagnosis code
						$('#<portlet:namespace/>diagnosisCodeAdditional' + d_cnt2).focus();
						$('#<portlet:namespace/>diagnosisCodeAdditional' + d_cnt2).blur();
					}
				}
			}
			validationInProcessFlag = false;
		}
		
		return retVal;
	}

	function checkDuplicateProcedure(idx, val) {
		var retVal = false;
		var chkVal = val;
		var nextVal = null;
		var procCodeName = null;

		procCodeName = 'procedureFromCode';

		// Do not test empty fields
		if (val != '') {
			
			// Compare to every procedure code except the selected one
			for (var p_cnt1 = 1; p_cnt1 <= <%= PROCEDURE_MAX_ROWS %>; p_cnt1++) {
				if (p_cnt1 != idx) {
					nextVal = $('#<portlet:namespace/>' + procCodeName + p_cnt1).val();
					if (nextVal != '') {
						if (chkVal == nextVal) {
							retVal = true;
							break;
						}
					}
				}
			}
		}

		// Validate the remaining procedure codes that are not empty
		if (validationInProcessFlag == false) {
			validationInProcessFlag = true;
			for (var p_cnt2 = 1; p_cnt2 <= <%= DIAGNOSIS_MAX_ROWS %>; p_cnt2++) {
				if (p_cnt2 != idx) {
					nextVal = $('#<portlet:namespace/>' + procCodeName + p_cnt2).val()
					if (nextVal != '') {

						// Force validation of the procedure code
						$('#<portlet:namespace/>' + procCodeName + p_cnt2).focus();
						$('#<portlet:namespace/>' + procCodeName + p_cnt2).blur();
					}
				}
			}
			validationInProcessFlag = false;
		}

		return retVal;
	}
	
	function setAuthDecisionDate(val) {
		if ((val == "<%= PriorAuthConstants.IN_PROCESS_INTAKE_REVIEW_CODE %>") || 
			(val == "<%= PriorAuthConstants.IN_PROCESS_NURSE_REVIEW_CODE %>") || 
			(val == "<%= PriorAuthConstants.IN_PROCESS_NURSE_READY_CODE %>") || 
			(val == "<%= PriorAuthConstants.IN_PROCESS_NURSE_WAITING_CODE %>") ||
			(val == "<%= PriorAuthConstants.IN_PROCESS_PHYSICIAN_REVIEW_CODE %>")) {

			// Clear Original Decision Date
			$('#<portlet:namespace/>authDecisionDate').val("");
			$('#<portlet:namespace/>authDecisionTime').val("");

			addWatermark('<portlet:namespace/>authDecisionTime', false);
		} else if ((val == "<%= PriorAuthConstants.COMPLETED_APPROVED_DESC %>") || 
				   (val == "<%= PriorAuthConstants.COMPLETED_PARTIALLY_APPROVED_DESC %>") || 
				   (val == "<%= PriorAuthConstants.COMPLETED_DENIED_DESC %>") || 
				   (val == "<%= PriorAuthConstants.COMPLETED_ADMINISTRATIVE_DENIAL_DESC %>") || 
				   (val == "<%= PriorAuthConstants.COMPLETED_CLINICAL_DENIAL_DESC %>") || 
				   (val == "<%= PriorAuthConstants.COMPLETED_WITHDRAWN_DESC %>")) {
			
			// Set Original Decision Date to the current date
			$('#<portlet:namespace/>authDecisionDate').val(getToday());
			$('#<portlet:namespace/>authDecisionTime').val(setTime('0'));
		}
	}

	function isPending() {
		var pendingFlag = false;

		<% for (int i = 1; i <= PROCEDURE_MAX_ROWS; i++) { %>
			var decision = $('#<portlet:namespace/>procedureDecision<%=i%>').val();
			var reason = $('#<portlet:namespace/>procedureDecisionReason<%=i%>').val();

			if (!pendingFlag) {
				if ((decision == "P") || (reason == "P")) {
					pendingFlag = true;
				}
			}
		<% } %>
		
		return pendingFlag;
	}

	function isInProcess() {
		var inProcessFlag = false;	
		var authStatus2 = $('#<portlet:namespace/>authStatus2').val();

		if ((authStatus2 == "") || (authStatus2 == "<%= PriorAuthConstants.IN_PROCESS_INTAKE_REVIEW_CODE %>") || 
			(authStatus2 == "<%= PriorAuthConstants.IN_PROCESS_READY_FOR_COMPL_CODE %>") || 
			(authStatus2 == "<%= PriorAuthConstants.IN_PROCESS_NURSE_REVIEW_CODE %>") || 
			(authStatus2 == "<%= PriorAuthConstants.IN_PROCESS_NURSE_READY_CODE %>") || 
			(authStatus2 == "<%= PriorAuthConstants.IN_PROCESS_NURSE_WAITING_CODE %>") ||
			(authStatus2 == "<%= PriorAuthConstants.IN_PROCESS_PHYSICIAN_REVIEW_CODE %>")) {
	    	inProcessFlag = true;
	    }

		return inProcessFlag;
	}
	
	function setOriginalDecisionState() {
		var stat = $('#<portlet:namespace/>authStatus2').val();

		// Set the Original Decision data and time disable status based on the status.
	    if ((stat == "") || (stat == "<%= PriorAuthConstants.IN_PROCESS_INTAKE_REVIEW_CODE %>") || 
			(stat == "<%= PriorAuthConstants.IN_PROCESS_NURSE_REVIEW_CODE %>") || 
			(stat == "<%= PriorAuthConstants.IN_PROCESS_NURSE_READY_CODE %>") || 
			(stat == "<%= PriorAuthConstants.IN_PROCESS_NURSE_WAITING_CODE %>") ||
			(stat == "<%= PriorAuthConstants.IN_PROCESS_PHYSICIAN_REVIEW_CODE %>")) {

	    	$("#<portlet:namespace/>authDecisionDate").attr("disabled", "disabled");
			$("#<portlet:namespace/>authDecisionTime").attr("disabled", "disabled");
	
			if ((browserStr == "IE") || (browserStr == "Firefox") || (browserStr == "unknown")) {
				$("#<portlet:namespace/>authDecisionDate").datepicker("disable");
			}
		} else {
			$("#<portlet:namespace/>authDecisionDate").removeAttr("disabled");
			$("#<portlet:namespace/>authDecisionTime").removeAttr("disabled");
		}
	}
	
	function setFormDirty() {
		$('.procForm').addClass('dirty');
	}
	
	function setSaveButtonStatus() {

		// Disable the save button until all the fields are valid
		if (isValidForm() && formChangedFlag && ($("#<portlet:namespace/>authStatus2").val() != "<%= PriorAuthConstants.COMPLETED_WITHDRAWN_CODE %>")) {
			
			// Enable SAVE button
			$("#<portlet:namespace/>saveProcedureInfo").removeAttr("disabled");
		} else {
			
			// Disable SAVE button
			$("#<portlet:namespace/>saveProcedureInfo").attr("disabled", "disabled");
		}
		
		// Add the mouse over text for the save button
		$("#<portlet:namespace/>saveProcedureInfo").prop("title", toolTipSave);
	}

	function setSubmitButtonStatus() {
		var stat = $('#<portlet:namespace/>authSubmitStatusdisableId').val();
		var saveUpdateFlag = false;

		// Test the submit status for saved
		if ((stat.toLowerCase() == 'saved') || (stat.toLowerCase() == 'updated')) {
			saveUpdateFlag = true;
		}
		
		// Set the submit button disable status based on the form data or the status is Withdrawn.
		if ((isValidForm() && isValidOptionalProcedures() && !isPending() && !isInProcess() && (saveUpdateFlag || formChangedFlag)) ||
		    (isValidForm() && ($("#<portlet:namespace/>authStatus2").val() == "<%= PriorAuthConstants.COMPLETED_WITHDRAWN_CODE %>"))) {
			$("#<portlet:namespace/>submitProcedureInfo").removeAttr("disabled");
		} else {
			$("#<portlet:namespace/>submitProcedureInfo").attr("disabled", "disabled");
		}
		
		// Add the mouse over text for the save button
		$("#<portlet:namespace/>submitProcedureInfo").prop("title", toolTipSubmit);
	}
	
	function checkWithdrawnReasonError() {
		var errorFlag = false;

		// Make Withdrawn Reason a required field it the status is withdrawn
		if ($("#<portlet:namespace/>authStatus2").val() == "<%= PriorAuthConstants.COMPLETED_WITHDRAWN_CODE %>") {

			if ($("#<portlet:namespace/>authWithdrawnReason").prop("selectedIndex") == 0) {
				errorFlag = true;
			}
		}
		
		cssWithdrawnReasonError(errorFlag);
	}
	
	function cssWithdrawnReasonError(errorFlag) {
		var itm = "<portlet:namespace/>authWithdrawnReason";
		var errorColor = "red";
		var validColor = "green";
		
		// Set label and select red for an error.  Set them green for no error.
		if (errorFlag) {
			$('#authWithdrawnReasonError').show();
			$('label[for=' + itm + ']').css("color", errorColor);

			$("#" + itm).css("color", errorColor);
			$("#" + itm).css("border-color", errorColor);
		} else {
			$('#authWithdrawnReasonError').hide();
			$('label[for=' + itm + ']').css("color", validColor);

			$("#" + itm).css("color", validColor);
			$("#" + itm).css("border-color", validColor);
		}
	}
	
	function isValidOptionalProcedures() {
		var validFlag = true;
		
		<% for (int i = 2, n = PROCEDURE_MAX_ROWS; i <= n; i++) { %>
			var fromId = $("#<portlet:namespace/>procedureFromCode<%= i %>").val();
			var decisionId = $("#<portlet:namespace/>procedureDecision<%= i %>").prop("selectedIndex");
			var reasonId = $("#<portlet:namespace/>procedureDecisionReason<%= i %>").prop("selectedIndex");
			var unitId = $("#<portlet:namespace/>procedureUnits<%= i %>").val();
			// fasttrack added Jan 19, 2017
			var fasttrackChecked = $("#<portlet:namespace/>procedureFastTrack<%= i %>").prop("checked");

			if (validFlag) {
				if ((fromId.length > 0) || (decisionId > 0) || (reasonId > 0) || (unitId.length > 0)) {

					// Make sure all the required fields have data 
					if ((fromId.length == 0) || (decisionId == 0) || (reasonId == 0) || (unitId.length == 0)) {
						validFlag = false;
									      
					}
				}
			}
		<% } %>
 
		return validFlag;
	}
	
	function isValidForm() {
		var commonValid = false;
		var primaryDiagnosisValid = false;
		var diagnosisValid = false;
		var firstProcedureValid = false;
		var procedureValid = false;
		var allCodesValid = false;

		var authStatus = $("#<portlet:namespace/>authStatus2").prop("selectedIndex");
		var authWithdrawnReason = $("#<portlet:namespace/>authWithdrawnReason").prop("selectedIndex");
		
		// Initialize the tool tip for the save button
		toolTipSave = "";
		toolTipSubmit = "";
		
		// Only check the withdrawn reason for a Completed - Withdrawn status
		if ($("#<portlet:namespace/>authStatus2").val() == "<%= PriorAuthConstants.COMPLETED_WITHDRAWN_CODE %>") {

			// Set all of the flags to valid
			commonValid = true;
			primaryDiagnosisValid = true;
			diagnosisValid = true;
			firstProcedureValid = true;
			procedureValid = true;
			allCodesValid = true;

			// Validate the withdrawl reason
			if (authWithdrawnReason == 0) {
				commonValid = false;
				toolTipSubmit += "Withdrawn Reason is not valid";
			}
			
			toolTipSave += "An Authorization Status of <%= PriorAuthConstants.COMPLETED_WITHDRAWN_DESC %> cannot be saved";
		} else {
			var errorStr = "";
			var authInboundChannel = $("#<portlet:namespace/>authInboundChannel").prop("selectedIndex");
			var authRequestedDate = $("#<portlet:namespace/>authRequestedDate").val().trim();
			var authRequestedTime = $("#<portlet:namespace/>authRequestedTime").val();
			var authPriority = $("#<portlet:namespace/>authPriority").prop("selectedIndex");
			var serviceBeginDate = $("#<portlet:namespace/>serviceBeginDate").val().trim();
			var serviceEndDate = $("#<portlet:namespace/>serviceEndDate").val().trim();
			var authDecisionDate = $("#<portlet:namespace/>authDecisionDate").val().trim();
			var authDecisionTime = $("#<portlet:namespace/>authDecisionTime").val();
			var authDueDate = $("#<portlet:namespace/>authDueDate").val().trim();
			var authDueTime = $("#<portlet:namespace/>authDueTime").val();
			var authWorkedBy = $("#<portlet:namespace/>authWorkedBy").prop("selectedIndex");
			var diagnosisCodePrimary = $("#<portlet:namespace/>diagnosisCodePrimary").val();
			var procedureFromCode1 = $("#<portlet:namespace/>procedureFromCode1").val();
			var procedureDecision1 = $("#<portlet:namespace/>procedureDecision1").prop("selectedIndex");
			var procedureDecisionReason1 = $("#<portlet:namespace/>procedureDecisionReason1").prop("selectedIndex");

			var procedureUnits1 = $("#<portlet:namespace/>procedureUnits1").val();
			var authRequestedDate_html5 = document.getElementById("<portlet:namespace/>authRequestedDate");
			var serviceBeginDate_html5 = document.getElementById("<portlet:namespace/>serviceBeginDate");
			var serviceEndDate_html5 = document.getElementById("<portlet:namespace/>serviceEndDate");
			var authDueDate_html5 = document.getElementById("<portlet:namespace/>authDueDate");
			
			// Test for HTML5 errors
			var dateFormatValid = true;
			var dueDateFormatValid = true;
			if ((browserStr != "unknown") && (browserStr != "IE") && (browserStr != "Firefox")) {
				if (authRequestedDate_html5.validity.badInput || serviceBeginDate_html5.validity.badInput || serviceEndDate_html5.validity.badInput || authDueDate_html5.validity.badInput) {
					dateFormatValid = false;
					
					// Test the due date
					if (authDueDate_html5.validity.badInput) {
						dueDateFormatValid = false;
					}
				}
			}
			
			// Set the due time if the date is set
			if (dueDateFormatValid && isValidDate(authDueDate, true)) {
				if (!isValidTime(authDueTime, true)) {
					
					// Set the time to 8 PM
					$("#<portlet:namespace/>authDueTime").val(setTime('20:00'));
					authDueTime = $("#<portlet:namespace/>authDueTime").val();
				}
			}
				
			// Validate common values
			if (dateFormatValid && (authInboundChannel > 0) && isValidDate(authRequestedDate, true) && isValidTime(authRequestedTime, true) && isValidDate(serviceBeginDate, true) && 
				isValidDate(serviceEndDate, true) && isValidDate(authDecisionDate, false) &&  isValidTime(authDecisionTime, false) && (authPriority > 0) && (authStatus > 0) &&
				isValidDate(authDueDate, true) &&  isValidTime(authDueTime, true)) {
				commonValid = true;
			}
	
			// Validate primary diagnosis values
			var diagnosisCodePrimaryDesc = $("#<portlet:namespace/>diagnosisCodeDesc0").val();
			if ((diagnosisCodePrimaryDesc != "") && (diagnosisCodePrimaryDesc != diagnosisError)) {
				primaryDiagnosisValid = true;
			}
			
			// Validate remaining diagnosis values
			diagnosisValid = true;
			var n = <%= DIAGNOSIS_MAX_ROWS %>;
			for (var i = 1; i <= n; i++) {
				var nextDiagnosisCodeDesc = $("#<portlet:namespace/>diagnosisCodeDesc" + i).val();
				
				if (nextDiagnosisCodeDesc == diagnosisError) {
					diagnosisValid = false;
					break;
				}
			}

			// Validate first procedure line
			var procedureCodePrimaryDesc = $("#<portlet:namespace/>procedureDesc1").val();
			if ((procedureCodePrimaryDesc != "") && (procedureCodePrimaryDesc != procedureError) && (procedureDecision1 > 0) && (procedureDecisionReason1 > 0) &&  isValidDigit(procedureUnits1, <%= PROCEDURE_UNIT_LEN %>, true, false)) {
				firstProcedureValid = true;
			}

			// Validate remaining procedure lines
			procedureValid = true;
			n = <%= PROCEDURE_MAX_ROWS %>;
			for (var i = 2; i <= n; i++) {
				var nextProcedureCodeDesc = $("#<portlet:namespace/>procedureDesc" + i).val();
				if (nextProcedureCodeDesc != "") {
					var nextProcedureDecision =$("#<portlet:namespace/>procedureDecision" + i).prop("selectedIndex");
					var nextProcedureDecisionReason =$("#<portlet:namespace/>procedureDecisionReason" + i).prop("selectedIndex");
					var nextProcedureUnits =$("#<portlet:namespace/>procedureUnits" + i).val();
	
					if ((nextProcedureCodeDesc == procedureError) || !isValidDigit(nextProcedureUnits, <%= PROCEDURE_CODE_LEN %>, false, false) || (nextProcedureDecision == 0) || (nextProcedureDecisionReason == 0)) {
						procedureValid = false;
						break;
					}
				}
			}
			
			// Determine if the diagnosis and procedure codes are valid
			allCodesValid = isAllCodesValid();
		
			// Build the toolTip for the Save and Submit buttons
			// Test the inbound channel
			if (authInboundChannel == 0) {
				errorStr = "The Inbound Channel is not valid";
				toolTipSave += addToToolTip(errorStr, toolTipSave.length);
				toolTipSubmit += addToToolTip(errorStr, toolTipSubmit.length);
			}
			
			// Test the request date format
			if (authRequestedDate_html5.validity.badInput || !isValidDate(authRequestedDate, true)) {
				errorStr = "The Request Date is not valid";
				toolTipSave += addToToolTip(errorStr, toolTipSave.length);
				toolTipSubmit += addToToolTip(errorStr, toolTipSubmit.length);
			}
			
			// Test the request time
			if (!isValidTime(authRequestedTime, true)) {
				errorStr = "The Request Time is not valid";
				toolTipSave += addToToolTip(errorStr, toolTipSave.length);
				toolTipSubmit += addToToolTip(errorStr, toolTipSubmit.length);
			}
			
			// Test the priority
			if (authPriority == 0) {
				errorStr = "The Priority is not valid";
				toolTipSave += addToToolTip(errorStr, toolTipSave.length);
				toolTipSubmit += addToToolTip(errorStr, toolTipSubmit.length);
			}
			
			// Test the service begin date
			if (serviceBeginDate_html5.validity.badInput || !isValidDate(serviceBeginDate, true)) {
				errorStr = "The Service Begin Date is not valid";
				toolTipSave += addToToolTip(errorStr, toolTipSave.length);
				toolTipSubmit += addToToolTip(errorStr, toolTipSubmit.length);
			}
			
			// Test the service end date
			if (serviceEndDate_html5.validity.badInput || !isValidDate(serviceEndDate, true)) {
				errorStr = "The Service End Date is not valid";
				toolTipSave += addToToolTip(errorStr, toolTipSave.length);
				toolTipSubmit += addToToolTip(errorStr, toolTipSubmit.length);
			}
			
			// Test the authorization status
			if (authStatus == 0) {
				errorStr = "The Authorization Status is not valid";
				toolTipSave += addToToolTip(errorStr, toolTipSave.length);
				toolTipSubmit += addToToolTip(errorStr, toolTipSubmit.length);
			}
			if (isInProcess()) {
				errorStr = "The Authorization Status must be one of the Completed states";
				toolTipSubmit += addToToolTip(errorStr, toolTipSubmit.length);
			}
			
			// Test the due date
			if (authDueDate_html5.validity.badInput || !isValidDate(authDueDate, true)) {
				errorStr = "The Due Date is not valid";
				toolTipSave += addToToolTip(errorStr, toolTipSave.length);
				toolTipSubmit += addToToolTip(errorStr, toolTipSubmit.length);
			}
			
			// Test the due time
			if (!isValidTime(authDueTime, true)) {
				errorStr = "The Due Time is not valid";
				toolTipSave += addToToolTip(errorStr, toolTipSave.length);
				toolTipSubmit += addToToolTip(errorStr, toolTipSubmit.length);
			}
			
			// Test the primary diagnosis code
			if (!primaryDiagnosisValid) {
				errorStr = "The Primary Diagnosis Code is not valid";
				toolTipSave += addToToolTip(errorStr, toolTipSave.length);
				toolTipSubmit += addToToolTip(errorStr, toolTipSubmit.length);
			}
			
			// Test the diagnosis codes
			if (!diagnosisValid) {
				errorStr = "Some Diagnosis Codes are not valid";
				toolTipSave += addToToolTip(errorStr, toolTipSave.length);
				toolTipSubmit += addToToolTip(errorStr, toolTipSubmit.length);
			}
			
			// Test the first procedure line
			if (!firstProcedureValid) {
				errorStr = "The first Procedure line is not valid";
				toolTipSave += addToToolTip(errorStr, toolTipSave.length);
				toolTipSubmit += addToToolTip(errorStr, toolTipSubmit.length);
			}
			
			// Test the procedure lines
			if (!procedureValid) {
				errorStr = "Some Procedures lines are not valid";
				toolTipSave += addToToolTip(errorStr, toolTipSave.length);
				toolTipSubmit += addToToolTip(errorStr, toolTipSubmit.length);
			}
	
			// Test for any pending procedure decision or decision reason
			if (isPending()) {
				toolTipSubmit += addToToolTip("There are pending Procedure Decisions or Decision Reasons", toolTipSubmit.length);
			}
			
			// Test for form changed
			if (!formChangedFlag) {
				errorStr = "None of the fields have been changed";
				if (toolTipSave.length == 0) {
					toolTipSave += errorStr;
				}
				if (toolTipSubmit.length == 0) {
					toolTipSubmit += errorStr;
				}
			}
		}
		return commonValid && primaryDiagnosisValid && diagnosisValid && firstProcedureValid && procedureValid && allCodesValid;
	}

	// Get start date for each browser type
	function getStartDate() {
		var dat;
		var d = "01";
		var m = "01";
		var y = "2016";

		if ((browserStr == "unknown") || (browserStr == "IE") || (browserStr == "Firefox")) {

			// Format for IE and Firefox is mm/dd/yyyy
			dat = m + "/" + d + "/" + y;
		} else {

			// Format for all other browsers is yyyy-mm-dd
			dat = y + "-" + m + "-" + d;
		}

		return dat;
	}

	// Convert the date to a string
	function convertDate(_dt) {
		var dat;
		var d = _dt.getDate();
		var m = _dt.getMonth() + 1;
		var y = _dt.getFullYear();

		if (d < 10) {
			d = "0" + d;
		}

		if (m < 10) {
			m = "0" + m;
		}

		if ((browserStr == "unknown") || (browserStr == "IE") || (browserStr == "Firefox")) {

			// Format for IE and Firefox is mm/dd/yyyy
			dat = m + "/" + d + "/" + y;
		} else {

			// Format for all other browsers is yyyy-mm-dd
			dat = y + "-" + m + "-" + d;
		}

		return dat;
	}

	// Get the current date
	function getToday() {
		var today = new Date();
		var dat = convertDate(today);
		return dat;
	}

	// Set the time  (the parameter is a 24 hour time.  Set the current time fo 0)
	function setTime(newTime) {
		var date = new Date();
		
		// Change to UTC time
		var utc = date.getTime() + (date.getTimezoneOffset() * 60000);
	    var utcDate = new Date(utc);

		var hour = date.getHours() - (date.getHours() >= 12 ? 12 : 0);
		if (hour < 10) {
			hour = "0" + hour;
		}
		
		var minute = date.getMinutes();
		if (minute < 10) {
			minute = "0" + minute;
		}

		var period = date.getHours() >= 12 ? 'PM' : 'AM';

		// Test for setting a specific time
		if (newTime != '0') {
			var tempHour = "";
			var tempMinute = "";
			
			// Get the hour and minute to use
			itm = newTime.split(":");
			tempHour = itm[0];
			tempMinute = itm[1]
			if ((tempHour >= 0) && (tempHour < 24) && (tempMinute >= 0) && (tempMinute < 60) && (tempMinute.length == 2)) {
				
				// The numbers are valid
				if (tempHour > 12) {
					tempHour -= 12;
					period = "PM";
				} else {
					if (tempHour == 0) {
						tempHour = 12;	
					} 
					period = "AM";
				}
				
				if (tempHour < 10) {
					tempHour = "0" + tempHour;
				}
				hour = tempHour;
				minute = tempMinute;
			}
		}
		
		return (hour + ':' + minute + ' ' + period);
	}

	// Set start date to today
	var today = new Date();

	// Set date to today
	function setTodayDate(_id) {
		var data = document.querySelector(_id);
		var dat = convertDate(today);
		data.value = dat;
	};

	// Set dat to 15 days from today
	var datePlus15 = new Date();
	datePlus15.setDate(today.getDate() + 15);
	function setToday15Date(_id) {
		var data = document.querySelector(_id);
		var dat = convertDate(datePlus15);
		data.value = dat;
	};

	function showNextRow(tableId) {
		
		if ($("#" + tableId).val() != "undefined") {
			var element = document.getElementById(tableId);
			var hiddenElements = element.getElementsByClassName('hide');
			var showElements = element.getElementsByClassName('show');
			var count = (hiddenElements.length) - (showElements.length);

			if (hiddenElements && count > 0) {
				hiddenElements[0].setAttribute('class', hiddenElements[0].getAttribute('class').replace('hide', ''));
			}

			if (hiddenElements.length == 0) {
				$("#" + tableId + '-add').hide();
			}
			
			if (tableId == "diagnosis-table") {
				if ($("#<portlet:namespace/>diagnosisCodeAdditional11").is(":visible")) {
					$('#<portlet:namespace/>diagnosis-table-add').hide();
				}
				adjustDiagnosisLines(false);
			} else {
				if ($("#<portlet:namespace/>procedureFromCode25").is(":visible")) {
					$('#<portlet:namespace/>procedure-table-add').hide();
				}
			}
		}
	}
	
	/********To disable tabbing onto add diagnosis or add procedure links when in sent status******/
	
	function disableTabbingInSentStatus() {
		
		$('#<portlet:namespace/>diagnosis-table-add').hide();
		$('#<portlet:namespace/>procedure-table-add').hide();
		
	}

	function disableTabbing() {

		// Disable tabbing to all fields
		$('#<portlet:namespace/>authInboundChannel').prop('tabindex', '-1');
		$('#<portlet:namespace/>authPatientRelation').prop('tabindex', '-1');
		$('#<portlet:namespace/>authRequestedDate').prop('tabindex', '-1');
		$('#<portlet:namespace/>authRequestedTime').prop('tabindex', '-1');
		$('#<portlet:namespace/>authPriority').prop('tabindex', '-1');
		$('#<portlet:namespace/>serviceBeginDate').prop('tabindex', '-1');
		$('#<portlet:namespace/>serviceEndDate').prop('tabindex', '-1');
		$('#<portlet:namespace/>authDecisionDate').prop('tabindex', '-1');
		$('#<portlet:namespace/>authDecisionTime').prop('tabindex', '-1');
		$('#<portlet:namespace/>authStatus2').prop('tabindex', '-1');
		$('#<portlet:namespace/>authWithdrawnReason').prop('tabindex', '-1');
		$('#<portlet:namespace/>authDueDate').prop('tabindex', '-1');
		$('#<portlet:namespace/>authDueTime').prop('tabindex', '-1');
		$('#<portlet:namespace/>authWorkedBy').prop('tabindex', '-1');
		$('#<portlet:namespace/>diagnosisCodePrimary').prop('tabindex', '-1');
		for (var d_cnt = 1; d_cnt <= <%=DIAGNOSIS_MAX_ROWS%>; d_cnt++) {
			$('#<portlet:namespace/>diagnosisCodeAdditional' + d_cnt).prop('tabindex', '-1');
		}
		for (var p_cnt = 1; p_cnt <= <%=PROCEDURE_MAX_ROWS%>; p_cnt++) {
			$('#<portlet:namespace/>procedureFromCode' + p_cnt).prop('tabindex', '-1');
			$('#<portlet:namespace/>procedureDesc' + p_cnt).prop('tabindex','-1');
			$('#<portlet:namespace/>procedureDecision' + p_cnt).prop('tabindex', '-1');
			$('#<portlet:namespace/>procedureDecisionReason' + p_cnt).prop('tabindex', '-1');
			$('#<portlet:namespace/>procedureUnits' + p_cnt).prop('tabindex','-1');
			
			// Fix a readonly problem.  Disable clicking in the units on the first line.
			$('#<portlet:namespace/>procedureUnits' + p_cnt).attr('disabled','disabled');
			$('#<portlet:namespace/>procedureUnits' + p_cnt).css("background-color", "rgb(255, 255, 255)");
		}

		// Destroy the date pickers	for IE
		if ((browserStr == "unknown") || (browserStr == "IE") || (browserStr == "Firefox")) {
			$("#<portlet:namespace/>authRequestedDate").datepicker('destroy');
			$("#<portlet:namespace/>serviceBeginDate").datepicker('destroy');
			$("#<portlet:namespace/>serviceEndDate").datepicker('destroy');
			$("#<portlet:namespace/>authDecisionDate").datepicker('destroy');
			$("#<portlet:namespace/>authDueDate").datepicker('destroy');
		}

		// Disable tabbing to all buttons
		$('#<portlet:namespace/>submitProcedureInfo').prop('tabindex', '-1');
		$('#<portlet:namespace/>saveProcedureInfo').prop('tabindex', '-1');
		$('#<portlet:namespace/>cancelProcedureInfo').prop('tabindex', '-1');
		$('#<portlet:namespace/>voidProcedureInfo').prop('tabindex', '-1');

		// Hide the add row links
		$('#<portlet:namespace/>diagnosis-table-add').hide();
		$('#<portlet:namespace/>procedure-table-add').hide();
	}

	function disableDropdowns() {

		// Disable all of the dropdowns for IE
		if ((browserStr == "IE") || (browserStr == "Firefox") || (browserStr == "unknown")) {
			$('#<portlet:namespace/>authInboundChannel').attr('disabled', true);
			$('#<portlet:namespace/>authPatientRelation').attr('disabled', true);
			$('#<portlet:namespace/>authPriority').attr('disabled', true);
			$('#<portlet:namespace/>authStatus2').attr('disabled', true);
			$('#<portlet:namespace/>authWithdrawnReason').attr('disabled', true);
			$('#<portlet:namespace/>authWorkedBy').attr('disabled', true);

			for (var p_cnt = 1; p_cnt <= <%=PROCEDURE_MAX_ROWS%>; p_cnt++) {
				$('#<portlet:namespace/>procedureDecision' + p_cnt).attr('disabled', true);
				$('#<portlet:namespace/>procedureDecisionReason' + p_cnt).attr('disabled', true);
			}
		}
	}

	function setWithdrawnReasonStatus(cssFlag) {
		var itm = "<portlet:namespace/>authWithdrawnReason";
		var selectLabel = $('label[for=' + itm + ']').text();
		
		// Disable Withdrawn Reason if Authorization Status is not "Completed - Withdrawn"
		if ($("#<portlet:namespace/>authStatus2").val() == "<%= PriorAuthConstants.COMPLETED_WITHDRAWN_CODE %>") {
			$('#<portlet:namespace/>authWithdrawnReason').removeProp('tabindex');
			$('#<portlet:namespace/>authWithdrawnReason').removeAttr('disabled');
			
			// Add the required indicator
			if (selectLabel.indexOf("*") < 0) {
				$('label[for=' + itm + ']').append('<span style="color: blue"> *</span>');
			}
			
			if ($("#<portlet:namespace/>authWithdrawnReason").prop("selectedIndex") == 0) {
				if (cssFlag) {
					
					// Show the withdrawn reason error
					cssWithdrawnReasonError(true);
				} else {
					$('#authWithdrawnReasonError').show();
				}
			} else {
				if (cssFlag) {
					
					// Hide the withdrawn reason error
					cssWithdrawnReasonError(false);
				} else {
					$('#authWithdrawnReasonError').hide();
				}
			}
		} else {
			$('#<portlet:namespace/>authWithdrawnReason').prop('selectedIndex', 0);
			$('#<portlet:namespace/>authWithdrawnReason').prop('tabindex', '-1');
			$('#<portlet:namespace/>authWithdrawnReason').attr('disabled', true);
			
			// Remove the required indicator
			if (selectLabel.indexOf("*") > 0) {
				$('label[for=' + itm + ']').text(selectLabel.replace(" *", ""));
			}

			// Select the top value
			$('#<portlet:namespace/>authWithdrawnReason').val("");
			
			if (cssFlag) {
				
				// Hide the withdrawn reason error
				cssWithdrawnReasonError(false);
			} else {
				$('#authWithdrawnReasonError').hide();
			}
		}
	}
	
	function isAllCodesValid() {
		var validFlag = true;

		// Test the primary diagnosis code for an error
		var codeId = "<portlet:namespace />diagnosisCodePrimary";
		if ($('#' + codeId).hasClass('error-field')) {
			validFlag = false;
		}

		// Test the additional diagnosis codes for an error
		if (validFlag) {
			for (var cnkDiagCnt = 1; cnkDiagCnt < <%= DIAGNOSIS_MAX_ROWS %> + 1; cnkDiagCnt++) {
				codeId = "<portlet:namespace />diagnosisCodeAdditional" + cnkDiagCnt;
				
				if ($.trim($("#" + codeId).val()) != "") {
					if ($('#' + codeId).hasClass('error-field')) {
						validFlag = false;
					}
				}
				
				if (!validFlag) {
					break;
				}
			}
		}

		// Test the procedure codes for an error
		if (validFlag) {
			for (var cnkProcCnt = 1; cnkProcCnt < <%= PROCEDURE_MAX_ROWS %> + 1; cnkProcCnt++) {
				codeId = "<portlet:namespace />procedureFromCode" + cnkProcCnt;
				
				if ($.trim($("#" + codeId).val()) != "") {
					if ($('#' + codeId).hasClass('error-field')) {
						validFlag = false;
					}
				}
				
				if (!validFlag) {
					break;
				}
			}
		}
		 
		return validFlag;
	}

	function getAllDesc(displayFlag) {
		/* If the displayFlag is 0, change the diagnosis codes
		 * If the displayFlag is 1, change the procedure codes
		 * If the displayFlag is anything else, change both the diagnosis and procedure codes
		 */
		 
		if (displayFlag != "1") {

			// Add the description if the diagnosis code is not empty
		 	var primaryCodeId = "<portlet:namespace />diagnosisCodePrimary";
		 	if ($.trim($("#" + primaryCodeId).val()) != "") {
		 		getDiagnosisDesc(primaryCodeId);
		 	}

		 	for (var diagCnt = 1; diagCnt < <%= DIAGNOSIS_MAX_ROWS %> + 1; diagCnt++) {
				var diagCodeId = "<portlet:namespace />diagnosisCodeAdditional" + diagCnt;
				if ($.trim($("#" + diagCodeId).val()) != "") {
					getDiagnosisDesc(diagCodeId);
				}
			}
		}
		 
		if (displayFlag != "0") {
	
		 	// Add the description if the procedure code is not empty
			for (var procCnt = 1; procCnt < <%= PROCEDURE_MAX_ROWS %> + 1; procCnt++) {
				var procCodeId = "<portlet:namespace />procedureFromCode" + procCnt;
				if ($.trim($("#" + procCodeId).val()) != "") {
					getProcedureDesc(procCodeId);
				}
			}
		}
	}

	function getDiagnosisDesc(diagCodeId) {
		var diagcode= $.trim($("#" + diagCodeId).val());
		var row = diagCodeId.match(/\d+$/);
		
		// Test for the primary diagnosis code.  The id does not end in a number.
		if (row == null) {
			row = 0;
		}
		var diagDescId = "<portlet:namespace/>diagnosisCodeDesc" + row;
		var serviceBeginDate = $("#<portlet:namespace/>serviceBeginDate").val();
		AUI().ready('autocomplete-list','aui-base','aui-io-request','autocomplete-filters','autocomplete-highlighters',function (A) {
			A.io.request('<%=getDiagnosisDescription%>', {
				dataType: 'text',
				method: 'POST', 
				data: {
					<portlet:namespace />diagcode : diagcode,
					<portlet:namespace />fromDate : serviceBeginDate,
				},
			    on: {
					success: function() {
						var desc = "";
						if (diagcode != "") {
							var desc = this.get('responseData');
						}
						var errStr = diagnosisError;
						var notFound = false;
						if (desc.indexOf(errStr) > -1) {
							notFound = true;
						}
	
						if (notFound) {
							A.one("#" + diagCodeId).addClass("error-field");
							A.one("#" + diagCodeId).addClass("error");
	
							// show error description in red
							$("#" + diagDescId).css("border-color", "red");
							$("#" + diagDescId).css("color", "red");
						} else {
							if(A.one("#" + diagDescId).hasClass("error-field")) {
								A.one("#" + diagDescId).removeClass("error-field");
								A.one("#" + diagDescId).removeClass("error");
							}
							$("#" + diagDescId).css("border-color", "");
							$("#" + diagDescId).css("color", "");
						}
						A.one('#'+ diagDescId).setAttribute('value', desc);
						
						// The form check will not be valid until the diagnosis display is completed so set the button status here after the description is completed
						setSaveButtonStatus();
						setSubmitButtonStatus();
						
						// Adjust diagnosis field widths
						adjustDiagnosisLines(false);
					}
				}
	  		});
		});
	}

	function getProcedureDesc(procCodeId) {
		var procedureCode= $.trim($("#" + procCodeId).val());
		var row = procCodeId.match(/\d+$/);
		if (row == "") {
			row = 0;
		}
		var procDescId = "<portlet:namespace/>procedureDesc" + row;

		AUI().ready('autocomplete-list','aui-base','aui-io-request','autocomplete-filters','autocomplete-highlighters',function (A) {
		    A.io.request('<%=getProcedureDescription%>', {
			    dataType: 'text',
			    method: 'POST',
			    data: {
					<portlet:namespace />procedureCode : procedureCode,
			    },
			    on: {
			        success: function() {
			        	var desc = "";
						if (procedureCode != "") {
				 			desc = this.get('responseData');
						}
						var errStr = procedureError;
						var notFound = false;

						if (desc.indexOf(errStr) > -1) {
							notFound = true;
						}
	
						if (notFound) {
							A.one("#" + procDescId).addClass("error");

							// show error description in red
							$("#" + procDescId).css("border-color", "red");
							$("#" + procDescId).css("color", "red");
						} else {
							if(A.one("#" + procDescId).hasClass("error-field")) {
								A.one("#" + procDescId).removeClass("error-field");
							}
							$("#" + procDescId).css("border-color", "");
							$("#" + procDescId).css("color", "");
						}
						A.one("#"+ procDescId).setAttribute("value", desc);
						
						// The form check will not be valid until the procedure display is completed so set the button status here after the description is completed
						if (procCodeId.indexOf("procedureFromCode") >= 0) {
							setSaveButtonStatus();
							setSubmitButtonStatus();
			        	}
						
						// Adjust claim field widths
						adjustClaimLines(false);
				    }
				}
			});
		});
	}

	function setCheckboxValue(id) {

		// Set the value based on the status of the checkbox.  The value is used by the controller.
		if ($("#" + id).prop("checked")) {
			$("#" + id).val("true");
		} else {
			$("#" + id).val("false");
		}
	}
	
	function setWorkedByFlag() {
		workedByChanged = true;
	}
</aui:script>
	
<%
    String authSubmissionStatus = "";
    String recordCreator = "";
    String authPatientRelation = "";
    String appCreateDateTime = "";
    if (request.getAttribute("authSubmissionStatus") != null) {
        authSubmissionStatus = (String) request.getAttribute("authSubmissionStatus");
        recordCreator = (String) request.getAttribute("recordCreator");
    }
    if (request.getAttribute("authPatientRelation") != null) {
        authPatientRelation = (String) request.getAttribute("authPatientRelation");
    }
%>

<% // Authorization Status constants %>
<c:set var="inProcessIntakeReview" value="<%= PriorAuthConstants.IN_PROCESS_INTAKE_REVIEW_CODE %>" />
<c:set var="inProcessIntakeReady" value="<%= PriorAuthConstants.IN_PROCESS_READY_FOR_COMPL_CODE %>" />
<c:set var="inProcessNurseReview" value="<%= PriorAuthConstants.IN_PROCESS_NURSE_REVIEW_CODE %>" />
<c:set var="inProcessNurseReviewReady" value="<%= PriorAuthConstants.IN_PROCESS_NURSE_READY_CODE %>" />
<c:set var="inProcessNurseReviewWaiting" value="<%= PriorAuthConstants.IN_PROCESS_NURSE_WAITING_CODE %>" />
<c:set var="inProcessPhysicianReview" value="<%= PriorAuthConstants.IN_PROCESS_PHYSICIAN_REVIEW_CODE %>" />
<c:set var="completedApproved" value="<%= PriorAuthConstants.COMPLETED_APPROVED_CODE %>" />
<c:set var="completedPartiallyApproved" value="<%= PriorAuthConstants.COMPLETED_PARTIALLY_APPROVED_CODE %>" />
<c:set var="completedDenied" value="<%= PriorAuthConstants.COMPLETED_DENIED_CODE %>" />
<c:set var="completedAdministrativeDenial" value="<%= PriorAuthConstants.COMPLETED_ADMINISTRATIVE_DENIAL_CODE %>" />
<c:set var="completedClinicalDenial" value="<%= PriorAuthConstants.COMPLETED_CLINICAL_DENIAL_CODE %>" />
<c:set var="completedWithdrawn" value="<%= PriorAuthConstants.COMPLETED_WITHDRAWN_CODE %>" />

<% // Inbound Channel constants %>
<c:set var="phone" value="<%= PriorAuthConstants.PHONE_STR.toLowerCase() %>"></c:set>
<c:set var="fax" value="<%= PriorAuthConstants.FAX_STR.toLowerCase() %>"></c:set>
<c:set var="web" value="<%= PriorAuthConstants.WEB_STR.toLowerCase() %>"></c:set>

<% // Priority constants %>
<c:set var="routine" value="<%= PriorAuthConstants.ROUTINE_STR.toLowerCase() %>"></c:set>
<c:set var="urgent" value="<%= PriorAuthConstants.URGENT_STR.toLowerCase() %>"></c:set>

<% // Withdrawn Reason constants %>
<c:set var="duplicate" value="<%= PriorAuthConstants.DUPLICATE_CODE %>"></c:set>
<c:set var="providerWithdrawn" value="<%= PriorAuthConstants.PROVIDER_WITHDRAWN_CODE %>"></c:set>
<c:set var="authEnteredInError" value="<%= PriorAuthConstants.AUTH_ENTERED_IN_ERROR_CODE %>"></c:set>
<c:set var="nonParticipatingMember" value="<%= PriorAuthConstants.NON_PARTICIPATING_MEMBER_CODE %>"></c:set>

<div id="busy_indicator" style="display: none">
	<img src="<%=request.getContextPath()%>/images/busy-spinner.gif" alt="Busy indicator">
</div>

<input type="text" id="auSubStatid" style="display: none !important" value="<%=authSubmissionStatus%>"></input>
<div class="avalon__proc_portlet">
	<liferay-ui:panel title="pa.procedure.info" collapsible="false">
		<aui:form name="ProcedureInformationDetails" cssClass="procForm" method="post" commandname="procedureInformationFO" 
		          onChange="javascript:setSubmitButtonStatus();formChangedFlag = true;javascript:setSaveButtonStatus()" >
			<aui:input type="hidden" name="submitButtonStatus" id="submitButtonStatus" value="10" />
			<aui:fieldset label="pa.procedure.info.auth.info">
				<aui:input type="hidden" name="hiddenRequestedDate" id="hiddenRequestedDate" label="" value="${procedureInformationFO.authRequestedDate}" />
				<aui:input type="hidden" name="hiddenCreationDate" id="hiddenCreationDate" label="" value='<%= request.getAttribute("paCreationDate") %>' />
				<aui:input type="hidden" name="va" id="hiddenWorkedByPopupFlag" label="" value='<%= request.getAttribute("workedByPopupFlag") %>' />
				<aui:container>
					<aui:row>
						<aui:col span="6">
							<aui:select inlineLabel="true" name="authInboundChannel" label="pa.procedure.info.label.inbound.channel" cssClass="span7 sntdis checkForDisable" required="true" showRequiredLabel="true" id="authInboundChannel">
								<aui:option value="">Make a Selection</aui:option>
								<aui:option value="<%= PriorAuthConstants.PHONE_STR.toLowerCase() %>" selected="${procedureInformationFO.authInboundChannel eq phone}"><%= PriorAuthConstants.PHONE_STR %></aui:option>
								<aui:option value="<%= PriorAuthConstants.FAX_STR.toLowerCase() %>" selected="${procedureInformationFO.authInboundChannel eq fax}"><%= PriorAuthConstants.FAX_STR %></aui:option>
								<aui:option value="<%= PriorAuthConstants.WEB_STR.toLowerCase() %>" selected="${procedureInformationFO.authInboundChannel eq web}"><%= PriorAuthConstants.WEB_STR %></aui:option>
							</aui:select>
						</aui:col>
						<aui:col span="6">
							<aui:select inlineLabel="true" name="authPatientRelation" label="pa.procedure.info.label.patient.relation" cssClass="span7 checkForDisable" id="authPatientRelation">
								<aui:option value="">Make a Selection</aui:option>
								<aui:option value="1" selected='<%=authPatientRelation.equals("1")%>'>Male Subscriber</aui:option>
								<aui:option value="2" selected='<%=authPatientRelation.equals("2")%>'>Female Subscriber</aui:option>
								<aui:option value="3" selected='<%=authPatientRelation.equals("3")%>'>Male Spouse</aui:option>
								<aui:option value="4" selected='<%=authPatientRelation.equals("4")%>'>Female Spouse</aui:option>
								<aui:option value="5" selected='<%=authPatientRelation.equals("5")%>'>Male Dependent</aui:option>
								<aui:option value="6" selected='<%=authPatientRelation.equals("6")%>'>Female Dependent</aui:option>
							</aui:select>
						</aui:col>
					</aui:row>
					<aui:row>
						<aui:col span="6">
							<aui:input type="date" inlineLabel="true" name="authRequestedDate" id="authRequestedDate" cssClass="checkForDisable sntdis" label="pa.procedure.info.label.request.date" required="true"
							           value="${procedureInformationFO.authRequestedDate}" >
								<aui:validator name="custom" errorMessage="Please enter a valid date (format mm/dd/yyyy).">
									function (val, fieldNode, ruleValue) {
										var retValue = false;
										
										// Test for invalid date
										if ((browserStr == "IE") || (browserStr == "Firefox") || (browserStr == "unknown")) {
											retValue = isValidDate(val, true);
										} else {
											var authRequestedDate = document.getElementById("<portlet:namespace/>authRequestedDate");
											retValue = !authRequestedDate.validity.badInput;
										}
										
										return retValue;
									}
								</aui:validator>
							</aui:input>
						</aui:col>
						<aui:col span="6">
							<aui:input type="text" inlineLabel="true" name="authRequestedTime" cssClass="sntdis" label="pa.procedure.info.label.request.time" value="${procedureInformationFO.authRequestedTime}">
								<aui:validator name="required" />
								<aui:validator name="custom" errorMessage="Please enter a valid time (format: hh:mm AM/PM).">
									function (val, fieldNode, ruleValue) {
										var retValue = false;
										
										retValue = isValidTime(val, true);
										
										return retValue;
									}
								</aui:validator>
							</aui:input>
						</aui:col>
					</aui:row>
							<aui:row>
						<aui:col span="6">
							<aui:input type="text" inlineLabel="true" name="authRecordCreator" cssClass="sntdis" label="pa.procedure.info.label.record.creator" value="<%=recordCreator%>" maxlength="<%= RECORD_CREATOR_LEN %>" disabled="true" />
						</aui:col>
						<aui:col span="6">
							<aui:input type="datetime" inlineLabel="true" name="authCreatedDate" cssClass="sntdis" label="pa.procedure.info.label.create.date" value="${procedureInformationFO.authCreatedDate}" disabled="true" />
						</aui:col>
					</aui:row>
					<aui:row>
						<aui:col span="6">
							<aui:input type="text" inlineLabel="true" name="authSubmissionStatus" cssClass="authSubmitStatusdisable" id="authSubmitStatusdisableId" label="pa.procedure.info.label.submission.status" value="<%=authSubmissionStatus%>" />
						</aui:col>
						<aui:col span="6">
							<aui:select inlineLabel="true" name="authPriority" label="pa.procedure.info.label.priority" cssClass="span7 sntdis checkForDisable" required="true" showRequiredLabel="true" id="authPriority">
								<aui:option value="">Make a Selection</aui:option>
								<aui:option value="routine" selected="${procedureInformationFO.authPriority eq 'routine'}">Routine</aui:option>
								<aui:option value="urgent" selected="${procedureInformationFO.authPriority eq 'urgent'}">Urgent</aui:option>
							</aui:select>
						</aui:col>
					</aui:row>
					<aui:row>
 						<aui:col span="6">
							<aui:input type="date" inlineLabel="true" name="serviceBeginDate" id="serviceBeginDate" value="${procedureInformationFO.serviceBeginDate}" cssClass="checkForDisable" required="true" 
							           label="pa.procedure.info.label.service.start.date" onChange="getAllDesc('0')" >
								<aui:validator name="custom" errorMessage="Service Begin Date cannot be before 1/1/2016 and must be before or equal to the Service End Date.">
									function (val, fieldNode, ruleValue) {
									
										var retValue = false;
										
										if (!isValidDate(val, true)) {
											
											// Do not cause this error message if the date is invalid
											retValue = true;
										}
											
										if (!retValue) {
											var serviceEndDate = $("#<portlet:namespace />serviceEndDate").val();
																    	
											if ((browserStr == "IE") || (browserStr == "Firefox") || (browserStr == "unknown")) {
												if (val == "") {
													val = convertDate(val);
												}
												
												if (serviceEndDate == "") {
													serviceEndDate = convertDate(serviceEndDate);
												}
											}
											
											if (serviceEndDate.length == 0) {
												retValue = true;
											} else {
												retValue = (val <= serviceEndDate);
											}
										}
										
										if (!serviceDateFlag) {
										
											// Run the serviceEndDate validation
											serviceDateFlag = true;
											processItem("<portlet:namespace />serviceEndDate", "<portlet:namespace />serviceBeginDate")
											serviceDateFlag = false;
										}
									
									     return retValue;
									}
								</aui:validator>
								<aui:validator name="custom" errorMessage="Please enter a valid date (format mm/dd/yyyy).">
									function (val, fieldNode, ruleValue) {
										var retValue = false;
								
										// Test for invalid date
										if ((browserStr == "IE") || (browserStr == "Firefox") || (browserStr == "unknown")) {
											retValue = isValidDate(val, true);
										} else {
											var serviceBeginDate = document.getElementById("<portlet:namespace/>serviceBeginDate");
											retValue = !serviceBeginDate.validity.badInput;
										}
									
										return retValue;
									}
								</aui:validator>
							</aui:input>
						</aui:col>
						<aui:col span="6">
							<aui:input type="date" inlineLabel="true" name="serviceEndDate" id="serviceEndDate" value="${procedureInformationFO.serviceEndDate}" cssClass="checkForDisable" required="true" label="pa.procedure.info.label.service.end.date" >
								<aui:validator name="custom" errorMessage="Service End Date cannot be before 1/1/2016 and must be after or equal to the Service Begin Date.">
								  function (val, fieldNode, ruleValue) {
								
										var retValue = false;
										
										if (!isValidDate(val, true)) {
											
											// Do not cause this error message if the date format is invalid
											retValue = true;
										}
									
										if (!retValue) {
											var serviceBeginDate = $("#<portlet:namespace />serviceBeginDate").val();
										   					
											if ((browserStr == "IE") || (browserStr == "Firefox") || (browserStr == "unknown")) {
												if (val == "") {
													val = convertDate(val);
												}
												
												if (serviceBeginDate == "") {
													serviceBeginDate = convertDate(serviceBeginDate);
												}
											}
										   	
											if (serviceBeginDate.length == 0) {
												retValue = true;
											} else {
												retValue = (val >= serviceBeginDate);
											}
										}
								
										if (!serviceDateFlag) {
								
											// Run the serviceBeginDate validation
											serviceDateFlag = true;
											processItem("<portlet:namespace />serviceBeginDate", "<portlet:namespace />serviceEndDate")
											serviceDateFlag = false;
										}
								  	
										return retValue;
									}
								</aui:validator>
								<aui:validator name="custom" errorMessage="Please enter a valid date (format mm/dd/yyyy).">
									function (val, fieldNode, ruleValue) {
										var retValue = false;
								
										// Test for invalid date
										if ((browserStr == "IE") || (browserStr == "Firefox") || (browserStr == "unknown")) {
											retValue = isValidDate(val, true);
										} else {
											var serviceEndDate = document.getElementById("<portlet:namespace/>serviceEndDate");
											retValue = !serviceEndDate.validity.badInput;
										}
									
										return retValue;
									}
								</aui:validator>
							</aui:input>
						</aui:col>
					</aui:row>
					<aui:row>
 						<aui:col span="6">
							<aui:input type="date" inlineLabel="true" name="authDecisionDate" id="authDecisionDate" cssClass="checkForDisable" label="pa.procedure.info.label.decision.date"
							           value="${procedureInformationFO.authDecisionDate}" onblur="this.value=removeSpaces(this.value)">
								<aui:validator name="custom" errorMessage="Original Decision Date cannot be before 1/1/2016.">
									function (val, fieldNode, ruleValue) {
										var retValue = false;
								
										if (!isValidDate(val, true)) {
									
											// Do not cause this error message if the date format is invalid
											retValue = true;
										}
									
										if (!retValue) {
											if (val == "") {
												retValue = true;
											} else {
												retValue = (val >= getStartDate());
											}
										}
								  	
										return retValue;
									}
								</aui:validator>
								<aui:validator name="custom" errorMessage="Please enter a valid date (format mm/dd/yyyy).">
									function (val, fieldNode, ruleValue) {
										var retValue = false;
								
										// Test for invalid date
										if ((browserStr == "IE") || (browserStr == "Firefox") || (browserStr == "unknown")) {
											retValue = isValidDate(val, true);
										} else {
								 		    var authDecisionDate  = document.getElementById("<portlet:namespace/>authDecisionDate ");
											retValue = !authDecisionDate .validity.badInput;
									}
									
										return retValue;
									}
								</aui:validator>
							</aui:input>
						</aui:col>
						<aui:col span="6">
							<aui:input type="text" inlineLabel="true" name="authDecisionTime" label="pa.procedure.info.label.decision.time" value="${procedureInformationFO.authDecisionTime}">
								<aui:validator name="custom" errorMessage="Please enter a valid time (format: hh:mm AM/PM).">
									function (val, fieldNode, ruleValue) {
										var retValue = false;
									
									 	retValue = isValidTime(val, false);
									
									 	return retValue;
									}
								</aui:validator>
							</aui:input>
						</aui:col>
					</aui:row>
					<aui:row>
						<aui:col span="6">
							<aui:select name="authStatus" inlineLabel="true" id="authStatus2" label="pa.label.auth.status" cssClass="span7 checkForDisable" required="true" showRequiredLabel="true"
							            onChange="javascript:setOriginalDecisionState(); javascript:setWithdrawnReasonStatus(true)" >
								<aui:option value="">Make a selection</aui:option>
								<optgroup label = "===========================================">
									<aui:option value="<%= PriorAuthConstants.IN_PROCESS_INTAKE_REVIEW_CODE %>" selected="${procedureInformationFO.authStatusCode eq inProcessIntakeReview}" >
									            <%= PriorAuthConstants.IN_PROCESS_INTAKE_REVIEW_DESC %></aui:option>
									<aui:option value="<%= PriorAuthConstants.IN_PROCESS_READY_FOR_COMPL_CODE %>" selected="${procedureInformationFO.authStatusCode eq inProcessIntakeReady}" >
									            <%= PriorAuthConstants.IN_PROCESS_READY_FOR_COMPL_DESC %></aui:option>
									<aui:option value="<%= PriorAuthConstants.IN_PROCESS_NURSE_REVIEW_CODE %>" selected="${procedureInformationFO.authStatusCode eq inProcessNurseReview}" >
									            <%= PriorAuthConstants.IN_PROCESS_NURSE_REVIEW_DESC %></aui:option>
									<aui:option value="<%= PriorAuthConstants.IN_PROCESS_NURSE_READY_CODE %>" selected="${procedureInformationFO.authStatusCode eq inProcessNurseReviewReady}" >
									            <%= PriorAuthConstants.IN_PROCESS_NURSE_READY_DESC %></aui:option>
									<aui:option value="<%= PriorAuthConstants.IN_PROCESS_NURSE_WAITING_CODE %>" selected="${procedureInformationFO.authStatusCode eq inProcessNurseReviewWaiting}" >
									            <%= PriorAuthConstants.IN_PROCESS_NURSE_WAITING_DESC %></aui:option>
									<aui:option value="<%= PriorAuthConstants.IN_PROCESS_PHYSICIAN_REVIEW_CODE %>" selected="${procedureInformationFO.authStatusCode eq inProcessPhysicianReview}" >
									            <%= PriorAuthConstants.IN_PROCESS_PHYSICIAN_REVIEW_DESC %></aui:option>
								</optgroup>
								<optgroup label = "===========================================">
									<aui:option value="<%= PriorAuthConstants.COMPLETED_APPROVED_CODE %>" selected="${procedureInformationFO.authStatusCode eq completedApproved}" >
									            <%= PriorAuthConstants.COMPLETED_APPROVED_DESC %></aui:option>
									<aui:option value="<%= PriorAuthConstants.COMPLETED_PARTIALLY_APPROVED_CODE %>" selected="${procedureInformationFO.authStatusCode eq completedPartiallyApproved}" >
									            <%= PriorAuthConstants.COMPLETED_PARTIALLY_APPROVED_DESC %></aui:option>
									<aui:option value="<%= PriorAuthConstants.COMPLETED_DENIED_CODE %>" selected="${procedureInformationFO.authStatusCode eq completedDenied}" >
									            <%= PriorAuthConstants.COMPLETED_DENIED_DESC %></aui:option>
									<aui:option value="<%= PriorAuthConstants.COMPLETED_ADMINISTRATIVE_DENIAL_CODE %>" selected="${procedureInformationFO.authStatusCode eq completedAdministrativeDenial}" >
									            <%= PriorAuthConstants.COMPLETED_ADMINISTRATIVE_DENIAL_DESC %></aui:option>
									<aui:option value="<%= PriorAuthConstants.COMPLETED_CLINICAL_DENIAL_CODE %>" selected="${procedureInformationFO.authStatusCode eq completedClinicalDenial}" >
									            <%= PriorAuthConstants.COMPLETED_CLINICAL_DENIAL_DESC %></aui:option>
									<aui:option value="<%= PriorAuthConstants.COMPLETED_WITHDRAWN_CODE %>" selected="${procedureInformationFO.authStatusCode eq completedWithdrawn}" >
									            <%= PriorAuthConstants.COMPLETED_WITHDRAWN_DESC %></aui:option>
								</optgroup>
							</aui:select>
							</aui:col>
							<aui:col span="6">
								<aui:select inlineLabel="true" name="authWithdrawnReason" label="pa.procedure.info.label.auth.withdrawn.reason" cssClass="span7 checkForDisable" id="authWithdrawnReason"
								           onFocusOut="javascript:checkWithdrawnReasonError()">
									<aui:option value="">Make a selection</aui:option>
									<aui:option value="<%= PriorAuthConstants.DUPLICATE_CODE %>" selected="${procedureInformationFO.authWithdrawnReasonCode eq duplicate}" >
									            <%= PriorAuthConstants.DUPLICATE_DESC %></aui:option>
									<aui:option value="<%= PriorAuthConstants.PROVIDER_WITHDRAWN_CODE %>" selected="${procedureInformationFO.authWithdrawnReasonCode eq providerWithdrawn}" >
									            <%= PriorAuthConstants.PROVIDER_WITHDRAWN_DESC %></aui:option>
									<aui:option value="<%= PriorAuthConstants.AUTH_ENTERED_IN_ERROR_CODE %>" selected="${procedureInformationFO.authWithdrawnReasonCode eq authEnteredInError}" >
									            <%= PriorAuthConstants.AUTH_ENTERED_IN_ERROR_DESC %></aui:option>
									<aui:option value="<%= PriorAuthConstants.NON_PARTICIPATING_MEMBER_CODE %>" selected="${procedureInformationFO.authWithdrawnReasonCode eq nonParticipatingMember}" >
									            <%= PriorAuthConstants.NON_PARTICIPATING_MEMBER_DESC %></aui:option>
									</aui:select>
									<div  id="authWithdrawnReasonError" class="form-validator-stack help-inline">
										<div role="alert" style="color: red" >This field is required.</div>
									</div>
						</aui:col>
					</aui:row>
					<aui:row>
						<aui:col span="6">
							<aui:input type="date" inlineLabel="true" name="authDueDate" id="authDueDate" cssClass="checkForDisable" label="pa.procedure.info.label.due.date" value="${procedureInformationFO.authDueDate}" 
							           onblur="this.value=removeSpaces(this.value)">
								<aui:validator name="required" />
								<aui:validator name="custom" errorMessage="Please enter a valid date (format mm/dd/yyyy).">
									function (val, fieldNode, ruleValue) {
										var retValue = false;
								
										// Test for invalid date
										if ((browserStr == "IE") || (browserStr == "Firefox") || (browserStr == "unknown")) {
											retValue = isValidDate(val, true);
										} else {
								 		    var authDueDate  = document.getElementById("<portlet:namespace/>authDueDate");
											retValue = !authDueDate.validity.badInput;
										}
										
										return retValue;
									}
								</aui:validator>
							</aui:input>
						</aui:col>
						<aui:col span="6">
							<aui:input type="text" inlineLabel="true" name="authDueTime" id="authDueTime" label="pa.procedure.info.label.due.time" value="${procedureInformationFO.authDueTime}">
								<aui:validator name="required" />
								<aui:validator name="custom" errorMessage="Please enter a valid time (format: hh:mm AM/PM).">
									function (val, fieldNode, ruleValue) {
										var retValue = false;
								
										retValue = isValidTime(val, false);
								
										return retValue;
									}
								</aui:validator>
							</aui:input>
						</aui:col>
					</aui:row>
					<aui:row>
						<aui:col span="6">
							<aui:select inlineLabel="true" name="authWorkedBy" label="pa.procedure.info.label.auth.worked.by" cssClass="span7 checkForDisable" required="true" showRequiredLabel="true" id="authWorkedBy" 
							            onChange="setWorkedByFlag()">
								<aui:option value="Make a selection">Make a selection</aui:option>
								<c:choose>
									<c:when test="${fn:length(userList) > 0}">
										<c:forEach items="${userList}" var="user" >
											<aui:option value="${user}" selected="${procedureInformationFO.authWorkedBy eq user}" >${user}</aui:option>
										</c:forEach>
									</c:when>
								</c:choose>
							</aui:select>
						</aui:col>
					</aui:row>
				</aui:container>
			</aui:fieldset>
   
			<aui:fieldset label="pa.procedure.info.diagnosis.codes" id="diagnosis-table" cssClass="diagnosis-table sntdis">
				<aui:container>
					<%
						Map<Integer, String> diagnosisCodeAddition = (Map) request.getAttribute("diagnosisCodeAdditional");
						Map<Integer, String> diagnosisCodeRetain = new LinkedHashMap<Integer, String>();
						String addDiag = "";
						ArrayList<DiagnosisDisplayOrder> dialist = new ArrayList<DiagnosisDisplayOrder>();
						if ((ArrayList<DiagnosisDisplayOrder>) request.getAttribute("diadtoListreReq") != null) {
						    dialist = (ArrayList<DiagnosisDisplayOrder>) request.getAttribute("diadtoListreReq");
						} else {
						    dialist = new ArrayList<DiagnosisDisplayOrder>();
						}
						String finalRetainDiag = "";
						String diagnosisCodeRetainName = "";
						String diagnosisDescRetainName = "";
						String primaryDiacode = "";
						if (dialist != null && dialist.size() > 0) {
						    primaryDiacode = dialist.get(0).getDiagnosisCode();
						}
					%>
					<aui:row>
						<aui:col span="4">
							<aui:input type="text" inlineLabel="true" name="diagnosisCodePrimary" id="diagnosisCodePrimary" cssClass="checkForDisable toUppercase sntdis" label="pa.procedure.info.label.primary.diagnosis"
							           value="<%=primaryDiacode%>"  style="text-transform:uppercase" maxlength="<%= DIAGNOSIS_CODE_LEN %>" onChange="javascript:getDiagnosisDesc(this.id)">
								<aui:validator name="required" />
								<aui:validator name="custom" errorMessage="Please enter only alpha characters, digits or period.">
									function (val, fieldNode, ruleValue) {
										var retValue = true;
									
										// If the value is too long make sure the length error is displayed.
										if (val.length <= <%=DIAGNOSIS_CODE_LEN%>) {
											retValue = isValidDiagnosis(val, <%=DIAGNOSIS_CODE_LEN%>, false);
										}
										
										return retValue;
									}
								</aui:validator>
								<aui:validator name="custom" errorMessage="Cannot enter duplicate Diagnosis Code.">
									function (val, fieldNode, ruleValue) {
										var retValue = false;
										
										retValue = !checkDuplicateDiagnosis(0, val);
									
										return retValue;
									}
								</aui:validator>
							</aui:input>
						</aui:col>
						<aui:col span="8">
							<aui:input type="text" inlineLabel="true" name="diagnosisCodeDesc0" label="" id="diagnosisCodeDesc0" cssClass="diagSize diaCodeDesc" readonly="true" style="width: 99%" />
						</aui:col>
					</aui:row>
					<%
						if (dialist != null && dialist.size() > 0) {
							for (int i = 1; i < dialist.size(); i++) {
						   	finalRetainDiag = dialist.get(i).getDiagnosisCode();
						       diagnosisCodeRetainName = "diagnosisCodeAdditional" + i;
						       diagnosisDescRetainName = "diagnosisCodeDesc" + i;
								%>
								<aui:row>
									<aui:col span="4">
										<aui:input type="text" inlineLabel="true" name="<%=diagnosisCodeRetainName%>" cssClass="sntdis checkForDisable toUppercase" label="pa.procedure.info.label.additional.diagnosis" 
										           style="text-transform:uppercase" value="<%=finalRetainDiag%>" maxlength="<%= DIAGNOSIS_CODE_LEN %>" onChange="javascript:getDiagnosisDesc(this.id)" >
											<aui:validator name="custom" errorMessage="Please enter only alpha characters, digits or period.">
												function (val, fieldNode, ruleValue) {
													var retValue = true;
												
													// If the value is too long make sure the length error is displayed.
													if (val.length <= <%=DIAGNOSIS_CODE_LEN%>) {
														retValue = isValidDiagnosis(val, <%=DIAGNOSIS_CODE_LEN%>, false);
													}
													
													return retValue;
												}
											</aui:validator>
											<aui:validator name="custom" errorMessage="Cannot enter duplicate Diagnosis Code.">
												function (val, fieldNode, ruleValue) {
													var retValue = false;
													
													retValue = !checkDuplicateDiagnosis(<%=i%>, val);
												
													return retValue;
												}
											</aui:validator>
										</aui:input>
									</aui:col>
									<aui:col span="8">
			       						<aui:input type="text" inlineLabel="true" name="<%=diagnosisDescRetainName%>" label="" id="<%=diagnosisDescRetainName%>" cssClass="diagSize diaCodeDesc" readonly="true" style="width: 99%" />
									</aui:col>
								</aui:row>
								<%
							}
      					}
						if (DIAGNOSIS_MAX_ROWS >= dialist.size()) {
							int x = 1;
							if (dialist.size() > 0) {
								x = 0;
							}
							for (int i = dialist.size() + x, n = DIAGNOSIS_MAX_ROWS, j = 1; i <= n; i++) {
								String diagnosisCodeAdditional = "diagnosisCodeAdditional" + i;
								String diagnosisDescAdditional = "diagnosisCodeDesc" + i;
								if (diagnosisCodeAddition != null && diagnosisCodeAddition.size() > 0) {
									addDiag = diagnosisCodeAddition.get(i);
									if (addDiag != "") {
										finalRetainDiag = diagnosisCodeAddition.put(j, addDiag);
										diagnosisCodeRetainName = "diagnosisCodeAdditional" + j;
										diagnosisDescRetainName = "diagnosisDescAdditional" + j;
										j++;
					    				%>
											<aui:row cssClass="show">
												<aui:col span="4">
													<aui:input type="text" inlineLabel="true" name="<%=diagnosisCodeRetainName%>" cssClass="checkForDisable sntdis toUppercase" label="pa.procedure.info.label.additional.diagnosis" 
													           style="text-transform:uppercase" value="<%=finalRetainDiag%>" maxlength="<%= DIAGNOSIS_CODE_LEN %>" onChange="javascript:getDiagnosisDesc(this.id)" >
														<aui:validator name="custom" errorMessage="Please enter only alpha characters, digits or period.">
															function (val, fieldNode, ruleValue) {
																var retValue = true;
															
																// If the value is too long make sure the length error is displayed.
																if (val.length <= <%=DIAGNOSIS_CODE_LEN%>) {
																	retValue = isValidDiagnosis(val, <%=DIAGNOSIS_CODE_LEN%>, false);
																}
																
																return retValue;
															}
														</aui:validator>
														<aui:validator name="custom" errorMessage="Cannot enter duplicate Diagnosis Code.">
															function (val, fieldNode, ruleValue) {
																var retValue = false;
															
																retValue = !checkDuplicateDiagnosis(<%=i%>, val);
															
																return retValue;
															}
														</aui:validator>
													</aui:input>
												</aui:col>
												<aui:col span="8">
													<aui:input type="text" inlineLabel="true" name="<%=diagnosisDescRetainName%>" label="" id="<%=diagnosisDescRetainName%>" cssClass="diagSize diaCodeDesc" readonly="true" style="width: 99%" />
												</aui:col>
											</aui:row>
										<%
										}
									}
								%>
									<aui:row cssClass="hide">
										<aui:col span="4">
											<aui:input type="text" inlineLabel="true" name="<%=diagnosisCodeAdditional%>" cssClass="checkForDisable toUppercase sntdis" label="pa.procedure.info.label.additional.diagnosis" 
											           style="text-transform:uppercase" maxlength="<%= DIAGNOSIS_CODE_LEN %>" onChange="javascript:getDiagnosisDesc(this.id)" >
												<aui:validator name="custom" errorMessage="Please enter only alpha characters, digits or period.">
													function (val, fieldNode, ruleValue) {
														var retValue = true;
													
														// If the value is too long make sure the length error is displayed.
														if (val.length <= <%=DIAGNOSIS_CODE_LEN%>) {
															retValue = isValidDiagnosis(val, <%=DIAGNOSIS_CODE_LEN%>, false);
														}
														
														return retValue;
													}
												</aui:validator>
												<aui:validator name="custom" errorMessage="Cannot enter duplicate Diagnosis Code.">
													function (val, fieldNode, ruleValue) {
														var retValue = false;
														
														retValue = !checkDuplicateDiagnosis(<%=i%>, val);
													
														return retValue;
													}
												</aui:validator>
											</aui:input>
										</aui:col>
										<aui:col span="8">
											<aui:input type="text" inlineLabel="true" name="<%=diagnosisDescAdditional%>" label="" id="<%=diagnosisDescAdditional%>" cssClass="diagSize diaCodeDesc" readonly="true" style="width: 99%" />
										</aui:col>
									</aui:row>
		     					<%
         						}
							}
     					%>
						<aui:row id="diagnosis-table-add" cssClass="checkForLinkDisable">
							<aui:col span="6">
								<aui:a href="javascript:showNextRow('diagnosis-table')">
									<aui:icon image="plus" cssClass="pull-right sntdis" label="pa.procedure.info.label.add.diagnosis" />
	 							</aui:a>
							</aui:col>
						</aui:row>
					</aui:container>
  				</aui:fieldset>
  				
			<aui:fieldset label="pa.procedure.info.procedures" id="procedure-table" cssClass="procedure-table">
				<aui:container>
					<aui:row cssClass="row-header">
						<aui:col span="1">
							<span id="procNumLabelId">#</span>
						</aui:col>
						<aui:col span="2">
							<span id="procCodeLabelId">Procedure Code<span style="color: blue"> *</span></span>
						</aui:col>
						<aui:col span="3">
							<span id="procDescLabelId">Procedure Description</span>
						</aui:col>
						<aui:col span="2">
							<span id="procDecisionLabelId">Procedure Decision<span style="color: blue"> *</span></span>
						</aui:col>
						<aui:col span="2">
							<span id="procReasonLabelId">Decision Reason<span style="color: blue"> *</span></span>
						</aui:col>
						<aui:col span="1">
							<span id="procUnitLabelId">Units<span style="color: blue"> *</span></span>
						</aui:col>
						<!-- fasttrack added Jan 19, 2017 -->
						<aui:col span="1" style="display: none">
							<span>Fast Track</span>
						</aui:col>
					</aui:row>
 					<%
						Map<Integer, String> procedureDetails = (Map) request.getAttribute("procedureDetails");
						Map<Integer, String> procedureDecisionDetails = (Map) request.getAttribute("procedureDecisionDetails");
						Map<Integer, String> procedureReasonDetails = (Map) request.getAttribute("procedureReasonDetails");
						Map<Integer, String> procedureUnitsDetails = (Map) request.getAttribute("procedureUnitsDetails");
						// fasttrack added Jan 19, 2017
						Map<Integer, String> procedureFasttrackDetails = (Map) request.getAttribute("procedureFasttrackDetails");
						String fromCode = "";
						String toCode = "";
						String proUnits = "";
						String reasonDecision = "";
						String decisionDetails = "";
						int procedureMinRows = 10;
						System.out.print("inview jsp" + renderRequest.getAttribute("pdtoList"));
						ArrayList<ProcedureDTO> list = new ArrayList<ProcedureDTO>();
						if ((ArrayList<ProcedureDTO>) request.getAttribute("pdtoListreorgReq") != null) {
						    list = (ArrayList<ProcedureDTO>) request.getAttribute("pdtoListreorgReq");
						} else {
						    list = new ArrayList<ProcedureDTO>();
						}
						int j = 0;
						if (list != null && list.size() > 0) {
							for (ProcedureDTO procedure : list) {
								j++;
								String procedureFromCode = "procedureFromCode" + j;
								String procedureDesc = "procedureDesc" + j;
								String procedureDecision = "procedureDecision" + j;
								String procedureReason = "procedureDecisionReason" + j;
								String procedureUnits = "procedureUnits" + j;
								// fasttrack added Jan 19, 2017
								String procedureFastTrack = "procedureFastTrack" + j;
								String hidden = (j > procedureMinRows) ? "hide" : "";
								if (list.size() > 10) {
									hidden = (j > list.size()) ? "hide" : "";
								} else {
									hidden = (j > procedureMinRows) ? "hide" : "";
								}
								fromCode = procedure.getProcedureFromCode();
								int i = Float.valueOf(String.valueOf(procedure.getProcedureUnits())).intValue();
								proUnits = Integer.toString(i);
								decisionDetails = procedure.getProcedureDecision();
								reasonDecision = procedure.getProcedureDecisionReason();
								// fasttrack added Jan 19, 2017
								String proFastTrackStr = procedure.getProcedureFastTrack();
					%>
     
					<% // Set the ETL use in the drop-down selects %>
					<c:set var="decisionDetails" value="<%= decisionDetails.trim() %>"></c:set>
					<c:set var="reasonDecision" value="<%= reasonDecision.trim() %>"></c:set>
					<aui:row cssClass="<%=hidden%>">
						<aui:col span="1">
							<% if (j == 1) { %>
								<span><%=j%><span style="color: blue">*</span></span>
							<% } else { %>
								<span><%=j%></span>
							<% } %>
						</aui:col>
						<aui:col span="2">
							<aui:input type="text" inlineLabel="true" name="<%=procedureFromCode%>" label="" cssClass="span12 sntdis checkForDisable toUppercase" value="<%=fromCode%>" 
							           style="text-transform:uppercase" maxlength="<%= PROCEDURE_CODE_LEN %>" onChange="javascript:getProcedureDesc(this.id)" >
								<% if (j == 1) { %>
									<validator name="required" />
								<% } %>
								<%
									// AUI ERROR: The following custom validations are added because minLength and alphanum built in 
									//            validations where showing the error messages when they should not.
								%>
								<aui:validator name="custom" errorMessage="Please enter only alphanumeric characters.">
									function (val, fieldNode, ruleValue) {
										var retValue = true;
										
										if( /[^a-zA-Z0-9]/.test( val ) ) {
											retValue = false;
										}
									
										return retValue;
									}
								</aui:validator>
								<aui:validator name="custom" errorMessage="Please enter at least 5 characters.">
									function (val, fieldNode, ruleValue) {
										var retValue = true;
										
										if ((val.length > 0) && (val.length < 5)) {
											retValue = false;
										}
										
										return retValue;
									}
								</aui:validator>
								<aui:validator name="custom" errorMessage="Cannot enter duplicate Procedure Code.">
									function (val, fieldNode, ruleValue) {
										var retValue = false;
									 	
										retValue = !checkDuplicateProcedure(<%= j %>, val);
									 	
										return retValue;
									}
								</aui:validator>
							</aui:input>
						</aui:col>
						<aui:col span="3">
							<aui:input type="text" inlineLabel="true" name="<%=procedureDesc%>" label="" id="<%=procedureDesc%>" cssClass="span12 sntdis checkForDisable toUppercase" readonly="true" />
						</aui:col>
						<aui:col span="2">
      
						<% // Only the first row is required %>
						<% if (j == 1) { %>
							<aui:select inlineLabel="true" name="<%= procedureDecision %>" required="true" label="" cssClass="span12 checkForDisable" >
								<aui:option value="">Make a selection</aui:option>
								<aui:option value="P" selected="${decisionDetails eq 'P'}">Pending</aui:option>
								<aui:option value="A" selected="${decisionDetails eq 'A'}">Approved</aui:option>
								<aui:option value="D" selected="${decisionDetails eq 'D'}">Denied</aui:option>
								<aui:option value="N" selected="${decisionDetails eq 'N'}">No PA required</aui:option>
							</aui:select>
 						<% } else { %>
							<aui:select inlineLabel="true" name="<%= procedureDecision %>" label="" cssClass="span12 checkForDisable" >
								<aui:option value="">Make a selection</aui:option>
								<aui:option value="P" selected="${decisionDetails eq 'P'}">Pending</aui:option>
								<aui:option value="A" selected="${decisionDetails eq 'A'}">Approved</aui:option>
								<aui:option value="D" selected="${decisionDetails eq 'D'}">Denied</aui:option>
								<aui:option value="N" selected="${decisionDetails eq 'N'}">No PA required</aui:option>
							</aui:select>
						<% } %>
						</aui:col>
						<aui:col span="2">
						<% if (j == 1) { %>
							<aui:select inlineLabel="true" name="<%=procedureReason%>" required="true" label="" cssClass="span12 checkForDisable" >
								<aui:option value="">Make a selection</aui:option>
								<aui:option value="P" selected="${reasonDecision eq 'P'}">Pending</aui:option>
								<aui:option value="A1" selected="${reasonDecision eq 'A1'}">Approved, medically appropriate</aui:option>
								<aui:option value="D1" selected="${reasonDecision eq 'D1'}">Denied, not medically necessary</aui:option>
								<aui:option value="D2" selected="${reasonDecision eq 'D2'}">Contract exclusion, not covered under policy</aui:option>
								<aui:option value="D3" selected="${reasonDecision eq 'D3'}">Lack of Timely Filing Administrative Denial</aui:option>
								<aui:option value="N1" selected="${reasonDecision eq 'N1'}">Non-PA Code</aui:option>
							</aui:select>
						<% } else { %>
							<aui:select inlineLabel="true" name="<%=procedureReason%>" label="" cssClass="span12 checkForDisable" >
								<aui:option value="">Make a selection</aui:option>
								<aui:option value="P" selected="${reasonDecision eq 'P'}">Pending</aui:option>
								<aui:option value="A1" selected="${reasonDecision eq 'A1'}">Approved, medically appropriate</aui:option>
								<aui:option value="D1" selected="${reasonDecision eq 'D1'}">Denied, not medically necessary</aui:option>
								<aui:option value="D2" selected="${reasonDecision eq 'D2'}">Contract exclusion, not covered under policy</aui:option>
								<aui:option value="D3" selected="${reasonDecision eq 'D3'}">Lack of Timely Filing Administrative Denial</aui:option>
								<aui:option value="N1" selected="${reasonDecision eq 'N1'}">Non-PA Code</aui:option>
							</aui:select>
						<% } %>
						</aui:col>
						<aui:col span="1">
							<aui:input type="text" inlineLabel="true" name="<%=procedureUnits%>" label="" cssClass="span12 checkForDisable " value="<%=proUnits%>" maxlength="<%= PROCEDURE_UNIT_LEN %>">
								<aui:validator name="digits" />
								<% if (j == 1) { %>
									<aui:validator name="required" />
								<% } %>
							</aui:input>
						</aui:col>
						<!-- fasttrack added Jan 19, 2017 -->
						<aui:col span="1" style="display: none">
							<% if (proFastTrackStr.equals("1")) { %>
								<input type="checkbox" name="<portlet:namespace/><%=procedureFastTrack%>" id="<portlet:namespace/><%=procedureFastTrack%>" class="fasttrack sntdis checkForDisable" checked value="true"
								       onChange="javascript:setCheckboxValue(this.id)" />
							<% } else { %>
								<input type="checkbox" name="<portlet:namespace/><%=procedureFastTrack%>" id="<portlet:namespace/><%=procedureFastTrack%>" class="fasttrack sntdis checkForDisable" value="false"
								       onChange="javascript:setCheckboxValue(this.id)" />
							<% } %>
						</aui:col>
					</aui:row>
					<c:set var="decisionDetails" value=""></c:set>
					<c:set var="reasonDecision" value=""></c:set>
					<%
 							}	// for (ProcedureDTO procedure : list)
						}	// if (list != null && list.size() > 0)
						if (PROCEDURE_MAX_ROWS > list.size()) {
							fromCode = "";
							toCode = "";
							proUnits = "";
							reasonDecision = "";
							decisionDetails = "";
							for (int i = list.size() + 1, n = PROCEDURE_MAX_ROWS; i <= n; i++) {
								String procedureFromCode = "procedureFromCode" + i;
								String procedureDesc = "procedureDesc" + i;
								String procedureDecision = "procedureDecision" + i;
								String procedureReason = "procedureDecisionReason" + i;
								String procedureUnits = "procedureUnits" + i;
								// fasttrack added Jan 19, 2017
								String procedureFastTrack = "procedureFastTrack" + i;
								String hidden = (i > procedureMinRows) ? "hide" : "";
								if (procedureDetails != null && procedureDetails.size() > 0) {
								    fromCode = procedureDetails.get(i);
								}
								
								if (procedureUnitsDetails != null && procedureUnitsDetails.size() > 0) {
								    proUnits = procedureUnitsDetails.get(i);
								}
								
								// fasttrack added Jan 19, 2017
								String proFastTrackStr = "0";
								if (procedureFasttrackDetails != null && procedureFasttrackDetails.size() > 0) {
									proFastTrackStr = procedureFasttrackDetails.get(i);
								}
								%>
								<% // Set the ETL use in the drop-down selects %>
								<% if (procedureDecisionDetails != null && procedureDecisionDetails.size() > 0) { %>
									<c:set var="procedureDecisionDetailsItem" value="<%= procedureDecisionDetails.get(i).trim() %>"></c:set>
								<% } else { %>
									<c:set var="procedureDecisionDetailsItem" value=""></c:set>
								<% } %>
								<% if (procedureReasonDetails != null && procedureReasonDetails.size() > 0) { %>
									<c:set var="procedureReasonDetailsItem" value="<%= procedureReasonDetails.get(i).trim() %>"></c:set>
								<% } else { %>
									<c:set var="procedureReasonDetailsItem" value=""></c:set>
								<% } %>
			
								<aui:row cssClass="<%=hidden%>">
									<aui:col span="1">
										<% if (i == 1) { %>
											<span><%=i%><span style="color: blue">*</span></span>
										<% } else { %>
											<span><%=i%></span>
										<% } %>
									</aui:col>
			 						<aui:col span="2">
										<aui:input type="text" inlineLabel="true" name="<%=procedureFromCode%>" label="" cssClass="span12 sntdis checkForDisable toUppercase" value="<%=fromCode%>" 
										           style="text-transform:uppercase" id="" maxlength="<%= PROCEDURE_CODE_LEN %>" onChange="javascript:getProcedureDesc(this.id)" >
											<% if (i == 1) { %>
												<aui:validator name="required" />
											<% } %>
											<%
												// AUI ERROR: The following custom validations are added because minLength and alphanum built in 
												//            validations where showing the error messages when they should not.
											%>
											<aui:validator name="custom" errorMessage="Please enter only alphanumeric characters.">
												function (val, fieldNode, ruleValue) {
													var retValue = true;
													
													if( /[^a-zA-Z0-9]/.test( val ) ) {
														retValue = false;
													}
												
													return retValue;
												}
											</aui:validator>
											<aui:validator name="custom" errorMessage="Please enter at least 5 characters.">
												function (val, fieldNode, ruleValue) {
													var retValue = true;
													
													if ((val.length > 0) && (val.length < 5)) {
														retValue = false;
													}
													
													return retValue;
												}
											</aui:validator>
											<aui:validator name="custom" errorMessage="Cannot enter duplicate Procedure Code.">
												function (val, fieldNode, ruleValue) {
													var retValue = false;
												 	
													retValue = !checkDuplicateProcedure(<%=i%>, val);
													
													return retValue;
												}
											</aui:validator>
										</aui:input>
									</aui:col>
									<aui:col span="3">
										<aui:input type="text" inlineLabel="true" name="<%=procedureDesc%>" label="" id="<%=procedureDesc%>" cssClass="span12 sntdis checkForDisable toUppercase" readonly="true" />
									</aui:col>
									<aui:col span="2">
										<% if (i == 1) { %>
											<aui:select inlineLabel="true" name="<%=procedureDecision%>" label="" cssClass="span12 checkForDisable" required="true" showRequiredLabel="true" id="" >
												<aui:option value="">Make a selection</aui:option>
												<aui:option value="P" selected="${procedureDecisionDetailsItem eq 'P'}">Pending</aui:option>
												<aui:option value="A" selected="${procedureDecisionDetailsItem eq 'A'}">Approved</aui:option>
												<aui:option value="D" selected="${procedureDecisionDetailsItem eq 'D'}">Denied</aui:option>
												<aui:option value="N" selected="${decisionDetails eq 'N'}">No PA required</aui:option>
											</aui:select>
										<% } else { %>
											<aui:select inlineLabel="true" name="<%=procedureDecision%>" label="" cssClass="span12 checkForDisable" id="" >
												<aui:option value="">Make a selection</aui:option>
												<aui:option value="P" selected="${procedureDecisionDetailsItem eq 'P'}">Pending</aui:option>
												<aui:option value="A" selected="${procedureDecisionDetailsItem eq 'A'}">Approved</aui:option>
												<aui:option value="D" selected="${procedureDecisionDetailsItem eq 'D'}">Denied</aui:option>
												<aui:option value="N" selected="${decisionDetails eq 'N'}">No PA required</aui:option>
											</aui:select>
										<% } %>
									</aui:col>
									<aui:col span="2">
										<% if (i == 1) { %>
											<aui:select inlineLabel="true" name="<%=procedureReason%>" label="" cssClass="span12 checkForDisable" required="true" showRequiredLabel="true" id="" >
												<aui:option value="">Make a selection</aui:option>
												<aui:option value="P" selected="${procedureReasonDetailsItem eq 'P'}">Pending</aui:option>
												<aui:option value="A1" selected="${procedureReasonDetailsItem eq 'A1'}">Approved, medically appropriate</aui:option>
												<aui:option value="D1" selected="${procedureReasonDetailsItem eq 'D1'}">Denied, not medically necessary</aui:option>
												<aui:option value="D2" selected="${procedureReasonDetailsItem eq 'D2'}">Contract exclusion, not covered under policy</aui:option>
												<aui:option value="D3" selected="${procedureReasonDetailsItem eq 'D3'}">Lack of Timely Filing Administrative Denial</aui:option>
												<aui:option value="N1" selected="${decisionDetails eq 'N1'}">Non-PA Code</aui:option>
											</aui:select>
										<% } else { %>
											<aui:select inlineLabel="true" name="<%=procedureReason%>" label="" cssClass="span12 checkForDisable" id="" >
												<aui:option value="">Make a selection</aui:option>
												<aui:option value="P" selected="${procedureReasonDetailsItem eq 'P'}">Pending</aui:option>
												<aui:option value="A1" selected="${procedureReasonDetailsItem eq 'A1'}">Approved, medically appropriate</aui:option>
												<aui:option value="D1" selected="${procedureReasonDetailsItem eq 'D1'}">Denied, not medically necessary</aui:option>
												<aui:option value="D2" selected="${procedureReasonDetailsItem eq 'D2'}">Contract exclusion, not covered under policy</aui:option>
												<aui:option value="D3" selected="${procedureReasonDetailsItem eq 'D3'}">Lack of Timely Filing Administrative Denial</aui:option>
												<aui:option value="N1" selected="${decisionDetails eq 'N1'}">Non-PA Code</aui:option>
											</aui:select>
										<% } %>
									</aui:col>
									<aui:col span="1">
										<aui:input type="text" inlineLabel="true" name="<%=procedureUnits%>" label="" cssClass="span12 checkForDisable " value="<%=proUnits%>" maxlength="<%= PROCEDURE_UNIT_LEN %>">
											<% if (i == 1) { %>
												<aui:validator name="required" />
											<% } %>
											<aui:validator name="digits" />
										</aui:input>
									</aui:col>
									<!-- fasttrack added Jan 19, 2017 -->
									<aui:col span="1" style="display: none">
										<% if (proFastTrackStr.equals("1")) { %>
											<input type="checkbox" name="<portlet:namespace/><%=procedureFastTrack%>" id="<portlet:namespace/><%=procedureFastTrack%>" class="fasttrack sntdis checkForDisable" checked value="true"
											       onChange="javascript:setCheckboxValue(this.id)" />
										<% } else { %>
											<input type="checkbox" name="<portlet:namespace/><%=procedureFastTrack%>" id="<portlet:namespace/><%=procedureFastTrack%>" class="fasttrack sntdis checkForDisable" value="false"
											       onChange="javascript:setCheckboxValue(this.id)" />
										<% } %>
									</aui:col>
								</aui:row>
					<%
							}	// for (int i = list.size() + 1, n = PROCEDURE_MAX_ROWS; i <= n; i++)
						}	// if (PROCEDURE_MAX_ROWS > list.size())
					%>
					<aui:row id="procedure-table-add " cssClass="checkForLinkDisable sntdis">
						<aui:a href="javascript:showNextRow('procedure-table')">
							<aui:icon image="plus" cssClass="pull-right sntdis" label="pa.procedure.info.label.add.codes" />
						</aui:a>
					</aui:row>
				</aui:container>
			</aui:fieldset>
			
			<aui:fieldset>
				<aui:button-row cssClass="btn-divider">
					<div onmouseenter="setSubmitButtonStatus()">
						<aui:button name="submitProcedureInfo" type="submit" primary="true" cssClass="pull-right submitAction" value="pa.label.submit" onclick="javascript:onSubmit()" />
					</div>
					<div onmouseenter="setSaveButtonStatus()">
						<aui:button name="saveProcedureInfo" type="submit" primary="true" cssClass="pull-right saveAction" value="pa.label.save" onclick="javascript:onSubmit()" />
					</div>
					<aui:button name="cancelProcedureInfo" type="button" id="cancelProcedureInfo" cssClass="btn-gray pull-left cancelAction" value="pa.label.cancel" style="margin-right: .5em" />
				</aui:button-row>
			</aui:fieldset>
		</aui:form>
	</liferay-ui:panel>
</div>
<div class="yui3-skin-sam">
	<div id="modal"></div>
</div>
