<%--
/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
 
 /**
  * Description
  *		This file contain the Provider Information portlet for Prior Auth.
  *
  * CHANGE History
  * 	Version 1.0
  *			All of the changes have been merged.
  * 		This file includes all of the field validation, the busy icon, and the check for leaving the page without saving changes.
  * 	Version 1.1
  *			Moved all javascript except 'areYouSure' and 'YUI' from sui:script block to script block.
  *		Version 1.2
  *			Do not show busy icon when the Submit button is pressed and there are invalid fields.
  *			Display the initial prior auth page whrn the cancel button is pressed.
  *			Moved functions to main.js. 
  *			Use language file for button labels.
  *		Version 1.3
  *			Add parameter to isValidDigit.
  *		Version 1.4
  *			Allow any characters in first name, last name, city, and lab name.
  *			Ordering phone numbers must be 10 digits.
  *			Rendering phone numbers must by blank or 10 digits.
  *			Ordering zip code must be 5 digits.
  *			Rendering zip code must by blank or 5 digits.
  *		Version 1.5
  *			If any rendering field is entered make NPI/TIN required.
  *		Version 1.6
  *			Move Cancel button to the left margin.
  *		Version 1.7
  *			Fix tabbing order.
  *		Version 1.8
  *			Validating rendering name and city as alphanumeric fields	
  *		Version 1.9
  *			Disable tabbing for VOID.
  *		Version 1.10
  *			Disable tabbing if the user is in the HP Employee group.
  *		Version 1.11
  *			Fixed the tabbing order.
  *		Version 1.12
  *			Switch all HTML DOM to jQuery.
  *			Fixed busy spinner for IE.
  *		Version 1.13					11/08/2017
  *			Added the AvalonEmployeeROPA group to read-only for PA.
  *		Version 1.14					01/12/2018
  *			Disable Save button until all required fields are valid.
  *			When the Save button is disabled show a mouse over tooltip with what is missing.
  *		Version 1.15					06/06/2018
  *			Change the cancel button from a hyperlink to a button.
  *		Version 1.16					06/18/2018
  *			Align the AUI fields.
  *		Version 1.17					07/16/2018
  *			Move the label to the left of the field.
  *		Version 1.18					09/20/2018
  *			Fixed the read-only fields.
  *
  */
--%>
<%@page import="com.liferay.portal.kernel.util.PortalUtil"%>
<%@ include file="/html/prior-auth/init.jsp"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ include file="/html/prior-auth/auth-details.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


<%@page import="com.liferay.portal.kernel.service.UserGroupLocalServiceUtil"%>
<%@page import="com.liferay.portal.kernel.model.User"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="theme" %>

<portlet:defineObjects />
<theme:defineObjects />

<portlet:actionURL var='providerInformationPageURL'>
 <portlet:param name="action" value="providerInformationAction" />
</portlet:actionURL>
<portlet:actionURL var='providerInformationCancelURL'>
 <portlet:param name="cancelaction" value="providerInfoCancelAction" />
</portlet:actionURL>
<portlet:actionURL var='providerAuthRedirectURL'>
 <portlet:param name="authRaction" value="authRedirectAction" />
</portlet:actionURL>


<style>
	.avalon__proc_portlet .control-group label {
		pointer-events: none;
	}
</style>

<script>
	<%
		int FIRST_NAME_LEN = 20;
		int LAST_NAME_LEN = 70;
		int PHONE_NUMBER_LEN = 10;
		int ADDRESS_LEN = 55;
		int CITY_LEN = 40;
		int ZIP_LEN = 5;
		
		int NPI_LEN = 10;
		int TIN_LEN = 9;
	%>
</script>

<aui:script>
	var toolTip = "";
	var fromOrderingNpi = false;
	var fromOrderingTin = false;
	var fromRenderingNpi = false;
	var fromRenderingTin = false;

	$(function() {
		
		// Highlight the Provider Information selection
		boldSelection();

		$('form').areYouSure({
			'message' : exitString
		});
		
		// Set the initial save button status
		setSaveButtonStatus();
		
		// Adjust the field widths
		adjustMemberDataWidth();
		adjustProviderDataWidth();
	});

	$(window).resize(function () {
		
		// Reset the fields widths
		adjustMemberDataWidth();
		adjustProviderDataWidth();
	});

	function adjustProviderDataWidth() {
		var memberDetailsSmallSz = (window.innerWidth - parseInt($('.panel-heading').css('width'))) / 15;
		
		// Set the size for the Ordering Provider State and zipcode
		$("#<portlet:namespace/>orderingState").css("cssText", "width:" + memberDetailsSmallSz + "px !important");
		$("#<portlet:namespace/>orderingZip").css("cssText", "width:" + memberDetailsSmallSz + "px !important");
		
		// Set the size for the Rendering Provider State and zipcode
		$("#<portlet:namespace/>renderingState").css("cssText", "width:" + memberDetailsSmallSz + "px !important");
		$("#<portlet:namespace/>renderingZip").css("cssText", "width:" + memberDetailsSmallSz + "px !important");
	}
	
	function getNamespace() {
		return('<portlet:namespace/>');
	}

	function isValidForm() {
		var orderValid = false;
		var renderingValid = false;
		var orderingFirstName = $("#<portlet:namespace/>orderingFirstName").val();
		var orderingLastName = $("#<portlet:namespace/>orderingLastName").val();
		var orderingNpi = $("#<portlet:namespace/>orderingNpi").val();
		var orderingTinEin = $("#<portlet:namespace/>orderingTinEin").val();
		var orderingPhoneNumber = $("#<portlet:namespace/>orderingPhoneNumber").val();
		var orderingFaxNumber = $("#<portlet:namespace/>orderingFaxNumber").val();
		var orderingAddressLine1 = $("#<portlet:namespace/>orderingAddressLine1").val();
		var orderingAddressLine2 = $("#<portlet:namespace/>orderingAddressLine2").val();
		var orderingCity = $("#<portlet:namespace/>orderingCity").val();
		var orderingState = $("#<portlet:namespace/>orderingState").prop("selectedIndex");
		var orderingZip = $("#<portlet:namespace/>orderingZip").val();

		var renderingLabName = $("#<portlet:namespace/>renderingLabName").val();
		var renderingNpi = $("#<portlet:namespace/>renderingNpi").val();
		var renderingTinEin = $("#<portlet:namespace/>renderingTinEin").val();
		var renderingPhoneNumber = $("#<portlet:namespace/>renderingPhoneNumber").val();
		var renderingFaxNumber = $("#<portlet:namespace/>renderingFaxNumber").val();
		var renderingAddressLine1 = $("#<portlet:namespace/>renderingAddressLine1").val();
		var renderingAddressLine2 = $("#<portlet:namespace/>renderingAddressLine2").val();
		var renderingCity = $("#<portlet:namespace/>renderingCity").val();
		var renderingZip = $("#<portlet:namespace/>renderingZip").val();
		
		// Initialize the tool tip for the save button
		toolTip = "";

		// Validate ordering values
		if (isValidAlphaProviderName(orderingFirstName, <%= FIRST_NAME_LEN %>, true) && isValidAlphaProviderName(orderingLastName, <%= LAST_NAME_LEN %>, true) && 
			isValidNpiTin(orderingNpi, orderingTinEin, false) && isValidDigit(orderingPhoneNumber, <%= PHONE_NUMBER_LEN %>, true, false) && 
			isValidDigit(orderingFaxNumber, <%= PHONE_NUMBER_LEN %>, true, false) && isValid(orderingAddressLine1, <%= ADDRESS_LEN %>, true) && 
			isValid(orderingAddressLine2, <%= ADDRESS_LEN %>, false) && isValidAlphaProviderName(orderingCity, <%= CITY_LEN %>, true) && (orderingState > 0) && 
			isValidDigit(orderingZip, <%= ZIP_LEN %>, true, true) && isValidphoneNumber(orderingPhoneNumber) && isValidphoneNumber(orderingFaxNumber)) {
			orderValid = true;
		} else {

			// Test the ordering provider fields
			toolTip += addToToolTip("Ordering Provider invalid fields:", toolTip.length);

			// Test first name
			if (!isValidAlphaProviderName(orderingFirstName, <%= FIRST_NAME_LEN %>, true)) {
				toolTip += addToToolTip("   First Name", toolTip.length);
			}
			
			// Test last name
			if (!isValidAlphaProviderName(orderingLastName, <%= LAST_NAME_LEN %>, true)) {
				toolTip += addToToolTip("   Last Name", toolTip.length);
			}
			
			// Test NPI and TIN/EIN
			if (!isValidNpiTin(orderingNpi, orderingTinEin, false)) {
				toolTip += addToToolTip("   NPI and TIN/EIN", toolTip.length);
			}
			
			// Test phone number
			if (!isValidDigit(orderingPhoneNumber, <%= PHONE_NUMBER_LEN %>, true, false) || !isValidphoneNumber(orderingPhoneNumber)) {
				toolTip += addToToolTip("   Phone Number", toolTip.length);
			}
			
			// Test fax number
			if (!isValidDigit(orderingFaxNumber, <%= PHONE_NUMBER_LEN %>, true, false) || !isValidphoneNumber(orderingFaxNumber)) {
				toolTip += addToToolTip("   Fax Number", toolTip.length);
			}
			
			// Test address line 1
			if (!isValid(orderingAddressLine1, <%= ADDRESS_LEN %>, true)) {
				toolTip += addToToolTip("   Address Line 1", toolTip.length);
			}
					
			// Test address line 2
			if (!isValid(orderingAddressLine2, <%= ADDRESS_LEN %>, false)) {
				toolTip += addToToolTip("   Address Line 2", toolTip.length);
			}
					
			// Test city
			if (!isValidAlphaProviderName(orderingCity, <%= CITY_LEN %>, true)) {
				toolTip += addToToolTip("   City", toolTip.length);
			}
					
			// Test state
			if (orderingState <= 0) {
				toolTip += addToToolTip("   State", toolTip.length);
			}
					
			// Test zip code
			if (!isValidDigit(orderingZip, <%= ZIP_LEN %>, true, true)) {
				toolTip += addToToolTip("   ZIP", toolTip.length);
			}
		}

		// Validate rendering values
		if (isValidAlphaProviderName(renderingLabName, <%= LAST_NAME_LEN %>, false) && isValidNpiTin(renderingNpi, renderingTinEin, true) && 
			isValidDigit(renderingPhoneNumber, <%= PHONE_NUMBER_LEN %>, false, false) && isValidDigit(renderingFaxNumber, <%= PHONE_NUMBER_LEN %>, false, false) && 
			isValid(renderingAddressLine1, <%= ADDRESS_LEN %>, false) && isValid(renderingAddressLine2, <%= ADDRESS_LEN %>, false) && 
			isValidAlphaProviderName(renderingCity, <%= CITY_LEN %>, false) && isValidDigit(renderingZip, <%= ZIP_LEN %>, false, true)) {
			renderingValid = true;
		} else {

			// Test the ordering provider fields
			toolTip += addToToolTip("Rendering Provider invalid fields:", toolTip.length);
			
			// Test lab Name
			if (!isValidAlphaProviderName(renderingLabName, <%= LAST_NAME_LEN %>, false)) {
				toolTip += addToToolTip("   Lab Name", toolTip.length);
			}
			
			// Test NPI and TIN/EIN
			if (!isValidNpiTin(renderingNpi, renderingTinEin, true)) {
				toolTip += addToToolTip("   NPI and TIN/EIN", toolTip.length);
			}
			
			// Test phone number
			if (!isValidDigit(renderingPhoneNumber, <%= PHONE_NUMBER_LEN %>, false, false)) {
				toolTip += addToToolTip("   Phone Number", toolTip.length);
			}
			
			// Test fax number
			if (!isValidDigit(renderingFaxNumber, <%= PHONE_NUMBER_LEN %>, false, false)) {
				toolTip += addToToolTip("   Fax Number", toolTip.length);
			}
			
			// Test address line 1
			if (!isValid(renderingAddressLine1, <%= ADDRESS_LEN %>, false)) {
				toolTip += addToToolTip("   Address Line 1", toolTip.length);
			}
					
			// Test address line 2
			if (!isValid(renderingAddressLine2, <%= ADDRESS_LEN %>, false)) {
				toolTip += addToToolTip("   Address Line 2", toolTip.length);
			}
					
			// Test city
			if (!isValidAlphaProviderName(renderingCity, <%= CITY_LEN %>, false)) {
				toolTip += addToToolTip("   City", toolTip.length);
			}
					
			// Test zip code
			if (!isValidDigit(renderingZip, <%= ZIP_LEN %>, false, true)) {
				toolTip += addToToolTip("   ZIP", toolTip.length);
			}
		}
		
		return orderValid && renderingValid;
	}

	function displayProp_NPIorTIN(id, showFlag) {
		var elementId = "<portlet:namespace/>" + id;

		if ($("#" + elementId)) {
			var element = $("#" + elementId);

			if ($("#" + elementId).attr("class")) {

				if (showFlag == "true") {
					$("#" + elementId).attr("class", $("#" + elementId).attr("class").replace("hide", ""));
				} else {
					$("#" + elementId).attr("class", $("#" + elementId).attr("class") + " hide");
				}
			}
		}
	}

	function checkForRendering() {
		var retValue = false;

		var renderingLabName = $("#" + getNamespace() + "renderingLabName").val();
		var renderingPhoneNumber = $("#" + getNamespace() + "renderingPhoneNumber").val();
		var renderingFaxNumber = $("#" + getNamespace() + "renderingFaxNumber").val();
		var renderingAddressLine1 = $("#" + getNamespace() + "renderingAddressLine1").val();
		var renderingAddressLine2 = $("#" + getNamespace() + "renderingAddressLine2").val();
		var renderingCity = $("#" + getNamespace() + "renderingCity").val();
		var renderingState = $("#" + getNamespace() + "renderingState").prop("selectedIndex");
		var renderingZip = $("#" + getNamespace() + "renderingZip").val();

		if ((renderingLabName.length > 0) || (renderingPhoneNumber.length > 0) || (renderingAddressLine1.length > 0) || 
			(renderingAddressLine2.length > 0) || (renderingCity.length > 0) || (renderingState > 0) || (renderingZip.length > 0)) {
			retValue = true;
		}

		return retValue;
	}

	function isValidNpiTin(_npi, _tin, _renderingFlag) {
		var rtnValue = false;

		var isValidNPI = isValidDigit(_npi, 10, true, true);
		var isValidTIN = isValidDigit(_tin, 9, true, true);

		if (isValidNPI && isValidTIN) {
			rtnValue = true;
		} else if (isValidNPI && (_tin.length == 0)) {
			rtnValue = true;
		} else if (isValidTIN && (_npi.length == 0)) {
			rtnValue = true;
		}
		
		if (_renderingFlag == true) {
			if ((_npi.length == 0) && (_tin.length == 0) && checkForRendering()) {
				rtnValue = false;	
			} else if ((_npi.length == 0) && (_tin.length == 0) && !checkForRendering()){
				rtnValue = true;
			}
		}
		
		return rtnValue;
	}

	/**To disable tabbing in ordering fields when submission status is sent**/
	function disableOrderingfieldsTabbing() {

		// Disable tabbing to all fields
		$('#<portlet:namespace/>orderingFirstName').prop('tabIndex', '-1'); 
		$('#<portlet:namespace/>orderingLastName').prop('tabIndex', '-1'); 
		$('#<portlet:namespace/>orderingNpi').prop('tabIndex', '-1'); 
		$('#<portlet:namespace/>orderingTinEin').prop('tabIndex', '-1'); 
		$('#<portlet:namespace/>orderingPhoneNumber').prop('tabIndex', '-1'); 
		$('#<portlet:namespace/>orderingFaxNumber').prop('tabIndex', '-1'); 
		$('#<portlet:namespace/>orderingAddressLine1').prop('tabIndex', '-1'); 
		$('#<portlet:namespace/>orderingAddressLine2').prop('tabIndex', '-1'); 
		$('#<portlet:namespace/>orderingCity').prop('tabIndex', '-1'); 
		$('#<portlet:namespace/>orderingState').prop('tabIndex', '-1'); 
		$('#<portlet:namespace/>orderingZip').prop('tabIndex', '-1'); 
	}

	function disableTabbing() {

		// Disable tabbing to all fields
		$('#<portlet:namespace/>orderingFirstName').prop('tabIndex', '-1'); 
		$('#<portlet:namespace/>orderingLastName').prop('tabIndex', '-1'); 
		$('#<portlet:namespace/>orderingNpi').prop('tabIndex', '-1'); 
		$('#<portlet:namespace/>orderingTinEin').prop('tabIndex', '-1'); 
		$('#<portlet:namespace/>orderingPhoneNumber').prop('tabIndex', '-1'); 
		$('#<portlet:namespace/>orderingFaxNumber').prop('tabIndex', '-1'); 
		$('#<portlet:namespace/>orderingAddressLine1').prop('tabIndex', '-1'); 
		$('#<portlet:namespace/>orderingAddressLine2').prop('tabIndex', '-1'); 
		$('#<portlet:namespace/>orderingCity').prop('tabIndex', '-1'); 
		$('#<portlet:namespace/>orderingState').prop('tabIndex', '-1'); 
		$('#<portlet:namespace/>orderingZip').prop('tabIndex', '-1'); 

		$('#<portlet:namespace/>renderingLabName').prop('tabIndex', '-1'); 
		$('#<portlet:namespace/>renderingNpi').prop('tabIndex', '-1'); 
		$('#<portlet:namespace/>renderingTinEin').prop('tabIndex', '-1'); 
		$('#<portlet:namespace/>renderingPhoneNumber').prop('tabIndex', '-1'); 
		$('#<portlet:namespace/>renderingFaxNumber').prop('tabIndex', '-1'); 
		$('#<portlet:namespace/>renderingAddressLine1').prop('tabIndex', '-1'); 
		$('#<portlet:namespace/>renderingAddressLine2').prop('tabIndex', '-1'); 
		$('#<portlet:namespace/>renderingCity').prop('tabIndex', '-1'); 
		$('#<portlet:namespace/>renderingState').prop('tabIndex', '-1'); 
		$('#<portlet:namespace/>renderingZip').prop('tabIndex', '-1'); 

		// Disable tabbing to all buttons
		$('#<portlet:namespace/>saveProviderInfo').prop('tabIndex', '-1');
		$('#<portlet:namespace/>cancelProviderInfo').prop('tabIndex', '-1');
	}

	function disableOrderingfieldsDropdowns() {
		var browserStr = getBrowser();

		// Disable all of the ordering provider dropdowns for IE
		if ((browserStr == "IE") || (browserStr == "Firefox") || (browserStr == "unknown")) {
			$('#<portlet:namespace/>orderingState').attr('disabled', true);
		}
	}

	function disableDropdowns() {
		var browserStr = getBrowser();

		// Disable all of the dropdowns for IE
		if ((browserStr == "IE") || (browserStr == "Firefox") || (browserStr == "unknown")) {
			$('#<portlet:namespace/>orderingState').attr('disabled', true);
			$('#<portlet:namespace/>renderingState').attr('disabled', true);
		}
	}

	/* preventing save button functionality when any validation fails for form */
	YUI().use('aui-node','aui-modal','event',function(Y) {
		Y.one('.saveAction').on('click',function(e){
			if(Y.one('.error-field')){
				Y.one('.error-field').focus();
				e.preventDefault();
			} else{
		         window.onbeforeunload = function(e) {
		                e = e || window.event;
		                e.preventDefault = false;
		                e.cancelBubble = false;
		         }
			}
		});
	});

	var cancelUrl = '<%=providerInformationCancelURL.toString()%>';
	var authCreated = "<c:out value='${authCreated}'/>";
	YUI().use('aui-modal', function(Y) {

		if (authCreated != "") {
			var modal = new Y.Modal({
				bodyContent : 'Authorization Number:' + authCreated,
				centered : true,
				destroyOnHide : false,
				headerContent : 'The authorization has been saved.',
				modal : true,
				render : '#modal',
				resizable : {
					handles : 'b, r'
				},

				visible : true,
				width : 450
			}).render();

			modal.addToolbar([ {
				label : 'OK',
				on : {
					click : function() {
						var url = '<%=providerAuthRedirectURL.toString()%>';
						window.location.href = url;

					}
				}
			}
			]);
		}
	});

	/****For disabling tabbing when the Auth submission status are related to Void****/
	YUI().ready('aui-node', 'event', function(Y) {
		/*
		   Set the page to read only if the user is in the HealthPlanEmployee or AvalonEmployeeROPA group and not in 
		   AvalonAdmin, AvalonEmployee, AvalonProvider, PortalApprover, or PortalCreator groups.
		*/
		<% 
			boolean readOnlyFlag = false;
			boolean hpEmployeeFlag = false;
			boolean avalonEmployeeRoPaFlag = false;
			boolean notReadOnlyGroupFlag = false;
			User userU = themeDisplay.getUser();
			long[] groups = userU.getUserGroupIds();
			int length = groups.length;
			for (int i = 0; i < length; ++i) {
				String nextGroup = UserGroupLocalServiceUtil.getUserGroup(groups[i]).getName();
				if (nextGroup.equalsIgnoreCase("HealthPlanEmployee")) {
					hpEmployeeFlag = true;
				} else if (nextGroup.equalsIgnoreCase("AvalonEmployeeROPA")) {
					avalonEmployeeRoPaFlag = true;
				} else if (nextGroup.equalsIgnoreCase("AvalonAdmin") || nextGroup.equalsIgnoreCase("AvalonEmployee") || 
						   nextGroup.equalsIgnoreCase("AvalonProvider") || nextGroup.equalsIgnoreCase("PortalApprover") || 
						   nextGroup.equalsIgnoreCase("PortalCreator")) {
					notReadOnlyGroupFlag = true;
				}
			}
			if (!notReadOnlyGroupFlag && (hpEmployeeFlag || avalonEmployeeRoPaFlag)) {
				readOnlyFlag = true;
			}
		%>
		if (<%= readOnlyFlag %>) {

	  		// Make the page readonly
  		 	Y.one('.avalon__proc_portlet').addClass('optdisable');
			
			// Disable the tabbing for read only
			disableTabbing();
			
			// Disable the dropdowns in IE and Firefox
			disableDropdowns();
		} else if(Y.one('#authSubmitStatusdisableId')){
			var authSubmitStatusdisable = Y.one('#authSubmitStatusdisableId').val();
			
			if(authSubmitStatusdisable!=null && authSubmitStatusdisable!="" && authSubmitStatusdisable!='undefined'){
				if(authSubmitStatusdisable=="Void HP - Submitted" || authSubmitStatusdisable=="Void-Submitted" || authSubmitStatusdisable=="Void"||authSubmitStatusdisable=="Void HP"){

			  		// Make the page readonly
		  		 	Y.one('.avalon__proc_portlet').addClass('optdisable');
			  		
					disableTabbing();
					
					// Disable the dropdowns in IE and Firefox
					disableDropdowns();
				}
				/**In sent status only ordering fields have to be disabled****/
				if(authSubmitStatusdisable=="Sent"){
					disableOrderingfieldsTabbing();
					
					// Disable the dropdowns in IE and Firefox
					disableOrderingfieldsDropdowns();
				}
			}
		}
	});

	function setSaveButtonStatus() {
		
		// Disable the save button until all the fields are valid
		if (isValidForm()) {
			$("#<portlet:namespace/>saveProviderInfo").removeAttr("disabled");
		} else {
			$("#<portlet:namespace/>saveProviderInfo").attr("disabled", "disabled");
		}
		
		// Add the mouse over text for the save button
		$("#<portlet:namespace/>saveProviderInfo").prop("title", toolTip);
	}
</aui:script>

<div id="busy_indicator" style="display: none">
 	<img src="<%=request.getContextPath()%>/images/busy-spinner.gif" alt="Busy indicator">
</div>

<div class="avalon__proc_portlet">
	<liferay-ui:panel title="pa.provider.info" collapsible="false">
		<aui:form name="providerInformationDetails" cssClass="form-inline" action="${providerInformationPageURL}" method="post" commandName="providerInformationFO" onChange="javascript:setSaveButtonStatus()">
			<aui:fieldset label="pa.provider.info.ordering.provider.info" id="ordering-fields">
				<aui:container>
					<aui:row>
						<aui:col span="6">
							<aui:input type="text" inlineLabel="true" name="orderingFirstName" maxlength="<%= FIRST_NAME_LEN %>" class="orderingFirstName" label="pa.label.first.name" value="${providerInformationFO.orderingFirstName}" id="orderingFirstName">
								<aui:validator name="required" />
							</aui:input>
						</aui:col>
						<aui:col span="6">
							<aui:input type="text" inlineLabel="true" name="orderingLastName" maxlength="<%= LAST_NAME_LEN %>" label="pa.label.last.name" value="${providerInformationFO.orderingLastName}">
								<aui:validator name="required" />
							</aui:input>
						</aui:col>
					</aui:row>
					<aui:row cssClass="hide" id="orderingNPI-TIN-helper">
						<aui:col span="12">
							<span style="color: blue">You must enter either a valid 10 digit NPI or a valid 9 digit TIN/EIN for the ordering provider</span>
						</aui:col>
					</aui:row>
					<aui:row>
						<aui:col span="6">
							<aui:input type="text" inlineLabel="true" name="orderingNpi" maxlength="<%= NPI_LEN %>" label="pa.label.npi" value="${providerInformationFO.orderingNpi}" 
							           onFocus="javascript:displayProp_NPIorTIN('orderingNPI-TIN-helper', 'true')" onBlur="javascript:displayProp_NPIorTIN('orderingNPI-TIN-helper', 'faLse')">
								<aui:validator name="custom" errorMessage="You must enter either a valid NPI or a valid TIN/EIN">
									function (val, fieldNode, ruleValue) {
										var validNpiTin = isValidNpiTin(val, $("#<portlet:namespace />orderingTinEin").val(), false);

										if (!fromOrderingTin) {
											fromOrderingTin = true;

											// Save the current active element
											var activeId = document.activeElement.id;

											// Force validation of orderingTinEin (only do this once)
											$("#<portlet:namespace />orderingTinEin").focus();
											$("#<portlet:namespace />orderingTinEin").blur();
	
											// Set focus to the saved field
											$("#" + activeId).focus();
										}
										fromOrderingTin = false;

										return validNpiTin;
									}
								</aui:validator>
							</aui:input>
						</aui:col>
						<aui:col span="6">
							<aui:input type="text" inlineLabel="true" name="orderingTinEin" maxlength="<%= TIN_LEN %>" label="pa.label.tin.ein" value="${providerInformationFO.orderingTinEin}" 
							           onFocus="javascript:displayProp_NPIorTIN('orderingNPI-TIN-helper', 'true')" onBlur="javascript:displayProp_NPIorTIN('orderingNPI-TIN-helper', 'faLse')">
								<aui:validator name="custom" errorMessage="You must enter either a valid NPI or a valid TIN/EIN">
									function (val, fieldNode, ruleValue) {
										var validNpiTin = isValidNpiTin($("#<portlet:namespace />orderingNpi").val(), val, false);

										if (!fromOrderingNpi) {
											fromOrderingNpi = true;

											// Save the current active element
											var activeId = document.activeElement.id;

											// Force validation of orderingNpi (only do this once)
											$("#<portlet:namespace />orderingNpi").focus();
											$("#<portlet:namespace />orderingNpi").blur();
	
											// Set focus to the saved field
											$("#" + activeId).focus();
										}
										fromOrderingNpi = false;

										return validNpiTin;
									}
								</aui:validator>
							</aui:input>
						</aui:col>
					</aui:row>
					<aui:row>
						<aui:col span="6">
							<aui:input type="text" inlineLabel="true" name="orderingPhoneNumber" maxlength="<%= PHONE_NUMBER_LEN %>" label="pa.label.phone.number" value="${providerInformationFO.orderingPhoneNumber}">
								<aui:validator name="required" />
								<aui:validator name="digits" />
								<aui:validator name="minLength"><%= PHONE_NUMBER_LEN %></aui:validator>
							</aui:input>
						</aui:col>
						<aui:col span="6">
							<aui:input type="text" inlineLabel="true" name="orderingFaxNumber" maxlength="<%= PHONE_NUMBER_LEN %>" label="pa.label.fax.number" value="${providerInformationFO.orderingFaxNumber}">
								<aui:validator name="required" />
								<aui:validator name="digits" />
								<aui:validator name="minLength"><%= PHONE_NUMBER_LEN %></aui:validator>
							</aui:input>
						</aui:col>
					</aui:row>
					<aui:row>
						<aui:col span="6">
							<aui:input type="text" inlineLabel="true" name="orderingAddressLine1" maxlength="<%= ADDRESS_LEN %>" label="pa.label.address" value="${providerInformationFO.orderingAddressLine1}">
								<aui:validator name="required" />
						</aui:input>
						</aui:col>
						<aui:col span="6">
							<aui:input type="text" inlineLabel="true" name="orderingAddressLine2" maxlength="<%= ADDRESS_LEN %>" label="pa.label.address.additional" value="${providerInformationFO.orderingAddressLine2}">
							</aui:input>
						</aui:col>
					</aui:row>
					<aui:row>
						<aui:col span="6">
							<aui:input type="text" inlineLabel="true" name="orderingCity" maxlength="<%= CITY_LEN %>" label="pa.label.city" value="${providerInformationFO.orderingCity}">
								<aui:validator name="required" />
							</aui:input>
						</aui:col>
						<aui:col span="3">
							<aui:select inlineLabel="true" name="orderingState" label="pa.label.state" cssClass="span7" required="true" showRequiredLabel="true">
								<aui:option value="">Selection</aui:option>
								<aui:option value="AL" selected="${providerInformationFO.orderingState eq 'AL'}">Alabama</aui:option>
								<aui:option value="AK" selected="${providerInformationFO.orderingState eq 'AK'}">Alaska</aui:option>
								<aui:option value="AZ" selected="${providerInformationFO.orderingState eq 'AZ'}">Arizona</aui:option>
								<aui:option value="AR" selected="${providerInformationFO.orderingState eq 'AR'}">Arkansas</aui:option>
								<aui:option value="CA" selected="${providerInformationFO.orderingState eq 'CA'}">California</aui:option>
								<aui:option value="CO" selected="${providerInformationFO.orderingState eq 'CO'}">Colorado</aui:option>
								<aui:option value="CT" selected="${providerInformationFO.orderingState eq 'CT'}">Connecticut</aui:option>
								<aui:option value="DE" selected="${providerInformationFO.orderingState eq 'DE'}">Delaware</aui:option>
								<aui:option value="FL" selected="${providerInformationFO.orderingState eq 'FL'}">Florida</aui:option>
								<aui:option value="GA" selected="${providerInformationFO.orderingState eq 'GA'}">Georgia</aui:option>
								<aui:option value="HI" selected="${providerInformationFO.orderingState eq 'HI'}">Hawaii</aui:option>
								<aui:option value="ID" selected="${providerInformationFO.orderingState eq 'ID'}">Idaho</aui:option>
								<aui:option value="IL" selected="${providerInformationFO.orderingState eq 'IL'}">Illinois</aui:option>
								<aui:option value="IN" selected="${providerInformationFO.orderingState eq 'IN'}">Indiana</aui:option>
								<aui:option value="IA" selected="${providerInformationFO.orderingState eq 'IA'}">Iowa</aui:option>
								<aui:option value="KS" selected="${providerInformationFO.orderingState eq 'KS'}">Kansas</aui:option>
								<aui:option value="KY" selected="${providerInformationFO.orderingState eq 'KY'}">Kentucky</aui:option>
								<aui:option value="LA" selected="${providerInformationFO.orderingState eq 'LA'}">Louisiana</aui:option>
								<aui:option value="ME" selected="${providerInformationFO.orderingState eq 'ME'}">Maine</aui:option>
								<aui:option value="MD" selected="${providerInformationFO.orderingState eq 'MD'}">Maryland</aui:option>
								<aui:option value="MA" selected="${providerInformationFO.orderingState eq 'MA'}">Massachusetts</aui:option>
								<aui:option value="MI" selected="${providerInformationFO.orderingState eq 'MI'}">Michigan</aui:option>
								<aui:option value="MN" selected="${providerInformationFO.orderingState eq 'MN'}">Minnesota</aui:option>
								<aui:option value="MS" selected="${providerInformationFO.orderingState eq 'MS'}">Mississippi</aui:option>
								<aui:option value="MO" selected="${providerInformationFO.orderingState eq 'MO'}">Missouri</aui:option>
								<aui:option value="MT" selected="${providerInformationFO.orderingState eq 'MT'}">Montana</aui:option>
								<aui:option value="NE" selected="${providerInformationFO.orderingState eq 'NE'}">Nebraska</aui:option>
								<aui:option value="NV" selected="${providerInformationFO.orderingState eq 'NV'}">Nevada</aui:option>
								<aui:option value="NH" selected="${providerInformationFO.orderingState eq 'NH'}">New Hampshire</aui:option>
								<aui:option value="NJ" selected="${providerInformationFO.orderingState eq 'NJ'}">New Jersey</aui:option>
								<aui:option value="NM" selected="${providerInformationFO.orderingState eq 'NM'}">New Mexico</aui:option>
								<aui:option value="NY" selected="${providerInformationFO.orderingState eq 'NY'}">New York</aui:option>
								<aui:option value="NC" selected="${providerInformationFO.orderingState eq 'NC'}">North Carolina</aui:option>
								<aui:option value="ND" selected="${providerInformationFO.orderingState eq 'ND'}">North Dakota</aui:option>
								<aui:option value="OH" selected="${providerInformationFO.orderingState eq 'OH'}">Ohio</aui:option>
								<aui:option value="OK" selected="${providerInformationFO.orderingState eq 'OK'}">Oklahoma</aui:option>
								<aui:option value="OR" selected="${providerInformationFO.orderingState eq 'OR'}">Oregon</aui:option>
								<aui:option value="PA" selected="${providerInformationFO.orderingState eq 'PA'}">Pennsylvania</aui:option>
								<aui:option value="RI" selected="${providerInformationFO.orderingState eq 'RI'}">Rhode Island</aui:option>
								<aui:option value="SC" selected="${providerInformationFO.orderingState eq 'SC'}">South Carolina</aui:option>
								<aui:option value="SD" selected="${providerInformationFO.orderingState eq 'SD'}">South Dakota</aui:option>
								<aui:option value="TN" selected="${providerInformationFO.orderingState eq 'TN'}">Tennessee</aui:option>
								<aui:option value="TX" selected="${providerInformationFO.orderingState eq 'TX'}">Texas</aui:option>
								<aui:option value="UT" selected="${providerInformationFO.orderingState eq 'UT'}">Utah</aui:option>
								<aui:option value="VT" selected="${providerInformationFO.orderingState eq 'VT'}">Vermont</aui:option>
								<aui:option value="VA" selected="${providerInformationFO.orderingState eq 'VA'}">Virginia</aui:option>
								<aui:option value="WA" selected="${providerInformationFO.orderingState eq 'WA'}">Washington</aui:option>
								<aui:option value="DC" selected="${providerInformationFO.orderingState eq 'DC'}">Washington, D.C.</aui:option>
								<aui:option value="WV" selected="${providerInformationFO.orderingState eq 'WV'}">West Virginia</aui:option>
								<aui:option value="WI" selected="${providerInformationFO.orderingState eq 'WI'}">Wisconsin</aui:option>
								<aui:option value="WY" selected="${providerInformationFO.orderingState eq 'WY'}">Wyoming</aui:option>
							</aui:select>
						</aui:col>
						<aui:col span="3">
							<aui:input type="text" inlineLabel="true" name="orderingZip" maxlength="<%= ZIP_LEN %>" label="pa.label.zip.code" value="${providerInformationFO.orderingZip}">
								<aui:validator name="required" />
								<aui:validator name="digits" />
								<aui:validator name="minLength"><%= ZIP_LEN %></aui:validator>
							</aui:input>
						</aui:col>
					</aui:row>
				</aui:container>
			</aui:fieldset>
			
			<aui:fieldset label="pa.provider.info.rendering.provider.info" id="rendering-fields">
				<aui:container>
					<aui:row>
						<aui:col span="6">
							<aui:input type="text" inlineLabel="true" name="renderingLabName" maxlength="<%= LAST_NAME_LEN %>" label="pa.label.lab.name" value="${providerInformationFO.renderingLabName}" >
							</aui:input>
						</aui:col>
					</aui:row>
					<aui:row cssClass="hide" id="renderingNPI-TIN-helper">
						<aui:col span="12">
							<span style="color: blue">If you enter any information on the rendering provider, you must also enter either a valid 10 digit NPI or a valid 9 digit TIN/EIN for the rendering provider</span>
						</aui:col>
					</aui:row>
					<aui:row>
						<aui:col span="6">
							<aui:input type="text" inlineLabel="true" name="renderingNpi" maxlength="<%= NPI_LEN %>" label="pa.label.npi" value="${providerInformationFO.renderingNpi}" 
							           onFocus="javascript:displayProp_NPIorTIN('renderingNPI-TIN-helper', 'true')" onBlur="javascript:displayProp_NPIorTIN('renderingNPI-TIN-helper', 'faLse')">
								<aui:validator name="custom" errorMessage="You must enter either a valid NPI or a valid TIN/EIN">
									function (val, fieldNode, ruleValue) {
										var renderFlag = true;
							
										if (checkForRendering()) {
											renderFlag = false;
										}
										var validNpiTin = isValidNpiTin(val, $("#<portlet:namespace />renderingTinEin").val(), renderFlag);
							
										if (!fromRenderingTin) {
											fromRenderingTin = true;
							
											// Save the current active element
											var activeId = document.activeElement.id;
								
											// Force validation of renderingTinEin (only do this once)
											$("#<portlet:namespace />renderingTinEin").focus();
											$("#<portlet:namespace />renderingTinEin").blur();
							
											// Set focus to the saved field
											$("#" + activeId).focus();
										}
										fromRenderingTin = false;
							
										return validNpiTin;
									}
								</aui:validator>
							</aui:input>
						</aui:col>
						<aui:col span="6">
							<aui:input type="text" inlineLabel="true" name="renderingTinEin" maxlength="<%= TIN_LEN %>" label="pa.label.tin.ein" value="${providerInformationFO.renderingTinEin}" 
							           onFocus="javascript:displayProp_NPIorTIN('renderingNPI-TIN-helper', 'true')" onBlur="javascript:displayProp_NPIorTIN('renderingNPI-TIN-helper', 'faLse')">
								<aui:validator name="custom" errorMessage="You must enter either a valid NPI or a valid TIN/EIN">
									function (val, fieldNode, ruleValue) {
										var renderFlag = true;
										
										if (checkForRendering()) {
											renderFlag = false;
										}
										var validNpiTin = isValidNpiTin($("#<portlet:namespace />renderingNpi").val(), val, renderFlag);
										
										if (!fromRenderingNpi) {
											fromRenderingNpi = true;
											
											// Save the current active element
											var activeId = document.activeElement.id;
												
											// Force validation of renderingNpi (only do this once)
											$("#<portlet:namespace />renderingNpi").focus();
											$("#<portlet:namespace />renderingNpi").blur();
											
											// Set focus to the saved field
											$("#" + activeId).focus();
										}
										fromRenderingNpi = false;
										
										return validNpiTin;
									}
								</aui:validator>
							</aui:input>
						</aui:col>
					</aui:row>
					<aui:row>
						<aui:col span="6">
							<aui:input type="text" inlineLabel="true" name="renderingPhoneNumber" maxlength="<%= PHONE_NUMBER_LEN %>" label="pa.label.phone.number" value="${providerInformationFO.renderingPhoneNumber}" >
								<aui:validator name="digits" />
								<aui:validator name="minLength"><%= PHONE_NUMBER_LEN %></aui:validator>
							</aui:input>
						</aui:col>
						<aui:col span="6">
							<aui:input type="text" inlineLabel="true" name="renderingFaxNumber" maxlength="<%= PHONE_NUMBER_LEN %>" label="pa.label.fax.number" value="${providerInformationFO.renderingFaxNumber}" >
								<aui:validator name="digits" />
								<aui:validator name="minLength"><%= PHONE_NUMBER_LEN %></aui:validator>
							</aui:input>
						</aui:col>
					</aui:row>
					<aui:row>
						<aui:col span="6">
							<aui:input type="text" inlineLabel="true" name="renderingAddressLine1" maxlength="<%= ADDRESS_LEN %>" label="pa.label.address" value="${providerInformationFO.renderingAddressLine1}" >
							</aui:input>
						</aui:col>
					<aui:col span="6">
						<aui:input type="text" inlineLabel="true" name="renderingAddressLine2" maxlength="<%= ADDRESS_LEN %>" label="pa.label.address.additional" value="${providerInformationFO.renderingAddressLine2}" >
						</aui:input>
					</aui:col>
					</aui:row>
					<aui:row>
						<aui:col span="6">
							<aui:input type="text" inlineLabel="true" name="renderingCity" maxlength="<%= CITY_LEN %>" label="pa.label.city" value="${providerInformationFO.renderingCity}" >
							</aui:input>
						</aui:col>
						<aui:col span="3">
							<aui:select inlineLabel="true" name="renderingState" label="pa.label.state" cssClass="span7" >
								<aui:option value="">Selection</aui:option>
								<aui:option value="AL" selected="${providerInformationFO.renderingState eq 'AL'}">Alabama</aui:option>
								<aui:option value="AK" selected="${providerInformationFO.renderingState eq 'AK'}">Alaska</aui:option>
								<aui:option value="AZ" selected="${providerInformationFO.renderingState eq 'AZ'}">Arizona</aui:option>
								<aui:option value="AR" selected="${providerInformationFO.renderingState eq 'AR'}">Arkansas</aui:option>
								<aui:option value="CA" selected="${providerInformationFO.renderingState eq 'CA'}">California</aui:option>
								<aui:option value="CO" selected="${providerInformationFO.renderingState eq 'CO'}">Colorado</aui:option>
								<aui:option value="CT" selected="${providerInformationFO.renderingState eq 'CT'}">Connecticut</aui:option>
								<aui:option value="DE" selected="${providerInformationFO.renderingState eq 'DE'}">Delaware</aui:option>
								<aui:option value="FL" selected="${providerInformationFO.renderingState eq 'FL'}">Florida</aui:option>
								<aui:option value="GA" selected="${providerInformationFO.renderingState eq 'GA'}">Georgia</aui:option>
								<aui:option value="HI" selected="${providerInformationFO.renderingState eq 'HI'}">Hawaii</aui:option>
								<aui:option value="ID" selected="${providerInformationFO.renderingState eq 'ID'}">Idaho</aui:option>
								<aui:option value="IL" selected="${providerInformationFO.renderingState eq 'IL'}">Illinois</aui:option>
								<aui:option value="IN" selected="${providerInformationFO.renderingState eq 'IN'}">Indiana</aui:option>
								<aui:option value="IA" selected="${providerInformationFO.renderingState eq 'IA'}">Iowa</aui:option>
								<aui:option value="KS" selected="${providerInformationFO.renderingState eq 'KS'}">Kansas</aui:option>
								<aui:option value="KY" selected="${providerInformationFO.renderingState eq 'KY'}">Kentucky</aui:option>
								<aui:option value="LA" selected="${providerInformationFO.renderingState eq 'LA'}">Louisiana</aui:option>
								<aui:option value="ME" selected="${providerInformationFO.renderingState eq 'ME'}">Maine</aui:option>
								<aui:option value="MD" selected="${providerInformationFO.renderingState eq 'MD'}">Maryland</aui:option>
								<aui:option value="MA" selected="${providerInformationFO.renderingState eq 'MA'}">Massachusetts</aui:option>
								<aui:option value="MI" selected="${providerInformationFO.renderingState eq 'MI'}">Michigan</aui:option>
								<aui:option value="MN" selected="${providerInformationFO.renderingState eq 'MN'}">Minnesota</aui:option>
								<aui:option value="MS" selected="${providerInformationFO.renderingState eq 'MS'}">Mississippi</aui:option>
								<aui:option value="MO" selected="${providerInformationFO.renderingState eq 'MO'}">Missouri</aui:option>
								<aui:option value="MT" selected="${providerInformationFO.renderingState eq 'MT'}">Montana</aui:option>
								<aui:option value="NE" selected="${providerInformationFO.renderingState eq 'NE'}">Nebraska</aui:option>
								<aui:option value="NV" selected="${providerInformationFO.renderingState eq 'NV'}">Nevada</aui:option>
								<aui:option value="NH" selected="${providerInformationFO.renderingState eq 'NH'}">New Hampshire</aui:option>
								<aui:option value="NJ" selected="${providerInformationFO.renderingState eq 'NJ'}">New Jersey</aui:option>
								<aui:option value="NM" selected="${providerInformationFO.renderingState eq 'NM'}">New Mexico</aui:option>
								<aui:option value="NY" selected="${providerInformationFO.renderingState eq 'NY'}">New York</aui:option>
								<aui:option value="NC" selected="${providerInformationFO.renderingState eq 'NC'}">North Carolina</aui:option>
								<aui:option value="ND" selected="${providerInformationFO.renderingState eq 'ND'}">North Dakota</aui:option>
								<aui:option value="OH" selected="${providerInformationFO.renderingState eq 'OH'}">Ohio</aui:option>
								<aui:option value="OK" selected="${providerInformationFO.renderingState eq 'OK'}">Oklahoma</aui:option>
								<aui:option value="OR" selected="${providerInformationFO.renderingState eq 'OR'}">Oregon</aui:option>
								<aui:option value="PA" selected="${providerInformationFO.renderingState eq 'PA'}">Pennsylvania</aui:option>
								<aui:option value="RI" selected="${providerInformationFO.renderingState eq 'RI'}">Rhode Island</aui:option>
								<aui:option value="SC" selected="${providerInformationFO.renderingState eq 'SC'}">South Carolina</aui:option>
								<aui:option value="SD" selected="${providerInformationFO.renderingState eq 'SD'}">South Dakota</aui:option>
								<aui:option value="TN" selected="${providerInformationFO.renderingState eq 'TN'}">Tennessee</aui:option>
								<aui:option value="TX" selected="${providerInformationFO.renderingState eq 'TX'}">Texas</aui:option>
								<aui:option value="UT" selected="${providerInformationFO.renderingState eq 'UT'}">Utah</aui:option>
								<aui:option value="VT" selected="${providerInformationFO.renderingState eq 'VT'}">Vermont</aui:option>
								<aui:option value="VA" selected="${providerInformationFO.renderingState eq 'VA'}">Virginia</aui:option>
								<aui:option value="WA" selected="${providerInformationFO.renderingState eq 'WA'}">Washington</aui:option>
								<aui:option value="DC" selected="${providerInformationFO.renderingState eq 'DC'}">Washington, D.C.</</aui:option>
								<aui:option value="WV" selected="${providerInformationFO.renderingState eq 'WV'}">West Virginia</aui:option>
								<aui:option value="WI" selected="${providerInformationFO.renderingState eq 'WI'}">Wisconsin</aui:option>
								<aui:option value="WY" selected="${providerInformationFO.renderingState eq 'WY'}">Wyoming</aui:option>
							</aui:select>
						</aui:col>
						<aui:col span="3">
							<aui:input type="text" inlineLabel="true" name="renderingZip" maxlength="<%= ZIP_LEN %>" label="pa.label.zip.code" value="${providerInformationFO.renderingZip}" >
								<aui:validator name="digits" />
								<aui:validator name="minLength"><%= ZIP_LEN %></aui:validator>
							</aui:input>
						</aui:col>
					</aui:row>
				</aui:container>
			</aui:fieldset>
			
			<aui:fieldset>
				<aui:button-row cssClass="btn-divider">
					<div onmouseenter="setSaveButtonStatus()">
						<aui:button name="saveProviderInfo" type="button" primary="true" cssClass="pull-right saveAction" value="pa.label.save" onclick="javascript:checkValidForm('providerInformationDetails')" />
					</div>
					<aui:button name="cancelProviderInfo" id="cancelProviderInfo" cssClass="btn-gray pull-left" type="button" value="pa.label.cancel" onclick="javascript:closePortlet()" />
				</aui:button-row>
			</aui:fieldset>
		</aui:form>
	</liferay-ui:panel>
</div>

<div class="yui3-skin-sam">
	<div id="modal"></div>
</div>
