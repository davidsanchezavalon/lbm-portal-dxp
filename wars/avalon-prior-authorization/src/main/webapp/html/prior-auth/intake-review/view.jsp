<%--
/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
 
 /**
  * Description
  *		This file contain the Intake Review portlet for Prior Auth.
  *
  * CHANGE History
  * 	Version 1.0
  *			All of the changes have been merged.
  * 		This file includes all of the field validations, the busy icon, and the check for leaving the page without saving changes.
  * 	Version 1.1  
  *			Fixed ticket LJR-384.
  * 	Version 1.2  
  *			Fixed ticket LJR-383.
  *		Version 1.3
  *			Disable tabbing for VOID.
  *		Version 1.4
  *			Trim spaces from the notes field.
  *		Version 1.5
  *			Disable tabbing if the user is in the HP Employee group.
  *		Version 1.6
  *			Allow the scrolling of the notes in read only mode.
  *		Version 1.7
  *			Remove readonly for all but HP Employee group.
  *     Version 1.8
  *         Added functionality to sort the previous notes.
  *     Version 1.9
  *         Changed initial notes sort to descending.
  *		Version 1.10
  *			Switch all HTML DOM to jQuery.
  *			Fixed busy spinner for IE.
  *		Version 1.11					11/08/2017
  *			Added the AvalonEmployeeROPA group to read-only for PA.
  *		Version 1.12					01/12/2018
  *			Disable Save button until all required fields are valid.
  *			When the Save button is disabled show a mouse over tooltip with what is missing.
  *		Version 1.13					06/18/2018
  *			Align the AUI fields.
  * 	Version 1.14					08/15/2018
  * 			Added codify changes.
  *		Version 1.15					09/20/2018
  *			Fixed the read-only fields.
  *
  */
--%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@ include file="/html/prior-auth/init.jsp"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ include file="/html/prior-auth/auth-details.jsp"%>

<%@page import="com.liferay.portal.kernel.service.UserGroupLocalServiceUtil"%>
<%@page import="com.liferay.portal.kernel.model.User"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="theme" %>

<portlet:defineObjects />
<theme:defineObjects />

<script>
	<%
		int HP_REPRESENTATIVE_LEN = 70;
		int COMPLETED_BY_LEN = 70;
		int NOTE_LEN = 4000;
	%>
</script>

<aui:script>
	var toolTip = "";
	var browserStr = getBrowser();

	function getNamespace() {
		return('<portlet:namespace/>');
	}

	$(function() { 
		
		// Highlight the Intake Review selection
		boldSelection();

		$('form').areYouSure( {'message':exitString} );
	
		//  Hide one of the sorted notes text areas
		$(".hide_text").hide();
		
		// Set the initial save button status
		setSaveButtonStatus();
		
		// Disable tabbing into Completed By field.  If it is blank add the logged in user id.
		$('#<portlet:namespace/>completedBy').prop('tabindex', '-1'); 
		if ($("#<portlet:namespace/>completedBy").val().trim().length == 0) {
			$("#<portlet:namespace/>completedBy").val("<%= user.getEmailAddress() %>");
		}
		$("#<portlet:namespace/>hiddencompletedBy").val($("#<portlet:namespace/>completedBy").val());

		// Adjust the field widths
		adjustMemberDataWidth();
		adjustEligibilityBenefitInfoWidth();
	});

	$(window).resize(function () {
		
		// Reset the fields widths
		adjustMemberDataWidth();
		adjustEligibilityBenefitInfoWidth();
	});

	/****For disabling tabbing when the Auth submission status' are related to Void****/
	YUI().ready('aui-node', 'event', function(Y) {
		/*
		   Set the page to read only if the user is in the HealthPlanEmployee or AvalonEmployeeROPA group and not in 
		   AvalonAdmin, AvalonEmployee, AvalonProvider, PortalApprover, or PortalCreator groups.
		*/
		<% 
			boolean readOnlyFlag = false;
			boolean hpEmployeeFlag = false;
			boolean avalonEmployeeRoPaFlag = false;
			boolean notReadOnlyGroupFlag = false;
			User userU = themeDisplay.getUser();
			long[] groups = userU.getUserGroupIds();
			int length = groups.length;
			for (int i = 0; i < length; ++i) {
				String nextGroup = UserGroupLocalServiceUtil.getUserGroup(groups[i]).getName();
				if (nextGroup.equalsIgnoreCase("HealthPlanEmployee")) {
					hpEmployeeFlag = true;
				} else if (nextGroup.equalsIgnoreCase("AvalonEmployeeROPA")) {
					avalonEmployeeRoPaFlag = true;
				} else if (nextGroup.equalsIgnoreCase("AvalonAdmin") || nextGroup.equalsIgnoreCase("AvalonEmployee") || 
						   nextGroup.equalsIgnoreCase("AvalonProvider") || nextGroup.equalsIgnoreCase("PortalApprover") || 
						   nextGroup.equalsIgnoreCase("PortalCreator")) {
					notReadOnlyGroupFlag = true;
				}
			}
			if (!notReadOnlyGroupFlag && (hpEmployeeFlag || avalonEmployeeRoPaFlag)) {
				readOnlyFlag = true;
			}
		%>
		if (<%= readOnlyFlag %>) {
	
			// Disable the tabbing and make read only
			disableTabbing();
			makeReadonly();
		} else if(Y.one('#authSubmitStatusdisableId')){
			var authSubmitStatusdisable = Y.one('#authSubmitStatusdisableId').val();
			
			if(authSubmitStatusdisable!=null && authSubmitStatusdisable!="" && authSubmitStatusdisable!='undefined'){
				if(authSubmitStatusdisable=="Sent" || authSubmitStatusdisable=="Void HP - Submitted" || authSubmitStatusdisable=="Void-Submitted" || authSubmitStatusdisable=="Void"||authSubmitStatusdisable=="Void HP"){
					if(Y.one('.avalon_portlet')){
						// Opening all fields
					}
				}
			}
		}
	});

	function isValidForm() {
		var formValid = false;
		var eligibilityBenefitCheck = $("#<portlet:namespace/>eligibilityBenefitCheck").prop("selectedIndex");
		var hpRepresentative = $("#<portlet:namespace/>hpRepresentative").val().trim();
		var providerCheck = $("#<portlet:namespace/>providerCheck").prop("selectedIndex");
		var intakeNotesCreate = $("#<portlet:namespace/>intakeNotesCreate").val().trim();
		
		// Initialize the tool tip for the save button
		toolTip = "";
	
		var noteTest = false;
		if ((providerCheck == "4") || (providerCheck == "5") || (providerCheck == "6")) {
			noteTest = true;
		}
		
		// Validate the form
		if ((eligibilityBenefitCheck > 0) && (hpRepresentative.length > 0) && (providerCheck > 0) && isValid(intakeNotesCreate, <%= NOTE_LEN %>, noteTest)) {
			formValid = true;
			toolTip = "";
		} else {

			// Build the toolTip for the Save button
			// Test the Eligibility/Benefit Check
			if (eligibilityBenefitCheck == 0) {
				toolTip += addToToolTip("The Eligibility/Benefit Check is not valid", toolTip.length);
			}

			// Test the HP Representative
			if (hpRepresentative.length == 0) {
				toolTip += addToToolTip("The HP Representative is not valid", toolTip.length);
			}

			// Test the Provider Check
			if (providerCheck == 0) {
				toolTip += addToToolTip("The Provider Check is not valid", toolTip.length);
			}

			// Validate the notes
			if (!isValid(intakeNotesCreate, <%= NOTE_LEN %>, noteTest)) {
				toolTip += addToToolTip("The Intake Note is not valid", toolTip.length);
			}
		}
		return formValid;
	}

	function makeReadonly() {

		// Make all fields readonly
		$('#<portlet:namespace/>intakeNotesCreate').addClass("optdisable");
		$("#<portlet:namespace/>eligibilityBenefitCheck").addClass("optdisable");
		$("#<portlet:namespace/>hpRepresentative").addClass("optdisable");
		$("#<portlet:namespace/>providerCheck").addClass("optdisable");

		// Make the buttons readonly
		$('#<portlet:namespace/>saveIntakeNotes').addClass("optdisable");
	}
	
	function disableTabbing() {

		// Disable tabbing to all fields
		$('#<portlet:namespace/>intakeNotesCreate').prop('tabindex', '-1'); 
		$('#<portlet:namespace/>eligibilityBenefitCheck').prop('tabindex', '-1'); 
		$('#<portlet:namespace/>hpRepresentative').prop('tabindex', '-1'); 
		$('#<portlet:namespace/>providerCheck').prop('tabindex', '-1'); 

		// Disable tabbing to all buttons
		$('#<portlet:namespace/>saveIntakeNotes').prop('tabindex', '-1');
		
		// Disable all of the dropdowns for IE
		if ((browserStr == "IE") || (browserStr == "Firefox") || (browserStr == "unknown")) {
			$('#<portlet:namespace/>eligibilityBenefitCheck').attr('disabled', true);
			$('#<portlet:namespace/>providerCheck').attr('disabled', true);
		}

	}

	function setSaveButtonStatus() {
		
		// Disable the save button until all the fields are valid
		if (isValidForm()) {
			$("#<portlet:namespace/>saveIntakeNotes").removeAttr("disabled");
		} else {
			$("#<portlet:namespace/>saveIntakeNotes").attr("disabled", "disabled");
		}
		
		// Add the mouse over text for the save button
		$("#<portlet:namespace/>saveIntakeNotes").prop("title", toolTip);
	}
	
	function adjustEligibilityBenefitInfoWidth() {
		
		// Get the end of the field location
		var endLoc = $("#<portlet:namespace/>intakeNotesCreate").offset().left + parseInt($("#<portlet:namespace/>intakeNotesCreate").css("width"));
		
		// Get the current width of the fileds
		var fieldWidth = endLoc - $("#<portlet:namespace/>eligibilityBenefitCheck").offset().left;
		
		// Change the width of the fields so the end at the same location as the notes
		$("#<portlet:namespace/>eligibilityBenefitCheck").css("cssText", "width:" + fieldWidth + "px !important");
		$("#<portlet:namespace/>hpRepresentative").css("cssText", "width:" + fieldWidth + "px !important");
		$("#<portlet:namespace/>providerCheck").css("cssText", "width:" + fieldWidth + "px !important");
		$("#<portlet:namespace/>completedBy").css("cssText", "width:" + fieldWidth + "px !important");
	}
</aui:script>

<portlet:actionURL var="intakeReviewpageURL">
<portlet:param name="action" value="intakeReviewAction" />
</portlet:actionURL>
<%  
List readFdata=new ArrayList();
String fdata_desc = "";
String fdata_asc = "";

if((request.getAttribute("readData")!=null)) {
	readFdata=(List)request.getAttribute("readData");
	
		// Create the ascending list
		for(int i = 0;i < readFdata.size(); i++){
			fdata_asc = fdata_asc + readFdata.get(i) + System.getProperty("line.separator");
		}
		
		// Create the descending list
		for (int i = readFdata.size() - 1; i >= 0; i--) {
			fdata_desc = fdata_desc + readFdata.get(i) + System.getProperty("line.separator");
		}
	}
%>
<div id="busy_indicator" style="display: none">
	<img src="<%=request.getContextPath() %>/images/busy-spinner.gif" alt="Busy indicator">
</div>

<liferay-ui:panel title="pa.intake.eligibility.review" collapsible="false">
	<aui:form name="intakeReviewDetails" action="${intakeReviewpageURL}" method="post" commandname="intakeReviewFO" onChange="javascript:setSaveButtonStatus()">

		 <aui:fieldset label="pa.intake.review.eligibility.benefit.info">
			<aui:input type="hidden" name="hiddencompletedBy" id="hiddencompletedBy" label="" value="" />
		 	<aui:container>
				<aui:row>
					<aui:col span="12">
						<aui:select inlineLabel="true" name="eligibilityBenefitCheck" label="pa.intake.review.eligibility.benefit.check" cssClass="span12 checkForDisable" required="true" showRequiredLabel="true" >
							<aui:option value="" selected="<%= request.getAttribute("priorAuthEligibilityBenefitCheck").equals("") %>">Make a selection</aui:option>
							<aui:option value="E1" selected="<%= request.getAttribute("priorAuthEligibilityBenefitCheck").equals("E1") %>">E1 - Member not eligible</aui:option>
							<aui:option value="E2" selected="<%= request.getAttribute("priorAuthEligibilityBenefitCheck").equals("E2") %>">E2 - Member eligible and codes validated as Benefit or Non-Benefit via HP Online system</aui:option>
							<aui:option value="E3" selected="<%= request.getAttribute("priorAuthEligibilityBenefitCheck").equals("E3") %>">E3 - Member eligible, Plan called to verify benefits</aui:option>
						</aui:select>
					</aui:col>
				</aui:row>
				
				<aui:row>
					<aui:col span="12">
						<aui:input type="text" inlineLabel="true" name="hpRepresentative" label="pa.intake.review.hp.representative" value="<%= request.getAttribute("priorAuthHpRepresentative") %>" maxlength="<%= HP_REPRESENTATIVE_LEN %>" >
							<aui:validator name="required" />
						</aui:input>
					</aui:col>
				</aui:row>
					
				<aui:row>
					<aui:col span="12">
						<aui:select inlineLabel="true" name="providerCheck" label="pa.intake.review.provider.check" cssClass="span12 checkForDisable" required="true" showRequiredLabel="true" >
							<aui:option value="" selected="<%= request.getAttribute("priorAuthProviderCheck").equals("") %>">Make a selection</aui:option>
							<aui:option value="P1" selected="<%= request.getAttribute("priorAuthProviderCheck").equals("P1") %>">P1 - Ordering and Rendering Participating</aui:option>
							<aui:option value="P2" selected="<%= request.getAttribute("priorAuthProviderCheck").equals("P2") %>">P2 - Ordering Participating, Rendering no match on Verify Comply</aui:option>
							<aui:option value="P3" selected="<%= request.getAttribute("priorAuthProviderCheck").equals("P3") %>">P3 - Rendering Participating, Ordering no match on Verify Comply</aui:option>
							<aui:option value="P4" selected="<%= request.getAttribute("priorAuthProviderCheck").equals("P4") %>">P4 - Ordering no match on Verify Comply, Rendering Match - see Notes</aui:option>
							<aui:option value="P5" selected="<%= request.getAttribute("priorAuthProviderCheck").equals("P5") %>">P5 - Rendering no match on Verify Comply, Ordering Match - see Notes</aui:option>
							<aui:option value="P6" selected="<%= request.getAttribute("priorAuthProviderCheck").equals("P6") %>">P6 - Ordering and Rendering match on Verify Comply - see Notes</aui:option>
						</aui:select>
					</aui:col>
				</aui:row>
				
				<aui:row>
					<aui:col span="12">
						<aui:input type="text" inlineLabel="true" name="completedBy" label="pa.intake.review.completed.by" value="<%= request.getAttribute("priorAuthCompletedBy") %>" maxlength="<%= COMPLETED_BY_LEN %>" disabled="true" />
					</aui:col>
				</aui:row>
					
			</aui:container>
		</aui:fieldset>

		<aui:fieldset label="pa.intake.review.notes">
			<aui:container>
				<aui:row>
					<i class="icon-chevron-sign-up" title="Change Sort Order" onClick="javascript: changeSortOrder(this)" ></i>
					<aui:input type="textarea" cssClass="oldNotes note-control" name="intakeNotesRead" resizable="false" label="" readonly="true" rows="7" value='<%= fdata_desc %>' />
					<aui:input type="textarea" cssClass="hide_text oldNotes note-control" name="intakeNotesRead" resizable="false" label="" readonly="true" rows="7" value='<%= fdata_asc %>' />
					<aui:input type="textarea" cssClass="note-control" name="intakeNotesCreate" resizable="false" label="pa.intake.review.label.enter.notes" rows="3" value='' >
						<aui:validator name="maxLength"><%= NOTE_LEN %></aui:validator>
					</aui:input>
				</aui:row>
			</aui:container>
		</aui:fieldset>
		
		<aui:fieldset>
			<aui:button-row>
				<aui:button type="button" primary="true" name="saveIntakeNotes" cssClass="pull-right" value="pa.label.save" onclick="javascript:checkValidForm('intakeReviewDetails')" />
			</aui:button-row>
		</aui:fieldset>
	</aui:form>
</liferay-ui:panel>
