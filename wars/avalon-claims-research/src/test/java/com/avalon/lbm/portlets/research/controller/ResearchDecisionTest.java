package com.avalon.lbm.portlets.research.controller;

import com.avalon.lbm.portlets.claim.controller.ResearchDecision;
import com.avalon.lbm.portlets.claim.model.ResearchDecisionFO;
import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.ui.Model;

/**
 * @author sai.s.kumar.cirigiri
 *
 */

public class ResearchDecisionTest {
    
    @Test
    public void testResearchDecisionRender(){
	
	RenderRequest renderRequest = Mockito.mock(RenderRequest.class);
	RenderResponse renderResponse  = Mockito.mock(RenderResponse.class);
	Model model = Mockito.mock(Model.class);
	ResearchDecision researchDecision = new ResearchDecision();
	researchDecision.researchDecisionRender(renderRequest, renderResponse, model);
    }
    
    
    
    
    @Test
    public void testResearchClaimAction(){
	ResearchDecisionFO researchDecisionFO = new ResearchDecisionFO();
	researchDecisionFO.setClaimId("HP_CLM_04");
	ActionRequest actionRequest  = Mockito.mock(ActionRequest.class);
	ActionResponse actionResponse  = Mockito.mock(ActionResponse.class);
	ResearchDecision researchDecision = new ResearchDecision();
	researchDecision.researchClaimAction(actionRequest, actionResponse, researchDecisionFO);
	
    }
  
    
    @Test
    public void testResultsRenderMethod(){
	
	RenderRequest renderRequest  = Mockito.mock(RenderRequest.class);
	RenderResponse renderResponse  = Mockito.mock(RenderResponse.class);
	ResearchDecision researchDecision = new ResearchDecision();
	researchDecision.resultsRenderMethod(renderRequest, renderResponse);
	
    }
    
    

}
