//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.4-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2016.03.30 at 08:49:38 PM IST 
//


package com.avalon.lbm.xml.beans;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for CeLineType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CeLineType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="HealthPlanLineNumber" type="{http://www.w3.org/2001/XMLSchema}byte"/>
 *         &lt;element name="ProcedureCode" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="ProcedureDescription" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ProcedureModifier1Code" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ProcedureModifier2Code" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ProcedureModifier3Code" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ProcedureModifier4Code" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="DecisionTypeCode" type="{http://www.w3.org/2001/XMLSchema}byte"/>
 *         &lt;element name="DecisionTypeDescription" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ServiceFromDate" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *         &lt;element name="ServiceToDate" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *         &lt;element name="ServiceUnitsQuantity" type="{http://www.w3.org/2001/XMLSchema}byte"/>
 *         &lt;element name="HealthPlanGroupId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CeReason" type="{}CeReasonType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CeLineType", propOrder = {
    "healthPlanLineNumber",
    "procedureCode",
    "procedureDescription",
    "procedureModifier1Code",
    "procedureModifier2Code",
    "procedureModifier3Code",
    "procedureModifier4Code",
    "decisionTypeCode",
    "decisionTypeDescription",
    "serviceFromDate",
    "serviceToDate",
    "serviceUnitsQuantity",
    "healthPlanGroupId",
    "ceReason"
})
public class CeLineType {

    @XmlElement(name = "HealthPlanLineNumber")
    protected byte healthPlanLineNumber;
    @XmlElement(name = "ProcedureCode")
    protected String procedureCode;
    @XmlElement(name = "ProcedureDescription", required = true)
    protected String procedureDescription;
    @XmlElement(name = "ProcedureModifier1Code", required = true)
    protected String procedureModifier1Code;
    @XmlElement(name = "ProcedureModifier2Code", required = true)
    protected String procedureModifier2Code;
    @XmlElement(name = "ProcedureModifier3Code", required = true)
    protected String procedureModifier3Code;
    @XmlElement(name = "ProcedureModifier4Code", required = true)
    protected String procedureModifier4Code;
    @XmlElement(name = "DecisionTypeCode")
    protected byte decisionTypeCode;
    @XmlElement(name = "DecisionTypeDescription", required = true)
    protected String decisionTypeDescription;
    @XmlElement(name = "ServiceFromDate", required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar serviceFromDate;
    @XmlElement(name = "ServiceToDate", required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar serviceToDate;
    @XmlElement(name = "ServiceUnitsQuantity")
	protected int serviceUnitsQuantity;
    @XmlElement(name = "HealthPlanGroupId", required = true)
    protected String healthPlanGroupId;
    @XmlElement(name = "CeReason")
    protected List<CeReasonType> ceReason;

    /**
     * Gets the value of the healthPlanLineNumber property.
     * 
     */
    public byte getHealthPlanLineNumber() {
        return healthPlanLineNumber;
    }

    /**
     * Sets the value of the healthPlanLineNumber property.
     * 
     */
    public void setHealthPlanLineNumber(byte value) {
        this.healthPlanLineNumber = value;
    }

    /**
     * Gets the value of the procedureCode property.
     * 
     */
    public String getProcedureCode() {
        return procedureCode;
    }

    /**
     * Sets the value of the procedureCode property.
     * 
     */
    public void setProcedureCode(String value) {
        this.procedureCode = value;
    }

    /**
     * Gets the value of the procedureDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProcedureDescription() {
        return procedureDescription;
    }

    /**
     * Sets the value of the procedureDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProcedureDescription(String value) {
        this.procedureDescription = value;
    }

    /**
     * Gets the value of the procedureModifier1Code property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProcedureModifier1Code() {
        return procedureModifier1Code;
    }

    /**
     * Sets the value of the procedureModifier1Code property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProcedureModifier1Code(String value) {
        this.procedureModifier1Code = value;
    }

    /**
     * Gets the value of the procedureModifier2Code property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProcedureModifier2Code() {
        return procedureModifier2Code;
    }

    /**
     * Sets the value of the procedureModifier2Code property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProcedureModifier2Code(String value) {
        this.procedureModifier2Code = value;
    }

    /**
     * Gets the value of the procedureModifier3Code property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProcedureModifier3Code() {
        return procedureModifier3Code;
    }

    /**
     * Sets the value of the procedureModifier3Code property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProcedureModifier3Code(String value) {
        this.procedureModifier3Code = value;
    }

    /**
     * Gets the value of the procedureModifier4Code property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProcedureModifier4Code() {
        return procedureModifier4Code;
    }

    /**
     * Sets the value of the procedureModifier4Code property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProcedureModifier4Code(String value) {
        this.procedureModifier4Code = value;
    }

    /**
     * Gets the value of the decisionTypeCode property.
     * 
     */
    public byte getDecisionTypeCode() {
        return decisionTypeCode;
    }

    /**
     * Sets the value of the decisionTypeCode property.
     * 
     */
    public void setDecisionTypeCode(byte value) {
        this.decisionTypeCode = value;
    }

    /**
     * Gets the value of the decisionTypeDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDecisionTypeDescription() {
        return decisionTypeDescription;
    }

    /**
     * Sets the value of the decisionTypeDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDecisionTypeDescription(String value) {
        this.decisionTypeDescription = value;
    }

    /**
     * Gets the value of the serviceFromDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getServiceFromDate() {
        return serviceFromDate;
    }

    /**
     * Sets the value of the serviceFromDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setServiceFromDate(XMLGregorianCalendar value) {
        this.serviceFromDate = value;
    }

    /**
     * Gets the value of the serviceToDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getServiceToDate() {
        return serviceToDate;
    }

    /**
     * Sets the value of the serviceToDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setServiceToDate(XMLGregorianCalendar value) {
        this.serviceToDate = value;
    }

    /**
     * Gets the value of the serviceUnitsQuantity property.
     * 
     */
	public int getServiceUnitsQuantity() {
        return serviceUnitsQuantity;
    }

    /**
     * Sets the value of the serviceUnitsQuantity property.
     * 
     */
	public void setServiceUnitsQuantity(int value) {
        this.serviceUnitsQuantity = value;
    }

    /**
     * Gets the value of the healthPlanGroupId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHealthPlanGroupId() {
        return healthPlanGroupId;
    }

    /**
     * Sets the value of the healthPlanGroupId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHealthPlanGroupId(String value) {
        this.healthPlanGroupId = value;
    }

    /**
     * Gets the value of the ceReason property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the ceReason property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCeReason().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CeReasonType }
     * 
     * 
     */
    public List<CeReasonType> getCeReason() {
        if (ceReason == null) {
            ceReason = new ArrayList<CeReasonType>();
        }
        return this.ceReason;
    }

}
