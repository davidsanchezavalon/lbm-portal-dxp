package com.avalon.lbm.portlets.claim.model;

import java.io.Serializable;
import java.util.Map;

public class DecisionCodes implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -6082665147264427508L;

    private static DecisionCodes decisionCodes = null;

    private DecisionCodes() {

    }

    static {

	if (decisionCodes == null) {
	    decisionCodes = new DecisionCodes();
	}

    }

    /**
     * @return the secondaryCode
     */
    public Map<String, String> getSecondaryCode() {
	return secondaryCode;
    }

    /**
     * @param secondaryCode
     *            the secondaryCode to set
     */
    public void setSecondaryCode(Map<String, String> secondaryCode) {
	this.secondaryCode = secondaryCode;
    }

    private Map<String, String> secondaryCode;

    public static DecisionCodes getDecisionCodes() {

	return decisionCodes;

    }

}
