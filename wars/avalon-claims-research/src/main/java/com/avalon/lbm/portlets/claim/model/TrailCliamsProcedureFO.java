package com.avalon.lbm.portlets.claim.model;

import java.io.Serializable;

public class TrailCliamsProcedureFO  implements Serializable {
    
    private String fromDateOfService;
    private String procedureCode;
    private String procedureDescription;
    private String procModOne;
    private String procModTwo;
    private String procModThree;
    private String procModFour;
    private String revCode;
    private String placeOfService;
    private String relatedDiagnosisCode;
    private String primaryDiagnosisCode;
    private String units;
    
    /**
     * 
     */
    private static final long serialVersionUID = -1883935516936397780L;
    /**
     * @return the fromDateOfService
     */
    public String getFromDateOfService() {
        return fromDateOfService;
    }
    /**
     * @param fromDateOfService the fromDateOfService to set
     */
    public void setFromDateOfService(String fromDateOfService) {
        this.fromDateOfService = fromDateOfService;
    }

    /**
     * @return the procedureCode
     */
    public String getProcedureCode() {
        return procedureCode;
    }
    /**
     * @param procedureCode the procedureCode to set
     */
    public void setProcedureCode(String procedureCode) {
        this.procedureCode = procedureCode;
    }

    /**
     * @return the procedureDescription
     */
    public String getProcedureDescription() {
        return procedureDescription;
    }
    /**
     * @param procedureDescription the procedureDescription to set
     */
    public void setProcedureDescription(String procedureDescription) {
        this.procedureDescription = procedureDescription;
    }

    /**
     * @return the procModOne
     */
    public String getProcModOne() {
        return procModOne;
    }
    /**
     * @param procModOne the procModOne to set
     */
    public void setProcModOne(String procModOne) {
        this.procModOne = procModOne;
    }
 
    /**
     * @return the procModTwo
     */
    public String getProcModTwo() {
        return procModTwo;
    }
    /**
     * @param procModTwo the procModTwo to set
     */
    public void setProcModTwo(String procModTwo) {
        this.procModTwo = procModTwo;
    }
 
    /**
     * @return the procModThree
     */
    public String getProcModThree() {
        return procModThree;
    }
    /**
     * @param procModThree the procModThree to set
     */
    public void setProcModThree(String procModThree) {
        this.procModThree = procModThree;
    }

    /**
     * @return the procModFour
     */
    public String getProcModFour() {
        return procModFour;
    }
    /**
     * @param procModFour the procModFour to set
     */
    public void setProcModFour(String procModFour) {
        this.procModFour = procModFour;
    }
 
    /**
     * @return the revCode
     */
    public String getRevCode() {
        return revCode;
    }
    /**
     * @param revCode the revCode to set
     */
    public void setRevCode(String revCode) {
        this.revCode = revCode;
    }
 
    /**
     * @return the placeOfService
     */
    public String getPlaceOfService() {
        return placeOfService;
    }
    /**
     * @param placeOfService the placeOfService to set
     */
    public void setPlaceOfService(String placeOfService) {
        this.placeOfService = placeOfService;
    }
 
    /**
     * @return the primaryDiagnosisCode
     */
    public String getPrimaryDiagnosisCode() {
        return primaryDiagnosisCode;
    }
    /**
     * @param primaryDiagnosisCode the primaryDiagnosisCode to set
     */
    public void setPrimaryDiagnosisCode(String primaryDiagnosisCode) {
        this.primaryDiagnosisCode = primaryDiagnosisCode;
    }
 
    /**
     * @return the relatedDiagnosisCode
     */
    public String getRelatedDiagnosisCode() {
        return relatedDiagnosisCode;
    }
    /**
     * @param relatedDiagnosisCode the relatedDiagnosisCode to set
     */
    public void setRelatedDiagnosisCode(String relatedDiagnosisCode) {
        this.relatedDiagnosisCode = relatedDiagnosisCode;
    }
 
    /**
     * @return the units
     */
    public String getUnits() {
        return units;
    }
    /**
     * @param units the units to set
     */
    public void setUnits(String units) {
        this.units = units;
    }
}
