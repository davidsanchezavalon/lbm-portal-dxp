package com.avalon.lbm.portlets.claim.model;

import java.io.Serializable;

public class DiagnosisDescription implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -1747506807264281273L;
    
    
    
    /**
     * @return the diagnosisCode
     */
    public String getDiagnosisCode() {
        return diagnosisCode;
    }
    /**
     * @param diagnosisCode the diagnosisCode to set
     */
    public void setDiagnosisCode(String diagnosisCode) {
        this.diagnosisCode = diagnosisCode;
    }
    /**
     * @return the diagnosisDescription
     */
    public String getDiagnosisDescription() {
        return diagnosisDescription;
    }
    /**
     * @param diagnosisDescription the diagnosisDescription to set
     */
    public void setDiagnosisDescription(String diagnosisDescription) {
        this.diagnosisDescription = diagnosisDescription;
    }
    private String diagnosisCode;
    private String diagnosisDescription;
    
    
    
    
    

}
