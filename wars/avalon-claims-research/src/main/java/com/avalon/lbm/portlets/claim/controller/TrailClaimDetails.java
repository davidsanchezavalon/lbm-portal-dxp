/**
 * Description
 *		This file contain the controller methods for the Trial Claim Page.
 *
 * CHANGE History
 * 		Version 1.0
 * 			Initial version
 *  	Version 1.1			05/29/2018
 *  		Switch getting the descriptions from JAX-WS RI to Apache Axis2.
 *  
 */

package com.avalon.lbm.portlets.claim.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.PortletMode;
import javax.portlet.PortletModeException;
import javax.portlet.PortletRequest;
import javax.portlet.PortletSession;
import javax.portlet.PortletURL;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.portlet.WindowState;
import javax.portlet.WindowStateException;
import javax.servlet.http.HttpServletRequest;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;
import javax.xml.soap.SOAPFault;
import javax.xml.ws.soap.SOAPFaultException;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.ModelAndView;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;

import com.avalon.lbm.portlets.claim.exception.LBMCustomException;
import com.avalon.lbm.portlets.claim.model.DecisionCodes;
import com.avalon.lbm.portlets.claim.model.TrailClaimDetailsFO;
import com.avalon.lbm.portlets.claim.model.TrailCliamsProcedureFO;
import com.avalon.lbm.portlets.claim.servicehandler.TrailClaimUtils;
import com.avalon.lbm.portlets.claim.servicehandler.TrailDatesUtils;
import com.avalon.lbm.services.DAOExceptionException;
import com.avalon.lbm.services.PriorAuthTrialClaimsServiceStub;
import com.avalon.lbm.services.PriorAuthTrialClaimsServiceStub.GetEnumShrtDescription;
import com.avalon.lbm.services.PriorAuthTrialClaimsServiceStub.GetEnumShrtDescriptionE;
import com.avalon.lbm.services.PriorAuthTrialClaimsServiceStub.GetEnumShrtDescriptionResponse;
import com.avalon.lbm.services.PriorAuthTrialClaimsServiceStub.GetEnumShrtDescriptionResponseE;
import com.avalon.lbm.services.PriorAuthTrialClaimsServiceStub.GetIcdDiagnosisCode;
import com.avalon.lbm.services.PriorAuthTrialClaimsServiceStub.GetIcdDiagnosisCodeE;
import com.avalon.lbm.services.PriorAuthTrialClaimsServiceStub.GetIcdDiagnosisCodeResponse;
import com.avalon.lbm.services.PriorAuthTrialClaimsServiceStub.GetIcdDiagnosisCodeResponseE;
import com.avalon.lbm.services.PriorAuthTrialClaimsServiceStub.TrialClaimsDiagnosisReqHdr;
import com.avalon.lbm.services.PriorAuthTrialClaimsServiceStub.TrialClaimsProcedureReqHdr;
import com.avalon.member.search.beans.MemberDetailsFo;
import com.avalon.trails.research.constants.TrailsResearchConstant;
import com.benemgmt.wr01.claimeditor.EvaluateLabClaimServiceStub;
import com.benemgmt.wr01.claimeditor.EvaluateLabClaimServiceStub.AdviceDecisionType_type1;
import com.benemgmt.wr01.claimeditor.EvaluateLabClaimServiceStub.BlueCardCode_type1;
import com.benemgmt.wr01.claimeditor.EvaluateLabClaimServiceStub.CobIndicator_type1;
import com.benemgmt.wr01.claimeditor.EvaluateLabClaimServiceStub.EvaluateLabClaim;
import com.benemgmt.wr01.claimeditor.EvaluateLabClaimServiceStub.EvaluateLabClaimResponse;
import com.benemgmt.wr01.claimeditor.EvaluateLabClaimServiceStub.ExcludeMembershipIndicator_type1;
import com.benemgmt.wr01.claimeditor.EvaluateLabClaimServiceStub.HealthPlanIdType_type1;
import com.benemgmt.wr01.claimeditor.EvaluateLabClaimServiceStub.IcdDiagVersQualifier_type1;
import com.benemgmt.wr01.claimeditor.EvaluateLabClaimServiceStub.InNetworkIndicator_type1;
import com.benemgmt.wr01.claimeditor.EvaluateLabClaimServiceStub.LineOfBusiness_type1;
import com.benemgmt.wr01.claimeditor.EvaluateLabClaimServiceStub.OtherClaimEditorIndicator_type1;
import com.benemgmt.wr01.claimeditor.EvaluateLabClaimServiceStub.PaidDeniedIndicator_type1;
import com.benemgmt.wr01.claimeditor.EvaluateLabClaimServiceStub.PayAndEducate_type1;
import com.benemgmt.wr01.claimeditor.EvaluateLabClaimServiceStub.RequestHeaderData;
import com.benemgmt.wr01.claimeditor.EvaluateLabClaimServiceStub.RequestLineData;
import com.benemgmt.wr01.claimeditor.EvaluateLabClaimServiceStub.ResponseHeaderData;
import com.benemgmt.wr01.claimeditor.EvaluateLabClaimServiceStub.ResponseLineData;
import com.benemgmt.wr01.claimeditor.EvaluateLabClaimServiceStub.RevCode_type1;
import com.benemgmt.wr01.claimeditor.EvaluateLabClaimServiceStub.SecondaryCodes;
import com.benemgmt.wr01.claimeditor.EvaluateLabClaimServiceStub.TrialClaimIndicator_type1;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Layout;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.model.UserGroup;
import com.liferay.portal.kernel.portlet.PortletURLFactoryUtil;
import com.liferay.portal.kernel.service.LayoutLocalServiceUtil;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.WebKeys;

/**
 * Description
 *	This file contain the render, resource, and action methods used by the Trial Claim Entry portlet.
 *
 * @author David Sanchez
 *
 *	Version 1.0
 *		Initial version
 *	Version 1.1
 *		Set the default group ID for a non-member search.  A member search will use the MLB group ID
 *		  that is set by the claim editor.
 *  Version 1.2
 *		Added the REV code to the Claim Lines.
 *  Version 1.3
 *		Added Unknown to gender.
 *
 */

@Controller(value = "TrailClaimDetails")
@RequestMapping(value = { "VIEW" })
public class TrailClaimDetails {
   
	private static final Log log = LogFactoryUtil.getLog(TrailClaimDetails.class.getName());
	

    /**
     * Render method which called during initial load of research a decision view
     */
    @RenderMapping
    public String trailClaimRenderRequest(RenderRequest request,
	                                      RenderResponse response, Model model,
	                                      @ModelAttribute(value = "trailClaimDetailsFO") TrailClaimDetailsFO trailClaimDetailsFO) {
    	
    	String bogusIdCardNumber = "9999999999";
		log.info("Processing trailClaimRenderRequest for Trial Claim Entry");

		// Get the attributes returned from member search
		PortletSession portletSession = request.getPortletSession();

		// Add a flag for the disclaimer message
		String disclaimerShownFlag = (String)portletSession.getAttribute("disclaimerShownFlag", PortletSession.APPLICATION_SCOPE);
		String idCardNumber = (String)portletSession.getAttribute("idCardNumber", PortletSession.APPLICATION_SCOPE);
		String firstName = (String)portletSession.getAttribute("firstName", PortletSession.APPLICATION_SCOPE);
		String lastName = (String)portletSession.getAttribute("lastName", PortletSession.APPLICATION_SCOPE);
		String dateOfBirth = (String)portletSession.getAttribute("dateOfBirth", PortletSession.APPLICATION_SCOPE);
		String gender = (String)portletSession.getAttribute("gender", PortletSession.APPLICATION_SCOPE);
		String memberSearchStatus = (String)portletSession.getAttribute("memberSearchStatus", PortletSession.APPLICATION_SCOPE);
		log.info("member search data ---> " + idCardNumber + ":" + firstName + ":" + lastName + ":" + dateOfBirth + ":" + gender + ":" + memberSearchStatus + ".");

	    // Change the gender to male or female
		if (gender != null) {
			if (gender.toUpperCase().startsWith("M")) {
				gender = "Male";
			}
			if (gender.toUpperCase().startsWith("F")) {
				gender = "Female";
			}
			if (gender.toUpperCase().startsWith("U")) {
				gender = "Unknown";
			}
		}
		
	    // Save the data
		if (trailClaimDetailsFO.getMemberId() == null) {
			trailClaimDetailsFO.setMemberId(idCardNumber);
		}
		if (trailClaimDetailsFO.getMemberFirstName() == null) {
			trailClaimDetailsFO.setMemberFirstName(firstName);
		}
		if (trailClaimDetailsFO.getMemberLastName() == null) {
			trailClaimDetailsFO.setMemberLastName(lastName);
		}
		if (trailClaimDetailsFO.getMemberDOB() == null) {
			trailClaimDetailsFO.setMemberDOB(dateOfBirth);
		}
		if (trailClaimDetailsFO.getMemberGender() == null) {
			trailClaimDetailsFO.setMemberGender(gender);
		}
	    
	    // The initial flag should only be true the first time the page is displayed.
		trailClaimDetailsFO.setInitialStatus("false");
		if (memberSearchStatus == null) {
			if (trailClaimDetailsFO.getMemberAge() == null) {
				if (disclaimerShownFlag == null) {
					trailClaimDetailsFO.setInitialStatus("true");

					// Set this so the disclaimer is only shown once
					portletSession.setAttribute("disclaimerShownFlag", "true", PortletSession.APPLICATION_SCOPE);
					portletSession.setAttribute("idCardNumber", "bogusID", PortletSession.APPLICATION_SCOPE);
				} else {
					
					// This will show the disclaimer only once in the same liferay session
					trailClaimDetailsFO.setInitialStatus("false");
				}
			}
			if (trailClaimDetailsFO.getMemberSearchStatus() == null) {
				trailClaimDetailsFO.setMemberSearchStatus("false");
			}
		} else {
			trailClaimDetailsFO.setMemberSearchStatus(memberSearchStatus);
		}	

		// Set the member search status if it was not previously set
		if ((idCardNumber != null) && (trailClaimDetailsFO.getMemberSearchStatus() == "false")) {
			if (!idCardNumber.equals(bogusIdCardNumber) && !idCardNumber.equals("bogusID")) {
				if (idCardNumber.equals("cancelID")) {
					trailClaimDetailsFO.setInitialStatus("true");
				} else {
					trailClaimDetailsFO.setMemberSearchStatus("true");
				}
			}
		}

		if ((trailClaimDetailsFO.getInitialStatus() == "false") && (memberSearchStatus == null)) {
			if (trailClaimDetailsFO.getMemberAge().equals("978") && !idCardNumber.equals(bogusIdCardNumber)) {

				// set the search status for the member search
				trailClaimDetailsFO.setMemberSearchStatus("true");
			} else {

				// set the search status for the non-member search
				trailClaimDetailsFO.setMemberSearchStatus("false");
			}
		}

		log.info("Processing trailClaimRenderRequest for Trial Claim Exit");
		return "view";
    }
    
    /**
     * When user click on trail claim search button trailClaimDetails method will be execute
     * This method invokes the trail service and the same input data will be displayed in the results page.
     */

    @SuppressWarnings("null")
    @ActionMapping(params = { "action=trailClaimDetailsAction" })
    public void trailClaimDetails(ActionRequest request,
	                              ActionResponse response,
	                              @ModelAttribute(value = "trailClaimDetailsFO") TrailClaimDetailsFO trailClaimDetailsFO) {
	
		log.info("Processing trailClaimDetails for Trial Claim Entry");
		try {

			List<TrailDatesUtils> trailDatesUtils = new ArrayList<TrailDatesUtils>();
			Map<String, String> requestUnits = new ConcurrentHashMap<String, String>();
			Map<String, String> diagnosis = new LinkedHashMap<String, String>();
			Map<String, TrailCliamsProcedureFO> procedureMap = new LinkedHashMap<String, TrailCliamsProcedureFO>();
		
			String diagnosisCodes = null;
			String diagnosisDescription = null;
		
			log.info("trailClaimDetailsFO Object:" + trailClaimDetailsFO.toString());
			String primaryDecisionCode = null;
			String secondaryCode = null;
			XMLGregorianCalendar headerFromDt = null;
			XMLGregorianCalendar headerToDt = null;
			XMLGregorianCalendar claimPlaceServiceDate = null;
			String description = null;
			String panelTitle = null;
			DecisionCodes decisionCodes = null;
		
			int NoOfClaimRows = ParamUtil.getInteger(request, "claimLineMaxNumber");
	
			List<String> diagnosisCodeList = new ArrayList<String>();
		
			// set below for loop to diagnosiscodes
			for (int i = 1; i <= 25; i++) {
			    diagnosisCodes = ParamUtil.getString(request, "diaCode" + i).toUpperCase();
			    diagnosisDescription = ParamUtil.getString(request, "diaDesc" + i);
			    if ((diagnosisCodes != null) && !(diagnosisCodes.equals("")) && !(diagnosisCodes.isEmpty())) {
					log.info("diagnosis Code " + i + " ---> " + diagnosisCodes);
		
					//diagnosis.put(diagnosisCodes, diagnosisDescription);
					
					if (diagnosisCodes.contains(".")) {
						String diagnosisDisplay = diagnosisCodes.replaceAll("\\.","");
						diagnosisCodeList.add(diagnosisDisplay);
						diagnosis.put(diagnosisDisplay, diagnosisDescription);
					} else {
						diagnosisCodeList.add(diagnosisCodes);
						diagnosis.put(diagnosisCodes, diagnosisDescription);
					}
			    }
			}
		
			log.info("Claim Line Max Number ---> " + trailClaimDetailsFO.getClaimLineMaxNumber());

			RequestHeaderData requestHeader = new RequestHeaderData();
			List<RequestLineData> requestLine = new ArrayList<RequestLineData>();
			
			ResponseHeaderData responseHeader = null;
			ResponseLineData[] responseLine = null;

			XMLGregorianCalendar dateOfBirth = null;
			String memInfoExclude = ParamUtil.getString(request, "memberInfoExclude");
			ExcludeMembershipIndicator_type1 memInfoExcluInCode = ExcludeMembershipIndicator_type1.N;
			if (memInfoExclude.equalsIgnoreCase("true")) {
			    memInfoExcluInCode = ExcludeMembershipIndicator_type1.Y;
			}
			log.info("member Info Exclude ---> " + memInfoExclude);
			requestHeader.setExcludeMembershipIndicator(memInfoExcluInCode);
		
			String gender = trailClaimDetailsFO.getMemberGender();
			String groupId = "";
			try {
				if (memInfoExcluInCode.equals(ExcludeMembershipIndicator_type1.Y)) {
					String age = trailClaimDetailsFO.getMemberAge();
					log.info("Member Age ---> " + age);
					// Convert the age to DOB
					dateOfBirth = DatatypeFactory.newInstance().newXMLGregorianCalendar(TrailClaimUtils.getTrailClaimUtils().getPatientDob(age));
					gender = trailClaimDetailsFO.getMemberTestGender();
					trailClaimDetailsFO.setMemberSearchStatus("false");
					
					// Set the default group ID for a non-member search 
					groupId = "99ZZZZZZZ";
				} else {
					String dob = ParamUtil.getString(request, "memberDOB");
					if (dob != null) {
						String formattedDate = null;
						// Convert date of birth from MM/dd/yyyy to yyyy-MM-dd
						try {
							DateFormat originalFormat = new SimpleDateFormat("MM/dd/yyyy");
							DateFormat targetFormat = new SimpleDateFormat("yyyy-MM-dd");
							Date date = originalFormat.parse(dob);
							formattedDate = targetFormat.format(date);
						} catch (ParseException e) {
							
							// Default to the received format for an error
							formattedDate = dob;
							log.error("ParseException in trailClaimDetails method",e);
						}
						dob = formattedDate;
					}
		
					dateOfBirth = DatatypeFactory.newInstance().newXMLGregorianCalendar(dob);
					trailClaimDetailsFO.setMemberSearchStatus("true");
				}
			} catch (DatatypeConfigurationException e) {
				log.error("DatatypeConfigurationException in trailClaimDetails method",e);
			}
			// Change the gender to M, F, or U
			if (gender != null) {
				gender = gender.substring(0,1);
			}
		
			log.info("Member Gender ---> " + gender);
			log.info("Date Of Birth ---> " + dateOfBirth);
			
			requestHeader.setPatientGenderCode(gender);
			requestHeader.setPatientDateOfBirth(XMLGregorianCalendar(dateOfBirth));

			String idCardNumber = ParamUtil.getString(request, "memberIdCardNumber");
			if ((idCardNumber == null) || ((idCardNumber != null) && (idCardNumber.length() == 0))) {
				idCardNumber = "9999999999";   // hardcoded if not using member search
					
				// Set id card number to prevent disclaimer message
				PortletSession portletSession = request.getPortletSession();
				portletSession.setAttribute("idCardNumber", idCardNumber,PortletSession.APPLICATION_SCOPE);
				trailClaimDetailsFO.setMemberId(idCardNumber);
			}
			log.info("member Id Card Number ---> " + idCardNumber);
			requestHeader.setIdCardNumber(idCardNumber);
			String healthPlanId = ParamUtil.getString(request, "memberInfoHealthPlan");
			if ((healthPlanId == null) || ((healthPlanId != null) && (healthPlanId.length() == 0))) {
				healthPlanId = "0010";
			}
			log.info("memberInfoHealthPlan " + healthPlanId);
			requestHeader.setHealthPlanId(healthPlanId);
		
			String firstName = ParamUtil.getString(request, "memberFirstName");
			if ((firstName == null) || ((firstName != null) && (firstName.length() == 0))) {
				firstName = "tcFirstName";	// Required field
			}
			String lastName = ParamUtil.getString(request, "memberLastName");
			if ((lastName == null) || ((lastName != null) && (lastName.length() == 0))) {
				lastName = "tcLastName";	// Required field
			}

			log.info("member First Name ---> " + firstName);
			log.info("member Last Name ---> " + lastName);
			requestHeader.setPatientFirstName(firstName);
			requestHeader.setPatientLastName(lastName);
			requestHeader.setHealthPlanGroupId(groupId);	// hardcoded
			log.info("Health Plan Group Id ---> " + groupId);
	
			String billingTIN = trailClaimDetailsFO.getProviderInfoBillingTIN();
			if ((billingTIN == null) || ((billingTIN != null) && (billingTIN.length() == 0))) {
				billingTIN = "888888888"; // Required field
			}
			log.info("Billing Provider TIN ---> " + billingTIN);
			requestHeader.setBillingProviderTaxId(billingTIN);
	
			requestHeader.setPatientMiddleName("");			// hardcoded
			requestHeader.setPatientSuffixName("");			// hardcoded
			requestHeader.setBillingProviderNpi(trailClaimDetailsFO.getProviderInfoBillingNPI());
			requestHeader.setRenderingProviderNpi(trailClaimDetailsFO.getProviderInfoRenderingNPI());
			requestHeader.setOriginalClaimNumber(trailClaimDetailsFO.getAdditionalCriteriaOriginalNumber());
			Long claimNumberLong = (new java.util.Date()).getTime();
			String convertClaim = "TC" + String.valueOf(claimNumberLong);
			log.info("claim number ---> "  + convertClaim);
	
			requestHeader.setClaimNumber(convertClaim);
			requestHeader.setHealthPlanMemberId("");								// hardcoded
			requestHeader.setTrialClaimIndicator(TrialClaimIndicator_type1.Y);		// hardcoded
			requestHeader.setPrimaryDiagnosisCode(""); 		// the header primary diagnosis code is not used
			
			// Convert the diagnosisCodeList to java.lang.String[]
			String[] diagnosisCodeArr = new String[diagnosisCodeList.size()];
			diagnosisCodeArr = diagnosisCodeList.toArray(diagnosisCodeArr);
			requestHeader.setDiagnosisCodes(diagnosisCodeArr);
			
			requestHeader.setNumberOfLines(NoOfClaimRows);
			requestHeader.setBillType(""); 					// hardcoded
		
			String blueCardStatusCode = trailClaimDetailsFO.getMemberBlueCardStatus();
			if ((blueCardStatusCode == null) || ((blueCardStatusCode != null) && (blueCardStatusCode.length() == 0))) {
				blueCardStatusCode = "2";    				// hardcoded if not using member search
			}
			log.info("blue Card Status Code ---> " + blueCardStatusCode);
			if (blueCardStatusCode.equals("0")) {
				requestHeader.setBlueCardCode(BlueCardCode_type1.value1);
			} else if (blueCardStatusCode.equals("1")) {
				requestHeader.setBlueCardCode(BlueCardCode_type1.value2);
			} else {
				requestHeader.setBlueCardCode(BlueCardCode_type1.value3);
			}
			
			String secodaryClaimIndicator = ParamUtil.getString(request, "additionalCriteriaSecondaryClaim");
			CobIndicator_type1 secondaryClaimInCode = CobIndicator_type1.N;
			if (secodaryClaimIndicator.equalsIgnoreCase("true")) {
				secondaryClaimInCode = CobIndicator_type1.Y;
			}
			log.info("secondary Claim In Code ---> " + secondaryClaimInCode);
			requestHeader.setCobIndicator(secondaryClaimInCode);
			
			requestHeader.setLineOfBusiness(LineOfBusiness_type1.value1);				// hardcoded to 1
			requestHeader.setHealthPlanIdType(HealthPlanIdType_type1.value6);			// hardcoded to PI
			requestHeader.setIcdDiagVersQualifier(IcdDiagVersQualifier_type1.value2);	// hardcoded to 10
			
			log.info("Number of Lines ---> " + NoOfClaimRows);
			for (int ii = 1; ii <= NoOfClaimRows; ii++) {
				TrailCliamsProcedureFO trailCliamsProcedureFO = new TrailCliamsProcedureFO();
		
			    RequestLineData requestLineData = new RequestLineData();
		
				String primaryDiagnosis = ParamUtil.getString(request, "primaryDiagnosis" + ii);
		
			    String[] relDiagnosisTemp = request.getParameterValues("relDiagnosis" + ii);
				
				// Put the primary diagnosis first
				String[] relDiagnosis = null;
				if (relDiagnosisTemp == null) {
					relDiagnosis = new String [1];
					relDiagnosis[0] = primaryDiagnosis.replaceAll("\\.","");
					log.info("For line " + ii + " diagnosis code 1 ---> " + primaryDiagnosis.replaceAll("\\.",""));
				} else {
					relDiagnosis = new String [relDiagnosisTemp.length + 1];
					relDiagnosis[0] = primaryDiagnosis.replaceAll("\\.","");
					log.info("For line " + ii + " diagnosis code 1 ---> " + primaryDiagnosis.replaceAll("\\.",""));
	
					for(int i = 0; i < relDiagnosisTemp.length; i++){
						relDiagnosisTemp[i] = relDiagnosisTemp[i].replaceAll("\\.","");
						relDiagnosis[i + 1] = relDiagnosisTemp[i];
						log.info("For line " + ii + " diagnosis code " + (i + 2) + " ---> " + relDiagnosisTemp[i]);
					}
				}
		
			    String claimlineNumber = ParamUtil.getString(request, "procRowNumber" + ii);
			    String claimDtSer = ParamUtil.getString(request, "procedureDateOfService" + ii);
			    String claimprocedureCode = ParamUtil.getString(request, "code_procrow" + ii).toUpperCase();
			    String claimPlaceService = ParamUtil.getString(request, "placeOfService-row" + ii);
			    String claimUnits = ParamUtil.getString(request, "unit_procrow" + ii);
			    String procedureDescription = ParamUtil.getString(request, "codedesc_procrow" + ii);
		
				requestUnits.put(Integer.toString(ii), claimUnits);
				log.info("claim lin Number ---> " + ii);
			    log.info("claim line Number From UI ---> " + claimlineNumber);
			    log.info("claim Dt Ser for line " + ii +  " ---> " + claimDtSer);
			    log.info("claim procedure Code for line " + ii +  " ---> " + claimprocedureCode);
			    log.info("procedure Description for line " + ii + " ---> " + procedureDescription);
			    log.info("claim Place Of Service for line " + ii +  " ---> " + claimPlaceService);
			    log.info("claim Units for line " + ii +  " ---> " + claimUnits);
			    trailCliamsProcedureFO.setPlaceOfService(claimPlaceService);
			    trailCliamsProcedureFO.setUnits(claimUnits);
			    trailCliamsProcedureFO.setFromDateOfService(claimDtSer);
			    trailCliamsProcedureFO.setProcedureDescription(procedureDescription);
			    trailCliamsProcedureFO.setProcedureCode(claimprocedureCode);
				
				// Test for all procedure modifier codes empty
				String code_pmod1 = ParamUtil.getString(request, "code_pmod1-" + ii);
				String code_pmod2 = ParamUtil.getString(request, "code_pmod2-" + ii);
				String code_pmod3 = ParamUtil.getString(request, "code_pmod3-" + ii);
				String code_pmod4 = ParamUtil.getString(request, "code_pmod4-" + ii);
				boolean pmodFound = false;
				String[] code_pmod = new String [] {"","","",""};
				if ((code_pmod1 != "") || (code_pmod2 != "") || (code_pmod3 != "") || (code_pmod4 != "")) {
					pmodFound = true;
					// Put modifier codes into an array
					if (code_pmod1 != "") {
						code_pmod[0] = code_pmod1;
						log.info("code_pmod1 ---> " + code_pmod1);
					}
					if (code_pmod2 != "") {
						code_pmod[1] = code_pmod2;
						log.info("code_pmod2 ---> " + code_pmod2);
					}
					if (code_pmod3 != "") {
						code_pmod[2] = code_pmod3;
						log.info("code_pmod3 ---> " + code_pmod3);
					}
					if (code_pmod4 != "") {
						code_pmod[3] = code_pmod4;
						log.info("code_pmod4 ---> " + code_pmod4);
					}
				}    
		
			    if ((claimDtSer == "") && (claimprocedureCode == "") && (!pmodFound) && (primaryDiagnosis == "") &&
				    (relDiagnosisTemp == null) && (claimUnits == "")) {
			    	log.info("skipping blank claim line " + ii);
			    } else {
		
					TrailDatesUtils  trailDates = TrailDatesUtils.getTrailDatesUtils();
					trailDates.setClaimLineDate(claimDtSer);
					trailDatesUtils.add(trailDates);
					try {
						if (claimDtSer.substring(2, 3).equals("/")) {
		
							// Convert format
							String month = claimDtSer.substring(0, 2);
							String day = claimDtSer.substring(3, 5);
							String year = claimDtSer.substring(6);
							claimDtSer = year + '-' + month + '-' + day;
						}
						claimPlaceServiceDate = DatatypeFactory.newInstance().newXMLGregorianCalendar(claimDtSer);
					} catch (DatatypeConfigurationException e) {
						log.error("DatatypeConfigurationException in trailClaimDetails method",e);
					}
					log.info("claim Place Of Service Date ---> " + claimPlaceServiceDate);
					
					// Get the earliest and latest dates
					if (ii == 1) {
						headerFromDt = claimPlaceServiceDate;
						headerToDt  = claimPlaceServiceDate;
					} else {
						if (headerFromDt.compare(claimPlaceServiceDate) > 0) {
							headerFromDt = claimPlaceServiceDate;
						}
						if (claimPlaceServiceDate.compare(headerToDt) > 0) {
							headerToDt  = claimPlaceServiceDate;
						}
					}
			
				    requestLineData.setLineNumber(ii);
				    requestLineData.setUnits(Integer.parseInt(claimUnits));
			
				    if ((code_pmod != null) && (code_pmod.length > 0)) {
				    	requestLineData.setProcedureCodeModifiers(code_pmod);
				    } else {
				    	requestLineData.setProcedureCodeModifiers(null);
				    }
			
				    requestLineData.setFromDateOfService(XMLGregorianCalendar(claimPlaceServiceDate));
				    requestLineData.setToDateOfService(XMLGregorianCalendar(claimPlaceServiceDate));
				    requestLineData.setPlaceOfService(claimPlaceService);
				    requestLineData.setProcedureCode(claimprocedureCode);
			
				    // Set the REV code
				    String revCodeData = ParamUtil.getString(request, "revcode_procrow" + ii);
				    RevCode_type1 revCode_type1 = new RevCode_type1();
				    if ((revCodeData != null) && (revCodeData.length() > 0)) {
				    	revCode_type1.setRevCode_type0(revCodeData);
					    trailCliamsProcedureFO.setRevCode(revCodeData);
				    } else {
				    	revCode_type1.setRevCode_type0("");
					    trailCliamsProcedureFO.setRevCode("");
				    }
				    requestLineData.setRevCode(revCode_type1);
				    
				    if ((relDiagnosis != null) && (relDiagnosis.length > 0)) {
						
						// Add a pointer for each related diagnosis code
						String[] codeList = new String [relDiagnosis.length];
						for(int i = 0; i < relDiagnosis.length; i++) {
							String loc = getDiagnosisLocation(diagnosisCodeList, relDiagnosis[i]);
							codeList[i] = loc;
							log.info("relDiagnosis " + (i + 1) + " for line " + ii + " ---> " + relDiagnosis[i] + ", index " + codeList[i]);
						}
						requestLineData.setDiagnosisCodePointers(codeList);
				    } else {
				    	requestLineData.setDiagnosisCodePointers(null);
				    }
				    requestLineData.setHealthPlanGroupId("");
			
				    requestLineData.setRenderingProviderNpi(trailClaimDetailsFO.getProviderInfoRenderingNPI());
				    if (trailClaimDetailsFO.getProviderInfoInNetwork().equals("Y")) {
				    	requestLineData.setInNetworkIndicator(InNetworkIndicator_type1.Y);
				    } else {
				    	requestLineData.setInNetworkIndicator(InNetworkIndicator_type1.N);
				    }
				    requestLineData.setOtherClaimEditorIndicator(OtherClaimEditorIndicator_type1.value1);	// hardcoded to 1
				    requestLineData.setPaidDeniedIndicator(PaidDeniedIndicator_type1.P);					// harcoded to P
				    requestLine.add(requestLineData);
			
				    log.info("requestLine size ---> " + requestLine.size());
		
				    String[] codes = requestLine.get(ii - 1).getProcedureCodeModifiers();
			
					for (int j = 0; j < codes.length; j++) {
			
						if (j == 0) {
							log.info("pmod ---> " + j + codes[j]);
							trailCliamsProcedureFO.setProcModOne(codes[j]);
						}
			
						if (j == 1) {
							log.info("pmod ---> " + j + codes[j]);
							trailCliamsProcedureFO.setProcModTwo(codes[j]);
						}
			
						if (j == 2) {
							log.info("pmod ---> " + j + codes[j]);
							trailCliamsProcedureFO.setProcModThree(codes[j]);
						}
			
						if (j == 3) {
							log.info("pmod ---> " + j + codes[j]);
							trailCliamsProcedureFO.setProcModFour(codes[j]);
						}
			
					}
					// Build the related diagnosis that will be displayed
					StringBuilder relBuilder = new StringBuilder();
					if (relDiagnosisTemp == null) {
						relBuilder.append(" ");
					} else {
						for (int j = 0; j < relDiagnosisTemp.length; j++) {
							relDiagnosisTemp[j] = relDiagnosisTemp[j].replaceAll("\\.", "");
							relBuilder.append(relDiagnosisTemp[j]);
							relBuilder.append(" ");
						}
					}
		
					log.info("related diagnosis codes ---> " + relBuilder.toString());
		
					trailCliamsProcedureFO.setPrimaryDiagnosisCode(primaryDiagnosis);
				    trailCliamsProcedureFO.setRelatedDiagnosisCode(relBuilder.toString());

				    // Set the from date of service as a string for IE
					Calendar calendarFromDateOfService = claimPlaceServiceDate.toGregorianCalendar();
					SimpleDateFormat formatFromDateOfService = new SimpleDateFormat("MM/dd/yyyy");
					formatFromDateOfService.setTimeZone(calendarFromDateOfService.getTimeZone());
					String fromDateOfServiceStr = formatFromDateOfService.format(calendarFromDateOfService.getTime());
					trailCliamsProcedureFO.setFromDateOfService(fromDateOfServiceStr);
					procedureMap.put(String.valueOf(ii), trailCliamsProcedureFO);
				    relBuilder = new StringBuilder();	
				}
			}
		
			for (RequestLineData requestLineDataList : requestLine) {
				log.info("Line Data for Claim Line " + requestLineDataList.getLineNumber() + ":");
			    log.info("  From Date Of Service ---> " + requestLineDataList.getFromDateOfService());
			    log.info("  To Date Of Service ---> " + requestLineDataList.getToDateOfService());
			    log.info("  Health Plan Group Id ---> " + requestLineDataList.getHealthPlanGroupId());
			    log.info("  Rev Code ---> " + requestLineDataList.getRevCode());
			    log.info("  Place Of Service ---> " + requestLineDataList.getPlaceOfService());
			    log.info("  Procedure Code Modifiers ---> " + requestLineDataList.getProcedureCodeModifiers());
			    log.info("  Diagnosis Code Pointers ---> " + requestLineDataList.getDiagnosisCodePointers());
			    log.info("  Procedure Code ---> " + requestLineDataList.getProcedureCode());
				log.info("  Requested Units ---> " + requestUnits.get(Integer.toString(requestLineDataList.getLineNumber())));
			}
		
			log.info("procedure size " + procedureMap.size());
			// Set the from date to the earliest claim line date
			requestHeader.setFromDateOfService(XMLGregorianCalendar(headerFromDt));
		
			// Set the to date to the latest claim line date
			requestHeader.setToDateOfService(XMLGregorianCalendar(headerToDt));
			
			//TODO: Remove mock service code and condition. Don't remove else condition code
			if(GetterUtil.getBoolean(com.liferay.portal.kernel.util.PropsUtil.get(TrailsResearchConstant.ENABLED_MOCK_SERVICE))){
				
				responseHeader.setCeTransactionId("TrId");
				responseHeader.setClaimNumber("22");
				
				ResponseLineData responseLineData = new ResponseLineData();
				responseLineData.setAdviceDecisionType(AdviceDecisionType_type1.value1);
				responseLineData.setApprovedServiceUnitCount(1);
				responseLineData.setLineNumber(2);
				responseLineData.setPayAndEducate(PayAndEducate_type1.Y);
				responseLineData.setPrimaryDecisionCode("PDcode");
				responseLineData.setPrimaryPolicyNecessityCriterion("Policy");
				responseLineData.setPrimaryPolicyTag("Tag");
				responseLineData.setProcedureCode("AAAAA");
				responseLine[0] = responseLineData;
			}else{
				
				// Get the email of the user
				String email = "";
				try {
					email = PortalUtil.getUser((PortletRequest) request).getEmailAddress(); // SP - method 1
					if (email.length() == 0 ) {
						email = ((ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY)).getUser().getEmailAddress(); // SP - method 2
						log.info("Tried to set an email of 0 length, let's try this other way of getting the email: " + email);
					}
				}
				catch(Exception e) {
					log.error("Email of user submitting trial claim is unknown!", e);
					email = "UNKNOWN@UNKNOWN.com"; // SP - didn't want to send an empty string...
				}
				finally {
					log.info("email ---> \"" + email + "\"");
				}

				EvaluateLabClaimServiceStub evaluateLabClaimServiceStub = new EvaluateLabClaimServiceStub();
				EvaluateLabClaim evaluateLabClaim = new EvaluateLabClaim();

				// Set the request header data
				evaluateLabClaim.setRequestHeader(requestHeader);
				
				// set the request line data
				RequestLineData[] requestLineArr = new RequestLineData[requestLine.size()];
				requestLineArr = requestLine.toArray(requestLineArr);
				evaluateLabClaim.setRequestLine(requestLineArr);

				EvaluateLabClaimResponse evaluateLabClaimResponse = evaluateLabClaimServiceStub.evaluateLabClaim(evaluateLabClaim, email);
				
				responseHeader = evaluateLabClaimResponse.getResponseHeader();
				responseLine = evaluateLabClaimResponse.getResponseLine();

				// Release the email address String for garbage collection
				email = null;
			}

			Map<String, String> secondaryCodes = new LinkedHashMap<String, String>();
				
			if (responseHeader.getClaimNumber() != null) {
		
			    for (int i = 0; i < responseLine.length; i++) {
					primaryDecisionCode = responseLine[i].getPrimaryDecisionCode();
			
					responseLine[i].setAdviceDecisionType(responseLine[i].getAdviceDecisionType());
					if ((primaryDecisionCode != null) && !(primaryDecisionCode.equals(""))) {
			
					    //TODO: Remove mock service code and condition. Don't remove else condition code
		    			if(GetterUtil.getBoolean(com.liferay.portal.kernel.util.PropsUtil.get(TrailsResearchConstant.ENABLED_MOCK_SERVICE))){

		    				//Success Mock Code
						    description = null;
		    			}else{

		    		    	// Creating the stub object to invoke the description service
		    			    PriorAuthTrialClaimsServiceStub priorAuthTrialClaimsServiceStub = new PriorAuthTrialClaimsServiceStub();
		    			    GetEnumShrtDescriptionE getEnumShrtDescriptionE = new GetEnumShrtDescriptionE();

		    			    TrialClaimsProcedureReqHdr trialClaimsProcedureReqHdr = new TrialClaimsProcedureReqHdr();
						    trialClaimsProcedureReqHdr.setType("CeReason");
						    trialClaimsProcedureReqHdr.setEnumerationCode(primaryDecisionCode);
						    log.info("primary Decision Code ---> " + primaryDecisionCode);

		    			    GetEnumShrtDescription getEnumShrtDescription = new GetEnumShrtDescription();
		    			    getEnumShrtDescription.setGetEnumShrtDescriptionRequest(trialClaimsProcedureReqHdr);
		    			    getEnumShrtDescriptionE.setGetEnumShrtDescription(getEnumShrtDescription);

		    			    // Initialize the return variables
		    			    GetEnumShrtDescriptionResponseE getEnumShrtDescriptionResponseE;
		    			    GetEnumShrtDescriptionResponse getEnumShrtDescriptionResponse = null;

		    			    try {

		    				    // Invoke the service
		    			    	getEnumShrtDescriptionResponseE = priorAuthTrialClaimsServiceStub.getEnumShrtDescription(getEnumShrtDescriptionE);
		    			    	getEnumShrtDescriptionResponse = getEnumShrtDescriptionResponseE.getGetEnumShrtDescriptionResponse();
		    			        
		    			        // Fetching the data from the response object
		    				    description = getEnumShrtDescriptionResponse.get_return().getEnumerationShortDescription();
								log.info("primary Decision Code description ---> " + description);

		    				    // Add the description to the code field
		    				    primaryDecisionCode += "``" + description;
		    				    responseLine[i].setPrimaryDecisionCode(primaryDecisionCode);
		    				    
								description = null;
					
						    } catch (DAOExceptionException  e) {
						    	log.error("DAOExceptionException during getEnumShrtDescription", e);
						    }
		    			}
					}
			
					if (responseLine[i].getSecondaryCodesData() != null) {
						SecondaryCodes[] secondaryArr = responseLine[i].getSecondaryCodesData();
		
						for (SecondaryCodes secCode: secondaryArr) {
						    secondaryCode = secCode.getSecondaryDecisionCode();
				
						    if ((secondaryCode != null) && !(secondaryCode.equals(""))) {
								log.info("secondaryCode ---> " + secondaryCode);
	
								//TODO: Remove mock service code and condition. Don't remove else condition code
				    			if(GetterUtil.getBoolean(com.liferay.portal.kernel.util.PropsUtil.get(TrailsResearchConstant.ENABLED_MOCK_SERVICE))){
	
				    				//Success Mock Code
				    				secondaryCodes.put(secondaryCode, "TrailDecription");
									description = null;
				    			}else{
							    	TrialClaimsProcedureReqHdr trialClaimsProcedureReqHdr = new TrialClaimsProcedureReqHdr();
									trialClaimsProcedureReqHdr.setType("CeReason");
									trialClaimsProcedureReqHdr.setEnumerationCode(secondaryCode);
	
				    		    	// Creating the stub object to invoke the description service
				    			    PriorAuthTrialClaimsServiceStub priorAuthTrialClaimsServiceStub = new PriorAuthTrialClaimsServiceStub();
				    			    GetEnumShrtDescriptionE getEnumShrtDescriptionE = new GetEnumShrtDescriptionE();
				    			    
				    			    GetEnumShrtDescription getEnumShrtDescription = new GetEnumShrtDescription();
				    			    getEnumShrtDescription.setGetEnumShrtDescriptionRequest(trialClaimsProcedureReqHdr);
				    			    getEnumShrtDescriptionE.setGetEnumShrtDescription(getEnumShrtDescription);
	
				    			    // Initialize the return variables
				    			    GetEnumShrtDescriptionResponseE getEnumShrtDescriptionResponseE;
				    			    GetEnumShrtDescriptionResponse getEnumShrtDescriptionResponse = null;
				    				
									try {
								    	
									    // Invoke the service
								    	getEnumShrtDescriptionResponseE = priorAuthTrialClaimsServiceStub.getEnumShrtDescription(getEnumShrtDescriptionE);
								    	getEnumShrtDescriptionResponse = getEnumShrtDescriptionResponseE.getGetEnumShrtDescriptionResponse();
								        
								        // Fetching the data from the response object
									    description = getEnumShrtDescriptionResponse.get_return().getEnumerationShortDescription();
	
									    secondaryCodes.put(secondaryCode, description);
									    log.info("secondary Code description ---> " + description);
									    description = null;
						
									} catch (DAOExceptionException e) {
										log.error("DAOExceptionException during getEnumShrtDescription", e);
									}
				    			}
								
						    }
						}
					}
			    }

			    decisionCodes = DecisionCodes.getDecisionCodes();
				decisionCodes.setSecondaryCode(secondaryCodes);
				log.info("transaction ID " + responseHeader.getCeTransactionId());
			    log.info("claim number " + responseHeader.getClaimNumber());
			    log.info("line number " + responseLine[0].getLineNumber());
			    log.info("approved service unit code " + responseLine[0].getApprovedServiceUnitCount());
			    log.info("primary decision code " + responseLine[0].getPrimaryDecisionCode());
			    // Get the current date
			    DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss");
			    // This causes CST timezone  dateFormat.setTimeZone(TimeZone.getTimeZone("EST"));
			    Date date = new Date();
			    String today = dateFormat.format(date);
			    // Get the user ID
			    String userEmail;
			    try {
					userEmail = PortalUtil.getUser((PortletRequest) request).getEmailAddress();
					String userName = userEmail.substring(0, userEmail.indexOf('@'));
					panelTitle = "Trial Claim Results ID: " + userName + "    Transaction Date:" + today + " EST";
					log.info("\n" + panelTitle);
		
			    } catch (PortalException e) {
			    	log.error("PortalException during getEmailAddress in trailClaimDetails method", e);
			    } catch (SystemException e) {
			    	log.error("SystemException during getEmailAddress in trailClaimDetails method", e);
			    }
		
			    log.info("requestUnits " + requestUnits);
			    request.setAttribute("requestUnits", requestUnits);
			    request.setAttribute("panelTitle", panelTitle);
			    request.setAttribute("secondaryCodes", secondaryCodes);
			    request.setAttribute("responseHeader", responseHeader);
			    request.setAttribute("decisionCodes", decisionCodes);
			    request.setAttribute("responseLine", responseLine);
			    request.setAttribute("secodaryClaimIndicator", secodaryClaimIndicator);
			    request.setAttribute("diagnosis", diagnosis);
			    request.setAttribute("procedure", procedureMap);
			    request.setAttribute("trailClaimDetailsFO", trailClaimDetailsFO);
			}
		
			// Remove the session attributes.  These are needed for the results.
			// TODO Determine where these go.
			PortletSession portletSession = request.getPortletSession();
			//portletSession.removeAttribute("idCardNumber", PortletSession.APPLICATION_SCOPE);
			portletSession.removeAttribute("firstName", PortletSession.APPLICATION_SCOPE);
			portletSession.removeAttribute("lastName", PortletSession.APPLICATION_SCOPE);
			portletSession.removeAttribute("dateOfBirth", PortletSession.APPLICATION_SCOPE);
			portletSession.removeAttribute("gender", PortletSession.APPLICATION_SCOPE);
			portletSession.removeAttribute("memberSearchStatus", PortletSession.APPLICATION_SCOPE);

			request.setAttribute("message", "");
		} catch (SOAPFaultException e) {
			SOAPFault soapFault = e.getFault();
			String faultCode = soapFault.getFaultCode();
			String faultString = soapFault.getFaultString();
			
			// Get the number from the faultCode
			int idx = -1;
			Pattern pattern = Pattern.compile("^\\D*(\\d)");
			Matcher matcher = pattern.matcher(faultCode);
			if (matcher.find()) {
				idx = matcher.start(1);
			}
			faultCode = faultCode.substring(idx);

			String errorMsg = "Error code: " + faultCode + "<br>Error Message: " + faultString + "<br><br><span class='bigger'>Please update your search and try again.</span>";

			request.setAttribute("message", errorMsg);
			log.error("SOAPFaultException in trailClaimDetails method",e);
		} catch (Exception e) {
			String errorMsg = e.getMessage();
			
			// Remove all newlines
			errorMsg = errorMsg.replaceAll("\n", " ");
			request.setAttribute("message", errorMsg);
		}
		finally {
			log.info("Processing trailClaimDetails for Trial Claim Exit");
		}
    }

	
	/**
	 * Convert XMLGregorianCalendar to yyyy-mm-dd.
	 */

    private String XMLGregorianCalendar(XMLGregorianCalendar dt) {
    	Calendar calendar = dt.toGregorianCalendar();
    	SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
    	formatter.setTimeZone(calendar.getTimeZone());
    	String dateString = formatter.format(calendar.getTime());
    	
    	return dateString;
    }

	
	/**
	 * Get the location of the diagnosis code in the list of codes.
	 */

    private String getDiagnosisLocation(List<String> diagList, String cmp) {
		int idx = 0;
		
		for (int i = 0; i < diagList.size(); i++) {
			if (diagList.get(i).equals(cmp)) {
				idx = i;
				break;
			}
		}
		return Integer.toString(idx + 1);
	}

	
/**
 * Method invocation is ajax-call
 * To fetch the diagnosis code from the ui
 * Diagnosis code will be passed to the getUsers method
 */
    @ResourceMapping
    public void serveResource(ResourceRequest resourceRequest,
	    ResourceResponse resourceResponse) throws IOException,
	    PortletException, SystemException, PortalException {

		String diagcode = resourceRequest.getParameter("diagcode");
		log.info("diagcode" + diagcode);
		;
		String cmd = ParamUtil.getString(resourceRequest, "diagReq");
		if (cmd.equals("get_users")) {
		    getUsers(resourceRequest, resourceResponse, diagcode);
		}
    }

    
    /**
     * Method invocation is ajax-call
     * Based on the diagnosis code 
     * Diagnosis description will be fetched from the database through the  esb service .
     */
    @ResourceMapping(value = "getUsers")
    private void getUsers(ResourceRequest resourceRequest,
	    ResourceResponse resourceResponse, String diagcode)
	    throws IOException, PortletException, SystemException,
	    PortalException {

		String description = null;

	    log.info("diagnosis code ---> " + diagcode);
		
		//TODO: Remove mock service code and condition. Don't remove else condition code
		if(GetterUtil.getBoolean(com.liferay.portal.kernel.util.PropsUtil.get(TrailsResearchConstant.ENABLED_MOCK_SERVICE))){
			 //Success Mock Code
			description = "trialClaimsDiagnosisDesc";
		}else{
		    
	    	// Creating the stub object to invoke the description service
		    PriorAuthTrialClaimsServiceStub priorAuthTrialClaimsServiceStub = new PriorAuthTrialClaimsServiceStub();
		    GetIcdDiagnosisCodeE getIcdDiagnosisCodeE = new GetIcdDiagnosisCodeE();

		    // Add the diagnosis code to the service
		    TrialClaimsDiagnosisReqHdr trialClaimsDiagnosisReqHdr = new TrialClaimsDiagnosisReqHdr();
		    trialClaimsDiagnosisReqHdr.setIcdDiagnosisCode(diagcode);

		    GetIcdDiagnosisCode getIcdDiagnosisCode = new GetIcdDiagnosisCode();
		    getIcdDiagnosisCode.setGetIcdDiagnosisCodeRequest(trialClaimsDiagnosisReqHdr);
		    getIcdDiagnosisCodeE.setGetIcdDiagnosisCode(getIcdDiagnosisCode);
		    
		    // Initialize the return variables
		    GetIcdDiagnosisCodeResponseE getIcdDiagnosisCodeResponseE;
		    GetIcdDiagnosisCodeResponse getIcdDiagnosisCodeResponse = null;

		    try {
		    	
			    // Invoke the service
			    getIcdDiagnosisCodeResponseE = priorAuthTrialClaimsServiceStub.getIcdDiagnosisCode(getIcdDiagnosisCodeE);
			    getIcdDiagnosisCodeResponse = getIcdDiagnosisCodeResponseE.getGetIcdDiagnosisCodeResponse();
		    } catch (DAOExceptionException e) {
				log.error("DAOExceptionException in getUsers method",e);
		    }
        
	        // Fetching the data from the response object
	        description = getIcdDiagnosisCodeResponse.get_return().getIcdShortDescription();
		}

		PrintWriter out = resourceResponse.getWriter();
		
		if ((description != null) && !(description.equals(""))) {
		    out.println(description);
	        log.info("icdShortDescription : " + description);
		} else {
		    String des = "Please enter a valid diagnosis code";
		    out.println(des);
		}
    }

    
    /** Method invocation is ajax-call
     * Based on the procedureCode  
     * Procedure description will be fetched from the database through the esb-service.
     * @param getEnumShrtDescriptionResponseE 
     */
    @ResourceMapping(value = "getProcedureDescription")
    private void getProcedureDescription(ResourceResponse resourceResponse, String procedureCode)
    	    throws IOException, PortletException, SystemException, PortalException {

		// HashMap<String, String> map=new HashMap<String,String>();
		String description = null;
		
		log.info("procedure code ---> " + procedureCode);

		 //TODO: Remove mock service code and condition. Don't remove else condition code
		if(GetterUtil.getBoolean(com.liferay.portal.kernel.util.PropsUtil.get(TrailsResearchConstant.ENABLED_MOCK_SERVICE))){
			 //Success Mock Code
			description = "trialClaimsProcedureDesc";
		}else{

	    	// Creating the stub object to invoke the description service
		    PriorAuthTrialClaimsServiceStub priorAuthTrialClaimsServiceStub = new PriorAuthTrialClaimsServiceStub();
		    GetEnumShrtDescriptionE getEnumShrtDescriptionE = new GetEnumShrtDescriptionE();

		    // Add the procedure code to the service
		    TrialClaimsProcedureReqHdr trialClaimsProcedureReqHdr = new TrialClaimsProcedureReqHdr();
		    trialClaimsProcedureReqHdr.setType("Proc");
		    trialClaimsProcedureReqHdr.setEnumerationCode(procedureCode);
		    
		    GetEnumShrtDescription getEnumShrtDescription = new GetEnumShrtDescription();
		    getEnumShrtDescription.setGetEnumShrtDescriptionRequest(trialClaimsProcedureReqHdr);
		    getEnumShrtDescriptionE.setGetEnumShrtDescription(getEnumShrtDescription);

		    // Initialize the return variables
		    GetEnumShrtDescriptionResponseE getEnumShrtDescriptionResponseE;
		    GetEnumShrtDescriptionResponse getEnumShrtDescriptionResponse = null;
		    
		    try {
		    	
			    // Invoke the service
		    	getEnumShrtDescriptionResponseE = priorAuthTrialClaimsServiceStub.getEnumShrtDescription(getEnumShrtDescriptionE);
		    	getEnumShrtDescriptionResponse = getEnumShrtDescriptionResponseE.getGetEnumShrtDescriptionResponse();
		    } catch (DAOExceptionException e) {
				log.error("DAOExceptionException in getUsers method",e);
		    };
	        
	        // Fetching the data from the response object
		    description = getEnumShrtDescriptionResponse.get_return().getEnumerationShortDescription();
		}
        
		PrintWriter out = resourceResponse.getWriter();
	
		if ((description != null) && !(description.equals(""))) {
		    out.println(description);
	        log.info("procedureShortDescription : " + description);
		} else {
		    String des = "Please enter a valid procedure code";
		    out.println(des);
		}
    }

    
    /**
     * When user unchecks the exclude member check-box the trailClaimMemberDetails method will be execute
     * This method invokes the member search portlet.
     */

    @ActionMapping(params = { "action=trailClaimMember" })
    public void trailClaimMemberDetails(ActionRequest request,
    		                            ActionResponse response,
    		                            @ModelAttribute(value = "memberDetailsFo") MemberDetailsFo memberDetailsFo) { 
    	boolean privateFlag = true;   // True for a private page
		HttpServletRequest httpRequest = PortalUtil.getHttpServletRequest(request);
		ThemeDisplay themeDisplay1 = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
		try {
			Layout layout = LayoutLocalServiceUtil.getFriendlyURLLayout(themeDisplay1.getLayout().getGroupId(), privateFlag, com.liferay.util.portlet.PortletProps.get("member-search-page-url"));// member page 
			PortletURL portletURL = PortletURLFactoryUtil.create(httpRequest,com.liferay.util.portlet.PortletProps.get("member-search-info-page"),layout.getPlid(), PortletRequest.RENDER_PHASE); // member portlet name space
			portletURL.setWindowState(WindowState.MAXIMIZED);
			portletURL.setPortletMode(PortletMode.VIEW);
			response.sendRedirect(portletURL.toString());
		} catch (IOException e) {
			log.error("IOException in trailClaimMemberDetails method",e);
		} catch (PortalException e) {
			log.error("PortalException in trailClaimMemberDetails method",e);
		} catch (PortletModeException e) {
			log.error("PortletModeException in trailClaimMemberDetails method",e);
		} catch (SystemException e) {
			log.error("SystemException in trailClaimMemberDetails method",e);
		} catch (WindowStateException e) {
			log.error("WindowStateException in trailClaimMemberDetails method",e);
		}
    }
    
    /**
     * 
     * @param actionRequest
     * @param actionResponse
     * Whenever user click on cancel button on the disclaimer message cancelDisclaimerPage method will be executed 
     */

    @ActionMapping(params = { "canceldisclaimeraction=trailClaimsCancelDisclaimerAction" })
    public void cancelDisclaimerPage(ActionRequest actionRequest,
	                                 ActionResponse actionResponse) {

    	log.info("Processing Cancel Disclaimer Action for Trial Claim Entry");

    	// Remove the Trial Claim attributes
		PortletSession portletSession = actionRequest.getPortletSession();
		portletSession.setAttribute("idCardNumber", "cancelID" ,PortletSession.APPLICATION_SCOPE);
		portletSession.removeAttribute("firstName", PortletSession.APPLICATION_SCOPE);
		portletSession.removeAttribute("lastName", PortletSession.APPLICATION_SCOPE);
		portletSession.removeAttribute("dateOfBirth", PortletSession.APPLICATION_SCOPE);
		portletSession.removeAttribute("gender", PortletSession.APPLICATION_SCOPE);
		portletSession.removeAttribute("memberSearchStatus", PortletSession.APPLICATION_SCOPE);

    	String userName = null;
	    List usersList = null;
		try {
		    User user = PortalUtil.getUser((PortletRequest) actionRequest);
		    if (user != null) {
			    usersList = user.getUserGroups();
		    }
		} catch (PortalException e) {
			log.error("PortalException in cancelDisclaimerPage method",e);
		} catch (SystemException e) {
			log.error("SystemException in cancelDisclaimerPage method",e);
		}

	    HttpServletRequest request = PortalUtil.getHttpServletRequest((PortletRequest) actionRequest);
		String pathRedirect = getLandingPage(request, userName, usersList);
		log.info(userName + " " + pathRedirect);
		try {
		    actionResponse.sendRedirect(pathRedirect);
		} catch (IOException e) {
			log.error("IOException during redirection in cancelDisclaimerPage method",e);
		}
    	log.info("Processing Cancel Disclaimer Action for Trial Claim Exit");
    }
    
    /**
     * 
     * @param actionRequest
     * @param actionResponse
     * Whenever user click on cancel  button cancelPage method will be executed 
     */

    @ActionMapping(params = { "cancelaction=trailClaimsCancelAction" })
    public void cancelPage(ActionRequest actionRequest,
	                       ActionResponse actionResponse) {

    	log.info("Processing Cancel Action for Trial Claim Entry");

    	// Remove the Trial Claim attributes
		PortletSession portletSession = actionRequest.getPortletSession();
		portletSession.setAttribute("disclaimerShownFlag", "true", PortletSession.APPLICATION_SCOPE);
		portletSession.setAttribute("idCardNumber", "bogusID" ,PortletSession.APPLICATION_SCOPE);
		portletSession.removeAttribute("firstName", PortletSession.APPLICATION_SCOPE);
		portletSession.removeAttribute("lastName", PortletSession.APPLICATION_SCOPE);
		portletSession.removeAttribute("dateOfBirth", PortletSession.APPLICATION_SCOPE);
		portletSession.removeAttribute("gender", PortletSession.APPLICATION_SCOPE);
		portletSession.removeAttribute("memberSearchStatus", PortletSession.APPLICATION_SCOPE);

    	String userName = null;
	    List usersList = null;
		try {
		    User user = PortalUtil.getUser((PortletRequest) actionRequest);
		    if (user != null) {
			    usersList = user.getUserGroups();
		    }
		} catch (PortalException e) {
			log.error("PortalException in cancelPage method",e);
		} catch (SystemException e) {
			log.error("SystemException in cancelPage method",e);
		}

	    HttpServletRequest request = PortalUtil.getHttpServletRequest((PortletRequest) actionRequest);
		String pathRedirect = getLandingPage(request, userName, usersList);
		log.info(userName + " " + pathRedirect);
		try {
		    actionResponse.sendRedirect(pathRedirect);
		} catch (IOException e) {
			log.error("SystemException in cancelPage method during redirection ",e);
		}
    	log.info("Processing Cancel Action for Trial Claim Exit");
    }

    private String getLandingPage(HttpServletRequest request, String userName, List usersList) {
    	
	    ArrayList<String> groupNames = new ArrayList<String>();
	    if (usersList != null) {
	    	int length = usersList.size();
		    for (int i = 0; i < length; ++i) {
		    	
		    	// Ignore the groups that do not have a home page
				if (!((UserGroup)usersList.get(i)).getName().equalsIgnoreCase("CDSUsers") &&
					!((UserGroup)usersList.get(i)).getName().equalsIgnoreCase("Everyone") &&
					!((UserGroup)usersList.get(i)).getName().equalsIgnoreCase("ManagedUsers") &&
					!((UserGroup)usersList.get(i)).getName().equalsIgnoreCase("Multi-Factor") &&
					!((UserGroup)usersList.get(i)).getName().equalsIgnoreCase("PortalAdmin") &&
					!((UserGroup)usersList.get(i)).getName().equalsIgnoreCase("UMGroup")) {
					userName = ((UserGroup) usersList.get(i)).getName();
					groupNames.add(userName);
				}
		    }
	    }
	    String path = PortalUtil.getCurrentCompleteURL((HttpServletRequest) request);
	    StringBuilder output = new StringBuilder();
	    int count = 0;
	    char[] ch = path.toCharArray();
	    for (int i2 = 0; i2 < ch.length; ++i2) {
			if (ch[i2] == '/') {
			    ++count;
			}
			if (count >= 3)
			    continue;
			output = output.append(ch[i2]);
	    }

	    String landingPage = null;
	    if (usersList == null) {
			
			// Set to the default landing page since the user does not belong to any groups
			landingPage = "/web/guest/home";
	    } else {
	    	

	    	// Get the landing page based on the user's group
			landingPage = com.liferay.portal.kernel.util.PropsUtil.get("default.landing.page.path[" + userName  + "]"); 
			
			// If the user's group is not found use the defaule landing page
			if (landingPage == null) {
				landingPage = com.liferay.portal.kernel.util.PropsUtil.get("default.landing.page.path");
				if (landingPage == null) {
					
					// Set to the default landing page since it is not defined in the properties file
					landingPage = "/web/guest/home";
				}
			}
	    }
	    String pathRedirect = null;
		pathRedirect = output + landingPage;
		
		return (pathRedirect);
    }
    
    @ActionMapping(params = { "newtrialclaimaction=newTrailClaimsAction" })
    public void newTrailClaimsAction(ActionRequest actionRequest,
	                                 ActionResponse actionResponse) {
    	
    	
    	log.info("Processing New Trial Claim Action for Trial Claim Entry");

    	// Remove the attributes for a new claim
    	PortletSession portletSession = actionRequest.getPortletSession();
		portletSession.removeAttribute("firstName", PortletSession.APPLICATION_SCOPE);
		portletSession.removeAttribute("lastName", PortletSession.APPLICATION_SCOPE);
		portletSession.removeAttribute("dateOfBirth", PortletSession.APPLICATION_SCOPE);
		portletSession.removeAttribute("gender", PortletSession.APPLICATION_SCOPE);
		portletSession.removeAttribute("memberSearchStatus", PortletSession.APPLICATION_SCOPE);

		boolean privateFlag = true; // true for a private page, false for a public page
		HttpServletRequest httpRequest = PortalUtil.getHttpServletRequest(actionRequest);
		ThemeDisplay themeDisplay1 = (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
		try {
			Layout layout = LayoutLocalServiceUtil.getFriendlyURLLayout(themeDisplay1.getLayout().getGroupId(), privateFlag, com.liferay.util.portlet.PortletProps.get("trial-claims-page-url"));// trial claim page 
			PortletURL portletURL = PortletURLFactoryUtil.create(httpRequest,com.liferay.util.portlet.PortletProps.get("trial-claims-info-page"),layout.getPlid(), PortletRequest.RENDER_PHASE); // trial claim portlet name space
			portletURL.setWindowState(WindowState.MAXIMIZED);
			portletURL.setPortletMode(PortletMode.VIEW);
			actionResponse.sendRedirect(portletURL.toString());
		} catch (IOException e) {
			log.error("IOException in newTrailClaimsAction method",e);
		} catch (PortalException e) {
			log.error("PortalException in newTrailClaimsAction method",e);
		} catch (PortletModeException e) {
			log.error("PortletModeException in newTrailClaimsAction method",e);
		} catch (SystemException e) {
			log.error("SystemException in newTrailClaimsAction method",e);
		} catch (WindowStateException e) {
			log.error("WindowStateException in newTrailClaimsAction method",e);
		}
    	log.info("Processing New Trial Claim Action for Trial Claim Exit");
	}

    @ExceptionHandler(LBMCustomException.class)
    public ModelAndView handleCustomException(LBMCustomException ex) {
		ModelAndView model = new ModelAndView();
		model.addObject("errCode", ex.getErrCode());
		model.addObject("errMsg", ex.getErrMsg());
		model.setViewName("view");
		return model;
    }

    @ExceptionHandler(Exception.class)
    public ModelAndView handleError(RenderRequest request, Exception e) {
		ModelAndView mav = new ModelAndView();
		mav.addObject("errMsg", e.getMessage());
		mav.setViewName("view");
		return mav;
    }
}
