package com.avalon.lbm.portlets.claim.servicehandler;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

public class TrailClaimUtils  {
    
    
    private static TrailClaimUtils trailClaimUtils  = null;
    
    static{
		if(trailClaimUtils == null){
			trailClaimUtils = new TrailClaimUtils();
		}
    }
    
    
    private TrailClaimUtils(){
	
    }
    
    
    
    public static TrailClaimUtils getTrailClaimUtils(){
    	return trailClaimUtils;
    }
    
    public List<TrailDatesUtils> getTrailHeadderDates(List<TrailDatesUtils> trailClaimsList){
	
	TrailDatesUtils trailDatesUtils  = new TrailDatesUtils();
	Collections.sort(trailClaimsList, trailDatesUtils);
	
	return trailClaimsList;
    }
    
    
    
    public String getPatientDob(String age){
	
	DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	Date date = new Date();
	String currentDate  = dateFormat.format(date); 
	int dob = Integer.valueOf(currentDate.substring(0,4)) - Integer.parseInt(age);
	currentDate = currentDate.substring(4,10);
	return dob+currentDate;
	
    }
    
    
    
	public static void main(String args[]){
		List<TrailDatesUtils> list = new ArrayList<TrailDatesUtils>();
		TrailDatesUtils trailDatesUtils1 = new TrailDatesUtils();
		TrailDatesUtils trailDatesUtils2 = new TrailDatesUtils();
		TrailDatesUtils trailDatesUtils3 = new TrailDatesUtils();
		trailDatesUtils1.setClaimLineDate("2016-05-10");
		trailDatesUtils2.setClaimLineDate("2016-05-01");
		trailDatesUtils3.setClaimLineDate("2016-05-03");
		list.add(trailDatesUtils1);
		list.add(trailDatesUtils2);
		list.add(trailDatesUtils3);
		
		TrailDatesUtils trailDatesUtils  = new TrailDatesUtils();
		Collections.sort(list, trailDatesUtils);
			
		System.out.println(" Max size "  +list.get(list.size()-1).getClaimLineDate());
		System.out.println(" Min size "  +list.get(0).getClaimLineDate());
			
		for(TrailDatesUtils trailDatesUtilss :list){
		    System.out.println(trailDatesUtilss.getClaimLineDate());
		}
			
		TrailClaimUtils  trailClaimUtils   = new TrailClaimUtils();
		System.out.println("Age ---> " + trailClaimUtils.getPatientDob("23"));
	}
}
