package com.avalon.lbm.portlets.claim.model;

import java.io.Serializable;

public class ResearchDecisionFO implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -6689532739396748595L;

    /**
     * @return the claimId
     */
    public String getClaimId() {
	return claimId;
    }

    /**
     * @param claimId
     *            the claimId to set
     */
    public void setClaimId(String claimId) {
	this.claimId = claimId;
    }

    private String claimId;
   

}
