/**
 * DAOExceptionException.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.7.6  Built on : Jul 30, 2017 (09:08:31 BST)
 */
package com.avalon.lbm.services;

public class DAOExceptionException extends java.lang.Exception {
    private static final long serialVersionUID = 1527257022934L;
    private com.avalon.lbm.services.PriorAuthTrialClaimsServiceStub.DAOExceptionE faultMessage;

    public DAOExceptionException() {
        super("DAOExceptionException");
    }

    public DAOExceptionException(java.lang.String s) {
        super(s);
    }

    public DAOExceptionException(java.lang.String s, java.lang.Throwable ex) {
        super(s, ex);
    }

    public DAOExceptionException(java.lang.Throwable cause) {
        super(cause);
    }

    public void setFaultMessage(
        com.avalon.lbm.services.PriorAuthTrialClaimsServiceStub.DAOExceptionE msg) {
        faultMessage = msg;
    }

    public com.avalon.lbm.services.PriorAuthTrialClaimsServiceStub.DAOExceptionE getFaultMessage() {
        return faultMessage;
    }
}
