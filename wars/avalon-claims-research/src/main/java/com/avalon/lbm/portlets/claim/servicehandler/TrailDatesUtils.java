package com.avalon.lbm.portlets.claim.servicehandler;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

public class TrailDatesUtils implements Comparator<TrailDatesUtils> {
    
	private static final Log log = LogFactoryUtil.getLog(TrailDatesUtils.class.getName());

    
    private static TrailDatesUtils trailDatesUtils = null;
    
    static{
	if(trailDatesUtils == null){
	    trailDatesUtils = new TrailDatesUtils();
	}
    }
    
    
    public static TrailDatesUtils getTrailDatesUtils(){
	return trailDatesUtils;
    }
    
    private String claimLineDate;

    /**
     * @return the claimLineDate
     */
    public String getClaimLineDate() {
        return claimLineDate;
    }

    /**
     * @param claimLineDate the claimLineDate to set
     */
    public void setClaimLineDate(String claimLineDate) {
        this.claimLineDate = claimLineDate;
    }

    @Override
    public int compare(TrailDatesUtils date1, TrailDatesUtils date2) {
	int flag = 0;
	
	SimpleDateFormat sdf = new SimpleDateFormat( "yyyy-MM-dd" );
	Date d1;
	Date d2; 
	try {
	    d1 = sdf.parse( date1.getClaimLineDate() );
	    d2 = sdf.parse( date2.getClaimLineDate() );
	    Long days = d1.getTime() - d2.getTime();
	    flag  = days.intValue();
	} catch (ParseException e) {
		log.error("ParseException in compare method",e);
	}
	return flag;
	
	
	
    }
    
    

}
