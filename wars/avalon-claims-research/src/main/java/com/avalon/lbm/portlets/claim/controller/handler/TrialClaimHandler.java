package com.avalon.lbm.portlets.claim.controller.handler;

import java.util.Set;

import javax.xml.namespace.QName;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPHeader;
import javax.xml.soap.SOAPHeaderElement;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

public class TrialClaimHandler implements SOAPHandler<SOAPMessageContext> {
	
	private static final Log log = LogFactoryUtil.getLog(TrialClaimHandler.class.getName());
	
	// SP - email, set by constructor
    private final String email;
    // SP - create the appCreateUserId as <trial:appCreateUserId></trial:appCreateUserId>
    private static final QName appCreateUserId = new QName("http://wr01.benemgmt.com/claimEditor/","appCreateUserId","trial");
    public TrialClaimHandler(String email)
    {
    	this.email = email;
    }
	
    public boolean handleMessage(SOAPMessageContext smc) {
 
        Boolean outboundProperty = (Boolean) smc.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY);
        // SP - only handle outbound messages
        if (outboundProperty.booleanValue()) { 
            try {
                SOAPEnvelope envelope = smc.getMessage().getSOAPPart().getEnvelope();
                SOAPHeader header = envelope.getHeader();
                // SP - create header if it doesn't exist
                if (header == null) header = envelope.addHeader();                
                SOAPHeaderElement header_element = header.addHeaderElement(appCreateUserId); // SP - Adds the element
                header_element.addTextNode(email); // SP - Adds the text in it
                smc.getMessage().saveChanges();
                // SP - Print out the outbound SOAP message
                log.info("Injected SOAP Header elements: " + header.toString());
                
                
            } catch (Exception e) {
            	log.error("Exception in handleMessage method", e);
            }
 
        } else {
            try {
            	// This would be the inbound message
            } catch (Exception ex) {
                ex.printStackTrace();
            } 
        }
 
 
        return outboundProperty;
 
    }
 
    public Set getHeaders() {
        return java.util.Collections.emptySet();
    }
 
    public boolean handleFault(SOAPMessageContext context) {
        return true;
    }
 
    public void close(MessageContext context) {
    //throw new UnsupportedOperationException("Not supported yet.");
    }
}