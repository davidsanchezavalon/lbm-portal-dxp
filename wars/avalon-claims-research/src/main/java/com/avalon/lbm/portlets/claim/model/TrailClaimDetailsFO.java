/**
 * 
 */
package com.avalon.lbm.portlets.claim.model;

import java.io.Serializable;
import java.util.Calendar;

/**
 * @author l.radha.percherla
 *
 */
public class TrailClaimDetailsFO implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
   
    private String memberInfoExclude;
    private String memberSearchStatus;
    private String initialStatus;
    private String memberId;
    private String memberFirstName;
    private String memberLastName;
    private String memberDOB;
    private String memberAge;
    private String memberTestGender;
    private String memberGender;
    private String memberBlueCardStatus;
    
    private String providerInfoBillingTIN;
    private String providerInfoBillingNPI;
    private String providerInfoRenderingTIN;
    private String providerInfoRenderingNPI;
    private String providerInfoInNetwork;
    private String additionalCriteriaSecondaryClaim;
    private String additionalCriteriaOriginalNumber;
    private String additionalInstitutionalClaim;
    private String claimLineMaxNumber;
    private String fromServiceDate;
    /**
     * @return the fromServiceDate
     */
    public String getFromServiceDate() {
        return fromServiceDate;
    }
    /**
     * @param fromServiceDate the fromServiceDate to set
     */
    public void setFromServiceDate(String fromServiceDate) {
        this.fromServiceDate = fromServiceDate;
    }
    /**
     * @return the toServiceDate
     */
    public String getToServiceDate() {
        return toServiceDate;
    }
    /**
     * @param toServiceDate the toServiceDate to set
     */
    public void setToServiceDate(String toServiceDate) {
        this.toServiceDate = toServiceDate;
    }

    private String toServiceDate;
    
    public String getClaimLineMaxNumber() {
        return claimLineMaxNumber;
    }
    public void setClaimLineMaxNumber(String claimLineMaxNumber) {
        this.claimLineMaxNumber = claimLineMaxNumber;
    }

    /**
     * @return the memberInfoExclude
     */
    public String getMemberInfoExclude() {
        return memberInfoExclude;
    }

    /**
     * @param memberInfoExclude
     *            the memberInfoExclude to set
     */
    public void setMemberInfoExclude(String memberInfoExclude) {
        this.memberInfoExclude = memberInfoExclude;
    }

    /**
     * @return the memberSearchStatus
     */
    public String getMemberSearchStatus() {
        return memberSearchStatus;
    }

    /**
     * @param memberSearchStatus
     *            the memberSearchStatus to set
     */
    public void setMemberSearchStatus(String memberSearchStatus) {
        this.memberSearchStatus = memberSearchStatus;
    }

    /**
     * @return the initialStatus
     */
    public String getInitialStatus() {
        return initialStatus;
    }

    /**
     * @param initialStatus
     *            the initialStatus to set
     */
    public void setInitialStatus(String initialStatus) {
        this.initialStatus = initialStatus;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getMemberFirstName() {
        return memberFirstName;
    }

    public void setMemberFirstName(String memberFirstName) {
        this.memberFirstName = memberFirstName;
    }

    public String getMemberLastName() {
        return memberLastName;
    }

    public void setMemberLastName(String memberLastName) {
        this.memberLastName = memberLastName;
    }

    public String getMemberDOB() {
        return memberDOB;
    }

    public void setMemberDOB(String memberDOB) {
        this.memberDOB = memberDOB;
    }

    public String getMemberAge() {
        return memberAge;
    }

    public void setMemberAge(String memberAge) {
        this.memberAge = memberAge;
    }

    public String getMemberTestGender() {
        return memberTestGender;
    }

    public void setMemberTestGender(String memberTestGender) {
        this.memberTestGender = memberTestGender;
    }

    public String getMemberGender() {
        return memberGender;
    }

    public void setMemberGender(String memberGender) {
        this.memberGender = memberGender;
    }

    public String getMemberBlueCardStatus() {
        return memberBlueCardStatus;
    }

    public void setMemberBlueCardStatus(String memberBlueCardStatus) {
        this.memberBlueCardStatus = memberBlueCardStatus;
    }

    /**
     * @return the providerInfoBillingTIN
     */
    public String getProviderInfoBillingTIN() {
        return providerInfoBillingTIN;
    }

    /**
     * @param providerInfoBillingTIN
     *            the providerInfoBillingTIN to set
     */
    public void setProviderInfoBillingTIN(String providerInfoBillingTIN) {
        this.providerInfoBillingTIN = providerInfoBillingTIN;
    }

    /**
     * @return the providerInfoBillingNPI
     */
    public String getProviderInfoBillingNPI() {
        return providerInfoBillingNPI;
    }

    /**
     * @param providerInfoBillingNPI
     *            the providerInfoBillingNPI to set
     */
    public void setProviderInfoBillingNPI(String providerInfoBillingNPI) {
        this.providerInfoBillingNPI = providerInfoBillingNPI;
    }

    /**
     * @return the providerInfoRenderingTIN
     */
    public String getProviderInfoRenderingTIN() {
        return providerInfoRenderingTIN;
    }

    /**
     * @param providerInfoRenderingTIN
     *            the providerInfoRenderingTIN to set
     */
    public void setProviderInfoRenderingTIN(String providerInfoRenderingTIN) {
        this.providerInfoRenderingTIN = providerInfoRenderingTIN;
    }

    /**
     * @return the providerInfoRenderingNPI
     */
    public String getProviderInfoRenderingNPI() {
        return providerInfoRenderingNPI;
    }

    /**
     * @param providerInfoRenderingNPI
     *            the providerInfoRenderingNPI to set
     */
    public void setProviderInfoRenderingNPI(String providerInfoRenderingNPI) {
        this.providerInfoRenderingNPI = providerInfoRenderingNPI;
    }

    /**
     * @return the providerInfoInNetwork
     */
    public String getProviderInfoInNetwork() {
        return providerInfoInNetwork;
    }

    /**
     * @param providerInfoInNetwork
     *            the providerInfoInNetwork to set
     */
    public void setProviderInfoInNetwork(String providerInfoInNetwork) {
        this.providerInfoInNetwork = providerInfoInNetwork;
    }

    /**
     * @return the additionalCriteriaSecondaryClaim
     */
    public String getAdditionalCriteriaSecondaryClaim() {
        return additionalCriteriaSecondaryClaim;
    }

    /**
     * @param additionalCriteriaSecondaryClaim
     *            the additionalCriteriaSecondaryClaim to set
     */
    public void setAdditionalCriteriaSecondaryClaim(
            String additionalCriteriaSecondaryClaim) {
        this.additionalCriteriaSecondaryClaim = additionalCriteriaSecondaryClaim;
    }

    /**
     * @return the additionalCriteriaOriginalNumber
     */
    public String getAdditionalCriteriaOriginalNumber() {
        return additionalCriteriaOriginalNumber;
    }

    /**
     * @param additionalCriteriaOriginalNumber
     *            the additionalCriteriaOriginalNumber to set
     */
    public void setAdditionalCriteriaOriginalNumber(
            String additionalCriteriaOriginalNumber) {
        this.additionalCriteriaOriginalNumber = additionalCriteriaOriginalNumber;
    }

    /**
     * @return the additionalInstitutionalClaim
     */
    public String getAdditionalInstitutionalClaim() {
        return additionalInstitutionalClaim;
    }

    /**
     * @param additionalInstitutionalClaim
     *            the additionalInstitutionalClaim to set
     */
    public void setAdditionalInstitutionalClaim(
            String additionalInstitutionalClaim) {
        this.additionalInstitutionalClaim = additionalInstitutionalClaim;
    }
    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(", memberInfoExclude=");
        builder.append(memberInfoExclude);
        builder.append(", providerInfoBillingTIN=");
        builder.append(providerInfoBillingTIN);
        builder.append(", providerInfoBillingNPI=");
        builder.append(providerInfoBillingNPI);
        builder.append(", providerInfoRenderingTIN=");
        builder.append(providerInfoRenderingTIN);
        builder.append(", providerInfoRenderingNPI=");
        builder.append(providerInfoRenderingNPI);
        builder.append(", providerInfoInNetwork=");
        builder.append(providerInfoInNetwork);
        builder.append(", additionalCriteriaSecondaryClaim=");
        builder.append(additionalCriteriaSecondaryClaim);
        builder.append(", additionalCriteriaOriginalNumber=");
        builder.append(additionalCriteriaOriginalNumber);
        builder.append(", additionalInstitutionalClaim=");
        builder.append(additionalInstitutionalClaim);
        builder.append(", claimLineMaxNumber=");
        builder.append(claimLineMaxNumber);
        builder.append(", fromServiceDate=");
        builder.append(fromServiceDate);
        builder.append(", toServiceDate=");
        builder.append(toServiceDate);
        
        // Member search fields
        builder.append(", memberId=");
        builder.append(memberId);
        builder.append(", memberFirstName=");
        builder.append(memberFirstName);
        builder.append(", memberLastName=");
        builder.append(memberLastName);
        builder.append(", memberDOB=");
        builder.append(memberDOB);
        builder.append(", memberGender=");
        builder.append(memberGender);
        builder.append(", memberBlueCardStatus=");
        builder.append(memberBlueCardStatus);
        
        // Non member search field
        builder.append(", memberAge=");
        builder.append(memberAge);
        builder.append(", memberTestGender=");
        builder.append(memberTestGender);

        builder.append("]");
        return builder.toString();
    }
}
