package com.avalon.lbm.portlets.claim.controller.handler;

import java.util.ArrayList;
import java.util.List;
import javax.xml.ws.handler.Handler;
import javax.xml.ws.handler.HandlerResolver;
import javax.xml.ws.handler.PortInfo;

/**
 *
 * @author www.javadb.com
 */
public class TrialClaimHandlerResolver implements HandlerResolver {
	private final String email;

	public TrialClaimHandlerResolver(String email) {
		this.email = email;
	}

	public List<Handler> getHandlerChain(PortInfo portInfo) {
		List<Handler> handlerChain = new ArrayList<Handler>(1);

		handlerChain.add(new TrialClaimHandler(email));

		return handlerChain;
	}
}