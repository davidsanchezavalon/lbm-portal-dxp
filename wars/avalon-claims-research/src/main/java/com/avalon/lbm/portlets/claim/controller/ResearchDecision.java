/**
 * Description
 *		This file contain the controller methods for the Review Claim Advice Page.
 *
 * CHANGE History
 * 		Version 1.0
 * 			Initial version
 *  
 */

package com.avalon.lbm.portlets.claim.controller;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletMode;
import javax.portlet.PortletModeException;
import javax.portlet.PortletRequest;
import javax.portlet.PortletURL;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.WindowState;
import javax.portlet.WindowStateException;
import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.axis2.AxisFault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import com.avalon.lbm.portlets.claim.model.ResearchDecisionFO;
import com.avalon.lbm.services.ResearchDecisionServiceStub;
import com.avalon.lbm.services.ResearchDecisionServiceStub.ResearchDecisionRequest;
import com.avalon.lbm.services.ResearchDecisionServiceStub.ResearchDecisionRequestE;
import com.avalon.lbm.services.ResearchDecisionServiceStub.RetrieveClaimDetails;
import com.avalon.lbm.services.ResearchDecisionServiceStub.RetrieveClaimDetailsE;
import com.avalon.lbm.services.ResearchDecisionServiceStub.RetrieveClaimDetailsResponse;
import com.avalon.lbm.services.ResearchDecisionServiceStub.RetrieveClaimDetailsResponseE;
import com.avalon.lbm.xml.beans.CeDecisionResultsType;
import com.avalon.lbm.xml.beans.CeHeaderType;
import com.avalon.trails.research.constants.TrailsResearchConstant;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Layout;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.model.UserGroup;
import com.liferay.portal.kernel.portlet.PortletURLFactoryUtil;
import com.liferay.portal.kernel.service.LayoutLocalServiceUtil;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.WebKeys;

@Controller(value = "ResearchDecision")
@RequestMapping(value = { "VIEW" })
public class ResearchDecision implements TrailsResearchConstant {

	private static final Log log = LogFactoryUtil.getLog(ResearchDecision.class.getName());
	   
    /**
     * Render method which called during inital load of research a decision view
     */
    
    @RenderMapping
    public String researchDecisionRender(RenderRequest request,
	    RenderResponse response, Model model) { // 

	log.info(" We are in research render Request ");
	return TrailsResearchConstant.VIEW_DISPLAY;
    }
    
    //TODO: Uncomment below method
    
    /**
     * Action method which gets called on click of Submit Claim button
     * Invoke research decision service and service returns xml response in the form of String object.
     * We are un-marshalling the response and converted into   CeDecisionResultsType object.
     * From the CeDecisionResultsType object we are fetching the details.
     */
    
    @ActionMapping(params = { "action=researchClaimAction" })
    public void researchClaimAction(ActionRequest request,
	                                ActionResponse response,
	                                @ModelAttribute(value = "researchDecisionFO") ResearchDecisionFO researchDecisionFO) {

	    List<CeHeaderType> ceHeaderList = null;
		Map<String, String> transactionDate = new ConcurrentHashMap<String, String>();
	
		log.info(" We are in Research Claim Action ");
		String claimId = researchDecisionFO.getClaimId();
		log.info("claimId ------> " + claimId);

		//TODO: Remove mock service code and condition. Don't remove else condition code
		if(GetterUtil.getBoolean(com.liferay.portal.kernel.util.PropsUtil.get(ENABLED_MOCK_SERVICE))){
			setDetailsInSuccessMock(ceHeaderList,request,response,transactionDate);
		}else{
			
			try {
				ResearchDecisionServiceStub researchDecisionServiceStub = new ResearchDecisionServiceStub();
				
				// Set the request object
				ResearchDecisionRequest researchDecisionRequest = new ResearchDecisionRequest();
				ResearchDecisionRequestE researchDecisionRequestE = new ResearchDecisionRequestE();
				researchDecisionRequest.setHealthPlanClaimNumber(researchDecisionFO.getClaimId().trim());
				researchDecisionRequest.setHealthPlanId(TrailsResearchConstant.HEALTH_PLAN_ID);						// 0010
				researchDecisionRequest.setHealthPlanIdTypeCode(TrailsResearchConstant.HEALTH_PLAN_ID_TYPE_CODE);	// PI
				researchDecisionRequestE.setResearchDecisionRequest(researchDecisionRequest);
				
			    // Initialize the return variables
				RetrieveClaimDetailsResponse retrieveClaimDetailsResponse = null;
				RetrieveClaimDetailsE retrieveClaimDetailsE = new RetrieveClaimDetailsE();

			    // Invoke the service
				RetrieveClaimDetails retrieveClaimDetails = new RetrieveClaimDetails();
				retrieveClaimDetails.setResearchDecisionRequest(researchDecisionRequest);
				retrieveClaimDetailsE.setRetrieveClaimDetails(retrieveClaimDetails);
				RetrieveClaimDetailsResponseE retrieveClaimDetailsResponseE = researchDecisionServiceStub.retrieveClaimDetails(retrieveClaimDetailsE);
				retrieveClaimDetailsResponse = retrieveClaimDetailsResponseE.getRetrieveClaimDetailsResponse();
				String resultsResponse = retrieveClaimDetailsResponse.get_return().getCeResponse();
				
				if ((resultsResponse != null) && !(resultsResponse.equals(""))) {
				    InputStream fileInput = new ByteArrayInputStream(resultsResponse.getBytes());
				    
				    JAXBContext jaxbContext = null;
				    try {
				    	jaxbContext = JAXBContext.newInstance(CeDecisionResultsType.class);
					} catch (JAXBException exception) {
						response.setRenderParameter(TrailsResearchConstant.ERROR_VARIABLE, TrailsResearchConstant.ERROR_MESSAGE);
						response.setRenderParameter(TrailsResearchConstant.MVC_PATH, TrailsResearchConstant.VIEW_PAGE_LOCATION);
						log.error("JAXBException When assign jaxbContext in researchClaimAction method",exception);
				    }
				    Unmarshaller jaxbUnmarshaller = null;
				    try {
				    	jaxbUnmarshaller = jaxbContext.createUnmarshaller();
					} catch (JAXBException exception) {
						response.setRenderParameter(TrailsResearchConstant.ERROR_VARIABLE, TrailsResearchConstant.ERROR_MESSAGE);
						response.setRenderParameter(TrailsResearchConstant.MVC_PATH, TrailsResearchConstant.VIEW_PAGE_LOCATION);
						log.error("JAXBException When Unmarshaller jaxbContext in researchClaimAction method",exception);
				    }
				    CeDecisionResultsType ceDecisionResultsType = null;
				    try {
				    	ceDecisionResultsType = (CeDecisionResultsType) jaxbUnmarshaller.unmarshal(fileInput);
					} catch (JAXBException exception) {
						response.setRenderParameter(TrailsResearchConstant.ERROR_VARIABLE, TrailsResearchConstant.ERROR_MESSAGE);
						response.setRenderParameter(TrailsResearchConstant.MVC_PATH, TrailsResearchConstant.VIEW_PAGE_LOCATION);
						log.error("JAXBException When xml parse info in researchClaimAction method",exception);
				    }
				    ceHeaderList = ceDecisionResultsType.getCeHeader();
		
				    if (ceHeaderList != null && (ceHeaderList.size() > 0)) {
		
					for (int i = 0; i < ceHeaderList.size(); i++) {
					    log.info("ceHeader claim number ------> " + ceHeaderList.get(i).getHealthPlanClaimNumber());
					    XMLGregorianCalendar date = ceHeaderList.get(i).getClaimEditorBeginDatetime();
					    String tranDate = convertXMLGregorianCalendar(date) + " EST";
					    log.info("ceHeader transaction date ------> " + tranDate);
						transactionDate.put(ceHeaderList.get(i).getClaimEditorTransactionId(), tranDate);
					}
		
					request.setAttribute("transactionDate", transactionDate);
					request.setAttribute("ceHeaderType", ceHeaderList);
					response.setRenderParameter("action", "renderAfterAction");
		
				    } else {
						response.setRenderParameter(TrailsResearchConstant.ERROR_VARIABLE, TrailsResearchConstant.NO_CLAIMS_ERROR);
						response.setRenderParameter(TrailsResearchConstant.MVC_PATH, TrailsResearchConstant.VIEW_PAGE_LOCATION);
						log.info("No Response");
				    }
				} else {
				    response.setRenderParameter(TrailsResearchConstant.ERROR_VARIABLE, TrailsResearchConstant.NO_CLAIMS_ERROR);
				    response.setRenderParameter(TrailsResearchConstant.MVC_PATH, TrailsResearchConstant.VIEW_PAGE_LOCATION);
				    log.info("No Response");
				}		
			} catch (AxisFault exception) {
				response.setRenderParameter(TrailsResearchConstant.ERROR_VARIABLE, TrailsResearchConstant.ERROR_MESSAGE);
				response.setRenderParameter(TrailsResearchConstant.MVC_PATH, TrailsResearchConstant.VIEW_PAGE_LOCATION);
				log.error("JAXBException When assign jaxbContext in researchClaimAction method",exception);
			} catch (RemoteException exception) {
				response.setRenderParameter(TrailsResearchConstant.ERROR_VARIABLE, TrailsResearchConstant.ERROR_MESSAGE);
				response.setRenderParameter(TrailsResearchConstant.MVC_PATH, TrailsResearchConstant.VIEW_PAGE_LOCATION);
				log.error("JAXBException When assign jaxbContext in researchClaimAction method",exception);
			}
		}
    }
	
	/**
	 * Convert XMLGregorianCalendar to yyyy-MM-dd hh:mm:ss AM EST.
	 */

    private String convertXMLGregorianCalendar(XMLGregorianCalendar dt) {
    	Calendar calendar = dt.toGregorianCalendar();
    	SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss aaa");
    	formatter.setTimeZone(calendar.getTimeZone());
    	String dateString = formatter.format(calendar.getTime());
    	
    	return dateString;
    }
    
    /**
     * Re-directing to the research decision results page
     */
    @SuppressWarnings (value="unchecked")
    @RenderMapping(params = "action=renderAfterAction")
    public String resultsRenderMethod(RenderRequest request,
                                      RenderResponse response) {
		log.info("custom Render");
		Map<String, String> transactionDate  = (Map<String, String>) request.getAttribute("transactionDate");
		List<CeHeaderType> ceHeader = (List<CeHeaderType>) request.getAttribute("ceHeaderType");
		log.info("ceLineList --->" + ceHeader);
		
		request.setAttribute("ceHeaderType", ceHeader);
		request.setAttribute("transactionDate", transactionDate);
		return "decisionResults";
    }
    
    
    /**
     * 
     * when the user click on close button cancelPage method will be executed.
     */

    @ActionMapping(params = { "cancelaction=researchDecisionCancelAction" })
    
    public void cancelPage(ActionRequest actionRequest,
	    ActionResponse actionResponse) {
    	
    	log.info("Processing Cancel Action for Research Decision");
	    String userName = null;
	    List usersList = null;
		try {
		    User user = PortalUtil.getUser((PortletRequest) actionRequest);
		    if (user != null) {
			    usersList = user.getUserGroups();
		    }
		} catch (PortalException e) {
			log.error("PortalException in cancelPage method", e);
		} catch (SystemException e) {
			log.error("SystemException in cancelPage method", e);
		}
	    ArrayList<String> groupNames = new ArrayList<String>();
	    if (usersList != null) {
	    	int length = usersList.size();
		    for (int i = 0; i < length; ++i) {
		    	
		    	// Ignore the groups that do not have a home page
				if (!((UserGroup)usersList.get(i)).getName().equalsIgnoreCase("CDSUsers") &&
					!((UserGroup)usersList.get(i)).getName().equalsIgnoreCase("Everyone") &&
					!((UserGroup)usersList.get(i)).getName().equalsIgnoreCase("ManagedUsers") &&
					!((UserGroup)usersList.get(i)).getName().equalsIgnoreCase("Multi-Factor") &&
					!((UserGroup)usersList.get(i)).getName().equalsIgnoreCase("PortalAdmin") &&
					!((UserGroup)usersList.get(i)).getName().equalsIgnoreCase("UMGroup")) {
					userName = ((UserGroup) usersList.get(i)).getName();
					groupNames.add(userName);
				}
		    }
	    }
	    HttpServletRequest request = PortalUtil.getHttpServletRequest((PortletRequest) actionRequest);
	    String path = PortalUtil.getCurrentCompleteURL((HttpServletRequest) request);
	    StringBuilder output = new StringBuilder();
	    int count = 0;
	    char[] ch = path.toCharArray();
	    for (int i2 = 0; i2 < ch.length; ++i2) {
			if (ch[i2] == '/') {
			    ++count;
			}
			if (count >= 3)
			    continue;
			output = output.append(ch[i2]);
	    }

	    String landingPage = null;
	    if (usersList == null) {
			
			// Set to the default landing page since the user does not belong to any groups
			landingPage = "/web/guest/home";
	    } else {
	    	

	    	// Get the landing page based on the user's group
			landingPage = com.liferay.portal.kernel.util.PropsUtil.get("default.landing.page.path[" + userName  + "]"); 
			
			// If the user's group is not found use the defaule landing page
			if (landingPage == null) {
				landingPage = com.liferay.portal.kernel.util.PropsUtil.get("default.landing.page.path");
				if (landingPage == null) {
					
					// Set to the default landing page since it is not defined in the properties file
					landingPage = "/web/guest/home";
				}
			}
	    }
	    String pathRedirect = null;
		pathRedirect = output + landingPage;
		log.info(userName + " " + pathRedirect);

		try {
		    actionResponse.sendRedirect(pathRedirect);
		} catch (IOException e) {
			log.error("IOException during redirection in cancelPage method", e);
		}
    }

    @ActionMapping(params = { "newresearchdecisionaction=newResearchDecisionAction" })
    public void newResearchDecisionPage(ActionRequest actionRequest,
	                                    ActionResponse actionResponse) {
    	
    	log.info("Processing New Research Decision Action for Research Decision");
    	
    	boolean privateFlag = true;   // True for a private page
		HttpServletRequest httpRequest = PortalUtil.getHttpServletRequest(actionRequest);
		ThemeDisplay themeDisplay1 = (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
		try {
			Layout layout = LayoutLocalServiceUtil.getFriendlyURLLayout(themeDisplay1.getLayout().getGroupId(), privateFlag, com.liferay.util.portlet.PortletProps.get("research-decision-page-url"));// research decision page 
			PortletURL portletURL = PortletURLFactoryUtil.create(httpRequest,com.liferay.util.portlet.PortletProps.get("research-decision-info-page"),layout.getPlid(), PortletRequest.RENDER_PHASE); // research decision portlet name space
			portletURL.setWindowState(WindowState.MAXIMIZED);
			portletURL.setPortletMode(PortletMode.VIEW);
			actionResponse.sendRedirect(portletURL.toString());
		} catch (IOException e) {
			log.error("IOException in newResearchDecisionPage",e);
		} catch (PortalException e) {
			log.error("PortalException in newResearchDecisionPage",e);
		} catch (PortletModeException e) {
			log.error("PortletModeException in newResearchDecisionPage",e);
		} catch (SystemException e) {
			log.error("SystemException in newResearchDecisionPage",e);
		} catch (WindowStateException e) {
			log.error("WindowStateException in newResearchDecisionPage",e);
		}
	}

    /**
     * TODO: Remove below mock code 
     * @param ceHeaderList
     * @param request
     * @param response
     * @param transactionDate
     */
    private void setDetailsInSuccessMock(List<CeHeaderType> ceHeaderList, ActionRequest request, ActionResponse response,Map<String, String> transactionDate){
    	 try{
			    ceHeaderList  = new ArrayList<CeHeaderType>();
			    CeHeaderType ceHeaderType = new CeHeaderType();
			    ceHeaderType.setBusinessSectorCode("Scode");
			    ceHeaderType.setBusinessSegmentCode((byte)100);
			    GregorianCalendar gcal = (GregorianCalendar) GregorianCalendar.getInstance();
			    XMLGregorianCalendar xgcal = DatatypeFactory.newInstance().newXMLGregorianCalendar(gcal); 
			    ceHeaderType.setClaimEditorBeginDatetime(xgcal);
			    ceHeaderType.setClaimEditorTransactionId("TrId");
			    ceHeaderType.setHealthPlanGroupId("01");
			    ceHeaderType.setHealthPlanIdTypeCode("35");
			    ceHeaderType.setHealthPlanId((byte)100);
			    ceHeaderType.setHealthPlanPreviousClaimNumber("10");
			    ceHeaderType.setMasterPatientId("22");
			    ceHeaderType.setServiceFromDate(xgcal);
			    ceHeaderType.setServiceToDate(xgcal);
			    ceHeaderType.setClaimEditorEndDatetime(xgcal);
			    ceHeaderType.setClaimEditorErrorCode01((byte)100);
			    ceHeaderType.setClaimEditorErrorDescription01("Desc");
			    ceHeaderType.setClaimEditorErrorIndicator((byte) 11);
			    ceHeaderType.setHealthPlanClaimNumber("22");
			    ceHeaderType.setHealthPlanGroupId("HPGId");
			    ceHeaderType.setMasterPatientId("MPId");
			    ceHeaderType.setTrialClaimIndicator((byte)100);
			    ceHeaderList.add(ceHeaderType);
			    transactionDate.put("TrId", "2016/01/01 12:00 PM EST");
    	 	}catch(Exception e){
    	 		log.error("Exception in setDetailsInSuccessMock method", e);
		    }
		    request.setAttribute("transactionDate", transactionDate);
			request.setAttribute("ceHeaderType", ceHeaderList);
			response.setRenderParameter("action", "renderAfterAction");
    }
}
