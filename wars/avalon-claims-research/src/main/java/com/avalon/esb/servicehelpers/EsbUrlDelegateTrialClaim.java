/*
 * Decompiled with CFR 0_110.
 */
package com.avalon.esb.servicehelpers;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;

import com.avalon.trails.research.constants.TrailsResearchConstant;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.StringPool;

public class EsbUrlDelegateTrialClaim {
    
    private static final Log log = LogFactoryUtil.getLog(EsbUrlDelegateTrialClaim.class.getName());
    
    private static EsbUrlDelegateTrialClaim esbUrlDelegate = null;
	private static Properties properties = null;

	/**
	 * @return the properties
	 */
	public static Properties getProperties() {
	    return properties;
	}
	
	/**
	 * @param properties the properties to set
	 */
	public static void setProperties(Properties properties) {
		EsbUrlDelegateTrialClaim.properties = properties;
	}
	
	/**
	 * To load the common.properties files from the server bin directory and to fetch the esb-end point urls.
	 * @return
	 */

    static {
		if (properties == null || properties.size() == 0) {
		    properties = new Properties();
		    esbUrlDelegate = new EsbUrlDelegateTrialClaim();
		    String liferayHome = System.getProperty("LIFERAY_HOME");
		    String propertyLocation = liferayHome + StringPool.SLASH + TrailsResearchConstant.PROPERTIES_FILE;
		    try {
				log.info("EsbUrlDelegateTrialClaim for Trial Claim::propertyLocation: " + propertyLocation);
				properties.load(new FileInputStream(new File(propertyLocation)));
		    } catch (FileNotFoundException exception) {
		    	log.error("FileNotFoundException..warning..Occured..",exception);
			} catch (IOException exception) {
				log.error("IOException..warning..Occured..",exception);
			}
		}
    }

    public static EsbUrlDelegateTrialClaim getEsbUrlDelegate() {
        return esbUrlDelegate;
    }
	
	 /**
	     * Fetching the esb end point urls from the properties object
	     * Setting to the esb end point urls in the map object
	     * 
	     */

   public Map<String, String> getEsbUrl() {
		ConcurrentHashMap<String, String> urls = new ConcurrentHashMap<String, String>();
		try {
		    String serverInstance = System.getProperty("serverInstance");
		    log.info("ServerInstanceAtEsbDelegate :" + serverInstance);
		    log.info("researchDecisionUrl : " + properties.getProperty(new StringBuilder().append(serverInstance).append(".getResearchDecision.endpoint").toString()));
		    log.info("claimEditorUrl : " + properties.getProperty(new StringBuilder().append(serverInstance).append(".getClaimEditor.endpoint").toString()));
		    log.info("trailClaimsPriorAuthUrl : " + properties.getProperty(new StringBuilder().append(serverInstance).append(".getPriorAuth.endpoint").toString()));
		    urls.put("researchDecisionUrl", properties.getProperty(serverInstance + ".getResearchDecision.endpoint"));
		    urls.put("claimEditorUrl", properties.getProperty(serverInstance + ".getClaimEditor.endpoint"));
		    urls.put("trailClaimsPriorAuthUrl", properties.getProperty(serverInstance + ".getPriorAuth.endpoint"));
	
		} catch (Exception exception) {
			log.error("Exception..warning..Occured..",exception);
		}
		return urls;
    }
}
