/**
 * Description
 *		This file contain the controller methods for the Member Search on Trial Claim Page.
 *
 * CHANGE History
 * 		Version 1.0
 * 			Initial version
 *  
 */

package com.avalon.member.search.controller;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.PortletMode;
import javax.portlet.PortletModeException;
import javax.portlet.PortletRequest;
import javax.portlet.PortletSession;
import javax.portlet.PortletURL;
import javax.portlet.RenderRequest;
import javax.portlet.WindowState;
import javax.portlet.WindowStateException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import com.avalon.member.search.beans.MemberDetailsFo;
import com.avalon.member.search.beans.MemberSearchConstants;
import com.avalon.member.search.businessdelegate.MemberSearchBusinessDelegateVo;
import com.avalon.member.search.customexception.CustomException;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Layout;
import com.liferay.portal.kernel.portlet.PortletURLFactoryUtil;
import com.liferay.portal.kernel.service.LayoutLocalServiceUtil;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.WebKeys;

@Controller("memberSearchController")
@RequestMapping(value = "VIEW")
public class MemberSearchController implements MemberSearchConstants {
	
	private static final Log log = LogFactoryUtil.getLog(MemberSearchController.class.getName());

    /**
     * whenever the view page has been clicked the render method will be executed 
     * In this method we are removing the session variables which has been set in avalon-prior-authorization-portlet.
     * It redirects to the view page.
    */
    
    @RenderMapping
    public String viewHomePage(RenderRequest request, Map<String, Object> map) {
        log.info("View Home Page Method");
        try {
			
			// Not needed in Trial Claim
            //PortletSession portletSession = request.getPortletSession();
            //portletSession.removeAttribute("idCardNumber", PortletSession.APPLICATION_SCOPE);
            //portletSession.removeAttribute("firstName", PortletSession.APPLICATION_SCOPE);
            //portletSession.removeAttribute("lastName", PortletSession.APPLICATION_SCOPE);
            //portletSession.removeAttribute("dateOfBirth", PortletSession.APPLICATION_SCOPE);
            //portletSession.removeAttribute("gender", PortletSession.APPLICATION_SCOPE);
            //portletSession.removeAttribute("memberSearchStatus", PortletSession.APPLICATION_SCOPE);
            //map.put("memberDetailsFO", new MemberDetailsFo()); // Setting the commandBean for spring form                                                      
        } catch (Exception nullPointer) {
        	log.error("Exception in viewHomePage method",nullPointer);
        }
        return VIEW_DISPLAY;
    }
    
    /**
     * whenever the search button  has been clicked the findMemberDetails method will be executed 
     * In this method we will fetch the member details and set in session 
     * It will be redirect to the avalon-prior-authorization-portlet.
    */
    @ActionMapping(params = "action=memberSearchAction")
    public void findMemberDetails(ActionRequest actionRequest, 
    		                      ActionResponse actionResponse,
                                  @ModelAttribute("memberDetailsFO") MemberDetailsFo memberDetailsFo) throws IOException, PortletException {
        try {
            log.info("PriorAuthController Start findMemberDetails");

            log.info(" Member Id : " + memberDetailsFo.getMemberId());
            actionRequest.setAttribute("memberIDVal", memberDetailsFo.getMemberId());
            log.info(" HealthPlanGroupValue : "+memberDetailsFo.getHealthPlanGroupValue());
            log.info(" HealthPlan : " + memberDetailsFo.getHealthPlan());
            Map<Integer, MemberDetailsFo> details = new ConcurrentHashMap<Integer, MemberDetailsFo>();
            HttpSession httpSession = PortalUtil.getHttpServletRequest(actionRequest).getSession();
            String memberId = ParamUtil.getString(actionRequest, "memberID");
            String memberFirstName = ParamUtil.getString(actionRequest, "memberFirstName");
            String memberLastName = ParamUtil.getString(actionRequest, "memberLastName");
            String memberDob = ParamUtil.getString(actionRequest, "memberDob");
            String healthPlanGroup = ParamUtil.getString(actionRequest, "healthPlanGroupValue");
		    String healthPlan = ParamUtil.getString(actionRequest, "healthPlan");
            log.info(memberId + ": " + memberFirstName + ": " + memberLastName + ": "+ healthPlan + ": " + memberDob + ":" + healthPlanGroup);
            MemberSearchBusinessDelegateVo priorAuthBussinessDelegateVo = MemberSearchBusinessDelegateVo.getMemberSearchBusinessDelegateVo();
            log.info("Before Calling findMember");
            details = priorAuthBussinessDelegateVo.findMember(memberDetailsFo.getMemberId(), memberDetailsFo.getHealthPlanGroupValue(), memberDetailsFo.getHealthPlan());

            log.info("details : " + details);
            log.info("after Calling findMember details is " + details);
            if (details == null || details.isEmpty()) {
                log.info("details object null Condition");
                actionResponse.setRenderParameter("memberId", memberDetailsFo.getMemberId());
                actionRequest.setAttribute("message","No members were found that match your search criteria. Please update the search and try again.");
            } else if (details != null && !details.isEmpty() && details.size() > 0) {
                if ((!(details.get(0).getMpi().trim().isEmpty()) && ((details.get(0).getMpi()) != null))) {
                    log.info("details object not null and MPI is existing");
                    httpSession.setAttribute("memberdetails", details);// session
                    Map<Integer, MemberDetailsFo> maplist = (Map<Integer, MemberDetailsFo>) httpSession.getAttribute("memberdetails");
                    log.info("MapList: " + maplist);
                    log.info(" If block details : " + details);
                    log.info("memberId ----- > " + memberDetailsFo.getMemberId());
                    actionResponse.setRenderParameter("memberId", memberDetailsFo.getMemberId());
                    actionRequest.setAttribute("details", details);
                } else {
                    log.info("details object not null and MPI is null");
                    actionResponse.setRenderParameter("memberId", memberDetailsFo.getMemberId());
                    actionRequest.setAttribute("message","No members were found that match your search criteria. Please update the search and try again.");
                }
            } else {
                log.info("Server is Down");
                actionResponse.setRenderParameter("memberId", memberDetailsFo.getMemberId());
                actionRequest.setAttribute("message","Error while processing the request. Please contact system administrator.");
            }
            actionResponse.setRenderParameter("mvcPath", "/html/member-details/memberDetails.jsp");
            log.info("PriorAuthController End findMemberDetails");
         } catch (IOException ioe) {
            log.info("PriorAuthController IOException");
            if (ioe.getMessage().equals("No Records")) {
                log.info("PriorAuthController No Records");
                actionResponse.setRenderParameter("memberId", memberDetailsFo.getMemberId());
                actionRequest.setAttribute("message","No members were found that match your search criteria. Please update the search and try again.");
                actionResponse.setRenderParameter("mvcPath", "/html/member-details/memberDetails.jsp");
            } else if (ioe.getMessage().equals("Invalid Member Data")) {
                log.info("PriorAuthController Invalid Member Data");
                actionResponse.setRenderParameter("memberId", memberDetailsFo.getMemberId());
                actionRequest.setAttribute("message", "Invalid Member Data");
                actionResponse.setRenderParameter("mvcPath", "/html/member-details/memberDetails.jsp");
            } else {
                log.info("PriorAuthController Socket Time Out");
                log.error("PriorAuthController Socket Time Out",ioe);
            	
                actionResponse.setRenderParameter("memberId", memberDetailsFo.getMemberId());
                actionRequest.setAttribute("message","Error while processing the request. Please contact system administrator.");
                actionResponse.setRenderParameter("mvcPath", "/html/member-details/memberDetails.jsp");
            }
        } catch (NullPointerException noResponse) {
        	log.error("NullPointerException in findMemberDetails method",noResponse);
        	
            actionResponse.setRenderParameter("memberId", memberDetailsFo.getMemberId());
            actionRequest.setAttribute("message","No members were found that match your search criteria. Please update the search and try again.");
            actionResponse.setRenderParameter("mvcPath", "/html/member-details/memberDetails.jsp");
    	} catch (CustomException exception) {
    		log.error("CustomException in findMemberDetails method",exception);
        	actionResponse.setRenderParameter("memberId", memberDetailsFo.getMemberId());
    	    // To display the error message based on the response code from the cds response
    	    actionRequest.setAttribute("message", exception.getMessage());
    	    actionResponse.setRenderParameter("mvcPath", "/html/jsps/memberDetails.jsp");
        } catch (Exception exception) {
        	log.error("Exception in findMemberDetails method",exception);
        	
        	actionResponse.setRenderParameter("memberId", memberDetailsFo.getMemberId());
            actionRequest.setAttribute("message","Error while processing the request. Please contact system administrator.");
            actionResponse.setRenderParameter("mvcPath", "/html/member-details/memberDetails.jsp");
        }
    }

    /**
     * Whenever user click on hyperlink of the results page  getMemberDetails method will be execute
     */
    @ActionMapping(params = "action=getMemberDetails")
    public void getMemberDetails(ActionRequest actionRequest, ActionResponse actionResponse)
            throws IOException, PortletException {
        HttpServletRequest httpRequest = PortalUtil.getHttpServletRequest(actionRequest);
        HttpSession httpSession = PortalUtil.getHttpServletRequest(actionRequest).getSession();
        ThemeDisplay themeDisplay1 = (ThemeDisplay) actionRequest
                .getAttribute(WebKeys.THEME_DISPLAY);
        Layout layout;
        PortletSession portletSession = actionRequest.getPortletSession();
        try {
            String memberDetailsKeyCode = ParamUtil.getString(actionRequest, "memberDetailsKey");
            log.info("memberDetailsKeyCode : " + memberDetailsKeyCode);
            log.info("We are in getMemberDetails ");
            Map<Integer, MemberDetailsFo> maplist = (Map<Integer, MemberDetailsFo>) httpSession.getAttribute("memberdetails");
            log.info("maplist : " + maplist);
            MemberDetailsFo memberDetailsFo = null;
            log.info("memberdetails" + maplist);
            if ((maplist != null) && (maplist.size() > 0)) {
                log.info("map List inside if block");
                memberDetailsFo = maplist.get(Integer.parseInt(memberDetailsKeyCode));
                log.info("memberDetailsFo : " + memberDetailsFo);
                log.info("memberDetailsFo.setMemberNumber ------> "+ memberDetailsFo.getMemberNumber());
                if ((memberDetailsFo.getMemberNumber() != null)&& !(memberDetailsFo.getMemberNumber().equals(""))) {
                    httpSession.setAttribute("memberDetailsFo", memberDetailsFo);
                    portletSession.setAttribute("idCardNumber", memberDetailsFo.getMemberId(),PortletSession.APPLICATION_SCOPE);
                    portletSession.setAttribute("firstName", memberDetailsFo.getMemberFirstName(),PortletSession.APPLICATION_SCOPE);
                    portletSession.setAttribute("lastName", memberDetailsFo.getMemberLastName(),PortletSession.APPLICATION_SCOPE);
                    portletSession.setAttribute("dateOfBirth", memberDetailsFo.getMemberDob(),PortletSession.APPLICATION_SCOPE);
                    portletSession.setAttribute("gender", memberDetailsFo.getGender(),PortletSession.APPLICATION_SCOPE);
                    portletSession.setAttribute("memberSearchStatus", "true",PortletSession.APPLICATION_SCOPE);
                    log.info("memberDetailsFo : " + memberDetailsFo);
					log.info("Details (Attribute) ------> " + 
						portletSession.getAttribute("idCardNumber",PortletSession.APPLICATION_SCOPE) + ": " +  
						portletSession.getAttribute("firstName",PortletSession.APPLICATION_SCOPE) + ":" +
						portletSession.getAttribute("lastName",PortletSession.APPLICATION_SCOPE) + ":" +  
						portletSession.getAttribute("dateOfBirth",PortletSession.APPLICATION_SCOPE) + ":" +  
						portletSession.getAttribute("gender",PortletSession.APPLICATION_SCOPE) + ":" +
						portletSession.getAttribute("memberSearchStatus",PortletSession.APPLICATION_SCOPE));
					boolean privateFlag = true;
// LOCAL TEST
// privateFlag = false;   // Set for public page
//LOCAL TEST

                    layout = LayoutLocalServiceUtil.getFriendlyURLLayout(themeDisplay1.getLayout().getGroupId(), privateFlag, com.liferay.util.portlet.PortletProps.get("trial-claims-page-url"));
                    PortletURL portletURL = PortletURLFactoryUtil.create(httpRequest,com.liferay.util.portlet.PortletProps.get("trail-claims-info-page"),layout.getPlid(), PortletRequest.RENDER_PHASE);
                    portletURL.setWindowState(WindowState.MAXIMIZED);
                    portletURL.setPortletMode(PortletMode.VIEW);
                    log.info("To String :" + portletURL.toString());
                    actionResponse.sendRedirect(portletURL.toString());
                } else {
                    actionRequest.setAttribute("message", "Invalid Member Data");
                    actionResponse.setRenderParameter("mvcPath", "/html/member-details/memberDetails.jsp");
                }
            } else {
                actionRequest.setAttribute("message", "Invalid Member Data");
                actionResponse.setRenderParameter("mvcPath", "/html/member-details/memberDetails.jsp");
            }
        } catch (com.liferay.portal.kernel.exception.SystemException e) {
        	log.error("SystemException in getMemberDetails method",e);
        } catch (PortalException e) {
        	log.error("PortalException in getMemberDetails method",e);
        }
    }

    
    /**
     * Whenever user click on cancel  button cancelPage method will be executed 
     */
    @ActionMapping(params = "action=cancelPage")
    public void cancelPage(ActionRequest actionRequest, ActionResponse actionResponse) {

    	log.info("Processing Cancel Action for Member Search in Trial Claim");

    	boolean privateFlag = true;   // True for a private page
		HttpServletRequest httpRequest = PortalUtil.getHttpServletRequest(actionRequest);
		ThemeDisplay themeDisplay1 = (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
		try {
			Layout layout = LayoutLocalServiceUtil.getFriendlyURLLayout(themeDisplay1.getLayout().getGroupId(), privateFlag, com.liferay.util.portlet.PortletProps.get("trial-claims-page-url"));// trial claim page 
			PortletURL portletURL = PortletURLFactoryUtil.create(httpRequest,com.liferay.util.portlet.PortletProps.get("trail-claims-info-page"),layout.getPlid(), PortletRequest.RENDER_PHASE); // trial claim portlet name space
			portletURL.setWindowState(WindowState.MAXIMIZED);
			portletURL.setPortletMode(PortletMode.VIEW);
			actionResponse.sendRedirect(portletURL.toString());
		} catch (IOException e) {
			log.error("IOException in cancelPage method",e);
		} catch (PortalException e) {
			log.error("PortalException in viewHomePage method",e);
        } catch (PortletModeException e) {
			log.error("PortletModeException in viewHomePage method",e);
        } catch (SystemException e) {
			log.error("SystemException in viewHomePage method",e);
        } catch (WindowStateException e) {
			log.error("WindowStateException in viewHomePage method",e);
        }
    }
}
