package com.avalon.member.search.beans;

public interface MemberSearchConstants {
    public static final String REASON_FOR_INQUIRY = "P";
    public static final String HEALTH_PLAN_ID = "0010";
    public static final String HEALTH_PLAN_ID_TYPE = "PI";
    public static final String VIEW_DISPLAY = "memberDetails";
    //TODO: Remove below code
    public static final String ENABLED_MOCK_SERVICE = "enabled.mock.service";
}
