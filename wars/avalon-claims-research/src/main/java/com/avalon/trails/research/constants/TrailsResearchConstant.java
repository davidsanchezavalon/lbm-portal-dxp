package com.avalon.trails.research.constants;

/**
 * Description
 *		This file contain the getter and setter methods for the Post Service Review Constants.
 *
 * CHANGE History
 * 		Version 1.0
 * 			Initial version copied from avalon-prior-authorization.
 *  	Version 1.1			03/07/2017
 *  		Added a comstant for the properties file.
 *
 */

/**
 * To set the constants in the trail claims and research decision esb service request. 
 */
public interface TrailsResearchConstant {
	public static final String PROPERTIES_FILE ="lbm-common.properties";

	public static final String HEALTH_PLAN_ID = "0010";
	public static final String HEALTH_PLAN_ID_TYPE_CODE = "PI";
	public static final String VIEW_DISPLAY = "view";
	public static final String ERROR_VARIABLE = "message";
	public static final String ERROR_MESSAGE = "Error while processing the request. Please contact system administrator";
	public static final String MVC_PATH = "mvcPath";
	public static final String VIEW_PAGE_LOCATION = "/html/research-decision/view.jsp";
	public static final String NO_CLAIMS_ERROR = "No claims were found that match your search criteria. Please update and try again.";
	//TODO: Remove below code
    public static final String ENABLED_MOCK_SERVICE = "enabled.mock.service";
}
