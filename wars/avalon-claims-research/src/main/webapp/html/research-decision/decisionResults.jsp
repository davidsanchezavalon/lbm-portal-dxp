<%--
/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

 /**
  * Description
  *		This file contain the Research a Decision Results portlet.
  *
  * CHANGE History
  * 	Version 1.0
  *			Initial version
  * 	Version 1.1
  *			Change the position of the Close and Print button to match requirements.
  *		Version 1.2
  *			Center table text.
  *		Version 1.35			03/09/2018
  *			Use the current year for the copyright and use the symbol.
  *
  */
--%>

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<%@ page import="java.util.ArrayList"%>
<%@ page import="java.util.Calendar"%>
<%@ page import="java.util.List" %>
<%@ page import="com.avalon.lbm.xml.beans.*" %>

<portlet:actionURL var="researchDecisionNewURL">
	<portlet:param name="newresearchdecisionaction" value="newResearchDecisionAction" />
</portlet:actionURL>

<portlet:actionURL var='researchDecisionCancelURL'>
	<portlet:param name="cancelaction" value="researchDecisionCancelAction" />
</portlet:actionURL>

<portlet:defineObjects />

<script>
	<% 
		int currentYear = Calendar.getInstance().get(Calendar.YEAR);
	%>
</script>
	
<aui:script>

	//  Hide all detail rows
	$(".hide_row").hide();

	// Show/hide the detail rows and change the icon
	function toggleRow(elem, idx1, idx2) {
		if (elem.className == "icon-expand-alt") {
			elem.className = "icon-collapse-alt";
		} else {
			elem.className = "icon-expand-alt";
		}

		$(".resultrow" + idx1 + "_" + idx2).toggle();
	}
	
	
	
	YUI().use('aui-node','aui-modal','event',function(Y) {

		/********To process CANCEL button action***************/
		Y.one('.cancelAction').on('click',function(e){
			Y.one('.form-inline').set('action',"<%=researchDecisionCancelURL%>");
			Y.one('.form-inline').submit();
		});

	});  
	

	/********To process new claim search button action***************/
	function showNewResearchDecision(){
		 <%
			// Only use up to '?'
			String[] splitURL = researchDecisionNewURL.split("\\?");
			String newResearchDecisionURL = splitURL[0];
		 %>
		 var url = '<%=newResearchDecisionURL%>';
		 window.location.href = url;
	}
	
</aui:script>

<liferay-ui:panel title="rd.title" collapsible="false" cssClass="panelClass">
	<aui:form name="trialClaimForm" cssClass="form-inline" action="" method="post" commandName="" >
      
		<c:set var="index1" value="${1}"></c:set>
		<c:forEach var="ceHeader" items="${ceHeaderType}">
		<liferay-ui:panel title="ID: ${ceHeader.healthPlanClaimNumber} Transaction Date: ${transactionDate[ceHeader.claimEditorTransactionId]}" collapsible="true" cssClass="panelClass">
			<aui:fieldset label="" id="results-table">
				<aui:container>
					<aui:row>
						<table id="tbl-result-lines" class="table table-bordered" style="width: 100%; border-collapse: collapse; border-spacing: 0;">
							<thead>
					            <tr>
					                <th class="textCenter" ></th>
					                <th class="textCenter" >#</th>
					                <th class="textCenter" >Procedure Code</th>
					                <th class="textCenter" >Decision Type</th>
					                <th class="textCenter" >Decision Rank</th>
					                <th class="textCenter" >Decision Code</th>
					                <th class="textCenter" >Decision Description</th>
					                <th class="textCenter" >Medical Policy Tag</th>
					                <th class="textCenter" >Medical Necessity Criteria</th>
					                <th class="textCenter" >Requested Units</th>
                                    <th class="textCenter" >Approved Units</th>
					                <th class="textCenter" >Pay and Educate</th>
					                <!-- <th>Exclusion Type</th> -->
					            </tr>
					        </thead>
					        <tbody>
					        	<tr class="resultrow${index1}">
					              
					                <c:set var="index2" value="${1}"></c:set>
					                <c:forEach var="details" items="${ceHeader.ceLine}">
									<tr>
										<td class="textCenter" ><i class="icon-expand-alt" onClick="toggleRow(this, ${index1}, ${index2})"></i></td> 
										<td class="textCenter" ><c:out value="${index2}" /></td>    
										<td class="textCenter" ><c:out value="${details.procedureCode}" /></td>
										<td class="textCenter" ><c:out value="${details.decisionTypeDescription}" /></td>
										<c:set var="reasonIndex" value="${0}"></c:set>
										<c:forEach var="reason" items="${details.ceReason}">
											<c:if test="${fn:length(details.ceReason) == 1}">
												<td class="textCenter" ><c:out value="1" /></td>
												<td class="textCenter" ><c:out value="${reason.decisionReasonCode}" /></td> 
												<td class="textCenter" ><c:out value="${reason.decisionReasonDescription}" /></td>
												<td class="textCenter" ><c:out value="${reason.healthPlanPolicyTagId}" /></td>
												<td class="textCenter" ><c:out value="${reason.medicalNecessityCriteriaId}" /></td> 
												<td class="textCenter" ><c:out value="${details.serviceUnitsQuantity}" /></td>
												<td class="textCenter" ><c:out value="${reason.serviceUnitsApprovedQuantity}" /></td>
												<c:if test="${reason.payAndEducateIndicator eq '0' }">
												<td class="textCenter" ><c:out value="N" /></td>
												</c:if>
												<c:if test="${reason.payAndEducateIndicator eq '1' }">
													<td class="textCenter" ><c:out value="Y" /></td>
												</c:if>
													<c:if test="${empty reason.payAndEducateIndicator}">
													<td class="textCenter" ></td>
												</c:if>
												<%-- <td class="textCenter" ><c:out value="${reason.exclusionTypeCode}" /></td>  --%>
											</c:if>
					
											<c:if test="${fn:length(details.ceReason) > 1}">
												<c:if test="${reasonIndex == 0 }">
													<td class="textCenter" ><c:out value="1" /></td>
													<td class="textCenter" ><c:out value="${reason.decisionReasonCode}" /></td>  
													<td class="textCenter" ><c:out value="${reason.decisionReasonDescription}" /></td>
													<td class="textCenter" ><c:out value="${reason.healthPlanPolicyTagId}" /></td>
													<td class="textCenter" ><c:out value="${reason.medicalNecessityCriteriaId}" /></td> 
													<td class="textCenter" ><c:out value="${details.serviceUnitsQuantity}" /> </td>
													<td class="textCenter" > <c:out value="${reason.serviceUnitsApprovedQuantity}" /></td>
													<c:if test="${reason.payAndEducateIndicator eq '0' }">
														<td class="textCenter" ><c:out value="N" /></td>
													</c:if> 
													<c:if test="${reason.payAndEducateIndicator eq '1' }">
														<td><c:out value="Y" /></td>
													</c:if> 
													<c:if test="${empty reason.payAndEducateIndicator}"> 
														<td class="textCenter" ></td>
													</c:if> 
													<%-- <td class="textCenter" ><c:out value="${reason.exclusionTypeCode}" /></td>   --%>
												</c:if>
			        
												<c:if test="${reasonIndex >= 1}">
													<tr class="hide_row resultrow${index1}_${index2}">
													<td colspan="4" class="textCenter" ></td>
													<td class="textCenter" ><c:out value="${reason.sequenceNumber}" /></td> 
													<td class="textCenter" ><c:out value="${reason.decisionReasonCode}" /></td> 
													<td class="textCenter" ><c:out value="${reason.decisionReasonDescription}" /></td>
													<td class="textCenter" ><c:out value="${reason.healthPlanPolicyTagId}" /></td>
													<td class="textCenter" ><c:out value="${reason.medicalNecessityCriteriaId}" /></td> 
													<td class="textCenter" ><%-- <c:out value="${details.serviceUnitsQuantity}" /> --%></td>
													<td><%-- <c:out value="${reason.serviceUnitsApprovedQuantity}" /> --%></td>
													<%-- <c:if test="${reason.payAndEducateIndicator eq '0' }">
														<td class="textCenter" ><c:out value="N" /></td>
													</c:if> --%>
													<%-- <c:if test="${reason.payAndEducateIndicator eq '1' }">
														<td class="textCenter" ><c:out value="Y" /></td>
													</c:if> --%>
													<%-- <c:if test="${empty reason.payAndEducateIndicator}"> --%>
														<td class="textCenter" ></td>
													<%--  </c:if> --%>
														<%-- <td class="textCenter" ><c:out value="${reason.exclusionTypeCode}" /></td> --%>
													</tr>
												</c:if>
											</c:if>
										<c:set var="reasonIndex" value="${reasonIndex+1}"></c:set>
										</c:forEach>
									</tr>
									<c:set var="index2" value="${index2+1}"></c:set>
								</c:forEach>
					        </tbody>
						</table>
					</aui:row>
				</aui:container>
			</aui:fieldset>
		</liferay-ui:panel>
		<c:set var="index1" value="${index1+1}"></c:set>
		</c:forEach>
	
		<aui:fieldset>
			<aui:button-row cssClass="btn-divider">
				<%-- <aui:button name="printBtn" cssClass="pull-left" value="tcr.button.print" disabled="true" />  --%>
				<aui:button name="newDecision" cssClass="pull-right newAction" value="rd.button.new.decision" onClick="javascript:showNewResearchDecision()" />
				<aui:button name="closeBtn" cssClass="btn-gray pull-left cancelAction" type="button" value="tce.button.cancel"/>
			</aui:button-row>
		</aui:fieldset>

	</aui:form>
</liferay-ui:panel>
	
	
<div style="margin-top: 25px; font-weight: bold;">
	CPT &copy;<%= currentYear %> American Medical Association. All rights reserved.
</div>
	