<%--
/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

 /**
  * Description
  *		This file contain the Research a Decision portlet.
  *
  * CHANGE History
  * 	Version 1.0
  *			Initial version
  * 	Version 1.1
  *			Added busy indicator.
  *			Added validation.
  *		Version 1.2
  *			Added cancel button.
  *		Version 1.3				01/12/2018
  *			Disable Search button until all required fields are valid.
  *			When the Search button is disabled show a mouse over tooltip with what is missing.
  *		Version 1.4				06/06/2018
  *			Change the cancel button from a hyperlink to a button.
  *
  */
--%>

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>


<portlet:defineObjects />

<portlet:actionURL var='researchDecisionCancelURL'>
	<portlet:param name="cancelaction" value="researchDecisionCancelAction" />
</portlet:actionURL>

<portlet:actionURL var="researchDecisionNewURL">
	<portlet:param name="newresearchdecisionaction" value="newResearchDecisionAction" />
</portlet:actionURL>

<portlet:actionURL var="researchClaimPageURL">
	<portlet:param name="action" value="researchClaimAction" /> 
</portlet:actionURL>

<script>
	<%
		int HP_CLAIM_LEN = 20;
	%>
</script>

<aui:script> 
	var toolTip = "";

	$(function() {
		
		// Set the initial search button status
		setSearchButtonStatus();
	})
	
	function isValidForm() {
		var returnFlag = false;                   
		var claimId = document.getElementById('<portlet:namespace/>claimId').value;
		claimId = claimId.trim();
		var claimTest = /^[a-zA-Z0-9_]+$/.test(claimId);
		
		// Initialize the tool tip for the search button
		toolTip = "";
		
		if (isValidName(claimId, <%= HP_CLAIM_LEN %>, true) && claimTest){
			returnFlag = true;
			toolTip = "";
		} else {
			returnFlag = false;
			toolTip = "The HP Claim Number is not valid";
		}

		return returnFlag;
	} 

	function setSearchButtonStatus() {
		
		// Disable the search button until all the fields are valid
		if (isValidForm()) {
			$("#<portlet:namespace/>search").removeAttr("disabled");
		} else {
			$("#<portlet:namespace/>search").attr("disabled", "disabled");
		}
		
		// Add the mouse over text for the search button
		$("#<portlet:namespace/>search").prop("title", toolTip);
	}

	YUI().ready('aui-node','event',
		function(Y) {
		
			var msg = '<%=renderRequest.getParameter("message")%>';
	
			if(msg=='null')	{
			   document.getElementById("errorText").style.display = 'none';  
			} else {
				document.getElementById("errorText").innerHTML =  msg;
			}
		}
	);

	/********To process new claim search button action***************/
	function showNewResearchDecision(){
		 var url = '<%=researchDecisionNewURL.toString()%>';
		 window.location.href = url;
	}
	
	YUI().use('aui-node','aui-modal','event',function(Y) {
	
		/********To process CANCEL button action***************/
		Y.one('.cancelAction').on('click',function(e){
			Y.one('.form-inline').set('action',"<%=researchDecisionCancelURL%>");
			Y.one('.form-inline').submit();
		});
	
	});

</aui:script> 

<div id="busy_indicator" style="display: none">
	<img src="<%=request.getContextPath()%>/images/busy-spinner.gif" alt="Busy indicator">
</div>

<liferay-ui:panel title="fc.title" collapsible="false">
	<aui:form name="researchDecisionForm" cssClass="form-inline" action="${researchClaimPageURL}" method="post" commandName="researchDecisionFO"  onChange="javascript:setSearchButtonStatus()">
		<aui:fieldset>
			<aui:container>
				<aui:row>
					<aui:col span="6">
				    	<aui:input type="text" label="fc.label.hp.claim.number" class="form-control" id="claimId" maxlength="<%= HP_CLAIM_LEN %>" name="claimId" value='${researchDecisionFO.claimId}'>
							<aui:validator name="required" errorMessage="Please enter valid  HP Claim Number." />
							<aui:validator name="custom" errorMessage="Please enter valid  HP Claim Number.">//adding custom validation since default validation is not working in an order.alphanum is being executed before required validation
								function (val, fieldNode, ruleValue){
									var validClaim = false;
									var matchClaim = /^[a-zA-Z0-9_]+$/.test(val);
									
									if(matchClaim){
										validClaim = true;
									}
									return validClaim;
								}
							</aui:validator>
				    	</aui:input>
					</aui:col>
					<aui:col span="6">
					  <aui:input type="hidden" value="401_PI" name='healthPlanID_TypeValue' />
					  <aui:input type="text" label="fc.label.health-plan-id" style="height:30px" resizable="false" value='BCBS - South Carolina' readonly="true" name="healthPlan" />
					</aui:col>
				</aui:row>
			</aui:container>
		</aui:fieldset>
	
		<aui:fieldset>
			<aui:button-row cssClass="btn-divider">
    			<aui:button name="closeBtn" cssClass="btn-gray pull-left cancelAction" type="button" value="rd.button.cancel" />
				<aui:button name="search" cssClass="pull-right" type="submit" value="fc.button.search" onclick="javascript:checkValidForm()" /> 
			</aui:button-row>
		</aui:fieldset>
	</aui:form>
</liferay-ui:panel>

<div class="aui-message aui-message-error"  style="text-align:center">
	<span id="errorText" style="color:red"> </span>
</div>
