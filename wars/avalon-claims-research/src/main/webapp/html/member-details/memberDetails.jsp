
<%-- 
 /**
  * Description
  *		This file contain the Member Search portlet for Prior Auth.
  *
  * CHANGE History
  * 	Version 1.0
  *			Converted to Alloy UI.
  *		Version 1.1
  *     	Removed JQuery model dialog,fixed LJR-364,365,423 tickets
  *   		dialog-service div is added,onclick find member checkValidForm() method is added,
  *				isValidForm method  idacard number id is corrected to memberID
  * 	Version 1.2
  *     	Added Cancel button
  * 	Version 1.3						05/24/2018
  *     	Added data-senna-off="true" to the link in the results table.
  *		Version 1.4						06/06/2018
  *			Change the cancel button from a hyperlink to a button.
  *		Version 1.5						07/16/2018
  *			Move the label to the left of the field.
  *
  */
--%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui"%>
<%@ taglib uri="http://liferay.com/tld/portlet" prefix="liferay-portlet"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<portlet:defineObjects />
<portlet:actionURL var="actionURLByDetailsTagURL">
	<portlet:param name="action" value="memberSearchAction" />
</portlet:actionURL>
<portlet:actionURL var="actionURLGetMemberDetails">
	<portlet:param name="action" value="getMemberDetails" />
</portlet:actionURL>
<portlet:actionURL var="intakeReviewpageURL">
	<portlet:param name="action" value="intakeReviewAction" />
</portlet:actionURL>
<portlet:actionURL var="cancelURLByDetailsTagURL">
	<portlet:param name="action" value="cancelPage" />
</portlet:actionURL>
<liferay-portlet:renderURL var="redirectURL">
	<liferay-portlet:param name="mvcPath"
		value="/html/member-details/memberDetails.jsp" />
</liferay-portlet:renderURL>
<head>
<style type="text/css">
	input [type=text]::-ms-clear {
		display: none;
		width: 0px;
		height: 0px;
	}
	
	input [type=text]::-ms-reveal {
		display: none;
		width: 0;
		height: 0;
	}
	</style>
	<link rel="stylesheet" href="<%=request.getContextPath()%>/css/main.css">
	<style type="text/css">
	.mandatory {
		color: red;
	}
	
	.show_hideorder {
		color: red;
	}
	
	.clear-both {
		clear: both;
	}
	
	.errmsg {
		margin-bottom: 5px;
	}
	
	.col-fstRow {
		position: relative;
		float: left;
		margin-right: 10px;
		left: 147px;
		width: 550px;
	}
	
	.col-eachDivDet {
		position: relative;
		float: left;
		margin-right: -40px;
		width: 350px;
	}
	
	.col-eachDivWidth {
		width: 175px;
	}
	
	th {
		background-color: gray;
		color: white;
	}
	
	table, th, td {
		border: 1px solid black;
		border-collapse: collapse;
		text-align: center;
	}
	
	th, td {
		padding: 5px;
	}
	
	.prorAuth-borderRow {
		width: 98%;
		border-bottom: 1px solid black;
		height: 20px;
		margin-right: 0px !important;
		margin-left: 0px !important;
		left: 1%;
		position: relative;
	}
</style>

<%-- <link rel="stylesheet" href="<%=request.getContextPath()%>/css/jquery-ui.css">
	<link rel="stylesheet" type="text/css" media="screen"
	href="<%=request.getContextPath()%>/css/datepicker.css" />
<script src="<%=request.getContextPath()%>/js/custom_jquery-1.10.2.js"></script>
<script src="<%=request.getContextPath()%>/js/jquery-ui.js"></script> --%>

<div id="busy_indicator" style="display: none">
	<img src="<%=request.getContextPath()%>/images/busy-spinner.gif"
		alt="Busy indicator">
</div>
<liferay-ui:panel title="Member Information" collapsible="false">
	<aui:form name="intakeReviewDetails" action='<%=actionURLByDetailsTagURL%>' method="post" commandname="memberDetailsFO">
		<aui:fieldset>
			<aui:container>
				<aui:row>
					<aui:col span="6">
						<aui:input type="text" inlineLabel="true" class="form-control" id="memberID" maxlength="17" label="ID Card Number:" name="memberId" value='<%=request.getAttribute("memberIDVal")%>'>
							<aui:validator name="required" errorMessage="Please enter valid ID Card Number" />
							<aui:validator name="alphanum" errorMessage="Please enter valid ID Card Number" />
						</aui:input>
					</aui:col>
					<aui:col span="6">
						<aui:input type="hidden" value="123456789" name='healthPlanGroupValue' />
						<aui:select name="healthPlan" inlineLabel="true" label="Health Plan:" cssClass="span7" required="true" showRequiredLabel="true">
							<aui:option value="0010">BCBS &ndash; South Carolina</aui:option>
						</aui:select>
					</aui:col>
				</aui:row>
			</aui:container>
		</aui:fieldset>
		<aui:fieldset>
			<aui:button-row cssClass="btn-divider">
				<aui:button name="submitMemberSearch" cssClass="pull-right" type="submit" primary="true" value="Find Member" onclick="checkValidForm()" />
     			<aui:button name="cancelMemberSearchInfo" cssClass="btn-gray pull-left" type="button" value="Cancel" onclick="closePortlet()" />
			</aui:button-row>
		</aui:fieldset>
	</aui:form>
</liferay-ui:panel>
<c:if test="${fn:length(details) > 0}">
	<liferay-ui:panel title="Results" collapsible="false">
		<div class="row">
			<!-- <h4 class="text-center">Results</h4> -->
		</div>
		<!-- <div style="text-align:left;">Results</div>  -->
		<table
			style="margin: 0px auto; position: relative; width: 800px; height: 50px">
			<tr>
				<th>ID Card Number</th>
				<th>Group ID</th>
				<th>First Name</th>
				<th>Last Name</th>
				<th>Date Of Birth</th>
				<th>Gender</th>
			</tr>
			<c:forEach var="details" items="${details}">
				<tr>
					<td><a data-senna-off="true" href="<portlet:actionURL> 
							<portlet:param name="action" value="getMemberDetails" /> 
							<portlet:param name="memberDetailsKey" value="${details.key}" />
						</portlet:actionURL>">
						<c:out value="${details.value.memberId}" />
					</a></td>
					<td><c:out value="${details.value.groupId}" /></td>
					<td><c:out value="${details.value.memberFirstName}" /></td>
					<td><c:out value="${details.value.memberLastName}" /></td>
					<td><c:out value="${details.value.memberDob}" /></td>
					<td><c:out value="${details.value.gender}" /></td>
				</tr>
			</c:forEach>
		</table>
	</liferay-ui:panel>
</c:if>
<div class="dialog-service" title="Member Service"
	style="text-align: center; color: red;">${message}
</div>
<div class="yui3-skin-sam">
	<div id="modal"></div>
</div>

<aui:script>
var errorHandler = "<c:out value='${message}'/>"; 
YUI().ready('aui-node','event',
	function(Y) {
		if(Y.one('#heading span')){	
			var portletTitle = Y.one('#heading span').getHTML();
			Y.one('#heading span').setHTML('Member Selection');
		}		
	});	
</aui:script>
	

<script>
 function closePortlet() {
    var url='<%=cancelURLByDetailsTagURL.toString()%>';
    window.location.href = url;
 }
 
 function callCancel(evt){
	var cinfo = confirm("Do you want to cancel the search?");
	if(cinfo){
		var url = '<%=cancelURLByDetailsTagURL.toString()%>';
		var x=document.getElementsByTagName("form");
		x[0].action=url;
		x[0].submit();
	}else{
		evt.preventDefault();
	}
 } 
 function sendMemberDetails(memberKey){
	var url = '<%=cancelURLByDetailsTagURL.toString()%>';
	var x = document.getElementsByTagName("form");
	x[0].action = url;
	x[0].submit();
	// alert("memberKey: "+memberKey);
	return false;
 }
 
 function isValidForm() {
	var formValid = false;//false
	var idCardNumber = document
			.getElementById('<portlet:namespace/>memberID').value;
	var cardLen = 11;
	var exp = /^([a-zA-Z0-9]+)$/;
	if ((idCardNumber == null) || (idCardNumber === '')
			|| (idCardNumber.length === 0) || !(idCardNumber.match(exp))) {
		formValid = false;
	} else {
		formValid = true;
	}
	return formValid;
 }
</script>