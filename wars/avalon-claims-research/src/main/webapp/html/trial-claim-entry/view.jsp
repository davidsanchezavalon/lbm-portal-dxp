<%--
/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

 /**
  * Description
  *		This file contain the Trial Claim Entry portlet.
  *
  * CHANGE History
  *		Version 1.0
  *			Initial version
  *		Version 1.1
  *			Changed to match the latest requirements.
  *		Version 1.2
  *			Added busy indicator.
  *			Added validation.
  *		Version 1.3
  *			Removed from and to date.
  *			Removed hidden procedure modifier row and added them to the claim line row.
  *		Version 1.4
  *			Add procedure code modifiers as separate columns.
  *			Fixed column width in the Claim Line table.
  *		Version 1.5
  *			Added namespace to input fields in claim lines table.Added a hidden input field.
  *		Version 1.6
  *			Added Cancel button action
  *		Version 1.7
  *			Revised design so the name attribute for the row number, date of service, code. and description columns will remain sequential.
  *		Version 1.8
  *			Corrected sequence for units and place of service fields using above design
  *		Version 1.9  
  *			Removed the delete icon from the first claim line.  You should not be able to delete it.
  *			Fixed Related Diagnosis getting removed when a new claim line is added.
  *			Added validation for Date of Service, procedure code and units.
  *			Added validation for first procedure code and units required.
  *			Convert the diagnosis and procedure code to upper case.
  *		Version 1.10  
  *			Fixed claim line table width.
  *			Fixed validation messages for the Claim Lines
  *		Version 1.11  
  *			Disable the submit button until the form is valid.
  *			Added duplicate diagnosis code check.
  *		Version 1.12 
  *			Fixed the problem with the related diagnosis selections getting unselected when adding more than 3 claim lines.
  *			Do not add diagnosis codes that fail validation to related diagnosis.
  *			Added testing  for duplicate procedure code modifiers in the same claim line.
  *			Added testing for duplicate procedure codes.
  *		Version 1.13 
  *			Added error messages for first claim lines.
  *		Version 1.14 
  *			Validate for only digits in TIN and NPIs.
  *		Version 1.15 
  *			Make the related diagnosis manditory for the first row and any row with data.
  *		Version 1.16 
  *			Fixed problems with read only entry tables.
  *			Fixed problem with expand/collapse for results table.
  *		Version 1.17 
  *			Validate for unit > 0.
  *			Validate for original claim number < 20 characters.
  *			Removed the related diagnosis when the diagnosis code is changed or deleted.
  *		Version 1.18 
  *			Make original claim number checkbox read only when the results screen is included.
  *		Version 1.19 
  *			Added the confirmation dialog.
  *			Changed maximum related diagnosis to 4.
  *			Fixed duplicate procedure code check.
  *		Version 1.20 
  *			Added age and gender when the exclude member checkbox is checked.
  *		Version 1.21 
  *			Added member search.
  *			Added primary diagnosis for each claim line.
  *			Added duplicate procedure code check to isValidForm.
  *		Version 1.22 
  *			Put N/A in Actions column of first diagnosis line and claim line.
  *			Added a buton on the results screen to start a new Trial Claim.
  *			Display error descriptions in red.
  *			Change an error message for units.
  *			Move exclude member checkbox before all fields.  Hide if member search is selected.
  *			Changed checkboxes to html instead of AUI.
  *		Version 1.23
  *			Center table text.
  *		Version 1.24
  *			Do not display secondary codes with no decision code.
  *		Version 1.25
  *			Default blue card status to "No Blue Card".
  *		Version 1.26
  *			Fixed the busy spinner fo IE.
  *		Version 1.27
  *			Change Primary Diagnosis Code to 1st Diagnosis Code.
  *		Version 1.28
  *			Handle ENTER pressed in diagnosis code field.
  *			Make the billing provider TIN optional.
  *			Display the correct requested units for duplicate procedure codes with different units.
  *		Version 1.29
  *			Use the date picker for IE and firefox.
  *			Added a disclaimer to the results screen.
  *		Version 1.30
  *			Switch all HTML DOM to jQuery.
  *		Version 1.31
  *			Added the REV code to the Claim Lines.
  *			Added a popup to show any error returned.
  *		Version 1.32
  *			Allow 3 digit REV codes.
  *		Version 1.33
  *			Added Unknown to gender.
  *		Version 1.34			01/12/2018
  *			Added additional validation to isValidForm
  *			When the Submit Claim button is disabled show a mouse over tooltip with what is missing.
  *		Version 1.35			03/09/2018
  *			Use the current year for the copyright and use the symbol.
  *		Version 1.36			06/06/2018
  *			Change the cancel button from a hyperlink to a button.
  *		Version 1.37			06/14/2018
  *			Add a method to align the AUI fields.
  *		Version 1.38			07/31/2018
  *			Convert the numeric Decision Type in the response to the text value.
  *		Version 1.39			08/03/2018
  *			Added the REV code to the result table.
  *
  */
--%>


<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ page import="java.util.ArrayList"%>
<%@ page import="java.util.Calendar"%>
<%@ page import="java.util.List"%>
<%@ page import="com.liferay.portal.kernel.language.LanguageUtil"%>

<portlet:defineObjects />

<portlet:actionURL var="trailClaimPageURL">
	<portlet:param name="action" value="trailClaimDetailsAction" />
</portlet:actionURL>

<portlet:resourceURL var='getUsers' id='getUsers' />

<portlet:actionURL var="trailClaimsNewURL">
	<portlet:param name="newtrialclaimaction" value="newTrailClaimsAction" />
</portlet:actionURL>

<portlet:actionURL var='trailClaimsCancelDisclaimerURL'>
	<portlet:param name="canceldisclaimeraction" value="trailClaimsCancelDisclaimerAction" />
</portlet:actionURL>

<portlet:actionURL var='trailClaimsCancelURL'>
	<portlet:param name="cancelaction" value="trailClaimsCancelAction" />
</portlet:actionURL>

<portlet:resourceURL var="getProcedureDescription"
	id="getProcedureDescription" />

<portlet:actionURL var="memberSearchPageURL">
	<portlet:param name="action" value="trailClaimMember" />
</portlet:actionURL>

<style>
	.content {
	    white-space: pre;
	}
</style>

<script>
	<% 
		int currentYear = Calendar.getInstance().get(Calendar.YEAR);

		String EXCLUDE_MEMBER_STR = LanguageUtil.get(request, "tce.label.search.member");
		String SECONDARY_CLAIM_STR = LanguageUtil.get(request, "tce.label.secondary.claim");
		
	    //String EXCLUDE_MEMBER_STR = LanguageUtil.get(request.getLocale(), "tce.label.search.member");
		//String SECONDARY_CLAIM_STR = LanguageUtil.get(request.getLocale(), "tce.label.secondary.claim");
		String HIDDEN_AGE = "978";
	
		int DIAGNOSIS_LEN = 10;
		int PROC_CODE_LEN = 5;
		int MODIFIER_LEN = 2;
		int REVISION_CODE_LEN = 4;
		int UNIT_LEN = 5;
		
		int TIN_LEN = 9;
		int NPI_LEN = 10;
		int CLAIM_NUM_LEN = 20;
		int AGE_LEN = 3;
	%>
</script>

<aui:script>
	var toolTip = "";
	var errorMsg = "<c:out value='${message}'/>"; 
	
	var browserStr = getBrowser();
	var nameSpaceAttribute = '<portlet:namespace/>';
	var excludeMemberFlag = true;
	
	var diagErrStr = "Please enter a valid diagnosis code";
	var procErrStr = "Please enter a valid procedure code";

	$(function() {
		var numRows = numTableRows('tbl-claim-lines');
		
		// Update the number of lines
		$('.maxClaimLines').val(numRows);

		$(".diagsSelect").select2({ width: '100%' })
		
		// Add blank line to the first primary diagnosis dropdown
		populatePrimaryDiagnosis('primaryDiagnosis' + 1);
		
		initializeErrors();
		
		//  Hide all error messages
		$('.error1').hide();
		$('.error2').hide();
		$('.error3').hide();
		$('.error4').hide();
		$('.error5').hide();
		$('.error6').hide();
		$('.error7').hide();
		$('.error8').hide();
		$('.error9').hide();
		$('.error10').hide();
		$('.error11').hide();
		$('.error12').parents("tr").hide();
		
		// Convert class toUppercase to upper case characters
		$('.toUppercase').focusout(function() {
			this.value = this.value.toLocaleUpperCase();
		});
		
		// Display the missing fields for the first Claim line
		isValidClaimLine(1);
		
		// Process the exclude member checkbox 
		setExcludeMemberStatus();
		setSecondaryClaimStatus();
		
		// Show date picker for IE and Firefox
		if ((browserStr == "unknown") || (browserStr == "IE") || (browserStr == "Firefox")) {
		   	var numRows = numTableRows('tbl-claim-lines');
		
			for (var i = 1; i <= numRows; i++ ) {
				$("#<portlet:namespace/>procedureDateOfService" + i).datepicker({
					dateFormat: 'mm/dd/yy',
					changeMonth: true,
					changeYear: true
				});
				$("#<portlet:namespace/>procedureDateOfService" + i).attr("placeholder", "mm/dd/yyyy");
				$("#<portlet:namespace/>procedureDateOfService" + i).css({
					"background" : "url('<%=request.getContextPath()%>/images/icondatepicker.png') no-repeat",
					"background-position-x" : "100%",
					"background-position-y" : "50%"
				});
			}
		}
		
		// Check for an error message
		errorDialog();
		
		// Set the submit claim button after all the other variables have been initialized.
		setSubmitButtonStatus();
		
		setDiagnosisDescWidth();
	});
	
	function setDiagnosisDescWidth() {
		
		// Set the length of the diadnosis descriptions to 75% of the table width
		var diagTableWidth = parseInt($("#tbl-diagnosis-lines").css("width")) * 0.75;
		
		for (var i = 1; i <= maxDiagnosisRows; i++ ) {
			if ($("#<portlet:namespace/>diaDesc" + i).val() == diagErrStr) {
				$("#<portlet:namespace/>diaDesc" + i).css("cssText", "width:" + diagTableWidth + "px !important;border-color:red; color:red");
			} else {
				$("#<portlet:namespace/>diaDesc" + i).css("cssText", "width:" + diagTableWidth + "px !important");
			}
		}
	}
	
	function getNamespace() {
		return('<portlet:namespace/>');
	}
	
	// Disable the ENTER key
	$(document).keypress(
		function(event) {
			if (event.which == '13') {
		        event.preventDefault();
			}
		}
	);
	
	function initializeErrors() {
	
		// Add the error messages
		$('.error1').html('Date must be entered in mm/dd/yyyy format.');
		$('.error2').html('Date of Service cannot be before 4/1/2016.');
		$('.error3').html('Must be 5 alphanumeric characters.');
		$('.error4').html('Please enter only digits.');
		$('.error5').html('Must be alphanumeric characters.');
		$('.error6').html('Cannot enter duplicate Procedure Code.');
		$('.error7').html('Cannot enter duplicate Procedure Code Modifier.');
		$('.error8').html('At least one diagnosis codes is required.');
		$('.error9').html('Units must be greater than 0.');
		$('.error10').html('Select a diagnosis code.');
		$('.error11').html('Must be at least 3 or up to 4 numeric characters.');
		$('.error12').html('You must enter a valid Procedure Code or a valid REV code.');
	}
	
	// Display any error message
	function errorDialog() {
		
		if (errorMsg != "") {
			errorMsg = decode(errorMsg); 
			
			$("#errorDialog").dialog({
				modal: true,
				resizable: false,
				width: 450,
				buttons: {
					"OK": function() {
						$(this).dialog("close");
					}
				}
			});
			$("#errorDialog").html(errorMsg);
			$("#errorDialog").show();
		}
	}
	 
	function isValidForm() {
		var returnFlag = false;
		var providerInfoBillingTIN = $("#<portlet:namespace/>providerInfoBillingTIN").val();
		var providerInfoBillingNPI = $("#<portlet:namespace/>providerInfoBillingNPI").val();
		var providerInfoRenderingNPI = $("#<portlet:namespace/>providerInfoRenderingNPI").val();
		var additionalCriteriaOriginalNumber = $("#<portlet:namespace/>additionalCriteriaOriginalNumber").val();
		
		// Test for results screen
		var memberAge = "0";
		var memberTestGender = "U";
		<c:choose>
		<c:when test="${fn:length(diagnosis) > 0}">
		memberAge = $("#<portlet:namespace/>memberAge_r").val();
			memberTestGender = $("#<portlet:namespace/>memberTestGender_r").prop("selectedIndex");
		</c:when>
			<c:otherwise>
				memberAge = $("#<portlet:namespace/>memberAge").val();
				memberTestGender = $("#<portlet:namespace/>memberTestGender").prop("selectedIndex");
			</c:otherwise>
		</c:choose>
	
		// Initialize the tool tip for the search button
		toolTip = "";

		// Check age if the member exclude checkbox is checked
		var ageFlag = false;
		if ($("#<portlet:namespace/>memberSearchStatus").val() == "false") {
			if (isValidDigit(memberAge, <%= AGE_LEN %>, true, false) && (memberTestGender > 0)) {
				ageFlag = true;
			}
		} else {
			ageFlag = true;
		}
		
		// Check for duplicate diagnosis codes
		var duplicateDiagnosisFlag = isAnyDuplicateDiagnosis();

		// Check for duplicate procedure codes
		var duplicateProcedureFlag = false;
		var duplicateDisplayCss = $('.error6').css("display");
		if ((duplicateDisplayCss != "none") && (duplicateDisplayCss == "block")) {
			duplicateProcedureFlag = true;
		}

		if (isValidDigit(providerInfoBillingTIN, <%= TIN_LEN %>, false, true) && isValidDigit(providerInfoBillingNPI, <%= NPI_LEN %>, false, true) && 
			isValidDigit(providerInfoRenderingNPI, <%= NPI_LEN %>, false, true) && isValid(additionalCriteriaOriginalNumber, <%= CLAIM_NUM_LEN %>, false, false) &&
			isValidDiagnosisCodes() && isValidClaimLines() && ageFlag && !duplicateDiagnosisFlag && !duplicateProcedureFlag) {
			returnFlag = true;
		} else {
			
			// Test for duplicate diagnosis code
			if (duplicateDiagnosisFlag) {
				addToToolTip("There are duplicate diagnosis codes");
			}
			
			// Test for duplicate procedure code
			if (duplicateProcedureFlag) {
				addToToolTip("There are duplicate procedure codes");
			}
			
			// Test the age
			if (!isValidDigit(memberAge, <%= AGE_LEN %>, true, false)) {
				addToToolTip("The Age is not valid");
			}
			
			// Test the gender
			if (memberTestGender == 0) {
				addToToolTip("The Gender is not valid");
			}

			if (!isValidDigit(providerInfoBillingTIN, <%= TIN_LEN %>, false, true)) {
				addToToolTip("Billing Provider TIN is not valid");
			}
					
			if (!isValidDigit(providerInfoBillingNPI, <%= NPI_LEN %>, false, true)) {
				addToToolTip("Billing Provider NPI is not valid");
			}
			
			if (!isValidDigit(providerInfoRenderingNPI, <%= NPI_LEN %>, false, true)) {
				addToToolTip("Rendering Provider NPI is not valid");
			}
			
			if (!isValid(additionalCriteriaOriginalNumber, <%= CLAIM_NUM_LEN %>, false, false)) {
				addToToolTip("Primary Claim Number is not valid");
			}
			
			if (!isValidDiagnosisCodes()) {
				addToToolTip("There are diagnosis codes that are not valid");
			}
			
			if (!isValidClaimLines()) {
				addToToolTip("There are claim lines that are not valid");
			}
		}
		
		return returnFlag;
	}
	
	function addToToolTip(str) {
		
		// Test for a new line needed
		if (toolTip.length != 0) {
			toolTip += "\n";
		}
		toolTip += str;
	}

	function setSubmitButtonStatus() {

		// Disable the search claim button until all the fields are valid
		if (isValidForm()) {
			$("#<portlet:namespace/>submitClaim").removeAttr("disabled");
		} else {
			$("#<portlet:namespace/>submitClaim").attr("disabled", "disabled");
		}
		
		// Add the mouse over text for the search button
		$("#<portlet:namespace/>submitClaim").prop("title", toolTip);
	}
		
	function isAnyDuplicateDiagnosis() {
		var dupFlag = false;
		
		for (var i = 1; i <= maxDiagnosisRows; i++ ) {
			if ($("#<portlet:namespace/>diaDesc" + i).val() == "") {
				break;
			} else {
				var nextVal = $("#<portlet:namespace/>diaCode" + i).val();
				if (isDuplicateDiagnosis(nextVal, i)) {
					dupFlag = true;
					break;
				}
			}
		}
		return dupFlag;
	}
	
	function isDuplicateDiagnosis(val, idx) {
		var dupFlag = false;
		var cnt = 0;
		
		// Strip period when not on results screen
		<c:choose>
			<c:when test="${fn:length(diagnosis) == 0}">
				val = val.toLocaleUpperCase().replace(/\./g, '');
			</c:when>
		</c:choose>

		// Loop through all of the valid codes
		$('.showDiagRow').find('input.diaCode').each(function() {
			if (++cnt != idx) {
		 		var nextVal = $('#<portlet:namespace/>diaCode' + cnt).val();
		
		 		// Strip period
		 		var nextVal = nextVal.replace(/\./g, '');
		
		 		// Skip blank diagnosis codes
		 		if (nextVal != "") {
		 			if (nextVal == val) {
			 			dupFlag = true; 
			 		}
		 		}
			}
		});
		return dupFlag;
	}

	function isValidDiagnosisCodes() {
		var validFlag = true;
		
		// Return valid for the results screen
		<c:choose>
			<c:when test="${fn:length(diagnosis) == 0}">
				var firstCode = $("#" + getNamespace() + "diaCode1").val();
				var firstDesc = $("#" + getNamespace() + "diaDesc1").val();
			
				// Validate first diagnosis code
				if ((firstCode.length == 0) || (firstDesc == invalidDiagnosisMsg)) {
					validFlag = false;
				} else if ($('.showDiagRow').find('input').hasClass('error-field')) {
					validFlag = false;
				} else {
			
					// Check for diagnosis with error
					if ($('.showDiagRow').find('input.error-field').length > 0) {
						validFlag = false;
					}
				}
			</c:when>
		</c:choose>

		return validFlag;
	}

	var validationInProcessFlag = false;
	function checkDuplicateDiagnosis(val, idx) {
		var dupFlag = false;
		var oldVal = $('.hidden_diagcode' + idx).val();
		var cnt = 0;
		
		if (val != "") {
		
			// Loop through all of the valid codes
			$('.showDiagRow').find('input.diaCode').each(function() {
				if (++cnt != idx) {
					var nextVal = $('#<portlet:namespace/>diaCode' + cnt).val();
		
					// Skip blank diagnosis codes
					if (nextVal != "") {
						
						// Include validation for decimal points in the code
						if ((nextVal == val) || (nextVal.replace(/\./g, '') == val.replace(/\./g, ''))) {
							dupFlag = true;
							// Validate the matching code
							if (!validationInProcessFlag) {
			
								// Stop circular validations
								validationInProcessFlag = true;
					 			// If this is not from the description check
						 		// Force validation of the duplicated diagnosis code
					 			$('#<portlet:namespace/>diaCode' + cnt).focus();
					 			$('#<portlet:namespace/>diaCode' + cnt).blur();
			
					 			validationInProcessFlag = false;
							}
				 		}
				 	}
				}
			});
		
			if (oldVal != null) {
		
				// Validate against old value to remove duplicate messages
				if (oldVal != "") {
					cnt = 0;
					  
					// Loop through all of the valid codes
					$('.showDiagRow').find('input.diaCode').each(function() {
						if (++cnt != idx) {
							var nextVal = $('#<portlet:namespace/>diaCode' + cnt).val();
							
							// Include validation for decimal points in the code
							if ((nextVal == oldVal) || (nextVal.replace(/\./g, '') == oldVal.replace(/\./g, ''))) {
								if (!validationInProcessFlag) {
					
									// Stop circular validations
									validationInProcessFlag = true;
				
									// If this is not from the description check
									// Force validation of the duplicated diagnosis code
									$('#<portlet:namespace/>diaCode' + cnt).focus();
									$('#<portlet:namespace/>diaCode' + cnt).blur();
					
									validationInProcessFlag = false;
								}
							}
						}
					});
				}
			}
		}

		// Update the submit claim button status based on any duplicate diagnosis codes
		setSubmitButtonStatus();
		
		return dupFlag;
	}
	
	function checkDuplicateProcedure(val, idx) {
		var dupFlag = false;
		var err6 = ".error6-" + idx;
		var lineSelected = 1;
		var numRows = numTableRows('tbl-claim-lines');
		
		val = val.toLocaleUpperCase();
		if (val != "") {
		
			// Loop throught all the rows 
			for (var i = 1; lineSelected <= numRows; i++ ) {
				var procedureCodeItem = $('.code_procrow' + i).length;
			
				// Was the row found?
				if (procedureCodeItem > 0) {
					if (i != idx) {
						var nextVal = $('.code_procrow' + lineSelected).val().toLocaleUpperCase();
		
						// Skip blank procedure codes
						if (nextVal != "") {
							if (nextVal == val) {
								
								if (idx == 0) {
									var errDup6 = ".error6-" + lineSelected;
		
									$(errDup6).hide();
								} else {
									dupFlag = true;
									// $(err6).show();  Do not show duplicate procedure codes
									$(err6).hide();
		
									if (!validationInProcessFlag) {
										
										// Stop circular validations
										validationInProcessFlag = true;
		
							 			// If this is not from the description check
								 		// Force validation of the duplicated procedure code
										checkDuplicateProcedure(nextVal, lineSelected);
						
							 			validationInProcessFlag = false;
									}
						 		}
							 }
						 }
					 }
			
					 // Go to the next row
					 lineSelected++;
				 }
			 }
		
			 // Hide the duplicate message if no duplicate was found
			 if (!dupFlag) {
				$(err6).hide();
			 }
		
			 // Clear duplicate messages for rows matching hidden row.
			 if ($('.hidden_procrow' + idx).length > 0) {
				var oldVal = $('.hidden_procrow' + idx).val().toLocaleUpperCase();
				  
				if ((idx != 0) && (oldVal != "")) {
					lineSelected = 1;
		
					// Loop throught all the rows 
					for (var i = 1; lineSelected <= numRows; i++ ) {
						var procedureCodeItem = $('.code_procrow' + i).length;
						
						// Was the row found?
						if (procedureCodeItem > 0) {
							if (i != idx) {
								var nextVal = $('.code_procrow' + lineSelected).val().toLocaleUpperCase();
		
								// Skip blank procedure codes
								if (nextVal != "") {
									if (nextVal == oldVal) {
										var errDup6 = ".error6-" + lineSelected;
										
										$(errDup6).hide();
		
										// Does this match any other line?
										if (!validationInProcessFlag) {
											
											// Stop circular validations
											validationInProcessFlag = true;
		
								 			// If this is not from the description check
									 		// Force validation of the duplicated procedure code
											checkDuplicateProcedure(nextVal, lineSelected);
							
								 			validationInProcessFlag = false;
										}
									}
								}
							 }
						
							 // Go to the next row
							 lineSelected++;
						}
					}
				}
			}
		}

		// Update the submit claim button status based on any duplicate procedure codes
		setSubmitButtonStatus();

		return dupFlag;
	}
	
	function checkDuplicateModifier(idx) {
		var dupFlag = false;
		var mod1Val = $('.code_pmod1-' + idx).val();
		var mod2Val = $('.code_pmod2-' + idx).val();
		var mod3Val = $('.code_pmod3-' + idx).val();
		var mod4Val = $('.code_pmod4-' + idx).val();
		  
		// Test procedure code modifier 1
		var err7 = ".error7-1-" + idx;
		if ((mod1Val != "") && ((mod1Val == mod2Val) || (mod1Val == mod3Val) || (mod1Val == mod4Val))) {
			dupFlag = true;
			$(err7).show();
		} else {
			$(err7).hide();
		}
		  
		// Test procedure code modifier 2
		err7 = ".error7-2-" + idx;
		if ((mod2Val != "") && ((mod2Val == mod1Val) || (mod2Val == mod3Val) || (mod2Val == mod4Val))) {
			dupFlag = true;
			$(err7).show();
		} else {
			$(err7).hide();
		}
		  
		// Test procedure code modifier 3
		err7 = ".error7-3-" + idx;
		if ((mod3Val != "") && ((mod3Val == mod1Val) || (mod3Val == mod2Val) || (mod3Val == mod4Val))) {
			dupFlag = true;
			$(err7).show();
		} else {
			$(err7).hide();
		}
		  
		// Test procedure code modifier 4
		err7 = ".error7-4-" + idx;
		if ((mod4Val != "") && ((mod4Val == mod1Val) || (mod4Val == mod2Val) || (mod4Val == mod3Val))) {
			dupFlag = true;
			$(err7).show();
		} else {
			$(err7).hide();
		}
		
		return dupFlag;
	}
	
	// Get start date for each browser type
	function getStartDate() {
		var dat;
		var d = "01";
		var m = "01";
		var y = "2016";
		  
		if ((browserStr == "unknown") || (browserStr == "IE") || (browserStr == "Firefox")) {
		
			// Format for IE and Firefox is mm/dd/yyyy
			dat = m + "/" + d + "/" + y;
		} else {
		
			// Format for all other browsers is yyyy-mm-dd
			dat = y + "-" + m + "-" + d;
		}
		
		return dat;
	}
	
	
	function getProcedureDescription(rowNumber){
		var procCodeid='<portlet:namespace/>code_procrow'+ rowNumber;
		var procDescId='<portlet:namespace/>codedesc_procrow' + rowNumber;
		
		// The first procedure code is mandatory
		var mandatoryFlag = false;
		if (rowNumber == '1') {
			mandatoryFlag = true;
		}
		isValidProcedureCode($("#" + procCodeid).val(), rowNumber, mandatoryFlag);
		
		// Get the procedure description.
		var procCodeid='<portlet:namespace/>code_procrow' + rowNumber;
		var procDescId='<portlet:namespace/>codedesc_procrow' + rowNumber;
		setProcedureDescription(procCodeid, procDescId, rowNumber);
	}
	
	function setExcludeMemberStatus() {
	
		// Test for the new Trial Claim button
		if ($("#<portlet:namespace/>memberIdCardNumber").val() == "") {
			$("#<portlet:namespace/>memberSearchStatus").val("false");
		}
		
		if ($("#<portlet:namespace/>memberSearchStatus").val() == "false") {
		
			// The exclude member checkbox is checked (initial state)
			$("#<portlet:namespace/>memberInfoExclude").val("true");
			
			// Show the fields used by non-member search
			$('.nonMemberSearchFields').removeClass('hideMemberInfo');
			
			// Clear the required non-member search fields
			$('#<portlet:namespace/>memberAge').val('');
			$('#<portlet:namespace/>memberTestGender').val('');
			
			// Hide the fields used for member search
			$('.memberSearchFields').addClass('hideMemberInfo');
		} else {
		
			// The exclude member checkbox is not checked
			$("#<portlet:namespace/>memberInfoExclude").val("false");
		
			// Show the fields used for member search
			$('.memberSearchFields').removeClass('hideMemberInfo');
		
			// Hide the fields for non-member search
			$('.nonMemberSearchFields').addClass('hideMemberInfo');
			
			// Put values in the required non-member search fields
			$('#<portlet:namespace/>memberAge').val(<%= HIDDEN_AGE %>);
			$('#<portlet:namespace/>memberTestGender').val('M');
		}
	}
	
	function setProcedureDescription(procCodeid, procDescId, rowNumber){
	
		AUI().ready('autocomplete-list','aui-base','aui-io-request','autocomplete-filters','autocomplete-highlighters',function (A) {
		
			// Remove leading and trailing spaces from the procedure code
			var procedureCode = $("#" + procCodeid).val().replace(/^\s+|\s+$/g, "");
		
		    A.io.request('<%=getProcedureDescription%>',{
		    dataType: 'text',
		    method: 'POST',
		    data: {
			 <portlet:namespace />procedureCode : procedureCode,
		     },
		      on: {
		        success: function() {
					var desc = "";
					var notFound = false;
			 
			 		// Set the procedure description (show error in red)
			 		desc = this.get('responseData');
					if (desc.indexOf(procErrStr) > -1) {
						
						// Display the error message if the code is not blank
						if (procedureCode == "") {
							desc = "";
						} else {
							notFound = true;
						}
					}
		
					if (notFound) {
						$("#" + procDescId).attr('style', "border-color:red; color:red");
					} else {
						$("#" + procDescId).removeAttr("style");
					}
					$("#" + procDescId).val(desc);
			    }
		      }
			});
		});
	}
	
	//For diagnosis table, on initial load we are displaying all the 25 rows. When we are clicking on Add, the hidden row will be shown. When clicked on delete, the row will be hidden.
	
	//Script for adding diagnosis rows
	function addDiagnosisRow(){
		YUI().use('node','event', function (Y) {
			Y.one('.hideDiagRow input').focus();
			Y.one('.hideDiagRow').replaceClass('hideDiagRow', 'showDiagRow');
		if(!Y.one('.hideDiagRow')){
			Y.one('#<portlet:namespace/>addDiagnosisLink').addClass('hideAddDiagButton');
		}
		});
	}
	
	//Script for deleting diagnosis rows	
	function deleteDiagRow(diaCode,diaDes){
		var diagcode="";
		YUI().use('node','event', function (Y) {
			diagcode= Y.one('#<portlet:namespace/>' + diaCode).get('value');
			var diagNodCodeid="#<portlet:namespace/>" + diaCode;
			var diagNodDesid="#<portlet:namespace/>" + diaDes;
			$("#<portlet:namespace/>" + diaCode).val("");
			$("#<portlet:namespace/>" + diaDes).val("");
			 
			if (Y.one('.hideDiagRow')) {
				Y.one('.hideDiagRow').ancestor().append(Y.one(diagNodCodeid).ancestor().ancestor().ancestor());
				Y.one(diagNodCodeid).ancestor().ancestor().ancestor().addClass('hideDiagRow').removeClass('showDiagRow');
			}else{
				Y.one(diagNodCodeid).ancestor().ancestor().ancestor().addClass('hideDiagRow').removeClass('showDiagRow');
			}
			if (Y.one('#<portlet:namespace/>addDiagnosisLink').hasClass('hideAddDiagButton') && Y.one('.hideDiagRow')) {  
				Y.one('#<portlet:namespace/>addDiagnosisLink').removeClass('hideAddDiagButton')
			}
		});
		 
		// Clear any duplicates caused by this row.
		checkDuplicateDiagnosis(diagcode, 0);
		
		// Remove the diagnosis code from the primary and related diagnosis if it is not a duplicate
		if (!diagnosisFound(diagcode)) {
			removePrimaryDiagnosis(diagcode);
		  	removeRelatedDiagnosis(diagcode);
		}
	}
	
	YUI().use('aui-node','aui-modal','event',function(Y) {
	
		/********To process CANCEL button action***************/
		Y.one('.cancelAction').on('click',function(e){
			Y.one('.form-inline').set('action',"<%=trailClaimsCancelURL%>");
			Y.one('.form-inline').submit();
		});
	});


	/********To process new trial claim button action***************/
	function showNewTrialClaim(){
		var url = '<%=trailClaimsNewURL%>';
		window.location.href = url;
	}
	
	var nameSpaceConstant = '<portlet:namespace/>';
	
	// Line of Business values
	var BCBSPlan_values = {
		'2' : 'National Allance/Major Group',
		'4' : 'Group & Individual'
	}
	
	var blueChoice_values = {
		'7' : 'BCHP Blue Choice HP'
	}
	
	var statePlan_values = {
		'1' : 'State Group'
	}
	
	var federal_values = {
		'3' : 'FEP'
	}
	
	var planAdministrators_values = {
		'5' : 'PAI'
	}
	
	var thomasCooper_values = {
		'6' : 'TCC'
	}
	
	// Group Values
	var nationalAllanceMajorGroup_values = {
		'03' : 'Administrative Services Only (ASO)',
		'71' : 'ASO National Acct. Control Plans',
		'26' : 'Associations',
		'15' : 'Group sized 50 - 99',
		'25' : 'Group sized 100+',
		'70' : 'National Acct. Insured',
		'60' : 'National Acct. Par Plans',
		'50' : 'National Acct. Par Plans (Point of Service)',
		'95' : 'National Central Cert.',
		'99' : 'Out-of-area'
	}
	
	var groupIndividual_values = {
		'81' : 'Conversion',
		'61' : 'Individual FFM',
		'63' : 'Individual MSPP',
		'62' : 'Individual Private Matketplace',
		'77' : 'Medicare Advantage',
		'93' : 'Personal Blue Plan',
		'94' : 'SCHIP',
		'45' : 'Small Group, 2 - 6',
		'06' : 'Small Group, 7 - 12',
		'05' : 'Small Group, 13 - 50',
		'66' : 'Small Group, 51 - 99',
		'65' : 'Small Group Private Marketplace',
		'64' : 'Small Group SHOP',
	}
	
	var BCHPBlueChoiceHP_value = {
		'22' : 'ASO',
		'32' : 'ASO Open Access',
		'24' : 'BCHP for Kids and Individual Policies',
		'21' : 'Fully Insured Large Group',
		'28' : 'Fully Insured Open Access (Blue Advantage)',
		'23' : 'Individual (FFM and Exchange)',
		'16' : 'Small Group, 6 - 12 Lives',
		'19' : 'Small Group, 7 - 19 Lives',
		'20' : 'Small Group, 20 - 50 Lives',
		'27' : 'Small Group Exchange'
	}
	
	var stateGroup_values = {
		'00' : 'State Group (ASO)'
	}
	
	var FEP_values = {
		'98' : 'Federal Employee Program (FEP)'
	}
	
	var PAI_values = {
		'PAI' : 'PAI'
	}
	
	var TCC_values = {
		'TCC' : 'TCC'
	}
	
	function addOptions(itm, selectItm, tbl) {
		var nextSelect = itm.one('#<portlet:namespace />' + selectItm);
		  
		// Clear the current options
		nextSelect.empty();
		  
		// Create first option
		option = itm.Node.create('<option value=\" \">Make a Selection</option>');
		nextSelect.append(option);
		
		// Cycles the collection and append the new option elements
		for (key in tbl) {
		
			if (!Object.prototype.hasOwnProperty.call(tbl, key)) {
				continue;
			}
			  
			// retrieves the label from the collection
			var label = tbl[ key ];
			  
			// creates a new option element...
			option = itm.Node.create('<option value=\"' + key + '\">' + label + '</option>');
			  
			// ...and append it to the select element.
			nextSelect.append(option);
		}
	}
	
	// Populate Line of Business based on the Sub Health Plan selection
	AUI().ready('aui-dialog',"node",function(A){
		if(A.one("#<portlet:namespace />memberInfoSubHealthPlan")){
			A.one("#<portlet:namespace />memberInfoSubHealthPlan").on("change",function(Y){
				var subHealthPlanValue =  A.one("#<portlet:namespace/>memberInfoSubHealthPlan").val();
				   
				if (subHealthPlanValue == "401") {  // BCBS Plan
					addOptions(A, 'memberInfoLineOfBusiness', BCBSPlan_values);
				} else if (subHealthPlanValue == "922") {  // Blue Choice
					addOptions(A, 'memberInfoLineOfBusiness', blueChoice_values);
				} else if (subHealthPlanValue == "400") {  // BS State Plan
					addOptions(A, 'memberInfoLineOfBusiness', statePlan_values);
				} else if (subHealthPlanValue == "402") {  // Federal (FEP)
					addOptions(A, 'memberInfoLineOfBusiness', federal_values);
				} else if (subHealthPlanValue == "886") {  // Plan Administrators (PAI)
					addOptions(A, 'memberInfoLineOfBusiness', planAdministrators_values);
				} else if (subHealthPlanValue == "315") {  // Thomas Cooper
					addOptions(A, 'memberInfoLineOfBusiness', thomasCooper_values);
				}
			});
		};
		});
		
		// Populate Group based on the Line of Business selection
		AUI().ready('aui-dialog',"node",function(A){
		if(A.one("#<portlet:namespace />memberInfoLineOfBusiness")){
			A.one("#<portlet:namespace />memberInfoLineOfBusiness").on("change",function(Y){
				var lineOfBusinessValue =  A.one("#<portlet:namespace/>memberInfoLineOfBusiness").val();
				   
				if (lineOfBusinessValue == "2") {  // National Allance/Major Group
					addOptions(A, 'memberInfoGroup', nationalAllanceMajorGroup_values);
				} else if (lineOfBusinessValue == "4") {  // Group & Individual
					addOptions(A, 'memberInfoGroup', groupIndividual_values);
				} else if (lineOfBusinessValue == "7") {  // BCHP Blue Choice HP
					addOptions(A, 'memberInfoGroup', BCHPBlueChoiceHP_value);
				} else if (lineOfBusinessValue == "1") {  // State Group
					addOptions(A, 'memberInfoGroup', stateGroup_values);
				} else if (lineOfBusinessValue == "3") {  // FEP
					addOptions(A, 'memberInfoGroup', FEP_values);
				} else if (lineOfBusinessValue == "5") {  // PAI
					addOptions(A, 'memberInfoGroup', PAI_values);
				} else if (lineOfBusinessValue == "6") {  // TCC
					addOptions(A, 'memberInfoGroup', TCC_values);
				}
			});
		}
	});
	
	function memberInfoExcludeChanged() {
		var check = $("#<portlet:namespace/>memberInfoExclude").prop('checked');
		
		if (check) {
			excludeMemberFlag = true;
			$("#<portlet:namespace/>memberSearchStatus").val("false");
				     
			// Disable secondary claim checkbox and original claim number
			setSecondaryClaimStatus();
				     
			setExcludeMemberStatus();
		} else {
		
			// Process include member
			excludeMemberFlag = false;
			$("#<portlet:namespace/>memberSearchStatus").val("true");
			
			// Hide the exclude member checkbox
			$("#<portlet:namespace/>memberInfoExclude").hide();
			    
				// Enable secondary claim checkbox and original claim number
			setSecondaryClaimStatus();
				
			// Open member search page
			var url='<%=memberSearchPageURL.toString()%>';
			window.location.href = url;
		}
	}
	
	function setSecondaryClaimStatus() {
		if ($("#<portlet:namespace/>memberSearchStatus").val() == "false") {
			 
			 // Disable the secondary claim checkbox
			 $("#<portlet:namespace/>additionalCriteriaSecondaryClaim").attr('disabled', 'true');
			 
			 // Disable the original claim number field
			 $("#<portlet:namespace/>additionalCriteriaOriginalNumber").attr('disabled', 'true');
		} else {
		
			 // Enable the secondary claim checkbox
			 $("#<portlet:namespace/>additionalCriteriaSecondaryClaim").removeAttr('disabled');
		
			 // Enable the original claim number field
			 $("#<portlet:namespace/>additionalCriteriaOriginalNumber").removeAttr('disabled');
		}
	}
	
	function secondaryClaimChanged() {
		var check = $("#<portlet:namespace/>additionalCriteriaSecondaryClaim").prop('checked');
		
		if (check) {
		
			// The secondary claim checkbox is checked
			$("#<portlet:namespace/>additionalCriteriaSecondaryClaim").val("true");
		} else {
		
			// The secondary claim checkbox is not checked
			$("#<portlet:namespace/>additionalCriteriaSecondaryClaim").val("false");
		}
	}
	
	//Ajax call for getting diagnosis description based on diagnosis code
	function getDescription(rowNumber){
		var diagCodeid="diaCode"+rowNumber;
		var diagDescId="diaDesc"+rowNumber;
		
		setDescription(diagCodeid,diagDescId,rowNumber);

		// Remove the old diagnosis code if it is not a duplicate
		removeOldDiagnosisCode(rowNumber);
	}
	
	function setDescription(diagCodeid,diagDescId,rowNumber){
		AUI().ready('autocomplete-list','aui-base','aui-io-request','autocomplete-filters','autocomplete-highlighters',function (A) {
		
			var diagcode= A.one('#<portlet:namespace/>'+diagCodeid).get('value');
			A.io.request('<%=getUsers%>',{
				dataType: 'text',
				method: 'POST', 
				data: {
					<portlet:namespace />diagcode : diagcode,
				},
				on: {
					success: function() {
						var desc = this.get('responseData');
						var notFound = false;
						if (desc.indexOf(diagErrStr) > -1) {
							notFound = true;
						}

						if (A.one('#<portlet:namespace/>' + diagCodeid).hasClass('error-field')) {
							A.one('#<portlet:namespace/>' + diagCodeid).removeClass('error-field');
							A.one('#<portlet:namespace/>' + diagCodeid).removeClass('error');
						}
						$('#<portlet:namespace/>' + diagDescId).removeAttr("style");

						if (notFound) {
							if ($("#<portlet:namespace/>" + diagCodeid).val().replace(/^\s+|\s+$/g, "") == "") {
								
								//clear the error description for a blank code
								desc = "";
							} else {
								A.one('#<portlet:namespace/>' + diagCodeid).addClass('error-field');
								A.one('#<portlet:namespace/>' + diagCodeid).addClass('error');
											  
								// show error description in red
								$('#<portlet:namespace/>' + diagDescId).attr('style', "border-color:red; color:red");
							}
						}
						A.one('#<portlet:namespace/>' + diagDescId).setAttribute('value',desc);
						 
						// Add the code if it is valid.
						var code = diagcode.toLocaleUpperCase();
						var dupFlag = isDuplicateDiagnosis(code, rowNumber);
						var validFlag = isValidDiagnosis(code, <%= DIAGNOSIS_LEN %>, true);
						if (!dupFlag && validFlag && (code.length <= <%= DIAGNOSIS_LEN %>) && !notFound) {
							addRelatedDiagnosis("diagsSelect", code);
							addPrimaryDiagnosis("pDiagSelect", code);
						}
						
						// Restore the description length
						setDiagnosisDescWidth();
					}
				}
			});
		});
	}
	
	// Set up select2
	$(".diagsSelect").select2();
	$(".diagsSelect").select2 ({ maximumSelectionLength: 3 });
	
	// Set index column in claim lines
	$('.procrow').each(function (i) {
		$("td", this).eq(0).html(i + 1);
	});
	
	// Scroll the screen when the related diagnoisis menu is open.  This is a work around for a bug in select2.
	$('.diagsSelect').on('select2:open', function (e) {
		window.scrollBy (0, 5);
		window.scrollBy (0, -5);
	});
	
	//  Hide all detail rows
	$(".hide_row").hide();
	
	// Show/hide the detail rows and change the icon
	function toggleRow(elem, idx1) {
		if (elem.className == "icon-expand-alt") {
			elem.className = "icon-collapse-alt";
		} else {
			elem.className = "icon-expand-alt";
		}
		
		$(".resultrow_" + idx1).toggle();
	}
	
	function displayAddLink(type, showFlag, idx) {
		var idStr = null;
		  
		if (type == 'diagnosis') {
			idStr = '<portlet:namespace/>addDiagnosisLink';
		} else if (type == 'claim') {
			idStr = '<portlet:namespace/>addClaimLink';
		} else if (type == 'modifier') {
			idStr = 'addModifierLink' + idx;
		}
		  
		// Show or hide the link
		if (showFlag == true) {
			$('#' + idStr).show();
		} else {
			$('#' + idStr).hide();
		}
	}
	
	//Script for adding a claim line row	
	function addNewClaimRow() {
		addClaimRow(getNamespace());
		
		// Show date picker for IE and Firefox
		if ((browserStr == "unknown") || (browserStr == "IE") || (browserStr == "Firefox")) {
			var numRows = numTableRows('tbl-claim-lines');  // Use the added row for the date picker
			
			$("#" + getNamespace() + "procedureDateOfService" + numRows).datepicker({
				dateFormat: 'mm/dd/yy',
				changeMonth: true,
				changeYear: true
			});
			$("#" + getNamespace() + "procedureDateOfService" + numRows).attr("placeholder", "mm/dd/yyyy");
			$("#" + getNamespace() + "procedureDateOfService" + numRows).css({"background" : "url('<%=request.getContextPath()%>/images/icondatepicker.png') no-repeat",
			                                                                  "background-position-x" : "100%", "background-position-y" : "50%"});
		}
	}
	
	//Script for deleting claim line rows	
	function deleteRow(elem, idx, typeCode) {
	
		// Get the row containing the delete icon
		var tr = $(elem).closest('tr');
		
		// Check for details open and delete it
		if (typeCode == 'diagnosis') {
			removeRelatedDiagnosis(idx);
			$(".diagrow" + idx).remove();
		} else if (typeCode == 'claim') {	
			var procCode = $(".code_procrow" + idx).val();
		
			$(".procrow" + idx).remove();
			
			// Determine the line selected and the number of lines in the table
			var lineSelected = idx;
			var numRows = numTableRows('tbl-claim-lines');
			// Change the procedure code name attribute
			// Loop throught all the rows below the one selected and decrease the indexes in the name by one
			for (var i = idx + 1; lineSelected < numRows; i++, lineSelected++) {
				var procedureCodeItem = $(".code_procrow" + i).length;
				// Change the name if the class was found
				if (procedureCodeItem > 0){
		
					// Set  the new procedure row number name attribute
					var newRowNumberName = "<portlet:namespace/>procRowNumber" + lineSelected;
					var newRowNumberClass = "row_input procRowNumber" + lineSelected;
					$(".procRowNumber" + i).attr('name', newRowNumberName);
					$(".procRowNumber" + i).attr('id', newRowNumberName);
					$(".procRowNumber" + i).val(lineSelected);
					$(".procRowNumber" + i).attr('class', newRowNumberClass);
		
					// Set the new procedure date of service name attributes and errors
					var newDateOfServiceName = "<portlet:namespace/>procedureDateOfService" + lineSelected;
					var newDateOfServiceClass = "pDate procedureDateOfService" + lineSelected;
					var newDateOfServiceError1 = "error1 error1-" + lineSelected;
					var newDateOfServiceError2 = "error2 error2-" + lineSelected;
					$(".procedureDateOfService" + i).attr('name', newDateOfServiceName);
					$(".procedureDateOfService" + i).attr('id', newDateOfServiceName);
					$(".procedureDateOfService" + i).attr('onChange', "javascript:checkDate(this.value, " + lineSelected + ");" +
		                                                              "javascript:isValidClaimLine(" + lineSelected + ")");
					$(".procedureDateOfService" + i).attr('class', newDateOfServiceClass);
					$(".error1-" + i).attr('class', newDateOfServiceError1);
					$(".error2-" + i).attr('class', newDateOfServiceError2);
		
					// Set the new procedure code attributes and errors
					var newCodeName = "<portlet:namespace/>code_procrow" + lineSelected;
					var newCodeClass = "pCode toUppercase code_procrow" + lineSelected;
					var newRowNumberError3 = "error3 error3-" + lineSelected;
					var newRowNumberError6 = "error6 error6-" + lineSelected;
					$(".code_procrow" + i).attr('name', newCodeName);
					$(".code_procrow" + i).attr('id', newCodeName);
					$(".code_procrow" + i).attr('onChange', "javascript:getProcedureDescription(" + lineSelected + ");" +
					                                        "javascript:checkDuplicateProcedure(this.value, " + lineSelected + ");" +
					                                        "javascript:isValidClaimLine(" + lineSelected + ")");
					$(".code_procrow" + i).attr('onFocus', "javascript:saveCode(" + lineSelected + ", true)");
					$(".code_procrow" + i).attr('class', newCodeClass);
					$(".error3-" + i).attr('class', newRowNumberError3);
					$(".error6-" + i).attr('class', newRowNumberError6);
					
					// Set the new procedure code hidden row attributes
					var newHiddenCodeName = "<portlet:namespace/>hidden_procrow" + lineSelected;
					var newHiddenCodeClass = "pCode toUppercase hidden_procrow" + lineSelected;
					$(".hidden_procrow" + i).attr('id', newHiddenCodeName);
					$(".hidden_procrow" + i).attr('class', newHiddenCodeClass);
									
					// Set the new procedure description name attribute
					var newDescriptionName = "<portlet:namespace/>codedesc_procrow" + lineSelected;
					var newDescriptionClass = "pDesc codedesc_procrow" + lineSelected;
					$(".codedesc_procrow" + i).attr('name', newDescriptionName);
					$(".codedesc_procrow" + i).attr('id', newDescriptionName);
					$(".codedesc_procrow" + i).attr('onChange', "javascript:isValidClaimLine(" + lineSelected + ")");
					$(".codedesc_procrow" + i).attr('class', newDescriptionClass);
		
					// Set the new procedure code modifier attributes and errors
					var newModName1 = "<portlet:namespace/>code_pmod1-" + lineSelected;
					var newModName2 = "<portlet:namespace/>code_pmod2-" + lineSelected;
					var newModName3 = "<portlet:namespace/>code_pmod3-" + lineSelected;
					var newModName4 = "<portlet:namespace/>code_pmod4-" + lineSelected;
					var newModClass1 = "pMod code_pmod1-" + lineSelected;
					var newModClass2 = "pMod code_pmod2-" + lineSelected;
					var newModClass3 = "pMod code_pmod3-" + lineSelected;
					var newModClass4 = "pMod code_pmod4-" + lineSelected;
					var newMod1Error5 = "error5 error5-1-" + lineSelected;
					var newMod1Error7 = "error7 error7-1-" + lineSelected;
					var newMod2Error5 = "error5 error5-2-" + lineSelected;
					var newMod2Error7 = "error7 error7-2-" + lineSelected;
					var newMod3Error5 = "error5 error5-3-" + lineSelected;
					var newMod3Error7 = "error7 error7-3-" + lineSelected;
					var newMod4Error5 = "error5 error5-4-" + lineSelected;
					var newMod4Error7 = "error7 error7-4-" + lineSelected;
					$(".code_pmod1-" + i).attr('name', newModName1);
					$(".code_pmod2-" + i).attr('name', newModName2);
					$(".code_pmod3-" + i).attr('name', newModName3);
					$(".code_pmod4-" + i).attr('name', newModName4);
					$(".code_pmod1-" + i).attr('onChange', "javascript:isValidClaimLine(" + lineSelected + ")");
					$(".code_pmod2-" + i).attr('onChange', "javascript:isValidClaimLine(" + lineSelected + ")");
					$(".code_pmod3-" + i).attr('onChange', "javascript:isValidClaimLine(" + lineSelected + ")");
					$(".code_pmod4-" + i).attr('onChange', "javascript:isValidClaimLine(" + lineSelected + ")");
					$(".code_pmod1-" + i).attr('class', newModClass1);
					$(".code_pmod2-" + i).attr('class', newModClass2);
					$(".code_pmod3-" + i).attr('class', newModClass3);
					$(".code_pmod4-" + i).attr('class', newModClass4);
					$(".error5-1-" + i).attr('class', newMod1Error5);
					$(".error7-1-" + i).attr('class', newMod1Error7);
					$(".error5-2-" + i).attr('class', newMod2Error5);
					$(".error7-2-" + i).attr('class', newMod2Error7);
					$(".error5-3-" + i).attr('class', newMod3Error5);
					$(".error7-3-" + i).attr('class', newMod3Error7);
					$(".error5-4-" + i).attr('class', newMod4Error5);
					$(".error7-4-" + i).attr('class', newMod4Error7);
		
					// Set the new procedure code attributes and errors
					var newRevCodeName = "<portlet:namespace/>revcode_procrow" + lineSelected;
					var newRevCodeClass = "pRevcode revcode_procrow" + lineSelected;
					var newRevCodeError11 = "error11 error11-" + lineSelected;
					$(".revcode_procrow" + i).attr('name', newRevCodeName);
					$(".revcode_procrow" + i).attr('id', newRevCodeName);
					$(".revcode_procrow" + i).attr('onChange', "javascript:isValidRevCode(this.value, " + lineSelected + ", true);" +
		                                                       "javascript:isValidClaimLine(" + lineSelected + ")");
					$(".revcode_procrow" + i).attr('class', newRevCodeClass);
					$(".error11-" + i).attr('class', newRevCodeError11);
		
					// Set the new place of service name attribute
					var newPosName = "<portlet:namespace/>placeOfService-row" + lineSelected;
					var newPosClass = "pPlace placeOfService-row" + lineSelected;
					$(".placeOfService-row" + i).attr('name', newPosName);
					$(".placeOfService-row" + i).attr('class', newPosClass);
		
					// Set the new primary diagnosis attributes and errors
					var newPriName = "<portlet:namespace/>primaryDiagnosis" + lineSelected;
					var newPriClass = "pDiag pDiagSelect primaryDiagnosis" + lineSelected;
					var newPriError10 = "error10 error10-" + lineSelected;
					$(".primaryDiagnosis" + i).attr('name', newPriName);
					$(".primaryDiagnosis" + i).attr('id', newPriName);
					$(".primaryDiagnosis" + i).attr('onChange', "javascript:isValidPrimaryDiagnosis(" + lineSelected + ");" +
		                                                        "javascript:isValidClaimLine(" + lineSelected + ")");
					$(".primaryDiagnosis" + i).attr('class', newPriClass);
					$(".error10-" + i).attr('class', newPriError10);
		
					// Set the new related diagnosis name attribute
					var newRdName = "<portlet:namespace/>relDiagnosis" + lineSelected;
					$(".relDiagnosis" + i).attr('name', newRdName);
					$(".relDiagnosis" + i).attr('onChange', "javascript:isValidRelatedDiagnosis(" + lineSelected + ")");
					
					// Set the new units attributes and errors
					var newUnitsName = "<portlet:namespace/>unit_procrow" + lineSelected;
					var newUnitsClass = "pUnit procedureUnits unit_procrow" + lineSelected;
					var newUnitError4 = "error4 error4-" + lineSelected;
					var newUnitError9 = "error9 error9-" + lineSelected;
					$(".unit_procrow" + i).attr('name', newUnitsName);
					$(".unit_procrow" + i).attr('onChange', "javascript:isValidUnits(this.value, " + lineSelected + ");" +
		                                                    "javascript:isValidClaimLine(" + lineSelected + ")");
					$(".unit_procrow" + i).attr('class', newUnitsClass);
					$(".error4-" + i).attr('class', newUnitError4);
					$(".error9-" + i).attr('class', newUnitError9);
					
					// Set the new trash attributes
					var newTrashClass = "icon-trash proctrash" + lineSelected;
					$(".proctrash" + i).attr('onClick', "javascript:deleteRow(this, " + lineSelected + ", 'claim')");
					$(".proctrash" + (lineSelected + 1)).attr('class', newTrashClass);
		
					// Set the procedure code/REV code error message
					var newError12 = "error12 error12-" + lineSelected;
					$(".error12-" + i).attr('class', newError12);
				}
			}
		} else if (typeCode == 'modifier') {	
			// No other rows
		}
		
		// Delete the row containing the delete icon
		tr.remove();
			  
		// Clear any duplicates caused by this row.
		checkDuplicateProcedure(procCode, 0);
		
		// Show the link if the numer of rows is below the max
		if (typeCode == 'diagnosis') {
			if (numTableRows('tbl-diagnosis-lines') < maxDiagnosisRows) {
				displayAddLink('diagnosis', true, 0);
			}
		} else if (typeCode == 'claim') {
			var numRows = numTableRows('tbl-claim-lines');
		
			if (numRows < maxClaimRows) {
				displayAddLink('claim', true, 0);
			}
			
			// Update the number of lines
			$('.maxClaimLines').val(numRows);
		} else if (typeCode == 'modifier') {
			if (numTableRows('tbl-modifier-lines' + idx) < maxModifierRows) {
				displayAddLink('modifier', true, idx);
			}
		}
		
		$('.procrow').each(function (i) {
		 	  $("td", this).eq(0).html(i + 1);
		});
		
		// Test submit button status
		setSubmitButtonStatus();
	}

</aui:script>

<div id="errorDialog" title="Error Message" style="display: none">
</div>


<div id="busy_indicator" style="display: none">
	<img src="<%=request.getContextPath()%>/images/busy-spinner.gif" alt="Busy indicator">
</div>

<%
	String defaultState = "open";
%>
<c:choose>
	<c:when test="${fn:length(diagnosis) > 0}">
		<%
			defaultState = "closed";
		%>
	</c:when>
</c:choose>

<liferay-ui:panel title="tce.title" collapsible="true" defaultState="<%=defaultState%>" cssClass="panelClass">
	<aui:form name="trialClaimForm" cssClass="form-inline" action="${trailClaimPageURL}" method="post" commandName="trailClaimDetailsFO" onChange="javascript:setSubmitButtonStatus()" >

		<aui:fieldset label="tce.member.details" id="member-information-fields">
			<aui:container>
				<input type="hidden" class="hidden_excludeMemberFlag" id="<portlet:namespace/>memberSearchStatus" value='${trailClaimDetailsFO.memberSearchStatus}'>
				<input type="hidden" class="hidden_initialFlag" id="<portlet:namespace/>initialStatus" value='${trailClaimDetailsFO.initialStatus}'>
				<div class="nonMemberSearchFields">
					<aui:row>
						<aui:col span="12">
							<c:choose>
								<c:when test="${fn:length(diagnosis) > 0}">
									<input name="<portlet:namespace/>memberInfoExclude" id="<portlet:namespace/>memberInfoExclude" value="true" type="checkbox" checked="true" disabled="true" />
									 <label class="cbLabel"> <%=EXCLUDE_MEMBER_STR%></label> 
								</c:when>
								<c:otherwise>
									<input name="<portlet:namespace/>memberInfoExclude" id="<portlet:namespace/>memberInfoExclude" value="true"  type="checkbox" checked="true" onclick="javascript:memberInfoExcludeChanged()" />
									 <label class="cbLabel"><%=EXCLUDE_MEMBER_STR%></label> 
								</c:otherwise>
							</c:choose>
						</aui:col>
					</aui:row>
				</div>
				<aui:row>
					<div class="memberSearchFields">
						<aui:col span="6">
							<c:choose>
								<c:when test="${fn:length(diagnosis) > 0}">
									<aui:select name="memberInfoHealthPlan" label="tce.label.health.plan" cssClass="span7" required="true" showRequiredLabel="true" disabled="true">
										<aui:option value="0010">BCBS &ndash; South Carolina</aui:option>
									</aui:select>
								</c:when>
								<c:otherwise>
									<aui:select name="memberInfoHealthPlan" label="tce.label.health.plan" cssClass="span7" required="true" showRequiredLabel="true">
										<aui:option value="0010">BCBS &ndash; South Carolina</aui:option>
									</aui:select>
								</c:otherwise>
							</c:choose>
						</aui:col>
						<aui:col span="6">
							<aui:input name="memberIdCardNumber" label="tce.label.id.card.number" cssClass="span7" type="text" readonly="true" value='${trailClaimDetailsFO.memberId}' />
						</aui:col>
					</div>

					<div class="nonMemberSearchFields">
						<aui:col span="6">

							<c:choose>
								<c:when test="${fn:length(diagnosis) > 0}">
									<aui:input name="memberAge_r" label="tce.label.age" value='${trailClaimDetailsFO.memberAge}' disabled="true" type="text" />
								</c:when>
								<c:otherwise>
									<aui:input name="memberAge" label="tce.label.age" cssClass="span7" type="text" value=''>
										<aui:validator name="required" />
										<aui:validator name="digits" />
										<aui:validator name="minLength">1</aui:validator>
										<aui:validator name="maxLength"><%= AGE_LEN %></aui:validator>
									</aui:input>
								</c:otherwise>
							</c:choose>
						</aui:col>
						<aui:col span="6">

							<c:choose>
								<c:when test="${fn:length(diagnosis) > 0}">
									<aui:select name="memberTestGender_r" label="tce.label.gender" cssClass="span7" required="true" disabled="true" showRequiredLabel="true">
										<aui:option value="F" selected="${trailClaimDetailsFO.memberTestGender eq 'F'}">Female</aui:option>
										<aui:option value="M" selected="${trailClaimDetailsFO.memberTestGender eq 'M'}">Male</aui:option>
										<aui:option value="U" selected="${trailClaimDetailsFO.memberTestGender eq 'U'}">Unknown</aui:option>
									</aui:select>
								</c:when>
								<c:otherwise>
									<aui:select name="memberTestGender" label="tce.label.gender" cssClass="span7" required="true" showRequiredLabel="true">
										<aui:option value="">Make a Selection</aui:option>
										<aui:option value="F" selected="">Female</aui:option>
										<aui:option value="M" selected="">Male</aui:option>
										<aui:option value="U" selected="">Unknown</aui:option>
									</aui:select>
								</c:otherwise>
							</c:choose>
						</aui:col>
					</div>
				</aui:row>
				<div class="memberSearchFields">
					<aui:row>
						<aui:col span="6">
							<aui:input name="memberFirstName" label="tce.label.first.name" cssClass="span7" type="text" readonly="true" value='${trailClaimDetailsFO.memberFirstName}' />
						</aui:col>
						<aui:col span="6">
							<aui:input name="memberLastName" label="tce.label.last.name" cssClass="span7" type="text" readonly="true" value='${trailClaimDetailsFO.memberLastName}' />
						</aui:col>
					</aui:row>
					<aui:row>
						<aui:col span="6">
							<aui:input name="memberDOB" label="tce.label.date.of.birth" cssClass="span7" type="text" readonly="true" value='${trailClaimDetailsFO.memberDOB}' />
						</aui:col>
						<aui:col span="6">
							<aui:input name="memberGender" label="tce.label.gender" cssClass="span7" type="text" readonly="true" value='${trailClaimDetailsFO.memberGender}' />
						</aui:col>
					</aui:row>
					<aui:row>
						<aui:col span="6">
						</aui:col>
						<aui:col span="6">
							<c:choose>
								<c:when test="${fn:length(diagnosis) > 0}">
									<aui:select name="memberBlueCardStatus_r" label="tce.label.card.status" cssClass="span7" required="true" showRequiredLabel="true" disabled="true">
										<aui:option value="0" selected="${trailClaimDetailsFO.memberBlueCardStatus eq '0'}">No Blue Card</aui:option>
										<aui:option value="1" selected="${trailClaimDetailsFO.memberBlueCardStatus eq '1'}">Blue Card Home</aui:option>
										<aui:option value="2" selected="${trailClaimDetailsFO.memberBlueCardStatus eq '2'}">Blue Card Host</aui:option>
									</aui:select>
								</c:when>
								<c:otherwise>
									<aui:select name="memberBlueCardStatus" label="tce.label.card.status" cssClass="span7" required="true" showRequiredLabel="true">
										<aui:option value="0" selected="">No Blue Card</aui:option>
										<aui:option value="1" selected="">Blue Card Home</aui:option>
										<aui:option value="2" selected="">Blue Card Host</aui:option>
									</aui:select>
								</c:otherwise>
							</c:choose>
						</aui:col>
					</aui:row>
				</div>
			</aui:container>
		</aui:fieldset>

		<aui:fieldset label="tce.provider.details" id="provider-info-fields">
			<aui:container>
				<aui:row>
					<aui:col span="6">

						<c:choose>
							<c:when test="${fn:length(diagnosis) > 0}">
								<aui:input name="providerInfoBillingTIN" label="tce.label.billing.tin" value='${trailClaimDetailsFO.providerInfoBillingTIN}' maxlength="<%= TIN_LEN %>" disabled="true" type="text" />
							</c:when>
							<c:otherwise>
								<aui:input name="providerInfoBillingTIN" label="tce.label.billing.tin" type="text" maxlength="<%= TIN_LEN %>">
									<aui:validator name="digits" />
									<aui:validator name="minLength"><%= TIN_LEN %></aui:validator>
								</aui:input>
							</c:otherwise>
						</c:choose>

					</aui:col>
					<aui:col span="6">

						<c:choose>
							<c:when test="${fn:length(diagnosis) > 0}">
								<aui:input name="providerInfoBillingNPI" label="tce.label.billing.npi" value='${trailClaimDetailsFO.providerInfoBillingNPI}' maxlength="<%= NPI_LEN %>" disabled="true" type="text" />
							</c:when>
							<c:otherwise>
								<aui:input name="providerInfoBillingNPI" label="tce.label.billing.npi" type="text" maxlength="<%= NPI_LEN %>">
									<aui:validator name="digits"></aui:validator>
									<aui:validator name="minLength"><%= NPI_LEN %></aui:validator>
								</aui:input>
							</c:otherwise>
						</c:choose>

					</aui:col>
				</aui:row>
				 <aui:row>
					<aui:col span="6"> 
						<c:choose>
							<c:when test="${fn:length(diagnosis) > 0}">
								<aui:input name="providerInfoRenderingNPI" label="tce.label.rendering.npi" value='${trailClaimDetailsFO.providerInfoRenderingNPI}' maxlength="<%= NPI_LEN %>" disabled="true" type="text" />
							</c:when>
							<c:otherwise>
								<aui:input name="providerInfoRenderingNPI" label="tce.label.rendering.npi" type="text" maxlength="<%= NPI_LEN %>">
									<aui:validator name="digits"></aui:validator>
									<aui:validator name="minLength"><%= NPI_LEN %></aui:validator>
								</aui:input>

								<aui:script>
									// Include confirmation dialog
									if ($("#<portlet:namespace/>initialStatus").val() == "true") {
										var msg = 'Trial Claim is for simulation purposes only and not a guarantee of how an actual claim may process.\n\n' +
									      'CPT &copy;<%= currentYear %> American Medical Association. All rights reserved.\n\n' +
									      'Fee schedules, relative value units, conversion factors and/or related components are not assigned by the AMA, are not part of CPT, and the AMA is not recommending their\n' +
									      'use. The AMA does not directly or indirectly practice medicine or dispense medical services. The AMA assumes no liability for the data contained or not contained herein.\n\n' +
									      'CPT is a registered trademark of the American Medical Association.';

										$.confirm({
											animation: 'RotateY',
											closeAnimation: 'RotateYR',
											animationSpeed: 1000, // 1 second
											title: 'Disclaimer',
											content: msg,
											confirmButton: 'Yes I agree',
											cancelButton: 'Cancel',
											confirm: function(){
												// Close the dialog and continue processing
											},
											cancel: function(){
												// Exit to the users login screen by calling the cancel processing
												YUI().use('aui-node','aui-modal','event',function(Y) {
													Y.one('.form-inline').set('action',"<%=trailClaimsCancelDisclaimerURL%>");
													Y.one('.form-inline').submit();
												});
											}
										});
									}
								</aui:script>
							</c:otherwise>
						</c:choose>
						</aui:col>
						<aui:col span="6">

						<c:choose>
							<c:when test="${fn:length(diagnosis) > 0}">
								<aui:select name="providerInfoInNetwork" label="tce.label.rendering.in.network" cssClass="span7" required="true" 
								            disabled="true" showRequiredLabel="true">
									<aui:option value="Y" selected="${trailClaimDetailsFO.providerInfoInNetwork eq 'Y'}">Yes</aui:option>
									<aui:option value="N" selected="${trailClaimDetailsFO.providerInfoInNetwork eq 'N'}">No</aui:option>
								</aui:select>
							</c:when>
							<c:otherwise>
								<aui:select name="providerInfoInNetwork" label="tce.label.rendering.in.network" cssClass="span7" required="true" showRequiredLabel="true">
									<aui:option value="Y">Yes</aui:option>
									<aui:option value="N">No</aui:option>
								</aui:select>
							</c:otherwise>
						</c:choose>

					</aui:col>
				</aui:row> 
			</aui:container>
		</aui:fieldset>

		<aui:fieldset label="tce.additional.details"
			id="additional-criteria-fields">
			<aui:container>
				<aui:row>
					<aui:col span="6">
						<c:choose>
							<c:when test="${fn:length(diagnosis) > 0}">
								<c:if test="${secodaryClaimIndicator == 'true'}">
									<input name="<portlet:namespace/>additionalCriteriaSecondaryClaimR" id="<portlet:namespace/>additionalCriteriaSecondaryClaimR" type="checkbox" disabled="true" checked="true"
										   value='${trailClaimDetailsFO.additionalCriteriaSecondaryClaim}' />
									<label class="cbLabel"><%=SECONDARY_CLAIM_STR%></label>
								</c:if>
								<c:if test="${secodaryClaimIndicator != 'true'}">
									<input name="<portlet:namespace/>additionalCriteriaSecondaryClaimR" id="<portlet:namespace/>additionalCriteriaSecondaryClaimR" type="checkbox" disabled="true"
										   value='${trailClaimDetailsFO.additionalCriteriaSecondaryClaim}' />
									<label class="cbLabel"><%=SECONDARY_CLAIM_STR%></label>
								</c:if>
							</c:when>
							<c:otherwise>
								<input name="<portlet:namespace/>additionalCriteriaSecondaryClaim" id="<portlet:namespace/>additionalCriteriaSecondaryClaim" value="false" type="checkbox" disabled="true" 
								       onclick="javascript:secondaryClaimChanged()" />
								<label class="cbLabel"><%=SECONDARY_CLAIM_STR%></label>
							</c:otherwise>
						</c:choose>
					</aui:col>
					<aui:col span="6">
						<c:choose>
							<c:when test="${fn:length(diagnosis) > 0}">
								<aui:input name="additionalCriteriaOriginalNumber_r" label="tce.label.original.claim.number" value='${trailClaimDetailsFO.additionalCriteriaOriginalNumber}' maxlength="<%= CLAIM_NUM_LEN %>" type="text" disabled="true" />
							</c:when>
							<c:otherwise>
								<aui:input name="additionalCriteriaOriginalNumber" label="tce.label.original.claim.number" type="text" maxlength="<%= CLAIM_NUM_LEN %>">
								</aui:input>
							</c:otherwise>
						</c:choose>
					</aui:col>
				</aui:row>
			</aui:container>
		</aui:fieldset>

		<aui:fieldset label="Diagnosis Codes" id="diagnosis-table" cssClass="diagnosis-table">
			<aui:container>
				<aui:row cssClass="row-header">

					<table id="tbl-diagnosis-lines" class="table table-striped" style="width: 100%; border-collapse: collapse; border-spacing: 0;">
						<thead>
							<tr>
								<th class="textCenter" style="width: 20%">Diagnosis Code *</th>
								<th class="textCenter" style="width: 80%">Description</th>
								<th class="textCenter" style="width: 20%">Action</th>
							</tr>
						</thead>
						<tbody>
							<c:choose>
								<c:when test="${fn:length(diagnosis) > 0}">
									<c:forEach var="diagnosisList" items="${diagnosis}">
										<tr class="showDiagRow">
											<td class="textCenter"><c:out value="${diagnosisList.key}"></c:out> 
											<%-- <aui:input name="diaCode${diagnosisIndex}" id="diaCode${diagnosisIndex}"  value='${diagnosisList.key}'  cssClass="diaCode toUppercase"  style="text-transform: uppercase;" >
											</aui:input> --%></td>
											<td class="textCenter"><c:out value="${diagnosisList.value}"></c:out> 
											<%-- <aui:input name="diaDesc${diagnosisIndex}" id="diaDesc${diagnosisIndex}" value='${diagnosisList.value}' cssClass="diaCodeDesc"></aui:input> --%>
											</td>
											<td class="textCenter">
												<%-- <span> 
												<i class="icon-trash" title="Delete" onClick="deleteDiagRow(${diagnosisIndex},${diagnosisIndex})"></i>
												</span> --%>
											</td>
										</tr>
									</c:forEach>
								</c:when>

								<c:otherwise>
									<%
										int minLengt = 1;
										int maxLength = 24;
										String visibleStatus;
										String inputid;
										String inputDiaCodeId;
										String inputDiaDesId;
										String diagClasses;
										List outLsit = new ArrayList();
										String getDescription;
										String diagOnfocus;
										for (int i = 1; i <= maxLength; i++) {
											inputDiaCodeId = "diaCode" + i;
											getDescription = "javascript:getDescription(" + i + ")";
											inputDiaDesId = "diaDesc" + i;
											diagClasses = "diagSize diaCode toUppercase diagcode" + i;
											diagOnfocus = "javascript:saveCode(" + i + ", true)";
											if (i == 1) {
												visibleStatus = "showDiagRow";
											}/* else if(i>1&&i<=outLsit.size()){
												visibleStatus="extraDiagdataRow";
												} */
											else {
												visibleStatus = "hideDiagRow";
											}
									%>
									<tr class="<%=visibleStatus%>">
										<td>
											<aui:input name="<%=inputDiaCodeId%>" id="<%=inputDiaCodeId%>" maxlength="<%= DIAGNOSIS_LEN %>" cssClass="<%=diagClasses%>" onFocus="<%=diagOnfocus%>"
											           onChange="<%=getDescription%>" style="text-transform: uppercase;">
												<% if (i == 1) { %>
													<aui:validator name="required"></aui:validator>
												<% } %>
												<aui:validator name="custom" errorMessage="Cannot enter duplicate Diagnosis Code.">
													function (val, fieldNode, ruleValue) {
														var retValue = true;
				
														retValue = !checkDuplicateDiagnosis(val, <%=i%>);
										
														return retValue;
													}
												</aui:validator>
												<aui:validator name="custom" errorMessage="Please enter only alpha characters, digits or period.">
													function (val, fieldNode, ruleValue) {
														var retValue = true;
					
														// If the value is too long make sure the length error is displayed.
														if (val.length <= <%=DIAGNOSIS_LEN%>) {
															retValue = isValidDiagnosis(val, <%=DIAGNOSIS_LEN%>, false);
														}
														
														return retValue;
													}
												</aui:validator>
												<%
													//aui:validator name="minLength" 3/aui:validator
												%>
												<aui:validator name="maxLength"><%=DIAGNOSIS_LEN%></aui:validator>
											</aui:input>
											<input type="hidden" class="hidden_diagcode<%=i%>" id="<portlet:namespace/>diagcode<%=i%>" value="none">
										</td>
										<td>
											<aui:input name="<%=inputDiaDesId%>" id="<%=inputDiaDesId%>" maxlength="<%= DIAGNOSIS_LEN %>" cssClass="diaCodeDesc" readonly="true"></aui:input>
										</td>
										<td>
											<span>
												<% if (i == 1) { %>
													N/A
												<% } else { %>
													<i class="icon-trash" title="Delete" onClick="javascript:deleteDiagRow('<%=inputDiaCodeId%>','<%=inputDiaDesId%>')"></i>
												<% } %>
											</span>
										</td>
									</tr>
									<% } %>

								</c:otherwise>
							</c:choose>

						</tbody>
					</table>
				</aui:row>
			</aui:container>

			<c:if test="${fn:length(diagnosis) == 0}">
				<aui:row id="diagnosis-code-add" cssClass="checkForAddDiagLinkDisable">
					<aui:col span="12" id="addDiagnosisLink">
						<aui:a href="javascript:addDiagnosisRow()">
							<aui:icon image="plus" cssClass="pull-right" label="Add Diagnosis Code" />
						</aui:a>
					</aui:col>
				</aui:row>
			</c:if>

		</aui:fieldset>

		<aui:fieldset label="Claim Lines" id="claim-lines-fields">
			<aui:container>
				<aui:row>
					<span>Note: 1<sup>st</sup> Diagnosis Code will be considered as the Primary Diagnosis Code.</span>
					<table id="tbl-claim-lines" class="table table-striped table-bordered" style="width: 100%; border-collapse: collapse; border-spacing: 0;">
						<thead>
							<tr>
								<th class="textCenter">#</th>
								<th class="pDate textCenter">Date of Service *</th>
								<th class="pCode textCenter">Procedure Code</th>
								<th class="pDesc textCenter">Description</th>
								<th class="pMod textCenter">Proc Mod 1</th>
								<th class="pMod textCenter">Proc Mod 2</th>
								<th class="pMod textCenter">Proc Mod 3</th>
								<th class="pMod textCenter">Proc Mod 4</th>
								<th class="pRevcode textCenter">REV code</th>
								<th class="pPlace textCenter">Place of Service *</th>
								<th class="pDiag textCenter">1<sup>st</sup> Diagnosis Code *</th>
								<th class="textCenter">Related Diagnosis Code</th>
								<th class="pUnit textCenter">Units *</th>
								<th class="textCenter">Action</th>
							</tr>
						</thead>
						<tbody>

							<c:choose>
								<c:when test="${fn:length(procedure) > 0}">

									<c:set var="proresindex" value="${1}"></c:set>
									<c:forEach var="procedureList" items="${procedure}">

										<tr>
											<td class="textCenter"><c:out value="${proresindex}" /></td>
											<td class="textCenter"><c:out value="${procedureList.value.fromDateOfService}"></c:out></td>
											<td class="textCenter"><c:out value="${procedureList.value.procedureCode}"></c:out></td>
											<td class="textCenter"><c:out value="${procedureList.value.procedureDescription}"></c:out></td>
											<td class="textCenter"><c:out value="${procedureList.value.procModOne}"></c:out></td>
											<td class="textCenter"><c:out value="${procedureList.value.procModTwo}"></c:out></td>
											<td class="textCenter"><c:out value="${procedureList.value.procModThree}"></c:out></td>
											<td class="textCenter"><c:out value="${procedureList.value.procModFour}"></c:out></td>
											<td class="textCenter"><c:out value="${procedureList.value.revCode}"></c:out></td>
											<td class="textCenter"><c:out value="${procedureList.value.placeOfService}"></c:out></td>
											<td class="textCenter"><c:out value="${procedureList.value.primaryDiagnosisCode}"></c:out></td>
											<td class="textCenter"><c:out value="${procedureList.value.relatedDiagnosisCode}"></c:out></td>
											<td class="textCenter"><c:out value="${procedureList.value.units}"></c:out></td>
											<td class="textCenter"></td>
										</tr>

										<c:set var="proresindex" value="${proresindex+1}"></c:set>
									</c:forEach>

								</c:when>
								<c:otherwise>
									<tr class="procrow">
										<!--  This hidden field is required to get the claim line number in controller.Need to check if the location of the input tag causes any issues-->
										<input type="hidden" class="row_input procRowNumber1" name="<portlet:namespace/>procRowNumber1" id="<portlet:namespace/>procRowNumber1" value="1" readonly>

										<td>
										</td>
										<td>
											<input type="date" class="pDate procedureDateOfService1" name="<portlet:namespace/>procedureDateOfService1" id="<portlet:namespace/>procedureDateOfService1" value=""
											       onChange="javascript:checkDate(this.value, 1)">
											<div class="error1 error1-1" style="color: red"></div>
											<div class="error2 error2-1" style="color: red"></div>
										</td>
										<td>
											<input type="text" class="pCode code_procrow1 toUppercase" name="<portlet:namespace/>code_procrow1" id="<portlet:namespace/>code_procrow1" maxlength="<%= PROC_CODE_LEN %>" 
											       style="text-transform: uppercase" onFocus="javascript:saveCode(1, false)" onChange="javascript:getProcedureDescription(1); javascript:checkDuplicateProcedure(this.value, 1)">
											<input type="hidden" class="hidden_procrow1" id="<portlet:namespace/>hidden_procrow1" value="none">
											<div class="error3 error3-1" style="color: red"></div>
											<div class="error6 error6-1" style="color: red"></div>
										</td>
										<td>
											<input type="text" class="pDesc procedureCodes" name="<portlet:namespace/>codedesc_procrow1" id="<portlet:namespace/>codedesc_procrow1" readonly>
										</td>
										<td>
											<input type="text" class="pMod code_pmod1-1" name="<portlet:namespace/>code_pmod1-1" value="" maxlength="<%= MODIFIER_LEN %>" onChange="javascript:isValidProcedureModifier(this.value, 1, 1)">
											<div class="error5 error5-1-1" style="color: red"></div>
											<div class="error7 error7-1-1" style="color: red"></div>
										</td>
										<td>
											<input type="text" class="pMod code_pmod2-1" name="<portlet:namespace/>code_pmod2-1" value="" maxlength="<%= MODIFIER_LEN %>" onChange="javascript:isValidProcedureModifier(this.value, 2, 1)">
											<div class="error5 error5-2-1" style="color: red"></div>
											<div class="error7 error7-2-1" style="color: red"></div>
										</td>
										<td>
											<input type="text" class="pMod code_pmod3-1" name="<portlet:namespace/>code_pmod3-1" value="" maxlength="<%= MODIFIER_LEN %>" onChange="javascript:isValidProcedureModifier(this.value, 3, 1)">
											<div class="error5 error5-3-1" style="color: red"></div>
											<div class="error7 error7-3-1" style="color: red"></div>
										</td>
										<td>
											<input type="text" class="pMod code_pmod4-1" name="<portlet:namespace/>code_pmod4-1" value="" maxlength="<%= MODIFIER_LEN %>" onChange="javascript:isValidProcedureModifier(this.value, 4, 1)">
											<div class="error5 error5-4-1" style="color: red"></div>
											<div class="error7 error7-4-1" style="color: red"></div>
										</td>
										<td>
											<input type="text" class="pRevcode revcode_procrow1" name="<portlet:namespace/>revcode_procrow1" id="<portlet:namespace/>revcode_procrow1" maxlength="<%= REVISION_CODE_LEN %>" 
											       onChange="javascript:isValidRevCode(this.value, 1, true);javascript:isValidClaimLine('1')" >
											<div class="error11 error11-1" style="color: red"></div>
										</td>
										<td>
											<select name="<portlet:namespace/>placeOfService-row1" required class="pPlace placeOfService-row1">
												<option value="81">81</option>
												<option value="11">11</option>
												<option value="19">19</option>
												<option value="22">22</option>
											</select>
										</td>
										<td>
											<select class="pDiag pDiagSelect primaryDiagnosis1" name="<portlet:namespace/>primaryDiagnosis1" onChange="javascript:isValidPrimaryDiagnosis('1');javascript:isValidClaimLine('1')" >
											</select>
											<div class="error10 error10-1" style="color: red"></div>
										</td>
										<td>
											<select multiple="multiple" class="diagsSelect relDiagnosis1" name="<portlet:namespace/>relDiagnosis1" id="<portlet:namespace/>relDiagnosis1" onChange="javascript:isValidRelatedDiagnosis(1)" >
											</select>
											<!-- <div class="error8 error8-1" style="color:red"></div> -->
										</td>
										<td>
											<input type="text" class=" pUnit procedureUnits unit_procrow1" name="<portlet:namespace/>unit_procrow1" maxlength="<%= UNIT_LEN %>" onChange="javascript:isValidUnits(this.value, 1)">
											<div class="error4 error4-1" style="color: red"></div>
											<div class="error9 error9-1" style="color: red"></div>
										</td>
										<td>
											N/A
										</td>
									</tr>
									<tr>
										<td colspan="100%">
											<div class="error12 error12-1" align="center" style="color: red"></div>
										</td>
									</tr>
								</c:otherwise>
							</c:choose>
						</tbody>
					</table>
					
				</aui:row>
				<c:if test="${fn:length(procedure) == 0}">
					<aui:row id="claim-line-add" cssClass="checkForLinkDisable">
						<aui:col span="12" id="addClaimLink">
							<aui:a href="javascript:addNewClaimRow()">
								<aui:icon image="plus" cssClass="pull-right" label="Add Claim Line" />
							</aui:a>
							<input type="hidden" name="<portlet:namespace/>claimLineMaxNumber" id="<portlet:namespace/>claimLineMaxNumber" class="maxClaimLines" />
						</aui:col>
					</aui:row>
				</c:if>
			</aui:container>
		</aui:fieldset>

		<c:if test="${fn:length(procedure) == 0}">
			<aui:fieldset>
				<aui:button-row cssClass="btn-divider">
					<div onmouseenter="setSubmitButtonStatus()">
						<aui:button name="submitClaim" cssClass="pull-right" type="button" value="tce.button.submit.claim" onclick="javascript:checkValidForm()" />
					</div>
					<aui:button name="cancelClaim" cssClass="btn-gray pull-left cancelAction" type="button" value="tce.button.cancel" />
				</aui:button-row>
			</aui:fieldset>
		</c:if>

	</aui:form>

	<div style="margin-top: 25px; font-weight: bold;">
		CPT &copy;<%= currentYear %> American Medical Association. All rights reserved.
	</div>

</liferay-ui:panel>

<c:if test="${fn:length(responseLine) > 0}">

	<liferay-ui:panel title="${panelTitle}" collapsible="true" cssClass="panelClass">
		<aui:form name="trialClaimForm" cssClass="form-inline" action=""
			method="post" commandName="">
			<aui:fieldset label="" id="results-table">
				<aui:container>
					<aui:row>
						<table id="tbl-result-lines" class="table table-bordered" style="width: 100%; border-collapse: collapse; border-spacing: 0;">
							<thead>
								<tr>
									<th class="textCenter"></th>
									<th class="textCenter">#</th>
									<th class="textCenter">Procedure Code</th>
									<th class="textCenter">REV Code</th>
									<th class="textCenter">Decision Type</th>
									<th class="textCenter">Decision Rank</th>
									<th class="textCenter">Decision Code</th>
									<th class="textCenter">Decision Description</th>
									<th class="textCenter">Medical Policy Tag</th>
									<th class="textCenter">Medical Necessity Criteria</th>
									<th class="textCenter">Requested Units</th>
									<th class="textCenter">Approved Units</th>
									<th class="textCenter">Pay and Educate</th>
								</tr>
							</thead>
							<tbody>
								<c:set var="index" value="${1}"></c:set>
								<c:set var="details" value="${responseLine}"></c:set>
								<c:forEach var="procedureList" items="${procedure}" varStatus="status">
									<tr class="resultrow${index}">
										<c:set var="lineStr">${index}</c:set>
										<td class="textCenter"><i class="icon-expand-alt" onClick="javascript:toggleRow(this, ${index})"> </i></td>
										<td class="textCenter"><c:out value="${index}" /></td>
										<td class="textCenter"><c:out value="${details[index - 1].procedureCode}"></c:out></td>
										<td class="textCenter"><c:out value="${procedureList.value.revCode}"></c:out></td>
										<c:choose>
											<c:when test = "${details[index - 1].adviceDecisionType == '00'}">
												<td class="textCenter"><c:out value="No Advice" /></td>
											</c:when>

											<c:when test = "${details[index - 1].adviceDecisionType == '01'}">
												<td class="textCenter"><c:out value="Approve" /></td>
											</c:when>

											<c:when test = "${details[index - 1].adviceDecisionType == '02'}">
												<td class="textCenter"><c:out value="Deny" /></td>
											</c:when>

											<c:when test = "${details[index - 1].adviceDecisionType == '03'}">
												<td class="textCenter"><c:out value="Reduce Units" /></td>
											</c:when>

											<c:when test = "${details[index - 1].adviceDecisionType == '04'}">
												<td class="textCenter"><c:out value="Approve per Pay & Educate" /></td>
											</c:when>

											<c:when test = "${details[index - 1].adviceDecisionType == '05'}">
												<td class="textCenter"><c:out value="Error" /></td>
											</c:when>
										
											<c:otherwise>
										 		<td class="textCenter"><c:out value="" /></td>
											</c:otherwise>
										</c:choose>
										<td class="textCenter"><c:out value="1" /></td>
										<c:set var="primaryDecisionParts" value="${fn:split(details[index - 1].primaryDecisionCode, '``')}" />
										<td class="textCenter"><c:out value="${primaryDecisionParts[0]}" /></td>
										<td class="textCenter"><c:out value="${primaryDecisionParts[1]}" /></td>
										<td class="textCenter"><c:out value="${details[index - 1].primaryPolicyTag}" /></td>
										<td class="textCenter"><c:out value="${details[index - 1].primaryPolicyNecessityCriterion}" /></td>
										<td class="textCenter"><c:out value="${requestUnits[lineStr]}" /></td>
										<td class="textCenter"><c:out value="${details[index - 1].approvedServiceUnitCount}" /></td>
										<td class="textCenter"><c:out value="${details[index - 1].payAndEducate}" /></td>

										<c:if test="${ not empty details[index - 1].secondaryCodesData }">
											<c:set var="reasonIndex" value="${2}"></c:set>
											<c:forEach var="reason" items="${details[index - 1].secondaryCodesData}">
												<c:if test="${reason.secondaryDecisionCode != null}">
													<tr class="hide_row resultrow_${index}">
														<td colspan="5"></td>
														<td class="textCenter"><c:out value="${reasonIndex}" /></td>
														<td class="textCenter"><c:out value="${reason.secondaryDecisionCode}" /></td>
														<c:forEach var="secondaryCode" items="${decisionCodes.secondaryCode}">
															<c:if test="${reason.secondaryDecisionCode == secondaryCode.key }">
																<td class="textCenter"><c:out value="${secondaryCode.value}" /></td>
															</c:if>
														</c:forEach>
														<td class="textCenter"><c:out value="${reason.secondaryPolicyTag}" /></td>
														<td class="textCenter"><c:out value="${reason.secondaryPolicyNecessityCriteria}" /></td>
														<td class="textCenter"></td>
														<td class="textCenter"></td>
														<td class="textCenter"></td>
													</tr>
													<c:set var="reasonIndex" value="${reasonIndex+1}"></c:set>
												</c:if>
											</c:forEach>
										</c:if>
									</tr>
									<c:set var="index" value="${index+1}"></c:set>
								</c:forEach>
							</tbody>
						</table>
					</aui:row>
				</aui:container>
			</aui:fieldset>
			<aui:fieldset>
				<aui:button-row cssClass="btn-divider">
					<aui:button name="newClaim" cssClass="pull-right newAction" value="tce.button.new.claim" onClick="javascript:showNewTrialClaim()" />
					<aui:button name="closeBtn" cssClass="btn-gray pull-left cancelAction" type="cancel" value="tce.button.cancel" />
				</aui:button-row>
			</aui:fieldset>
		</aui:form>
	</liferay-ui:panel>

	<div style="margin-top: 25px; font-weight: bold;">
		The results of the Trial Claim Advice Tool are for simulation purposes only.  The health plan retains responsibility for adjudicating claims,   
		including without limitation assessing medical necessity and determining coverage under the applicable member&rsquo;s benefit plan.
	</div>

	<div style="margin-top: 25px; font-weight: bold;">
		CPT &copy;<%= currentYear %> American Medical Association. All rights reserved.
	</div>

</c:if>
