/**
 * Description This file contain the javascript for Trial Claim.
 * 
 * CHANGE History
 * 	Version 1.0
 * 		Initial version.
 * 	Version 1.1
 * 		Added portlet namespace to input fields in add claim rows functionality
 * 	Version 1.2
 *		Added functions needed for validation Added errors to Claim Line table.
 *	Version 1.3
 *		Fixed isValidAlphaNumeric test.
 *	Version 1.4
 *		Hide error messages if Claim Line is empty in isValidClaimLine.
 *	Version 1.5
 *		Make the related diagnosis manditory in isValidClaimLine.
 *	Version 1.6
 *		Fixed problems with read only entry tables.
 *	Version 1.7
 *		Removed the related diagnosis when the diagnosis code is changed or deleted.
 *	Version 1.8
 *		Added a function to determine if a diagnosis codes exists in the related diagnosis.
 *	Version 1.9
 *		Fixed problems adding diagnosis codes to related diagnosis in removeRelatedDiagnosis.
 * Version 1.10
 *		Set maximum related diagnosis to 4 in addClaimRow.
 *	Version 1.11
 *		Added member search.
 *	Version 1.12
 *		Fixed the busy spinner fo IE
 *	Version 1.13
 *		Added the REV code to claim lines
  *  Version 1.14
  *		Allow 3 digit REV codes.
 * 
 */
makeFormResponsive();
formatRequiredLabels('*');

var maxDiagnosisRows = 24;
var maxClaimRows = 999;
var maxModifierRows = 3;

var numDiagnosisRows = 2;
var numClaimRows = 2;

var invalidDiagnosisMsg = "Please enter a valid diagnosis code";
var invalidProcedureMsg = "Please enter a valid procedure code";

function makeFormResponsive() {
	var groups = document.getElementById('main-content')
			.getElementsByClassName('control-group');

	for (var i = 0, n = groups.length; i < n; i++) {
		if (groups[i].getAttribute('class').indexOf('no-reformat') < 0) {
			groups[i].setAttribute('class', groups[i].getAttribute('class') + ' row-fluid');

			var label = groups[i].getElementsByTagName('label')[0];

			if (label) {
				label.setAttribute('class', label.getAttribute('class') + ' span5');
			}

			var input = groups[i].getElementsByTagName('input')[0];

			if (input) {
				input.setAttribute('class', input.getAttribute('class') + ' span7');
			}
		}
	}
}

function addTableIcons() {
	var rows = document.getElementById('claimLinesTable').getElementsByTagName(
			'tr');

	for (var i = 1, n = rows.length; i < n; i++) {
		var td = rows[i].getElementsByTagName('td');
		td[td.length - 1].innerHTML = '<i class="icon-remove pull-right></i>';
	}
}

function formatRequiredLabels(text) {
	var reqLabels = document.getElementById('main-content')
			.getElementsByClassName('label-required');

	for (var i = 0, n = reqLabels.length; i < n; i++) {
		reqLabels[i].innerHTML = text;
	}
}

function decode(str) {
	str = str.replace(/&amp;/g, "&");
	str = str.replace(/&gt;/g, ">");
	str = str.replace(/&lt;/g, "<");
	str = str.replace(/&quot;/g, "\"");
	str = str.replace(/&#039;/g, "'");
	return str;
}

function checkValidForm() {
	if (isValidForm()) {
		showBusySign();
	}
}

function showBusySign() {

	// The form must be submitted before the animated GIF will move in IE
	$('#' + getNamespace() + 'trialClaimForm').submit();

	document.getElementById('busy_indicator').style.display = 'block';
}

function addRelatedDiag(diagStr, valStr) {
	var addStr = "<option value='" + diagStr + "'>" + valStr + "</option>";

	$(".diagsSelect").append(addStr);
}

function removeRelatedDiag(diagStr) {
	var removeStr = ".diagsSelect option[value='" + diagStr + "']";

	removeSelectedRelatedDiag(diagStr);
	$(removeStr).remove();
}

function removeSelectedRelatedDiag(diagStr) {
	var selectedElem = document
			.getElementsByClassName('select2-selection__choice');

	for (var i = 0; i < selectedElem.length; i++) {
		// innerText for IE, textContent for other browsers
		// var text = selectedElem[i].innerText || selectedElem[i].textContent;
		if (selectedElem[i].title == diagStr) {
			selectedElem[i].remove();
		}
	}
}

function isValid(_dat, _len, _requiredFlag) {
	var rtnValue = false;
	var length = 0;
	
	if (_dat != null) {
		length = _dat.length;
	}

	if (length == 0) {
		if (!_requiredFlag) {
			rtnValue = true;
		}
	} else if (length <= _len) {
		rtnValue = true;
	}

	return rtnValue;
}

function isValidDigit(_dat, _len, _requiredFlag, _exactLenFlag) {
	var failFlag = false;
	var length = _dat.length;

	if (length == 0) {
		if (!_requiredFlag) {
			failFlag = true;
		}
	} else {
		var isNumber = /^\d+$/.test(_dat);
		if (isNumber && (length <= _len)) {
			failFlag = true;
			if (_exactLenFlag && (length < _len)) {
				failFlag = false;
			}
		}
	}

	return failFlag;
}

function isValidAlpha(_dat, _len, _requiredFlag) {
	var failFlag = false;
	var length = _dat.length;

	if (length == 0) {
		if (!_requiredFlag) {
			failFlag = true;
		}
	} else {
		var isAlpha = /^[a-zA-Z]+$/.test(_dat);
		if (isAlpha && (length <= _len)) {
			failFlag = true;
		}
	}

	return failFlag;
}

function isValidAlphaNumeric(_dat, _len, _requiredFlag, _matchLenFlag) {
	var failFlag = false;
	var length = _dat.length;

	if (length == 0) {
		if (!_requiredFlag) {
			failFlag = true;
		}
	} else {
		var isValid = /^[a-zA-Z0-9]+$/.test(_dat);
		if (_matchLenFlag) {
			if (isValid && (length == _len)) {
				failFlag = true;
			}
		} else if (isValid && (length <= _len)) {
			failFlag = true;
		}
	}

	return failFlag;
}

function isValidName(_dat, _len, _requiredFlag) {
	var failFlag = false;
	var length = _dat.length;

	if (length == 0) {
		if (!_requiredFlag) {
			failFlag = true;
		}
	} else {
		if ((length <= _len)) {
			failFlag = true;
		}
	}

	return failFlag;
}

function isValidDate(_dateString, _requiredFlag) {
	var failFlag = true;
	var browserStr = getBrowser();
	var parts;
	var day;
	var month;
	var year;

	if (_dateString.length > 0) {

		// First check for the pattern
		if ((browserStr == "IE") || (browserStr == "Firefox")
				|| (browserStr == "unknown")) {

			// Check format mm/dd/yyyy
			if (!/^\d{1,2}\/\d{1,2}\/\d{4}$/.test(_dateString)) {
				failFlag = false;
			}

			if (failFlag) {

				// Parse the date parts to integers
				parts = _dateString.split("/");
				day = parseInt(parts[1], 10);
				month = parseInt(parts[0], 10);
				year = parseInt(parts[2], 10);
			}
		} else {

			// Check format yyyy-mm-dd
			if (!/^\d{4}-\d{1,2}-\d{1,2}$/.test(_dateString)) {
				failFlag = false;
			}

			if (failFlag) {
				// Parse the date parts to integers
				parts = _dateString.split("-");
				day = parseInt(parts[2], 10);
				month = parseInt(parts[1], 10);
				year = parseInt(parts[0], 10);
			}
		}

		if (failFlag) {

			// Check the ranges of month and year
			if ((year < 1000) || (year > 3000) || (month == 0) || (month > 12)) {
				failFlag = false;
			}

			var monthLength = [ 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 ];

			// Adjust for leap years
			if ((year % 400 == 0) || ((year % 100 != 0) && (year % 4 == 0))) {
				monthLength[1] = 29;
			}

			// Check the range of the day
			failFlag = ((day > 0) && (day <= monthLength[month - 1]));
		}
	} else {
		if (_requiredFlag) {
			failFlag = false;
		}
	}
	return failFlag;
}

// Get start date for each browser type
function serviceStartDate() {
	var dat;
	var d = "01";
	var m = "04";
	var y = "2016";

	// Format for all other browsers is yyyy-mm-dd
	dat = y + "-" + m + "-" + d;

	return dat;
}

/*
 * Convert the date to yyyy-MM-dd from MM/dd/yyyy.
 */
function convertDateFormat(_dat) {
	var rtnValue = "";

	// Parse the first date to individual parts
	var day = _dat.substring(3, 5);
	var month = _dat.substring(0, 2);
	var year = _dat.substring(6, 10);

	rtnValue = year + "-" + month + "-" + day;

	return rtnValue;
}

function checkDate(dt, idx) {
	var browserStr = getBrowser();
	var failFlag = false;
	var err1 = ".error1-" + idx;
	var err2 = ".error2-" + idx;

	// Is this a valid date format
	if (isValidDate(dt, true)) {
		$(err1).hide();

		// Convert the IE date to the correct format
		if ((browserStr == "IE") || (browserStr == "Firefox")
				|| (browserStr == "unknown")) {
			dt = convertDateFormat(dt);
		}

		// Is the date 4/1/2016 or later
		if (dt >= serviceStartDate()) {
			$(err2).hide();
		} else {
			failFlag = true;
			$(err1).hide();
			$(err2).show();
		}
	} else {
		failFlag = true;
		$(err1).show();
		$(err2).hide();
	}

	return failFlag;
}

function isValidProcedureCode(procCd, idx, reqFlag) {
	var failFlag = false;
	var err3 = ".error3-" + idx;
	var procedureCodeLen = 5;

	// Test for a valid field
	if (isValidAlphaNumeric(procCd, procedureCodeLen, reqFlag, true)) {
		$(err3).hide();
	} else {
		failFlag = true;
		$(err3).show();
	}
	
	// Test for a valid procedure code
	var procedureDescItem = $("#" + getNamespace() + "codedesc_procrow" + idx).val();
	if (procedureDescItem == invalidProcedureMsg) {
		failFlag = true;
	}

	return failFlag;
}

function isValidPrimaryDiagnosis(idx) {
	var failFlag = false;
	var err10 = ".error10-" + idx;

	// Test for at least one diagnosis selected
	if ($('.primaryDiagnosis' + idx).val() != "") {
		$(err10).hide();
	} else {
		failFlag = true;
		$(err10).show();
	}

	return failFlag;
}

function isValidRelatedDiagnosis(idx) {
	var failFlag = false;
	var err8 = ".error8-" + idx;

	// Test for at least one diagnosis selected
	if ($('.relDiagnosis' + idx).select2('data').length > 0) {
		$(err8).hide();
	} else {
		failFlag = true;
		$(err8).show();
	}

	return failFlag;
}

function isValidUnits(unit, idx) {
	var failFlag = false;
	var err4 = ".error4-" + idx;
	var err9 = ".error9-" + idx;
	var unitLen = 5;

	// Test for a valid field
	if (isValidDigit(unit, unitLen, true)) {
		$(err4).hide();

		// Convert the units to a number
		var intUnit = parseInt(unit);
		if (intUnit > 0) {
			$(err9).hide();
		} else {
			failFlag = true;
			$(err4).hide();
			$(err9).show();
		}
	} else {
		failFlag = true;
		$(err4).show();
		$(err9).hide();
	}

	return failFlag;
}

function isValidProcedureModifier(mod, num, idx) {
	var failFlag = false;
	var err5 = ".error5-" + num + "-" + idx;
	var modLen = 2;

	// Test for a valid field
	if (isValidAlphaNumeric(mod, modLen, false, false)) {
		$(err5).hide();
	} else {
		failFlag = true;
		$(err5).show();
	}

	// Test for duplicates
	if (checkDuplicateModifier(idx)) {
		failFlag = true;
	}

	return failFlag;
}

function isValidRevCode(revCd, idx, reqFlag) {
	var failFlag = false;
	var err11 = ".error11-" + idx;
	var revCodeLenMin = 3;
	var revCodeLenMax = 4;

	// Test for a valid field
	if (isValidDigit(revCd, revCodeLenMin, reqFlag, true) || isValidDigit(revCd, revCodeLenMax, reqFlag, true)) {
		$(err11).hide();
	} else {
		failFlag = true;
		$(err11).show();
	}
	return failFlag;
}

function manageProcedureAndRevCodeErrorMsg(idx) {
	var failFlag = false;
	var procedureCodeStatus = $('.error3-' + idx).css('display');
	var revCodeStatus = $('.error11-' + idx).css('display');
	var bothError = ".error12-" + idx;

	// Test for group error message
	if ((procedureCodeStatus == "block") && (revCodeStatus == "block")) {
		$(bothError).parents("tr").show();
		failFlag = true;
	} else {
		$(bothError).parents("tr").hide();
	}

	return failFlag;
}

function isValidClaimLine(idx) {
	var failFlag = false;

	// Test for read only mode
	if ($(".relDiagnosis" + idx).length > 0) {
		var dateOfService = $('.procedureDateOfService' + idx).val();
		var procedureCode = $('.code_procrow' + idx).val().replace(/^\s+|\s+$/g, "");
		var units = $('.unit_procrow' + idx).val().replace(/^\s+|\s+$/g, "");
		var revCode = $('.revcode_procrow' + idx).val().replace(/^\s+|\s+$/g, "");
		var mod1 = $('.code_pmod1-' + idx).val().replace(/^\s+|\s+$/g, "");
		var mod2 = $('.code_pmod2-' + idx).val().replace(/^\s+|\s+$/g, "");
		var mod3 = $('.code_pmod3-' + idx).val().replace(/^\s+|\s+$/g, "");
		var mod4 = $('.code_pmod4-' + idx).val().replace(/^\s+|\s+$/g, "");
		var primaryDiag = $('.primaryDiagnosis' + idx).val();
		var relatedDiagCount = $(".relDiagnosis" + idx).select2('data').length;		// This is no longer required

		if ((idx == 1) || (dateOfService != "") || (procedureCode != "") || (units != "") || (revCode != "") || (mod1 != "") || (mod2 != "") || (mod3 != "") || (mod4 != "") || (primaryDiag != "")) {

			// Row is mandatory
			// date of service is required
			if (checkDate(dateOfService, idx)) {
				failFlag = true;
			}

			// procedure code is required if the REV code is blank or not valid
			var reqFlag = false;
			if (revCode == "") {
				reqFlag = true;
			} else if (isValidRevCode(revCode, idx, true)) {
				reqFlag = true;
			}
			if (isValidProcedureCode(procedureCode, idx, reqFlag)) {
				failFlag = true;
			}

			// primary diagnosis is required
			if (isValidPrimaryDiagnosis(idx)) {
				failFlag = true;
			}

			// units is required
			if (isValidUnits(units, idx)) {
				failFlag = true;
			}

			// REV code is required if the procedure code is blank or not valid
			reqFlag = false;
			if (procedureCode == "") {
				reqFlag = true;
			} else if (isValidProcedureCode(procedureCode, idx, true)) {
				reqFlag = true;
			}
			if (isValidRevCode(revCode, idx, reqFlag)) {
				failFlag = true;
			}

			// validate the procedure modifier codes
			if (isValidProcedureModifier(mod1, "1", idx)) {
				failFlag = true;
			}
			if (isValidProcedureModifier(mod2, "2", idx)) {
				failFlag = true;
			}
			if (isValidProcedureModifier(mod3, "3", idx)) {
				failFlag = true;
			}
			if (isValidProcedureModifier(mod4, "4", idx)) {
				failFlag = true;
			}
		} else {

			// Hide the error messages
			var err1 = ".error1-" + idx;
			var err2 = ".error2-" + idx;
			var err3 = ".error3-" + idx;
			var err4 = ".error4-" + idx;
			var err8 = ".error8-" + idx;
			var err10 = ".error10-" + idx;
			var err11 = ".error11-" + idx;
			var err12 = ".error12-" + idx;

			$(err1).hide();
			$(err2).hide();
			$(err3).hide();
			$(err4).hide();
			$(err8).hide();
			$(err10).hide();
			$(err11).hide();
			$(err12).parents("tr").hide();
		}
		if (manageProcedureAndRevCodeErrorMsg(idx)) {
			failFlag = true;
		}
	}

	return failFlag;
}

function isAnyInvalidCode(diagFlag) {
	var errorFlag = false;
	var nextDesc = "";

	for (var i = 1; i <= maxDiagnosisRows; i++ ) {
		if (diagFlag) {
			nextDesc = $("#" + getNamespace() + "diaDesc" + i).val();
			
			// Test any entered diagnosis for any invalid code
			if (nextDesc != "") {
				if (nextDesc == invalidDiagnosisMsg) {
					errorFlag = true;
					break;
				}
			}
		} else {
			nextDesc = $("#" + getNamespace() + "codedesc_procrow1").val();
			
			// Test any entered procedure for any invalid code
			if (nextDesc != "") {
				if (nextDesc == invalidProcedureMsg) {
					errorFlag = true;
					break;
				}
			}
		}
	}
	return errorFlag;
}

function isValidDiagnosis(_dat, _len, _requiredFlag) {
	var rtnValue = false;
	var stripDat = _dat.replace(/\./g, '');

	rtnValue = isValidAlphaNumeric(stripDat, _len, _requiredFlag, false);

	return rtnValue;
}

function isValidClaimLines() {
	var validFlag = true;
	var lineSelected = 1;
	var numRows = numTableRows('tbl-claim-lines');

	// Loop throught all the rows
	for (var i = 1; lineSelected <= numRows; i++) {
		var dateOfServiceItem = $(".procedureDateOfService" + i).val().length;
		var procedureCodeItem = $(".code_procrow" + i).val().length;
		var unitsItem = $(".unit_procrow" + i).val().length;
		var revCodeItem = $(".revcode_procrow" + i).val().length;
		var mod1Item = $(".code_pmod1-" + i).val().length;
		var mod2Item = $(".code_pmod2-" + i).val().length;
		var mod3Item = $(".code_pmod3-" + i).val().length;
		var mod4Item = $(".code_pmod4-" + i).val().length;
		var relatedDiagnosisItem = $(".relDiagnosis" + i).select2("data").length;
		
		// Is the row visible with any values set?  The first row is required,
		if ((i == 1) || (dateOfServiceItem > 0) || (procedureCodeItem > 0) || (unitsItem > 0) || (revCodeItem > 0) || (mod1Item > 0) || (mod2Item > 0) || (mod3Item > 0) || (mod4Item > 0) || (relatedDiagnosisItem > 0)) {
			if (isValidClaimLine(i)) {
				validFlag = false;
				
				// Exit the loop if any row fails
				break;
			}
		}

		// Go to the next row
		lineSelected++;
	}

	return validFlag;
}

function numTableRows(tbl) {
	var rtnValue = 0;

	if (tbl == 'tbl-claim-lines') {
		rtnValue = $('.procrow').length;
	} else {
		rtnValue = $('.showDiagRow').length;
	}

	return rtnValue;
}

function saveCode(idx, diagnosisFlag) {
	var elemCodeValue = null;
	var elemHidden = null;

	if (diagnosisFlag) {
		elemCodeValue = $('.diagcode' + idx).val();
		elemHidden = $('.hidden_diagcode' + idx);
	} else {
		elemCodeValue = $('.code_procrow' + idx).val();
		elemHidden = $('.hidden_procrow' + idx);
	}

	elemHidden.val(elemCodeValue);
}

function getDescriptionStub(idx, portletNamespace, diagnosisFlag) {
	var elemCodeValue = null;
	var elemDesc = null;

	if (diagnosisFlag) {
		var elemHiddenValue = $('#hidden_diagrow' + idx).val();

		elemCodeValue = $('input[name=code_diagrow' + idx + ']').val();
		elemDesc = $('input[name=codedesc_diagrow' + idx + ']');

		// Remove the previous diagnosis from the related diagnosis lists
		removeRelatedDiag(elemHiddenValue);

		// Add the diagnosis to the related diagnosis lists
		addRelatedDiag(elemCodeValue, elemCodeValue);
	} else {
		elemCodeValue = $('input[class="pCode code_procrow' + idx + '"]').val();
		elemDesc = $('input[name=' + portletNamespace + 'codedesc_procrow' + idx + ']');
	}

	elemDesc.val("Description for " + elemCodeValue);
}

function populatePrimaryDiagnosis(selectElem) {
	var addStr = null;
	var cellStr = null;

	// removing all the previously existing options
	$("." + selectElem).find('option').remove();

	// Add a blank option
	addStr = "<option value=''></option>";
	$("." + selectElem).append(addStr);

	// Get the available diagnosis codes
	$('.showDiagRow').find('input.success-field').each(
			function() {
				cellStr = $(this).val();
				if (cellStr != 'none') {
					addStr = "<option value='" + cellStr + "'>" + cellStr + "</option>";
					$("." + selectElem).append(addStr);
				}
			});
}

function populateRelatedDiagnosis(selectElem) {
	var addStr = null;
	var cellStr = null;

	// removing all the previously existing options
	$("." + selectElem).find('option').remove();

	// Get the available diagnosis codes
	$('.showDiagRow').find('input.success-field').each(
			function() {
				cellStr = $(this).val();
				if (cellStr != 'none') {
					addStr = "<option value='" + cellStr + "'>" + cellStr
							+ "</option>";
					$("." + selectElem).append(addStr);
				}
			});

	/*
	 * Update the selection
	 * $("#_trialclaimentry_WAR_avalonclaimsresearchportlet_relDiagnosis1").select2('data').length
	 * $("#_trialclaimentry_WAR_avalonclaimsresearchportlet_relDiagnosis1").select2('data')[0].id
	 * $("#_trialclaimentry_WAR_avalonclaimsresearchportlet_relDiagnosis1").val([]).trigger("change");
	 * 
	 * $("#_trialclaimentry_WAR_avalonclaimsresearchportlet_relDiagnosis1").val(["CODE1",
	 * "CODE2"]).trigger("change");
	 */
}

function addRelatedDiagnosis(selectElem, itm) {
	var addStr = null;

	addStr = "<option value='" + itm + "'>" + itm + "</option>";
	$("." + selectElem).append(addStr);
}

function addPrimaryDiagnosis(selectElem, itm) {
	var addStr = null;

	addStr = "<option value='" + itm + "'>" + itm + "</option>";
	$("." + selectElem).append(addStr);
}

function removePrimaryDiagnosis(id) {
	$(".pDiagSelect option[value='" + id + "']").remove();
}

function removeRelatedDiagnosis(id) {
	var lineSelected = 1;
	var numRows = numTableRows('tbl-claim-lines');

	// Remove the id from the selected lists
	// Loop throught all the rows
	for (var rowCounter = 1; lineSelected <= numRows; rowCounter++) {
		var procedureCodeItem = $(".code_procrow" + rowCounter).length;

		// Was the row found?
		if (procedureCodeItem > 0) {

			// Get the current selections
			var currentLen = 0;
			var currentVal = "";
			if ($(".relDiagnosis" + rowCounter).select2('data').length != 0) {
				currentLen = $(".relDiagnosis" + rowCounter).select2('data').length;
				currentVal = $(".relDiagnosis" + rowCounter).val();
			}

			// Clear the selections
			$(".relDiagnosis" + rowCounter).select2("val", "");

			// Add all selections except the one being removed
			var newVal = [];
			for (var cnt = 0; cnt < currentLen; cnt++) {
				if (currentVal[cnt] != id) {
					newVal.push(currentVal[cnt]);
				}
			}
			$('.relDiagnosis' + rowCounter).val(newVal).trigger('change');
		}

		// Go to the next row
		lineSelected++;
	}

	// Remove the id from the list of choices
	$(".diagsSelect option[value='" + id + "']").remove();
}

function removeOldDiagnosisCode(idx) {
	var currentValue = $('.diagcode' + idx).val().toLocaleUpperCase();
	var oldValue = $('.hidden_diagcode' + idx).val().toLocaleUpperCase();

	if ((oldValue != "") && (oldValue != currentValue)) {

		// Remove the old value from the primary diagnosis lists
		removePrimaryDiagnosis(oldValue);

		// Remove the old value from the releated diagnosis lists
		removeRelatedDiagnosis(oldValue);
	}
}

function diagnosisFound(val) {
	var foundFlag = false;

	val = val.toLocaleUpperCase();

	// Loop through all of the valid codes
	$('.showDiagRow').find('input.diaCode').each(function() {
		var nextVal = $(this).val().toLocaleUpperCase();

		// Skip blank diagnosis codes
		if (nextVal != "") {
			if (nextVal == val) {
				foundFlag = true;
			}
		}
	});

	return foundFlag;
}

function addDiagnosisRow() {
	var idx = ++numDiagnosisRows;
	var numRows = 0;

	$('#tbl-diagnosis-lines > tbody:last').append('<tr class="diagrow diagrow' + idx + '"><td>' + '<input type="text" name="code_diagrow' + idx + '" value="" onFocus="saveCode(' + idx +
							                      ', true)" onChange="getDescriptionStub(' + idx + ', null, true)">' + '<input type="hidden" id="hidden_diagrow' + idx + '" value="none"></td>' +
							                      '<td><input type="text" class="diagnosisCodes" name="codedesc_diagrow' + idx + '" value="" disabled></td>' + 
							                      '<td><i class="icon-trash" title="Delete" onClick="deleteRow(this, ' + idx + ', &quot;diagnosis&quot;)"></i></td></tr>');

	// Set focus on the diagnosis code of added row
	$('input[name=code_diagrow' + idx + ']').focus();

	numRows = numTableRows('tbl-diagnosis-lines');

	// Hide the link when the max is reached
	if (numRows == maxDiagnosisRows) {
		displayAddLink('diagnosis', false, 0);
	}
}

function addClaimRow(portletNamespace, contextPath) {
	var numRows = numTableRows('tbl-claim-lines') + 1;

	var error1Str   = '<div class="error1 error1-' + numRows + '" style="color:red"></div>';
	var error2Str   = '<div class="error2 error2-' + numRows + '" style="color:red"></div>';
	var error3Str   = '<div class="error3 error3-' + numRows + '" style="color:red"></div>';
	var error4Str   = '<div class="error4 error4-' + numRows + '" style="color:red"></div>';
	var error5_1Str = '<div class="error5 error5-1-' + numRows + '" style="color:red"></div>';
	var error5_2Str = '<div class="error5 error5-2-' + numRows + '" style="color:red"></div>';
	var error5_3Str = '<div class="error5 error5-3-' + numRows + '" style="color:red"></div>';
	var error5_4Str = '<div class="error5 error5-4-' + numRows + '" style="color:red"></div>';
	var error6Str   = '<div class="error6 error6-' + numRows + '" style="color:red"></div>';
	var error7_1Str = '<div class="error5 error7-1-' + numRows + '" style="color:red"></div>';
	var error7_2Str = '<div class="error5 error7-2-' + numRows + '" style="color:red"></div>';
	var error7_3Str = '<div class="error5 error7-3-' + numRows + '" style="color:red"></div>';
	var error7_4Str = '<div class="error5 error7-4-' + numRows + '" style="color:red"></div>';
	var error8Str   = '<div class="error8 error8-' + numRows + '" style="color:red"></div>';
	error8Str       = ''; // Change to not required
	var error9Str   = '<div class="error9 error9-' + numRows + '" style="color:red"></div>';
	var error10Str  = '<div class="error10 error10-' + numRows + '" style="color:red"></div>';
	var error11Str  = '<div class="error11 error11-' + numRows + '" style="color:red"></div>';
	var error12Str  = '<div class="error12 error12-' + numRows + '" align="center" style="color:red"></div>';

	// Validation
	lineVal = 'isValidClaimLine(' + numRows + ')';
	dupCheck = 'checkDuplicateProcedure(this.value, ' + numRows + ');';
	procLineVal = 'isValidClaimLine(' + numRows + ')';
	procCodeVal = 'getProcedureDescription(' + numRows + '); ' + dupCheck + procLineVal;
	dateOfServiceVal = 'checkDate(this.value, ' + numRows + '); ' + lineVal;
	pdVal = 'isValidPrimaryDiagnosis(' + numRows + '); ' + lineVal;
	rdVal = 'isValidRelatedDiagnosis(' + numRows + '); ' + lineVal;
	unitsVal = 'isValidUnits(this.value, ' + numRows + '); ' + lineVal;
	revCodeVal = 'isValidRevCode(this.value, ' + numRows + ', true);' + lineVal;

	// hidden input field is required to get the claim line number in
	// controller.Need to check if the location of the input tag causes any
	// issues
	$('#tbl-claim-lines > tbody:last').append('<tr class="procrow"><input type="hidden" class="row_input procRowNumber' + numRows + '" readonly name="' + portletNamespace + 'procRowNumber' +
							                  numRows + '" id="' + portletNamespace + 'procRowNumber' + numRows + '" value="' + numRows + '">' + '<td></td>' +
							                  '<td><input type="date" class="pDate procedureDateOfService' + numRows + '" name="' + portletNamespace + 'procedureDateOfService' + numRows +
							                  '" id="' + portletNamespace + 'procedureDateOfService' + numRows + '" value="" onChange="' + dateOfServiceVal + '">' + error1Str + error2Str +
							                  '</td><td><input type="text" class="pCode code_procrow' + numRows + ' toUppercase" name="' + portletNamespace + 'code_procrow' + numRows +
							                  '" onFocus="saveCode(' + numRows + ', false)" id="' + portletNamespace + 'code_procrow' + numRows + '" onChange="' + procCodeVal +
							                  '" maxlength="5" style="text-transform:uppercase" >' + error3Str + error6Str + '<input type="hidden" class="hidden_procrow' + numRows +
							                  '" id="hidden_procrow' + numRows + '" value="none"></td><td><input type="text" class="pDesc codedesc_procrow' + numRows + '" name="' +
							                  portletNamespace + 'codedesc_procrow' + numRows + '" id="' + portletNamespace + 'codedesc_procrow' + numRows + '" onChange="' + lineVal +
							                  '"  readonly ></td>' + '<td><input type="text" class="pMod code_pmod1-' + numRows + '" name="' + portletNamespace + 'code_pmod1-' + numRows + 
							                  '" value="" maxlength="2" onChange="' + lineVal + '" onChange="isValidProcedureModifier(this.value, 1, ' + numRows + '); ' + lineVal + '">' + 
							                  error5_1Str + error7_1Str + '</td>' + '<td><input type="text" class="pMod code_pmod2-' + numRows + '" name="' + portletNamespace + 
							                  'code_pmod2-' + numRows + '" value="" maxlength="2" onChange="' + lineVal + '" onChange="isValidProcedureModifier(this.value, 2, ' + numRows + '); ' + 
							                  lineVal + '">' + error5_2Str + error7_2Str + '</td>' + '<td><input type="text" class="pMod code_pmod3-' + numRows + '" name="' + portletNamespace + 
							                  'code_pmod3-' + numRows + '" value="" maxlength="2" onChange="' + lineVal + '" onChange="isValidProcedureModifier(this.value, 3, ' + numRows + '); ' + 
							                  lineVal + '">' + error5_3Str + error7_3Str + '</td>' + '<td><input type="text" class="pMod code_pmod4-' + numRows + '" name="' + portletNamespace + 
							                  'code_pmod4-' + numRows + '" value="" maxlength="2" onChange="' + lineVal + '" onChange="isValidProcedureModifier(this.value, 4, ' + numRows + '); ' + 
							                  lineVal + '">' + error5_4Str + error7_4Str + '</td>' + '<td><input type="text" class="pRevcode revcode_procrow' + numRows + '" name="' + 
							                  portletNamespace + 'revcode_procrow' + numRows + '" ' + 'id="' + portletNamespace + 'revcode_procrow' + numRows + '" maxlength="4" onChange="' + 
							                  revCodeVal + '" >' + error11Str + '</td><td><select class="pPlace placeOfService-row' + numRows + '" name="' + portletNamespace + 'placeOfService-row' + 
							                  numRows + '" required><option value="81">81</option><option value="11">11</option><option value="19">19</option><option value="22">22</option></select></td>' + 
							                  '<td><select class="pDiag pDiagSelect primaryDiagnosis' + numRows + '" name="' + portletNamespace + 'primaryDiagnosis' + numRows + '" id="' + 
							                  portletNamespace + 'primaryDiagnosis' + numRows + '"onChange="' + pdVal + '"></select>' + error10Str + '</td><td><select multiple="multiple"' +
							                  'class="diagsSelect relDiagnosis' + numRows + '" name="' + portletNamespace + 'relDiagnosis' + numRows + '" onChange="' + rdVal  + '"></select>' + 
							                  error8Str + '</td><td><input type="text" class="pUnit procedureUnits unit_procrow' + numRows + '" name="' + portletNamespace + 'unit_procrow' + numRows + 
							                  '" maxlength="5" onChange="' + unitsVal + '">' + error4Str + error9Str + '</td><td><i class="icon-trash proctrash' + numRows + '" title="Delete" onClick="deleteRow(this, ' + 
							                  numRows + ', &quot;claim&quot;)"></i></td></tr>' + '<tr><td colspan="100%">' + error12Str + '</td></tr>');

	// Add diagnosis codes to primary diagnosis dropdown
	populatePrimaryDiagnosis('primaryDiagnosis' + numRows);

	// Add diagnosis codes to relDiagnosis
	populateRelatedDiagnosis('relDiagnosis' + numRows);

	// Set up the related diagnosis
	$(".diagsSelect").select2();
	$(".diagsSelect").select2({
		maximumSelectionLength : 3
	});

	// Update the index column
	$('.procrow').each(function(i) {
		$("td", this).eq(0).html(i + 1);
	});

	// Hide all detail rows
	$(".hide_row").hide();

	// Add the error messages and hide them
	initializeErrors();
	$('.error1-' + numRows).hide();
	$('.error2-' + numRows).hide();
	$('.error3-' + numRows).hide();
	$('.error4-' + numRows).hide();
	$('.error5-1-' + numRows).hide();
	$('.error5-2-' + numRows).hide();
	$('.error5-3-' + numRows).hide();
	$('.error5-4-' + numRows).hide();
	$('.error6-' + numRows).hide();
	$('.error7-1-' + numRows).hide();
	$('.error7-2-' + numRows).hide();
	$('.error7-3-' + numRows).hide();
	$('.error7-4-' + numRows).hide();
	$('.error8-' + numRows).hide();
	$('.error9-' + numRows).hide();
	$('.error10-' + numRows).hide();
	$('.error11-' + numRows).hide();
	$('.error12-' + numRows).parents("tr").hide();

	// Set focus on the procedure code of added row
	$('input[class="pCode code_procrow' + numRows + '"]').focus();

	numRows = numTableRows('tbl-claim-lines');

	// Hide the link when the max is reached
	if (numRows == maxClaimRows) {
		displayAddLink('claim', false, 0);
	}

	// Update the number of lines
	$('.maxClaimLines').val(numRows);

	// Set up for the next row
	numClaimRows++;

	// Disable the Exclude member search checkbox
	// setExcludeMemberStatus();
}

function addModifierRow(idx) {
	var numRTows = 0;

	$('#tbl-modifier-lines' + idx + ' > tbody:last').append('<tr><td><input type="text" class="modifierCodes" name="'+ portletNamespace+ 'modifierCode2" pattern=".{2,2}" required></td>' +
							                                '<td><i class="icon-trash" title="Delete" onClick="deleteRow(this, ' + idx + ', &quot;modifier&quot;)"></i></td></tr>');

	// Set focus on the procedure code modifier of added row
	$('input[name=modifierCode' + idx + ']').focus();

	numRows = numTableRows('tbl-modifier-lines' + idx);

	// Hide the link when the max is reached
	if (numRows == maxModifierRows) {
		displayAddLink('modifier', false, idx);
	}
}

function getBrowser() {
	var rtnValue = "unknown";

	if ((navigator.userAgent.indexOf("Opera") || navigator.userAgent
			.indexOf('OPR')) != -1) {
		rtnValue = "Opera";
	}
	if (navigator.userAgent.indexOf("Chrome") != -1) {
		rtnValue = "Chrome";
	}
	if (navigator.userAgent.indexOf("Safari") != -1) {
		rtnValue = "Safari";
	} else if ((navigator.userAgent.indexOf("MSIE") != -1) || (!!document.documentMode == true)) { // IF IE > 10
		rtnValue = "IE";
	} else if (navigator.userAgent.indexOf("Firefox") != -1) {
		rtnValue = "Firefox";
	} else {
		rtnValue = "unknown";
	}

	return rtnValue;
}

function addTextWatermark(elem, txt) {
	var browserStr = getBrowser();

	// Add a watermark to the text field
	$('#' + elem).Watermark(txt);
}

function addDateWatermark(cls) {
	var browserStr = getBrowser();

	// Add a watermark to the date field
	if ((browserStr == "IE") || (browserStr == "Firefox") || (browserStr == "unknown")) {
		$('.' + cls).Watermark("mm/dd/yyyy");
	}
}
