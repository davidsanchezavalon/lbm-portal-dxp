/*package com.avalon.member.search.controller;

import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import com.avalon.member.search.beans.MemberDetailsFo;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.model.Layout;
import com.liferay.portal.model.User;
import com.liferay.portal.model.UserGroup;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.Portal;
import com.liferay.portal.util.PortalUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletRequest;
import javax.portlet.PortletSession;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.ui.Model;

public class MemberSearchControllerTest {

	
	@Test
	public void testviewHomePage() {
		MemberSearchController memberSearchController=new MemberSearchController();
		RenderRequest request = Mockito.mock(RenderRequest.class);
		Map map=Mockito.mock(Map.class);
		PortletSession portletSession = Mockito.mock(PortletSession.class);
		when(request.getPortletSession()).thenReturn(portletSession);
			  try{
					memberSearchController.viewHomePage(request, map);
				} catch (Exception nullPointer) {
				    // TODO Auto-generated catch block
					nullPointer.printStackTrace();		
				}
	}
	@Test
	public void testFindMemberDetails() {
		MemberSearchController memberSearchController=new MemberSearchController();
		ActionRequest actionRequest = Mockito.mock(ActionRequest.class);
		ActionResponse response = Mockito.mock(ActionResponse.class);
		MemberDetailsFo memberDetailsFo=new MemberDetailsFo();
		PortletSession portletSession = Mockito.mock(PortletSession.class);
		when(actionRequest.getPortletSession()).thenReturn(portletSession);
	    Map map=Mockito.mock(Map.class);
	    HttpServletRequest httprequest = Mockito.mock(HttpServletRequest.class);
		HttpServletRequest httpServletRequest2 = Mockito.mock(HttpServletRequest.class);
	    HttpSession httpSession = Mockito.mock(HttpSession.class);
	    PortalUtil portalUtil = new PortalUtil();
        Portal portal = Mockito.mock(Portal.class);
        portalUtil.setPortal(portal);
        PortletRequest portletRequest = Mockito.mock(PortletRequest.class);
        when(portalUtil.getHttpServletRequest(any(PortletRequest.class))).thenReturn(httprequest);
	    ParamUtil paramUtil = Mockito.mock(ParamUtil.class);
	    when(ParamUtil.getString((PortletRequest)actionRequest, (String)"submitButtonStatus")).thenReturn("saved");
	    when(ParamUtil.getString((PortletRequest)actionRequest, (String)"groupId")).thenReturn("123456789");
		when(ParamUtil.getString(actionRequest, "memberID")).thenReturn("ZCL06440277");
		when(ParamUtil.getString(actionRequest,"memberFirstName")).thenReturn("Ram");
		when(ParamUtil.getString(actionRequest,"memberLastName")).thenReturn("Mohan");
		when( ParamUtil.getString(actionRequest, "healthPlan")).thenReturn("BCBS - South Carolina");
		when(ParamUtil.getString(actionRequest, "memberDob")).thenReturn("08/28/1983");
		when(ParamUtil.getString(actionRequest,"healthPlanGroupValue")).thenReturn("123456789");	
		memberDetailsFo.setMemberId("ZCL06440277");
	    memberDetailsFo.setHealthPlan("BCBS - South Carolina");
		memberDetailsFo.setMemberFirstName("Ram");
		memberDetailsFo.setMemberLastName("Mohan");		
		memberDetailsFo.setMemberDob("08/28/1983");
		memberDetailsFo.setHealthPlanGroupValue("123456789");
		  try{
			memberSearchController.findMemberDetails(actionRequest, response, memberDetailsFo);
			} catch (Exception e) {
			    // TODO Auto-generated catch bloc
			    e.printStackTrace();
			}
		}
		
	@Test
	public void testGetMemberDetails() {
		MemberSearchController memberSearchController=new MemberSearchController();
		ActionRequest request = Mockito.mock(ActionRequest.class);
	    ActionResponse response = Mockito.mock(ActionResponse.class);
		HttpServletRequest httprequest = Mockito.mock(HttpServletRequest.class);
	    HttpServletRequest httpServletRequest2 = Mockito.mock(HttpServletRequest.class);
		HttpSession httpSession = Mockito.mock(HttpSession.class);
		PortalUtil portalUtil = new PortalUtil();
		Portal portal = Mockito.mock(Portal.class);
	    portalUtil.setPortal(portal);
		PortletRequest portletRequest = Mockito.mock(PortletRequest.class);
		when(portalUtil.getHttpServletRequest(any(PortletRequest.class))).thenReturn(httprequest);
		when(portalUtil.getOriginalServletRequest(any(HttpServletRequest.class))).thenReturn(httpServletRequest2);
	    when(PortalUtil.getHttpServletRequest(any(PortletRequest.class)).getSession()).thenReturn(httpSession);
		ThemeDisplay themeDisplay =Mockito.mock(ThemeDisplay.class);
	    User user = Mockito.mock(User.class);
		when((ThemeDisplay)request.getAttribute("LIFERAY_SHARED_THEME_DISPLAY")).thenReturn(themeDisplay);
		when(themeDisplay.getUser()).thenReturn(user);
		when(themeDisplay.getUser().getEmailAddress()).thenReturn("test@liferay.com");
		PortletSession session = Mockito.mock(PortletSession.class);
		when(request.getPortletSession()).thenReturn(session);
	    Map map=Mockito.mock(Map.class);
		Map<Integer, MemberDetailsFo> mapList = new HashMap<Integer, MemberDetailsFo>();
		Map<Integer, MemberDetailsFo> portletmaplist = new HashMap<Integer, MemberDetailsFo>();
		List<MemberDetailsFo> details = new ArrayList<MemberDetailsFo>();
		MemberDetailsFo members = new MemberDetailsFo();
		members.setGender("M");
		members.setMemberId("ZCL06440277");
		members.setHealthPlan("401");	
		members.setMemberFirstName("Ram");
	    members.setMemberLastName("Mohan");		
		members.setMemberDob("08/28/1983");
		members.setPatientId("435");
	    members.setMpi("35");
		members.setGroupId("365");
		members.setHealthPlanName("BCBS - South Carolina");
		members.setSubscriberId("245");	  
		members.setMemberRelationship("brother");
		members.setMemberNumber("01");
        mapList.put(0, members);
        details.add(members);
        PortletSession portletSession = Mockito.mock(PortletSession.class);
        when(request.getPortletSession()).thenReturn(portletSession);
        when(ParamUtil.getString(request, "memberDetailsKey")).thenReturn("0");      
        when((Map<Integer, MemberDetailsFo>) httpSession.getAttribute("memberdetails")).thenReturn(mapList);
        when((Map<Integer, MemberDetailsFo>)	portletSession.getAttribute("memberdetails",PortletSession.APPLICATION_SCOPE)).thenReturn(portletmaplist);     
	    Layout layout=Mockito.mock(Layout.class);
	try{
		memberSearchController.getMemberDetails(request, response);
		} catch (Exception e) {
				 e.printStackTrace();
		}    
	}
	@Test
	public void testCancelPage() {
		MemberSearchController memberSearchController=new MemberSearchController();
		ActionRequest actionRequest = Mockito.mock(ActionRequest.class);
		ActionResponse actionResponse = Mockito.mock(ActionResponse.class);
		PortalUtil portalUtil = new PortalUtil();
		PortletRequest portletRequest = Mockito.mock(PortletRequest.class);
	    ThemeDisplay themeDisplay =Mockito.mock(ThemeDisplay.class); 
		User user = Mockito.mock(User.class);
		when((ThemeDisplay)actionRequest.getAttribute("LIFERAY_SHARED_THEME_DISPLAY")).thenReturn(themeDisplay);
		when(themeDisplay.getUser()).thenReturn(user);
		when(themeDisplay.getUser().getEmailAddress()).thenReturn("test@liferay.com");
		UserGroup userGroup = Mockito.mock(UserGroup.class);
		Portal portal = Mockito.mock(Portal.class);
		portalUtil.setPortal(portal);
	    HttpServletRequest httprequest = Mockito.mock(HttpServletRequest.class);
		HttpServletRequest httpServletRequest2 = Mockito.mock(HttpServletRequest.class);
		HttpSession httpSession = Mockito.mock(HttpSession.class);
		when(portalUtil.getHttpServletRequest(any(PortletRequest.class))).thenReturn(httprequest);
		  int i=0;
		
		try {
		    when(PortalUtil.getUser(any(PortletRequest.class))).thenReturn(user);
		 
		   String userName = "AvalonEmployee";
		 
		} catch (PortalException e) {
		    // TODO Auto-generated catch block
		    e.printStackTrace();
		} catch (SystemException e) {
		    // TODO Auto-generated catch block
		    e.printStackTrace();
		}
		
		        memberSearchController.cancelPage(actionRequest, actionResponse);
	}

}
*/