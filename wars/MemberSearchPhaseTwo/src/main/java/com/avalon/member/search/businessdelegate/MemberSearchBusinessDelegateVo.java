package com.avalon.member.search.businessdelegate;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import com.avalon.esb.servicehelpers.PriorAuthImplServiceStub;
import com.avalon.esb.servicehelpers.PriorAuthImplServiceStub.Address;
import com.avalon.esb.servicehelpers.PriorAuthImplServiceStub.BusinessSectorSegment;
import com.avalon.esb.servicehelpers.PriorAuthImplServiceStub.ContactData;
import com.avalon.esb.servicehelpers.PriorAuthImplServiceStub.Coverage;
import com.avalon.esb.servicehelpers.PriorAuthImplServiceStub.CoverageList;
import com.avalon.esb.servicehelpers.PriorAuthImplServiceStub.EnrollmentData;
import com.avalon.esb.servicehelpers.PriorAuthImplServiceStub.GetMemberData;
import com.avalon.esb.servicehelpers.PriorAuthImplServiceStub.GetMemberDataResponse;
import com.avalon.esb.servicehelpers.PriorAuthImplServiceStub.Member;
import com.avalon.esb.servicehelpers.PriorAuthImplServiceStub.MemberDataRequest;
import com.avalon.esb.servicehelpers.PriorAuthImplServiceStub.MemberDataResponse;
import com.avalon.esb.servicehelpers.PriorAuthImplServiceStub.MemberDemographicsOutput;
import com.avalon.esb.servicehelpers.PriorAuthImplServiceStub.MemberList;
import com.avalon.esb.servicehelpers.PriorAuthImplServiceStub.MoreDataOptions;
import com.avalon.esb.servicehelpers.PriorAuthImplServiceStub.SearchCriteria;
import com.avalon.member.search.beans.MemberDetailsFo;
import com.avalon.member.search.beans.MemberSearchConstants;
import com.avalon.member.search.customexception.CustomException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;

/**
 * Description
 *		This file contain the methods called by the controller for the PA Entry Page.
 *
 * @author David Sanchez
 *
 * CHANGE History
 * 		Version 1.0
 * 			Initial version
 *		Version 1.1					12/12/2017
 *			Changed health plan ID to the ID in the GUI dropdown
 *			Changed REASON_FOR_INQUIRY to P
 * 		Version 1.1					08/08/2018
 * 			Added HO LOE an subscriber name to the header.
 *  
 */

public class MemberSearchBusinessDelegateVo implements MemberSearchConstants {
    private static MemberSearchBusinessDelegateVo memberSearchBusinessDelegateVo = null;
    private static Properties properties = null;
    
    private static final Log log = LogFactoryUtil.getLog(MemberSearchBusinessDelegateVo.class.getName());

    private MemberSearchBusinessDelegateVo() {
    }

    static {

	if (properties == null) {
	    properties = new Properties();
	    memberSearchBusinessDelegateVo = new MemberSearchBusinessDelegateVo();
	    String responseCodePropertyPath = System.getProperty("LIFERAY_HOME");
	    String propertyLocation = responseCodePropertyPath + "/PAMessages_en_US.properties";
	    log.info("responseCodePropertyPath: " + responseCodePropertyPath);
		try {
		    properties.load(new FileInputStream(new File(propertyLocation)));
		} catch (FileNotFoundException e) {
			log.error("FileNotFoundException in static block",e);
		} catch (IOException e) {
			log.error("IOException in static block",e);
		}
		
	    }

	}
    

    public static MemberSearchBusinessDelegateVo getMemberSearchBusinessDelegateVo() {
	return memberSearchBusinessDelegateVo;
    }

    /**
     * 
     * @param memberId
     * @param healthPlanGroup
     * @param healthPlanId
     * @return
     * @throws IOException
     * @throws NullPointerException
     * Find member service will be invoked based on the memberId
     * If the member is available it fetches the results
     * Returning the results in the form of map object to the controller
     */
    public Map<Integer, MemberDetailsFo> findMember(String memberId,
	                                                String healthPlanGroup, 
	                                                String healthPlanId) throws IOException, NullPointerException, CustomException {

    	log.info("PriorAuthBusinessDelegateVo start findMember");
		List<MemberDetailsFo> details = new ArrayList<MemberDetailsFo>();
		Map<Integer, MemberDetailsFo> mapList = new HashMap<Integer, MemberDetailsFo>();
		//TODO remove comment later
		String memberNumber = null;
		try {
			PriorAuthImplServiceStub priorAuthImplServiceStub = new PriorAuthImplServiceStub();
			GetMemberData getMemberData = new GetMemberData();
			MemberDataRequest memberDataRequest = new MemberDataRequest();
			SearchCriteria searchCriteria = new SearchCriteria();
			searchCriteria.setReasonForInquiry(REASON_FOR_INQUIRY);
			searchCriteria.setRequestDate(null);
			searchCriteria.setRpn(" ");
			searchCriteria.setProductCode(" ");
			searchCriteria.setPlanCode(" ");
			searchCriteria.setHealthPlanId(healthPlanId);
			searchCriteria.setHealthPlanGroupId(" ");
			searchCriteria.setIdCardNumber(memberId);
			searchCriteria.setHealthPlanIdType(HEALTH_PLAN_ID_TYPE); // PI
			searchCriteria.setSubscriberNumber(" ");
			memberDataRequest.setSearchCriteria(searchCriteria);
			MoreDataOptions moreDataOptions = new MoreDataOptions();
			moreDataOptions.setRetrieveMemberList(true);
			moreDataOptions.setRetrieveCoverageData(false);
			memberDataRequest.setMoreDataOptions(moreDataOptions);
			getMemberData.setGetMemberData(memberDataRequest);
			log.info("Request Data : " + getMemberData.toString());
			GetMemberDataResponse getMemberDataResponse;
			 //TODO: Remove mock service code and condition. Don't remove else condition code
			if(GetterUtil.getBoolean(com.liferay.portal.kernel.util.PropsUtil.get(ENABLED_MOCK_SERVICE))){
				 getMemberDataResponse = getSuccessMockMemberData(getMemberData);
			}else{
				 getMemberDataResponse = priorAuthImplServiceStub.getMemberData(getMemberData);
			}
			
			boolean searchMemberFlag = true;
			String[] responseCodes = getMemberDataResponse.getGetMemberDataResponse().getResponseCode();
			log.info("response codes for member id " + memberId);
			for (int i = 0; i < responseCodes.length; i++) {
				log.info("response code " + i + ": " + responseCodes[i]);
			}
			for(String responseCode:responseCodes) {
				
				// Ignore all response codes except 950 and 951
				if (responseCode.matches("000|952|953|954|955|956|957|958|959|960|961|962|963|964|990|991|992|993|994|995|996")) {
					if (searchMemberFlag) {
						
						// Set the flag to indicate the member search has been done
						searchMemberFlag = false;
						memberNumber = getMemberDataResponse.getGetMemberDataResponse().getMemberDemographicsOutput().getMemberId();
						log.info("memberNumber -----> " + memberNumber);
						
						if (((memberNumber == null) || (memberNumber.equals("")) || (memberNumber.equals("null")))) {
							log.info("Invalid Member Data, member number is null");
							throw new IOException("Invalid Member Data");
						}
						
						// Get the addition member data (there is only one of these)
						String addressLine1 = getMemberDataResponse.getGetMemberDataResponse().getAddress().getAddressLine1();
						String addressLine2 = getMemberDataResponse.getGetMemberDataResponse().getAddress().getAddressLine2();
						String city = getMemberDataResponse.getGetMemberDataResponse().getAddress().getCity();
						String state = getMemberDataResponse.getGetMemberDataResponse().getAddress().getStateCode();
						String zipCode = getMemberDataResponse.getGetMemberDataResponse().getAddress().getZipCode();
						String phoneNumber = getMemberDataResponse.getGetMemberDataResponse().getContactData().getPhoneNumber();
						
						// Get the LOB data
						String businessSectorCode = getMemberDataResponse.getGetMemberDataResponse().getBusinessSectorSegment().getBusinessSectorCd();
						String businessSectorDescription = getMemberDataResponse.getGetMemberDataResponse().getBusinessSectorSegment().getBusinessSectorDesc();
						String businessSegmentCode = getMemberDataResponse.getGetMemberDataResponse().getBusinessSectorSegment().getBusinessSegmentCd();
						String businessSegmentDescription = getMemberDataResponse.getGetMemberDataResponse().getBusinessSectorSegment().getBusinessSegmentDesc();

						// Get the subscriber name
						String subscriberName = getMemberDataResponse.getGetMemberDataResponse().getMemberDemographicsOutput().getFirstName() + " " +
				                                getMemberDataResponse.getGetMemberDataResponse().getMemberDemographicsOutput().getLastName();

						log.info("Member Id : " + memberNumber);
						log.info("Health Plan Group Id : " + getMemberDataResponse.getGetMemberDataResponse().getHealthPlanGroupId());
						log.info("Lab Benefit Id : " + getMemberDataResponse.getGetMemberDataResponse().getLabBenefitId());
						log.info("Lab Benefit Id Description : " + getMemberDataResponse.getGetMemberDataResponse().getLabBenefitIdDescription());
						log.info("Subscriber Number : " + getMemberDataResponse.getGetMemberDataResponse().getSubscriberNumber());
						log.info("Subscriber Gender Code: " + getMemberDataResponse.getGetMemberDataResponse().getMemberDemographicsOutput().getGenderCode());
						log.info("Subscriber Member Id : " + getMemberDataResponse.getGetMemberDataResponse().getMemberDemographicsOutput().getMemberId());
						log.info("Subscriber Date Of Birth: " + getMemberDataResponse.getGetMemberDataResponse().getMemberDemographicsOutput().getDateOfBirth());
						log.info("Address Line 1: " + addressLine1);
						log.info("Address Line 2: " + addressLine2);
						log.info("City: " + city);
						log.info("State: " + state);
						log.info("Zip Code: " + zipCode);
						log.info("Phone Number: " + phoneNumber);
						String memeberListCount = getMemberDataResponse.getGetMemberDataResponse().getMemberList().getMemberListCount();

						// to display the ammsGropNumber from the Enrollment parent element.
						String ammsGroupNumber =  getMemberDataResponse.getGetMemberDataResponse().getEnrollmentData().getAmmsGroupNumber();
						log.info("ammsGroupNumber: " + ammsGroupNumber);
						log.info("member List Count: " + memeberListCount);
						Member[] member = getMemberDataResponse.getGetMemberDataResponse().getMemberList().getMember();
						for (int i = 0; i < member.length; i++) {
							MemberDetailsFo members = new MemberDetailsFo();
							members.setHealthPlan(member[i].getHealthPlanGroupId());
							log.info("HealthPlanGroupId " + members.getHealthPlan());
							members.setMemberFirstName(member[i].getFirstName());
							log.info("First Name " + members.getMemberFirstName());
							members.setMemberMiddleName(member[i].getMiddleName());
							log.info("Middle Name " + members.getMemberMiddleName());
							members.setMemberLastName(member[i].getLastName());
							log.info("Last Name " +members.getMemberLastName());
							members.setMemberSuffixName(member[i].getSuffix());
							log.info("Suffix Name " + members.getMemberSuffixName());
							members.setMemberDob(convertStringToDate(member[i].getDateOfBirth()));
							log.info("Date Of Birth " + member[i].getDateOfBirth());
							members.setGender(member[i].getGenderCode());
							log.info("Gender Code " + members.getGender());
							members.setPatientId(member[i].getPatientId());
							log.info("Patient Id " + members.getPatientId());
							members.setMemberId(memberId);
							log.info("Member Id " + members.getMemberId());
							members.setMpi(member[i].getUniqueMemberId());
							log.info("uniqueMemberId " + members.getMpi());
							members.setHealthPlanName(healthPlanId);
							log.info("HealthPlanName " + members.getHealthPlanName());
							members.setSubscriberId(memberId);
							log.info("SubscriberId " + members.getSubscriberId());
							members.setMemberRelationship(member[i].getRelationshipCodeDescription());
							log.info("relationshipCodeDescription " + members.getMemberRelationship());
							members.setMemberNumber(member[i].getMemberId());
							log.info("MemberNumber " + members.getMemberNumber());
							
							// Set the addition member data (there is only one of these)
							members.setMemberAddressLine1(addressLine1);
							log.info("memberAddressLine1: "+ members.getMemberAddressLine1());
							members.setMemberAddressLine2(addressLine2);
							log.info("memberAddressLine2: "+ members.getMemberAddressLine2());
							members.setMemberCity(city);
							log.info("memberCity: " + members.getMemberCity());
							members.setMemberStateCode(state);
							log.info("memberState: " + members.getMemberStateCode());
							members.setMemberZipcode(zipCode);
							log.info("memberZipCode: " + members.getMemberZipcode());
							members.setMemberPhoneNumber(phoneNumber);
							log.info("memberPhoneNumber: " + members.getMemberPhoneNumber());
							
							members.setBusinessSectorCode(businessSectorCode);
							log.info("businessSectorCode: " + members.getBusinessSectorCode());
							members.setBusinessSectorDescription(businessSectorDescription);
							log.info("businessSectorDescription: " + members.getBusinessSectorDescription());
							members.setBusinessSegmentCode(businessSegmentCode);
							log.info("businessSegmentCode: " + members.getBusinessSegmentCode());
							members.setBusinessSegmentDescription(businessSegmentDescription);
							log.info("businessSegmentDescription: " + members.getBusinessSegmentDescription());

							members.setSubscriberName(subscriberName);
							log.info("subscriberName: " + subscriberName);

							mapList.put(i, members);
							details.add(members);
						}
						String coverageList = getMemberDataResponse.getGetMemberDataResponse().getCoverageList().getCoverageCount();
						log.info("coverageListCount: " + coverageList);
						Coverage[] coverage = getMemberDataResponse.getGetMemberDataResponse().getCoverageList().getCoverage();
						for (int i = 0; i < coverage.length; i++) {
							log.info("HealthPlanGroupId " + coverage[i].getHealthPlanGroupId());
							log.info("Coverage Begin date " + coverage[i].getCoverageBeginDate());
							log.info("Client Number " + coverage[i].getClientNumber());
							log.info("getCesGroupNumber " + coverage[i].getCesGroupNumber());
							log.info("getCesGroupNumberDescription " + coverage[i].getCesGroupNumberDescription());
							log.info("getAmmsGroupNumber " + coverage[i].getAmmsGroupNumber());
							log.info("getBusinessSectorCd " + coverage[i].getBusinessSectorSegment().getBusinessSectorCd());
							log.info("getBusinessSectorDesc " + coverage[i].getBusinessSectorSegment().getBusinessSectorDesc());
							log.info("getBusinessSegmentCd " + coverage[i].getBusinessSectorSegment().getBusinessSegmentCd());
							log.info("getBusinessSegmentDesc " + coverage[i].getBusinessSectorSegment().getBusinessSegmentDesc());
						}
						log.info("PriorAuthBusinessDelegateVo End findMember");
					}
				} else {
											
					// Set the flag to indicate the a error that stops the member search
					searchMemberFlag = false;

					String errorMessage = getResponseCodeMessages(responseCodes);
					throw new CustomException(errorMessage);
				}
			}
		}
		// Exception thrown when general network I/O error occurs
		catch (IOException ioe) {
			log.info("Check: " + (ioe instanceof org.apache.axis2.AxisFault));
			log.info("Network I/O error - " + ioe);
			if (ioe instanceof org.apache.axis2.AxisFault) {
				log.info("ioe InstanceOf Axis");
				org.apache.axis2.AxisFault exception = (org.apache.axis2.AxisFault) ioe;
				if (exception.getMessage().equals("Read timed out")) {
					log.info("IOException Timed Out");
					throw new IOException("Your Request is Timed Out");
				} else {
					log.info("IOException No Records");
					throw new IOException("No Records");
				}
			}
			if (ioe.getMessage().equals("Invalid Member Data")) {
				log.info("IOException Invalid Member Data");
				throw new IOException("Invalid Member Data");
			} else {
				log.info("Timeout getting member");
				throw new IOException("Your Request is Timed Out");
			}
		} catch (CustomException exception) {

			log.info("MemberSearchBusinessDelegateVo Response Code Exception " + exception.getMessage());
			throw exception;

		} catch (Exception e) {
			log.error("Exception in findMember method",e);
		}
		return mapList;
    }

    /**
     * 
     * @param indate
     * @return To get the date in MM/dd/yyyy format for findMember response
     *         object
     */

    public String convertStringToDate(Date indate) {
	String dateString = null; //
	SimpleDateFormat formatDate = new SimpleDateFormat("MM/dd/yyyy");
	try {
	    dateString = formatDate.format(indate);
	} catch (Exception ex) {
		log.error("Exception in convertStringToDate method",ex);
	}
	return dateString;
    }

    
    /**
     * 
     * @param responseCodes
     * @param idCardNumber
     * @return To display error message based on the response code from the the cds response
     */
    
    public String getResponseCodeMessages(String responseCodes[]) {

		StringBuilder errorMessage = new StringBuilder();

		
		 for(String responseCode:responseCodes){
			log.info("responseCode ---> " +responseCode);
		   
			if (responseCode.matches("950|951")) {
				errorMessage.append(properties.getProperty(responseCode));
				//errorMessage.append("</br>");
			} else {
				log.info(responseCode + " is not a Valid Response Code");
			}
		}

	return errorMessage.toString();

    }
    
    /**
     * TODO: Remove Success Mock Code
     * @param getMemberData
     * @return
     */
    public GetMemberDataResponse getSuccessMockMemberData(GetMemberData getMemberData){
    	GetMemberDataResponse getMemberDataResponse = new GetMemberDataResponse();
    	MemberDataResponse memberDataResponse = new MemberDataResponse();
    	memberDataResponse.setResponseCode(new String[] {"000"});
    	
    	Address address = new Address();
    	address.setAddressLine1("Address Line1");
    	address.setAddressLine2("Address Line2");
    	address.setCity("Ahmedabad");
    	address.setStateCode("26");
    	address.setZipCode("382345");
    	memberDataResponse.setAddress(address);
    	ContactData contactData = new ContactData();
    	contactData.setPhoneNumber("13322323");
    	memberDataResponse.setContactData(contactData);
    	
    	BusinessSectorSegment businessSectorSegment = new BusinessSectorSegment();
    	businessSectorSegment.setBusinessSectorCd("SectorCd");
    	businessSectorSegment.setBusinessSectorDesc("SectorDesc");
    	businessSectorSegment.setBusinessSegmentCd("SegmentCd");
    	businessSectorSegment.setBusinessSegmentDesc("SegmentDesc");
    	
    	memberDataResponse.setBusinessSectorSegment(businessSectorSegment);
    	memberDataResponse.setHealthPlanGroupId("HPGID");
    	memberDataResponse.setLabBenefitId("LBID");
    	memberDataResponse.setSubscriberNumber("SUBNO");
    	
    	MemberDemographicsOutput memberDemographicsOutput = new MemberDemographicsOutput();
    	memberDemographicsOutput.setDateOfBirth(new Date());
    	memberDemographicsOutput.setFirstName("TestFirstName");
    	memberDemographicsOutput.setLastName("TestLastName");
    	memberDemographicsOutput.setGenderCode("GCODE");
    	memberDemographicsOutput.setMemberId(getMemberData.getGetMemberData().getSearchCriteria().getIdCardNumber());
    	memberDemographicsOutput.setMiddleName("TestMiddleName");
    	memberDemographicsOutput.setSuffix("Test");
    	memberDataResponse.setMemberDemographicsOutput(memberDemographicsOutput);
    	
    	MemberList memberList = new MemberList();
    	memberList.setMemberListCount("1");
    	Member member = new Member();
    	member.setMemberId("MEMID");
    	member.setFirstName("MeMFirstName");
    	member.setDateOfBirth(new Date());
    	member.setHealthPlanGroupId("MemberHPGroupId");
    	member.setLastName("MemLastName");
    	member.setMiddleName("MemMiddleName");
    	member.setSuffix("TestMem");
    	member.setGenderCode("MGEC");
    	member.setPatientId("MPID");
    	member.setUniqueMemberId("UMID");
    	member.setRelationshipCodeDescription("RCD");
    	memberList.setMember(new Member[] {member});
    	memberDataResponse.setMemberList(memberList);
    	
    	EnrollmentData enrollmentData = new EnrollmentData();
    	enrollmentData.setAmmsGroupNumber("AmsGrpNo");
    	memberDataResponse.setEnrollmentData(enrollmentData);
    	
    	CoverageList coverageList = new CoverageList();
    	coverageList.setCoverageCount("CCount");

    	Coverage coverage = new Coverage();
    	coverage.setHealthPlanGroupId("HPGID1");
    	coverage.setCoverageBeginDate(new Date());
    	coverage.setClientNumber("CNo");
    	coverage.setCesGroupNumber("CESGrpNo");
    	coverage.setCesGroupNumberDescription("CGND");
    	coverage.setAmmsGroupNumber("AMGRPNo");
    	coverage.setBusinessSectorSegment(businessSectorSegment);
    	coverageList.setCoverage(new Coverage[] {coverage});
    	memberDataResponse.setCoverageList(coverageList);
    	getMemberDataResponse.setGetMemberDataResponse(memberDataResponse);
    	return getMemberDataResponse;
    }
}
