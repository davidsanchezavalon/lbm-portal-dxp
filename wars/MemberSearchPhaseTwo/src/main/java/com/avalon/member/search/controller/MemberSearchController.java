package com.avalon.member.search.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.PortletMode;
import javax.portlet.PortletRequest;
import javax.portlet.PortletSession;
import javax.portlet.PortletURL;
import javax.portlet.RenderRequest;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.portlet.WindowState;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;

import com.avalon.member.search.beans.MemberDetailsFo;
import com.avalon.member.search.beans.MemberSearchConstants;
import com.avalon.member.search.businessdelegate.MemberSearchBusinessDelegateVo;
import com.avalon.member.search.customexception.CustomException;
import com.google.gson.Gson;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Layout;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.model.UserGroup;
import com.liferay.portal.kernel.portlet.PortletURLFactoryUtil;
import com.liferay.portal.kernel.service.LayoutLocalServiceUtil;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.WebKeys;

/**
 * Description
 *		This file contains the controller methods for the PA Entry Page.
 *
 * @author David Sanchez
 *
 * CHANGE History
 * 		Version 1.0
 * 			Initial version
 *		Version 1.1
 *			Added changes for initial version of Post Servce Review
 *		Version 1.2					01/03/2018
 *			Made the changes needed to use DataTables
 * 		Version 1.3					08/08/2018
 * 			Added HO LOE an subscriber name to the header.
 *  
 */

@Controller("memberSearchController")
@RequestMapping(value = "VIEW")
public class MemberSearchController implements MemberSearchConstants {
	
	private static final Log log = LogFactoryUtil.getLog(MemberSearchController.class.getName());

   /**
     * whenever the view page has been clicked the render method will be executed 
     * In this method we are removing the session variables which has been set in avalon-prior-authorization-portlet.
     * It redirects to the view page.
    */
    
    @RenderMapping
    public String viewHomePage(RenderRequest request,
    		                   Map<String, Object> map,
		                       @ModelAttribute(value = "memberDetailsFo") MemberDetailsFo memberDetailsFo) {
    	log.info("Processing Render Action for Member Search Enter");

    try {
	    PortletSession portletSession = request.getPortletSession();
		String typeStr = "&type=";
		String entryType = "";
		String queryString = PortalUtil.getHttpServletRequest(request).getQueryString();
		
		int typeIdx = queryString.lastIndexOf(typeStr);
		
		if (typeIdx != -1) {
			entryType = queryString.substring(typeIdx + typeStr.length());

			// Default to Prior Auth is the query string is not set
			if (!entryType.equals(MemberSearchConstants.PRIOR_AUTH) && !entryType.equals(MemberSearchConstants.POST_SERVICE_REVIEW)) {
				entryType = MemberSearchConstants.PRIOR_AUTH;
			}
			 portletSession.setAttribute("entryType", entryType, PortletSession.APPLICATION_SCOPE);
		} else {
			entryType = (String)portletSession.getAttribute("entryType", PortletSession.APPLICATION_SCOPE);
		}
	    portletSession.removeAttribute("authorizationKey", PortletSession.APPLICATION_SCOPE);
	    portletSession.removeAttribute("authorizationNumber", PortletSession.APPLICATION_SCOPE);
	    portletSession.removeAttribute("authorizationAge", PortletSession.APPLICATION_SCOPE);
	    portletSession.removeAttribute("authorizationStatus", PortletSession.APPLICATION_SCOPE);
	    portletSession.removeAttribute("enteredNotes", PortletSession.APPLICATION_SCOPE);
	    portletSession.removeAttribute("peerEnteredNotes", PortletSession.APPLICATION_SCOPE);
	    portletSession.removeAttribute("appCreateDateTime", PortletSession.APPLICATION_SCOPE);
	    portletSession.removeAttribute("originalUserId", PortletSession.APPLICATION_SCOPE);
	    portletSession.removeAttribute("authDecisionDate", PortletSession.APPLICATION_SCOPE);
        portletSession.removeAttribute("providerAuthStatus", PortletSession.APPLICATION_SCOPE);

	    // Remove the attributes shown in the prior authorization pages
	    portletSession.removeAttribute("memberDob", PortletSession.APPLICATION_SCOPE);
	    portletSession.removeAttribute("memberId", PortletSession.APPLICATION_SCOPE);
	    portletSession.removeAttribute("memberNumber", PortletSession.APPLICATION_SCOPE);
	    portletSession.removeAttribute("healthPlan", PortletSession.APPLICATION_SCOPE);
	    portletSession.removeAttribute("mpi", PortletSession.APPLICATION_SCOPE);
	    portletSession.removeAttribute("membergroupId", PortletSession.APPLICATION_SCOPE);
	    portletSession.removeAttribute("memberName", PortletSession.APPLICATION_SCOPE);
	    portletSession.removeAttribute("memberGender", PortletSession.APPLICATION_SCOPE);
	    portletSession.removeAttribute("memberAddressLine1", PortletSession.APPLICATION_SCOPE);
	    portletSession.removeAttribute("memberAddressLine2",PortletSession.APPLICATION_SCOPE);
	    portletSession.removeAttribute("memberCity", PortletSession.APPLICATION_SCOPE);
	    portletSession.removeAttribute("memberStateCode", PortletSession.APPLICATION_SCOPE);
	    portletSession.removeAttribute("memberZipcode", PortletSession.APPLICATION_SCOPE);
	    portletSession.removeAttribute("memberPhoneNumber", PortletSession.APPLICATION_SCOPE);

		// Remove the additional attributes saved by the prior authorization pages
		portletSession.removeAttribute("memberFirstName", PortletSession.APPLICATION_SCOPE);
		portletSession.removeAttribute("memberMiddleName", PortletSession.APPLICATION_SCOPE);
		portletSession.removeAttribute("memberLastName", PortletSession.APPLICATION_SCOPE);
		portletSession.removeAttribute("memberSuffixName", PortletSession.APPLICATION_SCOPE);
		portletSession.removeAttribute("businessSectorCode", PortletSession.APPLICATION_SCOPE);
		portletSession.removeAttribute("businessSectorDescription", PortletSession.APPLICATION_SCOPE);
		portletSession.removeAttribute("businessSegmentCode", PortletSession.APPLICATION_SCOPE);
		portletSession.removeAttribute("businessSegmentDescription", PortletSession.APPLICATION_SCOPE);

		// Remove the subscriber name attribute
		portletSession.removeAttribute("subsctiberName", PortletSession.APPLICATION_SCOPE);

	    map.put("memberDetailsFo", new MemberDetailsFo()); // Setting the commandBean for spring form
	} catch (Exception nullPointer) {
		log.error("Exception in viewHomePage method",nullPointer);
	}
    log.info("Processing Render Action for Member Search Exit");
	return VIEW_DISPLAY;
    }

    /**
     * whenever the search button  has been clicked the findMemberDetails method will be executed 
     * In this method we will fetch the member details and set in the session 
     * It will be redirect to the avalon-prior-authorization-portlet.  (Used for actionURL)
    */
    @ActionMapping(params = "action=memberSearchAction")
    public void findMemberDetails(ActionRequest actionRequest,
	                              ActionResponse actionResponse,
	                              @ModelAttribute("memberDetailsFo") MemberDetailsFo memberDetailsFo)
	    throws IOException, PortletException {

    log.info("Processing Search Action for Member Search Enter");
    try {
		    PortletSession portletSession = actionRequest.getPortletSession();
		    String healthPlanID = ParamUtil.getString(actionRequest, "healthPlan");
		    memberDetailsFo.setHealthPlan(healthPlanID);
			String memberID = ParamUtil.getString(actionRequest, "memberID");
			memberDetailsFo.setMemberId(memberID);
		    portletSession.setAttribute("idCardNumber", memberDetailsFo.getMemberId());
		    log.info(" Member Id : " + memberDetailsFo.getMemberId());
		    actionRequest.setAttribute("memberIDVal", memberDetailsFo.getMemberId());
		    log.info(" Health Plan Group Value : " + memberDetailsFo.getHealthPlanGroupValue());
		    log.info(" Health Plan : " + memberDetailsFo.getHealthPlan());
		    Map<Integer, MemberDetailsFo> details = new ConcurrentHashMap<Integer, MemberDetailsFo>();
		    HttpSession httpSession = PortalUtil.getHttpServletRequest(actionRequest).getSession();
		    actionResponse.setRenderParameter("memberFirstName", ParamUtil.getString(actionRequest, "memberFirstName"));
		    actionResponse.setRenderParameter("memberID", ParamUtil.getString(actionRequest, "memberID"));
		    actionResponse.setRenderParameter("memberLastName", ParamUtil.getString(actionRequest, "memberLastName"));
		    actionResponse.setRenderParameter("healthPlan", ParamUtil.getString(actionRequest, "healthPlan"));
		    actionResponse.setRenderParameter("memberDob", ParamUtil.getString(actionRequest, "memberDob"));
		    MemberSearchBusinessDelegateVo priorAuthBusinessDelegateVo = MemberSearchBusinessDelegateVo.getMemberSearchBusinessDelegateVo();
		    details = priorAuthBusinessDelegateVo.findMember(memberDetailsFo.getMemberId(), memberDetailsFo.getHealthPlanGroupValue(), memberDetailsFo.getHealthPlan());
		    log.info("details : " + details);
		    if (details == null || details.isEmpty()) {
				log.info("details object null Condition");
				actionResponse.setRenderParameter("memberId", memberDetailsFo.getMemberId());
				actionRequest.setAttribute("message", MemberSearchConstants.NO_MEMBER_FOUND);
		    } else if (details != null && !details.isEmpty() && details.size() > 0) {
				if ((!(details.get(0).getMpi().trim().isEmpty()) && ((details.get(0).getMpi()) != null))) {
				    log.info("details object not null and MPI is existing");
				    httpSession.setAttribute("memberdetails", details);	// session
				    Map<Integer, MemberDetailsFo> maplist = (Map<Integer, MemberDetailsFo>) httpSession.getAttribute("memberdetails");
				    log.info("MapList: " + maplist);
				    log.info(" Valid details : " + details);
				    log.info("memberId ----- > " + memberDetailsFo.getMemberId());
				    actionResponse.setRenderParameter("memberId", memberDetailsFo.getMemberId());
				    actionRequest.setAttribute("details", details);
				} else {
				    log.info("details object not null and MPI is null");
				    actionResponse.setRenderParameter("memberId", memberDetailsFo.getMemberId());
				    actionRequest.setAttribute("message", MemberSearchConstants.NO_MEMBER_FOUND);
				}
		    } else {
				log.info("Server is Down");
				actionResponse.setRenderParameter("memberId", memberDetailsFo.getMemberId());
				actionRequest.setAttribute("message", MemberSearchConstants.PROCESSING_ERROR);
		    }
		    actionResponse.setRenderParameter("mvcPath", "/html/jsps/memberDetails.jsp");
		} catch (IOException ioe) {
		    log.info("PriorAuthController IOException");
		    if (ioe.getMessage().equals("No Records")) {
				log.info("PriorAuthController No Records");
				actionResponse.setRenderParameter("memberId", memberDetailsFo.getMemberId());
				actionRequest.setAttribute("message", MemberSearchConstants.NO_MEMBER_FOUND);
				actionResponse.setRenderParameter("mvcPath", "/html/jsps/memberDetails.jsp");
		    } else if (ioe.getMessage().equals(MemberSearchConstants.INVALID_MEMBER_DATA)) {
				log.info("PriorAuthController Invalid Member Data");
				actionResponse.setRenderParameter("memberId", memberDetailsFo.getMemberId());
				actionRequest.setAttribute("message", MemberSearchConstants.INVALID_MEMBER_DATA);
				actionResponse.setRenderParameter("mvcPath", "/html/jsps/memberDetails.jsp");
		    } else {
		    	log.info("PriorAuthController Socket Time Out");
				log.error("IOException in findMemberDetails method",ioe);
				actionResponse.setRenderParameter("memberId", memberDetailsFo.getMemberId());
				actionRequest.setAttribute("message", MemberSearchConstants.PROCESSING_ERROR);
				actionResponse.setRenderParameter("mvcPath", "/html/jsps/memberDetails.jsp");
		    }
		} catch (NullPointerException noResponse) {
			log.error("NullPointerException in findMemberDetails method",noResponse);
		    actionResponse.setRenderParameter("memberId", memberDetailsFo.getMemberId());
		    actionRequest.setAttribute("message", MemberSearchConstants.PROCESSING_ERROR);
		    actionResponse.setRenderParameter("mvcPath", "/html/jsps/memberDetails.jsp");
		} catch (CustomException exception) {
			log.error("CustomException in findMemberDetails method",exception);
		    actionResponse.setRenderParameter("memberId", memberDetailsFo.getMemberId());
		    // To display the error message based on the response code from the cds response
		    actionRequest.setAttribute("message", exception.getMessage());
		    actionResponse.setRenderParameter("mvcPath", "/html/jsps/memberDetails.jsp");
		} catch (Exception exception) {
			log.error("Exception in findMemberDetails method",exception);
		    actionResponse.setRenderParameter("memberId", memberDetailsFo.getMemberId());
		    actionRequest.setAttribute("message", MemberSearchConstants.PROCESSING_ERROR);
		    actionResponse.setRenderParameter("mvcPath", "/html/jsps/memberDetails.jsp");
		}
	    log.info("Processing Search Action for Member Search Exit");
    }


    /**
     * whenever the search button  has been clicked the memberInfoSearchform method will be executed 
     * In this method we will fetch the member details and set in the session 
     * It will be redirect to the avalon-prior-authorization-portlet.  (Used for resourceURL)
    */
    @ResourceMapping(value="memberInfoSearchform")
    public void memberInfoSearchform(ResourceRequest request,
                                     ResourceResponse response,
	                                 @ModelAttribute("memberDetailsFo") MemberDetailsFo memberDetailsFo)
	    throws IOException, PortletException {

    log.info("Processing Search Action for Member Search Enter");
	String memberInfo = null;
	Gson gson = new Gson(); 
    try {
		    PortletSession portletSession = request.getPortletSession();
		    
		    // Get the health plan
		    String healthPlanID = ParamUtil.getString(request, "healthPlan");
		    memberDetailsFo.setHealthPlan(healthPlanID);
		    log.info(" Health Plan : " + memberDetailsFo.getHealthPlan());
		    
			// Get the member ID
			String memberID = ParamUtil.getString(request, "memberID");
			memberDetailsFo.setMemberId(memberID);
			portletSession.setAttribute("idCardNumber", memberDetailsFo.getMemberId());
			log.info(" Member Id : " + memberDetailsFo.getMemberId());

		    log.info(" Health Plan Group Value : " + memberDetailsFo.getHealthPlanGroupValue());
		    
		    Map<Integer, MemberDetailsFo> details = new ConcurrentHashMap<Integer, MemberDetailsFo>();
		    HttpSession httpSession = PortalUtil.getHttpServletRequest(request).getSession();
		    MemberSearchBusinessDelegateVo priorAuthBusinessDelegateVo = MemberSearchBusinessDelegateVo.getMemberSearchBusinessDelegateVo();
		    details = priorAuthBusinessDelegateVo.findMember(memberDetailsFo.getMemberId(), memberDetailsFo.getHealthPlanGroupValue(), memberDetailsFo.getHealthPlan());
		    log.info("details : " + details);
		    if (details == null || details.isEmpty()) {
				log.info("details object null Condition");
				memberInfo = createErrorMessage(MemberSearchConstants.NO_MEMBER_FOUND);
				response.getWriter().write(memberInfo.toString());	// Setting the coverted json response into response object
		    } else if (details != null && !details.isEmpty() && details.size() > 0) {
				if ((!(details.get(0).getMpi().trim().isEmpty()) && ((details.get(0).getMpi()) != null))) {
				    log.info("details object not null and MPI is existing");
				    httpSession.setAttribute("memberdetails", details);	// session
				    Map<Integer, MemberDetailsFo> maplist = (Map<Integer, MemberDetailsFo>) httpSession.getAttribute("memberdetails");
				    log.info("MapList: " + maplist);
				    log.info(" Valid details : " + details);
				    log.info("memberId ----- > " + memberDetailsFo.getMemberId());

					// Convert the ConcurrentHashMap to a List
					List<MemberDetailsFo> list = new ArrayList<MemberDetailsFo>();
					for (Integer key : details.keySet()) {
						MemberDetailsFo nextPair = details.get(key);
						nextPair.setKeyCode(Integer.toString(key));
						list.add(nextPair);
					} 
					memberInfo = gson.toJson(list);						// Coverting the list into Json Response 
					response.getWriter().write(memberInfo.toString());	// Setting the coverted json response into response object
				} else {
				    log.info("details object not null and MPI is null");
					memberInfo = createErrorMessage(MemberSearchConstants.NO_MEMBER_FOUND);
					response.getWriter().write(memberInfo.toString());	// Setting the coverted json response into response object
				}
		    } else {
				log.info("Server is Down");
				memberInfo = createErrorMessage(MemberSearchConstants.PROCESSING_ERROR);
				response.getWriter().write(memberInfo.toString());		// Setting the coverted json response into response object
		    }
		} catch (IOException ioe) {
		    log.info("PriorAuthController IOException");
		    if (ioe.getMessage().equals("No Records")) {
				log.info("PriorAuthController No Records");
				memberInfo = createErrorMessage(MemberSearchConstants.NO_MEMBER_FOUND);
				response.getWriter().write(memberInfo.toString());		// Setting the coverted json response into response object
		    } else if (ioe.getMessage().equals(MemberSearchConstants.INVALID_MEMBER_DATA)) {
				log.info("PriorAuthController Invalid Member Data");
				memberInfo = createErrorMessage(MemberSearchConstants.INVALID_MEMBER_DATA);
				response.getWriter().write(memberInfo.toString());		// Setting the coverted json response into response object
		    } else {
				log.info("PriorAuthController Socket Time Out");
				log.error("IOException in memberInfoSearchform method",ioe);
				memberInfo = createErrorMessage(MemberSearchConstants.PROCESSING_ERROR);
				response.getWriter().write(memberInfo.toString());		// Setting the coverted json response into response object
		    }
		} catch (NullPointerException noResponse) {
			log.error("NullPointerException in memberInfoSearchform method",noResponse);
		    memberInfo = createErrorMessage(MemberSearchConstants.PROCESSING_ERROR);
			response.getWriter().write(memberInfo.toString());			// Setting the coverted json response into response object
		} catch (CustomException exception) {
			log.error("CustomException in memberInfoSearchform method",exception);
		    
		    // Display the error message based on the response code from the cds response
			memberInfo = createErrorMessage(exception.getMessage());
			response.getWriter().write(memberInfo.toString());			// Setting the coverted json response into response object
		} catch (Exception exception) {
			log.error("Exception in memberInfoSearchform method",exception);
		    memberInfo = createErrorMessage(MemberSearchConstants.PROCESSING_ERROR);
			response.getWriter().write(memberInfo.toString());			// Setting the coverted json response into response object
		}
	    log.info("Processing Search Action for Member Search Exit");
    }

    /**
     * Whenever user click on hyperlink of the results page getMemberDetails method will be execute
     */
    @ActionMapping(params = "action=getMemberDetails")
    public void getMemberDetails(ActionRequest actionRequest,
	                             ActionResponse actionResponse) throws IOException, PortletException {

    	log.info("Processing getMemberDetails Action for Member Search Enter");
		HttpServletRequest httpRequest = PortalUtil.getHttpServletRequest(actionRequest);
		HttpSession httpSession = PortalUtil.getHttpServletRequest(actionRequest).getSession();
		ThemeDisplay themeDisplay1 = (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
		Layout layout;
		PortletSession portletSession = actionRequest.getPortletSession();
		try {
		    String memberDetailsKeyCode = ParamUtil.getString(actionRequest, "memberDetailsKey");
		    
		    log.info("memberDetailsKeyCode : " + memberDetailsKeyCode);
		    Map<Integer, MemberDetailsFo> maplist = (Map<Integer, MemberDetailsFo>) httpSession.getAttribute("memberdetails");
		    log.info("maplist : " + maplist);
		    MemberDetailsFo memberDetailsFo = null;
		    log.info("memberdetails" + maplist);
		    if ((maplist != null) && (maplist.size() > 0)) {
			memberDetailsFo = maplist.get(Integer.parseInt(memberDetailsKeyCode));
			
			log.info("memberDetailsFo.setMemberNumber ------> " + memberDetailsFo.getMemberNumber());
			
			if ((memberDetailsFo.getMemberNumber() != null) && !(memberDetailsFo.getMemberNumber().equals(""))) {
			    log.info("memberDetailsFo : " + memberDetailsFo);
			    log.info("Member ID : " + memberDetailsFo.getMemberId());
		   		log.info("Member First Name : " + memberDetailsFo.getMemberFirstName());
		   		log.info("Member Middle Name : " + memberDetailsFo.getMemberMiddleName());
				log.info("Member Last Name : " + memberDetailsFo.getMemberLastName());
				log.info("Member Suffix Name : " + memberDetailsFo.getMemberSuffixName());
				log.info("Member DOB : " + memberDetailsFo.getMemberDob());
				log.info("Member Gender : " + memberDetailsFo.getGender());
				log.info("Member Address Line 1 : " + memberDetailsFo.getMemberAddressLine1());
				log.info("Member Address Line 2 : " + memberDetailsFo.getMemberAddressLine2());
				log.info("Member City : " + memberDetailsFo.getMemberCity());
				log.info("Member State : " + memberDetailsFo.getMemberStateCode());
				log.info("Member Zip Code : " + memberDetailsFo.getMemberZipcode());
				log.info("Phone Number : " + memberDetailsFo.getMemberPhoneNumber());

				log.info("Business Sector Code : " + memberDetailsFo.getBusinessSectorCode());
				log.info("Business Sector Description : " + memberDetailsFo.getBusinessSectorDescription());
				log.info("Business Segment Code : " + memberDetailsFo.getBusinessSegmentCode());
				log.info("Business Segment Description : " + memberDetailsFo.getBusinessSegmentDescription());

				httpSession.setAttribute("memberDetailsFo", memberDetailsFo);
			    
			    // Set the attributes shown in the prior authorization pages
			    portletSession.setAttribute("memberDob", memberDetailsFo.getMemberDob(), PortletSession.APPLICATION_SCOPE);
			    portletSession.setAttribute("memberId", memberDetailsFo.getMemberId(), PortletSession.APPLICATION_SCOPE);
			    portletSession.setAttribute("memberNumber", memberDetailsFo.getMemberNumber(), PortletSession.APPLICATION_SCOPE);
			    portletSession.setAttribute("healthPlan", memberDetailsFo.getHealthPlan(), PortletSession.APPLICATION_SCOPE);
			    portletSession.setAttribute("mpi", memberDetailsFo.getMpi(), PortletSession.APPLICATION_SCOPE);
			    portletSession.setAttribute("membergroupId", memberDetailsFo.getGroupId(), PortletSession.APPLICATION_SCOPE);
			    portletSession.setAttribute("memberName", memberDetailsFo.getMemberFirstName() + " " + memberDetailsFo.getMemberLastName(), PortletSession.APPLICATION_SCOPE);
			    portletSession.setAttribute("memberGender", memberDetailsFo.getDisplayGender(), PortletSession.APPLICATION_SCOPE);
			    portletSession.setAttribute("memberAddressLine1", memberDetailsFo.getMemberAddressLine1(), PortletSession.APPLICATION_SCOPE);
			    portletSession.setAttribute("memberAddressLine2", memberDetailsFo.getMemberAddressLine2(), PortletSession.APPLICATION_SCOPE);
			    portletSession.setAttribute("memberCity", memberDetailsFo.getMemberCity(), PortletSession.APPLICATION_SCOPE);
			    portletSession.setAttribute("memberStateCode", memberDetailsFo.getMemberStateCode(), PortletSession.APPLICATION_SCOPE);
			    portletSession.setAttribute("memberZipcode", memberDetailsFo.getMemberZipcode(), PortletSession.APPLICATION_SCOPE);
			    portletSession.setAttribute("memberPhoneNumber", memberDetailsFo.getMemberPhoneNumber(), PortletSession.APPLICATION_SCOPE);

				// Set the additional attributes saved by the prior authorization pages
				portletSession.setAttribute("memberFirstName", memberDetailsFo.getMemberFirstName(), PortletSession.APPLICATION_SCOPE);
				portletSession.setAttribute("memberMiddleName", memberDetailsFo.getMemberMiddleName(), PortletSession.APPLICATION_SCOPE);
				portletSession.setAttribute("memberLastName", memberDetailsFo.getMemberLastName(), PortletSession.APPLICATION_SCOPE);
				portletSession.setAttribute("memberSuffixName", memberDetailsFo.getMemberSuffixName(), PortletSession.APPLICATION_SCOPE);
				portletSession.setAttribute("businessSectorCode", memberDetailsFo.getBusinessSectorCode(), PortletSession.APPLICATION_SCOPE);
				portletSession.setAttribute("businessSectorDescription", memberDetailsFo.getBusinessSectorDescription(), PortletSession.APPLICATION_SCOPE);
				portletSession.setAttribute("businessSegmentCode", memberDetailsFo.getBusinessSegmentCode(), PortletSession.APPLICATION_SCOPE);
				portletSession.setAttribute("businessSegmentDescription", memberDetailsFo.getBusinessSegmentDescription(), PortletSession.APPLICATION_SCOPE);

				// Set the subscriber name attribute for the header in the the prior authorization pages
				portletSession.setAttribute("subscriberName", memberDetailsFo.getSubscriberName(), PortletSession.APPLICATION_SCOPE);

				// Determine if this is PA or PSPPR based on the query string
			    String entryType = (String)portletSession.getAttribute("entryType", PortletSession.APPLICATION_SCOPE);
			    String url = "";
			    String providerInfoPage = "";
				if (entryType.equalsIgnoreCase(MemberSearchConstants.POST_SERVICE_REVIEW)) {
					url = com.liferay.util.portlet.PortletProps.get("post-service-page-url");
					providerInfoPage = com.liferay.util.portlet.PortletProps.get("post-service-provider-info-page");
					
				} else {
					url = com.liferay.util.portlet.PortletProps.get("prior-auth-page-url");
					providerInfoPage = com.liferay.util.portlet.PortletProps.get("provider-info-page");
				}
				layout = LayoutLocalServiceUtil.getFriendlyURLLayout(themeDisplay1.getLayout().getGroupId(), true, url);
				PortletURL portletURL = PortletURLFactoryUtil.create(httpRequest, providerInfoPage, layout.getPlid(), PortletRequest.RENDER_PHASE);

			    portletURL.setWindowState(WindowState.MAXIMIZED);
			    portletURL.setPortletMode(PortletMode.VIEW);
			    actionResponse.sendRedirect(portletURL.toString());
			} else {
				actionRequest.setAttribute("message", MemberSearchConstants.INVALID_MEMBER_DATA);
			    actionResponse.setRenderParameter("mvcPath", "/html/jsps/memberDetails.jsp");
			}
		    } else {
		    	actionRequest.setAttribute("message", MemberSearchConstants.INVALID_MEMBER_DATA);
				actionResponse.setRenderParameter("mvcPath", "/html/jsps/memberDetails.jsp");
		    }
		} catch (com.liferay.portal.kernel.exception.SystemException e) {
			log.error("SystemException in getMemberDetails method",e);
		} catch (PortalException e) {
			log.error("PortalException in getMemberDetails method",e);
		}
    	log.info("Processing getMemberDetails Action for Member Search Exit");
    }

    
    /**
     * Whenever user click on cancel button cancelPage method will be executed
     */
    @ActionMapping(params = "action=cancelPage")
    public void cancelPage(ActionRequest actionRequest,
	                       ActionResponse actionResponse) {

    	log.info("Processing Cancel Action for Member Search Enter");
	    String userName = null;
	    List usersList = null;
		try {
		    User user = PortalUtil.getUser((PortletRequest) actionRequest);
		    if (user != null) {
			    usersList = user.getUserGroups();
		    }
		} catch (PortalException e) {
			log.error("PortalException in cancelPage method",e);
		} catch (SystemException e) {
			log.error("SystemException in cancelPage method",e);
		}
	    ArrayList<String> groupNames = new ArrayList<String>();
	    if (usersList != null) {
	    	int length = usersList.size();
		    for (int i = 0; i < length; ++i) {
		    	
		    	// Ignore the groups that do not have a home page
				if (!((UserGroup)usersList.get(i)).getName().equalsIgnoreCase("CDSUsers") &&
					!((UserGroup)usersList.get(i)).getName().equalsIgnoreCase("Everyone") &&
					!((UserGroup)usersList.get(i)).getName().equalsIgnoreCase("ManagedUsers") &&
					!((UserGroup)usersList.get(i)).getName().equalsIgnoreCase("Multi-Factor") &&
					!((UserGroup)usersList.get(i)).getName().equalsIgnoreCase("PortalAdmin") &&
					!((UserGroup)usersList.get(i)).getName().equalsIgnoreCase("UMGroup")) {
					userName = ((UserGroup) usersList.get(i)).getName();
					groupNames.add(userName);
				}
		    }
	    }
	    HttpServletRequest request = PortalUtil.getHttpServletRequest((PortletRequest) actionRequest);
	    String path = PortalUtil.getCurrentCompleteURL((HttpServletRequest) request);
	    StringBuilder output = new StringBuilder();
	    int count = 0;
	    char[] ch = path.toCharArray();
	    for (int i2 = 0; i2 < ch.length; ++i2) {
			if (ch[i2] == '/') {
			    ++count;
			}
			if (count >= 3)
			    continue;
			output = output.append(ch[i2]);
	    }

	    String landingPage = null;
	    if (usersList == null) {
			
			// Set to the default landing page since the user does not belong to any groups
			landingPage = "/web/guest/home";
	    } else {
	    	

	    	// Get the landing page based on the user's group
			landingPage = com.liferay.portal.kernel.util.PropsUtil.get("default.landing.page.path[" + userName  + "]"); 
			
			// If the user's group is not found use the defaule landing page
			if (landingPage == null) {
				landingPage = com.liferay.portal.kernel.util.PropsUtil.get("default.landing.page.path");
				if (landingPage == null) {
					
					// Set to the default landing page since it is not defined in the properties file
					landingPage = "/web/guest/home";
				}
			}
	    }
	    String pathRedirect = null;
		pathRedirect = output + landingPage;
		log.info(userName + " " + pathRedirect);
		try {
		    actionResponse.sendRedirect(pathRedirect);
		} catch (IOException e) {
			log.error("IOException during redirection in cancelPage method", e);
		}
    	log.info("Processing Cancel Action for Member Search Exit");
    }
    
    /**
     * Create a json message for an error
     */
	private String createErrorMessage(String errorMsg) {
		Gson gson = new Gson(); 
		
		List memberList = new ArrayList();
		Map <String, Object> memSearch = new HashMap<String, Object>();
		memSearch.put(MemberSearchConstants.ERROR, errorMsg);
		memberList.add(memSearch);
		String memberInfo = gson.toJson(memberList);
		
		return memberInfo;
	}
}