package com.avalon.member.search.beans;

/**
 * Description
 *		This file contains the constants for the PA Entry Page.
 *
 * @author David Sanchez
 *
 * CHANGE History
 * 		Version 1.0
 * 			Initial version
 *		Version 1.1
 *			Added changes for initial version of Post Servce Review
 *		Version 1.2			12/12/2017
 *			Changed REASON_FOR_INQUIRY to P
 *  
 */

public interface MemberSearchConstants {
    public static final String REASON_FOR_INQUIRY = "P";
    public static final String HEALTH_PLAN_ID_TYPE = "PI";
    public static final String VIEW_DISPLAY = "memberDetails";
    
    // Entity types
    public static final String PRIOR_AUTH = "PA";
    public static final String POST_SERVICE_REVIEW = "PSR";
    
    // List Keys
    public static final String MEMBER_ID = "memberId";
    public static final String HEALTH_PLAN = "healthPlan";
	public static final String FIRST_NAME = "memberFirstName";
	public static final String LAST_NAME = "memberLastName";
	public static final String DATE_OF_BIRTH = "memberDob";
	public static final String GENDER = "gender";
	public static final String KEY_CODE = "keyCode";

    // Error messages
	public static final String ERROR = "error";
	public static final String NO_MEMBER_FOUND = "No members were found that match your search criteria. Please update the search and try again.";
	public static final String PROCESSING_ERROR = "Error while processing the request. Please contact system administrator";
	public static final String INVALID_MEMBER_DATA = "Invalid member data";
	
	//TODO: Remove below code
    public static final String ENABLED_MOCK_SERVICE = "enabled.mock.service";
}
