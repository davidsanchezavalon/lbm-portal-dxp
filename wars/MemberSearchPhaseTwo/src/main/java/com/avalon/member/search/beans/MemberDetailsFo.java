package com.avalon.member.search.beans;

import java.io.Serializable;

/**
 * Description
 *		This file contain the getter and setter methods for the PA Entry Page.
 *
 * @author David Sanchez
 *
 * CHANGE History
 * 		Version 1.0
 * 			Initial version
 * 		Version 1.1				08/08/2018
 * 			Added Subscriber Name
 *  
 */

public class MemberDetailsFo implements Serializable {
    /**
     * To store the member details fields
     */
    private static final long serialVersionUID = -6258273394902792568L;
    private String keyCode;
    private String memberId;
    private String memberFirstName;
    private String memberMiddleName;
    private String memberLastName;
    private String memberSuffixName;
    private String patientId;
    private String healthPlan;
    private String healthPlanName;
    private String memberNumber;
    private String healthPlanGroupValue;
    private String memberDob;
    private String groupId;
    private String gender;
    private String subscriberId;
    private String memberRelationship;
    private String mpi;
    private String memberAddressLine1;
    private String memberAddressLine2;
    private String memberCity;
    private String memberStateCode;
    private String memberZipcode;
    private String memberPhoneNumber;
    private String businessSectorCode;
    private String businessSectorDescription;
    private String businessSegmentCode;
    private String businessSegmentDescription;
    private String subscriberName;

    /**
     * @return the memberNumber
     */
    public String getMemberNumber() {
        return memberNumber;
    }

    /**
     * @param memberNumber
     *            the memberNumber to set
     */
    public void setMemberNumber(String memberNumber) {
        this.memberNumber = memberNumber;
    }

    /**
     * @return the healthPlanName
     */
    public String getHealthPlanName() {
        return healthPlanName;
    }

    /**
     * @param healthPlanName
     *            the healthPlanName to set
     */
    public void setHealthPlanName(String healthPlanName) {
        this.healthPlanName = healthPlanName;
    }

    public MemberDetailsFo() {
    }

    /**
     * @return the mpi
     */
    public String getMpi() {
        return mpi;
    }

    /**
     * @return the healthPlanGroupValue
     */
    public String getHealthPlanGroupValue() {
        return healthPlanGroupValue;
    }

    /**
     * @param healthPlanGroupValue
     *            the healthPlanGroupValue to set
     */
    public void setHealthPlanGroupValue(String healthPlanGroupValue) {
        this.healthPlanGroupValue = healthPlanGroupValue;
    }

    /**
     * @param mpi
     *            the mpi to set
     */
    public void setMpi(String mpi) {
        this.mpi = mpi;
    }

    /**
     * @return the patientId
     */
    public String getPatientId() {
        return patientId;
    }

    /**
     * @param patientId
     *            the patientId to set
     */
    public void setPatientId(String patientId) {
        this.patientId = patientId;
    }

    public String getSubscriberId() {
        return subscriberId;
    }

    public void setSubscriberId(String subscriberId) {
        this.subscriberId = subscriberId;
    }

    public String getMemberRelationship() {
        return memberRelationship;
    }

    public void setMemberRelationship(String memberRelationship) {
        this.memberRelationship = memberRelationship;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId.toUpperCase();
    }

    public String getKeyCode() {
        return keyCode;
    }

    public void setKeyCode(String keyCode) {
        this.keyCode = keyCode.toUpperCase();
    }

    public String getMemberFirstName() {
        return memberFirstName;
    }

    public void setMemberFirstName(String memberFirstName) {
        this.memberFirstName = memberFirstName;
    }

	public String getMemberMiddleName() {
	    return memberMiddleName;
	}
	
	public void setMemberMiddleName(String memberMiddleName) {
	    this.memberMiddleName = memberMiddleName;
	}

    public String getMemberLastName() {
        return memberLastName;
    }

    public void setMemberLastName(String memberLastName) {
        this.memberLastName = memberLastName;
    }

	public String getMemberSuffixName() {
	    return memberSuffixName;
	}
	
	public void setMemberSuffixName(String memberSuffixName) {
	    this.memberSuffixName = memberSuffixName;
	}

    public String getHealthPlan() {
        return healthPlan;
    }

    public void setHealthPlan(String healthPlan) {
        this.healthPlan = healthPlan;
    }

    public String getMemberDob() {
        return memberDob;
    }

    public void setMemberDob(String memberDob) {
        this.memberDob = memberDob;
    }

    public String getDisplayGender() {
    	String returnGender = null;

    	if (gender.equalsIgnoreCase("M")) {
    		returnGender = "Male";
    	} else if (gender.equalsIgnoreCase("F")) {
    		returnGender = "Female";
    	} else {
    		returnGender = gender;
    	}
        return returnGender;
    }

	public String getGender() {
		return gender;
	}

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getMemberAddressLine1() {
        return memberAddressLine1;
    }

    public void setMemberAddressLine1(String memberAddressLine1) {
        this.memberAddressLine1 = memberAddressLine1;
    }

    public String getMemberAddressLine2() {
        return memberAddressLine2;
    }

    public void setMemberAddressLine2(String memberAddressLine2) {
        this.memberAddressLine2 = memberAddressLine2;
    }

    public String getMemberCity() {
        return memberCity;
    }

    public void setMemberCity(String memberCity) {
        this.memberCity = memberCity;
    }

    public String getMemberStateCode() {
        return memberStateCode;
    }

    public void setMemberStateCode(String memberStateCode) {
        this.memberStateCode = memberStateCode;
    }

    public String getMemberZipcode() {
        return memberZipcode;
    }

    public void setMemberZipcode(String memberZipcode) {
        this.memberZipcode = memberZipcode;
    }

    public String getMemberPhoneNumber() {
        return memberPhoneNumber;
    }

    public void setMemberPhoneNumber(String memberPhoneNumber) {
        this.memberPhoneNumber = memberPhoneNumber;
    }
    
	public String getBusinessSectorCode() {
	    return businessSectorCode;
	}
	
	public void setBusinessSectorCode(String businessSectorCode) {
	    this.businessSectorCode = businessSectorCode;
	}
	
	public String getBusinessSectorDescription() {
	    return businessSectorDescription;
	}
	
	public void setBusinessSectorDescription(String businessSectorDescription) {
	    this.businessSectorDescription = businessSectorDescription;
	}
	
	public String getBusinessSegmentCode() {
	    return businessSegmentCode;
	}
	
	public void setBusinessSegmentCode(String businessSegmentCode) {
	    this.businessSegmentCode = businessSegmentCode;
	}
	
	public String getBusinessSegmentDescription() {
	    return businessSegmentDescription;
	}
	
	public void setBusinessSegmentDescription(String businessSegmentDescription) {
	    this.businessSegmentDescription = businessSegmentDescription;
	}

    public String getSubscriberName() {
        return subscriberName;
    }

    public void setSubscriberName(String subscriberName) {
        this.subscriberName = subscriberName;
    }
}
