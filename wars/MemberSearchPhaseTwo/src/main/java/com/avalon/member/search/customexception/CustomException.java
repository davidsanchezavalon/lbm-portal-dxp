package com.avalon.member.search.customexception;

import javax.portlet.ActionRequest;

import com.liferay.portal.kernel.servlet.SessionErrors;

public class CustomException extends Exception {
    /**
     * To display the exception message
     */
    private static final long serialVersionUID = 1L;

    public CustomException(String errorMessage) {
    	super(errorMessage);
    }

    public CustomException(String errorKey, String message, ActionRequest actionRequest) {
    	SessionErrors.add(actionRequest, errorKey);
    }
}
