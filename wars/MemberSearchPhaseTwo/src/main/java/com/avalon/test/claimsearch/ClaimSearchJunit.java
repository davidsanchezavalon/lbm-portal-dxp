/*package com.avalon.test.claimsearch;

import com.avalon.claimsearch.businessdelegate.ClaimsSearchBussinessDelegateVo;
import com.avalon.claimsearch.fo.ClaimSearchFO;
import com.avalon.claimsearch.fo.ClaimsSearchConstants;
import com.avalon.claimsearch.fo.MemberDetailsFo;
import com.liferay.portal.model.ClassName;
import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import org.junit.Test;

*//**
 * @author sai.s.kumar.cirigiri
 *
 *//*
public class ClaimSearchJunit implements ClaimsSearchConstants {
	
	private static final Logger log = Logger.getLogger( ClassName.class.getName() );
private ClaimSearchFO claimSearchFO;


public List getClaimDetails(ClaimSearchFO claimSearchFO ){

    
    String npiNumber = null ; 
    //Map<Integer, MemberDetailsFo> mapList = (ConcurrentHashMap<Integer, MemberDetailsFo>)session.getAttribute(MEMBER_DETAILS_LIST_SESSION); // to get the member details  from session
    MemberDetailsFo memberDetailsFo = null;
    
    if(  (mapList!=null) && (mapList.size()>0)){
    	memberDetailsFo = mapList.get(Integer.parseInt(memberKey));
    }
    
	List list = new ArrayList();
	ClaimStatusInboundReq claimStatusInboundReq = new ClaimStatusInboundReq(); 
	LBMClaimSearchServiceProxy lBMClaimSearchServiceProxy = new LBMClaimSearchServiceProxy();
	LBMClaimSearchService lBMClaimSearchService = lBMClaimSearchServiceProxy.getLBMClaimSearchService();
	FindClaims findClaims = new FindClaims();
	ClaimSearchCriteria claimSearchCriteriaInbound = new ClaimSearchCriteria();
	if(claimSearchFO.getClaimId()==null||claimSearchFO.getClaimId()==""){ // Search by from date and to date
		claimSearchCriteriaInbound.setServiceDateFrom(ClaimsSearchBussinessDelegateVo.getClaimsSearchBussinessDelegateVo().getDate(claimSearchFO.getDateofServiceFrom()));
 		claimSearchCriteriaInbound.setServiceDateTo(ClaimsSearchBussinessDelegateVo.getClaimsSearchBussinessDelegateVo().getDate(claimSearchFO.getDateofServiceTo()));
 	}else{  // Search by Claim Id
 		
 		claimSearchCriteriaInbound.setSearchClaimNumber(claimSearchFO.getClaimId());
 		claimSearchCriteriaInbound.setServiceDateFrom(null);
 		claimSearchCriteriaInbound.setServiceDateTo(null);
 	}
	
	Code carrierCode = new Code();
	carrierCode.setCode(CARRIER_CODE_VALUE);
	carrierCode.setDescription("");
	claimSearchCriteriaInbound.setCarrierCode(carrierCode);
	HealthClaimSearchCriteria healthClaimSearchCriteria = new HealthClaimSearchCriteria();
	healthClaimSearchCriteria.setBlueExchangeDaysToWait(BLUE_EXCHANGE_DAYS_TO_WAIT); //0
	claimSearchCriteriaInbound.setHealthClaimSearchCriteria(healthClaimSearchCriteria);
	ClaimSearchFilter claimSearchFilterInbound = new ClaimSearchFilter();
	claimSearchFilterInbound.setIdentifySensitiveClaims(TRUE);
	claimSearchFilterInbound.setIncludePrivacyProtectedClaims(TRUE);
	claimStatusInboundReq.setClaimSearchFilterInbound(claimSearchFilterInbound);
	Patient[] patient = new Patient[1];
	List<Patient> patientList  = new ArrayList<Patient>();
	for(int i=0;i<patient.length;i++){
		Patient patients = new Patient();
		 SubscriberId subscriberId = new SubscriberId();
		 CoverageInformation coverageInformation = new CoverageInformation();
		 Code code = new Code();
		 patients.setSubscriberId(subscriberId);
		 patients.setCoverageInformation(coverageInformation);
		 patients.setRelationshipToSubscriber(code); 
		
		 patients.getSubscriberId().setMemberNumber("4567777L");
		 patients.getRelationshipToSubscriber().setCode("");
		 patients.getRelationshipToSubscriber().setDescription("");
		 patients.getCoverageInformation().setDependentId("001");
		 patients.getCoverageInformation().setPrivacyProtectionEnabled(FALSE);
		 patients.setOutOfAreaPatient(FALSE);
		 patientList.add(patients);
	}
		claimStatusInboundReq.setPatients((Patient[]) patientList.toArray(new Patient[patientList.size()]));
		ProviderInformation  providerInfo = new ProviderInformation();
		ProviderIdentifier providerIdentifier = new ProviderIdentifier();
	    //npiNumber = (String)session.getAttribute(NPI_INDIVIDUAL_NUMBER_SESSION);
	    log.info("npiNumber : " +npiNumber );
		providerIdentifier.setNationalProviderIdentifier(npiNumber);
		providerInfo.setProviderIdentifier(providerIdentifier);
		claimStatusInboundReq.setClaimSearchCriteriaInbound(claimSearchCriteriaInbound);
		claimStatusInboundReq.setProviderInfo(providerInfo);
		findClaims.setClaimStatusInboundReq(claimStatusInboundReq);
	try {
		
			ClaimSummariesPatient[] claimSummariesPatient    =   lBMClaimSearchService.findClaims(findClaims);  //Invoking the findclaims by setting all the parameters in the request object  
			
	
			if( (claimSummariesPatient!=null) && (claimSummariesPatient.length>0)){
		
				String checkIssueDate = null;
				String claimStatus   = null;
				
				List<ClaimSummariesPatient> patientlist = Arrays.asList(claimSummariesPatient);
		
					for(int i=0;i<patientlist.size();i++){
			
						ClaimSummariesPatient claimSummariesPatients = patientlist.get(i);
						ClaimsSummaryItem[] claimsSummaryItems = claimSummariesPatients.getClaimSummary().getClaimsSummaryItems();
						String memberId = "889122";
					
						
			
							if( (claimsSummaryItems!=null) &&  (claimsSummaryItems.length>0) ){
								int count = claimsSummaryItems.length - 1;
								
									for(int j = 0 ; j < claimsSummaryItems.length;j++){
				
										Map <String, Object> search= new HashMap<String ,Object>();
				 
										if(claimsSummaryItems[j].getCheckIssueDate()!=null) {               
										checkIssueDate = getCalenderDate(claimsSummaryItems[j].getCheckIssueDate());
										}

										log.info(" checkIssueDate : " +checkIssueDate);
				 
										claimStatus = claimsSummaryItems[j].getStatusType().getValue();
				  
										log.info(" claimStatus : " +claimStatus);
			        
											if(  ( (claimStatus.equals(HARD_PENDED)) || (claimStatus.equals(PENDING) )  ) ){ // To check the cliam status when check issue date blank or value
												log.info("ClaimStatus : In Process ");
												search.put(CLAIM_STATUS,PENDING_STATUS);  // to display in view page
											}
			        
			        
											if(  ( (claimStatus.equals(DENIED)) || (claimStatus.equals(PROCESSED) ) || (claimStatus.equals(REJECTED) )  )   &&  ((checkIssueDate!=null) ) ){ // To check the cliam status when check issue date has value
												log.info("ClaimStatus : Settled ");
												search.put(CLAIM_STATUS,DENIED_WITH_CHECKDATE); // to display in view page
											}
			        
			        
											if(  ( (claimStatus.equals(DENIED)) || (claimStatus.equals(PROCESSED) ) || (claimStatus.equals(REJECTED) )  )  &&  ((checkIssueDate == null) ||(checkIssueDate.isEmpty())) ){ // To check the cliam status when check issue date is blank
												log.info("ClaimStatus : Completed ");
												search.put(CLAIM_STATUS,DENIED_WITHOUT_CHECKDATE); // to display in view page
											}

											search.put(CLAIM_ID,claimsSummaryItems[j].getClaimNumber()); // to display in view page
											search.put(MEMBER_ID,memberId); // to display in view page
				
											if( ( (claimsSummaryItems[j].getProviderID())!=null)){
											search.put(PROVIDER_ID,claimsSummaryItems[j].getProviderID()); // to display in view page
											}
											else{
												search.put(PROVIDER_ID,npiNumber); // to display in view page 
											}
											search.put( DATE_OF_SERVICE_FROM,  getCalenderDate(claimsSummaryItems[j].getClaimDateOfService().getFromDate()) ); // to display in view page
											search.put(DATE_OF_SERVICE_TO,getCalenderDate(claimsSummaryItems[j].getClaimDateOfService().getToDate())); // to display in view page
											search.put(TOTAL_CHARGE_GREATER_THAN,"$ "+claimsSummaryItems[j].getTotalClaimChargeAmount()); // to display in view page
											checkIssueDate = null;
											claimStatus   = null;
											list.add(search);
				
				
									}


			
							}else{
			
								Map <String, Object> search= new HashMap<String ,Object>();
								search.put(ERROR,  CLAIMS_ERROR); // To handle the error when their is no proper data in patient list
								list.add(search);
							}
					}
							}else{
			
								Map <String, Object> search= new HashMap<String ,Object>();
								search.put(ERROR,  CLAIMS_ERROR); // To handle the error when their is no proper data in claimsSummaryItems
								list.add(search);
							}
						
	
		} catch (RemoteException e) {
		
			Map <String, Object> memSearch= new HashMap<String ,Object>();
		 
			if(e.getMessage().equals(CLAIM_NOT_FOUND_ON_POINTER_FILE)){ // To handle the exception when their is no claim id 
			 
				memSearch.put(ERROR, CLAIMS_ERROR);
			 
			}else{
				memSearch.put(ERROR,ERROR_WHILE_PROCESSING); // To handle the exception when the esb servers are down
			}
    	 
			list.add(memSearch);
	
			log.warning(" ----> "+this.getClass().getName());
			log.warning("Exception : " +e.getMessage());
		  	
	}
	
	
		return list;
	
}


























@Test
public void testbasicSearch(){
	ClaimSearchJunit claimSearchJunit = new ClaimSearchJunit();
	claimSearchFO = new ClaimSearchFO();
	claimSearchFO.setClaimId("77777");
	claimSearchFO.setClaimId(null);
	claimSearchFO.setDateofServiceFrom("04/12/2015");
	claimSearchFO.setDateofServiceTo("05/12/2015");
	List list  = claimSearchJunit.getClaimDetails(claimSearchFO);
	
    for(int i=0;i<list.size();i++){
    	
    	Map <String, Object> search = (Map <String, Object>)list.get(i);
    	for(Map.Entry<String, Object> entry:search.entrySet()) {
    		
    		System.out.println(entry.getKey() +" : " +entry.getValue());
    		
    	}
    	
    	
    	
    }
	
	
	
	
}



public String getCalenderDate(Calendar calender){     // To get the date in MM/dd/yyyy format for  getClaimDetails response object
	
	
	calender.add(Calendar.DATE, 1);
	Date date = calender.getTime();             
	SimpleDateFormat format1 = new SimpleDateFormat("MM/dd/yyyy");          
	String inActiveDate = null;
	try {
			inActiveDate = format1.format(date);
			log.info("Date" +inActiveDate );
	} 	catch (Exception e) {
		log.warning(e.getMessage());
	}
	return inActiveDate;

}




}




*/