/**
 * Description
 *		This file contain the javascript for Prior Auth 2.5 Search.
 *
 * CHANGE History
 * 		Version 1.0  
 *			Moved from Prior Auth.  
 *		Version 1.1
 * 			isValidDigit method changed from number validation to alphanumeric validation of ID card number and removed unused jquery.js files.
 * 		Version 1.2				02/15/2018
 * 			Added checkItem and removeWhitespace functions.
 *
 */

var exitString = 'You have made updates to this page that have not been saved.  If you continue the updates will be lost.';

makeFormResponsive();
formatRequiredLabels('*');

function makeFormResponsive() {
	var groups = document.getElementById('main-content').getElementsByClassName('control-group');
	
	for (var i = 0, n = groups.length; i < n; i++ ) {
		groups[i].setAttribute('class', groups[i].getAttribute('class') + ' row-fluid');
		
		var label = groups[i].getElementsByTagName('label')[0];
		
		if (label) {
			label.setAttribute('class', label.getAttribute('class') + ' span5');
		}
		
		var input = groups[i].getElementsByTagName('input')[0];
		
		if (input) {
			input.setAttribute('class', input.getAttribute('class') + ' span7');
		}
	}
}

function formatRequiredLabels(text) {
	var reqLabels = document.getElementById('main-content').getElementsByClassName('label-required');
	
	for ( var i = 0, n = reqLabels.length; i < n; i++ ) {
		reqLabels[i].innerHTML = text;
	}
}

function showBusySign() {
	
	// The form must be submitted before the animated GIF will move in IE
	//$('#' + getNamespace() + 'memberInformationDetails').submit();

	document.getElementById('busy_indicator').style.display = 'block';
}

function checkValidForm() {
	if (isValidForm()) {
		showBusySign();
	}
}

function isValidDigit(_dat, _len) {
	var rtnValue = false;
	var length = _dat.length;

	var isNumber =  /^\w+$/.test(_dat);//changed validation of ID card number from numeric to alphanumeric
	if (isNumber && (_dat.length == _len)) {
		rtnValue = true;
	}
	
	return rtnValue;
}

// Cause the AUI validation to fire
function checkItem(checkId) {

	// Save the current active element
	var activeId = document.activeElement.id;

	// Set the focus then leave each field to display any error messages
	$("#" + checkId).focus();
	$("#" + checkId).blur();
	
	// Set focus to the saved field
	$("#" + activeId).focus();
}

function removeWhitespace(itm) {
	 
	// Get the trimmed value
	var trimItm = $("#" + itm).val().trim();

	if (trimItm) {

        	// Update the form with the trimmed value
        	$("#" + itm).val(trimItm);
	}
	else {
		return false;
	}
}
