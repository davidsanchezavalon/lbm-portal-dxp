
<%-- 
 /**
  * Description
  *		This file contain the Member Search portlet for Prior Auth 2.0.
  *
  * @author David Sanchez
  *
  * CHANGE History
  * 	Version 1.0
  *			Converted to Alloy UI.
  *		Version 1.1
  *			Removed JQuery model dialog,fixed LJR-364,365,423 tickets
  *			dialog-service div is added,onclick find member checkValidForm() method is added,
  *				isValidForm method id card number id is corrected to memberID
  * 	Version 1.2
  *     	Added Cancel button
  * 	Version 1.3
  *     	Convert ID Card number to upppercase.
  *			Remove hardcoded of Health Plan.
  *		Version 1.4						01/03/2018
  *			Made the changes needed to use DataTables
  *		Version 1.5						01/30/2018
  *			Disable Find Member button until all required fields are valid.
  *			When the Find Member button is disabled show a mouse over tooltip with what is missing.
  *			Added Enter key functionality.
  * 	Version 1.6						02/15/2018
  * 		Added trim to the ID Card number entered.
  *		Version 1.7						06/06/2018
  *			Change the cancel button from a hyperlink to a button.
  *		Version 1.8						07/16/2018
  *			Move the label to the left of the field.
  *
  */
--%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui"%>
<%@ taglib uri="http://liferay.com/tld/portlet" prefix="liferay-portlet"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<%@ page import="com.avalon.member.search.beans.MemberSearchConstants" %>

<portlet:defineObjects />

<portlet:resourceURL var="memberInfoSearchformURL" id="memberInfoSearchform">
</portlet:resourceURL>

<portlet:actionURL var="cancelURLByDetailsTagURL">
	<portlet:param name="action" value="cancelPage" />
</portlet:actionURL>

<liferay-portlet:renderURL var="redirectURL">
	<liferay-portlet:param name="mvcPath" value="/html/jsps/memberDetails.jsp" />
</liferay-portlet:renderURL>

<aui:script>
	var toolTip = "";
	var errorHandler = "<c:out value='${message}'/>"; 

	function getNamespace() {
		return('<portlet:namespace/>');
	}
	
	$(document).ready(function(){

		// Set the initial find button status
		setFindButtonStatus();

		// Execute the search when the enter key is pressed for the ID card number
		processEnterKey("memberID");
	});

	YUI().ready('aui-node','event',
		function(Y) {
			if(Y.one('#heading span')){	
			   var portletTitle = Y.one('#heading span').getHTML();
				Y.one('#heading span').setHTML('Member Selection');
			}		
	});	

	function convertToUppercase(val) {
		val.value = val.value.toLocaleUpperCase();
	}
	

	function closePortlet() {
		var url='<%=cancelURLByDetailsTagURL.toString()%>';
		window.location.href = url;
	}

	function callCancel(evt){
		var cinfo = confirm("Do you want to cancel the search?");
		if(cinfo){
	           var url = '<%=cancelURLByDetailsTagURL.toString()%>';
	           var x=document.getElementsByTagName("form");
	           x[0].action=url;
	           x[0].submit();
		}else{
			evt.preventDefault();
		}
	}

	function sendMemberDetails(memberKey){
		var url = '<%=cancelURLByDetailsTagURL.toString()%>';
		var x = document.getElementsByTagName("form");
		x[0].action = url;
		x[0].submit();
		return false;
	}

	function isValidForm() {

		// Remove any whitespace from the id card number
		idCardNumberName = "<portlet:namespace/>memberID";
		removeWhitespace(idCardNumberName);

		// Remove any invalid error messages
		checkItem(idCardNumberName);
		
		var formValid = false;
		var idCardNumber = $("#" + idCardNumberName).val();
		var cardLen = 11;
		var exp = /^([a-zA-Z0-9]+)$/;

		// Initialize the tool tip for the find button
		toolTip = "";

		if ((idCardNumber == null) || (idCardNumber === '') || (idCardNumber.length === 0) || !(idCardNumber.match(exp))) {
			formValid = false;
			toolTip = "The ID Card Number is not valid";
		} else {
			formValid = true;
			toolTip = "";
		}
		return formValid;
	}
	
	function createLink(id, key) {
		var lnk = '<a href="<portlet:actionURL><portlet:param name="action" value="getMemberDetails" />' +
                  '<portlet:param name="memberDetailsKey" value="000000" /> ' +
                  '</portlet:actionURL>">' + id + '</a>';
                  
		lnk = lnk.replace("000000", key);
		return lnk;
	}
	
	function getSearchResult() {

		// Display the member search results
		getMemberSearchResult();
	}
	
	function getMemberSearchResult() {
		$("#memSearchResults").hide();
		$("#jsonErrorMessage").hide();

		if (isValidForm()) {
			$("#dataloader").show();

			$.ajax({
				type : "POST",
				data : $('#<portlet:namespace/>memberInformationDetails').serialize(),
				dataType : "json",
				url : "${memberInfoSearchformURL}",
				
				success : function(memberInfo) {
					var memdata = JSON.stringify(memberInfo);

					var isError;
					for (var i = 0; i < memberInfo.length; i++) { 
						isError = memberInfo[i].error;
					}
				
					$("#dataloader").hide();
					if( (isError != null && isError != "" && isError != "undefined")  ){
						$("#jsonErrorMessage").show();
						$("#jsonErrorText").text(isError);
					} else {
						$("#memSearchResults").show();
						$("#memResults").dataTable({
							"bDestroy" : true,
							"sPaginationType" : "full_numbers",
							"bProcessing" : true,
							"aaData" : jQuery.parseJSON(memdata),
							"aoColumns" : [{
								"mData" : "<%= MemberSearchConstants.MEMBER_ID %>"
							}, {
								"mData" : "<%= MemberSearchConstants.HEALTH_PLAN %>"
							}, {
								"mData" : "<%= MemberSearchConstants.FIRST_NAME %>"
							}, {
								"mData" : "<%= MemberSearchConstants.LAST_NAME %>"
							}, {
								"mData" : "<%= MemberSearchConstants.DATE_OF_BIRTH %>"
							},{
								"mData" : "<%= MemberSearchConstants.GENDER %>"
							}],
							"aoColumnDefs" : [{
								"aTargets" : [0],
								"mRender" : function (data, type, row, meta) {
									console.log('type----->'+type);
									if (type === 'display') {
										data = createLink(data, row.keyCode);
									}
									return data;
								}
							}]
						}); 
					}
				},
		        error: function(jqXHR, textStatus, errorThrown) {
		        	alert("Error: jqXHR status: " + jqXHR.status + " jqXHR responseText: " + jqXHR.responseText + " Status: " + textStatus + " Message: " + errorThrown);
		        }
			});
		}
	}

	function setFindButtonStatus() {
		
		// Disable the find button until all the fields are valid
		if (isValidForm()) {
			$("#<portlet:namespace/>submitMemberSearch").removeAttr("disabled");
		} else {
			$("#<portlet:namespace/>submitMemberSearch").attr("disabled", "disabled");
		}
		
		// Add the mouse over text for the find button
		$("#<portlet:namespace/>submitMemberSearch").prop("title", toolTip);
	}

	// Disable the default enter processing
	$(document).keypress(function(event) {
		if (event.which == '13') {
			event.preventDefault();
		}
	});
	
	function processEnterKey(id) {
		document.getElementById(getNamespace() + id).addEventListener("keyup", function(event) {
			event.preventDefault();
			if (event.keyCode === 13) {
				getSearchResult();
			}
		});
	}
</aui:script>

<div id="busy_indicator" style="display: none">
	<img src="<%=request.getContextPath()%>/images/busy-spinner.gif" alt="Busy indicator">
</div>

<liferay-ui:panel title="Member Information" collapsible="false">
	<aui:form name="memberInformationDetails" commandname="memberDetailsFO" onChange="javascript:setFindButtonStatus()" >
		<aui:fieldset>
			<aui:container>
				<aui:row>
					<aui:col span="6">
						<aui:input type="text" inlineLabel="true" name="memberID" cssClass="form-control toUppercase" maxlength="17" label="ID Card Number:" style="text-transform:uppercase" 
						           value="${memberDetailsFO.memberId}" onfocusout="javascript:convertToUppercase(this)" >
							<aui:validator name="required" errorMessage="Please enter valid ID Card Number" />
							<aui:validator name="alphanum" errorMessage="Please enter valid ID Card Number" />
						</aui:input>
					</aui:col>
					<aui:col span="6">
						<aui:input type="hidden" value="123456789" name='healthPlanGroupValue' />
						<aui:select inlineLabel="true" name="healthPlan" label="Health Plan:" cssClass="span7" required="true" showRequiredLabel="true">
							<aui:option value="0010">BCBS &ndash; South Carolina</aui:option>
						</aui:select>
					</aui:col>
				</aui:row>
			</aui:container>
		</aui:fieldset>
		<aui:fieldset>
			<aui:button-row cssClass="btn-divider">
				<div onmouseenter="setFindButtonStatus()">
					<aui:button name="submitMemberSearch" cssClass="pull-right" type="button" primary="true" value="Find Member" onclick="javascript:getSearchResult()" />
				</div>
      			<aui:button name="cancelMemberSearchInfo" cssClass="btn-gray pull-left" type="button" value="Cancel" onclick="javascript:closePortlet()" />
			</aui:button-row>
		</aui:fieldset>
	</aui:form>
</liferay-ui:panel>

<div id="memSearchResults" style="display: none" >
	<liferay-ui:panel title="Results" collapsible="false">
		<table id="memResults" class="display" style="border-collapse: collapse; width: 100%" >
			<thead>
				<tr>
					<th>ID Card Number</th>
					<th>Group ID</th>
					<th>First Name</th>
					<th>Last Name</th>
					<th>Date of Birth</th>
					<th>Gender</th>
				</tr>
			</thead>
			<tbody>
	
			</tbody>
		</table>
	</liferay-ui:panel>
</div>

<div id="jsonErrorMessage" style="display: none; text-align: left; color: red" >
	<span id="jsonErrorText" > </span>
</div>

<div id="dataloader" class="blink_text" style="display: none" >  
     Please wait while loading the page...
</div>

<div class="dialog-service" title="Member Service" style="text-align: left; color: red;">
	${message}
</div>

<div class="yui3-skin-sam">
	<div id="modal"></div>
</div>
