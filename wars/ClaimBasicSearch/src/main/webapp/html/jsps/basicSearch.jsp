<%-- 
 /**
  * Description
  *		This file contain the Claims Search portlet.
  *
  * CHANGE History
  * 	Version 1.0
   *     	Removed Cancel button question.
  * 	Version 1.1
  *     	Convert to Alloy UI.
  * 	Version 1.2
  *     	Add clear button to "search by" screens.
  *		Version 1.3
  *			Switch all HTML DOM to jQuery.
  *		Version 1.4
  *			Use dataTables to show the results.
  *		Version 1.5
  *			Added changes to find a claim without member information.
  *			Added issue date and check number.
  *		Version 1.6
  *			When North Carolina is selected:
  *				Disable member search
  *				Hide member search results and error display
  *				Clear the member ID field
  *		Version 1.7
  *			Hide the error display when switching buttons on the main claim search page.
  *			Clear the opposite id fields when switching buttons on the main claim search page.
  *			Clear both id fields when the carrier is changed.
  *		Version 1.8
  *			Hide the close icon on the popup.
  *		Version 1.9						12/26/2017
  *			Removed member Id length check in isValidForm.
  *			Disable the member ID and claim ID radio buttons is a health plan is not selected.
  *			Added a mouse over message for the member ID disabled. 
  *		Version 1.10					01/22/2018
  *			When both Member ID and Claim ID radio buttons are disabled show a mouse over tooltip with what is missing.
  *			When Member ID is selected and the Search button is disabled, show a mouse over tooltip with what is missing.
  *			When Claim ID is selected and the Search button is disabled, show a mouse over tooltip with what is missing.
  *			For Search By Service Date, when the Find Claim button is disabled show a mouse over tooltip with what is missing.
  *			Added Enter key functionality.
  *		Version 1.11					03/15/2018
  *			Validate the from and to dates before converting the date for IE.
  *		Version 1.12					06/06/2018
  *			Change the cancel button from a hyperlink to a button.
  *		Version 1.13					07/16/2018
  *			Move the label to the left of the field.
  *		Version 1.14					11/21/2018
  *			Added Capital BlueCross to the list of health plans.
  *
  */
--%>

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui"%>
<%@ taglib uri="http://liferay.com/tld/portlet" prefix="liferay-portlet"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<%@page import="com.liferay.portal.kernel.service.UserGroupLocalServiceUtil"%>
<%@page import="com.liferay.portal.kernel.model.User"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="theme" %>

<portlet:defineObjects />
<theme:defineObjects />

<portlet:resourceURL var="memberInfoSearchformURL" id="memberInfoSearchform">
</portlet:resourceURL>

<portlet:resourceURL var="claimSearchformURL" id="claimSearchform">
</portlet:resourceURL>
<portlet:resourceURL var="memberClaimSearchformURL" id="memberClaimSearchform">
</portlet:resourceURL>

<portlet:actionURL var="cancelURLByDetailsTagURL">
	<portlet:param name="cancelaction" value="cancelPage" />
</portlet:actionURL>

<script>
	<%
		int maxCardIdLength = 17;

		String MEMBER_ID_ERROR = "Member ID is not valid until a health plan is selected.";
		String CLAIM_ID_ERROR = "Claim ID is not valid until a health plan is selected.";

		String DATE_RANGE_ERROR = "Service From Date must be before or equal to the Service To Date.";

		String healthPlanIdSouthCarolina = "0010";
		String healthPlanIdNorthCarolina = "0020";
		String healthPlanIdCapitalBlueCross = "0030";
	%>
</script>

<aui:script>
	var toolTip = "";
	var browserStr = getBrowser();

	$(function() {
        var msg ='<c:out value="${message}"/>'.replace(/\n|\r/g, "");
		var idx = $("#<portlet:namespace/>claimMemberKey").val();

        if ((msg == "<%= com.avalon.claimsearch.fo.ClaimsSearchConstants.CLAIMS_ERROR %>") ||
        	(msg == "<%= com.avalon.claimsearch.fo.ClaimsSearchConstants.ERROR_WHILE_PROCESSING %>") ||
        	(idx != "null")) {

			showClaimSearch(idx);
        }

        // Disable the member ID and claim ID radio buttons is a health plan is not selected
		var memberInfoHealthPlanIdx = $("#<portlet:namespace/>memberInfoHealthPlan").val();
		if (memberInfoHealthPlanIdx == "") {
			$("#radioselectmemberid").attr("disabled", "disabled");
			$("#radioselectclaimid").attr("disabled", "disabled");
			
			// Add a tooltip when both buttons are disabled
			$("#radioselectmemberid").prop("title", "<%= MEMBER_ID_ERROR %>");
			$("#radioselectclaimid").prop("title", "<%= CLAIM_ID_ERROR %>");
        }

		// Execute the search when the enter key is pressed for the claim ID
		processEnterKey("claimID", false);

		// Execute the search when the enter key is pressed for the member ID
		processEnterKey("memberID", false);
		
		// Execute the search when the enter key is pressed for the from date of sercice
		processEnterKey("dateofServiceFrom", true);
		
		// Execute the search when the enter key is pressed for the to date of sercice
		processEnterKey("dateofServiceTo", true);
	});

	// Disable the enter key so it does not submit the form
	$('html').bind('keypress', function(e) {
		if(e.keyCode == 13) {
			return false;
		}
	});

	function getNamespace() {
		return('<portlet:namespace/>');
	}

	YUI().ready('aui-node','event',
		function(Y) {
			if(Y.one('#heading span')){	
				var portletTitle = Y.one('#heading span').getHTML();
				Y.one('#heading span').setHTML('Claim Status');
			}		
	 	}
	 );

	// Cause the AUI validation to fire
	function checkItem(checkId) {

		// Save the current active element
		var activeId = document.activeElement.id;

		// Set the focus then leave each field to display any error messages
		$("#<portlet:namespace/>" + checkId).focus();
		$("#<portlet:namespace/>" + checkId).blur();
		
		// Set focus to the saved field
		$("#" + activeId).focus();
	}

	// Clear the item (the field must be visible to clear any error)
	function clearItem(id, divId) {
		var isVisible = $("#" + divId).is(":visible");

		$("#" + divId).show();
		$("#<portlet:namespace/>" + id).val("1");
		checkItem(id);
		$("#<portlet:namespace/>" + id).val("");
		if (!isVisible) {
			$("#" + divId).hide();
		}
	}

	// Get the check information
	function validateCheckInformation(id, chkNumber, chkIssueDate) {
		
		if (chkNumber == "") {
			
			// Get the check number
			getMemberSearchClaimResult(id);
		} else {
			showCheckInformation(id, chkNumber, chkIssueDate);
		}
	}

	// Display any check information
	function showCheckInformation(id, chkNumber, chkIssueDate) {
		var msg = "Claim Number: " + id + "<br>Check Number: " + chkNumber + "<br>Check Issued Date: " + chkIssueDate;

		$("#checkDialog").dialog({
			modal: true,
			resizable: false,
			width: 450,
			buttons: {
				"Close": function() {
					$(this).dialog("close");
				}
			}
		});
		$("#checkDialog").html(msg);
		$("#checkDialog").show();
		
		if (browserStr == "Safari") {
			if (!$(".ui-dialog-titlebar-close").hasClass("ui-icon ui-icon-closethick")) {
				$(".ui-dialog-titlebar-close").addClass("ui-icon ui-icon-closethick");
			}
		}

		// The close icon on the popup is not displaying in all environments.  It will be hidden until a solution is found.
		$(".ui-dialog-titlebar-close").css("visibility", "hidden") 
	}

	var fromDateFlag = false;
	var toDateFlag = false;
 
	function closePortlet() {
	    var url='<%=cancelURLByDetailsTagURL.toString()%>';
	    window.location.href = url;
	}
 
	function isValidForm(validateFlag) {
		var formValid = false;
		
		// Initialize the tool tip for the search button
		toolTip = "";
		
		if ($("#mainSearch").css("display") == 'block') {
			var healthPlanIdValid = false;
			var idCardValid = false;
			var claimIdValid = false; 
			
			if (validateFlag) {
				
				// Validate the health plan id
				checkItem('memberInfoHealthPlan'); 
			}

			var memberInfoHealthPlan = $("#<portlet:namespace/>memberInfoHealthPlan").val();
			if (memberInfoHealthPlan != "") {
				healthPlanIdValid = true;
			}

			if ($("#memberIdSearch").css("display") == 'block') {

				if (validateFlag) {
					
					// Validate the ID card number
					checkItem("memberID"); 
				}
				
				var idCardNumber = $("#<portlet:namespace/>memberID").val().trim();
				var exp = /^([a-zA-Z0-9]+)$/;
				
				if ((idCardNumber != null) && idCardNumber.match(exp)) {
					idCardValid = true;
				} else {
					toolTip += "The ID Card Number is not valid";
				}
				
				$("#mainSearchButtons").show();
				formValid = healthPlanIdValid && idCardValid;
			}
			
			if ($("#claimIdSearch").css("display") == 'block') {
				
				if (validateFlag) {
					
					// Validate the claimID
					checkItem("claimID"); 
				}
				
				var claimId = $("#<portlet:namespace/>claimID").val().trim();
				var exp = /^([a-zA-Z0-9]+)$/;
				
				if ((claimId != null) && (claimId.length > 0) && claimId.match(exp)) {
					claimIdValid = true;
				} else {
					toolTip += "The Claim ID is not valid"
				}
				
				$("#mainSearchButtons").show();
				formValid = healthPlanIdValid && claimIdValid;
			}
		}

		if ($("#claimInfoSearch").css("display") == "block") {
			if ($("#searchByDate").css("display") == "block") {
				if (validateFlag) {
					
					// Validate the date fields
					checkItem("dateofServiceFrom"); 
					checkItem("dateofServiceTo"); 
				}

				var dateofServiceFrom = $("#<portlet:namespace/>dateofServiceFrom").val();
				var dateofServiceTo = $("#<portlet:namespace/>dateofServiceTo").val();

				var dateofServiceFromFlag = isValidDate(dateofServiceFrom, true);
				var dateofServiceToFlag = isValidDate(dateofServiceTo, true);
					
				// For IE, change date format to yyyy-mm-dd for comparison
				if ((browserStr == "IE") || (browserStr == "Firefox") || (browserStr == "unknown")) {
					if (dateofServiceFrom != "") {
						dateofServiceFrom = convertDate(dateofServiceFrom);
					}
					if (dateofServiceTo != "") {
						dateofServiceTo = convertDate(dateofServiceTo);
					}
				}

				if (dateofServiceFromFlag && dateofServiceToFlag && (dateofServiceFrom <= dateofServiceTo)) {
					formValid = true;
				} else {
					errorFlag = false;

					// Test the service from date
					if (!dateofServiceFromFlag) {
						errorFlag = true;
						toolTip += "The Service From Date is not valid";
					}
					
					// Test the service to date
					if (!dateofServiceToFlag) {
						errorFlag = true;
						if (toolTip.length != 0) {
							toolTip += "\n";
						}
						toolTip += "The Service To Date is not valid";
					}
					
					// Test if from date is erarlier than to date
					if (!errorFlag && (dateofServiceFrom >= dateofServiceTo)) {
						toolTip += "<%= DATE_RANGE_ERROR %>";
					}
				}
			}
				
			if ($('#searchById').css('display') == 'block') { 
				
				if (validateFlag) {
					
					// Validate the claim ID field
					checkItem("memberClaimID"); 
				}

				var claimId = $("#<portlet:namespace/>memberClaimID").val().trim();
				var exp = /^([a-zA-Z0-9]+)$/;

				if ((claimId != null) && (claimId.length > 0) && claimId.match(exp)) {
					formValid = true;
				} else {
					toolTip += "The Claim ID is not valid";
				}
			}
		}

		return formValid;
	}
	
	function setSearchButtonStatus() {

		// Set the search button disable status based on the form data.
	    if (isValidForm(false)) {
			$("#<portlet:namespace/>submitSearch").removeAttr("disabled");
		} else {
			$("#<portlet:namespace/>submitSearch").attr("disabled", "disabled");
		}
		
		// Add the mouse over text for the search button
		$("#<portlet:namespace/>submitSearch").prop("title", toolTip);
	}
	
	function setFindClaimButtonStatus() {
		
		// Set the find claim button disable status based on the form data.
	    if (isValidForm(false)) {
			$("#<portlet:namespace/>submitMemberSearch").removeAttr("disabled");
		} else {
			$("#<portlet:namespace/>submitMemberSearch").attr("disabled", "disabled");
		}
		
		// Add the mouse over text for the search button
		$("#<portlet:namespace/>submitMemberSearch").prop("title", toolTip);
	}

	// Set the date field based on the offset
	function setDateWithOffset(_id, diff) {
		var todayWithOffset = new Date();

		todayWithOffset.setDate(todayWithOffset.getDate() + diff);

		if ((browserStr == "unknown") || (browserStr == "IE") || (browserStr == "Firefox")) {
			$("#" + _id).datepicker("setDate", todayWithOffset);
		} else {
			var dat = todayWithOffset.toISOString().substring(0, 10);
			
			$("#" + _id).val(dat);
		}
	}

	// Set the date field
	function setDate(_id, newDate) {

		if ((browserStr == "unknown") || (browserStr == "IE") || (browserStr == "Firefox")) {
			$("#" + _id).datepicker("setDate", newDate);
		} else {
			var dat = newDate.toISOString().substring(0, 10);

			$("#" + _id).val(dat);
		}
	}

	// Set the date field limit based on the offset
	function setDateLimitWithOffset(_id, diff, minFlag) {
		var todayWithOffset = new Date();
		todayWithOffset.setDate(todayWithOffset.getDate() + diff);
		
		if ((browserStr == "unknown") || (browserStr == "IE") || (browserStr == "Firefox")) {
			if (minFlag) {
				$("#" + _id).datepicker("option", "minDate", todayWithOffset);
			} else {
				$("#" + _id).datepicker("option", "maxDate", todayWithOffset);
			}
		} else {
			var dat = todayWithOffset.toISOString().substring(0, 10);

			if (minFlag) {
				$("#" + _id).attr("min", dat);
			} else {
				$("#" + _id).attr("max", dat);
			}
		}
	}

	// Set the date field limit
	function setDateLimit(_id, newDate, minFlag) {

		if ((browserStr == "unknown") || (browserStr == "IE") || (browserStr == "Firefox")) {
			if (minFlag) {
				$("#" + _id).datepicker("option", "minDate", newDate);
			} else {
				$("#" + _id).datepicker("option", "maxDate", newDate);
			}
		} else {
			var dat = newDate.toISOString().substring(0, 10);
	
			if (minFlag) {
				$("#" + _id).attr("min", dat);
			} else {
				$("#" + _id).attr("max", dat);
			}
		}
	}

	function addMonthsUTC (date, count) {
		if (date && count) {
			var m, d = (date = new Date(+date)).getUTCDate()

			date.setUTCMonth(date.getUTCMonth() + count, 1)
			m = date.getUTCMonth()
			date.setUTCDate(d)
			if (date.getUTCMonth() !== m) {
				date.setUTCDate(0)
			}
		}
		return date
	}
	
	function checkDateSpan() {
		
		// Convert both date strings to dates
		var fromDate = new Date($("#<portlet:namespace/>dateofServiceFrom").val());
		var toDate = new Date($("#<portlet:namespace/>dateofServiceTo").val());

		// Add six months to the from date
		fromDate = addMonthsUTC(fromDate, 6);
		
		// Is the to date more that 6 months past the from date?
		if (toDate >= fromDate) {

			// Set the to date to 6 month after the from date
			setDate('<portlet:namespace/>dateofServiceTo', fromDate);
			setDateLimit('<portlet:namespace/>dateofServiceTo', fromDate, false);
		}
	}
	
	function showClaimSearch(idx) {

		// Only process for a valid index
		if (idx != "null") {
			
			// Display the search page
			$("#claimInfoSearch").show();
			$("#mainSearch").hide();
		
			// Save the index of the item selected
			$("#<portlet:namespace/>claimMemberKey").val(idx);
	
			if ((browserStr == "unknown") || (browserStr == "IE") || (browserStr == "Firefox")) {
			
				// Show date picker for IE
				$("#<portlet:namespace/>dateofServiceFrom").datepicker({
					dateFormat: 'mm/dd/yy',
					changeMonth: true,
					changeYear: true,
					onSelect: function(selected) {
					}
				});
				$("#<portlet:namespace/>dateofServiceFrom").attr("placeholder", "mm/dd/yyyy");
				$("#<portlet:namespace/>dateofServiceFrom").css({
					"background" : "url('<%=request.getContextPath()%>/images/icondatepicker.png') no-repeat",
					"background-position-x" : "100%",
					"background-position-y" : "50%"
				});
				
				$("#<portlet:namespace/>dateofServiceTo").datepicker({
					dateFormat: 'mm/dd/yy',
					changeMonth: true,
					changeYear: true,
					onSelect: function(selected) {
					}
				});
				$("#<portlet:namespace/>dateofServiceTo").attr("placeholder", "mm/dd/yyyy");
				$("#<portlet:namespace/>dateofServiceTo").css({
					"background" : "url('<%=request.getContextPath()%>/images/icondatepicker.png') no-repeat",
					"background-position-x" : "100%",
					"background-position-y" : "50%"
				});
			}
			
			// Set the search by radio button
			var hiddenFromDate = $("#<portlet:namespace/>hidden_dateofServiceFrom").val();
			var hiddenToDate = $("#<portlet:namespace/>hidden_dateofServiceTo").val();
			var hiddenClaimId = $("#<portlet:namespace/>hidden_claimId").val();
			if (hiddenFromDate == "") {

				// Since the from date is not blank the search is by date
				$('#radioselectclaim').prop("checked", "checked");
				showSearchScreen('id');
				
				// Set the claim id
				if (hiddenClaimId == "null") {
					$("#<portlet:namespace/>memberClaimID").val("");
				} else {
					$("#<portlet:namespace/>memberClaimID").val(hiddenClaimId);
				}
			} else {
				
				// Since the from date is blank the search is by claim id
				$('#radioselectdate').prop("checked", "checked");
				showSearchScreen('date');
				
				// Set the from date
				if (hiddenFromDate == "null") {

					// Set from date to 60 days earlier than today
					setDateWithOffset('<portlet:namespace/>dateofServiceFrom', -60);
				} else {
					
					// Set the from date to the date that was used
					setDate('<portlet:namespace/>dateofServiceFrom', hiddenFromDate);
				}
				
				// Set the to date
				if (hiddenFromDate == "null") {

					// Set to date to today
					setDateWithOffset('<portlet:namespace/>dateofServiceTo', 0);
				} else {
					
					// Set the to date to the date that was used
					setDate('<portlet:namespace/>dateofServiceTo', hiddenToDate);
				}
				
				// Set the maximum allowable dates to today
				setDateLimitWithOffset('<portlet:namespace/>dateofServiceFrom', 0, false);
				setDateLimitWithOffset('<portlet:namespace/>dateofServiceTo', 0, false);
			}
		}
	}
	
	function clearSearchValues() {
		var dateFlag = $('#radioselectdate').is(":checked");
		
		if (dateFlag) {
			$("#<portlet:namespace/>dateofServiceFrom").val("");
			$("#<portlet:namespace/>dateofServiceTo").val("");
		} else {
			$("#<portlet:namespace/>memberClaimID").val("");
		}
	}
	
	function getSearchResult() {

		if ($("#memberIdSearch").css("display") == 'block') {
			
			// Display the member search results
			getMemberSearchResult();
			
			// Clear the claim ID
			clearItem("claimID", "claimIdSearch");
		} else if ($("#claimIdSearch").css("display") == 'block') {

			// Display the claim search results
			getClaimSearchResult("memberSearchDetails");
			
			// Clear the member ID
			clearItem("memberID", "memberIdSearch");
		}
	}
	
	function getMemberSearchResult() {
		console.log('inside getMemberSearchResult');
		$("#memSearchResults").hide();
		$("#errorMessage").hide();
	
		if (isValidForm(true)) {
			$("#dataloader").show();
	
			$.ajax({
				type : "POST",
				data : $('#<portlet:namespace/>memberSearchDetails').serialize(),
				dataType : "json",
				url : "${memberInfoSearchformURL}",
				
				success : function(memberInfo) {
					console.log(memberInfo);
					var memedata = JSON.stringify(memberInfo);
					console.log(memedata);
					var isError;
					for (var i = 0; i < memberInfo.length; i++) { 
					     isError = memberInfo[i].error;
					}
				
					$("#dataloader").hide();
					if( (isError != null && isError != "" && isError != "undefined")  ){
						console.log('inside error');
						$("#errorMessage").show();
						$("#errorText").text(isError);
					} else {
						console.log('inside success');
						$("#memSearchResults").show();
						$("#memResults").dataTable({
							"bDestroy" : true,
							"sPaginationType" : "full_numbers",
							"bProcessing" : true,
							"aaData" : jQuery.parseJSON(memedata),
							"aoColumns" : [ {
								"mData" : "memberID"
							}, {
								"mData" : "groupID"
							}, {
								"mData" : "memberFirstName"
							}, {
								"mData" : "memberLastName"
							}, {
								"mData" : "dateofBirth"
							},{
								"mData" : "gender"
							},{
								"mData" : "findClaims"
							}
		
							],
							"aoColumnDefs" : [
								{
									"aTargets" : [6],
									"bSortable" : false,
									/* "visible": false,
					                   "searchable": false, */
									"mRender" : function (data, type, row, meta) {
										return '<a class="memCls" href="javascript:showClaimSearch(' + meta.row + ')">Find Claims</a>';
					                }
								}
							]
						}); 
					}
					$(".memCls").click(
						function() {
							$("#mainSearch").hide();
							$("#memSearchResults").hide();
						}
					);
				}
			});
		}
	}

	function getMemberSearchClaimResult(id) {
		var claimId = "";
		var checkNumber = "";
		var checkIssueDate = "";

		$("#dataloader").show();

		$.ajax({
			type : "POST",
			data : {
				claimId: id
			},
			dataType : "json",
			url : "${memberClaimSearchformURL}",
	
			success : function(claimInfo) {
				var claimdata = JSON.stringify(claimInfo);
		
				var isError;
				for (var i = 0; i < claimInfo.length; i++) { 
				     isError = claimInfo[i].error;
				}

				$("#dataloader").hide();
				if( (isError != null && isError != "" && isError != "undefined")  ){
					$("#errorMessage").show();
					$("#errorText").text(isError);
				} else {
					if (claimInfo.length != 1) {
						isError = "Incorrect number of claims were returned."
						$("#errorMessage").show();
						$("#errorText").text(isError);
					} else {
						claimId = claimInfo[0].claimId;
						checkNumber = claimInfo[0].checkNumber;
						checkIssueDate = claimInfo[0].checkIssueDate;

						showCheckInformation(claimId, checkNumber, checkIssueDate);
					}
				}
			}
		});
	}
	
	function getClaimSearchResult(dataId) {
		$("#claimSearchResults").hide();
		$("#errorMessage").hide();

		if (isValidForm(true)) {

			if (dataId == "claimSearchDetails") {
				if ($('#radioselectdate').is(":checked")) {
					$("#<portlet:namespace/>memberClaimID").val("");
				} else {
					$("#<portlet:namespace/>dateofServiceFrom").val("");
					$("#<portlet:namespace/>dateofServiceTo").val("");
				}
			}

			$("#dataloader").show();

			$.ajax({
				type : "POST",
				data : $('#<portlet:namespace/>' + dataId).serialize(),
				dataType : "json",
				url : "${claimSearchformURL}",

				success : function(claimInfo) {
					var claimdata = JSON.stringify(claimInfo);
			
					var isError;
					for (var i = 0; i < claimInfo.length; i++) { 
					     isError = claimInfo[i].error;
					}

					$("#dataloader").hide();
					if( (isError != null && isError != "" && isError != "undefined")  ){
						$("#errorMessage").show();
						$("#errorText").text(isError);
					} else {
						$("#claimSearchResults").show();
						
						$("#claimResults").dataTable({
							"bDestroy" : true,
							"sPaginationType" : "full_numbers",
							"bProcessing" : true,
							"aaData" : jQuery.parseJSON(claimdata),
							"aoColumns" : [ {
									"mData" : "claimId"
								}, {
									"mData" : "memberId"
								}, {
									"mData" : "providerid"
								}, {
									"mData" : "dateofServiceFrom"
								}, {
									"mData" : "dateofServiceTo"
								}, {
									"mData" : "claimStatus"
								}, {
									"mData" : "totalChargeGreaterThan"
								}, {
									"mData" : "checkIssueDate"
								}, {
									"mData" : "checkNumber"
								},{
									"mData" : "viewCheck"
							}],
							"aoColumnDefs" : [
				                {
				                	"aTargets" : [7],
				                	"visible" : false
				                },
				                {
				                	"aTargets" : [8],
				                	"visible" : false
				                },
								{
									"aTargets" : [9],
									"bSortable" : false,
									"mRender" : function (data, type, row, meta) {
										if (row.claimStatus == "Settled") {
											return '<a class="claimCls" href="javascript:validateCheckInformation(\'' + row.claimId + '\', \'' + row.checkNumber + '\', \'' + row.checkIssueDate + '\')">View Check Information</a>';
										} else {
											return "";
										}
					                }
								}
							]
						});
					}
					
					// Hide the link column if there are not Settled claims
					if ($(".claimCls").length == 0) {
						$('#claimResults').DataTable().column(9).visible(false);
					}
				}
			});
		}
	}
	
	function setMemberIdStatus() {
		var memberInfoHealthPlanIdx = $("#<portlet:namespace/>memberInfoHealthPlan").val();

		// Remove any tooltip
		$("#radioselectmemberid").prop("title", "");
		$("#radioselectclaimid").prop("title", "");
		
		if (memberInfoHealthPlanIdx == "") {
			$("#radioselectmemberid").attr("disabled", "disabled");
			$("#radioselectclaimid").attr("disabled", "disabled");
			
			// Add a tooltip when both buttons are disabled
			$("#radioselectmemberid").prop("title", "<%= MEMBER_ID_ERROR %>");
			$("#radioselectclaimid").prop("title", "<%= CLAIM_ID_ERROR %>");
		} else {
			$("#radioselectclaimid").removeAttr("disabled");

			// Disable selection of the member ID radio button if BCBS-North Carolina is selected
			if (memberInfoHealthPlanIdx == "<%= healthPlanIdNorthCarolina %>") {
				
				// Hide the member search field, the results, and error messages
				$("#memberIdSearch").hide();
				$("#memSearchResults").hide();
				$("#errorMessage").hide();
				
				// Clear member radio button
				$("#radioselectmemberid").prop("checked", false);
	
				$("#radioselectmemberid").attr("disabled", "disabled");
				
				// Add a tooltip for North Carolina
				$("#radioselectmemberid").prop("title", "Member ID is not valid for the health plan selected.");
			} else {
				$("#radioselectmemberid").removeAttr("disabled");
				
				// Remove any title
				$("#radioselectmemberid").prop("title", "");
			}
		}

		// Clear both id fields and any error message
		clearItem("memberID", "memberIdSearch");
		clearItem("claimID", "claimIdSearch");
	}
	
	function clearInputField() {		
		// Hide the error message
		$("#errorMessage").hide();

		var id = "";
		
		if ($("#memberIdSearch").css("display") == 'block') {
			
			// Clear the member ID field
			id = "<portlet:namespace/>memberID";
		} else if ($("#claimIdSearch").css("display") == 'block') {

			// Clear the claim ID field
			id = "<portlet:namespace/>claimID";
		}
		$("#" + id).val("");
	}
	
	function processEnterKey(id, dateFlag) {
		document.getElementById(getNamespace() + id).addEventListener("keyup", function(event) {
			event.preventDefault();
			if (event.keyCode === 13) {
				if (dateFlag) {
					getClaimSearchResult("claimSearchDetails");
				} else {
					getSearchResult();
				}
			}
		});
	}
</aui:script>

<div id="busy_indicator" style="display: none">
	<img src="<%=request.getContextPath()%>/images/busy-spinner.gif" alt="Busy indicator">
</div>

<c:if test="${fn:length(list) == 0 && fn:length(claimList) > 0}">
	<% // memberDisplay = "none"; %>
</c:if>

<c:if test="${fn:length(claimList) > 0}">
	<% // claimDisplay = "inline"; %>
</c:if>
<c:if test="${fn:length(claimList) == 0}">
	<% // claimDisplay = "none"; %>
</c:if>

<div id="mainSearch" >
	<liferay-ui:panel title="Claim Search" collapsible="false">
		<aui:form name="memberSearchDetails" commandname="claimSearchFO" onChange="setSearchButtonStatus()">
			<aui:fieldset>
				<aui:container>
					<aui:row>
						<aui:col span="6">
							<aui:select inlineLabel="true" name="memberInfoHealthPlan" label="tce.label.health.plan" cssClass="span7" required="true" showRequiredLabel="true" onChange="setMemberIdStatus()">
								<aui:option value="">Make a Selection</aui:option>
								<aui:option value="<%= healthPlanIdCapitalBlueCross %>">Capital BlueCross</aui:option>
								<aui:option value="<%= healthPlanIdNorthCarolina %>">Blue Cross and Blue Shield of North Carolina</aui:option>
								<aui:option value="<%= healthPlanIdSouthCarolina %>">BlueCross BlueShield of South Carolina</aui:option>
							</aui:select>
						</aui:col>
					</aui:row>
					<aui:row>
						<aui:col span="6">
							<div>
								Search By:*
								<input style="width: 50px; margin-top: -1px;" type="radio" name="radioselect" id="radioselectmemberid" value="memberId" onClick="showSearchScreen(this.value)" >Member ID
								<input style="width: 50px; margin-top: -1px;" type="radio" name="radioselect" id="radioselectclaimid" value="claimId" onClick="showSearchScreen(this.value)" >Claim ID
							</div>
    					</aui:col>
					</aui:row>
				</aui:container>
			</aui:fieldset>

			<div id="memberIdSearch" style="display: none" >
				<aui:fieldset label="Search By Member ID">
					<aui:container>
						<aui:row>
							<aui:col span="6">
								<aui:input type="text" inlineLabel="true" name="memberID" id="memberID" class="form-control" maxlength="<%= maxCardIdLength %>" label="ID Card Number:" value='${claimSearchFO.memberID}' >
						  			<aui:validator name="required" />
									<aui:validator name="alphanum" />
								</aui:input>
							</aui:col>
						</aui:row>
					</aui:container>
				</aui:fieldset>
			</div>

			<div id="claimIdSearch" style="display: none" >
				<aui:fieldset label="Search By Claim ID">
					<aui:container>
						<aui:row>
							<aui:col span="6">
       							<aui:input type="text" inlineLabel="true" name="claimID" id="claimID" class="form-control" label="Claim ID:" value='${claimSearchFO.claimID}' >
 									<aui:validator name="required" />
									<aui:validator name="alphanum" />
								</aui:input>
							</aui:col>
						</aui:row>
						<aui:row>
							<aui:col span="12">
								<span id="claimIDMessage" style="font-weight:bold" ></span>
							</aui:col>
						</aui:row>
					</aui:container>
				</aui:fieldset>
			</div>
			
			<aui:fieldset>
				<aui:button-row cssClass="btn-divider">
					<div id="mainSearchButtons" style="display: none" >
						<div onmouseenter="setSearchButtonStatus()">
							<aui:button name="submitSearch" cssClass="pull-right" type="button" primary="true" value="Search" onClick="getSearchResult()" />
						</div>
						<aui:button name="clearSearch" cssClass="pull-right" type="button" value="Clear" onClick="clearInputField()" />
					</div>
	      			<aui:button name="cancelSearch" cssClass="btn-gray pull-left" type="button" value="Cancel" onclick="closePortlet()" />
				</aui:button-row>
			</aui:fieldset>
		</aui:form>
	</liferay-ui:panel>
</div>

<div id="memSearchResults" style="display: none" >
	<liferay-ui:panel title="Results" collapsible="false">
		<table id="memResults" class="display" style="border-collapse: collapse; width: 100%" >
			<thead>
				<tr>
					<th>ID Card Number</th>
					<th>Group ID</th>
					<th>First Name</th>
					<th>Last Name</th>
					<th>Date of Birth</th>
					<th>Gender</th>
					<th class="hidedisplayheader"></th>
				</tr>
			</thead>
			<tbody>
	
			</tbody>
		</table>
	</liferay-ui:panel>
</div>

<div id="claimInfoSearch" style="display: none" >
	<liferay-ui:panel title="Claims Search" collapsible="false" >
		<aui:form name="claimSearchDetails" commandname="claimSearchFO" onChange="setFindClaimButtonStatus()">
			<aui:fieldset>
				<aui:container>
					<input type="hidden" id="<portlet:namespace/>claimMemberKey" name="<portlet:namespace/>claimMemberKey" value="<%=request.getAttribute("claimMemberKeyVal")%>" />
					<input type="hidden" id="<portlet:namespace/>hidden_dateofServiceFrom" name="<portlet:namespace/>hidden_dateofServiceFrom" value="<%=request.getAttribute("memberFromDate")%>" />
					<input type="hidden" id="<portlet:namespace/>hidden_dateofServiceTo" name="<portlet:namespace/>hidden_dateofServiceTo" value="<%=request.getAttribute("memberToDate")%>" />
					<input type="hidden" id="<portlet:namespace/>hidden_claimId" name="<portlet:namespace/>hidden_claimId" value="${claimSearchFO.memberClaimID}" />
					<aui:row>
						<aui:col span="12">
							<div class="middle" style="text-align: center; width: 100%;">
								Search By:
								<input style="width: 50px; margin-top: -1px;" type="radio" name="radioselect" id="radioselectdate" value="dates" onClick="showSearchScreen('date')">Service Dates
								<input style="width: 50px; margin-top: -1px;" type="radio" name="radioselect" id="radioselectclaim" value="claim" onClick="showSearchScreen('id')" >Claim ID
							</div>
    					</aui:col>
					</aui:row>
				</aui:container>
			</aui:fieldset>
			<div id="searchByDate" style="display: block">
				<aui:fieldset label="Search by Service Date">
					<aui:container>
						<aui:row>
							<aui:col span="1">
							</aui:col>
 							<aui:col span="5">
								<aui:input type="date" inlineLabel="true" name="dateofServiceFrom" id="dateofServiceFrom" label="Service From Date:" value='<%= request.getAttribute("memberFromDate") %>' >
									<aui:validator name="required"/>
							        <aui:validator name="custom" errorMessage="<%= DATE_RANGE_ERROR %>">
									    function (val, fieldNode, ruleValue) {
									    	var retValue = false;
											var serviceToDate = $("#<portlet:namespace />dateofServiceTo").val();
									    	
											// For IE, change date format to yyyy-mm-dd
											if ((browserStr == "IE") || (browserStr == "Firefox") || (browserStr == "unknown")) {
												if (val != "") {
													val = convertDate(val);
												}

												if (serviceToDate != "") {
													serviceToDate = convertDate(serviceToDate);
												}
											}

									    	if (val.length == 0) {
									    		retValue = true;
									    	} else {
												if (serviceToDate.length == 0) {
													retValue = true;
												} else {
													retValue = (val <= serviceToDate);
												}
											}

									        return retValue;
									    }
									</aui:validator>
							        <aui:validator name="custom" errorMessage="Please enter a valid date (format mm/dd/yyyy).">
									    function (val, fieldNode, ruleValue) {
									    	var retValue = false;
									    	
									    	retValue = isValidDate(val, false);
									    	
									    	return retValue;
								    	}
									</aui:validator>
								</aui:input>
							</aui:col>
							<aui:col span="5">
								<aui:input type="date" inlineLabel="true" name="dateofServiceTo" id="dateofServiceTo" label="Service To Date:" value='<%= request.getAttribute("memberToDate") %>' >
									<aui:validator name="required"/>
							        <aui:validator name="custom" errorMessage="Service To Date must be after or equal to the Service From Date.">
									    function (val, fieldNode, ruleValue) {
									    	var retValue = false;
											var serviceFromDate = $("#<portlet:namespace />dateofServiceFrom").val();
									    	
											// For IE, change date format to yyyy-mm-dd
											if ((browserStr == "IE") || (browserStr == "Firefox") || (browserStr == "unknown")) {
												if (val != "") {
													val = convertDate(val);
												}

												if (serviceFromDate != "") {
													serviceFromDate = convertDate(serviceFromDate);
												}
											}

									    	if (val.length == 0) {
									    		retValue = true;
									    	} else {
												if (serviceFromDate.length == 0) {
													retValue = true;
												} else {
													retValue = (val >= serviceFromDate);
												}
											}
											
									        return retValue;
									    }
									</aui:validator>
							        <aui:validator name="custom" errorMessage="Please enter a valid date (format mm/dd/yyyy).">
									    function (val, fieldNode, ruleValue) {
									    	var retValue = false;
									    	
									    	retValue = isValidDate(val, false);
									    	
									    	return retValue;
								    	}
									</aui:validator>
								</aui:input>
							</aui:col>
							<aui:col span="1">
							</aui:col>
						</aui:row>
					</aui:container>
				</aui:fieldset>
			</div>
			<div id="searchById" style="display: none">
				<aui:fieldset label="Search by Claim ID">
					<aui:container>
						<aui:row>
							<aui:col span="3">
							</aui:col>
							<aui:col span="6">
       							<aui:input type="text" inlineLabel="true" name="memberClaimID" id="memberClaimID" label="Claim ID:" value='${claimSearchFO.memberClaimID}' >
 									<aui:validator name="required" />
									<aui:validator name="alphanum" />
								</aui:input>
							</aui:col>
							<aui:col span="3">
							</aui:col>
						</aui:row>
						<aui:row>
							<aui:col span="3">
							</aui:col>
							<aui:col span="9">
								<span id="memberClaimIDMessage" style="font-weight:bold" ></span>
							</aui:col>
						</aui:row>
					</aui:container>
				</aui:fieldset>
			</div>
			<aui:fieldset>
				<aui:button-row cssClass="btn-divider">
					<aui:button name="submitMemberSearch" cssClass="pull-right" type="button" primary="true" value="Find Claim" onClick="getClaimSearchResult('claimSearchDetails')" />
					<aui:button name="clearClaimSearch" cssClass="pull-right" type="button" value="Clear" onClick="clearSearchValues()" />
	      			<aui:button name="cancelClaimSearchInfo" cssClass="btn-gray pull-left" type="button" value="Cancel" onclick="closePortlet()" />
				</aui:button-row>
			</aui:fieldset>
		</aui:form>
	</liferay-ui:panel>
</div>

<div id="claimSearchResults" style="display: none" >
	<liferay-ui:panel title="Results" collapsible="false">
		<table id="claimResults" class="display" style="border-collapse: collapse; width: 100%" >
			<thead>
				<tr>
					<th>Claim ID</th>
					<th>ID Card Number</th>
					<th>Provider ID</th>
					<th>From Date of Service</th>
					<th>To Date of Service</th>
					<th>Claim Status</th>
					<th>Billed Charges</th>
					<th>Check Issue Date</th>
					<th>Check Number</th>
					<th class="hidedisplayheader"></th>
				</tr>
			</thead>
			<tbody>
	
			</tbody>
		</table>
	</liferay-ui:panel>
</div>

<div id="errorMessage" style="display: none; text-align: left; color: red" >
	<span id="errorText" > </span>
</div>

<div id="dataloader" class="blink_text" style="display: none" >  
     Please wait while loading the page...
</div>

<div class="dialog-service" title="Member Service" style="text-align: left; color: red;">
	${message}
</div>
<div class="yui3-skin-sam">
	<div id="modal"></div>
</div>

<div id="checkDialog" title="Check Information" style="display: none">
</div>
