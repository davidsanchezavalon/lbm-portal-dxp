/**
 * Description
 *		This file contain the javascript for Claim Status.
 * CHANGE History
 * 		Version 1.0  
 *			Moved from Prior Auth.  
 *		Version 1.1
 *			Added a message about how to get the claim number.
 *
 */

var exitString = 'You have made updates to this page that have not been saved.  If you continue the updates will be lost.';

makeFormResponsive();
formatRequiredLabels('*');

function makeFormResponsive() {
	var groups = document.getElementById('main-content').getElementsByClassName('control-group');
	
	for (var i = 0, n = groups.length; i < n; i++ ) {
		groups[i].setAttribute('class', groups[i].getAttribute('class') + ' row-fluid');
		
		var label = groups[i].getElementsByTagName('label')[0];
		
		if (label) {
			label.setAttribute('class', label.getAttribute('class') + ' span5');
		}
		
		var input = groups[i].getElementsByTagName('input')[0];
		
		if (input) {
			input.setAttribute('class', input.getAttribute('class') + ' span7');
		}
	}
}

function formatRequiredLabels(text) {
	var reqLabels = document.getElementById('main-content').getElementsByClassName('label-required');
	
	for ( var i = 0, n = reqLabels.length; i < n; i++ ) {
		reqLabels[i].innerHTML = text;
	}
}

function getBrowser() {
	var rtnValue = "unknown";
    
	if ((navigator.userAgent.indexOf("Opera") || navigator.userAgent.indexOf('OPR')) != -1 ) {
		rtnValue = "Opera";
	} if (navigator.userAgent.indexOf("Chrome") != -1 ) {
		rtnValue = "Chrome";
	} if (navigator.userAgent.indexOf("Safari") != -1) {
		rtnValue = "Safari";
	} else if ((navigator.userAgent.indexOf("MSIE") != -1 ) || (!!document.documentMode == true )) {  //IF IE > 10
		rtnValue = "IE";
	} else if (navigator.userAgent.indexOf("Firefox") != -1 )  {
		rtnValue = "Firefox";
	} else {
		rtnValue = "unknown";
	}

	return rtnValue;
}

function showBusySign() {
	document.getElementById('busy_indicator').style.display = 'block';
}

function checkValidForm() {
	if (isValidForm()) {
		showBusySign();
	}
}

function addWatermark(elem) {
	var browserStr = getBrowser();

	// Add a watermark to the date field
	if ((browserStr == "IE") || (browserStr == "Firefox") || (browserStr == "unknown")) {
		$('#' + elem).Watermark("mm/dd/yyyy");
	}
}

function isValidDigit(_dat, _len) {
	var rtnValue = false;
	var length = _dat.length;

	var isNumber =  /^\w+$/.test(_dat);//changed validation of ID card number from numeric to alphanumeric
	if (isNumber && (_dat.length <= _len)) {
		rtnValue = true;
	}
	
	return rtnValue;
}

function showSearchScreen(itm) {
	var claimMsg = "If you do not have Claim ID, please contact Provider Services at 1-855-895-1676.";

	if (itm == "memberId") {
		
		// Search by id card number
		$("#mainSearchButtons").show();
		$("#memberIdSearch").show();
		$("#claimIdSearch").hide();
		
		// Hide the results
		$("#memSearchResults").hide();
		$("#claimSearchResults").hide();
		
		// Hide the error message
		$("#errorMessage").hide();
	} else if (itm == "claimId") {
		$("#claimIDMessage").html(claimMsg);

		// Search by claim id
		$("#mainSearchButtons").show();
		$("#memberIdSearch").hide();
		$("#claimIdSearch").show();
		
		// Hide the results
		$("#memSearchResults").hide();
		$("#claimSearchResults").hide();
		
		// Hide the error message
		$("#errorMessage").hide();
	} else if (itm == "date") {
	 
		// Set search by to date
		$("#searchSelected").val("date")
			 
		$("#searchByDate").show();
		$("#searchById").hide();
	} else if (itm == "id") {
		$("#memberClaimIDMessage").html(claimMsg);

		// Set search by to claim id
		$("#searchSelected").val("id");
			 
		$("#searchByDate").hide();
		$("#searchById").show();
	}
}

/*
 *  Convert the date to yyyy-MM-dd from MM/dd/yyyy.
 */
function convertDate(_dat) {
	var rtnValue = "";
	
	// Parse the first date to individual parts
	var day = _dat.substring(3, 5);
	var month = _dat.substring(0, 2);
	var year = _dat.substring(6, 10);
	
	rtnValue = year + "-" + month + "-" + day;
	
	return rtnValue;
}

/*
 *  Convert the date to MM/dd/yyyy from yyyy-MM-dd.
 */
function convertToDisplayDate(_dat) {
	var rtnValue = "";
	
	// Parse the first date to individual parts
	var day = _dat.substring(8, 10);
	var month = _dat.substring(5, 7);
	var year = _dat.substring(0, 4);
	
	rtnValue = month + "/" + day + "/" + year;
	
	return rtnValue;
}

// Get start date for each browser type
function getStartDate() {
	var dat;
	var d = "01";
	var m = "01";
	var y = "2016";

	var browserStr = getBrowser();

	if ((browserStr == "Firefox") || (browserStr == "IE") || (browserStr == "unknown")) {

		// Format for IE and Firefox is mm/dd/yyyy
		dat = m + "/" + d + "/" + y;
	} else {

		// Format for all other browsers is yyyy-mm-dd
		dat = y + "-" + m + "-" + d;
	}

	return dat;
}

function isValidDate(_dateString, _requiredFlag) {
	var rtnValue = true;
	var browserStr = getBrowser();
	var parts;
 	var day;
 	var month;
	var year;

	if (_dateString.length > 0) {

		// First check for the pattern
		if ((browserStr == "Firefox") || (browserStr == "IE") || (browserStr == "unknown")) {
			
			// Check format mm/dd/yyyy
			if (!/^\d{1,2}\/\d{1,2}\/\d{4}$/.test(_dateString)) {
				rtnValue = false;
			}
			
			if (rtnValue) {
	
				// Parse the date parts to integers
				parts = _dateString.split("/");
				day = parseInt(parts[1], 10);
				month = parseInt(parts[0], 10);
				year = parseInt(parts[2], 10);
			}
		} else {
			
			// Check format yyyy-mm-dd
			if (!/^\d{4}-\d{1,2}-\d{1,2}$/.test(_dateString)) {
				rtnValue = false;
			}
		
			
			if (rtnValue) {
				// Parse the date parts to integers
				parts = _dateString.split("-");
				day = parseInt(parts[2], 10);
				month = parseInt(parts[1], 10);
				year = parseInt(parts[0], 10);
			}
		}
	
		if (rtnValue) {
	
			// Check the ranges of month and year
			if ((year < 1000) || (year > 3000) || (month == 0) || (month > 12)) {
				rtnValue = false;
			}
		
			var monthLength = [ 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 ];
		
			// Adjust for leap years
		 	if ((year % 400 == 0) || ((year % 100 != 0) && (year % 4 == 0))) {
				monthLength[1] = 29;
			}
		
			// Check the range of the day
			rtnValue = ((day > 0) && (day <= monthLength[month - 1]));
		}
	} else {
		if (_requiredFlag){
			rtnValue = false;
		}
	}
	return rtnValue;
}
