/**
 * ProviderId.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.avalon.esb.servicehelpers;

public class ProviderId  implements java.io.Serializable {
    private java.lang.String federalTaxIdNumber;

    private java.lang.String locationNumber;

    private java.lang.String nationalProviderIdentifier;

    private com.avalon.esb.servicehelpers.Code taxIdQualifier;

    private java.lang.String addtionalIdentifier;

    private com.avalon.esb.servicehelpers.Code addtionalIdentifierQualifier;

    private com.avalon.esb.servicehelpers.ProviderFivePartKey providerFivePartKey;

    public ProviderId() {
    }

    public ProviderId(
           java.lang.String federalTaxIdNumber,
           java.lang.String locationNumber,
           java.lang.String nationalProviderIdentifier,
           com.avalon.esb.servicehelpers.Code taxIdQualifier,
           java.lang.String addtionalIdentifier,
           com.avalon.esb.servicehelpers.Code addtionalIdentifierQualifier,
           com.avalon.esb.servicehelpers.ProviderFivePartKey providerFivePartKey) {
           this.federalTaxIdNumber = federalTaxIdNumber;
           this.locationNumber = locationNumber;
           this.nationalProviderIdentifier = nationalProviderIdentifier;
           this.taxIdQualifier = taxIdQualifier;
           this.addtionalIdentifier = addtionalIdentifier;
           this.addtionalIdentifierQualifier = addtionalIdentifierQualifier;
           this.providerFivePartKey = providerFivePartKey;
    }


    /**
     * Gets the federalTaxIdNumber value for this ProviderId.
     * 
     * @return federalTaxIdNumber
     */
    public java.lang.String getFederalTaxIdNumber() {
        return federalTaxIdNumber;
    }


    /**
     * Sets the federalTaxIdNumber value for this ProviderId.
     * 
     * @param federalTaxIdNumber
     */
    public void setFederalTaxIdNumber(java.lang.String federalTaxIdNumber) {
        this.federalTaxIdNumber = federalTaxIdNumber;
    }


    /**
     * Gets the locationNumber value for this ProviderId.
     * 
     * @return locationNumber
     */
    public java.lang.String getLocationNumber() {
        return locationNumber;
    }


    /**
     * Sets the locationNumber value for this ProviderId.
     * 
     * @param locationNumber
     */
    public void setLocationNumber(java.lang.String locationNumber) {
        this.locationNumber = locationNumber;
    }


    /**
     * Gets the nationalProviderIdentifier value for this ProviderId.
     * 
     * @return nationalProviderIdentifier
     */
    public java.lang.String getNationalProviderIdentifier() {
        return nationalProviderIdentifier;
    }


    /**
     * Sets the nationalProviderIdentifier value for this ProviderId.
     * 
     * @param nationalProviderIdentifier
     */
    public void setNationalProviderIdentifier(java.lang.String nationalProviderIdentifier) {
        this.nationalProviderIdentifier = nationalProviderIdentifier;
    }


    /**
     * Gets the taxIdQualifier value for this ProviderId.
     * 
     * @return taxIdQualifier
     */
    public com.avalon.esb.servicehelpers.Code getTaxIdQualifier() {
        return taxIdQualifier;
    }


    /**
     * Sets the taxIdQualifier value for this ProviderId.
     * 
     * @param taxIdQualifier
     */
    public void setTaxIdQualifier(com.avalon.esb.servicehelpers.Code taxIdQualifier) {
        this.taxIdQualifier = taxIdQualifier;
    }


    /**
     * Gets the addtionalIdentifier value for this ProviderId.
     * 
     * @return addtionalIdentifier
     */
    public java.lang.String getAddtionalIdentifier() {
        return addtionalIdentifier;
    }


    /**
     * Sets the addtionalIdentifier value for this ProviderId.
     * 
     * @param addtionalIdentifier
     */
    public void setAddtionalIdentifier(java.lang.String addtionalIdentifier) {
        this.addtionalIdentifier = addtionalIdentifier;
    }


    /**
     * Gets the addtionalIdentifierQualifier value for this ProviderId.
     * 
     * @return addtionalIdentifierQualifier
     */
    public com.avalon.esb.servicehelpers.Code getAddtionalIdentifierQualifier() {
        return addtionalIdentifierQualifier;
    }


    /**
     * Sets the addtionalIdentifierQualifier value for this ProviderId.
     * 
     * @param addtionalIdentifierQualifier
     */
    public void setAddtionalIdentifierQualifier(com.avalon.esb.servicehelpers.Code addtionalIdentifierQualifier) {
        this.addtionalIdentifierQualifier = addtionalIdentifierQualifier;
    }


    /**
     * Gets the providerFivePartKey value for this ProviderId.
     * 
     * @return providerFivePartKey
     */
    public com.avalon.esb.servicehelpers.ProviderFivePartKey getProviderFivePartKey() {
        return providerFivePartKey;
    }


    /**
     * Sets the providerFivePartKey value for this ProviderId.
     * 
     * @param providerFivePartKey
     */
    public void setProviderFivePartKey(com.avalon.esb.servicehelpers.ProviderFivePartKey providerFivePartKey) {
        this.providerFivePartKey = providerFivePartKey;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ProviderId)) return false;
        ProviderId other = (ProviderId) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.federalTaxIdNumber==null && other.getFederalTaxIdNumber()==null) || 
             (this.federalTaxIdNumber!=null &&
              this.federalTaxIdNumber.equals(other.getFederalTaxIdNumber()))) &&
            ((this.locationNumber==null && other.getLocationNumber()==null) || 
             (this.locationNumber!=null &&
              this.locationNumber.equals(other.getLocationNumber()))) &&
            ((this.nationalProviderIdentifier==null && other.getNationalProviderIdentifier()==null) || 
             (this.nationalProviderIdentifier!=null &&
              this.nationalProviderIdentifier.equals(other.getNationalProviderIdentifier()))) &&
            ((this.taxIdQualifier==null && other.getTaxIdQualifier()==null) || 
             (this.taxIdQualifier!=null &&
              this.taxIdQualifier.equals(other.getTaxIdQualifier()))) &&
            ((this.addtionalIdentifier==null && other.getAddtionalIdentifier()==null) || 
             (this.addtionalIdentifier!=null &&
              this.addtionalIdentifier.equals(other.getAddtionalIdentifier()))) &&
            ((this.addtionalIdentifierQualifier==null && other.getAddtionalIdentifierQualifier()==null) || 
             (this.addtionalIdentifierQualifier!=null &&
              this.addtionalIdentifierQualifier.equals(other.getAddtionalIdentifierQualifier()))) &&
            ((this.providerFivePartKey==null && other.getProviderFivePartKey()==null) || 
             (this.providerFivePartKey!=null &&
              this.providerFivePartKey.equals(other.getProviderFivePartKey())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getFederalTaxIdNumber() != null) {
            _hashCode += getFederalTaxIdNumber().hashCode();
        }
        if (getLocationNumber() != null) {
            _hashCode += getLocationNumber().hashCode();
        }
        if (getNationalProviderIdentifier() != null) {
            _hashCode += getNationalProviderIdentifier().hashCode();
        }
        if (getTaxIdQualifier() != null) {
            _hashCode += getTaxIdQualifier().hashCode();
        }
        if (getAddtionalIdentifier() != null) {
            _hashCode += getAddtionalIdentifier().hashCode();
        }
        if (getAddtionalIdentifierQualifier() != null) {
            _hashCode += getAddtionalIdentifierQualifier().hashCode();
        }
        if (getProviderFivePartKey() != null) {
            _hashCode += getProviderFivePartKey().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ProviderId.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://servicehelpers.esb.avalon.com", "ProviderId"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("federalTaxIdNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("", "federalTaxIdNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("locationNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("", "locationNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nationalProviderIdentifier");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nationalProviderIdentifier"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("taxIdQualifier");
        elemField.setXmlName(new javax.xml.namespace.QName("", "taxIdQualifier"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://servicehelpers.esb.avalon.com", "Code"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("addtionalIdentifier");
        elemField.setXmlName(new javax.xml.namespace.QName("", "addtionalIdentifier"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("addtionalIdentifierQualifier");
        elemField.setXmlName(new javax.xml.namespace.QName("", "addtionalIdentifierQualifier"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://servicehelpers.esb.avalon.com", "Code"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("providerFivePartKey");
        elemField.setXmlName(new javax.xml.namespace.QName("", "providerFivePartKey"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://servicehelpers.esb.avalon.com", "ProviderFivePartKey"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
