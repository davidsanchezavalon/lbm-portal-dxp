/**
 * ProviderInformation.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.avalon.esb.servicehelpers;

public class ProviderInformation  implements java.io.Serializable {
    private com.avalon.esb.servicehelpers.ProviderIdentifier providerIdentifier;

    private com.avalon.esb.servicehelpers.Name providerName;

    public ProviderInformation() {
    }

    public ProviderInformation(
           com.avalon.esb.servicehelpers.ProviderIdentifier providerIdentifier,
           com.avalon.esb.servicehelpers.Name providerName) {
           this.providerIdentifier = providerIdentifier;
           this.providerName = providerName;
    }


    /**
     * Gets the providerIdentifier value for this ProviderInformation.
     * 
     * @return providerIdentifier
     */
    public com.avalon.esb.servicehelpers.ProviderIdentifier getProviderIdentifier() {
        return providerIdentifier;
    }


    /**
     * Sets the providerIdentifier value for this ProviderInformation.
     * 
     * @param providerIdentifier
     */
    public void setProviderIdentifier(com.avalon.esb.servicehelpers.ProviderIdentifier providerIdentifier) {
        this.providerIdentifier = providerIdentifier;
    }


    /**
     * Gets the providerName value for this ProviderInformation.
     * 
     * @return providerName
     */
    public com.avalon.esb.servicehelpers.Name getProviderName() {
        return providerName;
    }


    /**
     * Sets the providerName value for this ProviderInformation.
     * 
     * @param providerName
     */
    public void setProviderName(com.avalon.esb.servicehelpers.Name providerName) {
        this.providerName = providerName;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ProviderInformation)) return false;
        ProviderInformation other = (ProviderInformation) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.providerIdentifier==null && other.getProviderIdentifier()==null) || 
             (this.providerIdentifier!=null &&
              this.providerIdentifier.equals(other.getProviderIdentifier()))) &&
            ((this.providerName==null && other.getProviderName()==null) || 
             (this.providerName!=null &&
              this.providerName.equals(other.getProviderName())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getProviderIdentifier() != null) {
            _hashCode += getProviderIdentifier().hashCode();
        }
        if (getProviderName() != null) {
            _hashCode += getProviderName().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ProviderInformation.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://servicehelpers.esb.avalon.com", "ProviderInformation"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("providerIdentifier");
        elemField.setXmlName(new javax.xml.namespace.QName("", "providerIdentifier"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://servicehelpers.esb.avalon.com", "ProviderIdentifier"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("providerName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "providerName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://servicehelpers.esb.avalon.com", "Name"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
