/**
 * ClaimSearchImplService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.avalon.esb.servicehelpers;

public interface ClaimSearchImplService extends javax.xml.rpc.Service {
    public java.lang.String getClaimSearchServicePortAddress();

    public com.avalon.esb.servicehelpers.LBMClaimSearchService getClaimSearchServicePort() throws javax.xml.rpc.ServiceException;

    public com.avalon.esb.servicehelpers.LBMClaimSearchService getClaimSearchServicePort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
