/**
 * ClaimSearchImplServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.avalon.esb.servicehelpers;

import com.avalon.claimsearch.fo.ClaimsSearchConstants;


/**
 * @author sai.s.kumar.cirigiri
 *
 */
public class ClaimSearchImplServiceLocator extends org.apache.axis.client.Service implements com.avalon.esb.servicehelpers.ClaimSearchImplService,ClaimsSearchConstants {

    /**
	 * 
	 */
	private static final long serialVersionUID = 2471647392810792672L;

	public ClaimSearchImplServiceLocator() {
    }


    public ClaimSearchImplServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public ClaimSearchImplServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

   
    
    //private java.lang.String ClaimSearchServicePort_address = ClaimsSearchBussinessDelegateVo.getClaimsSearchBussinessDelegateVo().getEndPointUrls().get(ESB_END_POINT_URL);// To Fetch evironment based esb urls

    /***To set the esb end point url**/
    
    private java.lang.String ClaimSearchServicePort_address = EsbUrlDelegate.getEsbUrlDelegate().getEsbUrl().get("findClaims");
    
    public java.lang.String getClaimSearchServicePortAddress() {
        return ClaimSearchServicePort_address;
    }
 
    // The WSDD service name defaults to the port name.
    private java.lang.String ClaimSearchServicePortWSDDServiceName = "ClaimSearchServicePort";

    public java.lang.String getClaimSearchServicePortWSDDServiceName() {
        return ClaimSearchServicePortWSDDServiceName;
    }

    public void setClaimSearchServicePortWSDDServiceName(java.lang.String name) {
        ClaimSearchServicePortWSDDServiceName = name;
    }

    public com.avalon.esb.servicehelpers.LBMClaimSearchService getClaimSearchServicePort() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(ClaimSearchServicePort_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getClaimSearchServicePort(endpoint);
    }

    public com.avalon.esb.servicehelpers.LBMClaimSearchService getClaimSearchServicePort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.avalon.esb.servicehelpers.ClaimSearchServicePortBindingStub _stub = new com.avalon.esb.servicehelpers.ClaimSearchServicePortBindingStub(portAddress, this);
            _stub.setPortName(getClaimSearchServicePortWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setClaimSearchServicePortEndpointAddress(java.lang.String address) {
        ClaimSearchServicePort_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.avalon.esb.servicehelpers.LBMClaimSearchService.class.isAssignableFrom(serviceEndpointInterface)) {
                com.avalon.esb.servicehelpers.ClaimSearchServicePortBindingStub _stub = new com.avalon.esb.servicehelpers.ClaimSearchServicePortBindingStub(new java.net.URL(ClaimSearchServicePort_address), this);
                _stub.setPortName(getClaimSearchServicePortWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("ClaimSearchServicePort".equals(inputPortName)) {
            return getClaimSearchServicePort();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://servicehelpers.esb.avalon.com", "ClaimSearchImplService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://servicehelpers.esb.avalon.com", "ClaimSearchServicePort"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("ClaimSearchServicePort".equals(portName)) {
            setClaimSearchServicePortEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
