/**
 * ClaimSearchFilter.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.avalon.esb.servicehelpers;

public class ClaimSearchFilter  implements java.io.Serializable {
    private java.lang.Boolean includePrivacyProtectedClaims;

    private java.lang.Boolean identifySensitiveClaims;

    public ClaimSearchFilter() {
    }

    public ClaimSearchFilter(
           java.lang.Boolean includePrivacyProtectedClaims,
           java.lang.Boolean identifySensitiveClaims) {
           this.includePrivacyProtectedClaims = includePrivacyProtectedClaims;
           this.identifySensitiveClaims = identifySensitiveClaims;
    }


    /**
     * Gets the includePrivacyProtectedClaims value for this ClaimSearchFilter.
     * 
     * @return includePrivacyProtectedClaims
     */
    public java.lang.Boolean getIncludePrivacyProtectedClaims() {
        return includePrivacyProtectedClaims;
    }


    /**
     * Sets the includePrivacyProtectedClaims value for this ClaimSearchFilter.
     * 
     * @param includePrivacyProtectedClaims
     */
    public void setIncludePrivacyProtectedClaims(java.lang.Boolean includePrivacyProtectedClaims) {
        this.includePrivacyProtectedClaims = includePrivacyProtectedClaims;
    }


    /**
     * Gets the identifySensitiveClaims value for this ClaimSearchFilter.
     * 
     * @return identifySensitiveClaims
     */
    public java.lang.Boolean getIdentifySensitiveClaims() {
        return identifySensitiveClaims;
    }


    /**
     * Sets the identifySensitiveClaims value for this ClaimSearchFilter.
     * 
     * @param identifySensitiveClaims
     */
    public void setIdentifySensitiveClaims(java.lang.Boolean identifySensitiveClaims) {
        this.identifySensitiveClaims = identifySensitiveClaims;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ClaimSearchFilter)) return false;
        ClaimSearchFilter other = (ClaimSearchFilter) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.includePrivacyProtectedClaims==null && other.getIncludePrivacyProtectedClaims()==null) || 
             (this.includePrivacyProtectedClaims!=null &&
              this.includePrivacyProtectedClaims.equals(other.getIncludePrivacyProtectedClaims()))) &&
            ((this.identifySensitiveClaims==null && other.getIdentifySensitiveClaims()==null) || 
             (this.identifySensitiveClaims!=null &&
              this.identifySensitiveClaims.equals(other.getIdentifySensitiveClaims())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getIncludePrivacyProtectedClaims() != null) {
            _hashCode += getIncludePrivacyProtectedClaims().hashCode();
        }
        if (getIdentifySensitiveClaims() != null) {
            _hashCode += getIdentifySensitiveClaims().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ClaimSearchFilter.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://servicehelpers.esb.avalon.com", "ClaimSearchFilter"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("includePrivacyProtectedClaims");
        elemField.setXmlName(new javax.xml.namespace.QName("", "includePrivacyProtectedClaims"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("identifySensitiveClaims");
        elemField.setXmlName(new javax.xml.namespace.QName("", "identifySensitiveClaims"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
