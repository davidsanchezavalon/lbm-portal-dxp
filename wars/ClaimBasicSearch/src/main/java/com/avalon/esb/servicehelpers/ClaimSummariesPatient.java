/**
 * ClaimSummariesPatient.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.avalon.esb.servicehelpers;

public class ClaimSummariesPatient  implements java.io.Serializable {
    private com.avalon.esb.servicehelpers.ClaimsSummary claimSummary;

    private com.avalon.esb.servicehelpers.Patient patient;
    
    private boolean confidentialCommunicationIndicator;

    public ClaimSummariesPatient() {
    }

    public ClaimSummariesPatient(
           com.avalon.esb.servicehelpers.ClaimsSummary claimSummary,
           com.avalon.esb.servicehelpers.Patient patient) {
           this.claimSummary = claimSummary;
           this.patient = patient;
    }


    /**
     * Gets the claimSummary value for this ClaimSummariesPatient.
     * 
     * @return claimSummary
     */
    public com.avalon.esb.servicehelpers.ClaimsSummary getClaimSummary() {
        return claimSummary;
    }


    /**
     * Sets the claimSummary value for this ClaimSummariesPatient.
     * 
     * @param claimSummary
     */
    public void setClaimSummary(com.avalon.esb.servicehelpers.ClaimsSummary claimSummary) {
        this.claimSummary = claimSummary;
    }


    /**
     * Gets the patient value for this ClaimSummariesPatient.
     * 
     * @return patient
     */
    public com.avalon.esb.servicehelpers.Patient getPatient() {
        return patient;
    }


    /**
     * Sets the patient value for this ClaimSummariesPatient.
     * 
     * @param patient
     */
    public void setPatient(com.avalon.esb.servicehelpers.Patient patient) {
        this.patient = patient;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ClaimSummariesPatient)) return false;
        ClaimSummariesPatient other = (ClaimSummariesPatient) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.claimSummary==null && other.getClaimSummary()==null) || 
             (this.claimSummary!=null &&
              this.claimSummary.equals(other.getClaimSummary()))) &&
            ((this.patient==null && other.getPatient()==null) || 
             (this.patient!=null &&
              this.patient.equals(other.getPatient())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getClaimSummary() != null) {
            _hashCode += getClaimSummary().hashCode();
        }
        if (getPatient() != null) {
            _hashCode += getPatient().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ClaimSummariesPatient.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://servicehelpers.esb.avalon.com", "ClaimSummariesPatient"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("claimSummary");
        elemField.setXmlName(new javax.xml.namespace.QName("", "claimSummary"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://servicehelpers.esb.avalon.com", "ClaimsSummary"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("patient");
        elemField.setXmlName(new javax.xml.namespace.QName("", "patient"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://servicehelpers.esb.avalon.com", "Patient"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
