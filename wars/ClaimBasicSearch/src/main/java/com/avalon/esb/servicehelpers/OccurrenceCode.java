/**
 * OccurrenceCode.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.avalon.esb.servicehelpers;

public class OccurrenceCode  implements java.io.Serializable {
    private java.lang.String occurrenceCode;

    private java.util.Calendar occurrenceDate;

    public OccurrenceCode() {
    }

    public OccurrenceCode(
           java.lang.String occurrenceCode,
           java.util.Calendar occurrenceDate) {
           this.occurrenceCode = occurrenceCode;
           this.occurrenceDate = occurrenceDate;
    }


    /**
     * Gets the occurrenceCode value for this OccurrenceCode.
     * 
     * @return occurrenceCode
     */
    public java.lang.String getOccurrenceCode() {
        return occurrenceCode;
    }


    /**
     * Sets the occurrenceCode value for this OccurrenceCode.
     * 
     * @param occurrenceCode
     */
    public void setOccurrenceCode(java.lang.String occurrenceCode) {
        this.occurrenceCode = occurrenceCode;
    }


    /**
     * Gets the occurrenceDate value for this OccurrenceCode.
     * 
     * @return occurrenceDate
     */
    public java.util.Calendar getOccurrenceDate() {
        return occurrenceDate;
    }


    /**
     * Sets the occurrenceDate value for this OccurrenceCode.
     * 
     * @param occurrenceDate
     */
    public void setOccurrenceDate(java.util.Calendar occurrenceDate) {
        this.occurrenceDate = occurrenceDate;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof OccurrenceCode)) return false;
        OccurrenceCode other = (OccurrenceCode) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.occurrenceCode==null && other.getOccurrenceCode()==null) || 
             (this.occurrenceCode!=null &&
              this.occurrenceCode.equals(other.getOccurrenceCode()))) &&
            ((this.occurrenceDate==null && other.getOccurrenceDate()==null) || 
             (this.occurrenceDate!=null &&
              this.occurrenceDate.equals(other.getOccurrenceDate())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getOccurrenceCode() != null) {
            _hashCode += getOccurrenceCode().hashCode();
        }
        if (getOccurrenceDate() != null) {
            _hashCode += getOccurrenceDate().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(OccurrenceCode.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://servicehelpers.esb.avalon.com", "OccurrenceCode"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("occurrenceCode");
        elemField.setXmlName(new javax.xml.namespace.QName("", "occurrenceCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("occurrenceDate");
        elemField.setXmlName(new javax.xml.namespace.QName("", "occurrenceDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
