/**
 * ClaimStatusType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.avalon.esb.servicehelpers;

public class ClaimStatusType implements java.io.Serializable {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected ClaimStatusType(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    }

    public static final java.lang.String _PROCESSED = "PROCESSED";
    public static final java.lang.String _REJECTED = "REJECTED";
    public static final java.lang.String _DENIED = "DENIED";
    public static final java.lang.String _PENDING = "PENDING";
    public static final java.lang.String _HARD_PENDED = "HARD_PENDED";
    public static final ClaimStatusType PROCESSED = new ClaimStatusType(_PROCESSED);
    public static final ClaimStatusType REJECTED = new ClaimStatusType(_REJECTED);
    public static final ClaimStatusType DENIED = new ClaimStatusType(_DENIED);
    public static final ClaimStatusType PENDING = new ClaimStatusType(_PENDING);
    public static final ClaimStatusType HARD_PENDED = new ClaimStatusType(_HARD_PENDED);
    public java.lang.String getValue() { return _value_;}
    public static ClaimStatusType fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        ClaimStatusType enumeration = (ClaimStatusType)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static ClaimStatusType fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}
    public java.lang.Object readResolve() throws java.io.ObjectStreamException { return fromValue(_value_);}
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumSerializer(
            _javaType, _xmlType);
    }
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumDeserializer(
            _javaType, _xmlType);
    }
    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ClaimStatusType.class);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://servicehelpers.esb.avalon.com", "ClaimStatusType"));
    }
    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

}
