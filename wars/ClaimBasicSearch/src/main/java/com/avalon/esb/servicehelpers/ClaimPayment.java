/**
 * ClaimPayment.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.avalon.esb.servicehelpers;

public class ClaimPayment  implements java.io.Serializable {
    private com.avalon.esb.servicehelpers.Code paymentMethod;

    private java.math.BigDecimal patientLiabilityAmount;

    private java.math.BigDecimal deductibleAmount;

    private java.math.BigDecimal coPaymentAmount;

    private java.math.BigDecimal nonCoveredAmount;

    private java.math.BigDecimal allowedAmount;

    private java.math.BigDecimal approvedAmount;

    private java.math.BigDecimal coInsuranceAmount;

    private java.math.BigDecimal otherAmount;

    private java.math.BigDecimal insurancePaidAmount;

    private java.math.BigDecimal remittancePaidAmount;

    private java.math.BigDecimal outOfPocketExpenseAmount;

    private java.math.BigDecimal globalOutOfPocketExpenseAmount;

    private java.math.BigDecimal cdhpPaidAmount;

    private java.math.BigDecimal otherInsurancePaidAmount;

    public ClaimPayment() {
    }

    public ClaimPayment(
           com.avalon.esb.servicehelpers.Code paymentMethod,
           java.math.BigDecimal patientLiabilityAmount,
           java.math.BigDecimal deductibleAmount,
           java.math.BigDecimal coPaymentAmount,
           java.math.BigDecimal nonCoveredAmount,
           java.math.BigDecimal allowedAmount,
           java.math.BigDecimal approvedAmount,
           java.math.BigDecimal coInsuranceAmount,
           java.math.BigDecimal otherAmount,
           java.math.BigDecimal insurancePaidAmount,
           java.math.BigDecimal remittancePaidAmount,
           java.math.BigDecimal outOfPocketExpenseAmount,
           java.math.BigDecimal globalOutOfPocketExpenseAmount,
           java.math.BigDecimal cdhpPaidAmount,
           java.math.BigDecimal otherInsurancePaidAmount) {
           this.paymentMethod = paymentMethod;
           this.patientLiabilityAmount = patientLiabilityAmount;
           this.deductibleAmount = deductibleAmount;
           this.coPaymentAmount = coPaymentAmount;
           this.nonCoveredAmount = nonCoveredAmount;
           this.allowedAmount = allowedAmount;
           this.approvedAmount = approvedAmount;
           this.coInsuranceAmount = coInsuranceAmount;
           this.otherAmount = otherAmount;
           this.insurancePaidAmount = insurancePaidAmount;
           this.remittancePaidAmount = remittancePaidAmount;
           this.outOfPocketExpenseAmount = outOfPocketExpenseAmount;
           this.globalOutOfPocketExpenseAmount = globalOutOfPocketExpenseAmount;
           this.cdhpPaidAmount = cdhpPaidAmount;
           this.otherInsurancePaidAmount = otherInsurancePaidAmount;
    }


    /**
     * Gets the paymentMethod value for this ClaimPayment.
     * 
     * @return paymentMethod
     */
    public com.avalon.esb.servicehelpers.Code getPaymentMethod() {
        return paymentMethod;
    }


    /**
     * Sets the paymentMethod value for this ClaimPayment.
     * 
     * @param paymentMethod
     */
    public void setPaymentMethod(com.avalon.esb.servicehelpers.Code paymentMethod) {
        this.paymentMethod = paymentMethod;
    }


    /**
     * Gets the patientLiabilityAmount value for this ClaimPayment.
     * 
     * @return patientLiabilityAmount
     */
    public java.math.BigDecimal getPatientLiabilityAmount() {
        return patientLiabilityAmount;
    }


    /**
     * Sets the patientLiabilityAmount value for this ClaimPayment.
     * 
     * @param patientLiabilityAmount
     */
    public void setPatientLiabilityAmount(java.math.BigDecimal patientLiabilityAmount) {
        this.patientLiabilityAmount = patientLiabilityAmount;
    }


    /**
     * Gets the deductibleAmount value for this ClaimPayment.
     * 
     * @return deductibleAmount
     */
    public java.math.BigDecimal getDeductibleAmount() {
        return deductibleAmount;
    }


    /**
     * Sets the deductibleAmount value for this ClaimPayment.
     * 
     * @param deductibleAmount
     */
    public void setDeductibleAmount(java.math.BigDecimal deductibleAmount) {
        this.deductibleAmount = deductibleAmount;
    }


    /**
     * Gets the coPaymentAmount value for this ClaimPayment.
     * 
     * @return coPaymentAmount
     */
    public java.math.BigDecimal getCoPaymentAmount() {
        return coPaymentAmount;
    }


    /**
     * Sets the coPaymentAmount value for this ClaimPayment.
     * 
     * @param coPaymentAmount
     */
    public void setCoPaymentAmount(java.math.BigDecimal coPaymentAmount) {
        this.coPaymentAmount = coPaymentAmount;
    }


    /**
     * Gets the nonCoveredAmount value for this ClaimPayment.
     * 
     * @return nonCoveredAmount
     */
    public java.math.BigDecimal getNonCoveredAmount() {
        return nonCoveredAmount;
    }


    /**
     * Sets the nonCoveredAmount value for this ClaimPayment.
     * 
     * @param nonCoveredAmount
     */
    public void setNonCoveredAmount(java.math.BigDecimal nonCoveredAmount) {
        this.nonCoveredAmount = nonCoveredAmount;
    }


    /**
     * Gets the allowedAmount value for this ClaimPayment.
     * 
     * @return allowedAmount
     */
    public java.math.BigDecimal getAllowedAmount() {
        return allowedAmount;
    }


    /**
     * Sets the allowedAmount value for this ClaimPayment.
     * 
     * @param allowedAmount
     */
    public void setAllowedAmount(java.math.BigDecimal allowedAmount) {
        this.allowedAmount = allowedAmount;
    }


    /**
     * Gets the approvedAmount value for this ClaimPayment.
     * 
     * @return approvedAmount
     */
    public java.math.BigDecimal getApprovedAmount() {
        return approvedAmount;
    }


    /**
     * Sets the approvedAmount value for this ClaimPayment.
     * 
     * @param approvedAmount
     */
    public void setApprovedAmount(java.math.BigDecimal approvedAmount) {
        this.approvedAmount = approvedAmount;
    }


    /**
     * Gets the coInsuranceAmount value for this ClaimPayment.
     * 
     * @return coInsuranceAmount
     */
    public java.math.BigDecimal getCoInsuranceAmount() {
        return coInsuranceAmount;
    }


    /**
     * Sets the coInsuranceAmount value for this ClaimPayment.
     * 
     * @param coInsuranceAmount
     */
    public void setCoInsuranceAmount(java.math.BigDecimal coInsuranceAmount) {
        this.coInsuranceAmount = coInsuranceAmount;
    }


    /**
     * Gets the otherAmount value for this ClaimPayment.
     * 
     * @return otherAmount
     */
    public java.math.BigDecimal getOtherAmount() {
        return otherAmount;
    }


    /**
     * Sets the otherAmount value for this ClaimPayment.
     * 
     * @param otherAmount
     */
    public void setOtherAmount(java.math.BigDecimal otherAmount) {
        this.otherAmount = otherAmount;
    }


    /**
     * Gets the insurancePaidAmount value for this ClaimPayment.
     * 
     * @return insurancePaidAmount
     */
    public java.math.BigDecimal getInsurancePaidAmount() {
        return insurancePaidAmount;
    }


    /**
     * Sets the insurancePaidAmount value for this ClaimPayment.
     * 
     * @param insurancePaidAmount
     */
    public void setInsurancePaidAmount(java.math.BigDecimal insurancePaidAmount) {
        this.insurancePaidAmount = insurancePaidAmount;
    }


    /**
     * Gets the remittancePaidAmount value for this ClaimPayment.
     * 
     * @return remittancePaidAmount
     */
    public java.math.BigDecimal getRemittancePaidAmount() {
        return remittancePaidAmount;
    }


    /**
     * Sets the remittancePaidAmount value for this ClaimPayment.
     * 
     * @param remittancePaidAmount
     */
    public void setRemittancePaidAmount(java.math.BigDecimal remittancePaidAmount) {
        this.remittancePaidAmount = remittancePaidAmount;
    }


    /**
     * Gets the outOfPocketExpenseAmount value for this ClaimPayment.
     * 
     * @return outOfPocketExpenseAmount
     */
    public java.math.BigDecimal getOutOfPocketExpenseAmount() {
        return outOfPocketExpenseAmount;
    }


    /**
     * Sets the outOfPocketExpenseAmount value for this ClaimPayment.
     * 
     * @param outOfPocketExpenseAmount
     */
    public void setOutOfPocketExpenseAmount(java.math.BigDecimal outOfPocketExpenseAmount) {
        this.outOfPocketExpenseAmount = outOfPocketExpenseAmount;
    }


    /**
     * Gets the globalOutOfPocketExpenseAmount value for this ClaimPayment.
     * 
     * @return globalOutOfPocketExpenseAmount
     */
    public java.math.BigDecimal getGlobalOutOfPocketExpenseAmount() {
        return globalOutOfPocketExpenseAmount;
    }


    /**
     * Sets the globalOutOfPocketExpenseAmount value for this ClaimPayment.
     * 
     * @param globalOutOfPocketExpenseAmount
     */
    public void setGlobalOutOfPocketExpenseAmount(java.math.BigDecimal globalOutOfPocketExpenseAmount) {
        this.globalOutOfPocketExpenseAmount = globalOutOfPocketExpenseAmount;
    }


    /**
     * Gets the cdhpPaidAmount value for this ClaimPayment.
     * 
     * @return cdhpPaidAmount
     */
    public java.math.BigDecimal getCdhpPaidAmount() {
        return cdhpPaidAmount;
    }


    /**
     * Sets the cdhpPaidAmount value for this ClaimPayment.
     * 
     * @param cdhpPaidAmount
     */
    public void setCdhpPaidAmount(java.math.BigDecimal cdhpPaidAmount) {
        this.cdhpPaidAmount = cdhpPaidAmount;
    }


    /**
     * Gets the otherInsurancePaidAmount value for this ClaimPayment.
     * 
     * @return otherInsurancePaidAmount
     */
    public java.math.BigDecimal getOtherInsurancePaidAmount() {
        return otherInsurancePaidAmount;
    }


    /**
     * Sets the otherInsurancePaidAmount value for this ClaimPayment.
     * 
     * @param otherInsurancePaidAmount
     */
    public void setOtherInsurancePaidAmount(java.math.BigDecimal otherInsurancePaidAmount) {
        this.otherInsurancePaidAmount = otherInsurancePaidAmount;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ClaimPayment)) return false;
        ClaimPayment other = (ClaimPayment) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.paymentMethod==null && other.getPaymentMethod()==null) || 
             (this.paymentMethod!=null &&
              this.paymentMethod.equals(other.getPaymentMethod()))) &&
            ((this.patientLiabilityAmount==null && other.getPatientLiabilityAmount()==null) || 
             (this.patientLiabilityAmount!=null &&
              this.patientLiabilityAmount.equals(other.getPatientLiabilityAmount()))) &&
            ((this.deductibleAmount==null && other.getDeductibleAmount()==null) || 
             (this.deductibleAmount!=null &&
              this.deductibleAmount.equals(other.getDeductibleAmount()))) &&
            ((this.coPaymentAmount==null && other.getCoPaymentAmount()==null) || 
             (this.coPaymentAmount!=null &&
              this.coPaymentAmount.equals(other.getCoPaymentAmount()))) &&
            ((this.nonCoveredAmount==null && other.getNonCoveredAmount()==null) || 
             (this.nonCoveredAmount!=null &&
              this.nonCoveredAmount.equals(other.getNonCoveredAmount()))) &&
            ((this.allowedAmount==null && other.getAllowedAmount()==null) || 
             (this.allowedAmount!=null &&
              this.allowedAmount.equals(other.getAllowedAmount()))) &&
            ((this.approvedAmount==null && other.getApprovedAmount()==null) || 
             (this.approvedAmount!=null &&
              this.approvedAmount.equals(other.getApprovedAmount()))) &&
            ((this.coInsuranceAmount==null && other.getCoInsuranceAmount()==null) || 
             (this.coInsuranceAmount!=null &&
              this.coInsuranceAmount.equals(other.getCoInsuranceAmount()))) &&
            ((this.otherAmount==null && other.getOtherAmount()==null) || 
             (this.otherAmount!=null &&
              this.otherAmount.equals(other.getOtherAmount()))) &&
            ((this.insurancePaidAmount==null && other.getInsurancePaidAmount()==null) || 
             (this.insurancePaidAmount!=null &&
              this.insurancePaidAmount.equals(other.getInsurancePaidAmount()))) &&
            ((this.remittancePaidAmount==null && other.getRemittancePaidAmount()==null) || 
             (this.remittancePaidAmount!=null &&
              this.remittancePaidAmount.equals(other.getRemittancePaidAmount()))) &&
            ((this.outOfPocketExpenseAmount==null && other.getOutOfPocketExpenseAmount()==null) || 
             (this.outOfPocketExpenseAmount!=null &&
              this.outOfPocketExpenseAmount.equals(other.getOutOfPocketExpenseAmount()))) &&
            ((this.globalOutOfPocketExpenseAmount==null && other.getGlobalOutOfPocketExpenseAmount()==null) || 
             (this.globalOutOfPocketExpenseAmount!=null &&
              this.globalOutOfPocketExpenseAmount.equals(other.getGlobalOutOfPocketExpenseAmount()))) &&
            ((this.cdhpPaidAmount==null && other.getCdhpPaidAmount()==null) || 
             (this.cdhpPaidAmount!=null &&
              this.cdhpPaidAmount.equals(other.getCdhpPaidAmount()))) &&
            ((this.otherInsurancePaidAmount==null && other.getOtherInsurancePaidAmount()==null) || 
             (this.otherInsurancePaidAmount!=null &&
              this.otherInsurancePaidAmount.equals(other.getOtherInsurancePaidAmount())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getPaymentMethod() != null) {
            _hashCode += getPaymentMethod().hashCode();
        }
        if (getPatientLiabilityAmount() != null) {
            _hashCode += getPatientLiabilityAmount().hashCode();
        }
        if (getDeductibleAmount() != null) {
            _hashCode += getDeductibleAmount().hashCode();
        }
        if (getCoPaymentAmount() != null) {
            _hashCode += getCoPaymentAmount().hashCode();
        }
        if (getNonCoveredAmount() != null) {
            _hashCode += getNonCoveredAmount().hashCode();
        }
        if (getAllowedAmount() != null) {
            _hashCode += getAllowedAmount().hashCode();
        }
        if (getApprovedAmount() != null) {
            _hashCode += getApprovedAmount().hashCode();
        }
        if (getCoInsuranceAmount() != null) {
            _hashCode += getCoInsuranceAmount().hashCode();
        }
        if (getOtherAmount() != null) {
            _hashCode += getOtherAmount().hashCode();
        }
        if (getInsurancePaidAmount() != null) {
            _hashCode += getInsurancePaidAmount().hashCode();
        }
        if (getRemittancePaidAmount() != null) {
            _hashCode += getRemittancePaidAmount().hashCode();
        }
        if (getOutOfPocketExpenseAmount() != null) {
            _hashCode += getOutOfPocketExpenseAmount().hashCode();
        }
        if (getGlobalOutOfPocketExpenseAmount() != null) {
            _hashCode += getGlobalOutOfPocketExpenseAmount().hashCode();
        }
        if (getCdhpPaidAmount() != null) {
            _hashCode += getCdhpPaidAmount().hashCode();
        }
        if (getOtherInsurancePaidAmount() != null) {
            _hashCode += getOtherInsurancePaidAmount().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ClaimPayment.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://servicehelpers.esb.avalon.com", "ClaimPayment"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("paymentMethod");
        elemField.setXmlName(new javax.xml.namespace.QName("", "paymentMethod"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://servicehelpers.esb.avalon.com", "Code"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("patientLiabilityAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("", "patientLiabilityAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("deductibleAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("", "deductibleAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("coPaymentAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("", "coPaymentAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nonCoveredAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nonCoveredAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("allowedAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("", "allowedAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("approvedAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("", "approvedAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("coInsuranceAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("", "coInsuranceAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("otherAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("", "otherAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("insurancePaidAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("", "insurancePaidAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("remittancePaidAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("", "remittancePaidAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("outOfPocketExpenseAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("", "outOfPocketExpenseAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("globalOutOfPocketExpenseAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("", "globalOutOfPocketExpenseAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cdhpPaidAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cdhpPaidAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("otherInsurancePaidAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("", "otherInsurancePaidAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
