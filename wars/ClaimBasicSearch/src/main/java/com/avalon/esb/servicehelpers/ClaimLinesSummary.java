/**
 * ClaimLinesSummary.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.avalon.esb.servicehelpers;

public class ClaimLinesSummary  implements java.io.Serializable {
    private com.avalon.esb.servicehelpers.Patient patient;

    private java.lang.String claimNumber;

    private java.lang.Boolean sensitiveClaim;

    private com.avalon.esb.servicehelpers.Code icdVersion;

    public ClaimLinesSummary() {
    }

    public ClaimLinesSummary(
           com.avalon.esb.servicehelpers.Patient patient,
           java.lang.String claimNumber,
           java.lang.Boolean sensitiveClaim,
           com.avalon.esb.servicehelpers.Code icdVersion) {
           this.patient = patient;
           this.claimNumber = claimNumber;
           this.sensitiveClaim = sensitiveClaim;
           this.icdVersion = icdVersion;
    }


    /**
     * Gets the patient value for this ClaimLinesSummary.
     * 
     * @return patient
     */
    public com.avalon.esb.servicehelpers.Patient getPatient() {
        return patient;
    }


    /**
     * Sets the patient value for this ClaimLinesSummary.
     * 
     * @param patient
     */
    public void setPatient(com.avalon.esb.servicehelpers.Patient patient) {
        this.patient = patient;
    }


    /**
     * Gets the claimNumber value for this ClaimLinesSummary.
     * 
     * @return claimNumber
     */
    public java.lang.String getClaimNumber() {
        return claimNumber;
    }


    /**
     * Sets the claimNumber value for this ClaimLinesSummary.
     * 
     * @param claimNumber
     */
    public void setClaimNumber(java.lang.String claimNumber) {
        this.claimNumber = claimNumber;
    }


    /**
     * Gets the sensitiveClaim value for this ClaimLinesSummary.
     * 
     * @return sensitiveClaim
     */
    public java.lang.Boolean getSensitiveClaim() {
        return sensitiveClaim;
    }


    /**
     * Sets the sensitiveClaim value for this ClaimLinesSummary.
     * 
     * @param sensitiveClaim
     */
    public void setSensitiveClaim(java.lang.Boolean sensitiveClaim) {
        this.sensitiveClaim = sensitiveClaim;
    }


    /**
     * Gets the icdVersion value for this ClaimLinesSummary.
     * 
     * @return icdVersion
     */
    public com.avalon.esb.servicehelpers.Code getIcdVersion() {
        return icdVersion;
    }


    /**
     * Sets the icdVersion value for this ClaimLinesSummary.
     * 
     * @param icdVersion
     */
    public void setIcdVersion(com.avalon.esb.servicehelpers.Code icdVersion) {
        this.icdVersion = icdVersion;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ClaimLinesSummary)) return false;
        ClaimLinesSummary other = (ClaimLinesSummary) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.patient==null && other.getPatient()==null) || 
             (this.patient!=null &&
              this.patient.equals(other.getPatient()))) &&
            ((this.claimNumber==null && other.getClaimNumber()==null) || 
             (this.claimNumber!=null &&
              this.claimNumber.equals(other.getClaimNumber()))) &&
            ((this.sensitiveClaim==null && other.getSensitiveClaim()==null) || 
             (this.sensitiveClaim!=null &&
              this.sensitiveClaim.equals(other.getSensitiveClaim()))) &&
            ((this.icdVersion==null && other.getIcdVersion()==null) || 
             (this.icdVersion!=null &&
              this.icdVersion.equals(other.getIcdVersion())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getPatient() != null) {
            _hashCode += getPatient().hashCode();
        }
        if (getClaimNumber() != null) {
            _hashCode += getClaimNumber().hashCode();
        }
        if (getSensitiveClaim() != null) {
            _hashCode += getSensitiveClaim().hashCode();
        }
        if (getIcdVersion() != null) {
            _hashCode += getIcdVersion().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ClaimLinesSummary.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://servicehelpers.esb.avalon.com", "ClaimLinesSummary"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("patient");
        elemField.setXmlName(new javax.xml.namespace.QName("", "patient"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://servicehelpers.esb.avalon.com", "Patient"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("claimNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("", "claimNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sensitiveClaim");
        elemField.setXmlName(new javax.xml.namespace.QName("", "sensitiveClaim"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("icdVersion");
        elemField.setXmlName(new javax.xml.namespace.QName("", "icdVersion"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://servicehelpers.esb.avalon.com", "Code"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
