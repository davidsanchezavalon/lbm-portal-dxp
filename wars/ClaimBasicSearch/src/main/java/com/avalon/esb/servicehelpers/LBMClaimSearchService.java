/**
 * LBMClaimSearchService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.avalon.esb.servicehelpers;

public interface LBMClaimSearchService extends java.rmi.Remote {
    public com.avalon.esb.servicehelpers.ClaimSummariesPatient[] findClaims(com.avalon.esb.servicehelpers.FindClaims findClaims) throws java.rmi.RemoteException;
    public com.avalon.esb.servicehelpers.GetClaimDetailsResponse getClaimDetails(com.avalon.esb.servicehelpers.FindClaims findClaims) throws java.rmi.RemoteException;
    //TODO: Remove Success Mock Code
    public com.avalon.esb.servicehelpers.GetClaimDetailsResponse getSuccessMockClaimDetails(com.avalon.esb.servicehelpers.FindClaims findClaims) throws java.rmi.RemoteException;
    //TODO: Remove Success Mock Code
    public com.avalon.esb.servicehelpers.ClaimSummariesPatient[] findClaimsSuccessMock(com.avalon.esb.servicehelpers.FindClaims findClaims) throws java.rmi.RemoteException;
    
}
