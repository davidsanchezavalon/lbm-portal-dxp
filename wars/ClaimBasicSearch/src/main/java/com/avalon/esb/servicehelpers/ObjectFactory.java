
package com.avalon.esb.servicehelpers;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.avalon.esb.servicehelpers package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GetClaimDetails_QNAME = new QName("http://servicehelpers.esb.avalon.com", "getClaimDetails");
    private final static QName _GetClaimDetailsResponse_QNAME = new QName("http://servicehelpers.esb.avalon.com", "getClaimDetailsResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.avalon.esb.servicehelpers
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetClaimDetails }
     * 
     */
    public GetClaimDetails createGetClaimDetails() {
        return new GetClaimDetails();
    }

    /**
     * Create an instance of {@link GetClaimDetailsResponse }
     * 
     */
    public GetClaimDetailsResponse createGetClaimDetailsResponse() {
        return new GetClaimDetailsResponse();
    }

    /**
     * Create an instance of {@link FindClaims }
     * 
     */
    public FindClaims createFindClaims() {
        return new FindClaims();
    }

    /**
     * Create an instance of {@link ClaimStatusInboundReq }
     * 
     */
    public ClaimStatusInboundReq createClaimStatusInboundReq() {
        return new ClaimStatusInboundReq();
    }

    /**
     * Create an instance of {@link ClaimSearchCriteria }
     * 
     */
    public ClaimSearchCriteria createClaimSearchCriteria() {
        return new ClaimSearchCriteria();
    }

    /**
     * Create an instance of {@link HealthClaimSearchCriteria }
     * 
     */
    public HealthClaimSearchCriteria createHealthClaimSearchCriteria() {
        return new HealthClaimSearchCriteria();
    }

    /**
     * Create an instance of {@link ClaimSearchFilter }
     * 
     */
    public ClaimSearchFilter createClaimSearchFilter() {
        return new ClaimSearchFilter();
    }

    /**
     * Create an instance of {@link Patient }
     * 
     */
    public Patient createPatient() {
        return new Patient();
    }

    /**
     * Create an instance of {@link ProviderInformation }
     * 
     */
    public ProviderInformation createProviderInformation() {
        return new ProviderInformation();
    }

    /**
     * Create an instance of {@link Name }
     * 
     */
    public Name createName() {
        return new Name();
    }

    /**
     * Create an instance of {@link Person }
     * 
     */
    public Person createPerson() {
        return new Person();
    }

    /**
     * Create an instance of {@link SubscriberId }
     * 
     */
    public SubscriberId createSubscriberId() {
        return new SubscriberId();
    }

    /**
     * Create an instance of {@link Code }
     * 
     */
    public Code createCode() {
        return new Code();
    }

    /**
     * Create an instance of {@link CoverageInformation }
     * 
     */
    public CoverageInformation createCoverageInformation() {
        return new CoverageInformation();
    }

    /**
     * Create an instance of {@link Provider }
     * 
     */
    public Provider createProvider() {
        return new Provider();
    }

    /**
     * Create an instance of {@link ProviderId }
     * 
     */
    public ProviderId createProviderId() {
        return new ProviderId();
    }

    /**
     * Create an instance of {@link ProviderIdentifier }
     * 
     */
    public ProviderIdentifier createProviderIdentifier() {
        return new ProviderIdentifier();
    }

    /**
     * Create an instance of {@link ProviderFivePartKey }
     * 
     */
    public ProviderFivePartKey createProviderFivePartKey() {
        return new ProviderFivePartKey();
    }

    /**
     * Create an instance of {@link ClaimDetailSearchCriteria }
     * 
     */
    public ClaimDetailSearchCriteria createClaimDetailSearchCriteria() {
        return new ClaimDetailSearchCriteria();
    }

    /**
     * Create an instance of {@link AccessibleGroups }
     * 
     */
    public AccessibleGroups createAccessibleGroups() {
        return new AccessibleGroups();
    }

    /**
     * Create an instance of {@link GroupNumberRange }
     * 
     */
    public GroupNumberRange createGroupNumberRange() {
        return new GroupNumberRange();
    }

    /**
     * Create an instance of {@link FindClaimsResponse }
     * 
     */
    public FindClaimsResponse createFindClaimsResponse() {
        return new FindClaimsResponse();
    }

    /**
     * Create an instance of {@link ClaimSummariesPatient }
     * 
     */
    public ClaimSummariesPatient createClaimSummariesPatient() {
        return new ClaimSummariesPatient();
    }

    /**
     * Create an instance of {@link ClaimsSummary }
     * 
     */
    public ClaimsSummary createClaimsSummary() {
        return new ClaimsSummary();
    }

    /**
     * Create an instance of {@link GroupPeriod }
     * 
     */
    public GroupPeriod createGroupPeriod() {
        return new GroupPeriod();
    }

    /**
     * Create an instance of {@link ClaimsSummaryItem }
     * 
     */
    public ClaimsSummaryItem createClaimsSummaryItem() {
        return new ClaimsSummaryItem();
    }

    /**
     * Create an instance of {@link PeriodDate }
     * 
     */
    public PeriodDate createPeriodDate() {
        return new PeriodDate();
    }

    /**
     * Create an instance of {@link ClaimLinesSummary }
     * 
     */
    public ClaimLinesSummary createClaimLinesSummary() {
        return new ClaimLinesSummary();
    }

    /**
     * Create an instance of {@link OutOfAreaClaimsSummaryItem }
     * 
     */
    public OutOfAreaClaimsSummaryItem createOutOfAreaClaimsSummaryItem() {
        return new OutOfAreaClaimsSummaryItem();
    }

    /**
     * Create an instance of {@link ClaimDetail }
     * 
     */
    public ClaimDetail createClaimDetail() {
        return new ClaimDetail();
    }

    /**
     * Create an instance of {@link KeyDescription }
     * 
     */
    public KeyDescription createKeyDescription() {
        return new KeyDescription();
    }

    /**
     * Create an instance of {@link OccurrenceCode }
     * 
     */
    public OccurrenceCode createOccurrenceCode() {
        return new OccurrenceCode();
    }

    /**
     * Create an instance of {@link ValueCode }
     * 
     */
    public ValueCode createValueCode() {
        return new ValueCode();
    }

    /**
     * Create an instance of {@link ClaimPayment }
     * 
     */
    public ClaimPayment createClaimPayment() {
        return new ClaimPayment();
    }

    /**
     * Create an instance of {@link ClaimStatusEntity }
     * 
     */
    public ClaimStatusEntity createClaimStatusEntity() {
        return new ClaimStatusEntity();
    }

    /**
     * Create an instance of {@link HealthClaimDetail }
     * 
     */
    public HealthClaimDetail createHealthClaimDetail() {
        return new HealthClaimDetail();
    }

    /**
     * Create an instance of {@link DentalClaimDetail }
     * 
     */
    public DentalClaimDetail createDentalClaimDetail() {
        return new DentalClaimDetail();
    }

    /**
     * Create an instance of {@link ExternalCauseInjuryDetail }
     * 
     */
    public ExternalCauseInjuryDetail createExternalCauseInjuryDetail() {
        return new ExternalCauseInjuryDetail();
    }

    /**
     * Create an instance of {@link DiagnosisCodeDetail }
     * 
     */
    public DiagnosisCodeDetail createDiagnosisCodeDetail() {
        return new DiagnosisCodeDetail();
    }

    /**
     * Create an instance of {@link ProcedureCodeDetail }
     * 
     */
    public ProcedureCodeDetail createProcedureCodeDetail() {
        return new ProcedureCodeDetail();
    }

    /**
     * Create an instance of {@link DiagnosisCode }
     * 
     */
    public DiagnosisCode createDiagnosisCode() {
        return new DiagnosisCode();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetClaimDetails }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://servicehelpers.esb.avalon.com", name = "getClaimDetails")
    public JAXBElement<GetClaimDetails> createGetClaimDetails(GetClaimDetails value) {
        return new JAXBElement<GetClaimDetails>(_GetClaimDetails_QNAME, GetClaimDetails.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetClaimDetailsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://servicehelpers.esb.avalon.com", name = "getClaimDetailsResponse")
    public JAXBElement<GetClaimDetailsResponse> createGetClaimDetailsResponse(GetClaimDetailsResponse value) {
        return new JAXBElement<GetClaimDetailsResponse>(_GetClaimDetailsResponse_QNAME, GetClaimDetailsResponse.class, null, value);
    }

}
