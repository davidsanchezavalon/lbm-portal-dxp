/**
 * ClaimsSummaryItem.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.avalon.esb.servicehelpers;

public class ClaimsSummaryItem  implements java.io.Serializable {
    private java.lang.String claimNumber;

    private com.avalon.esb.servicehelpers.PeriodDate claimDateOfService;

    private int numberOfServiceLines;

    private java.lang.String patientAccountNumber;

    private com.avalon.esb.servicehelpers.Provider baseProvider;

    private java.lang.String billingProviderName;

    private java.lang.String claimPaidTo;

    private java.lang.String claimPaidToDescription;

    private java.math.BigDecimal totalClaimChargeAmount;

    private java.util.Calendar processedDate;

    private java.lang.Boolean sensitiveClaim;

    private com.avalon.esb.servicehelpers.ClaimStatusType statusType;

    private java.math.BigDecimal insurancePaidAmount;

    private java.math.BigDecimal remittanceAmount;

    private java.math.BigDecimal patientLiabilityAmount;

    private java.util.Calendar checkIssueDate;

    private java.lang.String providerID;

    private java.lang.String paymentMethod;

    private com.avalon.esb.servicehelpers.Code billType;

    private java.lang.Boolean claimNotFound;

    private com.avalon.esb.servicehelpers.ClaimLinesSummary claimLinesSummary;

    private com.avalon.esb.servicehelpers.OutOfAreaClaimsSummaryItem[] outOfAreaClaimsSummaryItemList;

    public ClaimsSummaryItem() {
    }

    public ClaimsSummaryItem(
           java.lang.String claimNumber,
           com.avalon.esb.servicehelpers.PeriodDate claimDateOfService,
           int numberOfServiceLines,
           java.lang.String patientAccountNumber,
           com.avalon.esb.servicehelpers.Provider baseProvider,
           java.lang.String billingProviderName,
           java.lang.String claimPaidTo,
           java.lang.String claimPaidToDescription,
           java.math.BigDecimal totalClaimChargeAmount,
           java.util.Calendar processedDate,
           java.lang.Boolean sensitiveClaim,
           com.avalon.esb.servicehelpers.ClaimStatusType statusType,
           java.math.BigDecimal insurancePaidAmount,
           java.math.BigDecimal remittanceAmount,
           java.math.BigDecimal patientLiabilityAmount,
           java.util.Calendar checkIssueDate,
           java.lang.String providerID,
           java.lang.String paymentMethod,
           com.avalon.esb.servicehelpers.Code billType,
           java.lang.Boolean claimNotFound,
           com.avalon.esb.servicehelpers.ClaimLinesSummary claimLinesSummary,
           com.avalon.esb.servicehelpers.OutOfAreaClaimsSummaryItem[] outOfAreaClaimsSummaryItemList) {
           this.claimNumber = claimNumber;
           this.claimDateOfService = claimDateOfService;
           this.numberOfServiceLines = numberOfServiceLines;
           this.patientAccountNumber = patientAccountNumber;
           this.baseProvider = baseProvider;
           this.billingProviderName = billingProviderName;
           this.claimPaidTo = claimPaidTo;
           this.claimPaidToDescription = claimPaidToDescription;
           this.totalClaimChargeAmount = totalClaimChargeAmount;
           this.processedDate = processedDate;
           this.sensitiveClaim = sensitiveClaim;
           this.statusType = statusType;
           this.insurancePaidAmount = insurancePaidAmount;
           this.remittanceAmount = remittanceAmount;
           this.patientLiabilityAmount = patientLiabilityAmount;
           this.checkIssueDate = checkIssueDate;
           this.providerID = providerID;
           this.paymentMethod = paymentMethod;
           this.billType = billType;
           this.claimNotFound = claimNotFound;
           this.claimLinesSummary = claimLinesSummary;
           this.outOfAreaClaimsSummaryItemList = outOfAreaClaimsSummaryItemList;
    }


    /**
     * Gets the claimNumber value for this ClaimsSummaryItem.
     * 
     * @return claimNumber
     */
    public java.lang.String getClaimNumber() {
        return claimNumber;
    }


    /**
     * Sets the claimNumber value for this ClaimsSummaryItem.
     * 
     * @param claimNumber
     */
    public void setClaimNumber(java.lang.String claimNumber) {
        this.claimNumber = claimNumber;
    }


    /**
     * Gets the claimDateOfService value for this ClaimsSummaryItem.
     * 
     * @return claimDateOfService
     */
    public com.avalon.esb.servicehelpers.PeriodDate getClaimDateOfService() {
        return claimDateOfService;
    }


    /**
     * Sets the claimDateOfService value for this ClaimsSummaryItem.
     * 
     * @param claimDateOfService
     */
    public void setClaimDateOfService(com.avalon.esb.servicehelpers.PeriodDate claimDateOfService) {
        this.claimDateOfService = claimDateOfService;
    }


    /**
     * Gets the numberOfServiceLines value for this ClaimsSummaryItem.
     * 
     * @return numberOfServiceLines
     */
    public int getNumberOfServiceLines() {
        return numberOfServiceLines;
    }


    /**
     * Sets the numberOfServiceLines value for this ClaimsSummaryItem.
     * 
     * @param numberOfServiceLines
     */
    public void setNumberOfServiceLines(int numberOfServiceLines) {
        this.numberOfServiceLines = numberOfServiceLines;
    }


    /**
     * Gets the patientAccountNumber value for this ClaimsSummaryItem.
     * 
     * @return patientAccountNumber
     */
    public java.lang.String getPatientAccountNumber() {
        return patientAccountNumber;
    }


    /**
     * Sets the patientAccountNumber value for this ClaimsSummaryItem.
     * 
     * @param patientAccountNumber
     */
    public void setPatientAccountNumber(java.lang.String patientAccountNumber) {
        this.patientAccountNumber = patientAccountNumber;
    }


    /**
     * Gets the baseProvider value for this ClaimsSummaryItem.
     * 
     * @return baseProvider
     */
    public com.avalon.esb.servicehelpers.Provider getBaseProvider() {
        return baseProvider;
    }


    /**
     * Sets the baseProvider value for this ClaimsSummaryItem.
     * 
     * @param baseProvider
     */
    public void setBaseProvider(com.avalon.esb.servicehelpers.Provider baseProvider) {
        this.baseProvider = baseProvider;
    }


    /**
     * Gets the billingProviderName value for this ClaimsSummaryItem.
     * 
     * @return billingProviderName
     */
    public java.lang.String getBillingProviderName() {
        return billingProviderName;
    }


    /**
     * Sets the billingProviderName value for this ClaimsSummaryItem.
     * 
     * @param billingProviderName
     */
    public void setBillingProviderName(java.lang.String billingProviderName) {
        this.billingProviderName = billingProviderName;
    }


    /**
     * Gets the claimPaidTo value for this ClaimsSummaryItem.
     * 
     * @return claimPaidTo
     */
    public java.lang.String getClaimPaidTo() {
        return claimPaidTo;
    }


    /**
     * Sets the claimPaidTo value for this ClaimsSummaryItem.
     * 
     * @param claimPaidTo
     */
    public void setClaimPaidTo(java.lang.String claimPaidTo) {
        this.claimPaidTo = claimPaidTo;
    }


    /**
     * Gets the claimPaidToDescription value for this ClaimsSummaryItem.
     * 
     * @return claimPaidToDescription
     */
    public java.lang.String getClaimPaidToDescription() {
        return claimPaidToDescription;
    }


    /**
     * Sets the claimPaidToDescription value for this ClaimsSummaryItem.
     * 
     * @param claimPaidToDescription
     */
    public void setClaimPaidToDescription(java.lang.String claimPaidToDescription) {
        this.claimPaidToDescription = claimPaidToDescription;
    }


    /**
     * Gets the totalClaimChargeAmount value for this ClaimsSummaryItem.
     * 
     * @return totalClaimChargeAmount
     */
    public java.math.BigDecimal getTotalClaimChargeAmount() {
        return totalClaimChargeAmount;
    }


    /**
     * Sets the totalClaimChargeAmount value for this ClaimsSummaryItem.
     * 
     * @param totalClaimChargeAmount
     */
    public void setTotalClaimChargeAmount(java.math.BigDecimal totalClaimChargeAmount) {
        this.totalClaimChargeAmount = totalClaimChargeAmount;
    }


    /**
     * Gets the processedDate value for this ClaimsSummaryItem.
     * 
     * @return processedDate
     */
    public java.util.Calendar getProcessedDate() {
        return processedDate;
    }


    /**
     * Sets the processedDate value for this ClaimsSummaryItem.
     * 
     * @param processedDate
     */
    public void setProcessedDate(java.util.Calendar processedDate) {
        this.processedDate = processedDate;
    }


    /**
     * Gets the sensitiveClaim value for this ClaimsSummaryItem.
     * 
     * @return sensitiveClaim
     */
    public java.lang.Boolean getSensitiveClaim() {
        return sensitiveClaim;
    }


    /**
     * Sets the sensitiveClaim value for this ClaimsSummaryItem.
     * 
     * @param sensitiveClaim
     */
    public void setSensitiveClaim(java.lang.Boolean sensitiveClaim) {
        this.sensitiveClaim = sensitiveClaim;
    }


    /**
     * Gets the statusType value for this ClaimsSummaryItem.
     * 
     * @return statusType
     */
    public com.avalon.esb.servicehelpers.ClaimStatusType getStatusType() {
        return statusType;
    }


    /**
     * Sets the statusType value for this ClaimsSummaryItem.
     * 
     * @param statusType
     */
    public void setStatusType(com.avalon.esb.servicehelpers.ClaimStatusType statusType) {
        this.statusType = statusType;
    }


    /**
     * Gets the insurancePaidAmount value for this ClaimsSummaryItem.
     * 
     * @return insurancePaidAmount
     */
    public java.math.BigDecimal getInsurancePaidAmount() {
        return insurancePaidAmount;
    }


    /**
     * Sets the insurancePaidAmount value for this ClaimsSummaryItem.
     * 
     * @param insurancePaidAmount
     */
    public void setInsurancePaidAmount(java.math.BigDecimal insurancePaidAmount) {
        this.insurancePaidAmount = insurancePaidAmount;
    }


    /**
     * Gets the remittanceAmount value for this ClaimsSummaryItem.
     * 
     * @return remittanceAmount
     */
    public java.math.BigDecimal getRemittanceAmount() {
        return remittanceAmount;
    }


    /**
     * Sets the remittanceAmount value for this ClaimsSummaryItem.
     * 
     * @param remittanceAmount
     */
    public void setRemittanceAmount(java.math.BigDecimal remittanceAmount) {
        this.remittanceAmount = remittanceAmount;
    }


    /**
     * Gets the patientLiabilityAmount value for this ClaimsSummaryItem.
     * 
     * @return patientLiabilityAmount
     */
    public java.math.BigDecimal getPatientLiabilityAmount() {
        return patientLiabilityAmount;
    }


    /**
     * Sets the patientLiabilityAmount value for this ClaimsSummaryItem.
     * 
     * @param patientLiabilityAmount
     */
    public void setPatientLiabilityAmount(java.math.BigDecimal patientLiabilityAmount) {
        this.patientLiabilityAmount = patientLiabilityAmount;
    }


    /**
     * Gets the checkIssueDate value for this ClaimsSummaryItem.
     * 
     * @return checkIssueDate
     */
    public java.util.Calendar getCheckIssueDate() {
        return checkIssueDate;
    }


    /**
     * Sets the checkIssueDate value for this ClaimsSummaryItem.
     * 
     * @param checkIssueDate
     */
    public void setCheckIssueDate(java.util.Calendar checkIssueDate) {
        this.checkIssueDate = checkIssueDate;
    }


    /**
     * Gets the providerID value for this ClaimsSummaryItem.
     * 
     * @return providerID
     */
    public java.lang.String getProviderID() {
        return providerID;
    }


    /**
     * Sets the providerID value for this ClaimsSummaryItem.
     * 
     * @param providerID
     */
    public void setProviderID(java.lang.String providerID) {
        this.providerID = providerID;
    }


    /**
     * Gets the paymentMethod value for this ClaimsSummaryItem.
     * 
     * @return paymentMethod
     */
    public java.lang.String getPaymentMethod() {
        return paymentMethod;
    }


    /**
     * Sets the paymentMethod value for this ClaimsSummaryItem.
     * 
     * @param paymentMethod
     */
    public void setPaymentMethod(java.lang.String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }


    /**
     * Gets the billType value for this ClaimsSummaryItem.
     * 
     * @return billType
     */
    public com.avalon.esb.servicehelpers.Code getBillType() {
        return billType;
    }


    /**
     * Sets the billType value for this ClaimsSummaryItem.
     * 
     * @param billType
     */
    public void setBillType(com.avalon.esb.servicehelpers.Code billType) {
        this.billType = billType;
    }


    /**
     * Gets the claimNotFound value for this ClaimsSummaryItem.
     * 
     * @return claimNotFound
     */
    public java.lang.Boolean getClaimNotFound() {
        return claimNotFound;
    }


    /**
     * Sets the claimNotFound value for this ClaimsSummaryItem.
     * 
     * @param claimNotFound
     */
    public void setClaimNotFound(java.lang.Boolean claimNotFound) {
        this.claimNotFound = claimNotFound;
    }


    /**
     * Gets the claimLinesSummary value for this ClaimsSummaryItem.
     * 
     * @return claimLinesSummary
     */
    public com.avalon.esb.servicehelpers.ClaimLinesSummary getClaimLinesSummary() {
        return claimLinesSummary;
    }


    /**
     * Sets the claimLinesSummary value for this ClaimsSummaryItem.
     * 
     * @param claimLinesSummary
     */
    public void setClaimLinesSummary(com.avalon.esb.servicehelpers.ClaimLinesSummary claimLinesSummary) {
        this.claimLinesSummary = claimLinesSummary;
    }


    /**
     * Gets the outOfAreaClaimsSummaryItemList value for this ClaimsSummaryItem.
     * 
     * @return outOfAreaClaimsSummaryItemList
     */
    public com.avalon.esb.servicehelpers.OutOfAreaClaimsSummaryItem[] getOutOfAreaClaimsSummaryItemList() {
        return outOfAreaClaimsSummaryItemList;
    }


    /**
     * Sets the outOfAreaClaimsSummaryItemList value for this ClaimsSummaryItem.
     * 
     * @param outOfAreaClaimsSummaryItemList
     */
    public void setOutOfAreaClaimsSummaryItemList(com.avalon.esb.servicehelpers.OutOfAreaClaimsSummaryItem[] outOfAreaClaimsSummaryItemList) {
        this.outOfAreaClaimsSummaryItemList = outOfAreaClaimsSummaryItemList;
    }

    public com.avalon.esb.servicehelpers.OutOfAreaClaimsSummaryItem getOutOfAreaClaimsSummaryItemList(int i) {
        return this.outOfAreaClaimsSummaryItemList[i];
    }

    public void setOutOfAreaClaimsSummaryItemList(int i, com.avalon.esb.servicehelpers.OutOfAreaClaimsSummaryItem _value) {
        this.outOfAreaClaimsSummaryItemList[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ClaimsSummaryItem)) return false;
        ClaimsSummaryItem other = (ClaimsSummaryItem) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.claimNumber==null && other.getClaimNumber()==null) || 
             (this.claimNumber!=null &&
              this.claimNumber.equals(other.getClaimNumber()))) &&
            ((this.claimDateOfService==null && other.getClaimDateOfService()==null) || 
             (this.claimDateOfService!=null &&
              this.claimDateOfService.equals(other.getClaimDateOfService()))) &&
            this.numberOfServiceLines == other.getNumberOfServiceLines() &&
            ((this.patientAccountNumber==null && other.getPatientAccountNumber()==null) || 
             (this.patientAccountNumber!=null &&
              this.patientAccountNumber.equals(other.getPatientAccountNumber()))) &&
            ((this.baseProvider==null && other.getBaseProvider()==null) || 
             (this.baseProvider!=null &&
              this.baseProvider.equals(other.getBaseProvider()))) &&
            ((this.billingProviderName==null && other.getBillingProviderName()==null) || 
             (this.billingProviderName!=null &&
              this.billingProviderName.equals(other.getBillingProviderName()))) &&
            ((this.claimPaidTo==null && other.getClaimPaidTo()==null) || 
             (this.claimPaidTo!=null &&
              this.claimPaidTo.equals(other.getClaimPaidTo()))) &&
            ((this.claimPaidToDescription==null && other.getClaimPaidToDescription()==null) || 
             (this.claimPaidToDescription!=null &&
              this.claimPaidToDescription.equals(other.getClaimPaidToDescription()))) &&
            ((this.totalClaimChargeAmount==null && other.getTotalClaimChargeAmount()==null) || 
             (this.totalClaimChargeAmount!=null &&
              this.totalClaimChargeAmount.equals(other.getTotalClaimChargeAmount()))) &&
            ((this.processedDate==null && other.getProcessedDate()==null) || 
             (this.processedDate!=null &&
              this.processedDate.equals(other.getProcessedDate()))) &&
            ((this.sensitiveClaim==null && other.getSensitiveClaim()==null) || 
             (this.sensitiveClaim!=null &&
              this.sensitiveClaim.equals(other.getSensitiveClaim()))) &&
            ((this.statusType==null && other.getStatusType()==null) || 
             (this.statusType!=null &&
              this.statusType.equals(other.getStatusType()))) &&
            ((this.insurancePaidAmount==null && other.getInsurancePaidAmount()==null) || 
             (this.insurancePaidAmount!=null &&
              this.insurancePaidAmount.equals(other.getInsurancePaidAmount()))) &&
            ((this.remittanceAmount==null && other.getRemittanceAmount()==null) || 
             (this.remittanceAmount!=null &&
              this.remittanceAmount.equals(other.getRemittanceAmount()))) &&
            ((this.patientLiabilityAmount==null && other.getPatientLiabilityAmount()==null) || 
             (this.patientLiabilityAmount!=null &&
              this.patientLiabilityAmount.equals(other.getPatientLiabilityAmount()))) &&
            ((this.checkIssueDate==null && other.getCheckIssueDate()==null) || 
             (this.checkIssueDate!=null &&
              this.checkIssueDate.equals(other.getCheckIssueDate()))) &&
            ((this.providerID==null && other.getProviderID()==null) || 
             (this.providerID!=null &&
              this.providerID.equals(other.getProviderID()))) &&
            ((this.paymentMethod==null && other.getPaymentMethod()==null) || 
             (this.paymentMethod!=null &&
              this.paymentMethod.equals(other.getPaymentMethod()))) &&
            ((this.billType==null && other.getBillType()==null) || 
             (this.billType!=null &&
              this.billType.equals(other.getBillType()))) &&
            ((this.claimNotFound==null && other.getClaimNotFound()==null) || 
             (this.claimNotFound!=null &&
              this.claimNotFound.equals(other.getClaimNotFound()))) &&
            ((this.claimLinesSummary==null && other.getClaimLinesSummary()==null) || 
             (this.claimLinesSummary!=null &&
              this.claimLinesSummary.equals(other.getClaimLinesSummary()))) &&
            ((this.outOfAreaClaimsSummaryItemList==null && other.getOutOfAreaClaimsSummaryItemList()==null) || 
             (this.outOfAreaClaimsSummaryItemList!=null &&
              java.util.Arrays.equals(this.outOfAreaClaimsSummaryItemList, other.getOutOfAreaClaimsSummaryItemList())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getClaimNumber() != null) {
            _hashCode += getClaimNumber().hashCode();
        }
        if (getClaimDateOfService() != null) {
            _hashCode += getClaimDateOfService().hashCode();
        }
        _hashCode += getNumberOfServiceLines();
        if (getPatientAccountNumber() != null) {
            _hashCode += getPatientAccountNumber().hashCode();
        }
        if (getBaseProvider() != null) {
            _hashCode += getBaseProvider().hashCode();
        }
        if (getBillingProviderName() != null) {
            _hashCode += getBillingProviderName().hashCode();
        }
        if (getClaimPaidTo() != null) {
            _hashCode += getClaimPaidTo().hashCode();
        }
        if (getClaimPaidToDescription() != null) {
            _hashCode += getClaimPaidToDescription().hashCode();
        }
        if (getTotalClaimChargeAmount() != null) {
            _hashCode += getTotalClaimChargeAmount().hashCode();
        }
        if (getProcessedDate() != null) {
            _hashCode += getProcessedDate().hashCode();
        }
        if (getSensitiveClaim() != null) {
            _hashCode += getSensitiveClaim().hashCode();
        }
        if (getStatusType() != null) {
            _hashCode += getStatusType().hashCode();
        }
        if (getInsurancePaidAmount() != null) {
            _hashCode += getInsurancePaidAmount().hashCode();
        }
        if (getRemittanceAmount() != null) {
            _hashCode += getRemittanceAmount().hashCode();
        }
        if (getPatientLiabilityAmount() != null) {
            _hashCode += getPatientLiabilityAmount().hashCode();
        }
        if (getCheckIssueDate() != null) {
            _hashCode += getCheckIssueDate().hashCode();
        }
        if (getProviderID() != null) {
            _hashCode += getProviderID().hashCode();
        }
        if (getPaymentMethod() != null) {
            _hashCode += getPaymentMethod().hashCode();
        }
        if (getBillType() != null) {
            _hashCode += getBillType().hashCode();
        }
        if (getClaimNotFound() != null) {
            _hashCode += getClaimNotFound().hashCode();
        }
        if (getClaimLinesSummary() != null) {
            _hashCode += getClaimLinesSummary().hashCode();
        }
        if (getOutOfAreaClaimsSummaryItemList() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getOutOfAreaClaimsSummaryItemList());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getOutOfAreaClaimsSummaryItemList(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ClaimsSummaryItem.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://servicehelpers.esb.avalon.com", "ClaimsSummaryItem"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("claimNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("", "claimNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("claimDateOfService");
        elemField.setXmlName(new javax.xml.namespace.QName("", "claimDateOfService"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://servicehelpers.esb.avalon.com", "PeriodDate"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numberOfServiceLines");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numberOfServiceLines"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("patientAccountNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("", "patientAccountNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("baseProvider");
        elemField.setXmlName(new javax.xml.namespace.QName("", "baseProvider"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://servicehelpers.esb.avalon.com", "Provider"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("billingProviderName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "billingProviderName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("claimPaidTo");
        elemField.setXmlName(new javax.xml.namespace.QName("", "claimPaidTo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("claimPaidToDescription");
        elemField.setXmlName(new javax.xml.namespace.QName("", "claimPaidToDescription"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("totalClaimChargeAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("", "totalClaimChargeAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("processedDate");
        elemField.setXmlName(new javax.xml.namespace.QName("", "processedDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sensitiveClaim");
        elemField.setXmlName(new javax.xml.namespace.QName("", "sensitiveClaim"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("statusType");
        elemField.setXmlName(new javax.xml.namespace.QName("", "statusType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://servicehelpers.esb.avalon.com", "ClaimStatusType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("insurancePaidAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("", "insurancePaidAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("remittanceAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("", "remittanceAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("patientLiabilityAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("", "patientLiabilityAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("checkIssueDate");
        elemField.setXmlName(new javax.xml.namespace.QName("", "checkIssueDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("providerID");
        elemField.setXmlName(new javax.xml.namespace.QName("", "providerID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("paymentMethod");
        elemField.setXmlName(new javax.xml.namespace.QName("", "paymentMethod"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("billType");
        elemField.setXmlName(new javax.xml.namespace.QName("", "billType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://servicehelpers.esb.avalon.com", "Code"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("claimNotFound");
        elemField.setXmlName(new javax.xml.namespace.QName("", "claimNotFound"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("claimLinesSummary");
        elemField.setXmlName(new javax.xml.namespace.QName("", "claimLinesSummary"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://servicehelpers.esb.avalon.com", "ClaimLinesSummary"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("outOfAreaClaimsSummaryItemList");
        elemField.setXmlName(new javax.xml.namespace.QName("", "outOfAreaClaimsSummaryItemList"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://servicehelpers.esb.avalon.com", "OutOfAreaClaimsSummaryItem"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
