/**
 * ProcedureCodeDetail.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.avalon.esb.servicehelpers;

public class ProcedureCodeDetail  extends com.avalon.esb.servicehelpers.Code  implements java.io.Serializable {
    private java.util.Calendar procedureDate;

    private com.avalon.esb.servicehelpers.Code icdVersion;

    public ProcedureCodeDetail() {
    }

    public ProcedureCodeDetail(
           java.lang.String code,
           java.lang.String description,
           java.util.Calendar procedureDate,
           com.avalon.esb.servicehelpers.Code icdVersion) {
        super(
            code,
            description);
        this.procedureDate = procedureDate;
        this.icdVersion = icdVersion;
    }


    /**
     * Gets the procedureDate value for this ProcedureCodeDetail.
     * 
     * @return procedureDate
     */
    public java.util.Calendar getProcedureDate() {
        return procedureDate;
    }


    /**
     * Sets the procedureDate value for this ProcedureCodeDetail.
     * 
     * @param procedureDate
     */
    public void setProcedureDate(java.util.Calendar procedureDate) {
        this.procedureDate = procedureDate;
    }


    /**
     * Gets the icdVersion value for this ProcedureCodeDetail.
     * 
     * @return icdVersion
     */
    public com.avalon.esb.servicehelpers.Code getIcdVersion() {
        return icdVersion;
    }


    /**
     * Sets the icdVersion value for this ProcedureCodeDetail.
     * 
     * @param icdVersion
     */
    public void setIcdVersion(com.avalon.esb.servicehelpers.Code icdVersion) {
        this.icdVersion = icdVersion;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ProcedureCodeDetail)) return false;
        ProcedureCodeDetail other = (ProcedureCodeDetail) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.procedureDate==null && other.getProcedureDate()==null) || 
             (this.procedureDate!=null &&
              this.procedureDate.equals(other.getProcedureDate()))) &&
            ((this.icdVersion==null && other.getIcdVersion()==null) || 
             (this.icdVersion!=null &&
              this.icdVersion.equals(other.getIcdVersion())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getProcedureDate() != null) {
            _hashCode += getProcedureDate().hashCode();
        }
        if (getIcdVersion() != null) {
            _hashCode += getIcdVersion().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ProcedureCodeDetail.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://servicehelpers.esb.avalon.com", "ProcedureCodeDetail"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("procedureDate");
        elemField.setXmlName(new javax.xml.namespace.QName("", "procedureDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("icdVersion");
        elemField.setXmlName(new javax.xml.namespace.QName("", "icdVersion"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://servicehelpers.esb.avalon.com", "Code"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
