/**
 * Patient.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.avalon.esb.servicehelpers;

public class Patient  extends com.avalon.esb.servicehelpers.Person  implements java.io.Serializable {
    private com.avalon.esb.servicehelpers.SubscriberId subscriberId;

    private java.lang.String cesMemberNumber;

    private com.avalon.esb.servicehelpers.Code relationshipToSubscriber;

    private java.util.Calendar dateOfDeath;

    private com.avalon.esb.servicehelpers.CoverageInformation coverageInformation;

    private boolean outOfAreaPatient;
    
    private boolean student;
    
    private boolean handicapped;
    
    
    
    /**
	 * @return the handicapped
	 */
	public boolean isHandicapped() {
		return handicapped;
	}

	/**
	 * @param handicapped the handicapped to set
	 */
	public void setHandicapped(boolean handicapped) {
		this.handicapped = handicapped;
	}

	/**
	 * @return the pregnant
	 */
	public boolean isPregnant() {
		return pregnant;
	}

	/**
	 * @param pregnant the pregnant to set
	 */
	public void setPregnant(boolean pregnant) {
		this.pregnant = pregnant;
	}

	/**
	 * @return the weight
	 */
	public int getWeight() {
		return weight;
	}

	/**
	 * @param weight the weight to set
	 */
	public void setWeight(int weight) {
		this.weight = weight;
	}

	/**
	 * @return the customNetwork
	 */
	public boolean isCustomNetwork() {
		return customNetwork;
	}

	/**
	 * @param customNetwork the customNetwork to set
	 */
	public void setCustomNetwork(boolean customNetwork) {
		this.customNetwork = customNetwork;
	}

	/**
	 * @return the underAge
	 */
	public boolean isUnderAge() {
		return underAge;
	}

	/**
	 * @param underAge the underAge to set
	 */
	public void setUnderAge(boolean underAge) {
		this.underAge = underAge;
	}

	/**
	 * @return the permissionsChangedSinceLastLogin
	 */
	public boolean isPermissionsChangedSinceLastLogin() {
		return permissionsChangedSinceLastLogin;
	}

	/**
	 * @param permissionsChangedSinceLastLogin the permissionsChangedSinceLastLogin to set
	 */
	public void setPermissionsChangedSinceLastLogin(
			boolean permissionsChangedSinceLastLogin) {
		this.permissionsChangedSinceLastLogin = permissionsChangedSinceLastLogin;
	}

	private boolean pregnant;
    
    private int weight;
    
    private boolean  customNetwork;
    
    private boolean underAge;
    
    private boolean permissionsChangedSinceLastLogin;
    
    
    
    
   

    
    
    
    
    
    
    /**
	 * @return the student
	 */
	public boolean isStudent() {
		return student;
	}

	/**
	 * @param student the student to set
	 */
	public void setStudent(boolean student) {
		this.student = student;
	}

	

    public Patient() {
    }

    public Patient(
           com.avalon.esb.servicehelpers.Name name,
           com.avalon.esb.servicehelpers.Code gender,
           java.util.Calendar dateOfBirth,
           com.avalon.esb.servicehelpers.SubscriberId subscriberId,
           java.lang.String cesMemberNumber,
           com.avalon.esb.servicehelpers.Code relationshipToSubscriber,
           java.util.Calendar dateOfDeath,
           com.avalon.esb.servicehelpers.CoverageInformation coverageInformation,
           boolean outOfAreaPatient) {
        super(
            name,
            gender,
            dateOfBirth);
        this.subscriberId = subscriberId;
        this.cesMemberNumber = cesMemberNumber;
        this.relationshipToSubscriber = relationshipToSubscriber;
        this.dateOfDeath = dateOfDeath;
        this.coverageInformation = coverageInformation;
        this.outOfAreaPatient = outOfAreaPatient;
    }


    /**
     * Gets the subscriberId value for this Patient.
     * 
     * @return subscriberId
     */
    public com.avalon.esb.servicehelpers.SubscriberId getSubscriberId() {
        return subscriberId;
    }


    /**
     * Sets the subscriberId value for this Patient.
     * 
     * @param subscriberId
     */
    public void setSubscriberId(com.avalon.esb.servicehelpers.SubscriberId subscriberId) {
        this.subscriberId = subscriberId;
    }


    /**
     * Gets the cesMemberNumber value for this Patient.
     * 
     * @return cesMemberNumber
     */
    public java.lang.String getCesMemberNumber() {
        return cesMemberNumber;
    }


    /**
     * Sets the cesMemberNumber value for this Patient.
     * 
     * @param cesMemberNumber
     */
    public void setCesMemberNumber(java.lang.String cesMemberNumber) {
        this.cesMemberNumber = cesMemberNumber;
    }


    /**
     * Gets the relationshipToSubscriber value for this Patient.
     * 
     * @return relationshipToSubscriber
     */
    public com.avalon.esb.servicehelpers.Code getRelationshipToSubscriber() {
        return relationshipToSubscriber;
    }


    /**
     * Sets the relationshipToSubscriber value for this Patient.
     * 
     * @param relationshipToSubscriber
     */
    public void setRelationshipToSubscriber(com.avalon.esb.servicehelpers.Code relationshipToSubscriber) {
        this.relationshipToSubscriber = relationshipToSubscriber;
    }


    /**
     * Gets the dateOfDeath value for this Patient.
     * 
     * @return dateOfDeath
     */
    public java.util.Calendar getDateOfDeath() {
        return dateOfDeath;
    }


    /**
     * Sets the dateOfDeath value for this Patient.
     * 
     * @param dateOfDeath
     */
    public void setDateOfDeath(java.util.Calendar dateOfDeath) {
        this.dateOfDeath = dateOfDeath;
    }


    /**
     * Gets the coverageInformation value for this Patient.
     * 
     * @return coverageInformation
     */
    public com.avalon.esb.servicehelpers.CoverageInformation getCoverageInformation() {
        return coverageInformation;
    }


    /**
     * Sets the coverageInformation value for this Patient.
     * 
     * @param coverageInformation
     */
    public void setCoverageInformation(com.avalon.esb.servicehelpers.CoverageInformation coverageInformation) {
        this.coverageInformation = coverageInformation;
    }


    /**
     * Gets the outOfAreaPatient value for this Patient.
     * 
     * @return outOfAreaPatient
     */
    public boolean isOutOfAreaPatient() {
        return outOfAreaPatient;
    }


    /**
     * Sets the outOfAreaPatient value for this Patient.
     * 
     * @param outOfAreaPatient
     */
    public void setOutOfAreaPatient(boolean outOfAreaPatient) {
        this.outOfAreaPatient = outOfAreaPatient;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Patient)) return false;
        Patient other = (Patient) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.subscriberId==null && other.getSubscriberId()==null) || 
             (this.subscriberId!=null &&
              this.subscriberId.equals(other.getSubscriberId()))) &&
            ((this.cesMemberNumber==null && other.getCesMemberNumber()==null) || 
             (this.cesMemberNumber!=null &&
              this.cesMemberNumber.equals(other.getCesMemberNumber()))) &&
            ((this.relationshipToSubscriber==null && other.getRelationshipToSubscriber()==null) || 
             (this.relationshipToSubscriber!=null &&
              this.relationshipToSubscriber.equals(other.getRelationshipToSubscriber()))) &&
            ((this.dateOfDeath==null && other.getDateOfDeath()==null) || 
             (this.dateOfDeath!=null &&
              this.dateOfDeath.equals(other.getDateOfDeath()))) &&
            ((this.coverageInformation==null && other.getCoverageInformation()==null) || 
             (this.coverageInformation!=null &&
              this.coverageInformation.equals(other.getCoverageInformation()))) &&
            this.outOfAreaPatient == other.isOutOfAreaPatient();
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getSubscriberId() != null) {
            _hashCode += getSubscriberId().hashCode();
        }
        if (getCesMemberNumber() != null) {
            _hashCode += getCesMemberNumber().hashCode();
        }
        if (getRelationshipToSubscriber() != null) {
            _hashCode += getRelationshipToSubscriber().hashCode();
        }
        if (getDateOfDeath() != null) {
            _hashCode += getDateOfDeath().hashCode();
        }
        if (getCoverageInformation() != null) {
            _hashCode += getCoverageInformation().hashCode();
        }
        _hashCode += (isOutOfAreaPatient() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Patient.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://servicehelpers.esb.avalon.com", "Patient"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("subscriberId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "subscriberId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://servicehelpers.esb.avalon.com", "SubscriberId"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cesMemberNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cesMemberNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("relationshipToSubscriber");
        elemField.setXmlName(new javax.xml.namespace.QName("", "relationshipToSubscriber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://servicehelpers.esb.avalon.com", "Code"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dateOfDeath");
        elemField.setXmlName(new javax.xml.namespace.QName("", "dateOfDeath"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("coverageInformation");
        elemField.setXmlName(new javax.xml.namespace.QName("", "coverageInformation"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://servicehelpers.esb.avalon.com", "CoverageInformation"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("outOfAreaPatient");
        elemField.setXmlName(new javax.xml.namespace.QName("", "outOfAreaPatient"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
