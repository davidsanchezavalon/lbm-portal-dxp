
package com.avalon.esb.servicehelpers;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getClaimDetails complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getClaimDetails"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="claimDetailSearchCriteria" type="{http://servicehelpers.esb.avalon.com}ClaimDetailSearchCriteria" minOccurs="0"/&gt;
 *         &lt;element name="claimNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="searchFilter" type="{http://servicehelpers.esb.avalon.com}ClaimSearchFilter" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getClaimDetails", propOrder = {
    "claimDetailSearchCriteria",
    "claimNumber",
    "searchFilter"
})
public class GetClaimDetails {

    protected ClaimDetailSearchCriteria claimDetailSearchCriteria;
    protected String claimNumber;
    protected ClaimSearchFilter searchFilter;

    /**
     * Gets the value of the claimDetailSearchCriteria property.
     * 
     * @return
     *     possible object is
     *     {@link ClaimDetailSearchCriteria }
     *     
     */
    public ClaimDetailSearchCriteria getClaimDetailSearchCriteria() {
        return claimDetailSearchCriteria;
    }

    /**
     * Sets the value of the claimDetailSearchCriteria property.
     * 
     * @param value
     *     allowed object is
     *     {@link ClaimDetailSearchCriteria }
     *     
     */
    public void setClaimDetailSearchCriteria(ClaimDetailSearchCriteria value) {
        this.claimDetailSearchCriteria = value;
    }

    /**
     * Gets the value of the claimNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClaimNumber() {
        return claimNumber;
    }

    /**
     * Sets the value of the claimNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClaimNumber(String value) {
        this.claimNumber = value;
    }

    /**
     * Gets the value of the searchFilter property.
     * 
     * @return
     *     possible object is
     *     {@link ClaimSearchFilter }
     *     
     */
    public ClaimSearchFilter getSearchFilter() {
        return searchFilter;
    }

    /**
     * Sets the value of the searchFilter property.
     * 
     * @param value
     *     allowed object is
     *     {@link ClaimSearchFilter }
     *     
     */
    public void setSearchFilter(ClaimSearchFilter value) {
        this.searchFilter = value;
    }

}
