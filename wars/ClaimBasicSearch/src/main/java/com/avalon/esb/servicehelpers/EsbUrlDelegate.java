/**
 * Description
 *		This file methods to get values from the properties file for Claim Status.
 *
 * CHANGE History
 * 		Version 1.0
 * 			Initial version
 *  	Version 1.1			03/07/2017
 *  		Use the constant for the properties file.
 *  
 */

package com.avalon.esb.servicehelpers;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;

import com.avalon.claimsearch.fo.ClaimsSearchConstants;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.StringPool;

public class EsbUrlDelegate {
	
	private static final Log log = LogFactoryUtil.getLog(EsbUrlDelegate.class.getName());

    private static EsbUrlDelegate esbUrlDelegate = null;
    private static Properties properties = null;

    public static EsbUrlDelegate getEsbUrlDelegate() {
        return esbUrlDelegate;
    }

    

	 /**
	     * Fetching the esb end point urls from the properties object
	     * Setting to the esb end point urls in the map object
	     * 
	     */
    public Map<String, String> getEsbUrl() {
		ConcurrentHashMap<String, String> urls = new ConcurrentHashMap<String, String>();
		try {
		    String serverInstance = System.getProperty("serverInstance");
		    log.info("ServerInstanceAtEsbDelegate :" + serverInstance);
		    log.info( "memberBenefits : " + properties.getProperty(serverInstance + ".getMemberBenefits.endpoint"));
		    log.info("findClaims : " + properties.getProperty(serverInstance + ".searchClaim.endpoint"));
		    urls.put("memberBenefits", properties.getProperty(serverInstance + ".getMemberBenefits.endpoint"));
		    urls.put("findClaims", properties.getProperty(serverInstance + ".searchClaim.endpoint"));
		    
		} catch (Exception exception) {
			log.error("Exception in getEsbUrl method",exception);
		}
		return urls;
    }

    /**
	 * To load the common.properties files from the server bin directory and to fetch the esb-end point urls.
	 * @return
	 */
    static {
		if (properties == null  || properties.size()==0) {
		    properties = new Properties();
		    esbUrlDelegate = new EsbUrlDelegate();
		    String liferay_home = System.getProperty("LIFERAY_HOME");
			String propertyLocation = liferay_home +StringPool.SLASH + ClaimsSearchConstants.PROPERTIES_FILE;
		    try {
				properties.load(new FileInputStream(new File(propertyLocation)));
				log.info("EsbUrlDelegate for Claim Status::propertyLocation: " + propertyLocation);
		    } catch (FileNotFoundException exception) {
		    	log.error("FileNotFoundException in static block",exception);
		    } catch (IOException exception) {
		    	log.error("IOException in static block",exception);
		    }
		}
    }
}
