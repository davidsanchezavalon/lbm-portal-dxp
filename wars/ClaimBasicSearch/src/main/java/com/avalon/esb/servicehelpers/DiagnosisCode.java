/**
 * DiagnosisCode.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.avalon.esb.servicehelpers;

public class DiagnosisCode  extends com.avalon.esb.servicehelpers.Code  implements java.io.Serializable {
    private com.avalon.esb.servicehelpers.Code presentOnAdmissionCode;

    private java.lang.Boolean presentOnAdmission;

    private com.avalon.esb.servicehelpers.Code qualifier;

    public DiagnosisCode() {
    }

    public DiagnosisCode(
           java.lang.String code,
           java.lang.String description,
           com.avalon.esb.servicehelpers.Code presentOnAdmissionCode,
           java.lang.Boolean presentOnAdmission,
           com.avalon.esb.servicehelpers.Code qualifier) {
        super(
            code,
            description);
        this.presentOnAdmissionCode = presentOnAdmissionCode;
        this.presentOnAdmission = presentOnAdmission;
        this.qualifier = qualifier;
    }


    /**
     * Gets the presentOnAdmissionCode value for this DiagnosisCode.
     * 
     * @return presentOnAdmissionCode
     */
    public com.avalon.esb.servicehelpers.Code getPresentOnAdmissionCode() {
        return presentOnAdmissionCode;
    }


    /**
     * Sets the presentOnAdmissionCode value for this DiagnosisCode.
     * 
     * @param presentOnAdmissionCode
     */
    public void setPresentOnAdmissionCode(com.avalon.esb.servicehelpers.Code presentOnAdmissionCode) {
        this.presentOnAdmissionCode = presentOnAdmissionCode;
    }


    /**
     * Gets the presentOnAdmission value for this DiagnosisCode.
     * 
     * @return presentOnAdmission
     */
    public java.lang.Boolean getPresentOnAdmission() {
        return presentOnAdmission;
    }


    /**
     * Sets the presentOnAdmission value for this DiagnosisCode.
     * 
     * @param presentOnAdmission
     */
    public void setPresentOnAdmission(java.lang.Boolean presentOnAdmission) {
        this.presentOnAdmission = presentOnAdmission;
    }


    /**
     * Gets the qualifier value for this DiagnosisCode.
     * 
     * @return qualifier
     */
    public com.avalon.esb.servicehelpers.Code getQualifier() {
        return qualifier;
    }


    /**
     * Sets the qualifier value for this DiagnosisCode.
     * 
     * @param qualifier
     */
    public void setQualifier(com.avalon.esb.servicehelpers.Code qualifier) {
        this.qualifier = qualifier;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof DiagnosisCode)) return false;
        DiagnosisCode other = (DiagnosisCode) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.presentOnAdmissionCode==null && other.getPresentOnAdmissionCode()==null) || 
             (this.presentOnAdmissionCode!=null &&
              this.presentOnAdmissionCode.equals(other.getPresentOnAdmissionCode()))) &&
            ((this.presentOnAdmission==null && other.getPresentOnAdmission()==null) || 
             (this.presentOnAdmission!=null &&
              this.presentOnAdmission.equals(other.getPresentOnAdmission()))) &&
            ((this.qualifier==null && other.getQualifier()==null) || 
             (this.qualifier!=null &&
              this.qualifier.equals(other.getQualifier())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getPresentOnAdmissionCode() != null) {
            _hashCode += getPresentOnAdmissionCode().hashCode();
        }
        if (getPresentOnAdmission() != null) {
            _hashCode += getPresentOnAdmission().hashCode();
        }
        if (getQualifier() != null) {
            _hashCode += getQualifier().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(DiagnosisCode.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://servicehelpers.esb.avalon.com", "DiagnosisCode"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("presentOnAdmissionCode");
        elemField.setXmlName(new javax.xml.namespace.QName("", "presentOnAdmissionCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://servicehelpers.esb.avalon.com", "Code"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("presentOnAdmission");
        elemField.setXmlName(new javax.xml.namespace.QName("", "presentOnAdmission"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("qualifier");
        elemField.setXmlName(new javax.xml.namespace.QName("", "qualifier"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://servicehelpers.esb.avalon.com", "Code"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
