/**
 * ClaimSearchCriteria.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.avalon.esb.servicehelpers;

public class ClaimSearchCriteria  implements java.io.Serializable {
  /*  private java.util.Calendar serviceDateFrom;

    private java.util.Calendar serviceDateTo;*/
    
    
    private java.lang.String serviceDateFrom;

    private java.lang.String serviceDateTo;

    private java.math.BigDecimal totalChargeFrom;

    private java.math.BigDecimal totalChargeTo;

    private java.lang.String searchClaimNumber;

    private com.avalon.esb.servicehelpers.ClaimStatusType claimStatus;

    private com.avalon.esb.servicehelpers.Code carrierCode;

    private com.avalon.esb.servicehelpers.HealthClaimSearchCriteria healthClaimSearchCriteria;

    public ClaimSearchCriteria() {
    }

    public ClaimSearchCriteria(
    		java.lang.String serviceDateFrom,
    		java.lang.String  serviceDateTo,
           java.math.BigDecimal totalChargeFrom,
           java.math.BigDecimal totalChargeTo,
           java.lang.String searchClaimNumber,
           com.avalon.esb.servicehelpers.ClaimStatusType claimStatus,
           com.avalon.esb.servicehelpers.Code carrierCode,
           com.avalon.esb.servicehelpers.HealthClaimSearchCriteria healthClaimSearchCriteria) {
           this.serviceDateFrom = serviceDateFrom;
           this.serviceDateTo = serviceDateTo;
           this.totalChargeFrom = totalChargeFrom;
           this.totalChargeTo = totalChargeTo;
           this.searchClaimNumber = searchClaimNumber;
           this.claimStatus = claimStatus;
           this.carrierCode = carrierCode;
           this.healthClaimSearchCriteria = healthClaimSearchCriteria;
    }


    /**
     * Gets the serviceDateFrom value for this ClaimSearchCriteria.
     * 
     * @return serviceDateFrom
     */
    public  java.lang.String  getServiceDateFrom() {
        return serviceDateFrom;
    }


    /**
     * Sets the serviceDateFrom value for this ClaimSearchCriteria.
     * 
     * @param serviceDateFrom
     */
    public void setServiceDateFrom( java.lang.String  serviceDateFrom) {
        this.serviceDateFrom = serviceDateFrom;
    }


    /**
     * Gets the serviceDateTo value for this ClaimSearchCriteria.
     * 
     * @return serviceDateTo
     */
    public  java.lang.String  getServiceDateTo() {
        return serviceDateTo;
    }


    /**
     * Sets the serviceDateTo value for this ClaimSearchCriteria.
     * 
     * @param serviceDateTo
     */
    public void setServiceDateTo( java.lang.String  serviceDateTo) {
        this.serviceDateTo = serviceDateTo;
    }


    /**
     * Gets the totalChargeFrom value for this ClaimSearchCriteria.
     * 
     * @return totalChargeFrom
     */
    public java.math.BigDecimal getTotalChargeFrom() {
        return totalChargeFrom;
    }


    /**
     * Sets the totalChargeFrom value for this ClaimSearchCriteria.
     * 
     * @param totalChargeFrom
     */
    public void setTotalChargeFrom(java.math.BigDecimal totalChargeFrom) {
        this.totalChargeFrom = totalChargeFrom;
    }


    /**
     * Gets the totalChargeTo value for this ClaimSearchCriteria.
     * 
     * @return totalChargeTo
     */
    public java.math.BigDecimal getTotalChargeTo() {
        return totalChargeTo;
    }


    /**
     * Sets the totalChargeTo value for this ClaimSearchCriteria.
     * 
     * @param totalChargeTo
     */
    public void setTotalChargeTo(java.math.BigDecimal totalChargeTo) {
        this.totalChargeTo = totalChargeTo;
    }


    /**
     * Gets the searchClaimNumber value for this ClaimSearchCriteria.
     * 
     * @return searchClaimNumber
     */
    public java.lang.String getSearchClaimNumber() {
        return searchClaimNumber;
    }


    /**
     * Sets the searchClaimNumber value for this ClaimSearchCriteria.
     * 
     * @param searchClaimNumber
     */
    public void setSearchClaimNumber(java.lang.String searchClaimNumber) {
        this.searchClaimNumber = searchClaimNumber;
    }


    /**
     * Gets the claimStatus value for this ClaimSearchCriteria.
     * 
     * @return claimStatus
     */
    public com.avalon.esb.servicehelpers.ClaimStatusType getClaimStatus() {
        return claimStatus;
    }


    /**
     * Sets the claimStatus value for this ClaimSearchCriteria.
     * 
     * @param claimStatus
     */
    public void setClaimStatus(com.avalon.esb.servicehelpers.ClaimStatusType claimStatus) {
        this.claimStatus = claimStatus;
    }


    /**
     * Gets the carrierCode value for this ClaimSearchCriteria.
     * 
     * @return carrierCode
     */
    public com.avalon.esb.servicehelpers.Code getCarrierCode() {
        return carrierCode;
    }


    /**
     * Sets the carrierCode value for this ClaimSearchCriteria.
     * 
     * @param carrierCode
     */
    public void setCarrierCode(com.avalon.esb.servicehelpers.Code carrierCode) {
        this.carrierCode = carrierCode;
    }


    /**
     * Gets the healthClaimSearchCriteria value for this ClaimSearchCriteria.
     * 
     * @return healthClaimSearchCriteria
     */
    public com.avalon.esb.servicehelpers.HealthClaimSearchCriteria getHealthClaimSearchCriteria() {
        return healthClaimSearchCriteria;
    }


    /**
     * Sets the healthClaimSearchCriteria value for this ClaimSearchCriteria.
     * 
     * @param healthClaimSearchCriteria
     */
    public void setHealthClaimSearchCriteria(com.avalon.esb.servicehelpers.HealthClaimSearchCriteria healthClaimSearchCriteria) {
        this.healthClaimSearchCriteria = healthClaimSearchCriteria;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ClaimSearchCriteria)) return false;
        ClaimSearchCriteria other = (ClaimSearchCriteria) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.serviceDateFrom==null && other.getServiceDateFrom()==null) || 
             (this.serviceDateFrom!=null &&
              this.serviceDateFrom.equals(other.getServiceDateFrom()))) &&
            ((this.serviceDateTo==null && other.getServiceDateTo()==null) || 
             (this.serviceDateTo!=null &&
              this.serviceDateTo.equals(other.getServiceDateTo()))) &&
            ((this.totalChargeFrom==null && other.getTotalChargeFrom()==null) || 
             (this.totalChargeFrom!=null &&
              this.totalChargeFrom.equals(other.getTotalChargeFrom()))) &&
            ((this.totalChargeTo==null && other.getTotalChargeTo()==null) || 
             (this.totalChargeTo!=null &&
              this.totalChargeTo.equals(other.getTotalChargeTo()))) &&
            ((this.searchClaimNumber==null && other.getSearchClaimNumber()==null) || 
             (this.searchClaimNumber!=null &&
              this.searchClaimNumber.equals(other.getSearchClaimNumber()))) &&
            ((this.claimStatus==null && other.getClaimStatus()==null) || 
             (this.claimStatus!=null &&
              this.claimStatus.equals(other.getClaimStatus()))) &&
            ((this.carrierCode==null && other.getCarrierCode()==null) || 
             (this.carrierCode!=null &&
              this.carrierCode.equals(other.getCarrierCode()))) &&
            ((this.healthClaimSearchCriteria==null && other.getHealthClaimSearchCriteria()==null) || 
             (this.healthClaimSearchCriteria!=null &&
              this.healthClaimSearchCriteria.equals(other.getHealthClaimSearchCriteria())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getServiceDateFrom() != null) {
            _hashCode += getServiceDateFrom().hashCode();
        }
        if (getServiceDateTo() != null) {
            _hashCode += getServiceDateTo().hashCode();
        }
        if (getTotalChargeFrom() != null) {
            _hashCode += getTotalChargeFrom().hashCode();
        }
        if (getTotalChargeTo() != null) {
            _hashCode += getTotalChargeTo().hashCode();
        }
        if (getSearchClaimNumber() != null) {
            _hashCode += getSearchClaimNumber().hashCode();
        }
        if (getClaimStatus() != null) {
            _hashCode += getClaimStatus().hashCode();
        }
        if (getCarrierCode() != null) {
            _hashCode += getCarrierCode().hashCode();
        }
        if (getHealthClaimSearchCriteria() != null) {
            _hashCode += getHealthClaimSearchCriteria().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ClaimSearchCriteria.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://servicehelpers.esb.avalon.com", "ClaimSearchCriteria"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("serviceDateFrom");
        elemField.setXmlName(new javax.xml.namespace.QName("", "serviceDateFrom"));
        //elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("serviceDateTo");
        elemField.setXmlName(new javax.xml.namespace.QName("", "serviceDateTo"));
        //elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("totalChargeFrom");
        elemField.setXmlName(new javax.xml.namespace.QName("", "totalChargeFrom"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("totalChargeTo");
        elemField.setXmlName(new javax.xml.namespace.QName("", "totalChargeTo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("searchClaimNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("", "searchClaimNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("claimStatus");
        elemField.setXmlName(new javax.xml.namespace.QName("", "claimStatus"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://servicehelpers.esb.avalon.com", "ClaimStatusType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("carrierCode");
        elemField.setXmlName(new javax.xml.namespace.QName("", "carrierCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://servicehelpers.esb.avalon.com", "Code"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("healthClaimSearchCriteria");
        elemField.setXmlName(new javax.xml.namespace.QName("", "healthClaimSearchCriteria"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://servicehelpers.esb.avalon.com", "HealthClaimSearchCriteria"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
