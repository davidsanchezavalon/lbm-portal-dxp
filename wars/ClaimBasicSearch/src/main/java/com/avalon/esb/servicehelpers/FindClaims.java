package com.avalon.esb.servicehelpers;

import java.io.Serializable;
import javax.xml.namespace.QName;
import org.apache.axis.description.ElementDesc;
import org.apache.axis.description.TypeDesc;
import org.apache.axis.encoding.Deserializer;
import org.apache.axis.encoding.Serializer;
import org.apache.axis.encoding.ser.BeanDeserializer;
import org.apache.axis.encoding.ser.BeanSerializer;

public class FindClaims implements Serializable {
	private ClaimStatusInboundReq claimStatusInboundReq;
  
	public FindClaims() {
	  
	  }

	public FindClaims(ClaimStatusInboundReq claimStatusInboundReq) {
		this.claimStatusInboundReq = claimStatusInboundReq;
	}
  
	public ClaimStatusInboundReq getClaimStatusInboundReq() {
		return this.claimStatusInboundReq;
	}
  
	public void setClaimStatusInboundReq(ClaimStatusInboundReq claimStatusInboundReq) {
		this.claimStatusInboundReq = claimStatusInboundReq;
	}
  
	private Object __equalsCalc = null;
  
	public synchronized boolean equals(Object obj) {
	    if (!(obj instanceof FindClaims)) {
	    	return false;
	    }
	    FindClaims other = (FindClaims)obj;
	    if (obj == null) {
	    	return false;
	    }
	    if (this == obj) {
	    	return true;
	    }
	    if (this.__equalsCalc != null) {
	    	return this.__equalsCalc == obj;
	    }
	    this.__equalsCalc = obj;
	    
	    boolean _equals = ((this.claimStatusInboundReq == null) && (other.getClaimStatusInboundReq() == null)) || ((this.claimStatusInboundReq != null) && (this.claimStatusInboundReq.equals(other.getClaimStatusInboundReq())));
	    
	    this.__equalsCalc = null;
	    return _equals;
	}
  
	private boolean __hashCodeCalc = false;
  
	public synchronized int hashCode() {
	    if (this.__hashCodeCalc) {
	    	return 0;
	    }
	    this.__hashCodeCalc = true;
	    int _hashCode = 1;
	    if (getClaimStatusInboundReq() != null) {
	    	_hashCode += getClaimStatusInboundReq().hashCode();
	    }
	    this.__hashCodeCalc = false;
	    return _hashCode;
 	}
  
	private static TypeDesc typeDesc = new TypeDesc(FindClaims.class, true);
  
	static {
	    typeDesc.setXmlType(new QName("http://servicehelpers.esb.avalon.com", "findClaims"));
	    ElementDesc elemField = new ElementDesc();
	    elemField.setFieldName("claimStatusInboundReq");
	    elemField.setXmlName(new QName("", "claimStatusInboundReq"));
	    elemField.setXmlType(new QName("http://servicehelpers.esb.avalon.com", "claimStatusInboundReq"));
	    elemField.setMinOccurs(0);
	    elemField.setNillable(false);
	    typeDesc.addFieldDesc(elemField);
	}
  
	public static TypeDesc getTypeDesc() {
		return typeDesc;
	}
  
	public static Serializer getSerializer(String mechType, Class _javaType, QName _xmlType) {
		return new BeanSerializer(_javaType, _xmlType, typeDesc);
	}
  
	public static Deserializer getDeserializer(String mechType, Class _javaType, QName _xmlType) {
		return new BeanDeserializer(_javaType, _xmlType, typeDesc);
	}
}
