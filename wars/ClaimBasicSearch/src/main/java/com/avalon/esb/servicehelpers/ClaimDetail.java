package com.avalon.esb.servicehelpers;

import java.io.Serializable;
import java.lang.reflect.Array;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Calendar;
import javax.xml.namespace.QName;
import org.apache.axis.description.ElementDesc;
import org.apache.axis.description.TypeDesc;
import org.apache.axis.encoding.Deserializer;
import org.apache.axis.encoding.Serializer;
import org.apache.axis.encoding.ser.BeanDeserializer;
import org.apache.axis.encoding.ser.BeanSerializer;

public class ClaimDetail implements Serializable {
	private Patient patient;
	private String aliasRpn;
	private String claimNumber;
	private String itsClaimNumber;
	private String documentationRequiredCode;
	private String documentationDescription;
	private String hostHomeIndicator;
	private String statusCode;
	private ClaimStatusType statusType;
	private Calendar dateClaimReceived;
	private Calendar processedDate;
	private PeriodDate claimDateOfService;
	private String groupNumber;
	private String groupName;
	private Provider baseProvider;
	private String medicalRecordNumber;
	private String claimPaidToDescription;
	private int numberOfServiceLines;
	private BigDecimal totalClaimChargeAmount;
	private Provider renderingProvider;
	private Boolean otherInsurance;
	private String checkNumber;
	private Calendar checkIssueDate;
	private Boolean eobAvailable;
	private String patientAccountNumber;
	private KeyDescription[] conditionCodesGrp;
	private OccurrenceCode[] occurrenceCode;
	private ValueCode[] valueCode;
	private ClaimPayment claimPayment;
	private Code eobMessage;
	private Code statusVerbiage;
	private Code remitVerbiage;
	private ClaimStatusEntity primaryStatusEntity;
	private ClaimStatusEntity secondaryStatusEntity;
	private ClaimStatusEntity thirdStatusEntity;
	private String cdhpProductFundType;
	private Boolean cdhpApplies;
	private String denialReason;
	private Boolean sensitiveClaim;
	private HealthClaimDetail healthClaimDetail;
	private DentalClaimDetail dentalClaimDetail;
	  
	public ClaimDetail() {
			
	}

	public ClaimDetail(Patient patient, String aliasRpn, String claimNumber, String itsClaimNumber, String documentationRequiredCode, String documentationDescription,
			           String hostHomeIndicator, String statusCode, ClaimStatusType statusType, Calendar dateClaimReceived, Calendar processedDate, PeriodDate claimDateOfService, 
			           String groupNumber, String groupName, Provider baseProvider, String medicalRecordNumber, String claimPaidToDescription, int numberOfServiceLines, 
			           BigDecimal totalClaimChargeAmount, Provider renderingProvider, Boolean otherInsurance, String checkNumber, Calendar checkIssueDate, Boolean eobAvailable, 
			           String patientAccountNumber, KeyDescription[] conditionCodesGrp, OccurrenceCode[] occurrenceCode, ValueCode[] valueCode, ClaimPayment claimPayment, 
			           Code eobMessage, Code statusVerbiage, Code remitVerbiage, ClaimStatusEntity primaryStatusEntity, ClaimStatusEntity secondaryStatusEntity, 
			           ClaimStatusEntity thirdStatusEntity, String cdhpProductFundType, Boolean cdhpApplies, String denialReason, Boolean sensitiveClaim, 
			           HealthClaimDetail healthClaimDetail, DentalClaimDetail dentalClaimDetail) {
	    this.patient = patient;
	    this.aliasRpn = aliasRpn;
	    this.claimNumber = claimNumber;
	    this.itsClaimNumber = itsClaimNumber;
	    this.documentationRequiredCode = documentationRequiredCode;
	    this.documentationDescription = documentationDescription;
	    this.documentationDescription = hostHomeIndicator;
	    this.statusCode = statusCode;
	    this.statusType = statusType;
	    this.dateClaimReceived = dateClaimReceived;
	    this.processedDate = processedDate;
	    this.claimDateOfService = claimDateOfService;
	    this.groupNumber = groupNumber;
	    this.groupName = groupName;
	    this.baseProvider = baseProvider;
	    this.medicalRecordNumber = medicalRecordNumber;
	    this.claimPaidToDescription = claimPaidToDescription;
	    this.numberOfServiceLines = numberOfServiceLines;
	    this.totalClaimChargeAmount = totalClaimChargeAmount;
	    this.renderingProvider = renderingProvider;
	    this.otherInsurance = otherInsurance;
	    this.checkNumber = checkNumber;
	    this.checkIssueDate = checkIssueDate;
	    this.eobAvailable = eobAvailable;
	    this.patientAccountNumber = patientAccountNumber;
	    this.conditionCodesGrp = conditionCodesGrp;
	    this.occurrenceCode = occurrenceCode;
	    this.valueCode = valueCode;
	    this.claimPayment = claimPayment;
	    this.eobMessage = eobMessage;
	    this.statusVerbiage = statusVerbiage;
	    this.remitVerbiage = remitVerbiage;
	    this.primaryStatusEntity = primaryStatusEntity;
	    this.secondaryStatusEntity = secondaryStatusEntity;
	    this.thirdStatusEntity = thirdStatusEntity;
	    this.cdhpProductFundType = cdhpProductFundType;
	    this.cdhpApplies = cdhpApplies;
	    this.denialReason = denialReason;
	    this.sensitiveClaim = sensitiveClaim;
	    this.healthClaimDetail = healthClaimDetail;
	    this.dentalClaimDetail = dentalClaimDetail;
	}
  
	public Patient getPatient() {
		return this.patient;
	}
  
	public void setPatient(Patient patient) {
		this.patient = patient;
	}
  
	public String getAliasRpn() {
		return this.aliasRpn;
	}
  
	public void setAliasRpn(String aliasRpn) {
		this.aliasRpn = aliasRpn;
	}
  
	public String getClaimNumber() {
		return this.claimNumber;
	}
  
	public void setClaimNumber(String claimNumber) {
		this.claimNumber = claimNumber;
	}
  
	public String getItsClaimNumber() {
		return this.itsClaimNumber;
	}
  
	public void setItsClaimNumber(String itsClaimNumber) {
		this.itsClaimNumber = itsClaimNumber;
	}
  
	public String getDocumentationRequiredCode() {
		return this.documentationRequiredCode;
	}
  
	public void setDocumentationRequiredCode(String documentationRequiredCode) {
		this.documentationRequiredCode = documentationRequiredCode;
	}
  
	public String getDocumentationDescription() {
		return this.documentationDescription;
	}
  
	public void setDocumentationDescription(String documentationDescription) {
		this.documentationDescription = documentationDescription;
	}
  
	public String getHostHomeIndicator() {
		return this.hostHomeIndicator;
	}
  
	public void setHostHomeIndicator(String hostHomeIndicator) {
		this.hostHomeIndicator = hostHomeIndicator;
	}
  
	public String getStatusCode() {
		return this.statusCode;
	}
  
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
  
	public ClaimStatusType getStatusType() {
		return this.statusType;
	}
  
	public void setStatusType(ClaimStatusType statusType) {
		this.statusType = statusType;
	}
  
	public Calendar getDateClaimReceived() {
		return this.dateClaimReceived;
	}
  
	public void setDateClaimReceived(Calendar dateClaimReceived) {
		this.dateClaimReceived = dateClaimReceived;
	}
  
	public Calendar getProcessedDate() {
		return this.processedDate;
	}
  
	public void setProcessedDate(Calendar processedDate) {
		this.processedDate = processedDate;
	}
  
	public PeriodDate getClaimDateOfService() {
		return this.claimDateOfService;
	}
  
	public void setClaimDateOfService(PeriodDate claimDateOfService) {
		this.claimDateOfService = claimDateOfService;
	}
  
	public String getGroupNumber() {
		return this.groupNumber;
	}
  
	public void setGroupNumber(String groupNumber) {
		this.groupNumber = groupNumber;
	}
  
	public String getGroupName() {
		return this.groupName;
	}
  
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
  
	public Provider getBaseProvider() {
		return this.baseProvider;
	}
  
	public void setBaseProvider(Provider baseProvider) {
		this.baseProvider = baseProvider;
	}
  
	public String getMedicalRecordNumber() {
		return this.medicalRecordNumber;
	}
  
	public void setMedicalRecordNumber(String medicalRecordNumber) {
		this.medicalRecordNumber = medicalRecordNumber;
	}
  
	public String getClaimPaidToDescription()  {
		return this.claimPaidToDescription;
	}
  
	public void setClaimPaidToDescription(String claimPaidToDescription) {
		this.claimPaidToDescription = claimPaidToDescription;
	}
  
	public int getNumberOfServiceLines() {
		return this.numberOfServiceLines;
	}
  
	public void setNumberOfServiceLines(int numberOfServiceLines) {
		this.numberOfServiceLines = numberOfServiceLines;
	}
  
	public BigDecimal getTotalClaimChargeAmount() {
		return this.totalClaimChargeAmount;
	}
  
	public void setTotalClaimChargeAmount(BigDecimal totalClaimChargeAmount) {
		this.totalClaimChargeAmount = totalClaimChargeAmount;
	}
  
	public Provider getRenderingProvider() {
		return this.renderingProvider;
	}
  
	public void setRenderingProvider(Provider renderingProvider) {
		this.renderingProvider = renderingProvider;
	}
  
	public Boolean getOtherInsurance() {
		return this.otherInsurance;
	}
  
	public void setOtherInsurance(Boolean otherInsurance) {
		this.otherInsurance = otherInsurance;
	}
  
	public String getCheckNumber() {
		return this.checkNumber;
	}
  
	public void setCheckNumber(String checkNumber) {
		this.checkNumber = checkNumber;
	}
  
	public Calendar getCheckIssueDate() {
		return this.checkIssueDate;
	}
  
	public void setCheckIssueDate(Calendar checkIssueDate) {
		this.checkIssueDate = checkIssueDate;
	}
  
	public Boolean getEobAvailable() {
		return this.eobAvailable;
	}
  
	public void setEobAvailable(Boolean eobAvailable) {
		this.eobAvailable = eobAvailable;
	}
  
	public String getPatientAccountNumber() {
		return this.patientAccountNumber;
	}
  
	public void setPatientAccountNumber(String patientAccountNumber) {
		this.patientAccountNumber = patientAccountNumber;
	}
  
	public KeyDescription[] getConditionCodesGrp() {
		return this.conditionCodesGrp;
	}
  
	public void setConditionCodesGrp(KeyDescription[] conditionCodesGrp) {
		this.conditionCodesGrp = conditionCodesGrp;
	}
  
	public KeyDescription getConditionCodesGrp(int i) {
		return this.conditionCodesGrp[i];
	}
  
	public void setConditionCodesGrp(int i, KeyDescription _value) {
		this.conditionCodesGrp[i] = _value;
	}
  
	public OccurrenceCode[] getOccurrenceCode() {
		return this.occurrenceCode;
	}
  
	public void setOccurrenceCode(OccurrenceCode[] occurrenceCode) {
		this.occurrenceCode = occurrenceCode;
	}
  
	public OccurrenceCode getOccurrenceCode(int i) {
		return this.occurrenceCode[i];
	}
  
	public void setOccurrenceCode(int i, OccurrenceCode _value) {
		this.occurrenceCode[i] = _value;
	}
  
	public ValueCode[] getValueCode() {
		return this.valueCode;
	}
  
	public void setValueCode(ValueCode[] valueCode) {
		this.valueCode = valueCode;
	}
  
	public ValueCode getValueCode(int i) {
		return this.valueCode[i];
	}
  
	public void setValueCode(int i, ValueCode _value) {
		this.valueCode[i] = _value;
	}
  
	public ClaimPayment getClaimPayment() {
		return this.claimPayment;
	}
  
	public void setClaimPayment(ClaimPayment claimPayment) {
		this.claimPayment = claimPayment;
	}
  
	public Code getEobMessage() {
		return this.eobMessage;
	}
  
	public void setEobMessage(Code eobMessage) {
		this.eobMessage = eobMessage;
	}
  
	public Code getStatusVerbiage() {
		return this.statusVerbiage;
	}
  
	public void setStatusVerbiage(Code statusVerbiage) {
		this.statusVerbiage = statusVerbiage;
	}
  
	public Code getRemitVerbiage() {
		return this.remitVerbiage;
	}
  
	public void setRemitVerbiage(Code remitVerbiage) {
		this.remitVerbiage = remitVerbiage;
	}
  
	public ClaimStatusEntity getPrimaryStatusEntity() {
		return this.primaryStatusEntity;
	}
  
	public void setPrimaryStatusEntity(ClaimStatusEntity primaryStatusEntity) {
		this.primaryStatusEntity = primaryStatusEntity;
	}
  
	public ClaimStatusEntity getSecondaryStatusEntity() {
		return this.secondaryStatusEntity;
	}
  
	public void setSecondaryStatusEntity(ClaimStatusEntity secondaryStatusEntity) {
		this.secondaryStatusEntity = secondaryStatusEntity;
	}
  
	public ClaimStatusEntity getThirdStatusEntity() {
		return this.thirdStatusEntity;
	}
  
	public void setThirdStatusEntity(ClaimStatusEntity thirdStatusEntity) {
		this.thirdStatusEntity = thirdStatusEntity;
	}
  
	public String getCdhpProductFundType() {
		return this.cdhpProductFundType;
	}
  
	public void setCdhpProductFundType(String cdhpProductFundType) {
		this.cdhpProductFundType = cdhpProductFundType;
	}
  
	public Boolean getCdhpApplies() {
		return this.cdhpApplies;
	}
  
	public void setCdhpApplies(Boolean cdhpApplies) {
		this.cdhpApplies = cdhpApplies;
	}
  
	public String getDenialReason() {
		return this.denialReason;
	}
  
	public void setDenialReason(String denialReason) {
		this.denialReason = denialReason;
	}
  
	public Boolean getSensitiveClaim() {
		return this.sensitiveClaim;
	}
  
	public void setSensitiveClaim(Boolean sensitiveClaim) {
		this.sensitiveClaim = sensitiveClaim;
	}
  
	public HealthClaimDetail getHealthClaimDetail() {
		return this.healthClaimDetail;
	}
  
	public void setHealthClaimDetail(HealthClaimDetail healthClaimDetail) {
		this.healthClaimDetail = healthClaimDetail;
	}
  
	public DentalClaimDetail getDentalClaimDetail() {
		return this.dentalClaimDetail;
	}
  
	public void setDentalClaimDetail(DentalClaimDetail dentalClaimDetail) {
		this.dentalClaimDetail = dentalClaimDetail;
	}
  
	private Object __equalsCalc = null;
  
	public synchronized boolean equals(Object obj) {
	    if (!(obj instanceof ClaimDetail)) {
	      return false;
	    }
	    ClaimDetail other = (ClaimDetail)obj;
	    if (obj == null) {
	    	return false;
	    }
	    if (this == obj) {
	    	return true;
	    }
	    if (this.__equalsCalc != null) {
	    	return this.__equalsCalc == obj;
	    }
	    this.__equalsCalc = obj;
	    
        boolean _equals = true && 
            ((this.patient==null && other.getPatient()==null) || 
             (this.patient!=null &&
              this.patient.equals(other.getPatient()))) &&
            ((this.aliasRpn==null && other.getAliasRpn()==null) || 
             (this.aliasRpn!=null &&
              this.aliasRpn.equals(other.getAliasRpn()))) &&
            ((this.claimNumber==null && other.getClaimNumber()==null) || 
             (this.claimNumber!=null &&
              this.claimNumber.equals(other.getClaimNumber()))) &&
            ((this.itsClaimNumber==null && other.getItsClaimNumber()==null) || 
             (this.itsClaimNumber!=null &&
              this.itsClaimNumber.equals(other.getItsClaimNumber()))) &&
            ((this.documentationRequiredCode==null && other.getDocumentationRequiredCode()==null) || 
             (this.documentationRequiredCode!=null &&
              this.documentationRequiredCode.equals(other.getDocumentationRequiredCode()))) &&
            ((this.documentationDescription==null && other.getDocumentationDescription()==null) || 
             (this.documentationDescription!=null &&
              this.documentationDescription.equals(other.getDocumentationDescription()))) &&
            ((this.statusCode==null && other.getStatusCode()==null) || 
             (this.statusCode!=null &&
              this.statusCode.equals(other.getStatusCode()))) &&
            ((this.statusType==null && other.getStatusType()==null) || 
             (this.statusType!=null &&
              this.statusType.equals(other.getStatusType()))) &&
            ((this.dateClaimReceived==null && other.getDateClaimReceived()==null) || 
             (this.dateClaimReceived!=null &&
              this.dateClaimReceived.equals(other.getDateClaimReceived()))) &&
            ((this.processedDate==null && other.getProcessedDate()==null) || 
             (this.processedDate!=null &&
              this.processedDate.equals(other.getProcessedDate()))) &&
            ((this.claimDateOfService==null && other.getClaimDateOfService()==null) || 
             (this.claimDateOfService!=null &&
              this.claimDateOfService.equals(other.getClaimDateOfService()))) &&
            ((this.groupNumber==null && other.getGroupNumber()==null) || 
             (this.groupNumber!=null &&
              this.groupNumber.equals(other.getGroupNumber()))) &&
            ((this.groupName==null && other.getGroupName()==null) || 
             (this.groupName!=null &&
              this.groupName.equals(other.getGroupName()))) &&
            ((this.baseProvider==null && other.getBaseProvider()==null) || 
             (this.baseProvider!=null &&
              this.baseProvider.equals(other.getBaseProvider()))) &&
            ((this.medicalRecordNumber==null && other.getMedicalRecordNumber()==null) || 
             (this.medicalRecordNumber!=null &&
              this.medicalRecordNumber.equals(other.getMedicalRecordNumber()))) &&
            ((this.claimPaidToDescription==null && other.getClaimPaidToDescription()==null) || 
             (this.claimPaidToDescription!=null &&
              this.claimPaidToDescription.equals(other.getClaimPaidToDescription()))) &&
            this.numberOfServiceLines == other.getNumberOfServiceLines() &&
            ((this.totalClaimChargeAmount==null && other.getTotalClaimChargeAmount()==null) || 
             (this.totalClaimChargeAmount!=null &&
              this.totalClaimChargeAmount.equals(other.getTotalClaimChargeAmount()))) &&
            ((this.renderingProvider==null && other.getRenderingProvider()==null) || 
             (this.renderingProvider!=null &&
              this.renderingProvider.equals(other.getRenderingProvider()))) &&
            ((this.otherInsurance==null && other.getOtherInsurance()==null) || 
             (this.otherInsurance!=null &&
              this.otherInsurance.equals(other.getOtherInsurance()))) &&
            ((this.checkNumber==null && other.getCheckNumber()==null) || 
             (this.checkNumber!=null &&
              this.checkNumber.equals(other.getCheckNumber()))) &&
            ((this.checkIssueDate==null && other.getCheckIssueDate()==null) || 
             (this.checkIssueDate!=null &&
              this.checkIssueDate.equals(other.getCheckIssueDate()))) &&
            ((this.eobAvailable==null && other.getEobAvailable()==null) || 
             (this.eobAvailable!=null &&
              this.eobAvailable.equals(other.getEobAvailable()))) &&
            ((this.patientAccountNumber==null && other.getPatientAccountNumber()==null) || 
             (this.patientAccountNumber!=null &&
              this.patientAccountNumber.equals(other.getPatientAccountNumber()))) &&
            ((this.conditionCodesGrp==null && other.getConditionCodesGrp()==null) || 
             (this.conditionCodesGrp!=null &&
              java.util.Arrays.equals(this.conditionCodesGrp, other.getConditionCodesGrp()))) &&
            ((this.occurrenceCode==null && other.getOccurrenceCode()==null) || 
             (this.occurrenceCode!=null &&
              java.util.Arrays.equals(this.occurrenceCode, other.getOccurrenceCode()))) &&
            ((this.valueCode==null && other.getValueCode()==null) || 
             (this.valueCode!=null &&
              java.util.Arrays.equals(this.valueCode, other.getValueCode()))) &&
            ((this.claimPayment==null && other.getClaimPayment()==null) || 
             (this.claimPayment!=null &&
              this.claimPayment.equals(other.getClaimPayment()))) &&
            ((this.eobMessage==null && other.getEobMessage()==null) || 
             (this.eobMessage!=null &&
              this.eobMessage.equals(other.getEobMessage()))) &&
            ((this.statusVerbiage==null && other.getStatusVerbiage()==null) || 
             (this.statusVerbiage!=null &&
              this.statusVerbiage.equals(other.getStatusVerbiage()))) &&
            ((this.remitVerbiage==null && other.getRemitVerbiage()==null) || 
             (this.remitVerbiage!=null &&
              this.remitVerbiage.equals(other.getRemitVerbiage()))) &&
            ((this.primaryStatusEntity==null && other.getPrimaryStatusEntity()==null) || 
             (this.primaryStatusEntity!=null &&
              this.primaryStatusEntity.equals(other.getPrimaryStatusEntity()))) &&
            ((this.secondaryStatusEntity==null && other.getSecondaryStatusEntity()==null) || 
             (this.secondaryStatusEntity!=null &&
              this.secondaryStatusEntity.equals(other.getSecondaryStatusEntity()))) &&
            ((this.thirdStatusEntity==null && other.getThirdStatusEntity()==null) || 
             (this.thirdStatusEntity!=null &&
              this.thirdStatusEntity.equals(other.getThirdStatusEntity()))) &&
            ((this.cdhpProductFundType==null && other.getCdhpProductFundType()==null) || 
             (this.cdhpProductFundType!=null &&
              this.cdhpProductFundType.equals(other.getCdhpProductFundType()))) &&
            ((this.cdhpApplies==null && other.getCdhpApplies()==null) || 
             (this.cdhpApplies!=null &&
              this.cdhpApplies.equals(other.getCdhpApplies()))) &&
            ((this.denialReason==null && other.getDenialReason()==null) || 
             (this.denialReason!=null &&
              this.denialReason.equals(other.getDenialReason()))) &&
            ((this.sensitiveClaim==null && other.getSensitiveClaim()==null) || 
             (this.sensitiveClaim!=null &&
              this.sensitiveClaim.equals(other.getSensitiveClaim()))) &&
            ((this.healthClaimDetail==null && other.getHealthClaimDetail()==null) || 
             (this.healthClaimDetail!=null &&
              this.healthClaimDetail.equals(other.getHealthClaimDetail()))) &&
            ((this.dentalClaimDetail==null && other.getDentalClaimDetail()==null) || 
             (this.dentalClaimDetail!=null &&
              this.dentalClaimDetail.equals(other.getDentalClaimDetail())));
	    
	    this.__equalsCalc = null;
	    return _equals;
	}
  
	private boolean __hashCodeCalc = false;
  
	public synchronized int hashCode() {
	    if (this.__hashCodeCalc) {
	    	return 0;
	    }
	    this.__hashCodeCalc = true;
	    int _hashCode = 1;
	    if (getPatient() != null) {
	    	_hashCode += getPatient().hashCode();
	    }
	    if (getAliasRpn() != null) {
	    	_hashCode += getAliasRpn().hashCode();
	    }
	    if (getClaimNumber() != null) {
	    	_hashCode += getClaimNumber().hashCode();
	    }
	    if (getItsClaimNumber() != null) {
	    	_hashCode += getClaimNumber().hashCode();
	    }
	    if (getDocumentationRequiredCode() != null) {
	    	_hashCode += getDocumentationRequiredCode().hashCode();
	    }
	    if (getDocumentationDescription() != null) {
	    	_hashCode += getDocumentationDescription().hashCode();
	    }
	    if (getHostHomeIndicator() != null) {
	    	_hashCode += getHostHomeIndicator().hashCode();
	    }
	    if (getStatusCode() != null) {
	    	_hashCode += getStatusCode().hashCode();
	    }
	    if (getStatusType() != null) {
	    	_hashCode += getStatusType().hashCode();
	    }
	    if (getDateClaimReceived() != null) {
	    	_hashCode += getDateClaimReceived().hashCode();
	    }
	    if (getProcessedDate() != null) {
	    	_hashCode += getProcessedDate().hashCode();
	    }
	    if (getClaimDateOfService() != null) {
	    	_hashCode += getClaimDateOfService().hashCode();
	    }
	    if (getGroupNumber() != null) {
	    	_hashCode += getGroupNumber().hashCode();
	    }
	    if (getGroupName() != null) {
	    	_hashCode += getGroupName().hashCode();
	    }
	    if (getBaseProvider() != null) {
	    	_hashCode += getBaseProvider().hashCode();
	    }
	    if (getMedicalRecordNumber() != null) {
	    	_hashCode += getMedicalRecordNumber().hashCode();
	    }
	    if (getClaimPaidToDescription() != null) {
	    	_hashCode += getClaimPaidToDescription().hashCode();
	    }
	    _hashCode += getNumberOfServiceLines();
	    if (getTotalClaimChargeAmount() != null) {
	    	_hashCode += getTotalClaimChargeAmount().hashCode();
	    }
	    if (getRenderingProvider() != null) {
	    	_hashCode += getRenderingProvider().hashCode();
	    }
	    if (getOtherInsurance() != null) {
	    	_hashCode += getOtherInsurance().hashCode();
	    }
	    if (getCheckNumber() != null) {
	    	_hashCode += getCheckNumber().hashCode();
	    }
	    if (getCheckIssueDate() != null) {
	    	_hashCode += getCheckIssueDate().hashCode();
	    }
	    if (getEobAvailable() != null) {
	    	_hashCode += getEobAvailable().hashCode();
	    }
	    if (getPatientAccountNumber() != null) {
	    	_hashCode += getPatientAccountNumber().hashCode();
	    }
	    if (getConditionCodesGrp() != null) {
	    	for (int i = 0; i < Array.getLength(getConditionCodesGrp()); i++){
		        Object obj = Array.get(getConditionCodesGrp(), i);
		        if ((obj != null) && (!obj.getClass().isArray())) {
		        	_hashCode += obj.hashCode();
		        }
	    	}
	    }
	    if (getOccurrenceCode() != null) {
	    	for (int i = 0; i < Array.getLength(getOccurrenceCode()); i++) {
		        Object obj = Array.get(getOccurrenceCode(), i);
		        if ((obj != null) && (!obj.getClass().isArray())) {
		        	_hashCode += obj.hashCode();
		        }
		    }
	    }
	    if (getValueCode() != null) {
	    	for (int i = 0; i < Array.getLength(getValueCode()); i++) {
		        Object obj = Array.get(getValueCode(), i);
		        if ((obj != null) && (!obj.getClass().isArray())) {
		        	_hashCode += obj.hashCode();
		        }
	    	}
	    }
	    if (getClaimPayment() != null) {
	    	_hashCode += getClaimPayment().hashCode();
	    }
	    if (getEobMessage() != null) {
	    	_hashCode += getEobMessage().hashCode();
	    }
	    if (getStatusVerbiage() != null) {
	    	_hashCode += getStatusVerbiage().hashCode();
	    }
	    if (getRemitVerbiage() != null) {
	    	_hashCode += getRemitVerbiage().hashCode();
	    }
	    if (getPrimaryStatusEntity() != null) {
	    	_hashCode += getPrimaryStatusEntity().hashCode();
	    }
	    if (getSecondaryStatusEntity() != null) {
	    	_hashCode += getSecondaryStatusEntity().hashCode();
	    }
	    if (getThirdStatusEntity() != null) {
	    	_hashCode += getThirdStatusEntity().hashCode();
	    }
	    if (getCdhpProductFundType() != null) {
	    	_hashCode += getCdhpProductFundType().hashCode();
	    }
	    if (getCdhpApplies() != null) {
	    	_hashCode += getCdhpApplies().hashCode();
	    }
	    if (getDenialReason() != null) {
	    	_hashCode += getDenialReason().hashCode();
	    }
	    if (getSensitiveClaim() != null) {
	    	_hashCode += getSensitiveClaim().hashCode();
	    }
	    if (getHealthClaimDetail() != null) {
	    	_hashCode += getHealthClaimDetail().hashCode();
	    }
	    if (getDentalClaimDetail() != null) {
	    	_hashCode += getDentalClaimDetail().hashCode();
	    }
	    this.__hashCodeCalc = false;
	    return _hashCode;
	}
  
	private static TypeDesc typeDesc = new TypeDesc(ClaimDetail.class, true);
  
	static {
	    typeDesc.setXmlType(new QName("http://servicehelpers.esb.avalon.com", "ClaimDetail"));
	    ElementDesc elemField = new ElementDesc();
	    elemField.setFieldName("patient");
	    elemField.setXmlName(new QName("", "patient"));
	    elemField.setXmlType(new QName("http://servicehelpers.esb.avalon.com", "Patient"));
	    elemField.setMinOccurs(0);
	    elemField.setNillable(false);
	    typeDesc.addFieldDesc(elemField);
	    elemField = new ElementDesc();
	    elemField.setFieldName("aliasRpn");
	    elemField.setXmlName(new QName("", "aliasRpn"));
	    elemField.setXmlType(new QName("http://www.w3.org/2001/XMLSchema", "string"));
	    elemField.setMinOccurs(0);
	    elemField.setNillable(false);
	    typeDesc.addFieldDesc(elemField);
	    elemField = new ElementDesc();
	    elemField.setFieldName("claimNumber");
	    elemField.setXmlName(new QName("", "claimNumber"));
	    elemField.setXmlType(new QName("http://www.w3.org/2001/XMLSchema", "string"));
	    elemField.setMinOccurs(0);
	    elemField.setNillable(false);
	    typeDesc.addFieldDesc(elemField);
	    elemField = new ElementDesc();
	    elemField = new ElementDesc();
	    elemField.setFieldName("itsClaimNumber");
	    elemField.setXmlName(new QName("", "itsClaimNumber"));
	    elemField.setXmlType(new QName("http://www.w3.org/2001/XMLSchema", "string"));
	    elemField.setMinOccurs(0);
	    elemField.setNillable(false);
	    typeDesc.addFieldDesc(elemField);
	    elemField = new ElementDesc();
	    elemField = new ElementDesc();
	    elemField.setFieldName("documentationRequiredCode");
	    elemField.setXmlName(new QName("", "documentationRequiredCode"));
	    elemField.setXmlType(new QName("http://www.w3.org/2001/XMLSchema", "string"));
	    elemField.setMinOccurs(0);
	    elemField.setNillable(false);
	    typeDesc.addFieldDesc(elemField);
	    elemField = new ElementDesc();
	    elemField = new ElementDesc();
	    elemField.setFieldName("documentationDescription");
	    elemField.setXmlName(new QName("", "documentationDescription"));
	    elemField.setXmlType(new QName("http://www.w3.org/2001/XMLSchema", "string"));
	    elemField.setMinOccurs(0);
	    elemField.setNillable(false);
	    typeDesc.addFieldDesc(elemField);
	    elemField = new ElementDesc();
	    elemField.setFieldName("hostHomeIndicator");
	    elemField.setXmlName(new QName("", "hostHomeIndicator"));
	    elemField.setXmlType(new QName("http://www.w3.org/2001/XMLSchema", "string"));
	    elemField.setMinOccurs(0);
	    elemField.setNillable(false);
	    typeDesc.addFieldDesc(elemField);
	    elemField = new ElementDesc();
	    elemField.setFieldName("statusCode");
	    elemField.setXmlName(new QName("", "statusCode"));
	    elemField.setXmlType(new QName("http://www.w3.org/2001/XMLSchema", "string"));
	    elemField.setMinOccurs(0);
	    elemField.setNillable(false);
	    typeDesc.addFieldDesc(elemField);
	    elemField = new ElementDesc();
	    elemField.setFieldName("statusType");
	    elemField.setXmlName(new QName("", "statusType"));
	    elemField.setXmlType(new QName("http://servicehelpers.esb.avalon.com", "ClaimStatusType"));
	    elemField.setMinOccurs(0);
	    elemField.setNillable(false);
	    typeDesc.addFieldDesc(elemField);
	    elemField = new ElementDesc();
	    elemField.setFieldName("dateClaimReceived");
	    elemField.setXmlName(new QName("", "dateClaimReceived"));
	    elemField.setXmlType(new QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
	    elemField.setMinOccurs(0);
	    elemField.setNillable(false);
	    typeDesc.addFieldDesc(elemField);
	    elemField = new ElementDesc();
	    elemField.setFieldName("processedDate");
	    elemField.setXmlName(new QName("", "processedDate"));
	    elemField.setXmlType(new QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
	    elemField.setMinOccurs(0);
	    elemField.setNillable(false);
	    typeDesc.addFieldDesc(elemField);
	    elemField = new ElementDesc();
	    elemField.setFieldName("claimDateOfService");
	    elemField.setXmlName(new QName("", "claimDateOfService"));
	    elemField.setXmlType(new QName("http://servicehelpers.esb.avalon.com", "PeriodDate"));
	    elemField.setMinOccurs(0);
	    elemField.setNillable(false);
	    typeDesc.addFieldDesc(elemField);
	    elemField = new ElementDesc();
	    elemField.setFieldName("groupNumber");
	    elemField.setXmlName(new QName("", "groupNumber"));
	    elemField.setXmlType(new QName("http://www.w3.org/2001/XMLSchema", "string"));
	    elemField.setMinOccurs(0);
	    elemField.setNillable(false);
	    typeDesc.addFieldDesc(elemField);
	    elemField = new ElementDesc();
	    elemField.setFieldName("groupName");
	    elemField.setXmlName(new QName("", "groupName"));
	    elemField.setXmlType(new QName("http://www.w3.org/2001/XMLSchema", "string"));
	    elemField.setMinOccurs(0);
	    elemField.setNillable(false);
	    typeDesc.addFieldDesc(elemField);
	    elemField = new ElementDesc();
	    elemField.setFieldName("baseProvider");
	    elemField.setXmlName(new QName("", "baseProvider"));
	    elemField.setXmlType(new QName("http://servicehelpers.esb.avalon.com", "Provider"));
	    elemField.setMinOccurs(0);
	    elemField.setNillable(false);
	    typeDesc.addFieldDesc(elemField);
	    elemField = new ElementDesc();
	    elemField.setFieldName("medicalRecordNumber");
	    elemField.setXmlName(new QName("", "medicalRecordNumber"));
	    elemField.setXmlType(new QName("http://www.w3.org/2001/XMLSchema", "string"));
	    elemField.setMinOccurs(0);
	    elemField.setNillable(false);
	    typeDesc.addFieldDesc(elemField);
	    elemField = new ElementDesc();
	    elemField.setFieldName("claimPaidToDescription");
	    elemField.setXmlName(new QName("", "claimPaidToDescription"));
	    elemField.setXmlType(new QName("http://www.w3.org/2001/XMLSchema", "string"));
	    elemField.setMinOccurs(0);
	    elemField.setNillable(false);
	    typeDesc.addFieldDesc(elemField);
	    elemField = new ElementDesc();
	    elemField.setFieldName("numberOfServiceLines");
	    elemField.setXmlName(new QName("", "numberOfServiceLines"));
	    elemField.setXmlType(new QName("http://www.w3.org/2001/XMLSchema", "int"));
	    elemField.setNillable(false);
	    typeDesc.addFieldDesc(elemField);
	    elemField = new ElementDesc();
	    elemField.setFieldName("totalClaimChargeAmount");
	    elemField.setXmlName(new QName("", "totalClaimChargeAmount"));
	    elemField.setXmlType(new QName("http://www.w3.org/2001/XMLSchema", "decimal"));
	    elemField.setMinOccurs(0);
	    elemField.setNillable(false);
	    typeDesc.addFieldDesc(elemField);
	    elemField = new ElementDesc();
	    elemField.setFieldName("renderingProvider");
	    elemField.setXmlName(new QName("", "renderingProvider"));
	    elemField.setXmlType(new QName("http://servicehelpers.esb.avalon.com", "Provider"));
	    elemField.setMinOccurs(0);
	    elemField.setNillable(false);
	    typeDesc.addFieldDesc(elemField);
	    elemField = new ElementDesc();
	    elemField.setFieldName("otherInsurance");
	    elemField.setXmlName(new QName("", "otherInsurance"));
	    elemField.setXmlType(new QName("http://www.w3.org/2001/XMLSchema", "boolean"));
	    elemField.setMinOccurs(0);
	    elemField.setNillable(false);
	    typeDesc.addFieldDesc(elemField);
	    elemField = new ElementDesc();
	    elemField.setFieldName("checkNumber");
	    elemField.setXmlName(new QName("", "checkNumber"));
	    elemField.setXmlType(new QName("http://www.w3.org/2001/XMLSchema", "string"));
	    elemField.setMinOccurs(0);
	    elemField.setNillable(false);
	    typeDesc.addFieldDesc(elemField);
	    elemField = new ElementDesc();
	    elemField.setFieldName("checkIssueDate");
	    elemField.setXmlName(new QName("", "checkIssueDate"));
	    elemField.setXmlType(new QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
	    elemField.setMinOccurs(0);
	    elemField.setNillable(false);
	    typeDesc.addFieldDesc(elemField);
	    elemField = new ElementDesc();
	    elemField.setFieldName("eobAvailable");
	    elemField.setXmlName(new QName("", "eobAvailable"));
	    elemField.setXmlType(new QName("http://www.w3.org/2001/XMLSchema", "boolean"));
	    elemField.setMinOccurs(0);
	    elemField.setNillable(false);
	    typeDesc.addFieldDesc(elemField);
	    elemField = new ElementDesc();
	    elemField.setFieldName("patientAccountNumber");
	    elemField.setXmlName(new QName("", "patientAccountNumber"));
	    elemField.setXmlType(new QName("http://www.w3.org/2001/XMLSchema", "string"));
	    elemField.setMinOccurs(0);
	    elemField.setNillable(false);
	    typeDesc.addFieldDesc(elemField);
	    elemField = new ElementDesc();
	    elemField.setFieldName("conditionCodesGrp");
	    elemField.setXmlName(new QName("", "conditionCodesGrp"));
	    elemField.setXmlType(new QName("http://servicehelpers.esb.avalon.com", "KeyDescription"));
	    elemField.setMinOccurs(0);
	    elemField.setNillable(true);
	    elemField.setMaxOccursUnbounded(true);
	    typeDesc.addFieldDesc(elemField);
	    elemField = new ElementDesc();
	    elemField.setFieldName("occurrenceCode");
	    elemField.setXmlName(new QName("", "occurrenceCode"));
	    elemField.setXmlType(new QName("http://servicehelpers.esb.avalon.com", "OccurrenceCode"));
	    elemField.setMinOccurs(0);
	    elemField.setNillable(true);
	    elemField.setMaxOccursUnbounded(true);
	    typeDesc.addFieldDesc(elemField);
	    elemField = new ElementDesc();
	    elemField.setFieldName("valueCode");
	    elemField.setXmlName(new QName("", "valueCode"));
	    elemField.setXmlType(new QName("http://servicehelpers.esb.avalon.com", "ValueCode"));
	    elemField.setMinOccurs(0);
	    elemField.setNillable(true);
	    elemField.setMaxOccursUnbounded(true);
	    typeDesc.addFieldDesc(elemField);
	    elemField = new ElementDesc();
	    elemField.setFieldName("claimPayment");
	    elemField.setXmlName(new QName("", "claimPayment"));
	    elemField.setXmlType(new QName("http://servicehelpers.esb.avalon.com", "ClaimPayment"));
	    elemField.setMinOccurs(0);
	    elemField.setNillable(false);
	    typeDesc.addFieldDesc(elemField);
	    elemField = new ElementDesc();
	    elemField.setFieldName("eobMessage");
	    elemField.setXmlName(new QName("", "eobMessage"));
	    elemField.setXmlType(new QName("http://servicehelpers.esb.avalon.com", "Code"));
	    elemField.setMinOccurs(0);
	    elemField.setNillable(false);
	    typeDesc.addFieldDesc(elemField);
	    elemField = new ElementDesc();
	    elemField.setFieldName("statusVerbiage");
	    elemField.setXmlName(new QName("", "statusVerbiage"));
	    elemField.setXmlType(new QName("http://servicehelpers.esb.avalon.com", "Code"));
	    elemField.setMinOccurs(0);
	    elemField.setNillable(false);
	    typeDesc.addFieldDesc(elemField);
	    elemField = new ElementDesc();
	    elemField.setFieldName("remitVerbiage");
	    elemField.setXmlName(new QName("", "remitVerbiage"));
	    elemField.setXmlType(new QName("http://servicehelpers.esb.avalon.com", "Code"));
	    elemField.setMinOccurs(0);
	    elemField.setNillable(false);
	    typeDesc.addFieldDesc(elemField);
	    elemField = new ElementDesc();
	    elemField.setFieldName("primaryStatusEntity");
	    elemField.setXmlName(new QName("", "primaryStatusEntity"));
	    elemField.setXmlType(new QName("http://servicehelpers.esb.avalon.com", "ClaimStatusEntity"));
	    elemField.setMinOccurs(0);
	    elemField.setNillable(false);
	    typeDesc.addFieldDesc(elemField);
	    elemField = new ElementDesc();
	    elemField.setFieldName("secondaryStatusEntity");
	    elemField.setXmlName(new QName("", "secondaryStatusEntity"));
	    elemField.setXmlType(new QName("http://servicehelpers.esb.avalon.com", "ClaimStatusEntity"));
	    elemField.setMinOccurs(0);
	    elemField.setNillable(false);
	    typeDesc.addFieldDesc(elemField);
	    elemField = new ElementDesc();
	    elemField.setFieldName("thirdStatusEntity");
	    elemField.setXmlName(new QName("", "thirdStatusEntity"));
	    elemField.setXmlType(new QName("http://servicehelpers.esb.avalon.com", "ClaimStatusEntity"));
	    elemField.setMinOccurs(0);
	    elemField.setNillable(false);
	    typeDesc.addFieldDesc(elemField);
	    elemField = new ElementDesc();
	    elemField.setFieldName("cdhpProductFundType");
	    elemField.setXmlName(new QName("", "cdhpProductFundType"));
	    elemField.setXmlType(new QName("http://www.w3.org/2001/XMLSchema", "string"));
	    elemField.setMinOccurs(0);
	    elemField.setNillable(false);
	    typeDesc.addFieldDesc(elemField);
	    elemField = new ElementDesc();
	    elemField.setFieldName("cdhpApplies");
	    elemField.setXmlName(new QName("", "cdhpApplies"));
	    elemField.setXmlType(new QName("http://www.w3.org/2001/XMLSchema", "boolean"));
	    elemField.setMinOccurs(0);
	    elemField.setNillable(false);
	    typeDesc.addFieldDesc(elemField);
	    elemField = new ElementDesc();
	    elemField.setFieldName("denialReason");
	    elemField.setXmlName(new QName("", "denialReason"));
	    elemField.setXmlType(new QName("http://www.w3.org/2001/XMLSchema", "string"));
	    elemField.setMinOccurs(0);
	    elemField.setNillable(false);
	    typeDesc.addFieldDesc(elemField);
	    elemField = new ElementDesc();
	    elemField.setFieldName("sensitiveClaim");
	    elemField.setXmlName(new QName("", "sensitiveClaim"));
	    elemField.setXmlType(new QName("http://www.w3.org/2001/XMLSchema", "boolean"));
	    elemField.setMinOccurs(0);
	    elemField.setNillable(false);
	    typeDesc.addFieldDesc(elemField);
	    elemField = new ElementDesc();
	    elemField.setFieldName("healthClaimDetail");
	    elemField.setXmlName(new QName("", "healthClaimDetail"));
	    elemField.setXmlType(new QName("http://servicehelpers.esb.avalon.com", "HealthClaimDetail"));
	    elemField.setMinOccurs(0);
	    elemField.setNillable(false);
	    typeDesc.addFieldDesc(elemField);
	    elemField = new ElementDesc();
	    elemField.setFieldName("dentalClaimDetail");
	    elemField.setXmlName(new QName("", "dentalClaimDetail"));
	    elemField.setXmlType(new QName("http://servicehelpers.esb.avalon.com", "DentalClaimDetail"));
	    elemField.setMinOccurs(0);
	    elemField.setNillable(false);
	    typeDesc.addFieldDesc(elemField);
	}
  
	public static TypeDesc getTypeDesc() {
		return typeDesc;
	}
  
	public static Serializer getSerializer(String mechType, Class _javaType, QName _xmlType) {
		return new BeanSerializer(_javaType, _xmlType, typeDesc);
	}
  
	public static Deserializer getDeserializer(String mechType, Class _javaType, QName _xmlType) {
		return new BeanDeserializer(_javaType, _xmlType, typeDesc);
	}
}
