/**
 * HealthClaimSearchCriteria.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.avalon.esb.servicehelpers;

public class HealthClaimSearchCriteria  implements java.io.Serializable {
    private java.lang.String diagnosisFrom;

    private java.lang.String diagnosisTo;

    private java.lang.String authorizationNumber;

    private int blueExchangeDaysToWait;

    private java.lang.String billType;

    private com.avalon.esb.servicehelpers.Code icdVersionSubmitted;

    private com.avalon.esb.servicehelpers.Code icdVersionAdjudicated;

    public HealthClaimSearchCriteria() {
    }

    public HealthClaimSearchCriteria(
           java.lang.String diagnosisFrom,
           java.lang.String diagnosisTo,
           java.lang.String authorizationNumber,
           int blueExchangeDaysToWait,
           java.lang.String billType,
           com.avalon.esb.servicehelpers.Code icdVersionSubmitted,
           com.avalon.esb.servicehelpers.Code icdVersionAdjudicated) {
           this.diagnosisFrom = diagnosisFrom;
           this.diagnosisTo = diagnosisTo;
           this.authorizationNumber = authorizationNumber;
           this.blueExchangeDaysToWait = blueExchangeDaysToWait;
           this.billType = billType;
           this.icdVersionSubmitted = icdVersionSubmitted;
           this.icdVersionAdjudicated = icdVersionAdjudicated;
    }


    /**
     * Gets the diagnosisFrom value for this HealthClaimSearchCriteria.
     * 
     * @return diagnosisFrom
     */
    public java.lang.String getDiagnosisFrom() {
        return diagnosisFrom;
    }


    /**
     * Sets the diagnosisFrom value for this HealthClaimSearchCriteria.
     * 
     * @param diagnosisFrom
     */
    public void setDiagnosisFrom(java.lang.String diagnosisFrom) {
        this.diagnosisFrom = diagnosisFrom;
    }


    /**
     * Gets the diagnosisTo value for this HealthClaimSearchCriteria.
     * 
     * @return diagnosisTo
     */
    public java.lang.String getDiagnosisTo() {
        return diagnosisTo;
    }


    /**
     * Sets the diagnosisTo value for this HealthClaimSearchCriteria.
     * 
     * @param diagnosisTo
     */
    public void setDiagnosisTo(java.lang.String diagnosisTo) {
        this.diagnosisTo = diagnosisTo;
    }


    /**
     * Gets the authorizationNumber value for this HealthClaimSearchCriteria.
     * 
     * @return authorizationNumber
     */
    public java.lang.String getAuthorizationNumber() {
        return authorizationNumber;
    }


    /**
     * Sets the authorizationNumber value for this HealthClaimSearchCriteria.
     * 
     * @param authorizationNumber
     */
    public void setAuthorizationNumber(java.lang.String authorizationNumber) {
        this.authorizationNumber = authorizationNumber;
    }


    /**
     * Gets the blueExchangeDaysToWait value for this HealthClaimSearchCriteria.
     * 
     * @return blueExchangeDaysToWait
     */
    public int getBlueExchangeDaysToWait() {
        return blueExchangeDaysToWait;
    }


    /**
     * Sets the blueExchangeDaysToWait value for this HealthClaimSearchCriteria.
     * 
     * @param blueExchangeDaysToWait
     */
    public void setBlueExchangeDaysToWait(int blueExchangeDaysToWait) {
        this.blueExchangeDaysToWait = blueExchangeDaysToWait;
    }


    /**
     * Gets the billType value for this HealthClaimSearchCriteria.
     * 
     * @return billType
     */
    public java.lang.String getBillType() {
        return billType;
    }


    /**
     * Sets the billType value for this HealthClaimSearchCriteria.
     * 
     * @param billType
     */
    public void setBillType(java.lang.String billType) {
        this.billType = billType;
    }


    /**
     * Gets the icdVersionSubmitted value for this HealthClaimSearchCriteria.
     * 
     * @return icdVersionSubmitted
     */
    public com.avalon.esb.servicehelpers.Code getIcdVersionSubmitted() {
        return icdVersionSubmitted;
    }


    /**
     * Sets the icdVersionSubmitted value for this HealthClaimSearchCriteria.
     * 
     * @param icdVersionSubmitted
     */
    public void setIcdVersionSubmitted(com.avalon.esb.servicehelpers.Code icdVersionSubmitted) {
        this.icdVersionSubmitted = icdVersionSubmitted;
    }


    /**
     * Gets the icdVersionAdjudicated value for this HealthClaimSearchCriteria.
     * 
     * @return icdVersionAdjudicated
     */
    public com.avalon.esb.servicehelpers.Code getIcdVersionAdjudicated() {
        return icdVersionAdjudicated;
    }


    /**
     * Sets the icdVersionAdjudicated value for this HealthClaimSearchCriteria.
     * 
     * @param icdVersionAdjudicated
     */
    public void setIcdVersionAdjudicated(com.avalon.esb.servicehelpers.Code icdVersionAdjudicated) {
        this.icdVersionAdjudicated = icdVersionAdjudicated;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof HealthClaimSearchCriteria)) return false;
        HealthClaimSearchCriteria other = (HealthClaimSearchCriteria) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.diagnosisFrom==null && other.getDiagnosisFrom()==null) || 
             (this.diagnosisFrom!=null &&
              this.diagnosisFrom.equals(other.getDiagnosisFrom()))) &&
            ((this.diagnosisTo==null && other.getDiagnosisTo()==null) || 
             (this.diagnosisTo!=null &&
              this.diagnosisTo.equals(other.getDiagnosisTo()))) &&
            ((this.authorizationNumber==null && other.getAuthorizationNumber()==null) || 
             (this.authorizationNumber!=null &&
              this.authorizationNumber.equals(other.getAuthorizationNumber()))) &&
            this.blueExchangeDaysToWait == other.getBlueExchangeDaysToWait() &&
            ((this.billType==null && other.getBillType()==null) || 
             (this.billType!=null &&
              this.billType.equals(other.getBillType()))) &&
            ((this.icdVersionSubmitted==null && other.getIcdVersionSubmitted()==null) || 
             (this.icdVersionSubmitted!=null &&
              this.icdVersionSubmitted.equals(other.getIcdVersionSubmitted()))) &&
            ((this.icdVersionAdjudicated==null && other.getIcdVersionAdjudicated()==null) || 
             (this.icdVersionAdjudicated!=null &&
              this.icdVersionAdjudicated.equals(other.getIcdVersionAdjudicated())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getDiagnosisFrom() != null) {
            _hashCode += getDiagnosisFrom().hashCode();
        }
        if (getDiagnosisTo() != null) {
            _hashCode += getDiagnosisTo().hashCode();
        }
        if (getAuthorizationNumber() != null) {
            _hashCode += getAuthorizationNumber().hashCode();
        }
        _hashCode += getBlueExchangeDaysToWait();
        if (getBillType() != null) {
            _hashCode += getBillType().hashCode();
        }
        if (getIcdVersionSubmitted() != null) {
            _hashCode += getIcdVersionSubmitted().hashCode();
        }
        if (getIcdVersionAdjudicated() != null) {
            _hashCode += getIcdVersionAdjudicated().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(HealthClaimSearchCriteria.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://servicehelpers.esb.avalon.com", "HealthClaimSearchCriteria"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("diagnosisFrom");
        elemField.setXmlName(new javax.xml.namespace.QName("", "diagnosisFrom"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("diagnosisTo");
        elemField.setXmlName(new javax.xml.namespace.QName("", "diagnosisTo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("authorizationNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("", "authorizationNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("blueExchangeDaysToWait");
        elemField.setXmlName(new javax.xml.namespace.QName("", "blueExchangeDaysToWait"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("billType");
        elemField.setXmlName(new javax.xml.namespace.QName("", "billType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("icdVersionSubmitted");
        elemField.setXmlName(new javax.xml.namespace.QName("", "icdVersionSubmitted"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://servicehelpers.esb.avalon.com", "Code"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("icdVersionAdjudicated");
        elemField.setXmlName(new javax.xml.namespace.QName("", "icdVersionAdjudicated"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://servicehelpers.esb.avalon.com", "Code"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
