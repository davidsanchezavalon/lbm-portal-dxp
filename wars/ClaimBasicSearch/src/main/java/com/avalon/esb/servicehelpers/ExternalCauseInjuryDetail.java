/**
 * ExternalCauseInjuryDetail.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.avalon.esb.servicehelpers;

public class ExternalCauseInjuryDetail  implements java.io.Serializable {
    private com.avalon.esb.servicehelpers.Code externalCauseInjuryCode;

    private java.lang.Boolean externalCauseInjuryPoaIndicator;

    public ExternalCauseInjuryDetail() {
    }

    public ExternalCauseInjuryDetail(
           com.avalon.esb.servicehelpers.Code externalCauseInjuryCode,
           java.lang.Boolean externalCauseInjuryPoaIndicator) {
           this.externalCauseInjuryCode = externalCauseInjuryCode;
           this.externalCauseInjuryPoaIndicator = externalCauseInjuryPoaIndicator;
    }


    /**
     * Gets the externalCauseInjuryCode value for this ExternalCauseInjuryDetail.
     * 
     * @return externalCauseInjuryCode
     */
    public com.avalon.esb.servicehelpers.Code getExternalCauseInjuryCode() {
        return externalCauseInjuryCode;
    }


    /**
     * Sets the externalCauseInjuryCode value for this ExternalCauseInjuryDetail.
     * 
     * @param externalCauseInjuryCode
     */
    public void setExternalCauseInjuryCode(com.avalon.esb.servicehelpers.Code externalCauseInjuryCode) {
        this.externalCauseInjuryCode = externalCauseInjuryCode;
    }


    /**
     * Gets the externalCauseInjuryPoaIndicator value for this ExternalCauseInjuryDetail.
     * 
     * @return externalCauseInjuryPoaIndicator
     */
    public java.lang.Boolean getExternalCauseInjuryPoaIndicator() {
        return externalCauseInjuryPoaIndicator;
    }


    /**
     * Sets the externalCauseInjuryPoaIndicator value for this ExternalCauseInjuryDetail.
     * 
     * @param externalCauseInjuryPoaIndicator
     */
    public void setExternalCauseInjuryPoaIndicator(java.lang.Boolean externalCauseInjuryPoaIndicator) {
        this.externalCauseInjuryPoaIndicator = externalCauseInjuryPoaIndicator;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ExternalCauseInjuryDetail)) return false;
        ExternalCauseInjuryDetail other = (ExternalCauseInjuryDetail) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.externalCauseInjuryCode==null && other.getExternalCauseInjuryCode()==null) || 
             (this.externalCauseInjuryCode!=null &&
              this.externalCauseInjuryCode.equals(other.getExternalCauseInjuryCode()))) &&
            ((this.externalCauseInjuryPoaIndicator==null && other.getExternalCauseInjuryPoaIndicator()==null) || 
             (this.externalCauseInjuryPoaIndicator!=null &&
              this.externalCauseInjuryPoaIndicator.equals(other.getExternalCauseInjuryPoaIndicator())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getExternalCauseInjuryCode() != null) {
            _hashCode += getExternalCauseInjuryCode().hashCode();
        }
        if (getExternalCauseInjuryPoaIndicator() != null) {
            _hashCode += getExternalCauseInjuryPoaIndicator().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ExternalCauseInjuryDetail.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://servicehelpers.esb.avalon.com", "ExternalCauseInjuryDetail"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("externalCauseInjuryCode");
        elemField.setXmlName(new javax.xml.namespace.QName("", "externalCauseInjuryCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://servicehelpers.esb.avalon.com", "Code"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("externalCauseInjuryPoaIndicator");
        elemField.setXmlName(new javax.xml.namespace.QName("", "externalCauseInjuryPoaIndicator"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
