package com.avalon.esb.servicehelpers;

import java.rmi.RemoteException;

public class LBMClaimSearchServiceProxy implements com.avalon.esb.servicehelpers.LBMClaimSearchService {
	  private String _endpoint = null;
	  private com.avalon.esb.servicehelpers.LBMClaimSearchService lBMClaimSearchService = null;
	  
	  public LBMClaimSearchServiceProxy() {
	    _initLBMClaimSearchServiceProxy();
	  }
	  
	  public LBMClaimSearchServiceProxy(String endpoint) {
	    _endpoint = endpoint;
	    _initLBMClaimSearchServiceProxy();
	  }
	  
	  private void _initLBMClaimSearchServiceProxy() {
	    try {
	      lBMClaimSearchService = (new com.avalon.esb.servicehelpers.ClaimSearchImplServiceLocator()).getClaimSearchServicePort();
	      if (lBMClaimSearchService != null) {
	        if (_endpoint != null)
	          ((javax.xml.rpc.Stub)lBMClaimSearchService)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
	        else
	          _endpoint = (String)((javax.xml.rpc.Stub)lBMClaimSearchService)._getProperty("javax.xml.rpc.service.endpoint.address");
	      }
	      
	    }
	    catch (javax.xml.rpc.ServiceException serviceException) {}
	  }
	  
	  public String getEndpoint() {
	    return _endpoint;
	  }
	  
	  public void setEndpoint(String endpoint) {
	    _endpoint = endpoint;
	    if (lBMClaimSearchService != null)
	      ((javax.xml.rpc.Stub)lBMClaimSearchService)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
	    
	  }
	  
	  public com.avalon.esb.servicehelpers.LBMClaimSearchService getLBMClaimSearchService() {
	    if (lBMClaimSearchService == null)
	      _initLBMClaimSearchServiceProxy();
	    return lBMClaimSearchService;
	  }
	  
	  public com.avalon.esb.servicehelpers.ClaimSummariesPatient[] findClaims(com.avalon.esb.servicehelpers.FindClaims findClaims) throws java.rmi.RemoteException{
	    if (lBMClaimSearchService == null)
	      _initLBMClaimSearchServiceProxy();
	    return lBMClaimSearchService.findClaims(findClaims);
	  }
	  
	  public com.avalon.esb.servicehelpers.GetClaimDetailsResponse getClaimDetails(com.avalon.esb.servicehelpers.FindClaims findClaims) throws java.rmi.RemoteException{
	    if (lBMClaimSearchService == null)
	      _initLBMClaimSearchServiceProxy();
	    return lBMClaimSearchService.getClaimDetails(findClaims);
	  }

	//TODO: Remove Success Mock Code
	@Override
	public GetClaimDetailsResponse getSuccessMockClaimDetails(FindClaims findClaims) throws RemoteException {
		  return lBMClaimSearchService.getSuccessMockClaimDetails(findClaims);
	}

	//TODO: Remove Success Mock Code
	@Override
	public ClaimSummariesPatient[] findClaimsSuccessMock(FindClaims findClaims) throws RemoteException {
		  return lBMClaimSearchService.findClaimsSuccessMock(findClaims);
	}
	  
	  
	}