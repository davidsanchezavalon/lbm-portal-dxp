/**
 * ClaimStatusEntity.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.avalon.esb.servicehelpers;

public class ClaimStatusEntity  implements java.io.Serializable {
    private com.avalon.esb.servicehelpers.Code status;

    private com.avalon.esb.servicehelpers.Code statusCategory;

    private com.avalon.esb.servicehelpers.Code entity;

    private com.avalon.esb.servicehelpers.Code codeListQualifier;

    public ClaimStatusEntity() {
    }

    public ClaimStatusEntity(
           com.avalon.esb.servicehelpers.Code status,
           com.avalon.esb.servicehelpers.Code statusCategory,
           com.avalon.esb.servicehelpers.Code entity,
           com.avalon.esb.servicehelpers.Code codeListQualifier) {
           this.status = status;
           this.statusCategory = statusCategory;
           this.entity = entity;
           this.codeListQualifier = codeListQualifier;
    }


    /**
     * Gets the status value for this ClaimStatusEntity.
     * 
     * @return status
     */
    public com.avalon.esb.servicehelpers.Code getStatus() {
        return status;
    }


    /**
     * Sets the status value for this ClaimStatusEntity.
     * 
     * @param status
     */
    public void setStatus(com.avalon.esb.servicehelpers.Code status) {
        this.status = status;
    }


    /**
     * Gets the statusCategory value for this ClaimStatusEntity.
     * 
     * @return statusCategory
     */
    public com.avalon.esb.servicehelpers.Code getStatusCategory() {
        return statusCategory;
    }


    /**
     * Sets the statusCategory value for this ClaimStatusEntity.
     * 
     * @param statusCategory
     */
    public void setStatusCategory(com.avalon.esb.servicehelpers.Code statusCategory) {
        this.statusCategory = statusCategory;
    }


    /**
     * Gets the entity value for this ClaimStatusEntity.
     * 
     * @return entity
     */
    public com.avalon.esb.servicehelpers.Code getEntity() {
        return entity;
    }


    /**
     * Sets the entity value for this ClaimStatusEntity.
     * 
     * @param entity
     */
    public void setEntity(com.avalon.esb.servicehelpers.Code entity) {
        this.entity = entity;
    }


    /**
     * Gets the codeListQualifier value for this ClaimStatusEntity.
     * 
     * @return codeListQualifier
     */
    public com.avalon.esb.servicehelpers.Code getCodeListQualifier() {
        return codeListQualifier;
    }


    /**
     * Sets the codeListQualifier value for this ClaimStatusEntity.
     * 
     * @param codeListQualifier
     */
    public void setCodeListQualifier(com.avalon.esb.servicehelpers.Code codeListQualifier) {
        this.codeListQualifier = codeListQualifier;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ClaimStatusEntity)) return false;
        ClaimStatusEntity other = (ClaimStatusEntity) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.status==null && other.getStatus()==null) || 
             (this.status!=null &&
              this.status.equals(other.getStatus()))) &&
            ((this.statusCategory==null && other.getStatusCategory()==null) || 
             (this.statusCategory!=null &&
              this.statusCategory.equals(other.getStatusCategory()))) &&
            ((this.entity==null && other.getEntity()==null) || 
             (this.entity!=null &&
              this.entity.equals(other.getEntity()))) &&
            ((this.codeListQualifier==null && other.getCodeListQualifier()==null) || 
             (this.codeListQualifier!=null &&
              this.codeListQualifier.equals(other.getCodeListQualifier())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getStatus() != null) {
            _hashCode += getStatus().hashCode();
        }
        if (getStatusCategory() != null) {
            _hashCode += getStatusCategory().hashCode();
        }
        if (getEntity() != null) {
            _hashCode += getEntity().hashCode();
        }
        if (getCodeListQualifier() != null) {
            _hashCode += getCodeListQualifier().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ClaimStatusEntity.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://servicehelpers.esb.avalon.com", "ClaimStatusEntity"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("status");
        elemField.setXmlName(new javax.xml.namespace.QName("", "status"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://servicehelpers.esb.avalon.com", "Code"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("statusCategory");
        elemField.setXmlName(new javax.xml.namespace.QName("", "statusCategory"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://servicehelpers.esb.avalon.com", "Code"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("entity");
        elemField.setXmlName(new javax.xml.namespace.QName("", "entity"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://servicehelpers.esb.avalon.com", "Code"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codeListQualifier");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codeListQualifier"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://servicehelpers.esb.avalon.com", "Code"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
