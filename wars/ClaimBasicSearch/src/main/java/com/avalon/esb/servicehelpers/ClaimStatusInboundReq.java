/**
 * ClaimStatusInboundReq.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.avalon.esb.servicehelpers;

public class ClaimStatusInboundReq  implements java.io.Serializable {
    private java.lang.String requestType;

    private com.avalon.esb.servicehelpers.ClaimSearchCriteria claimSearchCriteriaInbound;

    private com.avalon.esb.servicehelpers.ClaimSearchFilter claimSearchFilterInbound;

    private com.avalon.esb.servicehelpers.ProviderInformation providerInfo;

    private com.avalon.esb.servicehelpers.Patient[] patients;

    public ClaimStatusInboundReq() {
    }

    public ClaimStatusInboundReq(
           java.lang.String requestType,
           com.avalon.esb.servicehelpers.ClaimSearchCriteria claimSearchCriteriaInbound,
           com.avalon.esb.servicehelpers.ClaimSearchFilter claimSearchFilterInbound,
           com.avalon.esb.servicehelpers.ProviderInformation providerInfo,
           com.avalon.esb.servicehelpers.Patient[] patients) {
           this.requestType = requestType;
           this.claimSearchCriteriaInbound = claimSearchCriteriaInbound;
           this.claimSearchFilterInbound = claimSearchFilterInbound;
           this.providerInfo = providerInfo;
           this.patients = patients;
    }


    /**
     * Gets the requestType value for this ClaimStatusInboundReq.
     * 
     * @return requestType
     */
    public java.lang.String getRequestType() {
        return requestType;
    }


    /**
     * Sets the requestType value for this ClaimStatusInboundReq.
     * 
     * @param requestType
     */
    public void setRequestType(java.lang.String requestType) {
        this.requestType = requestType;
    }


    /**
     * Gets the claimSearchCriteriaInbound value for this ClaimStatusInboundReq.
     * 
     * @return claimSearchCriteriaInbound
     */
    public com.avalon.esb.servicehelpers.ClaimSearchCriteria getClaimSearchCriteriaInbound() {
        return claimSearchCriteriaInbound;
    }


    /**
     * Sets the claimSearchCriteriaInbound value for this ClaimStatusInboundReq.
     * 
     * @param claimSearchCriteriaInbound
     */
    public void setClaimSearchCriteriaInbound(com.avalon.esb.servicehelpers.ClaimSearchCriteria claimSearchCriteriaInbound) {
        this.claimSearchCriteriaInbound = claimSearchCriteriaInbound;
    }


    /**
     * Gets the claimSearchFilterInbound value for this ClaimStatusInboundReq.
     * 
     * @return claimSearchFilterInbound
     */
    public com.avalon.esb.servicehelpers.ClaimSearchFilter getClaimSearchFilterInbound() {
        return claimSearchFilterInbound;
    }


    /**
     * Sets the claimSearchFilterInbound value for this ClaimStatusInboundReq.
     * 
     * @param claimSearchFilterInbound
     */
    public void setClaimSearchFilterInbound(com.avalon.esb.servicehelpers.ClaimSearchFilter claimSearchFilterInbound) {
        this.claimSearchFilterInbound = claimSearchFilterInbound;
    }


    /**
     * Gets the providerInfo value for this ClaimStatusInboundReq.
     * 
     * @return providerInfo
     */
    public com.avalon.esb.servicehelpers.ProviderInformation getProviderInfo() {
        return providerInfo;
    }


    /**
     * Sets the providerInfo value for this ClaimStatusInboundReq.
     * 
     * @param providerInfo
     */
    public void setProviderInfo(com.avalon.esb.servicehelpers.ProviderInformation providerInfo) {
        this.providerInfo = providerInfo;
    }


    /**
     * Gets the patients value for this ClaimStatusInboundReq.
     * 
     * @return patients
     */
    public com.avalon.esb.servicehelpers.Patient[] getPatients() {
        return patients;
    }


    /**
     * Sets the patients value for this ClaimStatusInboundReq.
     * 
     * @param patients
     */
    public void setPatients(com.avalon.esb.servicehelpers.Patient[] patients) {
        this.patients = patients;
    }

    public com.avalon.esb.servicehelpers.Patient getPatients(int i) {
        return this.patients[i];
    }

    public void setPatients(int i, com.avalon.esb.servicehelpers.Patient _value) {
        this.patients[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ClaimStatusInboundReq)) return false;
        ClaimStatusInboundReq other = (ClaimStatusInboundReq) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.requestType==null && other.getRequestType()==null) || 
             (this.requestType!=null &&
              this.requestType.equals(other.getRequestType()))) &&
            ((this.claimSearchCriteriaInbound==null && other.getClaimSearchCriteriaInbound()==null) || 
             (this.claimSearchCriteriaInbound!=null &&
              this.claimSearchCriteriaInbound.equals(other.getClaimSearchCriteriaInbound()))) &&
            ((this.claimSearchFilterInbound==null && other.getClaimSearchFilterInbound()==null) || 
             (this.claimSearchFilterInbound!=null &&
              this.claimSearchFilterInbound.equals(other.getClaimSearchFilterInbound()))) &&
            ((this.providerInfo==null && other.getProviderInfo()==null) || 
             (this.providerInfo!=null &&
              this.providerInfo.equals(other.getProviderInfo()))) &&
            ((this.patients==null && other.getPatients()==null) || 
             (this.patients!=null &&
              java.util.Arrays.equals(this.patients, other.getPatients())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getRequestType() != null) {
            _hashCode += getRequestType().hashCode();
        }
        if (getClaimSearchCriteriaInbound() != null) {
            _hashCode += getClaimSearchCriteriaInbound().hashCode();
        }
        if (getClaimSearchFilterInbound() != null) {
            _hashCode += getClaimSearchFilterInbound().hashCode();
        }
        if (getProviderInfo() != null) {
            _hashCode += getProviderInfo().hashCode();
        }
        if (getPatients() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getPatients());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getPatients(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ClaimStatusInboundReq.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://servicehelpers.esb.avalon.com", "claimStatusInboundReq"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("requestType");
        elemField.setXmlName(new javax.xml.namespace.QName("", "requestType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("claimSearchCriteriaInbound");
        elemField.setXmlName(new javax.xml.namespace.QName("", "claimSearchCriteriaInbound"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://servicehelpers.esb.avalon.com", "ClaimSearchCriteria"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("claimSearchFilterInbound");
        elemField.setXmlName(new javax.xml.namespace.QName("", "claimSearchFilterInbound"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://servicehelpers.esb.avalon.com", "ClaimSearchFilter"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("providerInfo");
        elemField.setXmlName(new javax.xml.namespace.QName("", "providerInfo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://servicehelpers.esb.avalon.com", "ProviderInformation"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("patients");
        elemField.setXmlName(new javax.xml.namespace.QName("", "patients"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://servicehelpers.esb.avalon.com", "Patient"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
