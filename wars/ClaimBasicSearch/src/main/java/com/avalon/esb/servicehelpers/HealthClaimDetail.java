/**
 * HealthClaimDetail.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.avalon.esb.servicehelpers;

public class HealthClaimDetail  implements java.io.Serializable {
    private java.lang.Boolean medicare;

    private java.math.BigDecimal medicarePayment;

    private java.util.Calendar admissionDate;

    private java.lang.String admissionHour;

    private java.lang.String dischargeHour;

    private java.util.Calendar dischargeDate;

    private com.avalon.esb.servicehelpers.Code billType;

    private com.avalon.esb.servicehelpers.Code primaryStatusCode;

    private java.lang.Boolean blueExchangeLocalClaim;

    private com.avalon.esb.servicehelpers.Code admissionDiagnosis;

    private java.lang.String[] patientReasonForVisitDiagnosis;

    private com.avalon.esb.servicehelpers.ExternalCauseInjuryDetail[] externalCauseInjuryDetails;

    private java.lang.Boolean moreExternalCauseInjuryIndicator;

    private com.avalon.esb.servicehelpers.Code icdVersionSubmitted;

    private com.avalon.esb.servicehelpers.DiagnosisCodeDetail[] submittedDiagnosisCodes;

    private com.avalon.esb.servicehelpers.ProcedureCodeDetail[] submittedProcedureCodes;

    private com.avalon.esb.servicehelpers.Code icdVersionAdjudicated;

    private com.avalon.esb.servicehelpers.DiagnosisCodeDetail[] adjudicatedDiagnosisCodes;

    private com.avalon.esb.servicehelpers.ProcedureCodeDetail[] adjudicatedProcedureCodes;

    public HealthClaimDetail() {
    }

    public HealthClaimDetail(
           java.lang.Boolean medicare,
           java.math.BigDecimal medicarePayment,
           java.util.Calendar admissionDate,
           java.lang.String admissionHour,
           java.lang.String dischargeHour,
           java.util.Calendar dischargeDate,
           com.avalon.esb.servicehelpers.Code billType,
           com.avalon.esb.servicehelpers.Code primaryStatusCode,
           java.lang.Boolean blueExchangeLocalClaim,
           com.avalon.esb.servicehelpers.Code admissionDiagnosis,
           java.lang.String[] patientReasonForVisitDiagnosis,
           com.avalon.esb.servicehelpers.ExternalCauseInjuryDetail[] externalCauseInjuryDetails,
           java.lang.Boolean moreExternalCauseInjuryIndicator,
           com.avalon.esb.servicehelpers.Code icdVersionSubmitted,
           com.avalon.esb.servicehelpers.DiagnosisCodeDetail[] submittedDiagnosisCodes,
           com.avalon.esb.servicehelpers.ProcedureCodeDetail[] submittedProcedureCodes,
           com.avalon.esb.servicehelpers.Code icdVersionAdjudicated,
           com.avalon.esb.servicehelpers.DiagnosisCodeDetail[] adjudicatedDiagnosisCodes,
           com.avalon.esb.servicehelpers.ProcedureCodeDetail[] adjudicatedProcedureCodes) {
           this.medicare = medicare;
           this.medicarePayment = medicarePayment;
           this.admissionDate = admissionDate;
           this.admissionHour = admissionHour;
           this.dischargeHour = dischargeHour;
           this.dischargeDate = dischargeDate;
           this.billType = billType;
           this.primaryStatusCode = primaryStatusCode;
           this.blueExchangeLocalClaim = blueExchangeLocalClaim;
           this.admissionDiagnosis = admissionDiagnosis;
           this.patientReasonForVisitDiagnosis = patientReasonForVisitDiagnosis;
           this.externalCauseInjuryDetails = externalCauseInjuryDetails;
           this.moreExternalCauseInjuryIndicator = moreExternalCauseInjuryIndicator;
           this.icdVersionSubmitted = icdVersionSubmitted;
           this.submittedDiagnosisCodes = submittedDiagnosisCodes;
           this.submittedProcedureCodes = submittedProcedureCodes;
           this.icdVersionAdjudicated = icdVersionAdjudicated;
           this.adjudicatedDiagnosisCodes = adjudicatedDiagnosisCodes;
           this.adjudicatedProcedureCodes = adjudicatedProcedureCodes;
    }


    /**
     * Gets the medicare value for this HealthClaimDetail.
     * 
     * @return medicare
     */
    public java.lang.Boolean getMedicare() {
        return medicare;
    }


    /**
     * Sets the medicare value for this HealthClaimDetail.
     * 
     * @param medicare
     */
    public void setMedicare(java.lang.Boolean medicare) {
        this.medicare = medicare;
    }


    /**
     * Gets the medicarePayment value for this HealthClaimDetail.
     * 
     * @return medicarePayment
     */
    public java.math.BigDecimal getMedicarePayment() {
        return medicarePayment;
    }


    /**
     * Sets the medicarePayment value for this HealthClaimDetail.
     * 
     * @param medicarePayment
     */
    public void setMedicarePayment(java.math.BigDecimal medicarePayment) {
        this.medicarePayment = medicarePayment;
    }


    /**
     * Gets the admissionDate value for this HealthClaimDetail.
     * 
     * @return admissionDate
     */
    public java.util.Calendar getAdmissionDate() {
        return admissionDate;
    }


    /**
     * Sets the admissionDate value for this HealthClaimDetail.
     * 
     * @param admissionDate
     */
    public void setAdmissionDate(java.util.Calendar admissionDate) {
        this.admissionDate = admissionDate;
    }


    /**
     * Gets the admissionHour value for this HealthClaimDetail.
     * 
     * @return admissionHour
     */
    public java.lang.String getAdmissionHour() {
        return admissionHour;
    }


    /**
     * Sets the admissionHour value for this HealthClaimDetail.
     * 
     * @param admissionHour
     */
    public void setAdmissionHour(java.lang.String admissionHour) {
        this.admissionHour = admissionHour;
    }


    /**
     * Gets the dischargeHour value for this HealthClaimDetail.
     * 
     * @return dischargeHour
     */
    public java.lang.String getDischargeHour() {
        return dischargeHour;
    }


    /**
     * Sets the dischargeHour value for this HealthClaimDetail.
     * 
     * @param dischargeHour
     */
    public void setDischargeHour(java.lang.String dischargeHour) {
        this.dischargeHour = dischargeHour;
    }


    /**
     * Gets the dischargeDate value for this HealthClaimDetail.
     * 
     * @return dischargeDate
     */
    public java.util.Calendar getDischargeDate() {
        return dischargeDate;
    }


    /**
     * Sets the dischargeDate value for this HealthClaimDetail.
     * 
     * @param dischargeDate
     */
    public void setDischargeDate(java.util.Calendar dischargeDate) {
        this.dischargeDate = dischargeDate;
    }


    /**
     * Gets the billType value for this HealthClaimDetail.
     * 
     * @return billType
     */
    public com.avalon.esb.servicehelpers.Code getBillType() {
        return billType;
    }


    /**
     * Sets the billType value for this HealthClaimDetail.
     * 
     * @param billType
     */
    public void setBillType(com.avalon.esb.servicehelpers.Code billType) {
        this.billType = billType;
    }


    /**
     * Gets the primaryStatusCode value for this HealthClaimDetail.
     * 
     * @return primaryStatusCode
     */
    public com.avalon.esb.servicehelpers.Code getPrimaryStatusCode() {
        return primaryStatusCode;
    }


    /**
     * Sets the primaryStatusCode value for this HealthClaimDetail.
     * 
     * @param primaryStatusCode
     */
    public void setPrimaryStatusCode(com.avalon.esb.servicehelpers.Code primaryStatusCode) {
        this.primaryStatusCode = primaryStatusCode;
    }


    /**
     * Gets the blueExchangeLocalClaim value for this HealthClaimDetail.
     * 
     * @return blueExchangeLocalClaim
     */
    public java.lang.Boolean getBlueExchangeLocalClaim() {
        return blueExchangeLocalClaim;
    }


    /**
     * Sets the blueExchangeLocalClaim value for this HealthClaimDetail.
     * 
     * @param blueExchangeLocalClaim
     */
    public void setBlueExchangeLocalClaim(java.lang.Boolean blueExchangeLocalClaim) {
        this.blueExchangeLocalClaim = blueExchangeLocalClaim;
    }


    /**
     * Gets the admissionDiagnosis value for this HealthClaimDetail.
     * 
     * @return admissionDiagnosis
     */
    public com.avalon.esb.servicehelpers.Code getAdmissionDiagnosis() {
        return admissionDiagnosis;
    }


    /**
     * Sets the admissionDiagnosis value for this HealthClaimDetail.
     * 
     * @param admissionDiagnosis
     */
    public void setAdmissionDiagnosis(com.avalon.esb.servicehelpers.Code admissionDiagnosis) {
        this.admissionDiagnosis = admissionDiagnosis;
    }


    /**
     * Gets the patientReasonForVisitDiagnosis value for this HealthClaimDetail.
     * 
     * @return patientReasonForVisitDiagnosis
     */
    public java.lang.String[] getPatientReasonForVisitDiagnosis() {
        return patientReasonForVisitDiagnosis;
    }


    /**
     * Sets the patientReasonForVisitDiagnosis value for this HealthClaimDetail.
     * 
     * @param patientReasonForVisitDiagnosis
     */
    public void setPatientReasonForVisitDiagnosis(java.lang.String[] patientReasonForVisitDiagnosis) {
        this.patientReasonForVisitDiagnosis = patientReasonForVisitDiagnosis;
    }

    public java.lang.String getPatientReasonForVisitDiagnosis(int i) {
        return this.patientReasonForVisitDiagnosis[i];
    }

    public void setPatientReasonForVisitDiagnosis(int i, java.lang.String _value) {
        this.patientReasonForVisitDiagnosis[i] = _value;
    }


    /**
     * Gets the externalCauseInjuryDetails value for this HealthClaimDetail.
     * 
     * @return externalCauseInjuryDetails
     */
    public com.avalon.esb.servicehelpers.ExternalCauseInjuryDetail[] getExternalCauseInjuryDetails() {
        return externalCauseInjuryDetails;
    }


    /**
     * Sets the externalCauseInjuryDetails value for this HealthClaimDetail.
     * 
     * @param externalCauseInjuryDetails
     */
    public void setExternalCauseInjuryDetails(com.avalon.esb.servicehelpers.ExternalCauseInjuryDetail[] externalCauseInjuryDetails) {
        this.externalCauseInjuryDetails = externalCauseInjuryDetails;
    }

    public com.avalon.esb.servicehelpers.ExternalCauseInjuryDetail getExternalCauseInjuryDetails(int i) {
        return this.externalCauseInjuryDetails[i];
    }

    public void setExternalCauseInjuryDetails(int i, com.avalon.esb.servicehelpers.ExternalCauseInjuryDetail _value) {
        this.externalCauseInjuryDetails[i] = _value;
    }


    /**
     * Gets the moreExternalCauseInjuryIndicator value for this HealthClaimDetail.
     * 
     * @return moreExternalCauseInjuryIndicator
     */
    public java.lang.Boolean getMoreExternalCauseInjuryIndicator() {
        return moreExternalCauseInjuryIndicator;
    }


    /**
     * Sets the moreExternalCauseInjuryIndicator value for this HealthClaimDetail.
     * 
     * @param moreExternalCauseInjuryIndicator
     */
    public void setMoreExternalCauseInjuryIndicator(java.lang.Boolean moreExternalCauseInjuryIndicator) {
        this.moreExternalCauseInjuryIndicator = moreExternalCauseInjuryIndicator;
    }


    /**
     * Gets the icdVersionSubmitted value for this HealthClaimDetail.
     * 
     * @return icdVersionSubmitted
     */
    public com.avalon.esb.servicehelpers.Code getIcdVersionSubmitted() {
        return icdVersionSubmitted;
    }


    /**
     * Sets the icdVersionSubmitted value for this HealthClaimDetail.
     * 
     * @param icdVersionSubmitted
     */
    public void setIcdVersionSubmitted(com.avalon.esb.servicehelpers.Code icdVersionSubmitted) {
        this.icdVersionSubmitted = icdVersionSubmitted;
    }


    /**
     * Gets the submittedDiagnosisCodes value for this HealthClaimDetail.
     * 
     * @return submittedDiagnosisCodes
     */
    public com.avalon.esb.servicehelpers.DiagnosisCodeDetail[] getSubmittedDiagnosisCodes() {
        return submittedDiagnosisCodes;
    }


    /**
     * Sets the submittedDiagnosisCodes value for this HealthClaimDetail.
     * 
     * @param submittedDiagnosisCodes
     */
    public void setSubmittedDiagnosisCodes(com.avalon.esb.servicehelpers.DiagnosisCodeDetail[] submittedDiagnosisCodes) {
        this.submittedDiagnosisCodes = submittedDiagnosisCodes;
    }

    public com.avalon.esb.servicehelpers.DiagnosisCodeDetail getSubmittedDiagnosisCodes(int i) {
        return this.submittedDiagnosisCodes[i];
    }

    public void setSubmittedDiagnosisCodes(int i, com.avalon.esb.servicehelpers.DiagnosisCodeDetail _value) {
        this.submittedDiagnosisCodes[i] = _value;
    }


    /**
     * Gets the submittedProcedureCodes value for this HealthClaimDetail.
     * 
     * @return submittedProcedureCodes
     */
    public com.avalon.esb.servicehelpers.ProcedureCodeDetail[] getSubmittedProcedureCodes() {
        return submittedProcedureCodes;
    }


    /**
     * Sets the submittedProcedureCodes value for this HealthClaimDetail.
     * 
     * @param submittedProcedureCodes
     */
    public void setSubmittedProcedureCodes(com.avalon.esb.servicehelpers.ProcedureCodeDetail[] submittedProcedureCodes) {
        this.submittedProcedureCodes = submittedProcedureCodes;
    }

    public com.avalon.esb.servicehelpers.ProcedureCodeDetail getSubmittedProcedureCodes(int i) {
        return this.submittedProcedureCodes[i];
    }

    public void setSubmittedProcedureCodes(int i, com.avalon.esb.servicehelpers.ProcedureCodeDetail _value) {
        this.submittedProcedureCodes[i] = _value;
    }


    /**
     * Gets the icdVersionAdjudicated value for this HealthClaimDetail.
     * 
     * @return icdVersionAdjudicated
     */
    public com.avalon.esb.servicehelpers.Code getIcdVersionAdjudicated() {
        return icdVersionAdjudicated;
    }


    /**
     * Sets the icdVersionAdjudicated value for this HealthClaimDetail.
     * 
     * @param icdVersionAdjudicated
     */
    public void setIcdVersionAdjudicated(com.avalon.esb.servicehelpers.Code icdVersionAdjudicated) {
        this.icdVersionAdjudicated = icdVersionAdjudicated;
    }


    /**
     * Gets the adjudicatedDiagnosisCodes value for this HealthClaimDetail.
     * 
     * @return adjudicatedDiagnosisCodes
     */
    public com.avalon.esb.servicehelpers.DiagnosisCodeDetail[] getAdjudicatedDiagnosisCodes() {
        return adjudicatedDiagnosisCodes;
    }


    /**
     * Sets the adjudicatedDiagnosisCodes value for this HealthClaimDetail.
     * 
     * @param adjudicatedDiagnosisCodes
     */
    public void setAdjudicatedDiagnosisCodes(com.avalon.esb.servicehelpers.DiagnosisCodeDetail[] adjudicatedDiagnosisCodes) {
        this.adjudicatedDiagnosisCodes = adjudicatedDiagnosisCodes;
    }

    public com.avalon.esb.servicehelpers.DiagnosisCodeDetail getAdjudicatedDiagnosisCodes(int i) {
        return this.adjudicatedDiagnosisCodes[i];
    }

    public void setAdjudicatedDiagnosisCodes(int i, com.avalon.esb.servicehelpers.DiagnosisCodeDetail _value) {
        this.adjudicatedDiagnosisCodes[i] = _value;
    }


    /**
     * Gets the adjudicatedProcedureCodes value for this HealthClaimDetail.
     * 
     * @return adjudicatedProcedureCodes
     */
    public com.avalon.esb.servicehelpers.ProcedureCodeDetail[] getAdjudicatedProcedureCodes() {
        return adjudicatedProcedureCodes;
    }


    /**
     * Sets the adjudicatedProcedureCodes value for this HealthClaimDetail.
     * 
     * @param adjudicatedProcedureCodes
     */
    public void setAdjudicatedProcedureCodes(com.avalon.esb.servicehelpers.ProcedureCodeDetail[] adjudicatedProcedureCodes) {
        this.adjudicatedProcedureCodes = adjudicatedProcedureCodes;
    }

    public com.avalon.esb.servicehelpers.ProcedureCodeDetail getAdjudicatedProcedureCodes(int i) {
        return this.adjudicatedProcedureCodes[i];
    }

    public void setAdjudicatedProcedureCodes(int i, com.avalon.esb.servicehelpers.ProcedureCodeDetail _value) {
        this.adjudicatedProcedureCodes[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof HealthClaimDetail)) return false;
        HealthClaimDetail other = (HealthClaimDetail) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.medicare==null && other.getMedicare()==null) || 
             (this.medicare!=null &&
              this.medicare.equals(other.getMedicare()))) &&
            ((this.medicarePayment==null && other.getMedicarePayment()==null) || 
             (this.medicarePayment!=null &&
              this.medicarePayment.equals(other.getMedicarePayment()))) &&
            ((this.admissionDate==null && other.getAdmissionDate()==null) || 
             (this.admissionDate!=null &&
              this.admissionDate.equals(other.getAdmissionDate()))) &&
            ((this.admissionHour==null && other.getAdmissionHour()==null) || 
             (this.admissionHour!=null &&
              this.admissionHour.equals(other.getAdmissionHour()))) &&
            ((this.dischargeHour==null && other.getDischargeHour()==null) || 
             (this.dischargeHour!=null &&
              this.dischargeHour.equals(other.getDischargeHour()))) &&
            ((this.dischargeDate==null && other.getDischargeDate()==null) || 
             (this.dischargeDate!=null &&
              this.dischargeDate.equals(other.getDischargeDate()))) &&
            ((this.billType==null && other.getBillType()==null) || 
             (this.billType!=null &&
              this.billType.equals(other.getBillType()))) &&
            ((this.primaryStatusCode==null && other.getPrimaryStatusCode()==null) || 
             (this.primaryStatusCode!=null &&
              this.primaryStatusCode.equals(other.getPrimaryStatusCode()))) &&
            ((this.blueExchangeLocalClaim==null && other.getBlueExchangeLocalClaim()==null) || 
             (this.blueExchangeLocalClaim!=null &&
              this.blueExchangeLocalClaim.equals(other.getBlueExchangeLocalClaim()))) &&
            ((this.admissionDiagnosis==null && other.getAdmissionDiagnosis()==null) || 
             (this.admissionDiagnosis!=null &&
              this.admissionDiagnosis.equals(other.getAdmissionDiagnosis()))) &&
            ((this.patientReasonForVisitDiagnosis==null && other.getPatientReasonForVisitDiagnosis()==null) || 
             (this.patientReasonForVisitDiagnosis!=null &&
              java.util.Arrays.equals(this.patientReasonForVisitDiagnosis, other.getPatientReasonForVisitDiagnosis()))) &&
            ((this.externalCauseInjuryDetails==null && other.getExternalCauseInjuryDetails()==null) || 
             (this.externalCauseInjuryDetails!=null &&
              java.util.Arrays.equals(this.externalCauseInjuryDetails, other.getExternalCauseInjuryDetails()))) &&
            ((this.moreExternalCauseInjuryIndicator==null && other.getMoreExternalCauseInjuryIndicator()==null) || 
             (this.moreExternalCauseInjuryIndicator!=null &&
              this.moreExternalCauseInjuryIndicator.equals(other.getMoreExternalCauseInjuryIndicator()))) &&
            ((this.icdVersionSubmitted==null && other.getIcdVersionSubmitted()==null) || 
             (this.icdVersionSubmitted!=null &&
              this.icdVersionSubmitted.equals(other.getIcdVersionSubmitted()))) &&
            ((this.submittedDiagnosisCodes==null && other.getSubmittedDiagnosisCodes()==null) || 
             (this.submittedDiagnosisCodes!=null &&
              java.util.Arrays.equals(this.submittedDiagnosisCodes, other.getSubmittedDiagnosisCodes()))) &&
            ((this.submittedProcedureCodes==null && other.getSubmittedProcedureCodes()==null) || 
             (this.submittedProcedureCodes!=null &&
              java.util.Arrays.equals(this.submittedProcedureCodes, other.getSubmittedProcedureCodes()))) &&
            ((this.icdVersionAdjudicated==null && other.getIcdVersionAdjudicated()==null) || 
             (this.icdVersionAdjudicated!=null &&
              this.icdVersionAdjudicated.equals(other.getIcdVersionAdjudicated()))) &&
            ((this.adjudicatedDiagnosisCodes==null && other.getAdjudicatedDiagnosisCodes()==null) || 
             (this.adjudicatedDiagnosisCodes!=null &&
              java.util.Arrays.equals(this.adjudicatedDiagnosisCodes, other.getAdjudicatedDiagnosisCodes()))) &&
            ((this.adjudicatedProcedureCodes==null && other.getAdjudicatedProcedureCodes()==null) || 
             (this.adjudicatedProcedureCodes!=null &&
              java.util.Arrays.equals(this.adjudicatedProcedureCodes, other.getAdjudicatedProcedureCodes())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getMedicare() != null) {
            _hashCode += getMedicare().hashCode();
        }
        if (getMedicarePayment() != null) {
            _hashCode += getMedicarePayment().hashCode();
        }
        if (getAdmissionDate() != null) {
            _hashCode += getAdmissionDate().hashCode();
        }
        if (getAdmissionHour() != null) {
            _hashCode += getAdmissionHour().hashCode();
        }
        if (getDischargeHour() != null) {
            _hashCode += getDischargeHour().hashCode();
        }
        if (getDischargeDate() != null) {
            _hashCode += getDischargeDate().hashCode();
        }
        if (getBillType() != null) {
            _hashCode += getBillType().hashCode();
        }
        if (getPrimaryStatusCode() != null) {
            _hashCode += getPrimaryStatusCode().hashCode();
        }
        if (getBlueExchangeLocalClaim() != null) {
            _hashCode += getBlueExchangeLocalClaim().hashCode();
        }
        if (getAdmissionDiagnosis() != null) {
            _hashCode += getAdmissionDiagnosis().hashCode();
        }
        if (getPatientReasonForVisitDiagnosis() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getPatientReasonForVisitDiagnosis());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getPatientReasonForVisitDiagnosis(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getExternalCauseInjuryDetails() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getExternalCauseInjuryDetails());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getExternalCauseInjuryDetails(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getMoreExternalCauseInjuryIndicator() != null) {
            _hashCode += getMoreExternalCauseInjuryIndicator().hashCode();
        }
        if (getIcdVersionSubmitted() != null) {
            _hashCode += getIcdVersionSubmitted().hashCode();
        }
        if (getSubmittedDiagnosisCodes() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getSubmittedDiagnosisCodes());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getSubmittedDiagnosisCodes(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getSubmittedProcedureCodes() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getSubmittedProcedureCodes());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getSubmittedProcedureCodes(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getIcdVersionAdjudicated() != null) {
            _hashCode += getIcdVersionAdjudicated().hashCode();
        }
        if (getAdjudicatedDiagnosisCodes() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getAdjudicatedDiagnosisCodes());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getAdjudicatedDiagnosisCodes(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getAdjudicatedProcedureCodes() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getAdjudicatedProcedureCodes());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getAdjudicatedProcedureCodes(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(HealthClaimDetail.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://servicehelpers.esb.avalon.com", "HealthClaimDetail"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("medicare");
        elemField.setXmlName(new javax.xml.namespace.QName("", "medicare"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("medicarePayment");
        elemField.setXmlName(new javax.xml.namespace.QName("", "medicarePayment"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("admissionDate");
        elemField.setXmlName(new javax.xml.namespace.QName("", "admissionDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("admissionHour");
        elemField.setXmlName(new javax.xml.namespace.QName("", "admissionHour"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dischargeHour");
        elemField.setXmlName(new javax.xml.namespace.QName("", "dischargeHour"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dischargeDate");
        elemField.setXmlName(new javax.xml.namespace.QName("", "dischargeDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("billType");
        elemField.setXmlName(new javax.xml.namespace.QName("", "billType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://servicehelpers.esb.avalon.com", "Code"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("primaryStatusCode");
        elemField.setXmlName(new javax.xml.namespace.QName("", "primaryStatusCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://servicehelpers.esb.avalon.com", "Code"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("blueExchangeLocalClaim");
        elemField.setXmlName(new javax.xml.namespace.QName("", "blueExchangeLocalClaim"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("admissionDiagnosis");
        elemField.setXmlName(new javax.xml.namespace.QName("", "admissionDiagnosis"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://servicehelpers.esb.avalon.com", "Code"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("patientReasonForVisitDiagnosis");
        elemField.setXmlName(new javax.xml.namespace.QName("", "patientReasonForVisitDiagnosis"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("externalCauseInjuryDetails");
        elemField.setXmlName(new javax.xml.namespace.QName("", "externalCauseInjuryDetails"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://servicehelpers.esb.avalon.com", "ExternalCauseInjuryDetail"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("moreExternalCauseInjuryIndicator");
        elemField.setXmlName(new javax.xml.namespace.QName("", "moreExternalCauseInjuryIndicator"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("icdVersionSubmitted");
        elemField.setXmlName(new javax.xml.namespace.QName("", "icdVersionSubmitted"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://servicehelpers.esb.avalon.com", "Code"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("submittedDiagnosisCodes");
        elemField.setXmlName(new javax.xml.namespace.QName("", "submittedDiagnosisCodes"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://servicehelpers.esb.avalon.com", "DiagnosisCodeDetail"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("submittedProcedureCodes");
        elemField.setXmlName(new javax.xml.namespace.QName("", "submittedProcedureCodes"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://servicehelpers.esb.avalon.com", "ProcedureCodeDetail"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("icdVersionAdjudicated");
        elemField.setXmlName(new javax.xml.namespace.QName("", "icdVersionAdjudicated"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://servicehelpers.esb.avalon.com", "Code"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("adjudicatedDiagnosisCodes");
        elemField.setXmlName(new javax.xml.namespace.QName("", "adjudicatedDiagnosisCodes"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://servicehelpers.esb.avalon.com", "DiagnosisCodeDetail"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("adjudicatedProcedureCodes");
        elemField.setXmlName(new javax.xml.namespace.QName("", "adjudicatedProcedureCodes"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://servicehelpers.esb.avalon.com", "ProcedureCodeDetail"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
