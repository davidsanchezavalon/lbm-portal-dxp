/**
 * ProviderIdentifier.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.avalon.esb.servicehelpers;

public class ProviderIdentifier  implements java.io.Serializable {
    private java.lang.String federalTaxIdNumber;

    private java.lang.String locationNumber;

    private java.lang.String nationalProviderIdentifier;

    public ProviderIdentifier() {
    }

    public ProviderIdentifier(
           java.lang.String federalTaxIdNumber,
           java.lang.String locationNumber,
           java.lang.String nationalProviderIdentifier) {
           this.federalTaxIdNumber = federalTaxIdNumber;
           this.locationNumber = locationNumber;
           this.nationalProviderIdentifier = nationalProviderIdentifier;
    }


    /**
     * Gets the federalTaxIdNumber value for this ProviderIdentifier.
     * 
     * @return federalTaxIdNumber
     */
    public java.lang.String getFederalTaxIdNumber() {
        return federalTaxIdNumber;
    }


    /**
     * Sets the federalTaxIdNumber value for this ProviderIdentifier.
     * 
     * @param federalTaxIdNumber
     */
    public void setFederalTaxIdNumber(java.lang.String federalTaxIdNumber) {
        this.federalTaxIdNumber = federalTaxIdNumber;
    }


    /**
     * Gets the locationNumber value for this ProviderIdentifier.
     * 
     * @return locationNumber
     */
    public java.lang.String getLocationNumber() {
        return locationNumber;
    }


    /**
     * Sets the locationNumber value for this ProviderIdentifier.
     * 
     * @param locationNumber
     */
    public void setLocationNumber(java.lang.String locationNumber) {
        this.locationNumber = locationNumber;
    }


    /**
     * Gets the nationalProviderIdentifier value for this ProviderIdentifier.
     * 
     * @return nationalProviderIdentifier
     */
    public java.lang.String getNationalProviderIdentifier() {
        return nationalProviderIdentifier;
    }


    /**
     * Sets the nationalProviderIdentifier value for this ProviderIdentifier.
     * 
     * @param nationalProviderIdentifier
     */
    public void setNationalProviderIdentifier(java.lang.String nationalProviderIdentifier) {
        this.nationalProviderIdentifier = nationalProviderIdentifier;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ProviderIdentifier)) return false;
        ProviderIdentifier other = (ProviderIdentifier) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.federalTaxIdNumber==null && other.getFederalTaxIdNumber()==null) || 
             (this.federalTaxIdNumber!=null &&
              this.federalTaxIdNumber.equals(other.getFederalTaxIdNumber()))) &&
            ((this.locationNumber==null && other.getLocationNumber()==null) || 
             (this.locationNumber!=null &&
              this.locationNumber.equals(other.getLocationNumber()))) &&
            ((this.nationalProviderIdentifier==null && other.getNationalProviderIdentifier()==null) || 
             (this.nationalProviderIdentifier!=null &&
              this.nationalProviderIdentifier.equals(other.getNationalProviderIdentifier())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getFederalTaxIdNumber() != null) {
            _hashCode += getFederalTaxIdNumber().hashCode();
        }
        if (getLocationNumber() != null) {
            _hashCode += getLocationNumber().hashCode();
        }
        if (getNationalProviderIdentifier() != null) {
            _hashCode += getNationalProviderIdentifier().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ProviderIdentifier.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://servicehelpers.esb.avalon.com", "ProviderIdentifier"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("federalTaxIdNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("", "federalTaxIdNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("locationNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("", "locationNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nationalProviderIdentifier");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nationalProviderIdentifier"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
