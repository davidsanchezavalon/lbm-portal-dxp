/**
 * CoverageInformation.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.avalon.esb.servicehelpers;

public class CoverageInformation  implements java.io.Serializable {
    private java.lang.String dependentId;

    private java.lang.String cesMemberNumber;

    private com.avalon.esb.servicehelpers.Code sexRelationship;

    private boolean privacyProtectionEnabled;

    private com.avalon.esb.servicehelpers.Code coverage;

    private boolean covered;

    private com.avalon.esb.servicehelpers.PeriodDate coveragePeriod;

    public CoverageInformation() {
    }

    public CoverageInformation(
           java.lang.String dependentId,
           java.lang.String cesMemberNumber,
           com.avalon.esb.servicehelpers.Code sexRelationship,
           boolean privacyProtectionEnabled,
           com.avalon.esb.servicehelpers.Code coverage,
           boolean covered,
           com.avalon.esb.servicehelpers.PeriodDate coveragePeriod) {
           this.dependentId = dependentId;
           this.cesMemberNumber = cesMemberNumber;
           this.sexRelationship = sexRelationship;
           this.privacyProtectionEnabled = privacyProtectionEnabled;
           this.coverage = coverage;
           this.covered = covered;
           this.coveragePeriod = coveragePeriod;
    }


    /**
     * Gets the dependentId value for this CoverageInformation.
     * 
     * @return dependentId
     */
    public java.lang.String getDependentId() {
        return dependentId;
    }


    /**
     * Sets the dependentId value for this CoverageInformation.
     * 
     * @param dependentId
     */
    public void setDependentId(java.lang.String dependentId) {
        this.dependentId = dependentId;
    }


    /**
     * Gets the cesMemberNumber value for this CoverageInformation.
     * 
     * @return cesMemberNumber
     */
    public java.lang.String getCesMemberNumber() {
        return cesMemberNumber;
    }


    /**
     * Sets the cesMemberNumber value for this CoverageInformation.
     * 
     * @param cesMemberNumber
     */
    public void setCesMemberNumber(java.lang.String cesMemberNumber) {
        this.cesMemberNumber = cesMemberNumber;
    }


    /**
     * Gets the sexRelationship value for this CoverageInformation.
     * 
     * @return sexRelationship
     */
    public com.avalon.esb.servicehelpers.Code getSexRelationship() {
        return sexRelationship;
    }


    /**
     * Sets the sexRelationship value for this CoverageInformation.
     * 
     * @param sexRelationship
     */
    public void setSexRelationship(com.avalon.esb.servicehelpers.Code sexRelationship) {
        this.sexRelationship = sexRelationship;
    }


    /**
     * Gets the privacyProtectionEnabled value for this CoverageInformation.
     * 
     * @return privacyProtectionEnabled
     */
    public boolean isPrivacyProtectionEnabled() {
        return privacyProtectionEnabled;
    }


    /**
     * Sets the privacyProtectionEnabled value for this CoverageInformation.
     * 
     * @param privacyProtectionEnabled
     */
    public void setPrivacyProtectionEnabled(boolean privacyProtectionEnabled) {
        this.privacyProtectionEnabled = privacyProtectionEnabled;
    }


    /**
     * Gets the coverage value for this CoverageInformation.
     * 
     * @return coverage
     */
    public com.avalon.esb.servicehelpers.Code getCoverage() {
        return coverage;
    }


    /**
     * Sets the coverage value for this CoverageInformation.
     * 
     * @param coverage
     */
    public void setCoverage(com.avalon.esb.servicehelpers.Code coverage) {
        this.coverage = coverage;
    }


    /**
     * Gets the covered value for this CoverageInformation.
     * 
     * @return covered
     */
    public boolean isCovered() {
        return covered;
    }


    /**
     * Sets the covered value for this CoverageInformation.
     * 
     * @param covered
     */
    public void setCovered(boolean covered) {
        this.covered = covered;
    }


    /**
     * Gets the coveragePeriod value for this CoverageInformation.
     * 
     * @return coveragePeriod
     */
    public com.avalon.esb.servicehelpers.PeriodDate getCoveragePeriod() {
        return coveragePeriod;
    }


    /**
     * Sets the coveragePeriod value for this CoverageInformation.
     * 
     * @param coveragePeriod
     */
    public void setCoveragePeriod(com.avalon.esb.servicehelpers.PeriodDate coveragePeriod) {
        this.coveragePeriod = coveragePeriod;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CoverageInformation)) return false;
        CoverageInformation other = (CoverageInformation) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.dependentId==null && other.getDependentId()==null) || 
             (this.dependentId!=null &&
              this.dependentId.equals(other.getDependentId()))) &&
            ((this.cesMemberNumber==null && other.getCesMemberNumber()==null) || 
             (this.cesMemberNumber!=null &&
              this.cesMemberNumber.equals(other.getCesMemberNumber()))) &&
            ((this.sexRelationship==null && other.getSexRelationship()==null) || 
             (this.sexRelationship!=null &&
              this.sexRelationship.equals(other.getSexRelationship()))) &&
            this.privacyProtectionEnabled == other.isPrivacyProtectionEnabled() &&
            ((this.coverage==null && other.getCoverage()==null) || 
             (this.coverage!=null &&
              this.coverage.equals(other.getCoverage()))) &&
            this.covered == other.isCovered() &&
            ((this.coveragePeriod==null && other.getCoveragePeriod()==null) || 
             (this.coveragePeriod!=null &&
              this.coveragePeriod.equals(other.getCoveragePeriod())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getDependentId() != null) {
            _hashCode += getDependentId().hashCode();
        }
        if (getCesMemberNumber() != null) {
            _hashCode += getCesMemberNumber().hashCode();
        }
        if (getSexRelationship() != null) {
            _hashCode += getSexRelationship().hashCode();
        }
        _hashCode += (isPrivacyProtectionEnabled() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        if (getCoverage() != null) {
            _hashCode += getCoverage().hashCode();
        }
        _hashCode += (isCovered() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        if (getCoveragePeriod() != null) {
            _hashCode += getCoveragePeriod().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CoverageInformation.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://servicehelpers.esb.avalon.com", "CoverageInformation"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dependentId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "dependentId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cesMemberNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cesMemberNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sexRelationship");
        elemField.setXmlName(new javax.xml.namespace.QName("", "sexRelationship"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://servicehelpers.esb.avalon.com", "Code"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("privacyProtectionEnabled");
        elemField.setXmlName(new javax.xml.namespace.QName("", "privacyProtectionEnabled"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("coverage");
        elemField.setXmlName(new javax.xml.namespace.QName("", "coverage"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://servicehelpers.esb.avalon.com", "Code"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("covered");
        elemField.setXmlName(new javax.xml.namespace.QName("", "covered"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("coveragePeriod");
        elemField.setXmlName(new javax.xml.namespace.QName("", "coveragePeriod"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://servicehelpers.esb.avalon.com", "PeriodDate"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
