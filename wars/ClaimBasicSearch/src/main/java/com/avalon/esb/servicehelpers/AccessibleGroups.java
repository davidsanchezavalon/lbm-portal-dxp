/**
 * AccessibleGroups.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.avalon.esb.servicehelpers;

public class AccessibleGroups  implements java.io.Serializable {
    private com.avalon.esb.servicehelpers.GroupNumberRange[] groupRanges;

    private java.lang.String[] cesGroupNumbers;

    private java.lang.String[] groupNumbers;

    public AccessibleGroups() {
    }

    public AccessibleGroups(
           com.avalon.esb.servicehelpers.GroupNumberRange[] groupRanges,
           java.lang.String[] cesGroupNumbers,
           java.lang.String[] groupNumbers) {
           this.groupRanges = groupRanges;
           this.cesGroupNumbers = cesGroupNumbers;
           this.groupNumbers = groupNumbers;
    }


    /**
     * Gets the groupRanges value for this AccessibleGroups.
     * 
     * @return groupRanges
     */
    public com.avalon.esb.servicehelpers.GroupNumberRange[] getGroupRanges() {
        return groupRanges;
    }


    /**
     * Sets the groupRanges value for this AccessibleGroups.
     * 
     * @param groupRanges
     */
    public void setGroupRanges(com.avalon.esb.servicehelpers.GroupNumberRange[] groupRanges) {
        this.groupRanges = groupRanges;
    }

    public com.avalon.esb.servicehelpers.GroupNumberRange getGroupRanges(int i) {
        return this.groupRanges[i];
    }

    public void setGroupRanges(int i, com.avalon.esb.servicehelpers.GroupNumberRange _value) {
        this.groupRanges[i] = _value;
    }


    /**
     * Gets the cesGroupNumbers value for this AccessibleGroups.
     * 
     * @return cesGroupNumbers
     */
    public java.lang.String[] getCesGroupNumbers() {
        return cesGroupNumbers;
    }


    /**
     * Sets the cesGroupNumbers value for this AccessibleGroups.
     * 
     * @param cesGroupNumbers
     */
    public void setCesGroupNumbers(java.lang.String[] cesGroupNumbers) {
        this.cesGroupNumbers = cesGroupNumbers;
    }

    public java.lang.String getCesGroupNumbers(int i) {
        return this.cesGroupNumbers[i];
    }

    public void setCesGroupNumbers(int i, java.lang.String _value) {
        this.cesGroupNumbers[i] = _value;
    }


    /**
     * Gets the groupNumbers value for this AccessibleGroups.
     * 
     * @return groupNumbers
     */
    public java.lang.String[] getGroupNumbers() {
        return groupNumbers;
    }


    /**
     * Sets the groupNumbers value for this AccessibleGroups.
     * 
     * @param groupNumbers
     */
    public void setGroupNumbers(java.lang.String[] groupNumbers) {
        this.groupNumbers = groupNumbers;
    }

    public java.lang.String getGroupNumbers(int i) {
        return this.groupNumbers[i];
    }

    public void setGroupNumbers(int i, java.lang.String _value) {
        this.groupNumbers[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof AccessibleGroups)) return false;
        AccessibleGroups other = (AccessibleGroups) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.groupRanges==null && other.getGroupRanges()==null) || 
             (this.groupRanges!=null &&
              java.util.Arrays.equals(this.groupRanges, other.getGroupRanges()))) &&
            ((this.cesGroupNumbers==null && other.getCesGroupNumbers()==null) || 
             (this.cesGroupNumbers!=null &&
              java.util.Arrays.equals(this.cesGroupNumbers, other.getCesGroupNumbers()))) &&
            ((this.groupNumbers==null && other.getGroupNumbers()==null) || 
             (this.groupNumbers!=null &&
              java.util.Arrays.equals(this.groupNumbers, other.getGroupNumbers())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getGroupRanges() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getGroupRanges());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getGroupRanges(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getCesGroupNumbers() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getCesGroupNumbers());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getCesGroupNumbers(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getGroupNumbers() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getGroupNumbers());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getGroupNumbers(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(AccessibleGroups.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://servicehelpers.esb.avalon.com", "AccessibleGroups"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("groupRanges");
        elemField.setXmlName(new javax.xml.namespace.QName("", "groupRanges"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://servicehelpers.esb.avalon.com", "GroupNumberRange"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cesGroupNumbers");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cesGroupNumbers"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("groupNumbers");
        elemField.setXmlName(new javax.xml.namespace.QName("", "groupNumbers"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
