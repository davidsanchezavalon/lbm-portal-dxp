/**
 * ClaimDetailSearchCriteria.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.avalon.esb.servicehelpers;

public class ClaimDetailSearchCriteria  implements java.io.Serializable {
    private com.avalon.esb.servicehelpers.AccessibleGroups employerAccessibleGroups;

    private com.avalon.esb.servicehelpers.ProviderIdentifier providerIdentifier;

    public ClaimDetailSearchCriteria() {
    }

    public ClaimDetailSearchCriteria(
           com.avalon.esb.servicehelpers.AccessibleGroups employerAccessibleGroups,
           com.avalon.esb.servicehelpers.ProviderIdentifier providerIdentifier) {
           this.employerAccessibleGroups = employerAccessibleGroups;
           this.providerIdentifier = providerIdentifier;
    }


    /**
     * Gets the employerAccessibleGroups value for this ClaimDetailSearchCriteria.
     * 
     * @return employerAccessibleGroups
     */
    public com.avalon.esb.servicehelpers.AccessibleGroups getEmployerAccessibleGroups() {
        return employerAccessibleGroups;
    }


    /**
     * Sets the employerAccessibleGroups value for this ClaimDetailSearchCriteria.
     * 
     * @param employerAccessibleGroups
     */
    public void setEmployerAccessibleGroups(com.avalon.esb.servicehelpers.AccessibleGroups employerAccessibleGroups) {
        this.employerAccessibleGroups = employerAccessibleGroups;
    }


    /**
     * Gets the providerIdentifier value for this ClaimDetailSearchCriteria.
     * 
     * @return providerIdentifier
     */
    public com.avalon.esb.servicehelpers.ProviderIdentifier getProviderIdentifier() {
        return providerIdentifier;
    }


    /**
     * Sets the providerIdentifier value for this ClaimDetailSearchCriteria.
     * 
     * @param providerIdentifier
     */
    public void setProviderIdentifier(com.avalon.esb.servicehelpers.ProviderIdentifier providerIdentifier) {
        this.providerIdentifier = providerIdentifier;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ClaimDetailSearchCriteria)) return false;
        ClaimDetailSearchCriteria other = (ClaimDetailSearchCriteria) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.employerAccessibleGroups==null && other.getEmployerAccessibleGroups()==null) || 
             (this.employerAccessibleGroups!=null &&
              this.employerAccessibleGroups.equals(other.getEmployerAccessibleGroups()))) &&
            ((this.providerIdentifier==null && other.getProviderIdentifier()==null) || 
             (this.providerIdentifier!=null &&
              this.providerIdentifier.equals(other.getProviderIdentifier())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getEmployerAccessibleGroups() != null) {
            _hashCode += getEmployerAccessibleGroups().hashCode();
        }
        if (getProviderIdentifier() != null) {
            _hashCode += getProviderIdentifier().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ClaimDetailSearchCriteria.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://servicehelpers.esb.avalon.com", "ClaimDetailSearchCriteria"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("employerAccessibleGroups");
        elemField.setXmlName(new javax.xml.namespace.QName("", "employerAccessibleGroups"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://servicehelpers.esb.avalon.com", "AccessibleGroups"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("providerIdentifier");
        elemField.setXmlName(new javax.xml.namespace.QName("", "providerIdentifier"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://servicehelpers.esb.avalon.com", "ProviderIdentifier"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
