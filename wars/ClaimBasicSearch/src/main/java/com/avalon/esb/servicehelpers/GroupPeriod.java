/**
 * GroupPeriod.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.avalon.esb.servicehelpers;

public class GroupPeriod  implements java.io.Serializable {
    private java.lang.String groupNumber;

    private java.lang.String cesGroupNumber;

    private java.lang.String groupName;

    private com.avalon.esb.servicehelpers.PeriodDate[] periods;

    public GroupPeriod() {
    }

    public GroupPeriod(
           java.lang.String groupNumber,
           java.lang.String cesGroupNumber,
           java.lang.String groupName,
           com.avalon.esb.servicehelpers.PeriodDate[] periods) {
           this.groupNumber = groupNumber;
           this.cesGroupNumber = cesGroupNumber;
           this.groupName = groupName;
           this.periods = periods;
    }


    /**
     * Gets the groupNumber value for this GroupPeriod.
     * 
     * @return groupNumber
     */
    public java.lang.String getGroupNumber() {
        return groupNumber;
    }


    /**
     * Sets the groupNumber value for this GroupPeriod.
     * 
     * @param groupNumber
     */
    public void setGroupNumber(java.lang.String groupNumber) {
        this.groupNumber = groupNumber;
    }


    /**
     * Gets the cesGroupNumber value for this GroupPeriod.
     * 
     * @return cesGroupNumber
     */
    public java.lang.String getCesGroupNumber() {
        return cesGroupNumber;
    }


    /**
     * Sets the cesGroupNumber value for this GroupPeriod.
     * 
     * @param cesGroupNumber
     */
    public void setCesGroupNumber(java.lang.String cesGroupNumber) {
        this.cesGroupNumber = cesGroupNumber;
    }


    /**
     * Gets the groupName value for this GroupPeriod.
     * 
     * @return groupName
     */
    public java.lang.String getGroupName() {
        return groupName;
    }


    /**
     * Sets the groupName value for this GroupPeriod.
     * 
     * @param groupName
     */
    public void setGroupName(java.lang.String groupName) {
        this.groupName = groupName;
    }


    /**
     * Gets the periods value for this GroupPeriod.
     * 
     * @return periods
     */
    public com.avalon.esb.servicehelpers.PeriodDate[] getPeriods() {
        return periods;
    }


    /**
     * Sets the periods value for this GroupPeriod.
     * 
     * @param periods
     */
    public void setPeriods(com.avalon.esb.servicehelpers.PeriodDate[] periods) {
        this.periods = periods;
    }

    public com.avalon.esb.servicehelpers.PeriodDate getPeriods(int i) {
        return this.periods[i];
    }

    public void setPeriods(int i, com.avalon.esb.servicehelpers.PeriodDate _value) {
        this.periods[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GroupPeriod)) return false;
        GroupPeriod other = (GroupPeriod) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.groupNumber==null && other.getGroupNumber()==null) || 
             (this.groupNumber!=null &&
              this.groupNumber.equals(other.getGroupNumber()))) &&
            ((this.cesGroupNumber==null && other.getCesGroupNumber()==null) || 
             (this.cesGroupNumber!=null &&
              this.cesGroupNumber.equals(other.getCesGroupNumber()))) &&
            ((this.groupName==null && other.getGroupName()==null) || 
             (this.groupName!=null &&
              this.groupName.equals(other.getGroupName()))) &&
            ((this.periods==null && other.getPeriods()==null) || 
             (this.periods!=null &&
              java.util.Arrays.equals(this.periods, other.getPeriods())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getGroupNumber() != null) {
            _hashCode += getGroupNumber().hashCode();
        }
        if (getCesGroupNumber() != null) {
            _hashCode += getCesGroupNumber().hashCode();
        }
        if (getGroupName() != null) {
            _hashCode += getGroupName().hashCode();
        }
        if (getPeriods() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getPeriods());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getPeriods(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GroupPeriod.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://servicehelpers.esb.avalon.com", "GroupPeriod"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("groupNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("", "groupNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cesGroupNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cesGroupNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("groupName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "groupName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("periods");
        elemField.setXmlName(new javax.xml.namespace.QName("", "periods"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://servicehelpers.esb.avalon.com", "PeriodDate"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
