package com.avalon.esb.servicehelpers;

import java.math.BigDecimal;
import java.net.URL;
import java.rmi.RemoteException;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.Vector;

import javax.xml.namespace.QName;

import org.apache.axis.AxisFault;
import org.apache.axis.client.Call;
import org.apache.axis.client.Stub;
import org.apache.axis.constants.Style;
import org.apache.axis.constants.Use;
import org.apache.axis.description.OperationDesc;
import org.apache.axis.description.ParameterDesc;
import org.apache.axis.encoding.DeserializerFactory;
import org.apache.axis.encoding.ser.ArrayDeserializerFactory;
import org.apache.axis.encoding.ser.ArraySerializerFactory;
import org.apache.axis.encoding.ser.BeanDeserializerFactory;
import org.apache.axis.encoding.ser.BeanSerializerFactory;
import org.apache.axis.encoding.ser.EnumDeserializerFactory;
import org.apache.axis.encoding.ser.EnumSerializerFactory;
import org.apache.axis.encoding.ser.SimpleDeserializerFactory;
import org.apache.axis.encoding.ser.SimpleListDeserializerFactory;
import org.apache.axis.encoding.ser.SimpleListSerializerFactory;
import org.apache.axis.encoding.ser.SimpleSerializerFactory;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

public class ClaimSearchServicePortBindingStub
  extends Stub
  implements LBMClaimSearchService
{
	
  private static final Log log = LogFactoryUtil.getLog(ClaimSearchServicePortBindingStub.class.getName());

  private Vector cachedSerClasses = new Vector();
  private Vector cachedSerQNames = new Vector();
  private Vector cachedSerFactories = new Vector();
  private Vector cachedDeserFactories = new Vector();
  static OperationDesc[] _operations = new OperationDesc[2];
  
  static
  {
    _initOperationDesc1();
  }
  
  private static void _initOperationDesc1()
  {
    OperationDesc oper = new OperationDesc();
    oper.setName("findClaims");
    ParameterDesc param = new ParameterDesc(new QName("http://servicehelpers.esb.avalon.com", "findClaims"), (byte)1, new QName("http://servicehelpers.esb.avalon.com", "findClaims"), FindClaims.class, false, false);
    oper.addParameter(param);
    oper.setReturnType(new QName("http://servicehelpers.esb.avalon.com", "findClaimsResponse"));
    oper.setReturnClass(ClaimSummariesPatient[].class);
    oper.setReturnQName(new QName("http://servicehelpers.esb.avalon.com", "findClaimsResponse"));
    param = oper.getReturnParamDesc();
    param.setItemQName(new QName("", "claimsSummary"));
    oper.setStyle(Style.DOCUMENT);
    oper.setUse(Use.LITERAL);
    _operations[0] = oper;
    
    oper = new OperationDesc();
    oper.setName("getClaimDetails");
    param = new ParameterDesc(new QName("http://servicehelpers.esb.avalon.com", "findClaims"), (byte)1, new QName("http://servicehelpers.esb.avalon.com", "findClaims"), FindClaims.class, false, false);
    oper.addParameter(param);
    oper.setReturnType(new QName("http://servicehelpers.esb.avalon.com", "getClaimDetailsResponse"));
    oper.setReturnClass(GetClaimDetailsResponse.class);
    oper.setReturnQName(new QName("http://servicehelpers.esb.avalon.com", "getClaimDetailsResponse"));
    oper.setStyle(Style.DOCUMENT);
    oper.setUse(Use.LITERAL);
    _operations[1] = oper;
  }
  
  public ClaimSearchServicePortBindingStub()
    throws AxisFault
  {
    this(null);
  }
  
  public ClaimSearchServicePortBindingStub(URL endpointURL, javax.xml.rpc.Service service)
    throws AxisFault
  {
    this(service);
    this.cachedEndpoint = endpointURL;
  }
  
  public ClaimSearchServicePortBindingStub(javax.xml.rpc.Service service)
    throws AxisFault
  {
    if (service == null) {
      this.service = new org.apache.axis.client.Service();
    } else {
      this.service = service;
    }
    ((org.apache.axis.client.Service)this.service).setTypeMappingVersion("1.2");
    
    Class beansf = BeanSerializerFactory.class;
    Class beandf = BeanDeserializerFactory.class;
    Class enumsf = EnumSerializerFactory.class;
    Class enumdf = EnumDeserializerFactory.class;
    Class arraysf = ArraySerializerFactory.class;
    Class arraydf = ArrayDeserializerFactory.class;
    Class simplesf = SimpleSerializerFactory.class;
    Class simpledf = SimpleDeserializerFactory.class;
    Class simplelistsf = SimpleListSerializerFactory.class;
    Class simplelistdf = SimpleListDeserializerFactory.class;
    QName qName = new QName("http://servicehelpers.esb.avalon.com", "ClaimDetail");
    this.cachedSerQNames.add(qName);
    Class cls = ClaimDetail.class;
    this.cachedSerClasses.add(cls);
    this.cachedSerFactories.add(beansf);
    this.cachedDeserFactories.add(beandf);
    
    qName = new QName("http://servicehelpers.esb.avalon.com", "ClaimLinesSummary");
    this.cachedSerQNames.add(qName);
    cls = ClaimLinesSummary.class;
    this.cachedSerClasses.add(cls);
    this.cachedSerFactories.add(beansf);
    this.cachedDeserFactories.add(beandf);
    
    qName = new QName("http://servicehelpers.esb.avalon.com", "ClaimPayment");
    this.cachedSerQNames.add(qName);
    cls = ClaimPayment.class;
    this.cachedSerClasses.add(cls);
    this.cachedSerFactories.add(beansf);
    this.cachedDeserFactories.add(beandf);
    
    qName = new QName("http://servicehelpers.esb.avalon.com", "ClaimSearchCriteria");
    this.cachedSerQNames.add(qName);
    cls = ClaimSearchCriteria.class;
    this.cachedSerClasses.add(cls);
    this.cachedSerFactories.add(beansf);
    this.cachedDeserFactories.add(beandf);
    
    qName = new QName("http://servicehelpers.esb.avalon.com", "ClaimSearchFilter");
    this.cachedSerQNames.add(qName);
    cls = ClaimSearchFilter.class;
    this.cachedSerClasses.add(cls);
    this.cachedSerFactories.add(beansf);
    this.cachedDeserFactories.add(beandf);
    
    qName = new QName("http://servicehelpers.esb.avalon.com", "ClaimsSummary");
    this.cachedSerQNames.add(qName);
    cls = ClaimsSummary.class;
    this.cachedSerClasses.add(cls);
    this.cachedSerFactories.add(beansf);
    this.cachedDeserFactories.add(beandf);
    
    qName = new QName("http://servicehelpers.esb.avalon.com", "ClaimsSummaryItem");
    this.cachedSerQNames.add(qName);
    cls = ClaimsSummaryItem.class;
    this.cachedSerClasses.add(cls);
    this.cachedSerFactories.add(beansf);
    this.cachedDeserFactories.add(beandf);
    
    qName = new QName("http://servicehelpers.esb.avalon.com", "ClaimStatusEntity");
    this.cachedSerQNames.add(qName);
    cls = ClaimStatusEntity.class;
    this.cachedSerClasses.add(cls);
    this.cachedSerFactories.add(beansf);
    this.cachedDeserFactories.add(beandf);
    
    qName = new QName("http://servicehelpers.esb.avalon.com", "claimStatusInboundReq");
    this.cachedSerQNames.add(qName);
    cls = ClaimStatusInboundReq.class;
    this.cachedSerClasses.add(cls);
    this.cachedSerFactories.add(beansf);
    this.cachedDeserFactories.add(beandf);
    
    qName = new QName("http://servicehelpers.esb.avalon.com", "ClaimStatusType");
    this.cachedSerQNames.add(qName);
    cls = ClaimStatusType.class;
    this.cachedSerClasses.add(cls);
    this.cachedSerFactories.add(enumsf);
    this.cachedDeserFactories.add(enumdf);
    
    qName = new QName("http://servicehelpers.esb.avalon.com", "ClaimSummariesPatient");
    this.cachedSerQNames.add(qName);
    cls = ClaimSummariesPatient.class;
    this.cachedSerClasses.add(cls);
    this.cachedSerFactories.add(beansf);
    this.cachedDeserFactories.add(beandf);
    
    qName = new QName("http://servicehelpers.esb.avalon.com", "Code");
    this.cachedSerQNames.add(qName);
    cls = Code.class;
    this.cachedSerClasses.add(cls);
    this.cachedSerFactories.add(beansf);
    this.cachedDeserFactories.add(beandf);
    
    qName = new QName("http://servicehelpers.esb.avalon.com", "CoverageInformation");
    this.cachedSerQNames.add(qName);
    cls = CoverageInformation.class;
    this.cachedSerClasses.add(cls);
    this.cachedSerFactories.add(beansf);
    this.cachedDeserFactories.add(beandf);
    
    qName = new QName("http://servicehelpers.esb.avalon.com", "DentalClaimDetail");
    this.cachedSerQNames.add(qName);
    cls = DentalClaimDetail.class;
    this.cachedSerClasses.add(cls);
    this.cachedSerFactories.add(beansf);
    this.cachedDeserFactories.add(beandf);
    
    qName = new QName("http://servicehelpers.esb.avalon.com", "DiagnosisCode");
    this.cachedSerQNames.add(qName);
    cls = DiagnosisCode.class;
    this.cachedSerClasses.add(cls);
    this.cachedSerFactories.add(beansf);
    this.cachedDeserFactories.add(beandf);
    
    qName = new QName("http://servicehelpers.esb.avalon.com", "DiagnosisCodeDetail");
    this.cachedSerQNames.add(qName);
    cls = DiagnosisCodeDetail.class;
    this.cachedSerClasses.add(cls);
    this.cachedSerFactories.add(beansf);
    this.cachedDeserFactories.add(beandf);
    
    qName = new QName("http://servicehelpers.esb.avalon.com", "ExternalCauseInjuryDetail");
    this.cachedSerQNames.add(qName);
    cls = ExternalCauseInjuryDetail.class;
    this.cachedSerClasses.add(cls);
    this.cachedSerFactories.add(beansf);
    this.cachedDeserFactories.add(beandf);
    
    qName = new QName("http://servicehelpers.esb.avalon.com", "findClaims");
    this.cachedSerQNames.add(qName);
    cls = FindClaims.class;
    this.cachedSerClasses.add(cls);
    this.cachedSerFactories.add(beansf);
    this.cachedDeserFactories.add(beandf);
    
    qName = new QName("http://servicehelpers.esb.avalon.com", "findClaimsResponse");
    this.cachedSerQNames.add(qName);
    cls = ClaimSummariesPatient[].class;
    this.cachedSerClasses.add(cls);
    qName = new QName("http://servicehelpers.esb.avalon.com", "ClaimSummariesPatient");
    QName qName2 = new QName("", "claimsSummary");
    this.cachedSerFactories.add(new ArraySerializerFactory(qName, qName2));
    this.cachedDeserFactories.add(new ArrayDeserializerFactory());
    
    qName = new QName("http://servicehelpers.esb.avalon.com", "getClaimDetailsResponse");
    this.cachedSerQNames.add(qName);
    cls = GetClaimDetailsResponse.class;
    this.cachedSerClasses.add(cls);
    this.cachedSerFactories.add(beansf);
    this.cachedDeserFactories.add(beandf);
    
    qName = new QName("http://servicehelpers.esb.avalon.com", "GroupPeriod");
    this.cachedSerQNames.add(qName);
    cls = GroupPeriod.class;
    this.cachedSerClasses.add(cls);
    this.cachedSerFactories.add(beansf);
    this.cachedDeserFactories.add(beandf);
    
    qName = new QName("http://servicehelpers.esb.avalon.com", "HealthClaimDetail");
    this.cachedSerQNames.add(qName);
    cls = HealthClaimDetail.class;
    this.cachedSerClasses.add(cls);
    this.cachedSerFactories.add(beansf);
    this.cachedDeserFactories.add(beandf);
    
    qName = new QName("http://servicehelpers.esb.avalon.com", "HealthClaimSearchCriteria");
    this.cachedSerQNames.add(qName);
    cls = HealthClaimSearchCriteria.class;
    this.cachedSerClasses.add(cls);
    this.cachedSerFactories.add(beansf);
    this.cachedDeserFactories.add(beandf);
    
    qName = new QName("http://servicehelpers.esb.avalon.com", "KeyDescription");
    this.cachedSerQNames.add(qName);
    cls = KeyDescription.class;
    this.cachedSerClasses.add(cls);
    this.cachedSerFactories.add(beansf);
    this.cachedDeserFactories.add(beandf);
    
    qName = new QName("http://servicehelpers.esb.avalon.com", "Name");
    this.cachedSerQNames.add(qName);
    cls = Name.class;
    this.cachedSerClasses.add(cls);
    this.cachedSerFactories.add(beansf);
    this.cachedDeserFactories.add(beandf);
    
    qName = new QName("http://servicehelpers.esb.avalon.com", "OccurrenceCode");
    this.cachedSerQNames.add(qName);
    cls = OccurrenceCode.class;
    this.cachedSerClasses.add(cls);
    this.cachedSerFactories.add(beansf);
    this.cachedDeserFactories.add(beandf);
    
    qName = new QName("http://servicehelpers.esb.avalon.com", "OutOfAreaClaimsSummaryItem");
    this.cachedSerQNames.add(qName);
    cls = OutOfAreaClaimsSummaryItem.class;
    this.cachedSerClasses.add(cls);
    this.cachedSerFactories.add(beansf);
    this.cachedDeserFactories.add(beandf);
    
    qName = new QName("http://servicehelpers.esb.avalon.com", "Patient");
    this.cachedSerQNames.add(qName);
    cls = Patient.class;
    this.cachedSerClasses.add(cls);
    this.cachedSerFactories.add(beansf);
    this.cachedDeserFactories.add(beandf);
    
    qName = new QName("http://servicehelpers.esb.avalon.com", "PeriodDate");
    this.cachedSerQNames.add(qName);
    cls = PeriodDate.class;
    this.cachedSerClasses.add(cls);
    this.cachedSerFactories.add(beansf);
    this.cachedDeserFactories.add(beandf);
    
    qName = new QName("http://servicehelpers.esb.avalon.com", "Person");
    this.cachedSerQNames.add(qName);
    cls = Person.class;
    this.cachedSerClasses.add(cls);
    this.cachedSerFactories.add(beansf);
    this.cachedDeserFactories.add(beandf);
    
    qName = new QName("http://servicehelpers.esb.avalon.com", "ProcedureCodeDetail");
    this.cachedSerQNames.add(qName);
    cls = ProcedureCodeDetail.class;
    this.cachedSerClasses.add(cls);
    this.cachedSerFactories.add(beansf);
    this.cachedDeserFactories.add(beandf);
    
    qName = new QName("http://servicehelpers.esb.avalon.com", "Provider");
    this.cachedSerQNames.add(qName);
    cls = Provider.class;
    this.cachedSerClasses.add(cls);
    this.cachedSerFactories.add(beansf);
    this.cachedDeserFactories.add(beandf);
    
    qName = new QName("http://servicehelpers.esb.avalon.com", "ProviderFivePartKey");
    this.cachedSerQNames.add(qName);
    cls = ProviderFivePartKey.class;
    this.cachedSerClasses.add(cls);
    this.cachedSerFactories.add(beansf);
    this.cachedDeserFactories.add(beandf);
    
    qName = new QName("http://servicehelpers.esb.avalon.com", "ProviderId");
    this.cachedSerQNames.add(qName);
    cls = ProviderId.class;
    this.cachedSerClasses.add(cls);
    this.cachedSerFactories.add(beansf);
    this.cachedDeserFactories.add(beandf);
    
    qName = new QName("http://servicehelpers.esb.avalon.com", "ProviderIdentifier");
    this.cachedSerQNames.add(qName);
    cls = ProviderIdentifier.class;
    this.cachedSerClasses.add(cls);
    this.cachedSerFactories.add(beansf);
    this.cachedDeserFactories.add(beandf);
    
    qName = new QName("http://servicehelpers.esb.avalon.com", "ProviderInformation");
    this.cachedSerQNames.add(qName);
    cls = ProviderInformation.class;
    this.cachedSerClasses.add(cls);
    this.cachedSerFactories.add(beansf);
    this.cachedDeserFactories.add(beandf);
    
    qName = new QName("http://servicehelpers.esb.avalon.com", "SubscriberId");
    this.cachedSerQNames.add(qName);
    cls = SubscriberId.class;
    this.cachedSerClasses.add(cls);
    this.cachedSerFactories.add(beansf);
    this.cachedDeserFactories.add(beandf);
    
    qName = new QName("http://servicehelpers.esb.avalon.com", "ValueCode");
    this.cachedSerQNames.add(qName);
    cls = ValueCode.class;
    this.cachedSerClasses.add(cls);
    this.cachedSerFactories.add(beansf);
    this.cachedDeserFactories.add(beandf);
  }
  
  protected Call createCall()
    throws RemoteException
  {
    try
    {
      Call _call = super._createCall();
      if (this.maintainSessionSet) {
        _call.setMaintainSession(this.maintainSession);
      }
      if (this.cachedUsername != null) {
        _call.setUsername(this.cachedUsername);
      }
      if (this.cachedPassword != null) {
        _call.setPassword(this.cachedPassword);
      }
      if (this.cachedEndpoint != null) {
        _call.setTargetEndpointAddress(this.cachedEndpoint);
      }
      if (this.cachedTimeout != null) {
        _call.setTimeout(this.cachedTimeout);
      }
      if (this.cachedPortName != null) {
        _call.setPortName(this.cachedPortName);
      }
      Enumeration keys = this.cachedProperties.keys();
      while (keys.hasMoreElements())
      {
        String key = (String)keys.nextElement();
        _call.setProperty(key, this.cachedProperties.get(key));
      }
      synchronized (this)
      {
        if (firstCall())
        {
          _call.setEncodingStyle(null);
          for (int i = 0; i < this.cachedSerFactories.size(); i++)
          {
            Class cls = (Class)this.cachedSerClasses.get(i);
            QName qName = (QName)this.cachedSerQNames.get(i);
            
            Object x = this.cachedSerFactories.get(i);
            if ((x instanceof Class))
            {
              Class sf = (Class)this.cachedSerFactories.get(i);
              
              Class df = (Class)this.cachedDeserFactories.get(i);
              
              _call.registerTypeMapping(cls, qName, sf, df, false);
            }
            else if ((x instanceof javax.xml.rpc.encoding.SerializerFactory))
            {
              org.apache.axis.encoding.SerializerFactory sf = (org.apache.axis.encoding.SerializerFactory)this.cachedSerFactories.get(i);
              
              DeserializerFactory df = (DeserializerFactory)this.cachedDeserFactories.get(i);
              
              _call.registerTypeMapping(cls, qName, sf, df, false);
            }
          }
        }
      }
      return _call;
    }
    catch (Throwable _t)
    {
      throw new AxisFault("Failure trying to get the Call object", _t);
    }
  }
  
  /* Error */
  public ClaimSummariesPatient[] findClaims(FindClaims findClaims)
    throws RemoteException
  {
      if (super.cachedEndpoint == null) {
          throw new org.apache.axis.NoEndPointException();
      }
      log.info("Inside findClaims method..");
      org.apache.axis.client.Call _call = createCall();
      _call.setOperation(_operations[0]);
      _call.setUseSOAPAction(true);
      _call.setSOAPActionURI("");
      _call.setEncodingStyle(null);
      _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
      _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
      _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
      _call.setOperationName(new javax.xml.namespace.QName("", "findClaims"));

      setRequestHeaders(_call);
      setAttachments(_call);
      try {
      	java.lang.Object _resp = _call.invoke(new java.lang.Object[] {findClaims});
        log.info("_resp of findClaims.."+_resp);
	        if (_resp instanceof java.rmi.RemoteException) {
	            throw (java.rmi.RemoteException)_resp;
	        }
	        else {
	            extractAttachments(_call);
	            try {
	                return (com.avalon.esb.servicehelpers.ClaimSummariesPatient[]) _resp;
	            } catch (java.lang.Exception _exception) {
	                return (com.avalon.esb.servicehelpers.ClaimSummariesPatient[]) org.apache.axis.utils.JavaUtils.convert(_resp, com.avalon.esb.servicehelpers.ClaimSummariesPatient[].class);
	            }
	        }
      } catch (org.apache.axis.AxisFault axisFaultException) {
		  throw axisFaultException;
		}
  }
  
  /* Error */
  public GetClaimDetailsResponse getClaimDetails(FindClaims findClaims)
    throws RemoteException
  {
	  log.info("Inside getClaimDetails method..");
      if (super.cachedEndpoint == null) {
          throw new org.apache.axis.NoEndPointException();
      }
      
      org.apache.axis.client.Call _call = createCall();
      _call.setOperation(_operations[1]);
      _call.setUseSOAPAction(true);
      _call.setSOAPActionURI("");
      _call.setEncodingStyle(null);
      _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
      _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
      _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
      _call.setOperationName(new javax.xml.namespace.QName("", "getClaimDetails"));

      setRequestHeaders(_call);
      setAttachments(_call);
	  try {
		  java.lang.Object _resp = _call.invoke(new java.lang.Object[] {findClaims});
		  log.info("_resp of getClaimDetails method.."+_resp);
		  if (_resp instanceof java.rmi.RemoteException) {
			  throw (java.rmi.RemoteException)_resp;
		  }
		  else {
			  extractAttachments(_call);
			  try {
				  return (com.avalon.esb.servicehelpers.GetClaimDetailsResponse) _resp;
		      } catch (java.lang.Exception _exception) {
		    	  return (com.avalon.esb.servicehelpers.GetClaimDetailsResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.avalon.esb.servicehelpers.GetClaimDetailsResponse.class);
		      }
		  }
	  } catch (org.apache.axis.AxisFault axisFaultException) {
		  throw axisFaultException;
	  }
  }

  /**
   * TODO: Remove mock code
   * Success Mock Code
   */
@Override
public GetClaimDetailsResponse getSuccessMockClaimDetails(FindClaims findClaims) throws RemoteException {
	GetClaimDetailsResponse claimDetailsResponse = new GetClaimDetailsResponse();
	
	ClaimDetail claimDetail = new ClaimDetail();
	claimDetail.setClaimNumber(findClaims.getClaimStatusInboundReq().getClaimSearchCriteriaInbound().getSearchClaimNumber());
	
	Patient patient = new Patient();
	SubscriberId subscriberId = new SubscriberId();
	subscriberId.setMemberNumber("1");
	
	subscriberId.setIdCardNumber("IdCardNum");
	patient.setSubscriberId(subscriberId);
	claimDetail.setPatient(patient);

	ClaimStatusType statusType = new ClaimStatusType("PROCESSED");
	claimDetail.setStatusType(statusType);
	
	Provider baseProvider = new Provider();
	
	ProviderId providerId = new ProviderId();

	providerId.setNationalProviderIdentifier("National");
	Name providerName = new Name();
	
	providerName.setFirstName("TestFirstName");
	providerName.setLastName("TestLastName");
	baseProvider.setProviderName(providerName);
	baseProvider.setProviderId(providerId);
	claimDetail.setBaseProvider(baseProvider);
	PeriodDate claimDateOfService = new PeriodDate();
	
	Calendar cal = Calendar.getInstance();
    int year = cal.get(Calendar.YEAR);
    int month = cal.get(Calendar.MONTH);      
    int day = cal.get(Calendar.DAY_OF_MONTH);
    
	claimDateOfService.setFromDate(cal);
	claimDateOfService.setToDate(cal);
	claimDetail.setClaimDateOfService(claimDateOfService);
	claimDetail.setTotalClaimChargeAmount(new BigDecimal(0.1));
	claimDetail.setCheckIssueDate(cal);
	claimDetail.setCheckNumber("2");
	claimDetail.setStatusCode("Code");
	
	claimDetailsResponse.setClaimDetail(claimDetail);
	return claimDetailsResponse;
}

/**
 * TODO: Remove mock code
 * Find Claims Success Mock
 */
@Override
public ClaimSummariesPatient[] findClaimsSuccessMock(FindClaims findClaims) throws RemoteException {
	
	ClaimSummariesPatient claimSummariesPatient = new ClaimSummariesPatient();
	 
	ClaimsSummaryItem claimsSummaryItem = new ClaimsSummaryItem();
	
	ClaimStatusType claimStatusType = new ClaimStatusType("PENDING");
	claimsSummaryItem.setStatusType(claimStatusType);
	claimsSummaryItem.setClaimNumber(findClaims.getClaimStatusInboundReq().getClaimSearchCriteriaInbound().getSearchClaimNumber());
	
	if(findClaims.getClaimStatusInboundReq().getClaimSearchCriteriaInbound().getSearchClaimNumber() == null){
		claimsSummaryItem.setClaimNumber("TestClaimID");
		
	}
	claimsSummaryItem.setProviderID("TestProviderID");
	claimsSummaryItem.setBillingProviderName("TestBillingProviderName");
	
	Calendar cal = Calendar.getInstance();
    int year = cal.get(Calendar.YEAR);
    int month = cal.get(Calendar.MONTH);      
    int day = cal.get(Calendar.DAY_OF_MONTH);
    
	PeriodDate periodDate = new PeriodDate();
	periodDate.setFromDate(cal);
	periodDate.setToDate(cal);
	
	claimsSummaryItem.setClaimDateOfService(periodDate);
	claimsSummaryItem.setTotalClaimChargeAmount(new BigDecimal(0.2));
	claimsSummaryItem.setCheckIssueDate(cal);
	claimsSummaryItem.setProviderID("TestProviderID");
	
	ClaimsSummary claimsSummary = new ClaimsSummary();
	claimsSummary.setClaimsSummaryItems(new ClaimsSummaryItem[] { claimsSummaryItem });
	claimSummariesPatient.setClaimSummary(claimsSummary);

	return new ClaimSummariesPatient[] { claimSummariesPatient } ;
	
}
  
}

