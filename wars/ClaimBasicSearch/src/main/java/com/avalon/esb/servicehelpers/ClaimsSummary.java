/**
 * ClaimsSummary.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.avalon.esb.servicehelpers;

public class ClaimsSummary  implements java.io.Serializable {
    private com.avalon.esb.servicehelpers.Patient patient;

    private com.avalon.esb.servicehelpers.GroupPeriod[] groupPeriods;
    
    
    private String resultCount;
    
    /**
	 * @return the resultCount
	 */
	public String getResultCount() {
		return resultCount;
	}

	/**
	 * @param resultCount the resultCount to set
	 */
	public void setResultCount(String resultCount) {
		this.resultCount = resultCount;
	}

	/**
	 * @return the maximumClaimsReturned
	 */
	public String getMaximumClaimsReturned() {
		return maximumClaimsReturned;
	}

	/**
	 * @param maximumClaimsReturned the maximumClaimsReturned to set
	 */
	public void setMaximumClaimsReturned(String maximumClaimsReturned) {
		this.maximumClaimsReturned = maximumClaimsReturned;
	}

	private String maximumClaimsReturned;

    private com.avalon.esb.servicehelpers.ClaimsSummaryItem[] claimsSummaryItems;

    public ClaimsSummary() {
    }

    public ClaimsSummary(
           com.avalon.esb.servicehelpers.Patient patient,
           com.avalon.esb.servicehelpers.GroupPeriod[] groupPeriods,
           com.avalon.esb.servicehelpers.ClaimsSummaryItem[] claimsSummaryItems) {
           this.patient = patient;
           this.groupPeriods = groupPeriods;
           this.claimsSummaryItems = claimsSummaryItems;
    }


    /**
     * Gets the patient value for this ClaimsSummary.
     * 
     * @return patient
     */
    public com.avalon.esb.servicehelpers.Patient getPatient() {
        return patient;
    }


    /**
     * Sets the patient value for this ClaimsSummary.
     * 
     * @param patient
     */
    public void setPatient(com.avalon.esb.servicehelpers.Patient patient) {
        this.patient = patient;
    }


    /**
     * Gets the groupPeriods value for this ClaimsSummary.
     * 
     * @return groupPeriods
     */
    public com.avalon.esb.servicehelpers.GroupPeriod[] getGroupPeriods() {
        return groupPeriods;
    }


    /**
     * Sets the groupPeriods value for this ClaimsSummary.
     * 
     * @param groupPeriods
     */
    public void setGroupPeriods(com.avalon.esb.servicehelpers.GroupPeriod[] groupPeriods) {
        this.groupPeriods = groupPeriods;
    }

    public com.avalon.esb.servicehelpers.GroupPeriod getGroupPeriods(int i) {
        return this.groupPeriods[i];
    }

    public void setGroupPeriods(int i, com.avalon.esb.servicehelpers.GroupPeriod _value) {
        this.groupPeriods[i] = _value;
    }


    /**
     * Gets the claimsSummaryItems value for this ClaimsSummary.
     * 
     * @return claimsSummaryItems
     */
    public com.avalon.esb.servicehelpers.ClaimsSummaryItem[] getClaimsSummaryItems() {
        return claimsSummaryItems;
    }


    /**
     * Sets the claimsSummaryItems value for this ClaimsSummary.
     * 
     * @param claimsSummaryItems
     */
    public void setClaimsSummaryItems(com.avalon.esb.servicehelpers.ClaimsSummaryItem[] claimsSummaryItems) {
        this.claimsSummaryItems = claimsSummaryItems;
    }

    public com.avalon.esb.servicehelpers.ClaimsSummaryItem getClaimsSummaryItems(int i) {
        return this.claimsSummaryItems[i];
    }

    public void setClaimsSummaryItems(int i, com.avalon.esb.servicehelpers.ClaimsSummaryItem _value) {
        this.claimsSummaryItems[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ClaimsSummary)) return false;
        ClaimsSummary other = (ClaimsSummary) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.patient==null && other.getPatient()==null) || 
             (this.patient!=null &&
              this.patient.equals(other.getPatient()))) &&
            ((this.groupPeriods==null && other.getGroupPeriods()==null) || 
             (this.groupPeriods!=null &&
              java.util.Arrays.equals(this.groupPeriods, other.getGroupPeriods()))) &&
            ((this.claimsSummaryItems==null && other.getClaimsSummaryItems()==null) || 
             (this.claimsSummaryItems!=null &&
              java.util.Arrays.equals(this.claimsSummaryItems, other.getClaimsSummaryItems())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getPatient() != null) {
            _hashCode += getPatient().hashCode();
        }
        if (getGroupPeriods() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getGroupPeriods());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getGroupPeriods(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getClaimsSummaryItems() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getClaimsSummaryItems());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getClaimsSummaryItems(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ClaimsSummary.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://servicehelpers.esb.avalon.com", "ClaimsSummary"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("patient");
        elemField.setXmlName(new javax.xml.namespace.QName("", "patient"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://servicehelpers.esb.avalon.com", "Patient"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("groupPeriods");
        elemField.setXmlName(new javax.xml.namespace.QName("", "groupPeriods"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://servicehelpers.esb.avalon.com", "GroupPeriod"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("claimsSummaryItems");
        elemField.setXmlName(new javax.xml.namespace.QName("", "claimsSummaryItems"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://servicehelpers.esb.avalon.com", "ClaimsSummaryItem"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
