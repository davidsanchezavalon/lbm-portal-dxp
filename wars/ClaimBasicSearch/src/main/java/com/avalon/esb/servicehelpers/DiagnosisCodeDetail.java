/**
 * DiagnosisCodeDetail.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.avalon.esb.servicehelpers;

public class DiagnosisCodeDetail  extends com.avalon.esb.servicehelpers.DiagnosisCode  implements java.io.Serializable {
    private com.avalon.esb.servicehelpers.Code icdVersion;

    public DiagnosisCodeDetail() {
    }

    public DiagnosisCodeDetail(
           java.lang.String code,
           java.lang.String description,
           com.avalon.esb.servicehelpers.Code presentOnAdmissionCode,
           java.lang.Boolean presentOnAdmission,
           com.avalon.esb.servicehelpers.Code qualifier,
           com.avalon.esb.servicehelpers.Code icdVersion) {
        super(
            code,
            description,
            presentOnAdmissionCode,
            presentOnAdmission,
            qualifier);
        this.icdVersion = icdVersion;
    }


    /**
     * Gets the icdVersion value for this DiagnosisCodeDetail.
     * 
     * @return icdVersion
     */
    public com.avalon.esb.servicehelpers.Code getIcdVersion() {
        return icdVersion;
    }


    /**
     * Sets the icdVersion value for this DiagnosisCodeDetail.
     * 
     * @param icdVersion
     */
    public void setIcdVersion(com.avalon.esb.servicehelpers.Code icdVersion) {
        this.icdVersion = icdVersion;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof DiagnosisCodeDetail)) return false;
        DiagnosisCodeDetail other = (DiagnosisCodeDetail) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.icdVersion==null && other.getIcdVersion()==null) || 
             (this.icdVersion!=null &&
              this.icdVersion.equals(other.getIcdVersion())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getIcdVersion() != null) {
            _hashCode += getIcdVersion().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(DiagnosisCodeDetail.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://servicehelpers.esb.avalon.com", "DiagnosisCodeDetail"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("icdVersion");
        elemField.setXmlName(new javax.xml.namespace.QName("", "icdVersion"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://servicehelpers.esb.avalon.com", "Code"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
