/**
 * Provider.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.avalon.esb.servicehelpers;

public class Provider  implements java.io.Serializable {
    private com.avalon.esb.servicehelpers.Code entityQualifier;

    private com.avalon.esb.servicehelpers.Code entityCode;

    private com.avalon.esb.servicehelpers.Code bcaEntityQualifier;

    private com.avalon.esb.servicehelpers.ProviderId providerId;

    private com.avalon.esb.servicehelpers.Name providerName;

    private com.avalon.esb.servicehelpers.Code bcaSpecialty;

    private java.lang.String providerFormattedName;

    private com.avalon.esb.servicehelpers.Code status;

    private com.avalon.esb.servicehelpers.Code gender;

    private com.avalon.esb.servicehelpers.Code acceptsAssignment;

    private com.avalon.esb.servicehelpers.Code signatureOnFile;

    private java.lang.Boolean aTypicalProvider;

    private java.lang.Boolean acceptsDenteMax;

    private java.lang.Boolean providerOnFile;

    private java.lang.Boolean npiRequired;

    private java.lang.String providerClass;

    private java.lang.String providerCategory;

    private java.lang.String providerTaxIdNumberType;

    private com.avalon.esb.servicehelpers.Code taxonomyCode;

    private java.lang.String tpoCode;

    private java.lang.String bcaDirectorySupressIndicator;

    private java.lang.String npiIndicator;

    private java.lang.Boolean npiExists;

    private java.lang.String aTypicalProviderCode;

    private java.lang.Boolean providerRatable;

    private java.lang.String itsClaimIndicator;

    private java.lang.String itsClassIndicator;

    private java.lang.String accreditationType;

    private java.lang.Boolean patientCenteredMedicalHomeIndicator;

    private java.lang.Boolean inPpcNetwork;

    private java.lang.String networkCode;

    public Provider() {
    }

    public Provider(
           com.avalon.esb.servicehelpers.Code entityQualifier,
           com.avalon.esb.servicehelpers.Code entityCode,
           com.avalon.esb.servicehelpers.Code bcaEntityQualifier,
           com.avalon.esb.servicehelpers.ProviderId providerId,
           com.avalon.esb.servicehelpers.Name providerName,
           com.avalon.esb.servicehelpers.Code bcaSpecialty,
           java.lang.String providerFormattedName,
           com.avalon.esb.servicehelpers.Code status,
           com.avalon.esb.servicehelpers.Code gender,
           com.avalon.esb.servicehelpers.Code acceptsAssignment,
           com.avalon.esb.servicehelpers.Code signatureOnFile,
           java.lang.Boolean aTypicalProvider,
           java.lang.Boolean acceptsDenteMax,
           java.lang.Boolean providerOnFile,
           java.lang.Boolean npiRequired,
           java.lang.String providerClass,
           java.lang.String providerCategory,
           java.lang.String providerTaxIdNumberType,
           com.avalon.esb.servicehelpers.Code taxonomyCode,
           java.lang.String tpoCode,
           java.lang.String bcaDirectorySupressIndicator,
           java.lang.String npiIndicator,
           java.lang.Boolean npiExists,
           java.lang.String aTypicalProviderCode,
           java.lang.Boolean providerRatable,
           java.lang.String itsClaimIndicator,
           java.lang.String itsClassIndicator,
           java.lang.String accreditationType,
           java.lang.Boolean patientCenteredMedicalHomeIndicator,
           java.lang.Boolean inPpcNetwork,
           java.lang.String networkCode) {
           this.entityQualifier = entityQualifier;
           this.entityCode = entityCode;
           this.bcaEntityQualifier = bcaEntityQualifier;
           this.providerId = providerId;
           this.providerName = providerName;
           this.bcaSpecialty = bcaSpecialty;
           this.providerFormattedName = providerFormattedName;
           this.status = status;
           this.gender = gender;
           this.acceptsAssignment = acceptsAssignment;
           this.signatureOnFile = signatureOnFile;
           this.aTypicalProvider = aTypicalProvider;
           this.acceptsDenteMax = acceptsDenteMax;
           this.providerOnFile = providerOnFile;
           this.npiRequired = npiRequired;
           this.providerClass = providerClass;
           this.providerCategory = providerCategory;
           this.providerTaxIdNumberType = providerTaxIdNumberType;
           this.taxonomyCode = taxonomyCode;
           this.tpoCode = tpoCode;
           this.bcaDirectorySupressIndicator = bcaDirectorySupressIndicator;
           this.npiIndicator = npiIndicator;
           this.npiExists = npiExists;
           this.aTypicalProviderCode = aTypicalProviderCode;
           this.providerRatable = providerRatable;
           this.itsClaimIndicator = itsClaimIndicator;
           this.itsClassIndicator = itsClassIndicator;
           this.accreditationType = accreditationType;
           this.patientCenteredMedicalHomeIndicator = patientCenteredMedicalHomeIndicator;
           this.inPpcNetwork = inPpcNetwork;
           this.networkCode = networkCode;
    }


    /**
     * Gets the entityQualifier value for this Provider.
     * 
     * @return entityQualifier
     */
    public com.avalon.esb.servicehelpers.Code getEntityQualifier() {
        return entityQualifier;
    }


    /**
     * Sets the entityQualifier value for this Provider.
     * 
     * @param entityQualifier
     */
    public void setEntityQualifier(com.avalon.esb.servicehelpers.Code entityQualifier) {
        this.entityQualifier = entityQualifier;
    }


    /**
     * Gets the entityCode value for this Provider.
     * 
     * @return entityCode
     */
    public com.avalon.esb.servicehelpers.Code getEntityCode() {
        return entityCode;
    }


    /**
     * Sets the entityCode value for this Provider.
     * 
     * @param entityCode
     */
    public void setEntityCode(com.avalon.esb.servicehelpers.Code entityCode) {
        this.entityCode = entityCode;
    }


    /**
     * Gets the bcaEntityQualifier value for this Provider.
     * 
     * @return bcaEntityQualifier
     */
    public com.avalon.esb.servicehelpers.Code getBcaEntityQualifier() {
        return bcaEntityQualifier;
    }


    /**
     * Sets the bcaEntityQualifier value for this Provider.
     * 
     * @param bcaEntityQualifier
     */
    public void setBcaEntityQualifier(com.avalon.esb.servicehelpers.Code bcaEntityQualifier) {
        this.bcaEntityQualifier = bcaEntityQualifier;
    }


    /**
     * Gets the providerId value for this Provider.
     * 
     * @return providerId
     */
    public com.avalon.esb.servicehelpers.ProviderId getProviderId() {
        return providerId;
    }


    /**
     * Sets the providerId value for this Provider.
     * 
     * @param providerId
     */
    public void setProviderId(com.avalon.esb.servicehelpers.ProviderId providerId) {
        this.providerId = providerId;
    }


    /**
     * Gets the providerName value for this Provider.
     * 
     * @return providerName
     */
    public com.avalon.esb.servicehelpers.Name getProviderName() {
        return providerName;
    }


    /**
     * Sets the providerName value for this Provider.
     * 
     * @param providerName
     */
    public void setProviderName(com.avalon.esb.servicehelpers.Name providerName) {
        this.providerName = providerName;
    }


    /**
     * Gets the bcaSpecialty value for this Provider.
     * 
     * @return bcaSpecialty
     */
    public com.avalon.esb.servicehelpers.Code getBcaSpecialty() {
        return bcaSpecialty;
    }


    /**
     * Sets the bcaSpecialty value for this Provider.
     * 
     * @param bcaSpecialty
     */
    public void setBcaSpecialty(com.avalon.esb.servicehelpers.Code bcaSpecialty) {
        this.bcaSpecialty = bcaSpecialty;
    }


    /**
     * Gets the providerFormattedName value for this Provider.
     * 
     * @return providerFormattedName
     */
    public java.lang.String getProviderFormattedName() {
        return providerFormattedName;
    }


    /**
     * Sets the providerFormattedName value for this Provider.
     * 
     * @param providerFormattedName
     */
    public void setProviderFormattedName(java.lang.String providerFormattedName) {
        this.providerFormattedName = providerFormattedName;
    }


    /**
     * Gets the status value for this Provider.
     * 
     * @return status
     */
    public com.avalon.esb.servicehelpers.Code getStatus() {
        return status;
    }


    /**
     * Sets the status value for this Provider.
     * 
     * @param status
     */
    public void setStatus(com.avalon.esb.servicehelpers.Code status) {
        this.status = status;
    }


    /**
     * Gets the gender value for this Provider.
     * 
     * @return gender
     */
    public com.avalon.esb.servicehelpers.Code getGender() {
        return gender;
    }


    /**
     * Sets the gender value for this Provider.
     * 
     * @param gender
     */
    public void setGender(com.avalon.esb.servicehelpers.Code gender) {
        this.gender = gender;
    }


    /**
     * Gets the acceptsAssignment value for this Provider.
     * 
     * @return acceptsAssignment
     */
    public com.avalon.esb.servicehelpers.Code getAcceptsAssignment() {
        return acceptsAssignment;
    }


    /**
     * Sets the acceptsAssignment value for this Provider.
     * 
     * @param acceptsAssignment
     */
    public void setAcceptsAssignment(com.avalon.esb.servicehelpers.Code acceptsAssignment) {
        this.acceptsAssignment = acceptsAssignment;
    }


    /**
     * Gets the signatureOnFile value for this Provider.
     * 
     * @return signatureOnFile
     */
    public com.avalon.esb.servicehelpers.Code getSignatureOnFile() {
        return signatureOnFile;
    }


    /**
     * Sets the signatureOnFile value for this Provider.
     * 
     * @param signatureOnFile
     */
    public void setSignatureOnFile(com.avalon.esb.servicehelpers.Code signatureOnFile) {
        this.signatureOnFile = signatureOnFile;
    }


    /**
     * Gets the aTypicalProvider value for this Provider.
     * 
     * @return aTypicalProvider
     */
    public java.lang.Boolean getATypicalProvider() {
        return aTypicalProvider;
    }


    /**
     * Sets the aTypicalProvider value for this Provider.
     * 
     * @param aTypicalProvider
     */
    public void setATypicalProvider(java.lang.Boolean aTypicalProvider) {
        this.aTypicalProvider = aTypicalProvider;
    }


    /**
     * Gets the acceptsDenteMax value for this Provider.
     * 
     * @return acceptsDenteMax
     */
    public java.lang.Boolean getAcceptsDenteMax() {
        return acceptsDenteMax;
    }


    /**
     * Sets the acceptsDenteMax value for this Provider.
     * 
     * @param acceptsDenteMax
     */
    public void setAcceptsDenteMax(java.lang.Boolean acceptsDenteMax) {
        this.acceptsDenteMax = acceptsDenteMax;
    }


    /**
     * Gets the providerOnFile value for this Provider.
     * 
     * @return providerOnFile
     */
    public java.lang.Boolean getProviderOnFile() {
        return providerOnFile;
    }


    /**
     * Sets the providerOnFile value for this Provider.
     * 
     * @param providerOnFile
     */
    public void setProviderOnFile(java.lang.Boolean providerOnFile) {
        this.providerOnFile = providerOnFile;
    }


    /**
     * Gets the npiRequired value for this Provider.
     * 
     * @return npiRequired
     */
    public java.lang.Boolean getNpiRequired() {
        return npiRequired;
    }


    /**
     * Sets the npiRequired value for this Provider.
     * 
     * @param npiRequired
     */
    public void setNpiRequired(java.lang.Boolean npiRequired) {
        this.npiRequired = npiRequired;
    }


    /**
     * Gets the providerClass value for this Provider.
     * 
     * @return providerClass
     */
    public java.lang.String getProviderClass() {
        return providerClass;
    }


    /**
     * Sets the providerClass value for this Provider.
     * 
     * @param providerClass
     */
    public void setProviderClass(java.lang.String providerClass) {
        this.providerClass = providerClass;
    }


    /**
     * Gets the providerCategory value for this Provider.
     * 
     * @return providerCategory
     */
    public java.lang.String getProviderCategory() {
        return providerCategory;
    }


    /**
     * Sets the providerCategory value for this Provider.
     * 
     * @param providerCategory
     */
    public void setProviderCategory(java.lang.String providerCategory) {
        this.providerCategory = providerCategory;
    }


    /**
     * Gets the providerTaxIdNumberType value for this Provider.
     * 
     * @return providerTaxIdNumberType
     */
    public java.lang.String getProviderTaxIdNumberType() {
        return providerTaxIdNumberType;
    }


    /**
     * Sets the providerTaxIdNumberType value for this Provider.
     * 
     * @param providerTaxIdNumberType
     */
    public void setProviderTaxIdNumberType(java.lang.String providerTaxIdNumberType) {
        this.providerTaxIdNumberType = providerTaxIdNumberType;
    }


    /**
     * Gets the taxonomyCode value for this Provider.
     * 
     * @return taxonomyCode
     */
    public com.avalon.esb.servicehelpers.Code getTaxonomyCode() {
        return taxonomyCode;
    }


    /**
     * Sets the taxonomyCode value for this Provider.
     * 
     * @param taxonomyCode
     */
    public void setTaxonomyCode(com.avalon.esb.servicehelpers.Code taxonomyCode) {
        this.taxonomyCode = taxonomyCode;
    }


    /**
     * Gets the tpoCode value for this Provider.
     * 
     * @return tpoCode
     */
    public java.lang.String getTpoCode() {
        return tpoCode;
    }


    /**
     * Sets the tpoCode value for this Provider.
     * 
     * @param tpoCode
     */
    public void setTpoCode(java.lang.String tpoCode) {
        this.tpoCode = tpoCode;
    }


    /**
     * Gets the bcaDirectorySupressIndicator value for this Provider.
     * 
     * @return bcaDirectorySupressIndicator
     */
    public java.lang.String getBcaDirectorySupressIndicator() {
        return bcaDirectorySupressIndicator;
    }


    /**
     * Sets the bcaDirectorySupressIndicator value for this Provider.
     * 
     * @param bcaDirectorySupressIndicator
     */
    public void setBcaDirectorySupressIndicator(java.lang.String bcaDirectorySupressIndicator) {
        this.bcaDirectorySupressIndicator = bcaDirectorySupressIndicator;
    }


    /**
     * Gets the npiIndicator value for this Provider.
     * 
     * @return npiIndicator
     */
    public java.lang.String getNpiIndicator() {
        return npiIndicator;
    }


    /**
     * Sets the npiIndicator value for this Provider.
     * 
     * @param npiIndicator
     */
    public void setNpiIndicator(java.lang.String npiIndicator) {
        this.npiIndicator = npiIndicator;
    }


    /**
     * Gets the npiExists value for this Provider.
     * 
     * @return npiExists
     */
    public java.lang.Boolean getNpiExists() {
        return npiExists;
    }


    /**
     * Sets the npiExists value for this Provider.
     * 
     * @param npiExists
     */
    public void setNpiExists(java.lang.Boolean npiExists) {
        this.npiExists = npiExists;
    }


    /**
     * Gets the aTypicalProviderCode value for this Provider.
     * 
     * @return aTypicalProviderCode
     */
    public java.lang.String getATypicalProviderCode() {
        return aTypicalProviderCode;
    }


    /**
     * Sets the aTypicalProviderCode value for this Provider.
     * 
     * @param aTypicalProviderCode
     */
    public void setATypicalProviderCode(java.lang.String aTypicalProviderCode) {
        this.aTypicalProviderCode = aTypicalProviderCode;
    }


    /**
     * Gets the providerRatable value for this Provider.
     * 
     * @return providerRatable
     */
    public java.lang.Boolean getProviderRatable() {
        return providerRatable;
    }


    /**
     * Sets the providerRatable value for this Provider.
     * 
     * @param providerRatable
     */
    public void setProviderRatable(java.lang.Boolean providerRatable) {
        this.providerRatable = providerRatable;
    }


    /**
     * Gets the itsClaimIndicator value for this Provider.
     * 
     * @return itsClaimIndicator
     */
    public java.lang.String getItsClaimIndicator() {
        return itsClaimIndicator;
    }


    /**
     * Sets the itsClaimIndicator value for this Provider.
     * 
     * @param itsClaimIndicator
     */
    public void setItsClaimIndicator(java.lang.String itsClaimIndicator) {
        this.itsClaimIndicator = itsClaimIndicator;
    }


    /**
     * Gets the itsClassIndicator value for this Provider.
     * 
     * @return itsClassIndicator
     */
    public java.lang.String getItsClassIndicator() {
        return itsClassIndicator;
    }


    /**
     * Sets the itsClassIndicator value for this Provider.
     * 
     * @param itsClassIndicator
     */
    public void setItsClassIndicator(java.lang.String itsClassIndicator) {
        this.itsClassIndicator = itsClassIndicator;
    }


    /**
     * Gets the accreditationType value for this Provider.
     * 
     * @return accreditationType
     */
    public java.lang.String getAccreditationType() {
        return accreditationType;
    }


    /**
     * Sets the accreditationType value for this Provider.
     * 
     * @param accreditationType
     */
    public void setAccreditationType(java.lang.String accreditationType) {
        this.accreditationType = accreditationType;
    }


    /**
     * Gets the patientCenteredMedicalHomeIndicator value for this Provider.
     * 
     * @return patientCenteredMedicalHomeIndicator
     */
    public java.lang.Boolean getPatientCenteredMedicalHomeIndicator() {
        return patientCenteredMedicalHomeIndicator;
    }


    /**
     * Sets the patientCenteredMedicalHomeIndicator value for this Provider.
     * 
     * @param patientCenteredMedicalHomeIndicator
     */
    public void setPatientCenteredMedicalHomeIndicator(java.lang.Boolean patientCenteredMedicalHomeIndicator) {
        this.patientCenteredMedicalHomeIndicator = patientCenteredMedicalHomeIndicator;
    }


    /**
     * Gets the inPpcNetwork value for this Provider.
     * 
     * @return inPpcNetwork
     */
    public java.lang.Boolean getInPpcNetwork() {
        return inPpcNetwork;
    }


    /**
     * Sets the inPpcNetwork value for this Provider.
     * 
     * @param inPpcNetwork
     */
    public void setInPpcNetwork(java.lang.Boolean inPpcNetwork) {
        this.inPpcNetwork = inPpcNetwork;
    }


    /**
     * Gets the networkCode value for this Provider.
     * 
     * @return networkCode
     */
    public java.lang.String getNetworkCode() {
        return networkCode;
    }


    /**
     * Sets the networkCode value for this Provider.
     * 
     * @param networkCode
     */
    public void setNetworkCode(java.lang.String networkCode) {
        this.networkCode = networkCode;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Provider)) return false;
        Provider other = (Provider) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.entityQualifier==null && other.getEntityQualifier()==null) || 
             (this.entityQualifier!=null &&
              this.entityQualifier.equals(other.getEntityQualifier()))) &&
            ((this.entityCode==null && other.getEntityCode()==null) || 
             (this.entityCode!=null &&
              this.entityCode.equals(other.getEntityCode()))) &&
            ((this.bcaEntityQualifier==null && other.getBcaEntityQualifier()==null) || 
             (this.bcaEntityQualifier!=null &&
              this.bcaEntityQualifier.equals(other.getBcaEntityQualifier()))) &&
            ((this.providerId==null && other.getProviderId()==null) || 
             (this.providerId!=null &&
              this.providerId.equals(other.getProviderId()))) &&
            ((this.providerName==null && other.getProviderName()==null) || 
             (this.providerName!=null &&
              this.providerName.equals(other.getProviderName()))) &&
            ((this.bcaSpecialty==null && other.getBcaSpecialty()==null) || 
             (this.bcaSpecialty!=null &&
              this.bcaSpecialty.equals(other.getBcaSpecialty()))) &&
            ((this.providerFormattedName==null && other.getProviderFormattedName()==null) || 
             (this.providerFormattedName!=null &&
              this.providerFormattedName.equals(other.getProviderFormattedName()))) &&
            ((this.status==null && other.getStatus()==null) || 
             (this.status!=null &&
              this.status.equals(other.getStatus()))) &&
            ((this.gender==null && other.getGender()==null) || 
             (this.gender!=null &&
              this.gender.equals(other.getGender()))) &&
            ((this.acceptsAssignment==null && other.getAcceptsAssignment()==null) || 
             (this.acceptsAssignment!=null &&
              this.acceptsAssignment.equals(other.getAcceptsAssignment()))) &&
            ((this.signatureOnFile==null && other.getSignatureOnFile()==null) || 
             (this.signatureOnFile!=null &&
              this.signatureOnFile.equals(other.getSignatureOnFile()))) &&
            ((this.aTypicalProvider==null && other.getATypicalProvider()==null) || 
             (this.aTypicalProvider!=null &&
              this.aTypicalProvider.equals(other.getATypicalProvider()))) &&
            ((this.acceptsDenteMax==null && other.getAcceptsDenteMax()==null) || 
             (this.acceptsDenteMax!=null &&
              this.acceptsDenteMax.equals(other.getAcceptsDenteMax()))) &&
            ((this.providerOnFile==null && other.getProviderOnFile()==null) || 
             (this.providerOnFile!=null &&
              this.providerOnFile.equals(other.getProviderOnFile()))) &&
            ((this.npiRequired==null && other.getNpiRequired()==null) || 
             (this.npiRequired!=null &&
              this.npiRequired.equals(other.getNpiRequired()))) &&
            ((this.providerClass==null && other.getProviderClass()==null) || 
             (this.providerClass!=null &&
              this.providerClass.equals(other.getProviderClass()))) &&
            ((this.providerCategory==null && other.getProviderCategory()==null) || 
             (this.providerCategory!=null &&
              this.providerCategory.equals(other.getProviderCategory()))) &&
            ((this.providerTaxIdNumberType==null && other.getProviderTaxIdNumberType()==null) || 
             (this.providerTaxIdNumberType!=null &&
              this.providerTaxIdNumberType.equals(other.getProviderTaxIdNumberType()))) &&
            ((this.taxonomyCode==null && other.getTaxonomyCode()==null) || 
             (this.taxonomyCode!=null &&
              this.taxonomyCode.equals(other.getTaxonomyCode()))) &&
            ((this.tpoCode==null && other.getTpoCode()==null) || 
             (this.tpoCode!=null &&
              this.tpoCode.equals(other.getTpoCode()))) &&
            ((this.bcaDirectorySupressIndicator==null && other.getBcaDirectorySupressIndicator()==null) || 
             (this.bcaDirectorySupressIndicator!=null &&
              this.bcaDirectorySupressIndicator.equals(other.getBcaDirectorySupressIndicator()))) &&
            ((this.npiIndicator==null && other.getNpiIndicator()==null) || 
             (this.npiIndicator!=null &&
              this.npiIndicator.equals(other.getNpiIndicator()))) &&
            ((this.npiExists==null && other.getNpiExists()==null) || 
             (this.npiExists!=null &&
              this.npiExists.equals(other.getNpiExists()))) &&
            ((this.aTypicalProviderCode==null && other.getATypicalProviderCode()==null) || 
             (this.aTypicalProviderCode!=null &&
              this.aTypicalProviderCode.equals(other.getATypicalProviderCode()))) &&
            ((this.providerRatable==null && other.getProviderRatable()==null) || 
             (this.providerRatable!=null &&
              this.providerRatable.equals(other.getProviderRatable()))) &&
            ((this.itsClaimIndicator==null && other.getItsClaimIndicator()==null) || 
             (this.itsClaimIndicator!=null &&
              this.itsClaimIndicator.equals(other.getItsClaimIndicator()))) &&
            ((this.itsClassIndicator==null && other.getItsClassIndicator()==null) || 
             (this.itsClassIndicator!=null &&
              this.itsClassIndicator.equals(other.getItsClassIndicator()))) &&
            ((this.accreditationType==null && other.getAccreditationType()==null) || 
             (this.accreditationType!=null &&
              this.accreditationType.equals(other.getAccreditationType()))) &&
            ((this.patientCenteredMedicalHomeIndicator==null && other.getPatientCenteredMedicalHomeIndicator()==null) || 
             (this.patientCenteredMedicalHomeIndicator!=null &&
              this.patientCenteredMedicalHomeIndicator.equals(other.getPatientCenteredMedicalHomeIndicator()))) &&
            ((this.inPpcNetwork==null && other.getInPpcNetwork()==null) || 
             (this.inPpcNetwork!=null &&
              this.inPpcNetwork.equals(other.getInPpcNetwork()))) &&
            ((this.networkCode==null && other.getNetworkCode()==null) || 
             (this.networkCode!=null &&
              this.networkCode.equals(other.getNetworkCode())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getEntityQualifier() != null) {
            _hashCode += getEntityQualifier().hashCode();
        }
        if (getEntityCode() != null) {
            _hashCode += getEntityCode().hashCode();
        }
        if (getBcaEntityQualifier() != null) {
            _hashCode += getBcaEntityQualifier().hashCode();
        }
        if (getProviderId() != null) {
            _hashCode += getProviderId().hashCode();
        }
        if (getProviderName() != null) {
            _hashCode += getProviderName().hashCode();
        }
        if (getBcaSpecialty() != null) {
            _hashCode += getBcaSpecialty().hashCode();
        }
        if (getProviderFormattedName() != null) {
            _hashCode += getProviderFormattedName().hashCode();
        }
        if (getStatus() != null) {
            _hashCode += getStatus().hashCode();
        }
        if (getGender() != null) {
            _hashCode += getGender().hashCode();
        }
        if (getAcceptsAssignment() != null) {
            _hashCode += getAcceptsAssignment().hashCode();
        }
        if (getSignatureOnFile() != null) {
            _hashCode += getSignatureOnFile().hashCode();
        }
        if (getATypicalProvider() != null) {
            _hashCode += getATypicalProvider().hashCode();
        }
        if (getAcceptsDenteMax() != null) {
            _hashCode += getAcceptsDenteMax().hashCode();
        }
        if (getProviderOnFile() != null) {
            _hashCode += getProviderOnFile().hashCode();
        }
        if (getNpiRequired() != null) {
            _hashCode += getNpiRequired().hashCode();
        }
        if (getProviderClass() != null) {
            _hashCode += getProviderClass().hashCode();
        }
        if (getProviderCategory() != null) {
            _hashCode += getProviderCategory().hashCode();
        }
        if (getProviderTaxIdNumberType() != null) {
            _hashCode += getProviderTaxIdNumberType().hashCode();
        }
        if (getTaxonomyCode() != null) {
            _hashCode += getTaxonomyCode().hashCode();
        }
        if (getTpoCode() != null) {
            _hashCode += getTpoCode().hashCode();
        }
        if (getBcaDirectorySupressIndicator() != null) {
            _hashCode += getBcaDirectorySupressIndicator().hashCode();
        }
        if (getNpiIndicator() != null) {
            _hashCode += getNpiIndicator().hashCode();
        }
        if (getNpiExists() != null) {
            _hashCode += getNpiExists().hashCode();
        }
        if (getATypicalProviderCode() != null) {
            _hashCode += getATypicalProviderCode().hashCode();
        }
        if (getProviderRatable() != null) {
            _hashCode += getProviderRatable().hashCode();
        }
        if (getItsClaimIndicator() != null) {
            _hashCode += getItsClaimIndicator().hashCode();
        }
        if (getItsClassIndicator() != null) {
            _hashCode += getItsClassIndicator().hashCode();
        }
        if (getAccreditationType() != null) {
            _hashCode += getAccreditationType().hashCode();
        }
        if (getPatientCenteredMedicalHomeIndicator() != null) {
            _hashCode += getPatientCenteredMedicalHomeIndicator().hashCode();
        }
        if (getInPpcNetwork() != null) {
            _hashCode += getInPpcNetwork().hashCode();
        }
        if (getNetworkCode() != null) {
            _hashCode += getNetworkCode().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Provider.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://servicehelpers.esb.avalon.com", "Provider"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("entityQualifier");
        elemField.setXmlName(new javax.xml.namespace.QName("", "entityQualifier"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://servicehelpers.esb.avalon.com", "Code"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("entityCode");
        elemField.setXmlName(new javax.xml.namespace.QName("", "entityCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://servicehelpers.esb.avalon.com", "Code"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("bcaEntityQualifier");
        elemField.setXmlName(new javax.xml.namespace.QName("", "bcaEntityQualifier"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://servicehelpers.esb.avalon.com", "Code"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("providerId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "providerId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://servicehelpers.esb.avalon.com", "ProviderId"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("providerName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "providerName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://servicehelpers.esb.avalon.com", "Name"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("bcaSpecialty");
        elemField.setXmlName(new javax.xml.namespace.QName("", "bcaSpecialty"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://servicehelpers.esb.avalon.com", "Code"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("providerFormattedName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "providerFormattedName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("status");
        elemField.setXmlName(new javax.xml.namespace.QName("", "status"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://servicehelpers.esb.avalon.com", "Code"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("gender");
        elemField.setXmlName(new javax.xml.namespace.QName("", "gender"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://servicehelpers.esb.avalon.com", "Code"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("acceptsAssignment");
        elemField.setXmlName(new javax.xml.namespace.QName("", "acceptsAssignment"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://servicehelpers.esb.avalon.com", "Code"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("signatureOnFile");
        elemField.setXmlName(new javax.xml.namespace.QName("", "signatureOnFile"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://servicehelpers.esb.avalon.com", "Code"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ATypicalProvider");
        elemField.setXmlName(new javax.xml.namespace.QName("", "aTypicalProvider"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("acceptsDenteMax");
        elemField.setXmlName(new javax.xml.namespace.QName("", "acceptsDenteMax"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("providerOnFile");
        elemField.setXmlName(new javax.xml.namespace.QName("", "providerOnFile"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("npiRequired");
        elemField.setXmlName(new javax.xml.namespace.QName("", "npiRequired"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("providerClass");
        elemField.setXmlName(new javax.xml.namespace.QName("", "providerClass"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("providerCategory");
        elemField.setXmlName(new javax.xml.namespace.QName("", "providerCategory"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("providerTaxIdNumberType");
        elemField.setXmlName(new javax.xml.namespace.QName("", "providerTaxIdNumberType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("taxonomyCode");
        elemField.setXmlName(new javax.xml.namespace.QName("", "taxonomyCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://servicehelpers.esb.avalon.com", "Code"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tpoCode");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tpoCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("bcaDirectorySupressIndicator");
        elemField.setXmlName(new javax.xml.namespace.QName("", "bcaDirectorySupressIndicator"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("npiIndicator");
        elemField.setXmlName(new javax.xml.namespace.QName("", "npiIndicator"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("npiExists");
        elemField.setXmlName(new javax.xml.namespace.QName("", "npiExists"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ATypicalProviderCode");
        elemField.setXmlName(new javax.xml.namespace.QName("", "aTypicalProviderCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("providerRatable");
        elemField.setXmlName(new javax.xml.namespace.QName("", "providerRatable"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("itsClaimIndicator");
        elemField.setXmlName(new javax.xml.namespace.QName("", "itsClaimIndicator"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("itsClassIndicator");
        elemField.setXmlName(new javax.xml.namespace.QName("", "itsClassIndicator"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("accreditationType");
        elemField.setXmlName(new javax.xml.namespace.QName("", "accreditationType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("patientCenteredMedicalHomeIndicator");
        elemField.setXmlName(new javax.xml.namespace.QName("", "patientCenteredMedicalHomeIndicator"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("inPpcNetwork");
        elemField.setXmlName(new javax.xml.namespace.QName("", "inPpcNetwork"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("networkCode");
        elemField.setXmlName(new javax.xml.namespace.QName("", "networkCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
