/**
 * OutOfAreaClaimsSummaryItem.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.avalon.esb.servicehelpers;

public class OutOfAreaClaimsSummaryItem  implements java.io.Serializable {
    private java.util.Calendar statusDate;

    private java.math.BigDecimal totalChargeAmount;

    private java.math.BigDecimal paymentAmount;

    private java.util.Calendar paidDate;

    private java.util.Calendar checkIssuedDate;

    private java.lang.String checkNumber;

    public OutOfAreaClaimsSummaryItem() {
    }

    public OutOfAreaClaimsSummaryItem(
           java.util.Calendar statusDate,
           java.math.BigDecimal totalChargeAmount,
           java.math.BigDecimal paymentAmount,
           java.util.Calendar paidDate,
           java.util.Calendar checkIssuedDate,
           java.lang.String checkNumber) {
           this.statusDate = statusDate;
           this.totalChargeAmount = totalChargeAmount;
           this.paymentAmount = paymentAmount;
           this.paidDate = paidDate;
           this.checkIssuedDate = checkIssuedDate;
           this.checkNumber = checkNumber;
    }


    /**
     * Gets the statusDate value for this OutOfAreaClaimsSummaryItem.
     * 
     * @return statusDate
     */
    public java.util.Calendar getStatusDate() {
        return statusDate;
    }


    /**
     * Sets the statusDate value for this OutOfAreaClaimsSummaryItem.
     * 
     * @param statusDate
     */
    public void setStatusDate(java.util.Calendar statusDate) {
        this.statusDate = statusDate;
    }


    /**
     * Gets the totalChargeAmount value for this OutOfAreaClaimsSummaryItem.
     * 
     * @return totalChargeAmount
     */
    public java.math.BigDecimal getTotalChargeAmount() {
        return totalChargeAmount;
    }


    /**
     * Sets the totalChargeAmount value for this OutOfAreaClaimsSummaryItem.
     * 
     * @param totalChargeAmount
     */
    public void setTotalChargeAmount(java.math.BigDecimal totalChargeAmount) {
        this.totalChargeAmount = totalChargeAmount;
    }


    /**
     * Gets the paymentAmount value for this OutOfAreaClaimsSummaryItem.
     * 
     * @return paymentAmount
     */
    public java.math.BigDecimal getPaymentAmount() {
        return paymentAmount;
    }


    /**
     * Sets the paymentAmount value for this OutOfAreaClaimsSummaryItem.
     * 
     * @param paymentAmount
     */
    public void setPaymentAmount(java.math.BigDecimal paymentAmount) {
        this.paymentAmount = paymentAmount;
    }


    /**
     * Gets the paidDate value for this OutOfAreaClaimsSummaryItem.
     * 
     * @return paidDate
     */
    public java.util.Calendar getPaidDate() {
        return paidDate;
    }


    /**
     * Sets the paidDate value for this OutOfAreaClaimsSummaryItem.
     * 
     * @param paidDate
     */
    public void setPaidDate(java.util.Calendar paidDate) {
        this.paidDate = paidDate;
    }


    /**
     * Gets the checkIssuedDate value for this OutOfAreaClaimsSummaryItem.
     * 
     * @return checkIssuedDate
     */
    public java.util.Calendar getCheckIssuedDate() {
        return checkIssuedDate;
    }


    /**
     * Sets the checkIssuedDate value for this OutOfAreaClaimsSummaryItem.
     * 
     * @param checkIssuedDate
     */
    public void setCheckIssuedDate(java.util.Calendar checkIssuedDate) {
        this.checkIssuedDate = checkIssuedDate;
    }


    /**
     * Gets the checkNumber value for this OutOfAreaClaimsSummaryItem.
     * 
     * @return checkNumber
     */
    public java.lang.String getCheckNumber() {
        return checkNumber;
    }


    /**
     * Sets the checkNumber value for this OutOfAreaClaimsSummaryItem.
     * 
     * @param checkNumber
     */
    public void setCheckNumber(java.lang.String checkNumber) {
        this.checkNumber = checkNumber;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof OutOfAreaClaimsSummaryItem)) return false;
        OutOfAreaClaimsSummaryItem other = (OutOfAreaClaimsSummaryItem) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.statusDate==null && other.getStatusDate()==null) || 
             (this.statusDate!=null &&
              this.statusDate.equals(other.getStatusDate()))) &&
            ((this.totalChargeAmount==null && other.getTotalChargeAmount()==null) || 
             (this.totalChargeAmount!=null &&
              this.totalChargeAmount.equals(other.getTotalChargeAmount()))) &&
            ((this.paymentAmount==null && other.getPaymentAmount()==null) || 
             (this.paymentAmount!=null &&
              this.paymentAmount.equals(other.getPaymentAmount()))) &&
            ((this.paidDate==null && other.getPaidDate()==null) || 
             (this.paidDate!=null &&
              this.paidDate.equals(other.getPaidDate()))) &&
            ((this.checkIssuedDate==null && other.getCheckIssuedDate()==null) || 
             (this.checkIssuedDate!=null &&
              this.checkIssuedDate.equals(other.getCheckIssuedDate()))) &&
            ((this.checkNumber==null && other.getCheckNumber()==null) || 
             (this.checkNumber!=null &&
              this.checkNumber.equals(other.getCheckNumber())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getStatusDate() != null) {
            _hashCode += getStatusDate().hashCode();
        }
        if (getTotalChargeAmount() != null) {
            _hashCode += getTotalChargeAmount().hashCode();
        }
        if (getPaymentAmount() != null) {
            _hashCode += getPaymentAmount().hashCode();
        }
        if (getPaidDate() != null) {
            _hashCode += getPaidDate().hashCode();
        }
        if (getCheckIssuedDate() != null) {
            _hashCode += getCheckIssuedDate().hashCode();
        }
        if (getCheckNumber() != null) {
            _hashCode += getCheckNumber().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(OutOfAreaClaimsSummaryItem.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://servicehelpers.esb.avalon.com", "OutOfAreaClaimsSummaryItem"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("statusDate");
        elemField.setXmlName(new javax.xml.namespace.QName("", "statusDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("totalChargeAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("", "totalChargeAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("paymentAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("", "paymentAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("paidDate");
        elemField.setXmlName(new javax.xml.namespace.QName("", "paidDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("checkIssuedDate");
        elemField.setXmlName(new javax.xml.namespace.QName("", "checkIssuedDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("checkNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("", "checkNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
