/**
 * SubscriberId.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.avalon.esb.servicehelpers;

public class SubscriberId  implements java.io.Serializable {
    private java.lang.String memberNumber;

    private java.lang.String idCardNumber;

    public SubscriberId() {
    }

    public SubscriberId(
           java.lang.String memberNumber,
           java.lang.String idCardNumber) {
           this.memberNumber = memberNumber;
           this.idCardNumber = idCardNumber;
    }


    /**
     * Gets the memberNumber value for this SubscriberId.
     * 
     * @return memberNumber
     */
    public java.lang.String getMemberNumber() {
        return memberNumber;
    }


    /**
     * Sets the memberNumber value for this SubscriberId.
     * 
     * @param memberNumber
     */
    public void setMemberNumber(java.lang.String memberNumber) {
        this.memberNumber = memberNumber;
    }


    /**
     * Gets the idCardNumber value for this SubscriberId.
     * 
     * @return idCardNumber
     */
    public java.lang.String getIdCardNumber() {
        return idCardNumber;
    }


    /**
     * Sets the idCardNumber value for this SubscriberId.
     * 
     * @param idCardNumber
     */
    public void setIdCardNumber(java.lang.String idCardNumber) {
        this.idCardNumber = idCardNumber;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SubscriberId)) return false;
        SubscriberId other = (SubscriberId) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.memberNumber==null && other.getMemberNumber()==null) || 
             (this.memberNumber!=null &&
              this.memberNumber.equals(other.getMemberNumber()))) &&
            ((this.idCardNumber==null && other.getIdCardNumber()==null) || 
             (this.idCardNumber!=null &&
              this.idCardNumber.equals(other.getIdCardNumber())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getMemberNumber() != null) {
            _hashCode += getMemberNumber().hashCode();
        }
        if (getIdCardNumber() != null) {
            _hashCode += getIdCardNumber().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SubscriberId.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://servicehelpers.esb.avalon.com", "SubscriberId"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("memberNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("", "memberNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("idCardNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("", "idCardNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
