/**
 * ValueCode.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.avalon.esb.servicehelpers;

public class ValueCode  implements java.io.Serializable {
    private java.lang.String valueCode;

    private java.lang.String valueAmount;

    public ValueCode() {
    }

    public ValueCode(
           java.lang.String valueCode,
           java.lang.String valueAmount) {
           this.valueCode = valueCode;
           this.valueAmount = valueAmount;
    }


    /**
     * Gets the valueCode value for this ValueCode.
     * 
     * @return valueCode
     */
    public java.lang.String getValueCode() {
        return valueCode;
    }


    /**
     * Sets the valueCode value for this ValueCode.
     * 
     * @param valueCode
     */
    public void setValueCode(java.lang.String valueCode) {
        this.valueCode = valueCode;
    }


    /**
     * Gets the valueAmount value for this ValueCode.
     * 
     * @return valueAmount
     */
    public java.lang.String getValueAmount() {
        return valueAmount;
    }


    /**
     * Sets the valueAmount value for this ValueCode.
     * 
     * @param valueAmount
     */
    public void setValueAmount(java.lang.String valueAmount) {
        this.valueAmount = valueAmount;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ValueCode)) return false;
        ValueCode other = (ValueCode) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.valueCode==null && other.getValueCode()==null) || 
             (this.valueCode!=null &&
              this.valueCode.equals(other.getValueCode()))) &&
            ((this.valueAmount==null && other.getValueAmount()==null) || 
             (this.valueAmount!=null &&
              this.valueAmount.equals(other.getValueAmount())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getValueCode() != null) {
            _hashCode += getValueCode().hashCode();
        }
        if (getValueAmount() != null) {
            _hashCode += getValueAmount().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ValueCode.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://servicehelpers.esb.avalon.com", "ValueCode"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("valueCode");
        elemField.setXmlName(new javax.xml.namespace.QName("", "valueCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("valueAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("", "valueAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
