/**
 * DentalClaimDetail.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.avalon.esb.servicehelpers;

public class DentalClaimDetail  implements java.io.Serializable {
    private java.lang.Boolean pretreatment;

    private java.lang.Boolean xraysTaken;

    private java.lang.Boolean orthodontic;

    private int requestedOrthodonticVisits;

    private int orthodonticVisitsUsed;

    private java.util.Calendar lastRecertificationDate;

    private java.lang.Boolean pretreatmentEstimateLetterAvailable;

    private java.lang.Boolean orthodonticLetterAvailable;

    public DentalClaimDetail() {
    }

    public DentalClaimDetail(
           java.lang.Boolean pretreatment,
           java.lang.Boolean xraysTaken,
           java.lang.Boolean orthodontic,
           int requestedOrthodonticVisits,
           int orthodonticVisitsUsed,
           java.util.Calendar lastRecertificationDate,
           java.lang.Boolean pretreatmentEstimateLetterAvailable,
           java.lang.Boolean orthodonticLetterAvailable) {
           this.pretreatment = pretreatment;
           this.xraysTaken = xraysTaken;
           this.orthodontic = orthodontic;
           this.requestedOrthodonticVisits = requestedOrthodonticVisits;
           this.orthodonticVisitsUsed = orthodonticVisitsUsed;
           this.lastRecertificationDate = lastRecertificationDate;
           this.pretreatmentEstimateLetterAvailable = pretreatmentEstimateLetterAvailable;
           this.orthodonticLetterAvailable = orthodonticLetterAvailable;
    }


    /**
     * Gets the pretreatment value for this DentalClaimDetail.
     * 
     * @return pretreatment
     */
    public java.lang.Boolean getPretreatment() {
        return pretreatment;
    }


    /**
     * Sets the pretreatment value for this DentalClaimDetail.
     * 
     * @param pretreatment
     */
    public void setPretreatment(java.lang.Boolean pretreatment) {
        this.pretreatment = pretreatment;
    }


    /**
     * Gets the xraysTaken value for this DentalClaimDetail.
     * 
     * @return xraysTaken
     */
    public java.lang.Boolean getXraysTaken() {
        return xraysTaken;
    }


    /**
     * Sets the xraysTaken value for this DentalClaimDetail.
     * 
     * @param xraysTaken
     */
    public void setXraysTaken(java.lang.Boolean xraysTaken) {
        this.xraysTaken = xraysTaken;
    }


    /**
     * Gets the orthodontic value for this DentalClaimDetail.
     * 
     * @return orthodontic
     */
    public java.lang.Boolean getOrthodontic() {
        return orthodontic;
    }


    /**
     * Sets the orthodontic value for this DentalClaimDetail.
     * 
     * @param orthodontic
     */
    public void setOrthodontic(java.lang.Boolean orthodontic) {
        this.orthodontic = orthodontic;
    }


    /**
     * Gets the requestedOrthodonticVisits value for this DentalClaimDetail.
     * 
     * @return requestedOrthodonticVisits
     */
    public int getRequestedOrthodonticVisits() {
        return requestedOrthodonticVisits;
    }


    /**
     * Sets the requestedOrthodonticVisits value for this DentalClaimDetail.
     * 
     * @param requestedOrthodonticVisits
     */
    public void setRequestedOrthodonticVisits(int requestedOrthodonticVisits) {
        this.requestedOrthodonticVisits = requestedOrthodonticVisits;
    }


    /**
     * Gets the orthodonticVisitsUsed value for this DentalClaimDetail.
     * 
     * @return orthodonticVisitsUsed
     */
    public int getOrthodonticVisitsUsed() {
        return orthodonticVisitsUsed;
    }


    /**
     * Sets the orthodonticVisitsUsed value for this DentalClaimDetail.
     * 
     * @param orthodonticVisitsUsed
     */
    public void setOrthodonticVisitsUsed(int orthodonticVisitsUsed) {
        this.orthodonticVisitsUsed = orthodonticVisitsUsed;
    }


    /**
     * Gets the lastRecertificationDate value for this DentalClaimDetail.
     * 
     * @return lastRecertificationDate
     */
    public java.util.Calendar getLastRecertificationDate() {
        return lastRecertificationDate;
    }


    /**
     * Sets the lastRecertificationDate value for this DentalClaimDetail.
     * 
     * @param lastRecertificationDate
     */
    public void setLastRecertificationDate(java.util.Calendar lastRecertificationDate) {
        this.lastRecertificationDate = lastRecertificationDate;
    }


    /**
     * Gets the pretreatmentEstimateLetterAvailable value for this DentalClaimDetail.
     * 
     * @return pretreatmentEstimateLetterAvailable
     */
    public java.lang.Boolean getPretreatmentEstimateLetterAvailable() {
        return pretreatmentEstimateLetterAvailable;
    }


    /**
     * Sets the pretreatmentEstimateLetterAvailable value for this DentalClaimDetail.
     * 
     * @param pretreatmentEstimateLetterAvailable
     */
    public void setPretreatmentEstimateLetterAvailable(java.lang.Boolean pretreatmentEstimateLetterAvailable) {
        this.pretreatmentEstimateLetterAvailable = pretreatmentEstimateLetterAvailable;
    }


    /**
     * Gets the orthodonticLetterAvailable value for this DentalClaimDetail.
     * 
     * @return orthodonticLetterAvailable
     */
    public java.lang.Boolean getOrthodonticLetterAvailable() {
        return orthodonticLetterAvailable;
    }


    /**
     * Sets the orthodonticLetterAvailable value for this DentalClaimDetail.
     * 
     * @param orthodonticLetterAvailable
     */
    public void setOrthodonticLetterAvailable(java.lang.Boolean orthodonticLetterAvailable) {
        this.orthodonticLetterAvailable = orthodonticLetterAvailable;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof DentalClaimDetail)) return false;
        DentalClaimDetail other = (DentalClaimDetail) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.pretreatment==null && other.getPretreatment()==null) || 
             (this.pretreatment!=null &&
              this.pretreatment.equals(other.getPretreatment()))) &&
            ((this.xraysTaken==null && other.getXraysTaken()==null) || 
             (this.xraysTaken!=null &&
              this.xraysTaken.equals(other.getXraysTaken()))) &&
            ((this.orthodontic==null && other.getOrthodontic()==null) || 
             (this.orthodontic!=null &&
              this.orthodontic.equals(other.getOrthodontic()))) &&
            this.requestedOrthodonticVisits == other.getRequestedOrthodonticVisits() &&
            this.orthodonticVisitsUsed == other.getOrthodonticVisitsUsed() &&
            ((this.lastRecertificationDate==null && other.getLastRecertificationDate()==null) || 
             (this.lastRecertificationDate!=null &&
              this.lastRecertificationDate.equals(other.getLastRecertificationDate()))) &&
            ((this.pretreatmentEstimateLetterAvailable==null && other.getPretreatmentEstimateLetterAvailable()==null) || 
             (this.pretreatmentEstimateLetterAvailable!=null &&
              this.pretreatmentEstimateLetterAvailable.equals(other.getPretreatmentEstimateLetterAvailable()))) &&
            ((this.orthodonticLetterAvailable==null && other.getOrthodonticLetterAvailable()==null) || 
             (this.orthodonticLetterAvailable!=null &&
              this.orthodonticLetterAvailable.equals(other.getOrthodonticLetterAvailable())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getPretreatment() != null) {
            _hashCode += getPretreatment().hashCode();
        }
        if (getXraysTaken() != null) {
            _hashCode += getXraysTaken().hashCode();
        }
        if (getOrthodontic() != null) {
            _hashCode += getOrthodontic().hashCode();
        }
        _hashCode += getRequestedOrthodonticVisits();
        _hashCode += getOrthodonticVisitsUsed();
        if (getLastRecertificationDate() != null) {
            _hashCode += getLastRecertificationDate().hashCode();
        }
        if (getPretreatmentEstimateLetterAvailable() != null) {
            _hashCode += getPretreatmentEstimateLetterAvailable().hashCode();
        }
        if (getOrthodonticLetterAvailable() != null) {
            _hashCode += getOrthodonticLetterAvailable().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(DentalClaimDetail.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://servicehelpers.esb.avalon.com", "DentalClaimDetail"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pretreatment");
        elemField.setXmlName(new javax.xml.namespace.QName("", "pretreatment"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("xraysTaken");
        elemField.setXmlName(new javax.xml.namespace.QName("", "xraysTaken"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("orthodontic");
        elemField.setXmlName(new javax.xml.namespace.QName("", "orthodontic"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("requestedOrthodonticVisits");
        elemField.setXmlName(new javax.xml.namespace.QName("", "requestedOrthodonticVisits"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("orthodonticVisitsUsed");
        elemField.setXmlName(new javax.xml.namespace.QName("", "orthodonticVisitsUsed"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("lastRecertificationDate");
        elemField.setXmlName(new javax.xml.namespace.QName("", "lastRecertificationDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pretreatmentEstimateLetterAvailable");
        elemField.setXmlName(new javax.xml.namespace.QName("", "pretreatmentEstimateLetterAvailable"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("orthodonticLetterAvailable");
        elemField.setXmlName(new javax.xml.namespace.QName("", "orthodonticLetterAvailable"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
