package com.avalon.claimsearch.fo;

/**
 * Description
 *		This file contain the Claims Search constants.
 *
 * CHANGE History
 *		Version 1.0
 *     		Initial version.
 *		Version 1.1				02/07/2018
 *     		Changed Reason for Inquiry to S.
 *  	Version 1.3				03/08/2017
 *  		Added a comstant for the properties file.

 */

public interface ClaimsSearchConstants {
	public static final String PROPERTIES_FILE ="lbm-common.properties";

	// JUnit variable
	public static final String CARRIER_CODE_VALUE_SC ="0010";
	
	public static final String HARD_PENDED = "HARD_PENDED";
	public static final String PENDING = "PENDING";
	public static final String PENDING_STATUS = "In Process";
	public static final String DENIED = "DENIED";
	public static final String PROCESSED = "PROCESSED";
	public static final String REJECTED = "REJECTED";
	public static final String DENIED_WITH_CHECKDATE = "Settled";
	public static final String DENIED_WITHOUT_CHECKDATE = "Completed";
	public static final String CLAIM_STATUS = "claimStatus";
	public static final String CLAIM_ID = "claimId";
	public static final String MEMBER_ID = "memberId";
	public static final String PROVIDER_ID = "providerid";
	public static final String DATE_OF_SERVICE_FROM = "dateofServiceFrom";
	public static final String DATE_OF_SERVICE_TO = "dateofServiceTo";
	public static final String TOTAL_CHARGE_GREATER_THAN = "totalChargeGreaterThan";
	public static final String CHECK_ISSUE_DATE = "checkIssueDate";
	public static final String CHECK_NUMBER = "checkNumber";
	public static final String GROUP_ID = "groupID";
	public static final String MEMBER_FIRST_NAME = "memberFirstName";
	public static final String MEMBER_LAST_NAME = "memberLastName";
	public static final String MEMBER_DATE_OF_BIRTH = "dateofBirth";
	public static final String MEMBER_GENDER = "gender";
	public static final String MEMBER_FIND_CLAIMS = "findClaims";
	
	public static final String OKTA_TAX_IDENTIFIER = "taxIdentifier";
	public static final String TAX_IDENTIFIER_SESSION = "TaxIdentifier";
	public static final String CLAIM_MEMBER_KEY = "claimMemberKey";
	public static final String CLAIMS_SEARCHFO = "claimSearchFO";
	public static final String MEMBER_DETAILS_LIST_SESSION = "memberDetailsList"; 
	public static final String MEMBER_SERVICE_ID = "memberID";
	public static final String REASON_FOR_INQUIRY = "S"; 
	public static final String HEALTH_PLAN_ID = "0010";
	public static final String HEALTH_PLAN_ID_TYPE = "PI";

	public static final  boolean TRUE = true;
	public static final  boolean FALSE = false;
	public static final String ERROR = "error";
	public static final String NO_TAX_ID_ERROR = "Missing login information.  Please contact system administrator.";
	public static final String MEMSERVICE_ERROR = "No members were found that match your search criteria. Please update the search and try again.";
	public static final String CLAIMS_ERROR = "No claims were found that match your search criteria. Please update the search and try again.";
	public static final String CLAIM_NOT_FOUND_ON_POINTER_FILE = "CLAIM NOT FOUND ON POINTER FILE";
	public static final String ERROR_WHILE_PROCESSING = "Error while processing the request.  Please contact system administrator.";
	public static final String PROPERTY_FILE_LOCATION = "content/config.properties";

	public static final String VIEW_DISPLAY = "basicSearch";
	public static final int    BLUE_EXCHANGE_DAYS_TO_WAIT = 0;
	//TODO: Remove below code
	public static final String ENABLED_MOCK_SERVICE = "enabled.mock.service";

}
