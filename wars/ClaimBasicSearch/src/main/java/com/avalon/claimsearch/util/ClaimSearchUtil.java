package com.avalon.claimsearch.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;


public class ClaimSearchUtil {
	
	private static final Log log = LogFactoryUtil.getLog(ClaimSearchUtil.class.getName());

	private static ClaimSearchUtil claimSearchUtil = null;

	static {
		if (claimSearchUtil == null) {
			claimSearchUtil = new ClaimSearchUtil();
		}
	}

	private ClaimSearchUtil() {
	}

	public static ClaimSearchUtil getClaimSearchUtil() {
		return claimSearchUtil;
	}
	
	
	/**
	 * To convert the date for claim search
	 */

	public String getClaimSearchDate(String dat, String browser) { 
		String input = dat;
		SimpleDateFormat inputformat = new SimpleDateFormat("MM/dd/yyyy");
		SimpleDateFormat outputformat = new SimpleDateFormat("yyyy-MM-dd");
		Date date = null;
		String output = null;

		try {
			if (browser.equals("IE") || browser.equals("Firefox") || browser.equals("unknown")) {
				date = inputformat.parse(input);
				output = outputformat.format(date);
			} else {
				output = input;
			}
		}
		catch (ParseException e) {
			log.error("Exception in getClaimSearchDate method", e);
		}
		return output;
	}
}
