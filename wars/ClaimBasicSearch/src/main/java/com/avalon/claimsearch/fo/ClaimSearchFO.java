package com.avalon.claimsearch.fo;

import java.io.Serializable;

public class ClaimSearchFO implements Serializable {

	/**
	 * To store the ClaimSearch fields
	 */
	private static final long serialVersionUID = -6518412498371720526L;
		private String memberID;
		private String memberFirstName;
	    private String dateofBirth;
		private String claimStatus;
		private String providerFirstName;
		private String memberLastName;
		private String totalChargeGreaterThan;
		private String providerTIN;
		private String dateofServiceFrom;
		private String providerLastName;
		private String dateofServiceTo;
		private String providerId;
		private String totalCharge;
		private String healthPlan;
		private String healthPlanID;
		private String claimMemberKey;
		private String claimID;
		private String memberClaimID;

		/**
		 * @return the claimMemberKey
		 */
		public ClaimSearchFO(){
			
		}

		public String getClaimMemberKey() {
			return claimMemberKey;
		}

		/**
		 * @param claimMemberKey the claimMemberKey to set
		 */
		public void setClaimMemberKey(String claimMemberKey) {
			this.claimMemberKey = claimMemberKey;
		}

		/**
		 * @return the memberID
		 */
		public String getMemberID() {
			return memberID;
		}

		/**
		 * @param memberID the memberID to set
		 */
		public void setMemberID(String memberID) {
			this.memberID = memberID;
		}
		
		public String getHealthPlan() {
			return healthPlan;
		}

		public void setHealthPlan(String healthPlan) {
			this.healthPlan = healthPlan;
		}
		
		public String getHealthPlanID() {
			return healthPlanID;
		}

		public void setHealthPlanID(String healthPlanID) {
			this.healthPlanID = healthPlanID;
		}

		public String getTotalCharge() {
			return totalCharge;
		}

		public void setTotalCharge(String totalCharge) {
			this.totalCharge = totalCharge;
		}

		public String getProviderId() {
			return providerId;
		}

		public void setProviderId(String providerId) {
			this.providerId = providerId;
		}
		
		public String getMemberFirstName() {
			return memberFirstName;
		}

		public void setMemberFirstName(String memberFirstName) {
			this.memberFirstName = memberFirstName;
		}

		public String getDateofBirth() {
			return dateofBirth;
		}

		public void setDateofBirth(String dateofBirth) {
			this.dateofBirth = dateofBirth;
		}

		public String getClaimStatus() {
			return claimStatus;
		}

		public void setClaimStatus(String claimStatus) {
			this.claimStatus = claimStatus;
		}
		public String getProviderFirstName() {
			return providerFirstName;
		}

		public void setProviderFirstName(String providerFirstName) {
			this.providerFirstName = providerFirstName;
		}

		public String getMemberLastName() {
			return memberLastName;
		}

		public void setMemberLastName(String memberLastName) {
			this.memberLastName = memberLastName;
		}

		public String getTotalChargeGreaterThan() {
			return totalChargeGreaterThan;
		}

		public void setTotalChargeGreaterThan(String totalChargeGreaterThan) {
			this.totalChargeGreaterThan = totalChargeGreaterThan;
		}

		public String getProviderTIN() {
			return providerTIN;
		}

		public void setProviderTIN(String providerTIN) {
			this.providerTIN = providerTIN;
		}

		public String getDateofServiceFrom() {
			return dateofServiceFrom;
		}

		public void setDateofServiceFrom(String dateofServiceFrom) {
			this.dateofServiceFrom = dateofServiceFrom;
		}

		public String getProviderLastName() {
			return providerLastName;
		}

		public void setProviderLastName(String providerLastName) {
			this.providerLastName = providerLastName;
		}

		public String getDateofServiceTo() {
			return dateofServiceTo;
		}

		public void setDateofServiceTo(String dateofServiceTo) {
			this.dateofServiceTo = dateofServiceTo;
		}

		public String getClaimID() {
			return claimID;
		}

		public void setClaimID(String claimID) {
			this.claimID = claimID;
		}

		public String getMemberClaimID() {
			return memberClaimID;
		}

		public void setMemberClaimID(String memberClaimID) {
			this.memberClaimID = memberClaimID;
		}
		
		/* (non-Javadoc):q!
		 * 
		 * @see java.lang.Object#toString()
		 */
		@Override
		public String toString() {
			return "ClaimSearchFO [memberId=" + memberID + ", memberFirstName=" + memberFirstName + ", dateofBirth=" + dateofBirth + ", claimStatus=" + claimStatus +
				   ", providerFirstName=" + providerFirstName + ", memberLastName=" + memberLastName + ", totalChargeGreaterThan=" + totalChargeGreaterThan +
				   ", providerTIN=" + providerTIN + ", dateofServiceFrom=" + dateofServiceFrom + ", providerLastName=" + providerLastName +
				   ", dateofServiceTo=" + dateofServiceTo + ", providerId=" + providerId + ", totalCharge=" + totalCharge  +
				   ", healthPlan=" + healthPlan + ", claimId=" + claimID + ", memberClaimId=" + memberClaimID + "]";
		}
		
}
