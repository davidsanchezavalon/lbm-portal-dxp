/**
  * Description
  *		This file contain the Claims Search portlet.
  *
  * CHANGE History
  * 	Version 1.0
  *     	Removed Cancel button question.
  * 	Version 1.1
  *     	Do not increment date in getCalenderDate method.
  * 	Version 1.2
  *     	Test for check issue date not returned.
  *
  */

package com.avalon.claimsearch.businessdelegate;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;

import javax.portlet.PortletSession;
import javax.xml.datatype.XMLGregorianCalendar;

import com.avalon.claimsearch.customexception.CustomException;
import com.avalon.claimsearch.fo.ClaimSearchFO;
import com.avalon.claimsearch.fo.ClaimsSearchConstants;
import com.avalon.claimsearch.fo.MemberDetailsFo;
import com.avalon.claimsearch.util.ClaimSearchUtil;
import com.avalon.esb.servicehelpers.ClaimSearchCriteria;
import com.avalon.esb.servicehelpers.ClaimSearchFilter;
import com.avalon.esb.servicehelpers.ClaimStatusInboundReq;
import com.avalon.esb.servicehelpers.ClaimSummariesPatient;
import com.avalon.esb.servicehelpers.ClaimsSummaryItem;
import com.avalon.esb.servicehelpers.Code;
import com.avalon.esb.servicehelpers.CoverageInformation;
import com.avalon.esb.servicehelpers.FindClaims;
import com.avalon.esb.servicehelpers.GetClaimDetailsResponse;
import com.avalon.esb.servicehelpers.HealthClaimSearchCriteria;
import com.avalon.esb.servicehelpers.LBMClaimSearchService;
import com.avalon.esb.servicehelpers.LBMClaimSearchServiceProxy;
import com.avalon.esb.servicehelpers.Patient;
import com.avalon.esb.servicehelpers.PriorAuthImplServiceStub;
import com.avalon.esb.servicehelpers.PriorAuthImplServiceStub.GetMemberData;
import com.avalon.esb.servicehelpers.PriorAuthImplServiceStub.GetMemberDataResponse;
import com.avalon.esb.servicehelpers.PriorAuthImplServiceStub.Member;
import com.avalon.esb.servicehelpers.PriorAuthImplServiceStub.MemberDataRequest;
import com.avalon.esb.servicehelpers.PriorAuthImplServiceStub.MoreDataOptions;
import com.avalon.esb.servicehelpers.PriorAuthImplServiceStub.SearchCriteria;
import com.avalon.esb.servicehelpers.ProviderIdentifier;
import com.avalon.esb.servicehelpers.ProviderInformation;
import com.avalon.esb.servicehelpers.SubscriberId;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;


/**
 * @author David Sanchez
 *
 */
public class ClaimsSearchBusinessDelegateVo implements ClaimsSearchConstants{
	
    
	private static ClaimsSearchBusinessDelegateVo claimsSearchBusinessDelegateVo = null;
	private static Properties properties = null;
	
	private static final Log log = LogFactoryUtil.getLog(ClaimsSearchBusinessDelegateVo.class.getName());
	
	private ClaimsSearchBusinessDelegateVo() {
		
	}
	     
	
	static{
	    
		if (properties == null) {
		    properties = new Properties();
		    claimsSearchBusinessDelegateVo = new ClaimsSearchBusinessDelegateVo();
		    String responseCodePropertyPath = System.getProperty("LIFERAY_HOME");
		    String propertyLocation = responseCodePropertyPath + "/PAMessages_en_US.properties";
		    log.info("responseCodePropertyPath: " + responseCodePropertyPath);
			try {
			    properties.load(new FileInputStream(new File(propertyLocation)));
			} catch (FileNotFoundException e) {
				log.error("FileNotFoundException in static block", e);
			} catch (IOException e) {
				log.error("IOException in static block", e);
			}
			
	    }
	}
	
	public static ClaimsSearchBusinessDelegateVo getClaimsSearchBusinessDelegateVo() {
		return claimsSearchBusinessDelegateVo;
	}
	
	/**
	 * 
	 * @param date
	 * @return
	 * To Covert the  date into yyyy-mm-dd format for request object
	 */
	public String getDate(String date) {
		StringBuilder stringBuilder = new StringBuilder();
		
		try {
		
			char[] ch = date.toCharArray();  // To Covert the  date into yyyy-mm-dd format for request object
			stringBuilder.append(ch[6]);
			stringBuilder.append(ch[7]);
			stringBuilder.append(ch[8]);
			stringBuilder.append(ch[9]);
			stringBuilder.append("-");
			stringBuilder.append(ch[0]);
			stringBuilder.append(ch[1]);
			stringBuilder.append("-");
			stringBuilder.append(ch[3]);
			stringBuilder.append(ch[4]);
			log.info("Date Of String " + stringBuilder.toString());
		}catch (Exception e) {
			log.error("Exception in getDate method",e);
		}
		return stringBuilder.toString();
	}
	
	/**
	 * 
	 * @param date
	 * @return
	 * To Covert the  date into mm/dd/yyyy format for response object
	 */
	public String getProperDate(String date) {
		String[] ss = date.split("/");
		StringBuilder st = new StringBuilder();  // To Covert the  date into mm/dd/yyyy format for response object
		st.append(ss[0]);
		st.append("/");
		st.append(ss[1]);
		st.append("/");  
		st.append(ss[2]);
		return st.toString();
	}
	
	/**
	 * 
	 * @param memberId
	 * @param session
	 * @return
	 * To invoke the find member service 
	 * Response will be returned in the form of list object
	 */
	public List findMember(String healthPlanId, String memberId, PortletSession session)throws CustomException{
		log.info("Inside findMember method..");
		List<MemberDetailsFo> details = new ArrayList<MemberDetailsFo>();
		List memberList = new ArrayList();
		GetMemberDataResponse getMemberDataResponse  = null;

		try {
			PriorAuthImplServiceStub priorAuthImplServiceStub = new PriorAuthImplServiceStub();
			GetMemberData getMemberData = new GetMemberData();
			MemberDataRequest memberDataRequest = new MemberDataRequest();
			SearchCriteria  searchCriteria = new SearchCriteria();
			searchCriteria.setReasonForInquiry(REASON_FOR_INQUIRY);//C
			searchCriteria.setRequestDate(null);
			searchCriteria.setRpn(" ");
			searchCriteria.setProductCode(" ");
			searchCriteria.setPlanCode(" ");
			searchCriteria.setHealthPlanId(healthPlanId);
			log.info("healthPlanId : " + healthPlanId);
			searchCriteria.setHealthPlanGroupId(" ");
			searchCriteria.setIdCardNumber(memberId);
			log.info("memberId : " + memberId);
			searchCriteria.setHealthPlanIdType(HEALTH_PLAN_ID_TYPE);//PI 
            searchCriteria.setSubscriberNumber(" ");
			memberDataRequest.setSearchCriteria(searchCriteria);
			MoreDataOptions moreDataOptions = new MoreDataOptions();
			moreDataOptions.setRetrieveMemberList(true);
			moreDataOptions.setRetrieveCoverageData(false);
			memberDataRequest.setMoreDataOptions(moreDataOptions);
			getMemberData.setGetMemberData(memberDataRequest);
			
			//TODO: Remove mock service code and condition. Don't remove else condition code
			if(GetterUtil.getBoolean(com.liferay.portal.kernel.util.PropsUtil.get(ClaimsSearchConstants.ENABLED_MOCK_SERVICE))){
				 //Success Mock Code
				getMemberDataResponse = priorAuthImplServiceStub.getSuccessMockMemberData(getMemberData);  //To send the request for ID Card Number Search.
			}else{
				getMemberDataResponse = priorAuthImplServiceStub.getMemberData(getMemberData);  //To send the request for ID Card Number Search.
			}	
				
			boolean searchMemberFlag = true;
			String[] responseCodes = getMemberDataResponse.getGetMemberDataResponse().getResponseCode();
			for (String responseCode:responseCodes) {
				
				if (responseCode.matches("000|952|953|954|955|956|957|958|959|960|961|962|963|964|990|991|992|993|994|995|996")) {
					if (searchMemberFlag) {
						log.info("responseCode is matching..");
						// Set the flag to indicate the member search has been done
						searchMemberFlag = false;
						Member[] member = getMemberDataResponse.getGetMemberDataResponse().getMemberList().getMember();
						Map<Integer,MemberDetailsFo> mapList = new ConcurrentHashMap<Integer, MemberDetailsFo>();
						log.info("member length.."+member.length);
						for (int i = 0; i <member.length; i++) {
							MemberDetailsFo members = new MemberDetailsFo();
							
							// to get the value for selected find claims
							members.setSubscriberId(getMemberDataResponse.getGetMemberDataResponse().getSubscriberNumber());
							members.setHealthPlan(member[i].getHealthPlanGroupId());
							members.setMemberFirstName(member[i].getFirstName());
							members.setMemberDob(convertStringToDate(member[i].getDateOfBirth()));
							members.setMemberLastName(member[i].getLastName());
							members.setGender(member[i].getGenderCode());
							members.setPatientId(member[i].getPatientId());
							members.setMemberId(memberId);
							members.setMpi(member[i].getUniqueMemberId());
							members.setGroupId(member[i].getHealthPlanGroupId());
							members.setMemberRelationship(member[i].getRelationshipCodeDescription());
							details.add(members);
							Map <String, Object> memSearch = new HashMap<String, Object>();
							
							// To display in view page 
							memSearch.put(MEMBER_SERVICE_ID,memberId);
							memSearch.put(GROUP_ID,member[i].getHealthPlanGroupId());
							memSearch.put(MEMBER_FIRST_NAME,member[i].getFirstName());
							memSearch.put(MEMBER_LAST_NAME,member[i].getLastName());
							memSearch.put(MEMBER_DATE_OF_BIRTH,convertStringToDate(member[i].getDateOfBirth()));
							memSearch.put(MEMBER_GENDER,member[i].getGenderCode());
							memSearch.put(MEMBER_FIND_CLAIMS,String.valueOf(i));
							memSearch.put(GROUP_ID, members.getGroupId());
							memSearch.put(MEMBER_FIRST_NAME, members.getMemberFirstName()); 
							memSearch.put(MEMBER_LAST_NAME, members.getMemberLastName());
							memSearch.put(MEMBER_DATE_OF_BIRTH, members.getMemberDob());
							memSearch.put(MEMBER_GENDER,members.getGender());
							memSearch.put(MEMBER_FIND_CLAIMS,String.valueOf(i)); 
							memberList.add(memSearch);
							
							// To fetch details for index based selection
							mapList.put(i,members);
						}
						session.setAttribute(MEMBER_DETAILS_LIST_SESSION, mapList); // to set the member details in session
				}
			} else {
				log.info("responseCode is not matching.."+responseCodes);
				// Set the flag to indicate the a error that stops the member search
				searchMemberFlag = false;

				String errorMessage = getResponseCodeMessages(responseCodes);
				throw new CustomException(errorMessage);
		    }
	    }	
		} catch (CustomException customException) { 
		    Map <String, Object> memSearch = new HashMap<String, Object>();
			memSearch.put(ERROR,customException.getMessage()); // to display the error in view page
			memberList.add(memSearch);
			log.error("CustomException in findMember method", customException);
	
		} catch (IOException ioe) { 
			log.error("IOException in findMember method", ioe);
			
			if (("Read timed out").equals(ioe.getMessage())) {
			   
				Map <String, Object> memSearch = new HashMap<String, Object>();
				memSearch.put(ERROR,ERROR_WHILE_PROCESSING); // to display the error in view page
				memberList.add(memSearch);
				log.error("Read timed out in findMember method", ioe);
			} else {
				Map <String, Object> memSearch = new HashMap<String, Object>();
				memSearch.put(ERROR,MEMSERVICE_ERROR); // to display the error in view page
				memberList.add(memSearch);
				log.error("MEMSERVICE_ERROR in findMember method", ioe);
			}	
		}
		return memberList;
	}

	/**
	  * 
	  * @param claimSearchFO
	  * @param session
	  * @param memberKey
	  * @return
	  * To invoke the claim service and setting the npi value in the request object
	  */
	public List getClaimDetails(ClaimSearchFO claimSearchFO, PortletSession session, String memberKey, String taxId, boolean adminFlag, String browser) {
		log.info("Inside getClaimDetails..");
		List list = new ArrayList();
		boolean errorFlag = false;
		
		// Any user not in the AvalonAdmin group must have a federal tax identifier
		if (!adminFlag) {
			if (taxId.equals("")) {
				errorFlag = true;
				Map <String, Object> search = new HashMap<String, Object>();
				search.put(ERROR, NO_TAX_ID_ERROR);
				list.add(search);
			}
		}
		
		if (!errorFlag) {

			MemberDetailsFo memberDetailsFo = null;
	
			// memberKey will be null if this search is without a member
			if ((memberKey != null) && !memberKey.equalsIgnoreCase("null")) {
			    Map<Integer, MemberDetailsFo> mapList = (ConcurrentHashMap<Integer, MemberDetailsFo>)session.getAttribute(MEMBER_DETAILS_LIST_SESSION); // to get the member details  from session
		
			    if ((mapList != null) && (!(mapList.isEmpty()))) {
			    	memberDetailsFo = mapList.get(Integer.parseInt(memberKey));
			    }
			    
			    // Set the claim ID from the member claim ID
			    claimSearchFO.setClaimID(claimSearchFO.getMemberClaimID());
			} else {
				memberKey = null;
			}
			
			ClaimStatusInboundReq claimStatusInboundReq = new ClaimStatusInboundReq(); 
			LBMClaimSearchServiceProxy lBMClaimSearchServiceProxy = new LBMClaimSearchServiceProxy();
			LBMClaimSearchService lBMClaimSearchService = lBMClaimSearchServiceProxy.getLBMClaimSearchService();
			FindClaims findClaims = new FindClaims();
			ClaimSearchCriteria claimSearchCriteriaInbound = new ClaimSearchCriteria();
			if (claimSearchFO.getClaimID() == null || claimSearchFO.getClaimID() == "") { 
				
				// Search by from date and to date
	    	    log.info("Service From Date : " + claimSearchFO.getDateofServiceFrom());
	    	    log.info("Service To Date : " + claimSearchFO.getDateofServiceTo());
	
	    	    claimSearchCriteriaInbound.setServiceDateFrom(ClaimSearchUtil.getClaimSearchUtil().getClaimSearchDate(claimSearchFO.getDateofServiceFrom(), browser));
	     		claimSearchCriteriaInbound.setServiceDateTo(ClaimSearchUtil.getClaimSearchUtil().getClaimSearchDate(claimSearchFO.getDateofServiceTo(), browser));
	     	} else {  
	     		
	     		// Search by Claim Id
	    	    log.info("ClaimId : " + claimSearchFO.getClaimID());
	     		
	     		claimSearchCriteriaInbound.setSearchClaimNumber(claimSearchFO.getClaimID());
	     		claimSearchCriteriaInbound.setServiceDateFrom(null);
	     		claimSearchCriteriaInbound.setServiceDateTo(null);
	     	}
			
			String healthPlanId = claimSearchFO.getHealthPlanID();
			log.info("claim healthPlanId : " + healthPlanId);
	
			// Set the request type based on the member key
			if (memberKey == null) {
				claimStatusInboundReq.setRequestType("detail");
			} else {
				claimStatusInboundReq.setRequestType("find");
			}
			Code carrierCode = new Code();
			carrierCode.setCode(healthPlanId);
			carrierCode.setDescription("");
			claimSearchCriteriaInbound.setCarrierCode(carrierCode);
			HealthClaimSearchCriteria healthClaimSearchCriteria = new HealthClaimSearchCriteria();
			healthClaimSearchCriteria.setBlueExchangeDaysToWait(BLUE_EXCHANGE_DAYS_TO_WAIT); //0
			claimSearchCriteriaInbound.setHealthClaimSearchCriteria(healthClaimSearchCriteria);
			ClaimSearchFilter claimSearchFilterInbound = new ClaimSearchFilter();
			claimSearchFilterInbound.setIdentifySensitiveClaims(TRUE);
			claimSearchFilterInbound.setIncludePrivacyProtectedClaims(TRUE);
			claimStatusInboundReq.setClaimSearchFilterInbound(claimSearchFilterInbound);
			if (memberKey != null) {
				Patient[] patient = new Patient[1];
				List<Patient> patientList = new ArrayList<Patient>();
				for (int i = 0; i< patient.length; i++) {
					Patient patients = new Patient();
					SubscriberId subscriberId = new SubscriberId();
					CoverageInformation coverageInformation = new CoverageInformation();
					Code code = new Code();
					patients.setSubscriberId(subscriberId);
					patients.setCoverageInformation(coverageInformation);
					patients.setRelationshipToSubscriber(code);
					log.info("Subscriber Id: " + memberDetailsFo.getSubscriberId());
					patients.getSubscriberId().setMemberNumber(memberDetailsFo.getSubscriberId());
					patients.getRelationshipToSubscriber().setCode("");
					patients.getRelationshipToSubscriber().setDescription("");
					patients.getCoverageInformation().setDependentId(memberDetailsFo.getPatientId());
					patients.getCoverageInformation().setPrivacyProtectionEnabled(FALSE);
					patients.setOutOfAreaPatient(FALSE);
					patientList.add(patients);
				}
				claimStatusInboundReq.setPatients((Patient[]) patientList.toArray(new Patient[patientList.size()]));
			}
			ProviderInformation  providerInfo = new ProviderInformation();
			ProviderIdentifier providerIdentifier = new ProviderIdentifier();
		    log.info("taxIdentifier : " + taxId);
		    providerIdentifier.setFederalTaxIdNumber(taxId);
		    providerInfo.setProviderIdentifier(providerIdentifier);
			claimStatusInboundReq.setClaimSearchCriteriaInbound(claimSearchCriteriaInbound);
			claimStatusInboundReq.setProviderInfo(providerInfo);
			findClaims.setClaimStatusInboundReq(claimStatusInboundReq);
			try {
				if (claimStatusInboundReq.getRequestType().equals("find")) {
					ClaimSummariesPatient[] claimSummariesPatient = null;
					
					//TODO: Remove mock service code and condition. Don't remove else condition code
					 if(GetterUtil.getBoolean(com.liferay.portal.kernel.util.PropsUtil.get(ClaimsSearchConstants.ENABLED_MOCK_SERVICE))){
						// mock service code
						 claimSummariesPatient = lBMClaimSearchService.findClaimsSuccessMock(findClaims);  
					 } else{
						 log.info("Invoking the findclaims..");
						//Invoking the findclaims by setting all the parameters in the request object  
						 claimSummariesPatient = lBMClaimSearchService.findClaims(findClaims);  
					 }
					 log.info("claimSummariesPatient.."+claimSummariesPatient);
					if ((claimSummariesPatient != null) && (claimSummariesPatient.length > 0)) {
						
						List<ClaimSummariesPatient> patientlist = Arrays.asList(claimSummariesPatient);
		
						for (int i = 0; i < patientlist.size(); i++) {
						
							ClaimSummariesPatient claimSummariesPatients = patientlist.get(i);
							ClaimsSummaryItem[] claimsSummaryItems = claimSummariesPatients.getClaimSummary().getClaimsSummaryItems();
							
							String memberId = memberDetailsFo.getMemberId();
							
							String checkNumber = "";
	
							if ((claimsSummaryItems != null) &&  (claimsSummaryItems.length > 0)) {
								for (int j = 0 ; j < claimsSummaryItems.length; j++) {
									Map <String, Object> search= new HashMap<String, Object>();
									 
									String claimStatus = claimsSummaryItems[j].getStatusType().getValue();
									log.info(" claimStatus : " + claimStatus);
	
									String claimNumber = claimsSummaryItems[j].getClaimNumber();
									
									log.info(" claimNumber : " + claimNumber);
	
									String providerId = claimsSummaryItems[j].getProviderID();
									log.info(" providerId : " + providerId);
									
									String billingProviderName = claimsSummaryItems[j].getBillingProviderName();
									log.info(" billingProviderName : " + billingProviderName);
		
									String dateOfServiceFrom = getCalenderDate(claimsSummaryItems[j].getClaimDateOfService().getFromDate());
									log.info(" dateOfServiceFrom : " + dateOfServiceFrom);
									
									String dateOfServiceTo = getCalenderDate(claimsSummaryItems[j].getClaimDateOfService().getToDate());
									log.info(" dateOfServiceTo : " + dateOfServiceTo);
	
									BigDecimal totalCharge = claimsSummaryItems[j].getTotalClaimChargeAmount();
									log.info(" totalCharge : " + totalCharge);
									
									String checkIssueDate = "";
									java.util.Calendar checkIssueDt = claimsSummaryItems[j].getCheckIssueDate();
									if (checkIssueDt != null) {
										checkIssueDate = getCalenderDate(checkIssueDt);
									}
									log.info(" checkIssueDate : " + checkIssueDate);
	
									// To check the claim status when check issue date blank or value
									if ((claimStatus.equals(HARD_PENDED)) || (claimStatus.equals(PENDING))) {
										log.info("ClaimStatus : In Process ");
										search.put(CLAIM_STATUS, PENDING_STATUS);				// to display in view page
									// To check the claim status when check issue date has value// to display in view page
									} else if (((claimStatus.equals(DENIED)) || (claimStatus.equals(PROCESSED)) || (claimStatus.equals(REJECTED))) && (checkIssueDate != null)) { 
										log.info("ClaimStatus : Settled ");
										search.put(CLAIM_STATUS, DENIED_WITH_CHECKDATE);		// to display in view page
									// To check the claim status when check issue date is blank
									} else if (((claimStatus.equals(DENIED)) || (claimStatus.equals(PROCESSED)) || (claimStatus.equals(REJECTED))) && ((checkIssueDate == null) || (checkIssueDate.isEmpty()))) { 
										log.info("ClaimStatus : Completed ");
										search.put(CLAIM_STATUS,DENIED_WITHOUT_CHECKDATE);		// to display in view page
									}
		
									search.put(CLAIM_ID, claimNumber);							// to display in view page
									search.put(MEMBER_ID, memberId);							// to display in view page
									if (providerId != null) {
										search.put(PROVIDER_ID, providerId); 					// to display in view page
									} else{
										search.put(PROVIDER_ID, billingProviderName);			// to display in view page 
									}
									search.put(DATE_OF_SERVICE_FROM, dateOfServiceFrom);		// to display in view page
									search.put(DATE_OF_SERVICE_TO, dateOfServiceFrom);			// to display in view page
									search.put(TOTAL_CHARGE_GREATER_THAN, "$ " + totalCharge);	// to display in view page
									search.put(CHECK_ISSUE_DATE, checkIssueDate);				// to display in view page
									search.put(CHECK_NUMBER, checkNumber);						// to display in view page
									list.add(search);
								}
							} else {
								
								log.info("no proper data in patient list..");
								Map <String, Object> search = new HashMap<String, Object>();
								search.put(ERROR, CLAIMS_ERROR); // To handle the error when their is no proper data in patient list
								list.add(search);
							}
						}
					} else {
						log.info("no proper data in claimsSummaryItems list..");
						Map <String, Object> search = new HashMap<String, Object>();
						search.put(ERROR, CLAIMS_ERROR); // To handle the error when their is no proper data in claimsSummaryItems
						list.add(search);
					}
				} else {
					GetClaimDetailsResponse getClaimDetailsResponse = null;
					
					//TODO: Remove mock service code and condition. Don't remove else condition code
					if(GetterUtil.getBoolean(com.liferay.portal.kernel.util.PropsUtil.get(ClaimsSearchConstants.ENABLED_MOCK_SERVICE))){
						 //Success Mock Code
						 getClaimDetailsResponse = lBMClaimSearchService.getSuccessMockClaimDetails(findClaims);
					 }else{
						 // Invoking the getClaimDetails by setting all the parameters in the request object  
						 getClaimDetailsResponse = lBMClaimSearchService.getClaimDetails(findClaims);
					 }
					log.info("getClaimDetailsResponse.."+getClaimDetailsResponse);
					if (getClaimDetailsResponse != null) {
						Map <String, Object> search= new HashMap<String, Object>();
	
						String statusCode = getClaimDetailsResponse.getClaimDetail().getStatusType().getValue();
						log.info(" statusCode : " + statusCode + "!");
	
						String claimId = getClaimDetailsResponse.getClaimDetail().getClaimNumber();
						log.info(" claimId : " + claimId + "!");
	
						String idCardNumber = getClaimDetailsResponse.getClaimDetail().getPatient().getSubscriberId().getIdCardNumber();
						log.info(" idCardNumber : " + idCardNumber + "!");
	
						String providerId = getClaimDetailsResponse.getClaimDetail().getBaseProvider().getProviderId().getNationalProviderIdentifier();
						log.info(" providerId : " + providerId + "!");
						
						String providerName = getClaimDetailsResponse.getClaimDetail().getBaseProvider().getProviderName().getFirstName() +
						                      getClaimDetailsResponse.getClaimDetail().getBaseProvider().getProviderName().getLastName();
						log.info(" providerName : " + providerName + "!");
						
						String dateOfServiceFrom = getCalenderDate(getClaimDetailsResponse.getClaimDetail().getClaimDateOfService().getFromDate());
						log.info(" dateOfServiceFrom : " + dateOfServiceFrom + "!");
						
						String dateOfServiceTo = getCalenderDate(getClaimDetailsResponse.getClaimDetail().getClaimDateOfService().getToDate());
						log.info(" dateOfServiceTo : " + dateOfServiceTo + "!");
						
						BigDecimal totalCharge = getClaimDetailsResponse.getClaimDetail().getTotalClaimChargeAmount();
						log.info(" totalCharge : " + totalCharge + "!");
						
						String checkIssueDate = "";
						java.util.Calendar checkIssueDt = getClaimDetailsResponse.getClaimDetail().getCheckIssueDate();
						if (checkIssueDt != null) {
							checkIssueDate = getCalenderDate(checkIssueDt);
						}
						log.info(" checkIssueDate : " + checkIssueDate + "!");
						
						String checkNumber = getClaimDetailsResponse.getClaimDetail().getCheckNumber();
						log.info(" checkNumber : " + checkNumber + "!");
						
						// To check the claim status when check issue date blank or value
						if ((statusCode.equals(HARD_PENDED)) || (statusCode.equals(PENDING))) {
							log.info("ClaimStatus : In Process ");
							search.put(CLAIM_STATUS, PENDING_STATUS);				// to display in view page
						// To check the claim status when check issue date has value// to display in view page
						} else if (((statusCode.equals(DENIED)) || (statusCode.equals(PROCESSED)) || (statusCode.equals(REJECTED))) && (checkIssueDate != null)) { 
							log.info("ClaimStatus : Settled ");
							search.put(CLAIM_STATUS, DENIED_WITH_CHECKDATE);		// to display in view page
						// To check the claim status when check issue date is blank
						} else if (((statusCode.equals(DENIED)) || (statusCode.equals(PROCESSED)) || (statusCode.equals(REJECTED))) && ((checkIssueDate == null) || (checkIssueDate.isEmpty()))) { 
							log.info("ClaimStatus : Completed ");
							search.put(CLAIM_STATUS,DENIED_WITHOUT_CHECKDATE);		// to display in view page
						}
	
						search.put(CLAIM_ID, claimId);								// to display in view page
						search.put(MEMBER_ID, idCardNumber);						// to display in view page
						if (providerId != null) {
							search.put(PROVIDER_ID, providerId);					// to display in view page
						} else{
							search.put(PROVIDER_ID, providerName);					// to display in view page 
						}
						search.put(DATE_OF_SERVICE_FROM, dateOfServiceFrom);		// to display in view page
						search.put(DATE_OF_SERVICE_TO, dateOfServiceTo);     		// to display in view page
						search.put(TOTAL_CHARGE_GREATER_THAN,"$ " + totalCharge);	// to display in view page
						search.put(CHECK_ISSUE_DATE, checkIssueDate);				// to display in view page
						search.put(CHECK_NUMBER, checkNumber);						// to display in view page
						list.add(search);
					}
				}
			} catch (IOException e  ) {
				Map <String, Object> search = new HashMap<String, Object>();
				String errorMsg = e.getMessage();
				
				if (e instanceof org.apache.axis2.AxisFault) {
					org.apache.axis2.AxisFault exception = (org.apache.axis2.AxisFault)e ;
					errorMsg = exception.getMessage();
					log.info("Exception --> " + exception);
				    log.info("Exception Message --> " + errorMsg);
				    log.info("IOException read time out");
					log.error("IOException in getClaimDetails method", e);
					if (errorMsg.equals("Read timed out")) {
						
					    /*** When socket time out exception occured ***/
						log.info("IOException Timed Out");
						search.put(ERROR, ERROR_WHILE_PROCESSING); 
						list.add(search);
					}
			   } else {
				    log.info("Exception --> " + e);
				    log.info("Exception Message --> " + errorMsg);
				    log.info("IOException Claims Error");   
				    log.info(" ----> " + this.getClass().getName());
					log.error("IOException in getClaimDetails method", e);
	
					if (errorMsg.contains("CustomFaultException")) {
	
						// Got a custom error message from ESB
						String searchStr = "# : ";
						
						int startIdx = errorMsg.indexOf(searchStr);
						errorMsg = errorMsg.substring(startIdx + searchStr.length());
						
						// Add text to the errors to meet the requirements
						if (errorMsg.contains("NOT AUTHORISED")) {
							errorMsg = "You are not authorized to view this claim. Please enter another claim number to continue.";
						} else if (errorMsg.contains("CLAIM NOT FOUND") || errorMsg.contains("NO CLAIM DATA ON FILE")) {
							errorMsg = "No claims were found that match your criteria. Please update the search and try again.";
						}
					} else {
						/*** To handle the exception when the esb servers are down ***/
						errorMsg = CLAIMS_ERROR;
					}
				    search.put(ERROR, errorMsg);
					list.add(search);
				}
			} catch (Exception exception) {
				String errorMsg = exception.getMessage();
				log.info("Exception --> " + exception);
				log.info("Exception Message --> " + errorMsg);
				log.info("Exception Block");   
				log.warn(" ----> " + this.getClass().getName());
				log.error("Exception in getClaimDetails method",exception);
				Map <String, Object> search = new HashMap<String, Object>();
				search.put(ERROR, CLAIMS_ERROR);
				list.add(search);
			}
		}

		return list;
	}
	
	
    /**
     * 
     * @param indate
     * @return
     * To get the date in MM/dd/yyyy format for  findMember response object
    */
	public String convertStringToDate(Date indate) {  
		String dateString = null;   // To get the date in MM/dd/yyyy format for  findMember response object
  
		SimpleDateFormat formatDate = new SimpleDateFormat("MM/dd/yyyy");
   
		try{
			dateString = formatDate.format( indate );
		}catch (Exception ex) {
			log.error("Exception in convertStringToDate method",ex);
				
		}
		return dateString;
	}
	

	/**
	 * 
	 * @param calender
	 * @return
	 * To get the date in MM/dd/yyyy format for  getClaimDetails response object
	 */
	public String getCalenderDate(Calendar calender) {     // To get the date in MM/dd/yyyy format for getClaimDetails response object
		Date date = calender.getTime();             
		SimpleDateFormat format1 = new SimpleDateFormat("MM/dd/yyyy");          
		String inActiveDate = null;
		try {
			inActiveDate = format1.format(date);
		} 	catch (Exception e) {
			log.error("Exception in getCalenderDate method",e);
		}
		return inActiveDate;
	}
		

	/**
	 * 
	 * @param calender
	 * @return
	 * To get the date in MM/dd/yyyy format for getClaimDetails response object
	 */
	public String getXMLGregorianCalenderDate(XMLGregorianCalendar xmlGregorianCalendar) {
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
		String dateString = null;
		
		Calendar calendar = xmlGregorianCalendar.toGregorianCalendar();
		sdf.setTimeZone(calendar.getTimeZone());
		dateString = sdf.format(calendar.getTime());
	
		return dateString;
	}
	
	
    /**
     * 
     * @param responseCodes
     * @param idCardNumber
     * @return To display error message based on the response code from the the cds response
     */
    public String getResponseCodeMessages(String responseCodes[]) {

		StringBuilder errorMessage = new StringBuilder();
		
		 for(String responseCode:responseCodes) {
		    log.info("responseCode ---> " + responseCode);
			StringBuilder propertyValue = new StringBuilder();
		   
		    if (responseCode.matches("950|951")) {

				propertyValue.append("authSearch_");
				propertyValue.append(responseCode);
				log.info("propertyValue" + propertyValue);
				errorMessage.append(properties.getProperty(propertyValue.toString()));
				errorMessage.append("\n");
				propertyValue = null;
			} else {
				log.info(responseCode + " is not a Valid Response Code");
		    }
		 }
		 return errorMessage.toString();
    }
}
