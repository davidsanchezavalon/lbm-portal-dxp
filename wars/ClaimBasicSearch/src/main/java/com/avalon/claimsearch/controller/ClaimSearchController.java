package com.avalon.claimsearch.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletRequest;
import javax.portlet.PortletSession;
import javax.portlet.RenderRequest;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;

/**
 * Description
 *		This file contain the Claims Search portlet.
 *
 * CHANGE History
 * 		Version 1.0
 *     		Initial version.
 *		Version 1.1
 *			Do not use tax id for the AvalonEmployeeClaim group.
 *
 */

import com.avalon.claimsearch.businessdelegate.ClaimsSearchBusinessDelegateVo;
import com.avalon.claimsearch.fo.ClaimSearchFO;
import com.avalon.claimsearch.fo.ClaimsSearchConstants;
import com.google.gson.Gson;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.model.UserGroup;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PortalUtil;

@Controller("claimSearchController")
@RequestMapping(value="VIEW")
public class ClaimSearchController implements ClaimsSearchConstants   {
	
	private static final Log log = LogFactoryUtil.getLog(ClaimSearchController.class.getName());

	/**
	 * 
	 * @param request
	 * @param map Setting the commandBean for spring form
	 * @return 
	 * whenever the view page has been clicked the render method will be executed 
	 * In this method we are removing the session variable TAX_IDENTIFIER_SESSION which has been set in viewHomePage method.
	 * It redirects to the view page.
	 */
        @RenderMapping
        public String viewHomePage(RenderRequest request,
        		                   Map<String, Object> map,
        		                   @ModelAttribute(value = "claimSearchFO") ClaimSearchFO claimSearchFO) {
        	log.info("Render method for Claim Status");
        	try {    
       		
        		PortletSession session = request.getPortletSession();	// Creation Of Session
        		
        		// Removing the tax identifier value
        		session.removeAttribute(TAX_IDENTIFIER_SESSION, PortletSession.APPLICATION_SCOPE);
 
				User user = PortalUtil.getUser(request);
				if (user != null) {

					// Get the tax identifier value in custom fields from portal (set the secure flag to false to always get the value)
					String taxIdentifier = (String)user.getExpandoBridge().getAttribute(OKTA_TAX_IDENTIFIER, false);
					if ((taxIdentifier == null) || ((taxIdentifier != null) && taxIdentifier.equals("null"))) {
						taxIdentifier = "";
					}
					session.setAttribute(TAX_IDENTIFIER_SESSION, taxIdentifier, PortletSession.APPLICATION_SCOPE);
				}
				log.info("taxIdentifier : " + (String)session.getAttribute(TAX_IDENTIFIER_SESSION, PortletSession.APPLICATION_SCOPE) + " for " + user.getFullName());
				map.put(CLAIMS_SEARCHFO, new ClaimSearchFO());

				// Set the service from date to display
				request.setAttribute("memberFromDate", claimSearchFO.getDateofServiceFrom());

				// Set the service to date to display
				request.setAttribute("memberToDate", claimSearchFO.getDateofServiceTo());

				// Set the claim ID to display
				request.setAttribute("memberClaimId", claimSearchFO.getMemberClaimID());
        	} catch (PortalException e) {
        		log.error("Exception in viewHomePage method", e);
        	} catch (SystemException e) {
    			log.error("SystemException in viewHomePage method", e);
    		} catch(NullPointerException nullPointer) {

    			// To handle the exception if the custom field npiIndividualNumber is not available.
    			log.error("NullPointerException in viewHomePage method", nullPointer);
    		}

        	return VIEW_DISPLAY;
        }

        /**
         * 
         * @param request
         * @param response
         * @param claimSearchFO
         * @throws IOException
         * When the user clicked on search button memberInfoSearchform method will be executed 
         * It will invoke findMember method to fetch the member details and results will be displayed.
		 * This method is for a resource URL.
		 * 
         */
        @ResourceMapping(value="memberInfoSearchform")
        public void memberInfoSearchform(ResourceRequest request,
        		                         ResourceResponse response,
        		                         @ModelAttribute("claimSearchFO") ClaimSearchFO claimSearchFO) throws IOException {

        	PortletSession portletSession = request.getPortletSession();
        	log.info("memberInfoSearchform Method for Claim Status");
			String memberInfo = null;
			Gson gson = new Gson();  
			List list;
			try {
				String healthPlanID = ParamUtil.getString(request, "memberInfoHealthPlan");
				claimSearchFO.setHealthPlanID(healthPlanID);

				// Set the health plan ID
				portletSession.setAttribute("healthPlanID", (Object) healthPlanID, PortletSession.APPLICATION_SCOPE);
				log.info("member search healthPlanID : " + healthPlanID);

				list = ClaimsSearchBusinessDelegateVo.getClaimsSearchBusinessDelegateVo().findMember(claimSearchFO.getHealthPlanID(), claimSearchFO.getMemberID(), portletSession); // To Search With the ID Card Number
				memberInfo   = gson.toJson(list); // Coverting the list into Json Response 
				response.getWriter().write(memberInfo.toString()); // Setting the coverted json response into response object
				log.info("End memberInfoSearchform method for Claim Status");
			} catch (Exception e) {
				log.info(" ----> " + this.getClass().getName());
				log.error("Exception in memberInfoSearchform method", e);
			} 
        }  
        
        /**
         * 
         * @param request
         * @param response
         * @param claimSearchFO
         * When the user click on hyperlink it will navigate to basic search page
         * Claim details will be displayed based on claimid and service from date and to date.
		 * This method is for a resource URL.
		 * 
         */

        @ResourceMapping(value="claimSearchform")
        public void claimSearchform(ResourceRequest request,
        		                    ResourceResponse response,
        		                    @ModelAttribute("claimSearchFO") ClaimSearchFO claimSearchFO) {

        	log.info("claimSearchform Method for ClaimSearch");

			PortletSession portletSession = request.getPortletSession();
			String claimInfo = null;
			List list;
		    Gson gson = new Gson();  
	    	try { 
			 	PortletSession session = request.getPortletSession();
	
			    HttpServletRequest httpRequest = PortalUtil.getHttpServletRequest(request);
			    String browserStr = getBrowser(httpRequest);
	    		
				if (claimSearchFO.getClaimMemberKey() != "") {
	
					// Save the member selected
					session.setAttribute(CLAIM_MEMBER_KEY, claimSearchFO.getClaimMemberKey(), PortletSession.APPLICATION_SCOPE);
				} else {
					claimSearchFO.setClaimMemberKey((String)session.getAttribute(CLAIM_MEMBER_KEY, PortletSession.APPLICATION_SCOPE));
				}
				request.setAttribute("claimMemberKeyVal", claimSearchFO.getClaimMemberKey());
				log.info("Claim Key --> " + claimSearchFO.getClaimMemberKey());

       		    // Set the health plan ID
       		    String healthPlanID = "";
       			if ((claimSearchFO.getClaimMemberKey() != null) && !claimSearchFO.getClaimMemberKey().equalsIgnoreCase("null")) {
           		    healthPlanID = (String) portletSession.getAttribute("healthPlanID", PortletSession.APPLICATION_SCOPE);
       			} else {
       				healthPlanID = ParamUtil.getString(request, "memberInfoHealthPlan");
       			}
       		    claimSearchFO.setHealthPlanID(healthPlanID);
       		    log.info("claim search healthPlanID : " + healthPlanID);
	 			
				// Set the tax id if the user is not in the AvalonAdmin or AvalonEmployeeClaim group
				boolean adminFlag = true;
				String taxIdentifier = "";
				if (!isValidGroup(request, "AvalonAdmin") && !isValidGroup(request, "AvalonEmployeeClaim")) {
					adminFlag = false;
					taxIdentifier = (String)session.getAttribute(TAX_IDENTIFIER_SESSION, PortletSession.APPLICATION_SCOPE);
				}
				log.info("claim search taxIdentifier : " + taxIdentifier);

				// Search by From-Date and To-Date or Claim id to fetch the claim details
	     		list = ClaimsSearchBusinessDelegateVo.getClaimsSearchBusinessDelegateVo().getClaimDetails(claimSearchFO, session, claimSearchFO.getClaimMemberKey(), taxIdentifier, adminFlag, browserStr);
	 		     
	 		    // Coverting the list into Json Response 
	 		    claimInfo = gson.toJson(list); 
	 		     
	 		    // Setting the coverted json response into response object
	 		    response.getWriter().write(claimInfo.toString()); 
       		
        		// Removing member details values  (Set in findMembers)
				session.removeAttribute(MEMBER_DETAILS_LIST_SESSION, PortletSession.APPLICATION_SCOPE);	
				
				// Removing member ID values
				session.removeAttribute(MEMBER_SERVICE_ID, PortletSession.APPLICATION_SCOPE);
				log.info("End claimSearchform method for Claim Status");
	    	} catch(IOException e) {
	    		log.info(" ----> " + this.getClass().getName());
	    		log.error("IOException in claimSearchform method", e);	   	
	    	} catch(Exception e) {
	    		log.info(" ----> " + this.getClass().getName());
	    		log.error("Exception in claimSearchform method", e);	  	
	    	}
        }
        
        /**
         * 
         * @param request
         * @param response
         * @param claimId
         * When the user click on hyperlink it will navigate to basic search page
         * Claim details will be returned based on the claim ID.
		 * This method is for a resource URL.
		 * 
         */

        @ResourceMapping(value="memberClaimSearchform")
        public void memberClaimSearchform(ResourceRequest request,
        		                          ResourceResponse response,
        		                          String claimId) {

        	log.info("memberClaimSearchform Method for ClaimSearch");

			PortletSession portletSession = request.getPortletSession();
			String claimInfo = null;
			List list;
			Gson gson = new Gson();  
			try {
			 	PortletSession session = request.getPortletSession();
			
			    HttpServletRequest httpRequest = PortalUtil.getHttpServletRequest(request);
			    String browserStr = getBrowser(httpRequest);
				
			    ClaimSearchFO claimSearchFO = new ClaimSearchFO();
			    
			    // Set the claim ID
			    claimSearchFO.setClaimID(claimId);
			    log.info("member claim search ClaimId : " + claimId);

				// Set the member key to not use the member search
				claimSearchFO.setClaimMemberKey("null");
				
			    // Set the health plan ID
			    String healthPlanID = (String) portletSession.getAttribute("healthPlanID", PortletSession.APPLICATION_SCOPE);
			    claimSearchFO.setHealthPlanID(healthPlanID);
			    log.info("member claim search healthPlanID : " + healthPlanID);
					
				// Set the tax id if the user is not in the AvalonAdmin or AvalonEmployeeClaim group
				boolean adminFlag = true;
				String taxIdentifier = "";
				if (!isValidGroup(request, "AvalonAdmin") && !isValidGroup(request, "AvalonEmployeeClaim")) {
					adminFlag = false;
					taxIdentifier = (String)session.getAttribute(TAX_IDENTIFIER_SESSION, PortletSession.APPLICATION_SCOPE);
				}
				log.info("member claim search taxIdentifier : " + taxIdentifier);

				// Search by Claim id to fetch the claim details
				list = ClaimsSearchBusinessDelegateVo.getClaimsSearchBusinessDelegateVo().getClaimDetails(claimSearchFO, session, claimSearchFO.getClaimMemberKey(), taxIdentifier, adminFlag, browserStr);
			     
			    // Coverting the list into Json Response 
			    claimInfo = gson.toJson(list); 
			     
			    // Setting the coverted json response into response object
			    response.getWriter().write(claimInfo.toString()); 
				
				// Removing member details values  (Set in findMembers)
				session.removeAttribute(MEMBER_DETAILS_LIST_SESSION, PortletSession.APPLICATION_SCOPE);	
				
				// Removing member ID values
				session.removeAttribute(MEMBER_SERVICE_ID, PortletSession.APPLICATION_SCOPE);
				log.info("End claimSearchform method for Claim Status");
			} catch(IOException e) {
				log.info(" ----> " + this.getClass().getName());
				log.error("IOException in memberClaimSearchform method", e);	
			} catch(Exception e) {
				log.info(" ----> " + this.getClass().getName());
				log.error("Exception in memberClaimSearchform method", e);	
			}
			log.info("End memberClaimSearchform method for Claim Status");
        }
        
      /**
       *   when the user click on cancel button cancelPage method will be executed
       */
      @ActionMapping(params="cancelaction=cancelPage")   
      public void cancelPage(ActionRequest actionRequest,
                             ActionResponse actionResponse) {

    	  log.info("Processing Cancel Action for Claim Status");
  	    String userName = null;
  	    List usersList = null;
  		try {
  		    User user = PortalUtil.getUser((PortletRequest) actionRequest);
  		    if (user != null) {
  			    usersList = user.getUserGroups();
  			 }
  		} catch (PortalException e) {
  			log.error("Exception in getUser of cancelPage method", e);
  		} catch (SystemException e) {
  			log.error("SystemException in getUser of cancelPage method", e);
  		}
  	    ArrayList<String> groupNames = new ArrayList<String>();
  	    if (usersList != null) {
  	    	int length = usersList.size();
  		    for (int i = 0; i < length; ++i) {
  		    	
  		    	// Ignore the groups that do not have a home page
  				if (!((UserGroup)usersList.get(i)).getName().equalsIgnoreCase("CDSUsers") &&
  					!((UserGroup)usersList.get(i)).getName().equalsIgnoreCase("Everyone") &&
  					!((UserGroup)usersList.get(i)).getName().equalsIgnoreCase("ManagedUsers") &&
  					!((UserGroup)usersList.get(i)).getName().equalsIgnoreCase("Multi-Factor") &&
  					!((UserGroup)usersList.get(i)).getName().equalsIgnoreCase("PortalAdmin") &&
  					!((UserGroup)usersList.get(i)).getName().equalsIgnoreCase("UMGroup")) {
  					userName = ((UserGroup) usersList.get(i)).getName();
  					groupNames.add(userName);
  				    
  				}
  		    }
  	    }
  	    HttpServletRequest request = PortalUtil.getHttpServletRequest((PortletRequest) actionRequest);
  	    String path = PortalUtil.getCurrentCompleteURL((HttpServletRequest) request);
  	    StringBuilder output = new StringBuilder();
  	    int count = 0;
  	    char[] ch = path.toCharArray();
  	    for (int i2 = 0; i2 < ch.length; ++i2) {
  			if (ch[i2] == '/') {
  			    ++count;
  			}
  			if (count >= 3)
  			    continue;
  			output = output.append(ch[i2]);
  	    }

  	    String landingPage = null;
  	    if (usersList == null) {
  	     	// Set to the default landing page since the user does not belong to any groups
  			landingPage = "/web/guest/home";
  	    } else {
  	    	// Get the landing page based on the user's group
  			landingPage = com.liferay.portal.kernel.util.PropsUtil.get("default.landing.page.path[" + userName  + "]"); 
  			// If the user's group is not found use the defaule landing page
  			if (landingPage == null) {
  				
  				landingPage = com.liferay.portal.kernel.util.PropsUtil.get("default.landing.page.path");
  				if (landingPage == null) {
  					
  					// Set to the default landing page since it is not defined in the properties file
  					landingPage = "/web/guest/home";
  					
  				}
  			}
  		 }
  	    String pathRedirect = null;
  		pathRedirect = output + landingPage;
  		log.info(userName + " " + pathRedirect);
  		try {
  		    actionResponse.sendRedirect(pathRedirect);
  		} catch (IOException e) {
  			log.error("Exception in Redirection of cancelPage method", e);
  		}
      }
   
      

	/**
	  * 
	  * @param group
	  * @return if the logged in user is in a group.
	  */
	private Boolean isValidGroup(ResourceRequest request, String group) {
		Boolean rtnValue = Boolean.FALSE;
		List usersList = null;
		try {
			User user = PortalUtil.getUser((PortletRequest) request);
			if (user != null) {
				usersList = user.getUserGroups();
		 	}
		} catch (PortalException e) {
			log.error("Exception in isValidGroup method", e);
		} catch (SystemException e) {
			log.error("SystemException in isValidGroup method", e);
		}
		
		if (usersList != null) {
			int length = usersList.size();
			for (int i = 0; i < length; ++i) {
				    	
				    	// Test for the user in the group
				if (((UserGroup)usersList.get(i)).getName().equalsIgnoreCase(group)) {
					rtnValue = Boolean.TRUE;
					break;
				}
			}
		}
		return rtnValue;
	}

      
	private String getBrowser(HttpServletRequest httpRequest) {
		String userAgent = httpRequest.getHeader("User-Agent");
		String rtnValue = "unknown";
  		
		if (userAgent.contains("Opera") || userAgent.contains("OPR")) {
			rtnValue = "Opera";
		} else if (userAgent.contains("Chrome")) {
			rtnValue = "Chrome";
		} else if (userAgent.contains("Safari")) {
			rtnValue = "Safari";
		} else if (userAgent.contains("MSIE") || (userAgent.contains("Trident") && userAgent.contains("rv:"))) { 
			rtnValue = "IE";
		} else if (userAgent.contains("Firefox"))  {
			rtnValue = "Firefox";
		} else {
			rtnValue = "unknown";
		}
		
		return rtnValue;
	}
}
