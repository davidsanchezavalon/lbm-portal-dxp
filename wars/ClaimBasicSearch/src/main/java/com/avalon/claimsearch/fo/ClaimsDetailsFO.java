package com.avalon.claimsearch.fo;



import java.io.Serializable;


public class ClaimsDetailsFO implements Serializable {

	/**
	 * To store the ClaimsDetails fields
	 */
	private static final long serialVersionUID = -2581256424493159902L;
	
	
	private ClaimSearchFO claimSearchFO;
	
	private String subscriberId;
	private String patientFirstName;
	private String providerFirstName;
	private String providerTin;
	private String providerNpi;
	private String providerLastName;
	private String groupId;
	private String patientDob;
	private String groupName;
	private String patientLastName;
	private String patientId;
	private String claimStatus;
	private String totalCharges;
	private String submissionDate;
	private String serviceDate;
	private String insurancePaidAmount;
	private String remittanceAmount;
	private String patientLiability;
	private String checkNumber;
	private String checkIssueDate;
	private String claimPaidTo;
	private String radioselectdate;
	private String radioselect;
	private String radioselectclaim;
	
	
	
	public ClaimsDetailsFO(){
		
	}
	
	
	public String getRadioselect() {
		return radioselect;
	}
	public void setRadioselect(String radioselect) {
		this.radioselect = radioselect;
	}
	public String getRadioselectdate() {
		return radioselectdate;
	}
	public void setRadioselectdate(String radioselectdate) {
		this.radioselectdate = radioselectdate;
	}
	public String getRadioselectclaim() {
		return radioselectclaim;
	}
	public void setRadioselectclaim(String radioselectclaim) {
		this.radioselectclaim = radioselectclaim;
	}
	
	
	
	public ClaimSearchFO getClaimSearchFO() {
		return claimSearchFO;
	}
	public void setClaimSearchFO(ClaimSearchFO claimSearchFO) {
		this.claimSearchFO = claimSearchFO;
	}
	
	public String getClaimPaidTo() {
		return claimPaidTo;
	}
	public void setClaimPaidTo(String claimPaidTo) {
		this.claimPaidTo = claimPaidTo;
	}
	public String getSubscriberId() {
		return subscriberId;
	}
	public void setSubscriberId(String subscriberId) {
		this.subscriberId = subscriberId;
	}
	public String getPatientFirstName() {
		return patientFirstName;
	}
	public void setPatientFirstName(String patientFirstName) {
		this.patientFirstName = patientFirstName;
	}
	public String getProviderFirstName() {
		return providerFirstName;
	}
	public void setProviderFirstName(String providerFirstName) {
		this.providerFirstName = providerFirstName;
	}
	public String getProviderTin() {
		return providerTin;
	}
	public void setProviderTin(String providerTin) {
		this.providerTin = providerTin;
	}
	public String getProviderNpi() {
		return providerNpi;
	}
	public void setProviderNpi(String providerNpi) {
		this.providerNpi = providerNpi;
	}
	public String getProviderLastName() {
		return providerLastName;
	}
	public void setProviderLastName(String providerLastName) {
		this.providerLastName = providerLastName;
	}
	public String getGroupId() {
		return groupId;
	}
	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}
	public String getPatientDob() {
		return patientDob;
	}
	public void setPatientDob(String patientDob) {
		this.patientDob = patientDob;
	}
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	public String getPatientLastName() {
		return patientLastName;
	}
	public void setPatientLastName(String patientLastName) {
		this.patientLastName = patientLastName;
	}
	public String getPatientId() {
		return patientId;
	}
	public void setPatientId(String patientId) {
		this.patientId = patientId;
	}
	public String getClaimStatus() {
		return claimStatus;
	}
	public void setClaimStatus(String claimStatus) {
		this.claimStatus = claimStatus;
	}
	public String getTotalCharges() {
		return totalCharges;
	}
	public void setTotalCharges(String totalCharges) {
		this.totalCharges = totalCharges;
	}
	public String getSubmissionDate() {
		return submissionDate;
	}
	public void setSubmissionDate(String submissionDate) {
		this.submissionDate = submissionDate;
	}
	public String getServiceDate() {
		return serviceDate;
	}
	public void setServiceDate(String serviceDate) {
		this.serviceDate = serviceDate;
	}
	public String getInsurancePaidAmount() {
		return insurancePaidAmount;
	}
	public void setInsurancePaidAmount(String insurancePaidAmount) {
		this.insurancePaidAmount = insurancePaidAmount;
	}
	public String getRemittanceAmount() {
		return remittanceAmount;
	}
	public void setRemittanceAmount(String remittanceAmount) {
		this.remittanceAmount = remittanceAmount;
	}
	public String getPatientLiability() {
		return patientLiability;
	}
	public void setPatientLiability(String patientLiability) {
		this.patientLiability = patientLiability;
	}
	public String getCheckNumber() {
		return checkNumber;
	}
	public void setCheckNumber(String checkNumber) {
		this.checkNumber = checkNumber;
	}
	public String getCheckIssueDate() {
		return checkIssueDate;
	}
	public void setCheckIssueDate(String checkIssueDate) {
		this.checkIssueDate = checkIssueDate;
	}
	

	
	
	
	

}
