
package com.rel.xi.relmobapp.check_ca_type;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DT_CHECK_CA_TYPE_resp complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DT_CHECK_CA_TYPE_resp">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CYCLE_NO" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DONT_DISPLAY" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FORM_TYPE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RETURN" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="TYPE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="NUMBER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="MESSAGE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="LOG_NO" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="LOG_MSG_NO" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="MESSAGE_V1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="MESSAGE_V2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="MESSAGE_V3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="MESSAGE_V4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="PARAMETER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ROW" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="FIELD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="SYSTEM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DT_CHECK_CA_TYPE_resp", propOrder = {
    "cycleno",
    "dontdisplay",
    "formtype",
    "_return"
})
public class DTCHECKCATYPEResp {

    @XmlElement(name = "CYCLE_NO")
    protected String cycleno;
    @XmlElement(name = "DONT_DISPLAY")
    protected String dontdisplay;
    @XmlElement(name = "FORM_TYPE")
    protected String formtype;
    @XmlElement(name = "RETURN")
    protected DTCHECKCATYPEResp.RETURN _return;

    /**
     * Gets the value of the cycleno property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCYCLENO() {
        return cycleno;
    }

    /**
     * Sets the value of the cycleno property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCYCLENO(String value) {
        this.cycleno = value;
    }

    /**
     * Gets the value of the dontdisplay property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDONTDISPLAY() {
        return dontdisplay;
    }

    /**
     * Sets the value of the dontdisplay property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDONTDISPLAY(String value) {
        this.dontdisplay = value;
    }

    /**
     * Gets the value of the formtype property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFORMTYPE() {
        return formtype;
    }

    /**
     * Sets the value of the formtype property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFORMTYPE(String value) {
        this.formtype = value;
    }

    /**
     * Gets the value of the return property.
     * 
     * @return
     *     possible object is
     *     {@link DTCHECKCATYPEResp.RETURN }
     *     
     */
    public DTCHECKCATYPEResp.RETURN getRETURN() {
        return _return;
    }

    /**
     * Sets the value of the return property.
     * 
     * @param value
     *     allowed object is
     *     {@link DTCHECKCATYPEResp.RETURN }
     *     
     */
    public void setRETURN(DTCHECKCATYPEResp.RETURN value) {
        this._return = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="TYPE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="NUMBER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="MESSAGE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="LOG_NO" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="LOG_MSG_NO" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="MESSAGE_V1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="MESSAGE_V2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="MESSAGE_V3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="MESSAGE_V4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="PARAMETER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ROW" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="FIELD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="SYSTEM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "type",
        "id",
        "number",
        "message",
        "logno",
        "logmsgno",
        "messagev1",
        "messagev2",
        "messagev3",
        "messagev4",
        "parameter",
        "row",
        "field",
        "system"
    })
    public static class RETURN {

        @XmlElement(name = "TYPE")
        protected String type;
        @XmlElement(name = "ID")
        protected String id;
        @XmlElement(name = "NUMBER")
        protected String number;
        @XmlElement(name = "MESSAGE")
        protected String message;
        @XmlElement(name = "LOG_NO")
        protected String logno;
        @XmlElement(name = "LOG_MSG_NO")
        protected String logmsgno;
        @XmlElement(name = "MESSAGE_V1")
        protected String messagev1;
        @XmlElement(name = "MESSAGE_V2")
        protected String messagev2;
        @XmlElement(name = "MESSAGE_V3")
        protected String messagev3;
        @XmlElement(name = "MESSAGE_V4")
        protected String messagev4;
        @XmlElement(name = "PARAMETER")
        protected String parameter;
        @XmlElement(name = "ROW")
        protected String row;
        @XmlElement(name = "FIELD")
        protected String field;
        @XmlElement(name = "SYSTEM")
        protected String system;

        /**
         * Gets the value of the type property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTYPE() {
            return type;
        }

        /**
         * Sets the value of the type property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTYPE(String value) {
            this.type = value;
        }

        /**
         * Gets the value of the id property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getID() {
            return id;
        }

        /**
         * Sets the value of the id property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setID(String value) {
            this.id = value;
        }

        /**
         * Gets the value of the number property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getNUMBER() {
            return number;
        }

        /**
         * Sets the value of the number property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setNUMBER(String value) {
            this.number = value;
        }

        /**
         * Gets the value of the message property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMESSAGE() {
            return message;
        }

        /**
         * Sets the value of the message property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMESSAGE(String value) {
            this.message = value;
        }

        /**
         * Gets the value of the logno property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getLOGNO() {
            return logno;
        }

        /**
         * Sets the value of the logno property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setLOGNO(String value) {
            this.logno = value;
        }

        /**
         * Gets the value of the logmsgno property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getLOGMSGNO() {
            return logmsgno;
        }

        /**
         * Sets the value of the logmsgno property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setLOGMSGNO(String value) {
            this.logmsgno = value;
        }

        /**
         * Gets the value of the messagev1 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMESSAGEV1() {
            return messagev1;
        }

        /**
         * Sets the value of the messagev1 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMESSAGEV1(String value) {
            this.messagev1 = value;
        }

        /**
         * Gets the value of the messagev2 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMESSAGEV2() {
            return messagev2;
        }

        /**
         * Sets the value of the messagev2 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMESSAGEV2(String value) {
            this.messagev2 = value;
        }

        /**
         * Gets the value of the messagev3 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMESSAGEV3() {
            return messagev3;
        }

        /**
         * Sets the value of the messagev3 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMESSAGEV3(String value) {
            this.messagev3 = value;
        }

        /**
         * Gets the value of the messagev4 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMESSAGEV4() {
            return messagev4;
        }

        /**
         * Sets the value of the messagev4 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMESSAGEV4(String value) {
            this.messagev4 = value;
        }

        /**
         * Gets the value of the parameter property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPARAMETER() {
            return parameter;
        }

        /**
         * Sets the value of the parameter property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPARAMETER(String value) {
            this.parameter = value;
        }

        /**
         * Gets the value of the row property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getROW() {
            return row;
        }

        /**
         * Sets the value of the row property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setROW(String value) {
            this.row = value;
        }

        /**
         * Gets the value of the field property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getFIELD() {
            return field;
        }

        /**
         * Sets the value of the field property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setFIELD(String value) {
            this.field = value;
        }

        /**
         * Gets the value of the system property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSYSTEM() {
            return system;
        }

        /**
         * Sets the value of the system property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSYSTEM(String value) {
            this.system = value;
        }

    }

}
