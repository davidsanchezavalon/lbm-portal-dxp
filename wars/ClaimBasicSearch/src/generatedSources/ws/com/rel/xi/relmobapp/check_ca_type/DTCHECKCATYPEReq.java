
package com.rel.xi.relmobapp.check_ca_type;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DT_CHECK_CA_TYPE_req complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DT_CHECK_CA_TYPE_req">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CONT_ACCT" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DT_CHECK_CA_TYPE_req", propOrder = {
    "contacct"
})
public class DTCHECKCATYPEReq {

    @XmlElement(name = "CONT_ACCT", required = true)
    protected String contacct;

    /**
     * Gets the value of the contacct property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCONTACCT() {
        return contacct;
    }

    /**
     * Sets the value of the contacct property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCONTACCT(String value) {
        this.contacct = value;
    }

}
