
package com.rel.xi.relmobapp.check_ca_type;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.rel.xi.relmobapp.check_ca_type package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _MTCHECKCATYPEReq_QNAME = new QName("http://rel.com/xi/RelMobApp/CHECK_CA_TYPE", "MT_CHECK_CA_TYPE_req");
    private final static QName _MTCHECKCATYPEResp_QNAME = new QName("http://rel.com/xi/RelMobApp/CHECK_CA_TYPE", "MT_CHECK_CA_TYPE_resp");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.rel.xi.relmobapp.check_ca_type
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DTCHECKCATYPEResp }
     * 
     */
    public DTCHECKCATYPEResp createDTCHECKCATYPEResp() {
        return new DTCHECKCATYPEResp();
    }

    /**
     * Create an instance of {@link DTCHECKCATYPEReq }
     * 
     */
    public DTCHECKCATYPEReq createDTCHECKCATYPEReq() {
        return new DTCHECKCATYPEReq();
    }

    /**
     * Create an instance of {@link DTCHECKCATYPEResp.RETURN }
     * 
     */
    public DTCHECKCATYPEResp.RETURN createDTCHECKCATYPERespRETURN() {
        return new DTCHECKCATYPEResp.RETURN();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DTCHECKCATYPEReq }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://rel.com/xi/RelMobApp/CHECK_CA_TYPE", name = "MT_CHECK_CA_TYPE_req")
    public JAXBElement<DTCHECKCATYPEReq> createMTCHECKCATYPEReq(DTCHECKCATYPEReq value) {
        return new JAXBElement<DTCHECKCATYPEReq>(_MTCHECKCATYPEReq_QNAME, DTCHECKCATYPEReq.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DTCHECKCATYPEResp }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://rel.com/xi/RelMobApp/CHECK_CA_TYPE", name = "MT_CHECK_CA_TYPE_resp")
    public JAXBElement<DTCHECKCATYPEResp> createMTCHECKCATYPEResp(DTCHECKCATYPEResp value) {
        return new JAXBElement<DTCHECKCATYPEResp>(_MTCHECKCATYPEResp_QNAME, DTCHECKCATYPEResp.class, null, value);
    }

}
