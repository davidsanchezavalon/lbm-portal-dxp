/**
 * Description
 *		This file contain the javascript needed for Alloy UI and Prior Auth Search.
 *
 * CHANGE History
 * 		Version 1.0  
 *			Added Alloy UI functions.  
 * 		Version 1.1  
 *			Added functions needed used by advanced search.  
 * 		Version 1.2
 *     		Fixed the busy spinner fo IE.
 * 		Version 1.3				02/15/2018
 * 			Added checkItem and removeWhitespace functions.
 *     
 */

var exitString = 'You have made updates to this page that have not been saved.  If you continue the updates will be lost.';

makeFormResponsive();
formatRequiredLabels('*');

function makeFormResponsive() {
	var groups = document.getElementById('main-content').getElementsByClassName('control-group');
	
	for (var i = 0, n = groups.length; i < n; i++ ) {
		groups[i].setAttribute('class', groups[i].getAttribute('class') + ' row-fluid');
		
		var label = groups[i].getElementsByTagName('label')[0];
		
		if (label) {
			label.setAttribute('class', label.getAttribute('class') + ' span5');
		}
		
		var input = groups[i].getElementsByTagName('input')[0];
		
		if (input) {
			input.setAttribute('class', input.getAttribute('class') + ' span7');
		}
	}
}


function formatRequiredLabels(text) {
	var reqLabels = document.getElementById('main-content').getElementsByClassName('label-required');
	
	for ( var i = 0, n = reqLabels.length; i < n; i++ ) {
		reqLabels[i].innerHTML = text;
	}
}

function checkValidForm(){
	if (isValidForm()) {
		showBusySign();
	}
}

function showBusySign() {
		
	// The form must be submitted before the animated GIF will move in IE
	$('#' + getNamespace() + 'authSearchDetails').submit();
	
	document.getElementById('busy_indicator').style.display = 'block';
}

function getBrowser() {
	var rtnValue = "unknown";
	
	if ((navigator.userAgent.indexOf("Opera") || navigator.userAgent.indexOf('OPR')) != -1 ) {
		rtnValue = "Opera";
	} if (navigator.userAgent.indexOf("Chrome") != -1 ) {
		rtnValue = "Chrome";
	} if (navigator.userAgent.indexOf("Safari") != -1) {
		rtnValue = "Safari";
	} else if ((navigator.userAgent.indexOf("MSIE") != -1 ) || (!!document.documentMode == true )) { //IF IE > 10
		rtnValue = "IE";
	} else if (navigator.userAgent.indexOf("Firefox") != -1 )  {
		rtnValue = "Firefox";
	} else {
		rtnValue = "unknown";
	}

	return rtnValue;
}

function addWatermark(elem, dateFlag) {
	var browserStr = getBrowser();
	
	if (dateFlag) {
		
		// Add a watermark to the date field
		if ((browserStr == "IE") || (browserStr == "Firefox") || (browserStr == "unknown")) {
			$('#' + elem).Watermark("mm/dd/yyyy");
		}
	} else {
		
		// Add a watermark to the time field
		$('#' + elem).Watermark("hh:mm AM/PM");
	}
}

function isValid(_dat, _len, _requiredFlag) {
	var rtnValue = false;
	var length = _dat.length;

	if (length == 0) {
		if (!_requiredFlag) {
			rtnValue = true;
		}
	} else if (length <= _len) {
		rtnValue = true;
	}
	
	return rtnValue;
}

function isValidDate(_dateString, _requiredFlag) {
	var rtnValue = true;
	var browserStr = getBrowser();
	var parts;
 	var day;
 	var month;
	var year;

	if (_dateString.length > 0) {

		// First check for the pattern
		if ((browserStr == "IE") || (browserStr == "Firefox")) {
			
			// Check format mm/dd/yyyy
			if (!/^\d{1,2}\/\d{1,2}\/\d{4}$/.test(_dateString)) {
				rtnValue = false;
			}
			
			if (rtnValue) {
	
				// Parse the date parts to integers
				parts = _dateString.split("/");
				day = parseInt(parts[1], 10);
				month = parseInt(parts[0], 10);
				year = parseInt(parts[2], 10);
			}
		} else {
			
			// Check format yyyy-mm-dd
			if (!/^\d{4}-\d{1,2}-\d{1,2}$/.test(_dateString)) {
				rtnValue = false;
			}
		
			
			if (rtnValue) {
				// Parse the date parts to integers
				parts = _dateString.split("-");
				day = parseInt(parts[2], 10);
				month = parseInt(parts[1], 10);
				year = parseInt(parts[0], 10);
			}
		}
	
		if (rtnValue) {
	
			// Check the ranges of month and year
			if ((year < 1000) || (year > 3000) || (month == 0) || (month > 12)) {
				rtnValue = false;
			}
		
			var monthLength = [ 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 ];
		
			// Adjust for leap years
		 	if ((year % 400 == 0) || ((year % 100 != 0) && (year % 4 == 0))) {
				monthLength[1] = 29;
			}
		
			// Check the range of the day
			rtnValue = ((day > 0) && (day <= monthLength[month - 1]));
		}
	} else {
		if (_requiredFlag){
			
			// Return not valid for a blank required date
			rtnValue = false;
		}
	}
	return rtnValue;
}

function isValidDigit(_dat, _len, _requiredFlag, equalFlag) {
	var rtnValue = false;
	var length = _dat.length;

	if (length == 0) {
		if (!_requiredFlag) {
			rtnValue = true;
		}
	} else {
		var isNumber =  /^\d+$/.test(_dat);
		if (equalFlag) {
			if (isNumber && (_dat.length == _len)) {
				rtnValue = true;
			}
		} else if (isNumber && (_dat.length <= _len)) {
			rtnValue = true;
		}
	}
	
	return rtnValue;
}

function isValidAlphanum(_dat, _len, _requiredFlag) {
	var rtnValue = false;
	var length = _dat.length;

	if (length == 0) {
		if (!_requiredFlag) {
			rtnValue = true;
		}
	} else {
		var isAlphanumeric =  /^[a-zA-Z0-9]+$/.test(_dat);
		if (isAlphanumeric && (length <= _len)) {
			rtnValue = true;
		}
	}
	
	return rtnValue;
}

$("#memberResults").on("sortEnd", function(event) {
	var imgUp = "<i class='icon-chevron-sign-up'></i>";
	var imgDown = "<i class='icon-chevron-sign-down'></i>";
	var headStr = ["Authorization Number", "Authorization Status", "Creation Date and Time", "Service Begin Date", 
	               "ID Card Number", "First Name", "Last Name", "Date Of Birth"];
	var htmlText = "";

	// Get the change to the table sort
    lastSortlist = event.target.config.sortList;
	sortColumn = lastSortlist[0][0];
	sortOrder = lastSortlist[0][1];

	// Add the icon to the sorted column
	for (var  i = 0; i < headStr.length; i++) {
		htmlText = headStr[i];
		if (sortColumn == i) {
			if (sortOrder == 0){
				htmlText += imgUp;
			} else {
				htmlText += imgDown;
			}
		}
		$("#head" + i).html(htmlText);
	}
});

function convertToUppercase(val) {
	val.value = val.value.toLocaleUpperCase();
}

function showSearchScreen() {

	// Hide the results table
	$('#showResults').hide();
	
	// Do not display any error text
	document.getElementById("errorText").style.display = 'none';

	// Show correct search type based on the selected radio button
	showSearchItems($('input[name=radioselect]:radio:checked').val(), false);
}

/*
 *  Convert the date to yyyy-MM-dd from MM/dd/yyyy.
 */
function convertDateChrome(_dat) {
	var rtnValue = "";
	
	// Parse the first date to individual parts
	var day = _dat.substring(3, 5);
	var month = _dat.substring(0, 2);
	var year = _dat.substring(6, 10);
	
	rtnValue = year + "-" + month + "-" + day;
	
	return rtnValue;
}

//Cause the AUI validation to fire
function checkItem(checkId) {

	// Save the current active element
	var activeId = document.activeElement.id;

	// Set the focus then leave each field to display any error messages
	$("#" + checkId).focus();
	$("#" + checkId).blur();
	
	// Set focus to the saved field
	$("#" + activeId).focus();
}

function removeWhitespace(itm) {
	 
	// Get the trimmed value
	var trimItm = $("#" + itm).val().trim();

	if (trimItm) {

     	// Update the form with the trimmed value
     	$("#" + itm).val(trimItm);
	}
	else {
		return false;
	}
}
