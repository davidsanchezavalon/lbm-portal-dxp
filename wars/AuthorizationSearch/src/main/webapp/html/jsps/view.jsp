 <%--
 /**
  * Description
  *		This file contain the Authorization Search portlet for Prior Auth Search.
  *
  * CHANGE History
  * 	Version 1.0
  *			All of the changes have been merged.
  *			Added the validations.
  * 	Version 1.1
  *			Fixed LJR-403.
  *		Version 1.2
  *     	Added Cancel button
  *		Version 1.3
  *     	Added advanced search fields.
  *			Allow sort on any column in the table returned from the member search.
  * 	Version 1.4
  *     	Fixed the busy spinner fo IE
  *         Fixed date fields in IE.
  * 	Version 1.5
  *     	Do not require ID Card Number.  The required fields are ID Card Number and/or First Name, Last Name, and DOB.
  * 	Version 1.6
  *     	Added the changes for the initial version of Post Service Review.
  * 	Version 1.7						10/04/2017
  *     	Added constants for authType select element.
  * 	Version 1.8						12/05/2017
  *			Fixed the page getting submitted twice.
  *		Version 1.9						01/03/2018
  *			Made the changes needed to use DataTables
  *		Version 1.10					01/12/2018
  *			Disable Search button until all required fields are valid.
  *			When the Search button is disabled show a mouse over tooltip with what is missing.
  *			Added Enter key functionality.
  *		Version 1.11					06/06/2018
  *			Change the cancel button from a hyperlink to a button.
  *		Version 1.12					07/16/2018
  *			Move the label to the left of the field.
  *
  */
--%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<%@ page import="com.avalon.member.search.beans.MemberSearchConstants" %>

<portlet:defineObjects />

<portlet:resourceURL var="authInfoSearchformURL" id="authInfoSearchform">
</portlet:resourceURL>

<portlet:actionURL var="getAuthorizationDetailsURL">
	<portlet:param name="action" value="getAuthorizationDetails" />
	<portlet:param name="authorizationDetailsKey" value="0" />
</portlet:actionURL>

<portlet:actionURL var="cancelURLByDetailsTagURL">
	<portlet:param name="action" value="cancelPage" />
</portlet:actionURL>


<script>
	<%
		final int FIRST_NAME_LEN = 20;
		final int LAST_NAME_LEN = 70;
		
		final int NUM_LEN = 17;
	%>
</script>

<aui:script>
	var toolTip = "";
	var browserStr = getBrowser();
	var startDate = "2016-01-01";

	function getNamespace() {
		return("<portlet:namespace/>");
	}

	YUI().ready("aui-node", "event", function(Y) {
		var msg = "<%=renderRequest.getParameter("message")%>";
		if (msg == "null") {
			document.getElementById("errorText").style.display = "none";
		} else {
			document.getElementById("errorText").innerHTML = msg;
		}
	});

	var checkGroupFlag = false;
	var firstNameFlag = false;
	var lastNameFlag = false;
	var dobFlag = false;

	var bogusAuthorizationNumber = "99999999999999999"
	var bogusIdCardNumber = "REQID";

	$(document).ready(function(){

		// Set the initial search button status
		setSearchButtonStatus();
		
		// call the tablesorter plugin 
	    $("#memberResults").tablesorter( {sortList: [[0,1]]} ); 
		
		// Show correct search type
		showSearchItems($("#<portlet:namespace/>searchType").val(), true);
		
		// Add a watermark to date fields
		addWatermark("<portlet:namespace/>dateOfBirth", true);
		addWatermark("<portlet:namespace/>dateOfService", true);

		// Execute the search when the enter key is pressed for the authorization number
		processEnterKey("authorizationNumber");

		// Execute the search when the enter key is pressed for the ID card number
		processEnterKey("memberId");

		// Execute the search when the enter key is pressed for the first name
		processEnterKey("firstName");

		// Execute the search when the enter key is pressed for the last name
		processEnterKey("lastName");

		// Execute the search when the enter key is pressed for the date of birth
		processEnterKey("dateOfBirth");

		// Execute the search when the enter key is pressed for the date of service
		processEnterKey("dateOfService");
	});

	function clearWatermarks() {
		var browserStr = getBrowser();
		
		if ((browserStr == "IE") || (browserStr == "Firefox") || (browserStr == "unknown")) {
				
			// Clear the watermark in dob if it is not set
			if ($("#<portlet:namespace/>dateOfBirth").val() == "mm/dd/yyyy") {
				$("#<portlet:namespace/>dateOfBirth").Watermark("");
			}
			
			// Clear the watermark in service date if it is not set
			if ($("#<portlet:namespace/>dateOfService").val() == "mm/dd/yyyy") {
				$("#<portlet:namespace/>dateOfService").Watermark("");
			}
		}
	}
	
	function closePortlet() {
	    var url="<%= cancelURLByDetailsTagURL.toString() %>";
	    window.location.href = url;
	}

	function openPriorAuthPortlet() {
	    var url="<%= getAuthorizationDetailsURL.toString() %>";
	    window.location.href = url;
	}

	function validateNameAndDob() {
		var retValue = false;
		var firstName = $("#<portlet:namespace/>firstName").val();
		var lastName = $("#<portlet:namespace/>lastName").val();
		var dob = $("#<portlet:namespace/>dateOfBirth").val();
	
		if ((firstName.length > 0) || (lastName.length > 0) || (dob.length > 0)) {
			retValue = isValid(firstName, <%= FIRST_NAME_LEN %>, true) && 
			           isValid(lastName, <%= LAST_NAME_LEN %>, true) && 
			           isValidDate(dob, true);
		} else {
			retValue = true;
		}
		return retValue;
	}

	function isValidForm(validateFlag) {
		authorizationNumberName = "<portlet:namespace/>authorizationNumber";
		idCardNumberName = "<portlet:namespace/>memberId";
		firstnameName = "<portlet:namespace/>firstName";
		lastnameName = "<portlet:namespace/>lastName";
		dateOfBirthName = "<portlet:namespace/>dateOfBirth";
		dateOfServiceName = "<portlet:namespace/>dateOfService";

		// Remove any whitespace from the id card number, first namd, and last name
		removeWhitespace(idCardNumberName);
		removeWhitespace(firstnameName);
		removeWhitespace(lastnameName);

		var retValue = false;
		var authEntered = false;
		var memberAddlEntered = false;
		var memberEntered = false;
		var searchType = $("#<portlet:namespace/>searchType").val();
		var authorizationNumber = $("#" + authorizationNumberName).val();
		var memberId = $("#" + idCardNumberName).val();
		var firstName = $("#" + firstnameName).val();
		var lastName = $("#" + lastnameName).val();
		var dateOfBirth = $("#" + dateOfBirthName).val();
		var dateOfService = $("#" + dateOfServiceName).val();
		var dateOfBirth_html5 = null;
		var dateOfService_html5 = null;

		// Initialize the tool tip for the search button
		toolTip = "";

		// Test for a valid authorization number
		if (isValidAuthNumber(authorizationNumber)) {
			authEntered = true;
		}
		
		// Test for a valid ID card number
		if (isValidAlphanum(memberId, <%= NUM_LEN %>, false)) {
			memberEntered = true;
		}

		// Check for watermark in date fields (IE problem)
		if ((browserStr == "IE") || (browserStr == "Firefox") || (browserStr == "unknown")) {
			if (dateOfBirth == "mm/dd/yyyy") {
				dateOfBirth = "";
			}
			if (dateOfService == "mm/dd/yyyy") {
				dateOfService = "";
			}
		}

		// Test for valid first name, last name, and date of birth
		if ((firstName.length == 0) && (lastName.length == 0) && (dateOfBirth.length == 0)) {
			memberAddlEntered = true;
		} else if (isValid(firstName, <%= FIRST_NAME_LEN %>, true) && isValid(lastName, <%= LAST_NAME_LEN %>, true) && isValidDate(dateOfBirth, true)) {
			memberAddlEntered = true;
		}
		
		// Test for a valid date of service (valid format and not before 01/01/2016)
		if (isValidDate(dateOfService, false)) {
			if (dateOfService.length > 0) {
				if ((browserStr == "IE") || (browserStr == "Firefox") || (browserStr == "unknown")) {
					dateOfService = convertDateChrome(dateOfService);
				}
				if (dateOfService < startDate) {
					memberAddlEntered = false;
				}
			}
			
		} else {
			memberAddlEntered = false;
		}
		
		// Test for html5 errors
		if ((browserStr != "IE") && (browserStr != "Firefox") && (browserStr != "unknown") && memberAddlEntered) {
			dateOfBirth_html5 = document.getElementById(dateOfBirthName);
			dateOfService_html5 = document.getElementById(dateOfServiceName);
			
			if (dateOfBirth_html5.validity.badInput || dateOfService_html5.validity.badInput) {
				memberAddlEntered = false;
			}
		}
		
		// Test for either ID Card of additional data is entered
		if ((memberId == 0) && (firstName.length == 0) && (lastName.length == 0) && (dateOfBirth.length == 0)) {
			memberEntered = false;
			memberAddlEntered = false;
		}

		if (searchType == "member") {
			retValue = (memberEntered && memberAddlEntered);
			
			if (!retValue) {
				if (!memberEntered && !memberAddlEntered) {
					toolTip += "Enter either valid ID Card Number or First Name/Last Name/Date of Birth";
				} else {
					
					// Set the tool tip
					// Test the ID Card number
					if (!isValidAlphanum(memberId, <%= NUM_LEN %>, false)) {
						toolTip += "ID Card Number is not valid";
					}
					
					if ((firstName.length > 0) || (lastName.length > 0) || (dateOfBirth.length > 0)) {
						
						// Test first name
						if (!isValid(firstName, <%= FIRST_NAME_LEN %>, true)) {
							toolTip += "\nFirst Name is not valid";
						}
						
						// Test last name
						if (!isValid(lastName, <%= LAST_NAME_LEN %>, true)) {
							toolTip += "\nLast Name is not valid";
						}
						
						// Test date of birth
						if (!isValidDate(dateOfBirth, true)) {
								toolTip += "\nDate of Birth is not valid";
							} else if (dateOfBirth_html5 != null) {
								if (dateOfBirth_html5.validity.badInput) {
								toolTip += "\nDate of Birth is not valid";
							}
						}
					}
				}
				
				// Test the date of service
				if ((dateOfService.length > 0) && (dateOfService < startDate)) {
					toolTip += "\nDate of Service is not valid";
				} else if (dateOfService_html5 != null) {
					if (dateOfService_html5.validity.badInput) {
						toolTip += "\nDate of Service is not valid";
					}
				}
			}
		} else {
			retValue = authEntered;
			
			if (!retValue) {
				
				// Set the tool tip
				// Test the authorization number
				if (!isValidAuthNumber(authorizationNumber)) {
					toolTip += "Authorization Number is not valid";
				}
			}
		}
		
		if (retValue && validateFlag) {

			// Remove any invalid error messages
			checkItem(authorizationNumberName);
			checkItem(idCardNumberName);
			checkItem(firstnameName);
			checkItem(lastnameName);
			checkItem(dateOfBirthName);
			checkItem(dateOfServiceName);
		}
		
		return retValue;
	}
	
	function isValidAuthNumber(num) {
		var retValue = false;

		if (num.length == <%= NUM_LEN %>) {
			
			// Test for a numeric provider number
			var providerStr = num.substring(0, 4);
			var providerTest = !isNaN(providerStr) && /[+-\\.]/.test(providerStr);
			
			// Test for valid Post Service or numeric
			var typeStr = num.substring(4, 6);
			var typeTest = (typeStr == "PR") || (typeStr == "01");
			
			// Test for a numeric date/sequence number string
			var sequenceStr = num.substring(6);
			var sequenceTest = !isNaN(sequenceStr) && /[+-\\.]/.test(sequenceStr);
			
			retValue = providerTest && typeTest && sequenceTest;
		}
		
		return retValue;
	}

	function showSearchItems(itm, initFlag) {
		
		// Show the search screen based on the search type
		if (itm == "member") {

			// Turn on search by Member
			$("#searchByNumber").hide();
			$("#searchByMember").show();
			
			if (initFlag) {

				// Select the 'Member' radio button
				$("input[id=radioselectmember]").prop("checked", true);
			} else {
			
				// Clear the required ID card number if it was set in the html
				if ($("#<portlet:namespace/>memberId").val().toUpperCase() == bogusIdCardNumber) {
					$("#<portlet:namespace/>memberId").val("");
				}
			}

			// Add text to hidden required Authorization Number
			$("#<portlet:namespace/>authorizationNumber").val(bogusAuthorizationNumber);
			
			// Set the search type in the hidden field
			$("#<portlet:namespace/>searchType").val("member");
		} else {
			
			// Turn on search by Authorization Number
			$("#searchByNumber").show();
			$("#searchByMember").hide();

			if (initFlag) {

				// Select the 'Authorization Number' radio button
				$("input[id=radioselectnumber]").prop("checked", true);
			} else {

				// Clear the required Authorization Number if it was set in the html
				if ($("#<portlet:namespace/>authorizationNumber").val() == bogusAuthorizationNumber) {
					$("#<portlet:namespace/>authorizationNumber").val("");
				}
			}

			// Add text to hidden required member ID and clear name, dob, and date of service
			$("#<portlet:namespace/>memberId").val(bogusIdCardNumber);
			$("#<portlet:namespace/>firstName").val("");
			$("#<portlet:namespace/>lastName").val("");
			$("#<portlet:namespace/>dateOfBirth").val("");
			$("#<portlet:namespace/>dateOfService").val("");
			
			// Set the search type in the hidden field
			$("#<portlet:namespace/>searchType").val("number");
		}
		
		// Reset member flags
		checkGroupFlag = false
		firstNameFlag = false;
		lastNameFlag = false;
		dobFlag = false;
	}
	
	function setSearchButtonStatus() {
		
		// Disable the search button until all the fields are valid
		if (isValidForm(false)) {
			$("#<portlet:namespace/>authorizationSearch").removeAttr("disabled");
		} else {
			$("#<portlet:namespace/>authorizationSearch").attr("disabled", "disabled");
		}
		
		// Add the mouse over text for the search button
		$("#<portlet:namespace/>authorizationSearch").prop("title", toolTip);
	}
	
	function createLink(id, key) {
		var lnk = '<a href="<portlet:actionURL><portlet:param name="action" value="getAuthorizationDetails" />' +
                  '<portlet:param name="authorizationDetailsKey" value="000000" /> ' +
                  '</portlet:actionURL>">' + id + '</a>';
                  
		lnk = lnk.replace("000000", key);
		return lnk;
	}

	function getSearchResult() {
		clearWatermarks();

		// Display the prior auth search results
		getAuthSearchResult();
	}
	
	function getAuthSearchResult() {
		$("#authSearchResults").hide();
		$("#jsonErrorMessage").hide();
	
		if (isValidForm(true)) {
			$("#dataloader").show();
	
			$.ajax({
				type : "POST",
				data : $("#<portlet:namespace/>authSearchDetails").serialize(),
				dataType : "json",
				url : "${authInfoSearchformURL}",
				
				success : function(authInfo) {
					var authdata = JSON.stringify(authInfo);
					console.log('authdata---->>'+authdata);
					var isError;
					for (var i = 0; i < authInfo.length; i++) { 
						isError = authInfo[i].error;
					}
			
					$("#dataloader").hide();
					if ((isError != null && isError != "" && isError != "undefined")) {
						console.log('error---->>');
						// Display the error message
						$("#jsonErrorMessage").show();
						$("#jsonErrorText").text(isError);
					} else {
						
						if (authInfo.length == 1) {
							console.log('inside length 1---->>');
							// Go to the Procedure Information page
							openPriorAuthPortlet();
						} else {
							console.log('inside else---->>');
							// Display the records in the result table
							$("#authSearchResults").show();
							$("#authResults").dataTable({
								"bDestroy" : true,
								"sPaginationType" : "full_numbers",
								"bProcessing" : true,
								"aaData" : jQuery.parseJSON(authdata),
								"aoColumns" : [{
									"mData" : "<%= MemberSearchConstants.AUTH_NUMBER %>"
								}, {
									"mData" : "<%= MemberSearchConstants.AUTH_STATUS_CODE %>"
								}, {
									"mData" : "<%= MemberSearchConstants.CREATE_DATETIME %>"
								}, {
									"mData" : "<%= MemberSearchConstants.AUTH_BEGIN_SERVICE_DATE %>"
								}, {
									"mData" : "<%= MemberSearchConstants.MEMBER_ID %>"
								}, {
									"mData" : "<%= MemberSearchConstants.FIRST_NAME %>"
								}, {
									"mData" : "<%= MemberSearchConstants.LAST_NAME %>"
								}, {
									"mData" : "<%= MemberSearchConstants.DATE_OF_BIRTH %>"
								}],
								"aoColumnDefs" : [{
									"aTargets" : [0],
									"mRender" : function (data, type, row, meta) {
										if (type === "display") {
											data = createLink(data, row.keyCode);
										}
										return data;
									}
								}]
							});
						}
					}
				}
			});
		}
	}

	function processEnterKey(id) {
		document.getElementById(getNamespace() + id).addEventListener("keyup", function(event) {
			event.preventDefault();
			if (event.keyCode === 13) {
				getSearchResult();
			}
		});
	}
</aui:script>

<div id="busy_indicator" style="display: none">
	<img src="<%=request.getContextPath()%>/images/busy-spinner.gif" alt="Busy indicator">
</div>

<liferay-ui:panel title="as.title" collapsible="false">
	<aui:form name="authSearchDetails" commandname="authorizationSearchFO" onChange="javascript:setSearchButtonStatus()" >
		<aui:fieldset>
			<aui:container>
				<input type="hidden" class="hidden_searchType" name="<portlet:namespace/>searchType" id="<portlet:namespace/>searchType" value="${authorizationSearchFO.searchType}">
				<aui:row>
					<aui:col span="12">
						<div class="middle" style="text-align: center; width: 100%;">
							Search By:
							<input type="radio" name="radioselect" id="radioselectnumber" style="width: 50px; margin-top: -1px;" value="number" checked="checked"
							       onclick="javascript:showSearchScreen()">Authorization Number
							<input type="radio" name="radioselect" id="radioselectmember" style="width: 50px; margin-top: -1px;" value="member"
							       onclick="javascript:showSearchScreen()" >Member
						</div>
   					</aui:col>
				</aui:row>
			</aui:container>
		</aui:fieldset>
		<div id="searchByNumber" style="display: inline">
			<aui:fieldset label="as.search.by.number">
				<aui:container>
					<aui:row>
						<aui:col span="6">
							<aui:input type="text" inlineLabel="true" name="authorizationNumber" resizable="false" label="as.authorization.number" maxlength="<%= NUM_LEN %>"
								value="${authorizationSearchFO.authorizationNumber}" >
								<aui:validator name="required" errorMessage="<%= MemberSearchConstants.AUTHNUMBER_ERROR %>" />
						        <aui:validator name="custom" errorMessage="<%= MemberSearchConstants.AUTHNUMBER_ERROR %>">
								    function (val, fieldNode, ruleValue) {
								    	var retValue = false;
								    	
								    	retValue = isValidAuthNumber(val);
								    	
								    	return retValue;
								    }
						        </aui:validator>
								<aui:validator name="minLength"><%= NUM_LEN %></aui:validator>
								<aui:validator name="maxLength"><%= NUM_LEN %></aui:validator>
							</aui:input>
						</aui:col>
					</aui:row>
				</aui:container>
			</aui:fieldset>
		</div>
		<div id="searchByMember" style="display: none">
			<aui:fieldset label="as.search.by.member">
				<aui:container>
					<aui:row>
						<aui:col span="6">
							<aui:input type="text" inlineLabel="true" name="memberId" cssClass="form-control toUppercase" label="as.id.card.number" maxlength="<%= NUM_LEN %>" 
							           style="text-transform:uppercase"  value="${authorizationSearchFO.memberId}" onfocusout="javascript:convertToUppercase(this)" >
								<aui:validator name="alphanum" />
							</aui:input>
						</aui:col>
						<aui:col span="6">
							<aui:select inlineLabel="true" name="healthPlan" label="as.health.plan" cssClass="span7" required="true" showRequiredLabel="true" value="${authorizationSearchFO.healthPlan}" >
								<aui:option value="0010">BCBS &ndash; South Carolina</aui:option>
							</aui:select>
						</aui:col>
					</aui:row>
					<aui:row>
						<aui:col span="6">
							<aui:input type="text" inlineLabel="true" name="firstName" label="as.first.name" value="${authorizationSearchFO.firstName}" maxlength="<%= FIRST_NAME_LEN %>">
						        <aui:validator name="custom" errorMessage="<%= MemberSearchConstants.MEMBER_SEARCH_ERROR %>">
								    function (val, fieldNode, ruleValue) {
								    	var retValue = false;
								    	
										retValue = validateNameAndDob();	

										if (!checkGroupFlag) {
											checkGroupFlag = true;

											// Force validation of last name (only do this once)
											$("#<portlet:namespace/>lastName").focus();
											$("#<portlet:namespace/>lastName").blur();

											// Force validation of date of birth (only do this once)
											$("#<portlet:namespace/>dateOfBirth").focus();
											$("#<portlet:namespace/>dateOfBirth").blur();

											checkGroupFlag = false;
										}

								        return retValue;
								    }
								</aui:validator>
							</aui:input>
						</aui:col>
						<aui:col span="6">
							<aui:input type="text" inlineLabel="true" name="lastName" label="as.last.name" value="${authorizationSearchFO.lastName}" maxlength="<%= LAST_NAME_LEN %>" >
						        <aui:validator name="custom" errorMessage="<%= MemberSearchConstants.MEMBER_SEARCH_ERROR %>">
								    function (val, fieldNode, ruleValue) {
								    	var retValue = false;
								    	
										retValue = validateNameAndDob();										
										
										if (!checkGroupFlag) {
											checkGroupFlag = true;

											// Force validation of first name (only do this once)
											$("#<portlet:namespace/>firstName").focus();
											$("#<portlet:namespace/>firstName").blur();

											// Force validation of date of birth (only do this once)
											$("#<portlet:namespace/>dateOfBirth").focus();
											$("#<portlet:namespace/>dateOfBirth").blur();

											checkGroupFlag = false;
										}

								        return retValue;
								    }
								</aui:validator>
							</aui:input>
						</aui:col>
					</aui:row>
					<aui:row>
						<aui:col span="6">
							<aui:input type="date" inlineLabel="true" name="dateOfBirth" cssClass="checkForDisable sntdis datetime" label="as.date.of.birth" value="${authorizationSearchFO.dateOfBirth}">
						        <aui:validator name="custom" errorMessage="<%= MemberSearchConstants.MEMBER_SEARCH_ERROR %>">
								    function (val, fieldNode, ruleValue) {
								    	var retValue = false;
								    	
										retValue = validateNameAndDob();										

										if (!checkGroupFlag) {
											checkGroupFlag = true;

											// Force validation of first name (only do this once)
											$("#<portlet:namespace/>firstName").focus();
											$("#<portlet:namespace/>firstName").blur();

											// Force validation of last name (only do this once)
											$("#<portlet:namespace/>lastName").focus();
											$("#<portlet:namespace/>lastName").blur();

											checkGroupFlag = false;
										}

								        return retValue;
								    }
								</aui:validator>
								<aui:validator name="custom" errorMessage="Please enter a valid date (format mm/dd/yyyy)." >
									function (val, fieldNode, ruleValue) {
										var retValue = false;
										
										// Test for invalid date
										if ((browserStr == "IE") || (browserStr == "Firefox") || (browserStr == "unknown")) {
											if (val == "mm/yy/yyyy") {
												val = "";
											}
											retValue = isValidDate(val, false);
										} else {
											var dateOfBirth = document.getElementById("<portlet:namespace/>dateOfBirth");
											retValue = !dateOfBirth.validity.badInput;
										}
											
										return retValue;
									}
								</aui:validator>
							</aui:input>
						</aui:col>
						<aui:col span="6">
							<aui:input type="date" inlineLabel="true" name="dateOfService" cssClass="checkForDisable sntdis datetime" label="as.date.of.service" value="${authorizationSearchFO.dateOfService}">
						        <aui:validator name="custom" errorMessage="<%= MemberSearchConstants.DATEOFSERVICE_ERROR %>">
								    function (val, fieldNode, ruleValue) {
										var retValue = false;

										if ((browserStr == "IE") || (browserStr == "Firefox") || (browserStr == "unknown")) {
											if (val == "mm/yy/yyyy") {
												val = "";
											}
											if (val != "") {
												val = convertDateChrome(val);
											}
										}
											
										if ((val.length == 0) || (val >= startDate)) {
											retValue = true;
										}

								        return retValue;
								    }
								</aui:validator>
								<aui:validator name="custom" errorMessage="Please enter a valid date (format mm/dd/yyyy)." >
									function (val, fieldNode, ruleValue) {
										var retValue = false;
										
										// Test for invalid date
										if ((browserStr == "IE") || (browserStr == "Firefox") || (browserStr == "unknown")) {
											if (val == "mm/yy/yyyy") {
												val = "";
											}
											retValue = isValidDate(val, false);
										} else {
											var dateOfService = document.getElementById("<portlet:namespace/>dateOfService");
											retValue = !dateOfService.validity.badInput;
										}
											
										return retValue;
									}
								</aui:validator>
							</aui:input>
						</aui:col>
      				</aui:row>
      				<aui:row>
						<aui:col span="6">
							<aui:select inlineLabel="true" name="authType" id="authType" label="as.select.auth.type" cssClass="span7" >
								<aui:option value=""><%= MemberSearchConstants.AUTH_ALL %></aui:option>
								<aui:option value="<%= MemberSearchConstants.AUTH_PA %>"><%= MemberSearchConstants.AUTH_PA_DESC %></aui:option>
								<aui:option value="<%= MemberSearchConstants.AUTH_PSR %>"><%= MemberSearchConstants.AUTH_PSR_DESC %></aui:option>
							</aui:select>
						</aui:col>
      				</aui:row>
				</aui:container>
			</aui:fieldset>
		</div>
		<aui:fieldset>
			<aui:button-row cssClass="btn-divider">
				<div onmouseenter="setSearchButtonStatus()">
					<aui:button name="authorizationSearch" cssClass="pull-right" type="button" primary="true" value="Search" onclick="javascript:getSearchResult()" />
				</div>
  				<aui:button name="cancelAuthSearchInfo" cssClass="btn-gray pull-left" type="button" value="as.cancel" onclick="javascript:closePortlet()" />
			</aui:button-row>
		</aui:fieldset>
	</aui:form>
</liferay-ui:panel>

<div id="authSearchResults" style="display: none" >
	<liferay-ui:panel title="Results" collapsible="false">
		<table id="authResults" class="display" style="border-collapse: collapse; width: 100%" >
			<thead>
				<tr>
					<th>Authorization Number</th>
					<th>Authorization Status</th>
					<th>Creation Date and Time</th>
					<th>Service Begin Date</th>
					<th>ID Card Number</th>
					<th>First Name</th>
					<th>Last Name</th>
					<th>Date Of Birth</th>
				</tr>
			</thead>
			<tbody>
	
			</tbody>
		</table>
	</liferay-ui:panel>
</div>

<div id="jsonErrorMessage" style="display: none; text-align: left; color: red" >
	<span id="jsonErrorText" > </span>
</div>

<div id="dataloader" class="blink_text" style="display: none" >  
     Please wait while loading the page...
</div>

<div class="aui-message aui-message-error" style="text-align: left">
	<span id="errorText" style="color: red"> </span>
</div>
