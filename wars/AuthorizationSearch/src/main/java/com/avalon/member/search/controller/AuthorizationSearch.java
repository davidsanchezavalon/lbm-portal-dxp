package com.avalon.member.search.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletMode;
import javax.portlet.PortletModeException;
import javax.portlet.PortletRequest;
import javax.portlet.PortletSession;
import javax.portlet.PortletURL;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.portlet.WindowState;
import javax.portlet.WindowStateException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;

/**
 * Description
 *		This file is the controller for the Prior Authorization Search portlet.
 *
 * CHANGE History
 *		Version 1.0
 *			Initial version.
 *		Version 1.1
 *     		Do not require ID Card Number.  The required fields are ID Card Number and/or First Name, Last Name, and DOB.
 *		Version 1.2			01/03/2018
 *			Made the changes needed to use DataTables
 *
 */

import com.avalon.lbm.services.PriorAuthHeaderServiceStub;
import com.avalon.lbm.services.PriorAuthHeaderServiceStub.PriorAuthHeader;
import com.avalon.lbm.services.PriorAuthHeaderServiceStub.PriorAuthHeaderDTO;
import com.avalon.lbm.services.PriorAuthHeaderServiceStub.PriorAuthorizationSearch;
import com.avalon.lbm.services.PriorAuthHeaderServiceStub.PriorAuthorizationSearchE;
import com.avalon.lbm.services.PriorAuthHeaderServiceStub.PriorAuthorizationSearchResponse;
import com.avalon.lbm.services.PriorAuthHeaderServiceStub.PriorAuthorizationSearchResponseE;
import com.avalon.member.search.beans.AuthorizationSearchFO;
import com.avalon.member.search.beans.MemberDetailsFo;
import com.avalon.member.search.beans.MemberSearchConstants;
import com.avalon.member.search.businessdelegate.MemberSearchBusinessDelegateVo;
import com.avalon.member.search.customexception.CustomException;
import com.google.gson.Gson;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Layout;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.model.UserGroup;
import com.liferay.portal.kernel.portlet.PortletURLFactoryUtil;
import com.liferay.portal.kernel.service.LayoutLocalServiceUtil;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.util.portlet.PortletProps;

/**
 * Description
 *		This file contains the controller methods for the member search page.
 *
 * @author David Sanchez
 *
 * CHANGE History
 * 		Version 1.0
 * 			Initial version
 * 		Version 1.1					08/08/2018
 * 			Added HO LOE an subscriber name to the header.
 *  
 */

@Controller(value = "AuthorizationSearch")
@RequestMapping("VIEW")
public class AuthorizationSearch {
   
    private static final Log log = LogFactoryUtil.getLog(AuthorizationSearch.class.getName());
    
    /**
     * whenever the view page has been clicked the render method will be executed 
     * In this method we are removing the session variables which has been set in avalon-prior-authorization-portlet.
     * It redirects to the view page.
    */
    @RenderMapping
    public String handleRenderRequest(RenderRequest request, 
    		                          RenderResponse response, 
    		                          Model model) {
	
    	log.info("Processing Render Action for Authorization Search Enter");
    	PortletSession portletSession = request.getPortletSession();
    	
    	// Clear the details if the portlet is restarted
    	String dob = (String)portletSession.getAttribute("memberDob", PortletSession.APPLICATION_SCOPE);
		if (dob == null) {
    		dob = "";
    	}

    	if (!dob.equals("13/31/1900")) {

    		// Remove the table
            HttpSession httpSession = PortalUtil.getHttpServletRequest(request).getSession();
            httpSession.removeAttribute("details");
    	}

    	portletSession.removeAttribute("authorizationAge", PortletSession.APPLICATION_SCOPE);  
        portletSession.removeAttribute("authorizationStatus", PortletSession.APPLICATION_SCOPE);
        portletSession.removeAttribute("enteredNotes", PortletSession.APPLICATION_SCOPE);
        portletSession.removeAttribute("peerEnteredNotes", PortletSession.APPLICATION_SCOPE);
        portletSession.removeAttribute("appCreateDateTime", PortletSession.APPLICATION_SCOPE);
        portletSession.removeAttribute("originalUserId", PortletSession.APPLICATION_SCOPE);
        portletSession.removeAttribute("authDecisionDate", PortletSession.APPLICATION_SCOPE);
        portletSession.removeAttribute("providerAuthStatus", PortletSession.APPLICATION_SCOPE);
	    
	    // Remove the attributes shown in the prior authorization pages
	    portletSession.removeAttribute("memberDob", PortletSession.APPLICATION_SCOPE);
	    portletSession.removeAttribute("memberId", PortletSession.APPLICATION_SCOPE);
	    portletSession.removeAttribute("memberNumber", PortletSession.APPLICATION_SCOPE);
	    portletSession.removeAttribute("healthPlan", PortletSession.APPLICATION_SCOPE);
	    portletSession.removeAttribute("mpi", PortletSession.APPLICATION_SCOPE);
	    portletSession.removeAttribute("membergroupId", PortletSession.APPLICATION_SCOPE);
	    portletSession.removeAttribute("memberName", PortletSession.APPLICATION_SCOPE);
	    portletSession.removeAttribute("memberGender", PortletSession.APPLICATION_SCOPE);
	    portletSession.removeAttribute("memberAddressLine1", PortletSession.APPLICATION_SCOPE);
	    portletSession.removeAttribute("memberAddressLine2",PortletSession.APPLICATION_SCOPE);
	    portletSession.removeAttribute("memberCity", PortletSession.APPLICATION_SCOPE);
	    portletSession.removeAttribute("memberStateCode", PortletSession.APPLICATION_SCOPE);
	    portletSession.removeAttribute("memberZipcode", PortletSession.APPLICATION_SCOPE);
	    portletSession.removeAttribute("memberPhoneNumber", PortletSession.APPLICATION_SCOPE);

		// Remove the additional attributes saved by the prior authorization pages
		portletSession.removeAttribute("memberFirstName", PortletSession.APPLICATION_SCOPE);
		portletSession.removeAttribute("memberMiddleName", PortletSession.APPLICATION_SCOPE);
		portletSession.removeAttribute("memberLastName", PortletSession.APPLICATION_SCOPE);
		portletSession.removeAttribute("memberSuffixName", PortletSession.APPLICATION_SCOPE);
		portletSession.removeAttribute("businessSectorCode", PortletSession.APPLICATION_SCOPE);
		portletSession.removeAttribute("businessSectorDescription", PortletSession.APPLICATION_SCOPE);
		portletSession.removeAttribute("businessSegmentCode", PortletSession.APPLICATION_SCOPE);
		portletSession.removeAttribute("businessSegmentDescription", PortletSession.APPLICATION_SCOPE);

		// Remove the subscriber name attribute
		portletSession.removeAttribute("subscriberName", PortletSession.APPLICATION_SCOPE);

    	log.info("Processing Render Action for Authorization Search Exit");
    	return "view";
    }
    
    
    /**
     * whenever the search button  has been clicked the authSearchDetails method will be executed 
     * In this method we will fetch the member details and set in session 
     * It will be redirect to the avalon-prior-authorization-portlet.    (Used for actionURL)
    */
    @ActionMapping(params = "action=authSearchAction")
    public void authSearchDetails(ActionRequest request, 
    		                      ActionResponse response,
    		                      @ModelAttribute("authorizationSearchFO") AuthorizationSearchFO authorizationSearchFO)
    		throws IOException, WindowStateException, PortletModeException {

    	String mvcPathStr = "/html/jsps/view.jsp";

    	HttpSession httpSession = PortalUtil.getHttpServletRequest(request).getSession();

    	log.info("Processing Search Action for Authorization Search Enter");
        PortletSession portletSession = request.getPortletSession();
        String searchType = authorizationSearchFO.getSearchType();
        log.info("searchType : " + searchType);
        if (searchType.equals("number")) {
            log.info("authNumber : " + authorizationSearchFO.getAuthorizationNumber());
        } else {
            log.info("ID Card Number : " + authorizationSearchFO.getAuthorizationNumber());
            log.info("healthPlan : " + authorizationSearchFO.getHealthPlan());
            log.info("firstName : " + authorizationSearchFO.getFirstName());
            log.info("lastName : " + authorizationSearchFO.getLastName());
            log.info("dateOfBirth : " + authorizationSearchFO.getDateOfBirth());
            log.info("serviceDate : " + authorizationSearchFO.getDateOfService());
        }
        Map<Integer, MemberDetailsFo> details = null;
        ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
        try {

        	// Creating the stub object to invoke the authorization search service
        	PriorAuthHeaderServiceStub priorAuthHeaderServiceStub = new PriorAuthHeaderServiceStub();
            PriorAuthorizationSearchE priorAuthorizationSearchE = new PriorAuthorizationSearchE();

            PriorAuthorizationSearch priorAuthorizationSearch = new PriorAuthorizationSearch();
            PriorAuthHeader priorAuthHeader = new PriorAuthHeader();
            PriorAuthHeaderDTO[] priorAuthHeaderDTO = new PriorAuthHeaderDTO[1];
            priorAuthHeaderDTO[0] = new PriorAuthHeaderDTO();

            // Set the operation type for the prior auth search
            priorAuthHeader.setOperationType("AuthorizationSearch");
           
            String IDCardNumber = null;
        	String memberFirstName = null;
        	String memberLastName = null;
        	String memberDob = null;
        	String serviceDate = null;
        	String authType = null;
            if (searchType.equals("number")) {
            	priorAuthHeaderDTO[0].setPriorAuthNumber(authorizationSearchFO.getAuthorizationNumber());
            } else {
            	HttpServletRequest httpRequest = PortalUtil.getHttpServletRequest(request);
            	String browserStr = getBrowser(httpRequest);

                IDCardNumber = authorizationSearchFO.getMemberId();
                log.info("IDCardNumber : " + IDCardNumber);
            	priorAuthHeaderDTO[0].setIdCardNumber(IDCardNumber);
            	
            	// If the First Name is not empty, then set the first name, last name, and date of birth to be included in the search.
            	memberFirstName = authorizationSearchFO.getFirstName();
                log.info("memberFirstName : " + memberFirstName);
            	priorAuthHeaderDTO[0].setPriorAuthPatientFirstName(memberFirstName);

            	memberLastName = authorizationSearchFO.getLastName();
                log.info("memberFirstName : " + memberLastName);
            	priorAuthHeaderDTO[0].setPriorAuthPatientLastName(memberLastName);

            	memberDob = authorizationSearchFO.getDateOfBirth();
    			if (browserStr.equals("IE") || browserStr.equals("Firefox") || browserStr.equals("unknown")) {
    				if (memberDob.equals("mm/dd/yyyy")) {
    					memberDob = "";
    				}
    			}
                log.info("memberDob : " + memberDob);
            	priorAuthHeaderDTO[0].setPriorAuthPatientBirthDate(memberDob);

            	serviceDate = authorizationSearchFO.getDateOfService();
    			if (browserStr.equals("IE") || browserStr.equals("Firefox") || browserStr.equals("unknown")) {
    				if (serviceDate.equals("mm/dd/yyyy")) {
    					serviceDate = "";
    				}
    			}
	            log.info("serviceDate : " + serviceDate);
	            if (serviceDate.length() > 0) {
	            	priorAuthHeaderDTO[0].setPriorAuthServiceDate(serviceDate);
	            }
	            
	            authType = ParamUtil.getString(request, "authType");
	            log.info("authType : " + authType);
	            authorizationSearchFO.setAuthType(authType);
            }

            // SP - Just a heads up, these are actually AppAccess fields. We are transferring them as AppCreate fields so that the WSDL doesn't need to be changed pointlessly.
            priorAuthHeaderDTO[0].setAppCreateUserId(themeDisplay.getUser().getEmailAddress());
            priorAuthHeaderDTO[0].setAppCreateDatetime(new java.text.SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new java.util.Date()));

            priorAuthHeader.setPriorAuthHeaderDTO(priorAuthHeaderDTO);
            priorAuthorizationSearch.setPriorAuthorizationSearch(priorAuthHeader);

            priorAuthorizationSearchE.setPriorAuthorizationSearch(priorAuthorizationSearch);
            PriorAuthorizationSearchResponseE priorAuthorizationSearchResponseE;
            PriorAuthorizationSearchResponse priorAuthorizationSearchResponse = null;


          //TODO: Remove mock service code and condition. Don't remove else condition code
			if(GetterUtil.getBoolean(com.liferay.portal.kernel.util.PropsUtil.get(MemberSearchConstants.ENABLED_MOCK_SERVICE))){
				 //Success Mock Code
				priorAuthorizationSearchResponseE =new PriorAuthorizationSearchResponseE();
				priorAuthorizationSearchResponse = new PriorAuthorizationSearchResponse();
				priorAuthorizationSearchResponse.set_return(priorAuthHeader);
				priorAuthorizationSearchResponseE.setPriorAuthorizationSearchResponse(priorAuthorizationSearchResponse);
			}else{
				// Invoke the service
	            priorAuthorizationSearchResponseE = priorAuthHeaderServiceStub.priorAuthorizationSearch(priorAuthorizationSearchE);
	            priorAuthorizationSearchResponse = priorAuthorizationSearchResponseE.getPriorAuthorizationSearchResponse();
			}
           
            // Fetching the data from the response object
            PriorAuthHeaderDTO[] priorAuthHeaderDTOResponse = priorAuthorizationSearchResponse.get_return().getPriorAuthHeaderDTO();
            log.info("priorAuthHeaderDTOResponse : " + priorAuthHeaderDTOResponse);

            if ((priorAuthHeaderDTOResponse[0].getMemberNumber() != null) && !(priorAuthHeaderDTOResponse[0].getMemberNumber().equals(""))) {
                priorAuthHeaderDTOResponse[0].getPriorAuthSubmissionStatusCode();

                // Use the first record to get the member details
                IDCardNumber = priorAuthHeaderDTOResponse[0].getIdCardNumber();
				String memberNumber = priorAuthHeaderDTOResponse[0].getMemberNumber().trim();
				int authorizationKeyResponse = priorAuthHeaderDTOResponse[0].getPriorAuthHeaderKey();
    		    String healthPlanId = ParamUtil.getString(request, "healthPlan");

    		    log.info("IDCardNumber :" + IDCardNumber);
				log.info("memberNumber :" + memberNumber);
				log.info("authorizationKeyResponse :" + authorizationKeyResponse);
				log.info("priorAuthHeaderDTOResponse : "+ priorAuthHeaderDTOResponse[0].getPriorAuthHeaderKey());
            	log.info("healthPlanId: " + healthPlanId);
 
            	MemberSearchBusinessDelegateVo memberSearchBussinessDelegateVo = MemberSearchBusinessDelegateVo.getMemberSearchBusinessDelegateVo();

                if (searchType.equals("number")) {
                	log.info("Find member using authorization number");
                	// Invoking the member service
                	details = memberSearchBussinessDelegateVo.findMember(IDCardNumber, authorizationSearchFO.getAuthorizationNumber(), healthPlanId);
                } else {
                	String healthPlan = authorizationSearchFO.getHealthPlan();
                	log.info("Find member using id card number and/or first name, last name, and dob");
                    log.info("IDCardNumber :" + IDCardNumber);
                    log.info("healthPlan :" + healthPlan);
                    log.info("memberFirstName :" + memberFirstName);
                    log.info("memberLastName :" + memberLastName);
                    log.info("memberDob :" + memberDob);
                    log.info("serviceDate :" + serviceDate);
                    log.info("authType :" + authType);
 
					// Process the records
                	details = memberSearchBussinessDelegateVo.findMember(priorAuthHeaderDTOResponse, IDCardNumber, memberFirstName, memberLastName, memberDob, healthPlanId, authType);
               }

                if (details == null || details.isEmpty()) {

    		    	// Display the error message in the view page when there is no authorization is found
    		    	response.setRenderParameter("message", MemberSearchConstants.NO_RECORDS_ERROR);
                	httpSession.removeAttribute("detailsAuth");
                    
                    // Re-directing to the view page
                    response.setRenderParameter("mvcPath", mvcPathStr);
                    log.info("No records");
    		    } else if (details != null && !details.isEmpty() && details.size() > 0) {
					if ((!(details.get(0).getMpi().trim().isEmpty()) && ((details.get(0).getMpi()) != null))) {
					    log.info("details object not null and MPI exists");
		            	httpSession.setAttribute("detailsAuth", details);
					    log.info("Valid details : " + details);

		            	// Set the DOB to indicate the portlet has been run.
                        portletSession.setAttribute("memberDob", "13/31/1900", PortletSession.APPLICATION_SCOPE);
					} else {

	    		    	// Display the error message in the view page when there is no authorization is found
	                    response.setRenderParameter("message", MemberSearchConstants.NO_RECORDS_ERROR);
	                	httpSession.removeAttribute("detailsAuth");
	                    
	                    // Re-directing to the view page
	                    response.setRenderParameter("mvcPath", mvcPathStr);
	                    log.info("No records");
					}
                } else {

    		    	// Display the error message in the view page when there is no authorization is found
                    response.setRenderParameter("message", MemberSearchConstants.NO_RECORDS_ERROR);
                	httpSession.removeAttribute("detailsAuth");
                    
                    // Re-directing to the view page
                    response.setRenderParameter("mvcPath", mvcPathStr);
                    log.info("No records");
                }
            } else {

		    	// Display the error message in the view page when there is no authorization is found
                response.setRenderParameter("message", MemberSearchConstants.NO_RECORDS_ERROR);
            	httpSession.removeAttribute("detailsAuth");
                
                // Re-directing to the view page
                response.setRenderParameter("mvcPath", mvcPathStr);
                log.info("No records");
            }
        } catch (IOException ioe) {
            log.info("AuthorizationSearch IOException");
            if (ioe.getMessage().equals("No Records")) {
            	log.warn("No Records in AuthorizationSearch",ioe);
                log.info("AuthorizationSearch No Records");
            	httpSession.removeAttribute("detailsAuth");

		    	// Display the error message in the view page when there is no authorization is found
                response.setRenderParameter("message", MemberSearchConstants.NO_RECORDS_ERROR);
                
                // Re-directing to the view page
                response.setRenderParameter("mvcPath", mvcPathStr);
            } else {
                log.info("AuthorizationSearch Socket Time Out");
                log.warn("AuthorizationSearch Socket Time Out",ioe);
               
                /* When the integration server is down to display the error message */
                response.setRenderParameter("message", MemberSearchConstants.SYSTEM_ADMIN_ERROR);
            	httpSession.removeAttribute("detailsAuth");
                
                // Re-directing to the view page
                response.setRenderParameter("mvcPath", mvcPathStr);
            }
        } catch (NullPointerException noResponse) {
        	log.error("NullPointerException in authSearchDetails method",noResponse);
            response.setRenderParameter("message", MemberSearchConstants.NO_RECORDS_ERROR);
        	httpSession.removeAttribute("detailsAuth");
             
        	// Re-directing to the view page
            response.setRenderParameter("mvcPath", mvcPathStr);
        } catch (CustomException exception) {
        	log.warn("CustomException in authSearchDetails method",exception);
		    log.info("customException ---> " + exception.getMessage());
		    
		    // To display the error message based on the response code from the cds response
		    response.setRenderParameter("message", exception.getMessage());
        	httpSession.removeAttribute("detailsAuth");
	        
	        // Re-directing to the view page
		    response.setRenderParameter("mvcPath", mvcPathStr);
		} catch (Exception exception) {
			log.error("Exception in authSearchDetails method",exception);
            if (exception.getMessage().contains("Index: 0, Size: 0")) {
				response.setRenderParameter("message", MemberSearchConstants.NO_RECORDS_ERROR);
			} else {
				response.setRenderParameter("message", MemberSearchConstants.SYSTEM_ADMIN_ERROR);
			}
        	httpSession.removeAttribute("detailsAuth");
            
            // Re-directing to the view page
            response.setRenderParameter("mvcPath", mvcPathStr);
        }
    	log.info("Processing Search Action for Authorization Search Exit");
    }
    
    
    /**
     * whenever the search button  has been clicked the authSearchDetails method will be executed 
     * In this method we will fetch the member details and set in session 
     * It will be redirect to the avalon-prior-authorization-portlet.    (Used for resourceURL)
    */
    @ResourceMapping(value="authInfoSearchform")
    public void authInfoSearchform(ResourceRequest request, 
    		                       ResourceResponse response,
    		                       @ModelAttribute("authorizationSearchFO") AuthorizationSearchFO authorizationSearchFO)
    		throws IOException, WindowStateException, PortletModeException {

    	log.info("Processing Search Action for Authorization Search Enter");
 
    	HttpSession httpSession = PortalUtil.getHttpServletRequest(request).getSession();
    	PortletSession portletSession = request.getPortletSession();
        String searchType = authorizationSearchFO.getSearchType();
        log.info("searchType : " + searchType);
        if (searchType.equals("number")) {
            log.info("authNumber : " + authorizationSearchFO.getAuthorizationNumber());
        } else {
            log.info("ID Card Number : " + authorizationSearchFO.getAuthorizationNumber());
            log.info("healthPlan : " + authorizationSearchFO.getHealthPlan());
            log.info("firstName : " + authorizationSearchFO.getFirstName());
            log.info("lastName : " + authorizationSearchFO.getLastName());
            log.info("dateOfBirth : " + authorizationSearchFO.getDateOfBirth());
            log.info("serviceDate : " + authorizationSearchFO.getDateOfService());
        }
        Map<Integer, MemberDetailsFo> details = null;
        ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
		String authInfo = null;
        try {

        	// Creating the stub object to invoke the authorization search service
        	PriorAuthHeaderServiceStub priorAuthHeaderServiceStub = new PriorAuthHeaderServiceStub();
            PriorAuthorizationSearchE priorAuthorizationSearchE = new PriorAuthorizationSearchE();

            PriorAuthorizationSearch priorAuthorizationSearch = new PriorAuthorizationSearch();
            PriorAuthHeader priorAuthHeader = new PriorAuthHeader();
            PriorAuthHeaderDTO[] priorAuthHeaderDTO = new PriorAuthHeaderDTO[1];
            priorAuthHeaderDTO[0] = new PriorAuthHeaderDTO();

            // Set the operation type for the prior auth search
            priorAuthHeader.setOperationType("AuthorizationSearch");
           
            String IDCardNumber = null;
        	String memberFirstName = null;
        	String memberLastName = null;
        	String memberDob = null;
        	String serviceDate = null;
        	String authType = null;
            if (searchType.equals("number")) {
            	priorAuthHeaderDTO[0].setPriorAuthNumber(authorizationSearchFO.getAuthorizationNumber());
            } else {
            	HttpServletRequest httpRequest = PortalUtil.getHttpServletRequest(request);
            	String browserStr = getBrowser(httpRequest);

                IDCardNumber = authorizationSearchFO.getMemberId();
                log.info("IDCardNumber : " + IDCardNumber);
            	priorAuthHeaderDTO[0].setIdCardNumber(IDCardNumber);
            	
            	// If the First Name is not empty, then set the first name, last name, and date of birth to be included in the search.
            	memberFirstName = authorizationSearchFO.getFirstName();
                log.info("memberFirstName : " + memberFirstName);
            	priorAuthHeaderDTO[0].setPriorAuthPatientFirstName(memberFirstName);

            	memberLastName = authorizationSearchFO.getLastName();
                log.info("memberFirstName : " + memberLastName);
            	priorAuthHeaderDTO[0].setPriorAuthPatientLastName(memberLastName);

            	memberDob = authorizationSearchFO.getDateOfBirth();
    			if (browserStr.equals("IE") || browserStr.equals("Firefox") || browserStr.equals("unknown")) {
    				if (memberDob.equals("mm/dd/yyyy")) {
    					memberDob = "";
    				}
    			}
                log.info("memberDob : " + memberDob);
            	priorAuthHeaderDTO[0].setPriorAuthPatientBirthDate(memberDob);

            	serviceDate = authorizationSearchFO.getDateOfService();
    			if (browserStr.equals("IE") || browserStr.equals("Firefox") || browserStr.equals("unknown")) {
    				if (serviceDate.equals("mm/dd/yyyy")) {
    					serviceDate = "";
    				}
    			}
	            log.info("serviceDate : " + serviceDate);
	            if (serviceDate.length() > 0) {
	            	priorAuthHeaderDTO[0].setPriorAuthServiceDate(serviceDate);
	            }
	            
	            authType = ParamUtil.getString(request, "authType");
	            log.info("authType : " + authType);
	            authorizationSearchFO.setAuthType(authType);
            }
            
            // SP - Just a heads up, these are actually AppAccess fields. We are transferring them as AppCreate fields so that the WSDL doesn't need to be changed pointlessly.
            priorAuthHeaderDTO[0].setAppCreateUserId(themeDisplay.getUser().getEmailAddress());
            priorAuthHeaderDTO[0].setAppCreateDatetime(new java.text.SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new java.util.Date()));

            priorAuthHeader.setPriorAuthHeaderDTO(priorAuthHeaderDTO);
            priorAuthorizationSearch.setPriorAuthorizationSearch(priorAuthHeader);

            priorAuthorizationSearchE.setPriorAuthorizationSearch(priorAuthorizationSearch);
            PriorAuthorizationSearchResponseE priorAuthorizationSearchResponseE;

            PriorAuthorizationSearchResponse priorAuthorizationSearchResponse =null;
            //TODO: Remove mock service code and condition. Don't remove else condition code
			if(GetterUtil.getBoolean(com.liferay.portal.kernel.util.PropsUtil.get(MemberSearchConstants.ENABLED_MOCK_SERVICE))){
				 //Success Mock Code
				if (searchType.equals("number")) {
		            priorAuthorizationSearchResponse = MemberSearchBusinessDelegateVo.priorAuthorizationSearchResponseSuccessMock(authorizationSearchFO.getAuthorizationNumber(),"0");
				} else {
		        	priorAuthorizationSearchResponse = MemberSearchBusinessDelegateVo.priorAuthorizationSearchResponseSuccessMock("0",authorizationSearchFO.getMemberId());
				}
			}else{
				 // Invoke the service
				priorAuthorizationSearchResponseE = priorAuthHeaderServiceStub.priorAuthorizationSearch(priorAuthorizationSearchE);
	            priorAuthorizationSearchResponse = priorAuthorizationSearchResponseE.getPriorAuthorizationSearchResponse();
			}
            
            // Fetching the data from the response object
            PriorAuthHeaderDTO[] priorAuthHeaderDTOResponse = priorAuthorizationSearchResponse.get_return().getPriorAuthHeaderDTO();
            log.info("priorAuthHeaderDTOResponse : " + priorAuthHeaderDTOResponse);

            if ((priorAuthHeaderDTOResponse[0].getMemberNumber() != null) && !(priorAuthHeaderDTOResponse[0].getMemberNumber().equals(""))) {
                priorAuthHeaderDTOResponse[0].getPriorAuthSubmissionStatusCode();

                // Use the first record to get the member details
                IDCardNumber = priorAuthHeaderDTOResponse[0].getIdCardNumber();
				String memberNumber = priorAuthHeaderDTOResponse[0].getMemberNumber().trim();
				int authorizationKeyResponse = priorAuthHeaderDTOResponse[0].getPriorAuthHeaderKey();
    		    String healthPlanId = ParamUtil.getString(request, "healthPlan");

    		    log.info("IDCardNumber :" + IDCardNumber);
				log.info("memberNumber :" + memberNumber);
				log.info("authorizationKeyResponse :" + authorizationKeyResponse);
				log.info("priorAuthHeaderDTOResponse : "+ priorAuthHeaderDTOResponse[0].getPriorAuthHeaderKey());
            	log.info("healthPlanId: " + healthPlanId);
 
            	MemberSearchBusinessDelegateVo memberSearchBussinessDelegateVo = MemberSearchBusinessDelegateVo.getMemberSearchBusinessDelegateVo();
            	
                if (searchType.equals("number")) {
                	log.info("Find member using authorization number");

	                // Invoking the member service
                	details = memberSearchBussinessDelegateVo.findMember(IDCardNumber, authorizationSearchFO.getAuthorizationNumber(), healthPlanId);
                } else {
                	String healthPlan = authorizationSearchFO.getHealthPlan();
                	log.info("Find member using id card number and/or first name, last name, and dob");
                    log.info("IDCardNumber :" + IDCardNumber);
                    log.info("healthPlan :" + healthPlan);
                    log.info("memberFirstName :" + memberFirstName);
                    log.info("memberLastName :" + memberLastName);
                    log.info("memberDob :" + memberDob);
                    log.info("serviceDate :" + serviceDate);
                    log.info("authType :" + authType);
 
					// Process the records
                	details = memberSearchBussinessDelegateVo.findMember(priorAuthHeaderDTOResponse, IDCardNumber, memberFirstName, memberLastName, memberDob, healthPlanId, authType);
                }

                if (details == null || details.isEmpty()) {
                	// Display the error message in the view page when there is no authorization is found
                    log.info("No records");
                	httpSession.removeAttribute("detailsAuth");
                    authInfo = createErrorMessage(MemberSearchConstants.NO_RECORDS_ERROR);
    				response.getWriter().write(authInfo.toString());	// Setting the coverted json response into response object
     		    } else if (details != null && !details.isEmpty() && details.size() > 0) {
					if ((!(details.get(0).getMpi().trim().isEmpty()) && ((details.get(0).getMpi()) != null))) {
						
						log.info("details object not null and MPI exists");
		            	httpSession.setAttribute("detailsAuth", details);
					    log.info("Valid details : " + details);

						// Convert the map to a List
						Gson gson = new Gson(); 
						List<MemberDetailsFo> list = new ArrayList<MemberDetailsFo>();
						for (Integer key : details.keySet()) {
							MemberDetailsFo nextPair = details.get(key);
							nextPair.setKeyCode(Integer.toString(key));
							list.add(nextPair);
						} 
						authInfo = gson.toJson(list);						// Coverting the list into Json Response 
						response.getWriter().write(authInfo.toString());	// Setting the coverted json response into response object
						// Set the DOB to indicate the portlet has been run.
                        portletSession.setAttribute("memberDob", "13/31/1900", PortletSession.APPLICATION_SCOPE);
					} else {
						// Display the error message in the view page when there is no authorization is found
	                    log.info("No records");
	                	httpSession.removeAttribute("detailsAuth");
	                	authInfo = createErrorMessage(MemberSearchConstants.NO_RECORDS_ERROR);
	    				response.getWriter().write(authInfo.toString());	// Setting the coverted json response into response object
					}
                } else {
                	// Display the error message in the view page when there is no authorization is found
                    log.info("No records");
                	httpSession.removeAttribute("detailsAuth");
                	authInfo = createErrorMessage(MemberSearchConstants.NO_RECORDS_ERROR);
       				response.getWriter().write(authInfo.toString());	// Setting the coverted json response into response object
                }
            } else {
            	// Display the error message in the view page when there is no authorization is found
                log.info("No records");
            	httpSession.removeAttribute("detailsAuth");
            	authInfo = createErrorMessage(MemberSearchConstants.NO_RECORDS_ERROR);
   				response.getWriter().write(authInfo.toString());	// Setting the coverted json response into response object
            }
        } catch (IOException ioe) {
            log.info("AuthorizationSearch IOException");
            if (ioe.getMessage().equals("No Records")) {
            	log.warn(" No Records AuthorizationSearch",ioe);
                log.info("AuthorizationSearch No Records");
            	httpSession.removeAttribute("detailsAuth");

		    	// Display the error message in the view page when there is no authorization is found
            	authInfo = createErrorMessage(MemberSearchConstants.NO_RECORDS_ERROR);
   				response.getWriter().write(authInfo.toString());	// Setting the coverted json response into response object
            } else {
                log.info("AuthorizationSearch Socket Time Out");
                log.warn("AuthorizationSearch Socket Time Out",ioe);
                httpSession.removeAttribute("detailsAuth");

                // When the integration server is down to display the error message
            	authInfo = createErrorMessage(MemberSearchConstants.SYSTEM_ADMIN_ERROR);
   				response.getWriter().write(authInfo.toString());	// Setting the coverted json response into response object
            }
        } catch (NullPointerException noResponse) {
        	log.error("NullPointerException in authInfoSearchform method",noResponse);
            httpSession.removeAttribute("detailsAuth");
        	authInfo = createErrorMessage(MemberSearchConstants.NO_RECORDS_ERROR);
			response.getWriter().write(authInfo.toString());	// Setting the coverted json response into response object
       } catch (CustomException exception) {
    	    log.error("CustomException in authInfoSearchform method",exception);
		    log.info("customException ---> " + exception.getMessage());
        	httpSession.removeAttribute("detailsAuth");
		    
		    // To display the error message based on the response code from the cds response
        	authInfo = createErrorMessage(exception.getMessage());
			response.getWriter().write(authInfo.toString());	// Setting the coverted json response into response object
		} catch (Exception exception) {
			log.error("Exception in authInfoSearchform method",exception);
            httpSession.removeAttribute("detailsAuth");
            if (exception.getMessage().contains("Index: 0, Size: 0")) {
	        	authInfo = createErrorMessage(MemberSearchConstants.NO_RECORDS_ERROR);
			} else {
	        	authInfo = createErrorMessage(MemberSearchConstants.SYSTEM_ADMIN_ERROR);
			}
			response.getWriter().write(authInfo.toString());	// Setting the coverted json response into response object
        }
    	log.info("Processing Search Action for Authorization Search Exit");
    }


    /**
     * Whenever user click on hyperlink of the results page getAuthorizationDetails method will be execute
     */
    @ActionMapping(params = "action=getAuthorizationDetails")
    public void getAuthorizationDetails(ActionRequest actionRequest,
	                                    ActionResponse actionResponse) throws IOException, PortalException {

		log.info("Processing getAuthorizationDetails Action for Authorization Search Enter");
		HttpServletRequest httpRequest = PortalUtil.getHttpServletRequest(actionRequest);
		HttpSession httpSession = PortalUtil.getHttpServletRequest(actionRequest).getSession();
		ThemeDisplay themeDisplay1 = (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
		Layout layout;
		PortletSession portletSession = actionRequest.getPortletSession();
		try {
		    String authorizationDetailsKeyCode = ParamUtil.getString(actionRequest, "authorizationDetailsKey");
		    log.info("authorizationDetailsKey : " + authorizationDetailsKeyCode);
		    Map<Integer, MemberDetailsFo> maplist = (Map<Integer, MemberDetailsFo>) httpSession.getAttribute("detailsAuth");
		    log.info("maplist : " + maplist);
		    MemberDetailsFo memberDetailsFo = null;
		    if ((maplist != null) && (maplist.size() > 0)) {
				memberDetailsFo = maplist.get(Integer.parseInt(authorizationDetailsKeyCode));
				
				log.info("memberDetailsFo : " + memberDetailsFo);
				log.info("memberDetailsFo.setMemberNumber ------> " + memberDetailsFo.getMemberNumber());
				if ((memberDetailsFo.getMemberNumber() != null) && !(memberDetailsFo.getMemberNumber().equals(""))) {
				    httpSession.setAttribute("memberDetailsFo", memberDetailsFo);
				    log.info("Member ID : " + memberDetailsFo.getMemberId());
                    log.info("Member First Name : " + memberDetailsFo.getMemberFirstName());
                    log.info("Member Middle Name : " + memberDetailsFo.getMemberMiddleName());
                    log.info("Member Last Name : " + memberDetailsFo.getMemberLastName());
                    log.info("Member Suffix Name : " + memberDetailsFo.getMemberSuffixName());
                    log.info("Member DOB : " + memberDetailsFo.getMemberDob());
                    log.info("Member Gender : " + memberDetailsFo.getGender());
                    log.info("Member Address Line 1 : " + memberDetailsFo.getMemberAddressLine1());
                    log.info("Member Address Line 2 : " + memberDetailsFo.getMemberAddressLine2());
                    log.info("Member City : " + memberDetailsFo.getMemberCity());
                    log.info("Member State : " + memberDetailsFo.getMemberStateCode());
                    log.info("Member Zip Code : " + memberDetailsFo.getMemberZipcode());
                    log.info("Member Phone Number : " + memberDetailsFo.getMemberPhoneNumber());
                    
				    log.info("Business Sector Code : " + memberDetailsFo.getBusinessSectorCode());
					log.info("Business Sector Description : " + memberDetailsFo.getBusinessSectorDescription());
					log.info("Business Segment Code : " + memberDetailsFo.getBusinessSegmentCode());
					log.info("Business Segment Description : " + memberDetailsFo.getBusinessSegmentDescription());

					log.info("Subscriber Name : " + memberDetailsFo.getSubscriberName());

				    // Setting the member details in session variables that are used by the prior auth procedure page
                    portletSession.setAttribute("memberDob", memberDetailsFo.getMemberDob(), PortletSession.APPLICATION_SCOPE);
                    portletSession.setAttribute("memberId", memberDetailsFo.getMemberId(), PortletSession.APPLICATION_SCOPE);
                    portletSession.setAttribute("authorizationNumber", memberDetailsFo.getPriorAuthNumber(), PortletSession.APPLICATION_SCOPE);
                    portletSession.setAttribute("authorizationStatus", memberDetailsFo.getPriorAuthStatusCode(), PortletSession.APPLICATION_SCOPE);
                    portletSession.setAttribute("authorizationKey",Long.valueOf(memberDetailsFo.getAuthorizationKeyResponse()), PortletSession.APPLICATION_SCOPE);
                    portletSession.setAttribute("healthPlan", memberDetailsFo.getHealthPlan(), PortletSession.APPLICATION_SCOPE);
                    portletSession.setAttribute("mpi", memberDetailsFo.getMpi(), PortletSession.APPLICATION_SCOPE);
                    portletSession.setAttribute("memberName", memberDetailsFo.getMemberFirstName() + " "+ memberDetailsFo.getMemberLastName(), PortletSession.APPLICATION_SCOPE);
                    portletSession.setAttribute("membergroupId", memberDetailsFo.getGroupId(), PortletSession.APPLICATION_SCOPE);
                    portletSession.setAttribute("authSubmissionStatus", memberDetailsFo.getAuthSubmissionStatusCode(), PortletSession.APPLICATION_SCOPE);
    			    portletSession.setAttribute("memberGender", memberDetailsFo.getDisplayGender(), PortletSession.APPLICATION_SCOPE);
    			    portletSession.setAttribute("memberAddressLine1", memberDetailsFo.getMemberAddressLine1(), PortletSession.APPLICATION_SCOPE);
    			    portletSession.setAttribute("memberAddressLine2", memberDetailsFo.getMemberAddressLine2(), PortletSession.APPLICATION_SCOPE);
    			    portletSession.setAttribute("memberCity", memberDetailsFo.getMemberCity(), PortletSession.APPLICATION_SCOPE);
    			    portletSession.setAttribute("memberStateCode", memberDetailsFo.getMemberStateCode(), PortletSession.APPLICATION_SCOPE);
    			    portletSession.setAttribute("memberZipcode", memberDetailsFo.getMemberZipcode(), PortletSession.APPLICATION_SCOPE);
    			    portletSession.setAttribute("memberPhoneNumber", memberDetailsFo.getMemberPhoneNumber(), PortletSession.APPLICATION_SCOPE);

    				// Set the additional attributes saved by the prior authorization pages
    				portletSession.setAttribute("memberFirstName", memberDetailsFo.getMemberFirstName(), PortletSession.APPLICATION_SCOPE);
    				portletSession.setAttribute("memberMiddleName", memberDetailsFo.getMemberMiddleName(), PortletSession.APPLICATION_SCOPE);
    				portletSession.setAttribute("memberLastName", memberDetailsFo.getMemberLastName(), PortletSession.APPLICATION_SCOPE);
    				portletSession.setAttribute("memberSuffixName", memberDetailsFo.getMemberSuffixName(), PortletSession.APPLICATION_SCOPE);
    				portletSession.setAttribute("businessSectorCode", memberDetailsFo.getBusinessSectorCode(), PortletSession.APPLICATION_SCOPE);
    				portletSession.setAttribute("businessSectorDescription", memberDetailsFo.getBusinessSectorDescription(), PortletSession.APPLICATION_SCOPE);
    				portletSession.setAttribute("businessSegmentCode", memberDetailsFo.getBusinessSegmentCode(), PortletSession.APPLICATION_SCOPE);
    				portletSession.setAttribute("businessSegmentDescription", memberDetailsFo.getBusinessSegmentDescription(), PortletSession.APPLICATION_SCOPE);

    				// Set the attribute for the subscriber name in the header
    				portletSession.setAttribute("subscriberName", memberDetailsFo.getSubscriberName(), PortletSession.APPLICATION_SCOPE);
    				
    			    String url = "";
    			    String procedureInfoPage = "";
    			    
    			    // Test for Post Service Request authorization number
    				if (memberDetailsFo.getPriorAuthNumber().contains("PR")) {
    					url = PortletProps.get("post-service-page-url");
    					procedureInfoPage = com.liferay.util.portlet.PortletProps.get("post-service-procedure-info-page");
    				} else {
    					url = com.liferay.util.portlet.PortletProps.get("prior-auth-page-url");
    					procedureInfoPage = com.liferay.util.portlet.PortletProps.get("procedure-info-page");
    				}
					boolean privateFlag = true;   // True for a private page
    				layout = LayoutLocalServiceUtil.getFriendlyURLLayout(themeDisplay1.getLayout().getGroupId(), privateFlag, url);
    				PortletURL portletURL = PortletURLFactoryUtil.create(httpRequest, procedureInfoPage, layout.getPlid(), PortletRequest.RENDER_PHASE);

				    portletURL.setWindowState(WindowState.MAXIMIZED);
				    portletURL.setPortletMode(PortletMode.VIEW);
				    log.info("To String :" + portletURL.toString());
				    actionResponse.sendRedirect(portletURL.toString());
				} else {
				    actionRequest.setAttribute("message", MemberSearchConstants.INVALID_MEMBER_DATA);
				    actionResponse.setRenderParameter("mvcPath", "/html/jsps/memberDetails.jsp");
				}
		    } else {
				actionRequest.setAttribute("message", MemberSearchConstants.INVALID_MEMBER_DATA);
				actionResponse.setRenderParameter("mvcPath", "/html/jsps/memberDetails.jsp");
		    }
		} catch (com.liferay.portal.kernel.exception.SystemException e) {
			log.error("SystemException in getAuthorizationDetails method",e);

		} catch (PortletModeException e) {
			log.error("PortletModeException in getAuthorizationDetails method",e);

		} catch (WindowStateException e) {
			log.error("WindowStateException in getAuthorizationDetails method",e);

		}
		log.info("Processing getAuthorizationDetails Action for Authorization Search Enter");
	}
    
    /**
     * Whenever user click on cancel button cancelPage method will be executed
     */
    @ActionMapping(params = "action=cancelPage")
    public void cancelPage(ActionRequest actionRequest,
	                       ActionResponse actionResponse) {

    	log.info("Processing Cancel Action for Authorization Search Enter");
	    String userName = null;
	    List usersList = null;
		try {
		    User user = PortalUtil.getUser((PortletRequest) actionRequest);
		    if (user != null) {
			    usersList = user.getUserGroups();
		    }
		} catch (PortalException e) {
		    log.error("PortalException in cancelPage method", (Throwable) e);
		} catch (SystemException e) {
			log.error("SystemException in cancelPage method", (Throwable) e);
		}
	    ArrayList<String> groupNames = new ArrayList<String>();
	    if (usersList != null) {
	    	int length = usersList.size();
		    for (int i = 0; i < length; ++i) {
		    	
		    	// Ignore the groups that do not have a home page
				if (!((UserGroup)usersList.get(i)).getName().equalsIgnoreCase("CDSUsers") &&
					!((UserGroup)usersList.get(i)).getName().equalsIgnoreCase("Everyone") &&
					!((UserGroup)usersList.get(i)).getName().equalsIgnoreCase("ManagedUsers") &&
					!((UserGroup)usersList.get(i)).getName().equalsIgnoreCase("Multi-Factor") &&
					!((UserGroup)usersList.get(i)).getName().equalsIgnoreCase("PortalAdmin") &&
					!((UserGroup)usersList.get(i)).getName().equalsIgnoreCase("UMGroup")) {
					userName = ((UserGroup) usersList.get(i)).getName();
					groupNames.add(userName);
				}
		    }
	    }

	    // Show the initial page for future selections
        HttpSession httpSession = PortalUtil.getHttpServletRequest(actionRequest).getSession();
        httpSession.removeAttribute("details");
    	
    	HttpServletRequest request = PortalUtil.getHttpServletRequest((PortletRequest) actionRequest);
	    String path = PortalUtil.getCurrentCompleteURL((HttpServletRequest) request);
	    StringBuilder output = new StringBuilder();
	    int count = 0;
	    char[] ch = path.toCharArray();
	    for (int i2 = 0; i2 < ch.length; ++i2) {
			if (ch[i2] == '/') {
			    ++count;
			}
			if (count >= 3)
			    continue;
			output = output.append(ch[i2]);
	    }

	    String landingPage = null;
	    if (usersList == null) {
			
			// Set to the default landing page since the user does not belong to any groups
			landingPage = "/web/guest/home";
	    } else {
	    	

	    	// Get the landing page based on the user's group
			landingPage = com.liferay.portal.kernel.util.PropsUtil.get("default.landing.page.path[" + userName  + "]"); 
			
			// If the user's group is not found use the defaule landing page
			if (landingPage == null) {
				landingPage = com.liferay.portal.kernel.util.PropsUtil.get("default.landing.page.path");
				if (landingPage == null) {
					
					// Set to the default landing page since it is not defined in the properties file
					landingPage = "/web/guest/home";
				}
			}
	    }
	    String pathRedirect = null;
		pathRedirect = output + landingPage;
		log.info(userName + " " + pathRedirect);
		try {
		    actionResponse.sendRedirect(pathRedirect);
		} catch (IOException e) {
			log.error("Exception during redirection of page", e);
		}
    	log.info("Processing Cancel Action for Authorization Search Exit");
    }

    /**
     * Default method
     */
    @ActionMapping
    public void providerHandler() {
        log.info("search handler");
    }
    
	private String getBrowser(HttpServletRequest httpRequest) {
		String userAgent = httpRequest.getHeader("User-Agent");
		String rtnValue = "unknown";
		
		if (userAgent.contains("Opera") || userAgent.contains("OPR")) {
			rtnValue = "Opera";
		} else if (userAgent.contains("Chrome")) {
			rtnValue = "Chrome";
		} else if (userAgent.contains("Safari")) {
			rtnValue = "Safari";
		} else if (userAgent.contains("MSIE") || (userAgent.contains("Trident") && userAgent.contains("rv:"))) { 
			rtnValue = "IE";
		} else if (userAgent.contains("Firefox"))  {
			rtnValue = "Firefox";
		} else {
			rtnValue = "unknown";
		}

		return rtnValue;
	}
    
    /**
     * Create a json message for an error
     */
	private String createErrorMessage(String errorMsg) {
		Gson gson = new Gson(); 
		
		List list = new ArrayList();
		Map <String, Object> authSearch = new HashMap<String, Object>();
		authSearch.put(MemberSearchConstants.ERROR, errorMsg);
		list.add(authSearch);
		String authInfo = gson.toJson(list);
		
		return authInfo;
	}
	
	
	
}
