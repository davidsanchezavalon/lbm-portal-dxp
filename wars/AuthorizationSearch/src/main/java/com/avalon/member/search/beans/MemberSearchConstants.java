package com.avalon.member.search.beans;

/**
 * Description
 *		This file contain the constants for the Authorization Search portlet for Prior Auth Search.
 *
 * CHANGE History
 * 		Version 1.0
 *			Initial version.
 *		Version 1.2			12/12/2017
 *			Changed REASON_FOR_INQUIRY to P
 *  	Version 1.1			03/07/2017
 *  		Added a comstant for the properties file.
 *
 */

public interface MemberSearchConstants {
	public static final String PROPERTIES_FILE ="lbm-common.properties";

    public static final String REASON_FOR_INQUIRY = "P"; 
    public static final String HEALTH_PLAN_ID_TYPE = "PI";
 
	// Authorization search types
	public static final String AUTH_ALL = "All";
	public static final String AUTH_PA  = "PA";
	public static final String AUTH_PSR = "PSR";
	public static final String AUTH_PA_DESC  = "Prior Authorization";
	public static final String AUTH_PSR_DESC = "Post Service Review";
	
	// List Keys
	public static final String AUTH_NUMBER = "priorAuthNumber";
	public static final String AUTH_STATUS_CODE = "priorAuthStatusCode";
	public static final String CREATE_DATETIME = "appCreateDatetime";
	public static final String AUTH_BEGIN_SERVICE_DATE = "priorAuthBeginServiceDate";
	public static final String MEMBER_ID = "memberId";
	public static final String FIRST_NAME = "memberFirstName";
	public static final String LAST_NAME = "memberLastName";
	public static final String DATE_OF_BIRTH = "memberDob";
	public static final String KEY_CODE = "keyCode";

    // Error messages
	public static final String ERROR = "error";
	public static final String MEMBER_SEARCH_ERROR = "Please enter First Name, Last Name, and Date of Birth.";
	public static final String AUTHNUMBER_ERROR = "Please enter valid  Authorization Number for Prior Auth or Post Service Review";
	public static final String DATEOFSERVICE_ERROR = "Service Date cannot be before 1/1/2016.";

	public static final String SYSTEM_ADMIN_ERROR = "Error while processing the request. Please contact system administrator";
	public static final String NO_RECORDS_ERROR = "No authorizations were found that match your search criteria. Please update the search and try again.";
	public static final String INVALID_MEMBER_DATA = "Invalid Member Data";
	
	//TODO: Remove below code
    public static final String ENABLED_MOCK_SERVICE = "enabled.mock.service";
}
