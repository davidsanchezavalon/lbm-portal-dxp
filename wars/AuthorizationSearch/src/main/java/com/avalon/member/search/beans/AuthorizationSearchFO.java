package com.avalon.member.search.beans;

import java.io.Serializable;

public class AuthorizationSearchFO implements Serializable {
	
    /**
     * To store the AuthorizationSearch fields
     */
    private static final long serialVersionUID = 8246097910683363742L;
	private String searchType;
	private String authorizationNumber;
	private String memberId;
	private String healthPlan;
	private String firstName;
	private String lastName;
	private String dateOfBirth;
	private String dateOfService;
	private String authType;

	/**
	 * @return the searchType
	 */
	public String getSearchType() {
		return searchType;
	}
	/**
	 * @param searchType the searchType to set
	 */
	public void setSearchType(String searchType) {
		this.searchType = searchType;
	}

	/**
	 * @return the authorizationNumber
	 */
	public String getAuthorizationNumber() {
		return authorizationNumber;
	}
	/**
	 * @param authorizationNumber the authorizationNumber to set
	 */
	public void setAuthorizationNumber(String authorizationNumber) {
		this.authorizationNumber = authorizationNumber;
	}

	/**
	 * @return the memberId
	 */
	public String getMemberId() {
		return memberId;
	}
	/**
	 * @param memberId the memberId to set
	 */
	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}

	/**
	 * @return the healthPlan
	 */
	public String getHealthPlan() {
		return healthPlan;
	}
	/**
	 * @param healthPlan the healthPlan to set
	 */
	public void setHealthPlan(String healthPlan) {
		this.healthPlan = healthPlan;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}
	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}
	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return the dateOfBirth
	 */
	public String getDateOfBirth() {
		return dateOfBirth;
	}
	/**
	 * @param dateOfBirth the dateOfBirth to set
	 */
	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	/**
	 * @return the dateOfService
	 */
	public String getDateOfService() {
		return dateOfService;
	}
	/**
	 * @param dateOfService the dateOfService to set
	 */
	public void setDateOfService(String dateOfService) {
		this.dateOfService = dateOfService;
	}

	/**
	 * @return the authType
	 */
	public String getAuthType() {
		return authType;
	}
	/**
	 * @param authType the authType to set
	 */
	public void setAuthType(String authType) {
		this.authType = authType;
	}
}
