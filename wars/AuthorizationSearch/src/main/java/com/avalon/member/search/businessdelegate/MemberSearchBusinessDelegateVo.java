package com.avalon.member.search.businessdelegate;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.rmi.RemoteException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.axis2.AxisFault;

/**
 * Description
 *		This methods in this file are called by the controller for the Prior Authorization Search portlet.
 *
 * CHANGE History
 * 		Version 1.0
 *			inital version.
 *		Version 1.2			12/12/2017
 *			Changed health plan ID to the ID in the GUI dropdown
 *			Changed REASON_FOR_INQUIRY to P
 *
 */

import com.avalon.esb.servicehelpers.PriorAuthImplServiceStub;
import com.avalon.esb.servicehelpers.PriorAuthImplServiceStub.Address;
import com.avalon.esb.servicehelpers.PriorAuthImplServiceStub.BusinessSectorSegment;
import com.avalon.esb.servicehelpers.PriorAuthImplServiceStub.ContactData;
import com.avalon.esb.servicehelpers.PriorAuthImplServiceStub.Coverage;
import com.avalon.esb.servicehelpers.PriorAuthImplServiceStub.CoverageList;
import com.avalon.esb.servicehelpers.PriorAuthImplServiceStub.GetMemberData;
import com.avalon.esb.servicehelpers.PriorAuthImplServiceStub.GetMemberDataResponse;
import com.avalon.esb.servicehelpers.PriorAuthImplServiceStub.Member;
import com.avalon.esb.servicehelpers.PriorAuthImplServiceStub.MemberDataRequest;
import com.avalon.esb.servicehelpers.PriorAuthImplServiceStub.MemberDataResponse;
import com.avalon.esb.servicehelpers.PriorAuthImplServiceStub.MemberDemographicsOutput;
import com.avalon.esb.servicehelpers.PriorAuthImplServiceStub.MemberList;
import com.avalon.esb.servicehelpers.PriorAuthImplServiceStub.MoreDataOptions;
import com.avalon.esb.servicehelpers.PriorAuthImplServiceStub.SearchCriteria;
import com.avalon.lbm.services.DAOExceptionException;
import com.avalon.lbm.services.PriorAuthHeaderServiceStub;
import com.avalon.lbm.services.PriorAuthHeaderServiceStub.PriorAuthHeader;
import com.avalon.lbm.services.PriorAuthHeaderServiceStub.PriorAuthHeaderDTO;
import com.avalon.lbm.services.PriorAuthHeaderServiceStub.PriorAuthorizationSearch;
import com.avalon.lbm.services.PriorAuthHeaderServiceStub.PriorAuthorizationSearchE;
import com.avalon.lbm.services.PriorAuthHeaderServiceStub.PriorAuthorizationSearchResponse;
import com.avalon.lbm.services.PriorAuthHeaderServiceStub.PriorAuthorizationSearchResponseE;
import com.avalon.member.search.beans.MemberDetailsFo;
import com.avalon.member.search.beans.MemberSearchConstants;
import com.avalon.member.search.customexception.CustomException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;

/**
 * Description
 *		This file contains utilities for the member search page.
 *
 * @author David Sanchez
 *
 * CHANGE History
 * 		Version 1.0
 * 			Initial version
 * 		Version 1.1					08/08/2018
 * 			Added HO LOE an subscriber name to the header.
 *  
 */

public class MemberSearchBusinessDelegateVo implements MemberSearchConstants {

    private static MemberSearchBusinessDelegateVo memberSearchBusinessDelegateVo = null;
    private static final Log log = LogFactoryUtil.getLog(MemberSearchBusinessDelegateVo.class.getName());
    private static Properties properties = null;
    private MemberSearchBusinessDelegateVo() {
    }

    static {

		if (properties == null) {
		    properties = new Properties();
		    memberSearchBusinessDelegateVo = new MemberSearchBusinessDelegateVo();
		    String responseCodePropertyPath = System.getProperty("LIFERAY_HOME");
		    String propertyLocation = responseCodePropertyPath + "/PAMessages_en_US.properties";
		    log.info("responseCodePropertyPath: " + responseCodePropertyPath);
			try {
			    properties.load(new FileInputStream(new File(propertyLocation)));
			} catch (FileNotFoundException e) {
				log.error("FileNotFoundException in static block",e);
			} catch (IOException e) {
				log.error("IOException in static block",e);
			}
	    }
	}
    
    
    public static MemberSearchBusinessDelegateVo getMemberSearchBusinessDelegateVo() {
    	return memberSearchBusinessDelegateVo;
    }
    

    /**
     * 
     * @param idCardNumber
     * @param authorizationNumber
     * @param healthPlanId
     * @return
     * @throws IOException
     * @throws NullPointerException
     * @throws CustomException
     * Find member service will be invoked based on the memberId
     * If the member is available it fetches the results 
     * Returning the results in the form of map object to the controller
     */
    public Map<Integer, MemberDetailsFo> findMember(String idCardNumber, 
    		                                        String authorizationNumber,
    		                                        String healthPlanId) throws IOException, NullPointerException, CustomException {

    	log.info("PriorAuthBusinessDelegateVo start findMember (by authorization number)");
    	
    	log.info("idCardNumber in auth search " +idCardNumber);
		Map<Integer, MemberDetailsFo> mapList = new HashMap<Integer, MemberDetailsFo>();
		try {
				
			// Creating the member service stub object
			PriorAuthImplServiceStub priorAuthImplServiceStub = new PriorAuthImplServiceStub();
			GetMemberData getMemberData = new GetMemberData();
			MemberDataRequest memberDataRequest = new MemberDataRequest();
			SearchCriteria searchCriteria = new SearchCriteria();
			searchCriteria.setReasonForInquiry(REASON_FOR_INQUIRY);
			searchCriteria.setRequestDate(null);
			searchCriteria.setRpn(" ");
			searchCriteria.setProductCode(" ");
			searchCriteria.setPlanCode(" ");
			searchCriteria.setHealthPlanId(healthPlanId);
			searchCriteria.setHealthPlanGroupId(" ");
			searchCriteria.setIdCardNumber(idCardNumber);
			searchCriteria.setHealthPlanIdType(HEALTH_PLAN_ID_TYPE); // PI
			searchCriteria.setSubscriberNumber(" ");
			memberDataRequest.setSearchCriteria(searchCriteria);    // Setting the required objects in the service request
			MoreDataOptions moreDataOptions = new MoreDataOptions();
			moreDataOptions.setRetrieveMemberList(true);
			moreDataOptions.setRetrieveCoverageData(false);
			memberDataRequest.setMoreDataOptions(moreDataOptions);
			getMemberData.setGetMemberData(memberDataRequest);
			log.info("Request Data : " + getMemberData.toString());
			GetMemberDataResponse getMemberDataResponse = null;
			 //TODO: Remove mock service code and condition. Don't remove else condition code
			if(GetterUtil.getBoolean(com.liferay.portal.kernel.util.PropsUtil.get(MemberSearchConstants.ENABLED_MOCK_SERVICE))){
				 //Success Mock Code
				getMemberDataResponse = getSuccessMockMemberData(getMemberData,idCardNumber);
			}else{
				getMemberDataResponse = priorAuthImplServiceStub.getMemberData(getMemberData);
			}
			
			boolean searchMemberFlag = true;
			String[] responseCodes = getMemberDataResponse.getGetMemberDataResponse().getResponseCode();
			for(String responseCode:responseCodes){
				if (responseCode.matches("000|952|953|954|955|956|957|958|959|960|961|962|963|964|990|991|992|993|994|995|996")) {
					if (searchMemberFlag) {
						// Set the flag to indicate the member search has been done
						searchMemberFlag = false;

						mapList = processMemberDataResponse(getMemberDataResponse, authorizationNumber, null, null, true);

						String coverageList = getMemberDataResponse.getGetMemberDataResponse().getCoverageList().getCoverageCount();
						log.info("coverageListCount: " + coverageList);
						Coverage[] coverage = getMemberDataResponse.getGetMemberDataResponse().getCoverageList().getCoverage();
						for (int j = 0; j < coverage.length; j++) {
							log.info("HealthPlanGroupId:  " + coverage[j].getHealthPlanGroupId());
							log.info("Coverage Begin date:  " + coverage[j].getCoverageBeginDate());
							log.info("Client Number: " + coverage[j].getClientNumber());
							log.info("getCesGroupNumber:  " + coverage[j].getCesGroupNumber());
							log.info("getCesGroupNumberDescription:  " + coverage[j].getCesGroupNumberDescription());
							log.info("getAmmsGroupNumber:  " + coverage[j].getAmmsGroupNumber());
							log.info("getBusinessSectorCd:  " + coverage[j].getBusinessSectorSegment().getBusinessSectorCd());
							log.info("getBusinessSectorDesc:  " + coverage[j].getBusinessSectorSegment().getBusinessSectorDesc());
							log.info("getBusinessSegmentCd:  " + coverage[j].getBusinessSectorSegment().getBusinessSegmentCd());
							log.info("getBusinessSegmentDesc:  " + coverage[j].getBusinessSectorSegment().getBusinessSegmentDesc());
						}
						log.info("PriorAuthBusinessDelegateVo End findMember (by authorization number)");
					}
				} else {
											
					// Set the flag to indicate the a error that stops the member search
					searchMemberFlag = false;

					String errorMessage = getResponseCodeMessages(responseCodes);
					throw new CustomException(errorMessage);
				}
			}
		}
		// Exception thrown when general network I/O error occurs
		catch (IOException ioe) {
			log.info("Check: " + (ioe instanceof org.apache.axis2.AxisFault));
			log.info("Network I/O error - " + ioe);
			if (ioe instanceof org.apache.axis2.AxisFault) {
			org.apache.axis2.AxisFault exception = (org.apache.axis2.AxisFault) ioe;
			if (exception.getMessage().equals("Read timed out")) {
				log.info("IOException Timed Out");
				throw new IOException("Your Request is Timed Out");/***Throwing the exception to the controller when the socket time out exception has been occured ***/
			} else {
				log.info("IOException No Records");
				throw new IOException("No Records");/***Throwing the exception to the controller when the no records exception has been occured***/
			}

			} else {
			log.info("IOException else block");
			throw new IOException("Your Request is Timed Out");
			}
		} catch (CustomException exception) {
			log.warn("MemberSearchBusinessDelegateVo Response Code Exception " + exception.getMessage());
			throw exception;
		} catch (Exception e) {
			log.info("PriorAuthBusiness Super Exception");
			log.error("Exception in findMember method",e);
		}
		
		return mapList;
    }
    
    /**
     * 
     * @param searchResponse
     * @param idCardNumber
     * @param memberFirstName
     * @param memberLastName
     * @param memberDob
     * @param healthPlanId
     * @param authType
     * @return
     * @throws IOException
     * @throws NullPointerException
     * Find member service will be invoked based on the ID card number, DOB, first name, last name, service date, and authorization type..
     * If the member is available it fetches the results 
     * Returning the results in the form of map object to the controller
     */
    public Map<Integer, MemberDetailsFo> findMember(PriorAuthHeaderDTO[] searchResponse,
    		                                        String idCardNumber, 
    		                                        String memberFirstName, 
    		                                        String memberLastName, 
    		                                        String memberDob,
    		                                        String healthPlanId,
    		                                        String authType) throws IOException, NullPointerException, CustomException {

    	log.info("PriorAuthBusinessDelegateVo start findMember (by member)");
		List<MemberDetailsFo> details = new ArrayList<MemberDetailsFo>();
		Map<Integer, MemberDetailsFo> mapList = new HashMap<Integer, MemberDetailsFo>();
		String memberNumber = null;
		try {
			PriorAuthImplServiceStub priorAuthImplServiceStub = new PriorAuthImplServiceStub();
			GetMemberData getMemberData = new GetMemberData();
			MemberDataRequest memberDataRequest = new MemberDataRequest();
			SearchCriteria searchCriteria = new SearchCriteria();
			searchCriteria.setReasonForInquiry(REASON_FOR_INQUIRY); // C
			searchCriteria.setRequestDate(null);
			searchCriteria.setRpn(" ");
			searchCriteria.setProductCode(" ");
			searchCriteria.setPlanCode(" ");
			searchCriteria.setHealthPlanId(healthPlanId); // 0010
			searchCriteria.setHealthPlanGroupId(" ");
			searchCriteria.setIdCardNumber(idCardNumber);
			searchCriteria.setHealthPlanIdType(HEALTH_PLAN_ID_TYPE); // PI
			searchCriteria.setSubscriberNumber(" ");
			memberDataRequest.setSearchCriteria(searchCriteria);
			MoreDataOptions moreDataOptions = new MoreDataOptions();
			moreDataOptions.setRetrieveMemberList(true);
			moreDataOptions.setRetrieveCoverageData(false);
			memberDataRequest.setMoreDataOptions(moreDataOptions);
			getMemberData.setGetMemberData(memberDataRequest);
			log.info("Request Data : " + getMemberData.toString());
			GetMemberDataResponse getMemberDataResponse = null;
			  //TODO: Remove mock service code and condition. Don't remove else condition code
			if(GetterUtil.getBoolean(com.liferay.portal.kernel.util.PropsUtil.get(MemberSearchConstants.ENABLED_MOCK_SERVICE))){
				 //Success Mock Code
				 getMemberDataResponse = getSuccessMockMemberData(getMemberData,idCardNumber);
			}else{
				 getMemberDataResponse = priorAuthImplServiceStub.getMemberData(getMemberData);
			}
		
			boolean searchMemberFlag = true;
			String[] responseCodes = getMemberDataResponse.getGetMemberDataResponse().getResponseCode();
			
			for(String responseCode:responseCodes) {
				
				// Ignore all response codes except 950 and 951
				if (responseCode.matches("000|952|953|954|955|956|957|958|959|960|961|962|963|964|990|991|992|993|994|995|996")) {
					if (searchMemberFlag) {
						
						// Set the flag to indicate the member search has been done
						searchMemberFlag = false;
						memberNumber = getMemberDataResponse.getGetMemberDataResponse().getMemberDemographicsOutput().getMemberId();
						
						log.info("member ID card number ---> " + memberNumber);
						if (((memberNumber == null) || (memberNumber.equals("")) || (memberNumber.equals("null")))) {
							log.info("Invalid Member Data --->" + memberNumber);
							log.info("Invalid Member Data");
							throw new IOException("Invalid Member Data");
						}
						
						mapList = processMemberDataResponse(getMemberDataResponse, idCardNumber, searchResponse, authType, false);

						String coverageList = getMemberDataResponse.getGetMemberDataResponse().getCoverageList().getCoverageCount();
						log.info("coverageListCount: " + coverageList);
						Coverage[] coverage = getMemberDataResponse.getGetMemberDataResponse().getCoverageList().getCoverage();
						for (int i = 0; i < coverage.length; i++) {
							log.info("HealthPlanGroupId " + coverage[i].getHealthPlanGroupId());
							log.info("Coverage Begin date " + coverage[i].getCoverageBeginDate());
							log.info("Client Number " + coverage[i].getClientNumber());
							log.info("getCesGroupNumber " + coverage[i].getCesGroupNumber());
							log.info("getCesGroupNumberDescription " + coverage[i].getCesGroupNumberDescription());
							log.info("getAmmsGroupNumber " + coverage[i].getAmmsGroupNumber());
							log.info("getBusinessSectorCd " + coverage[i].getBusinessSectorSegment().getBusinessSectorCd());
							log.info("getBusinessSectorDesc " + coverage[i].getBusinessSectorSegment().getBusinessSectorDesc());
							log.info("getBusinessSegmentCd " + coverage[i].getBusinessSectorSegment().getBusinessSegmentCd());
							log.info("getBusinessSegmentDesc " + coverage[i].getBusinessSectorSegment().getBusinessSegmentDesc());
						}
				    	log.info("PriorAuthBusinessDelegateVo start findMember (by member)");
					}
				} else {
											
					// Set the flag to indicate the a error that stops the member search
					searchMemberFlag = false;

					String errorMessage = getResponseCodeMessages(responseCodes);
					throw new CustomException(errorMessage);
				}
			}
		}
		// Exception thrown when general network I/O error occurs
		catch (IOException ioe) {
			log.info("Check: " + (ioe instanceof org.apache.axis2.AxisFault));
			log.info("Network I/O error - " + ioe);
			if (ioe instanceof org.apache.axis2.AxisFault) {
				log.info("ioe InstanceOf Axis");
				org.apache.axis2.AxisFault exception = (org.apache.axis2.AxisFault) ioe;
				if (exception.getMessage().equals("Read timed out")) {
					log.info("IOException Timed Out");
					throw new IOException("Your Request is Timed Out");
				} else {
					log.info("IOException No Records");
					throw new IOException("No Records");
				}
			}
			if (ioe.getMessage().equals("Invalid Member Data")) {
				log.info("if block  ioe");
				log.info("IOException Invalid Member Data");
				throw new IOException("Invalid Member Data");
			} else {
				log.info("IOException else block");
				throw new IOException("Your Request is Timed Out");
			}
		} catch (CustomException exception) {
			log.info("MemberSearchBusinessDelegateVo Response Code Exception " + exception.getMessage());
			throw exception;
		} catch (Exception e) {
			log.info("PriorAuthBusiness Super Exception");
			log.error("Exception in findMember method",e);
		}
		return mapList;
    }

    /**
     * 
     * @param indate
     * @return
     * To get the date in MM/dd/yyyy format for findMember response object
     */
    private String convertStringToDate(Date indate) {
		String dateString = null; 
		SimpleDateFormat formatDate = new SimpleDateFormat("MM/dd/yyyy");

		try {
		    dateString = formatDate.format(indate);
		} catch (Exception ex) {
			log.error("Exception in convertStringToDate method",ex);
		}
		return dateString;
    }
    
    /**
     * 
     * @param responseCodes
     * @param idCardNumber
     * @return To display error message based on the response code from the the cds response
     */
    
    private String getResponseCodeMessages(String responseCodes[]) {
		StringBuilder errorMessage = new StringBuilder();
		
		for(String responseCode:responseCodes){
			log.info("responseCode ---> " +responseCode);
			StringBuilder propertyValue = new StringBuilder();
			if (responseCode.matches("950|951")) {
				propertyValue.append("authSearch_");
				propertyValue.append(responseCode);
				log.info("propertyValue" +propertyValue);
				errorMessage.append(properties.getProperty(propertyValue.toString()));
				//errorMessage.append("</br>");
				propertyValue = null;
			} else {
				log.info(responseCode + " is not a Valid Response Code");
			}
		}
		return errorMessage.toString();
    }
    
    
    /**
     * 
     * @param getMemberDataResponse
     * @param searchNumber
     * @param searchResponse
     * @param authType
     * @param authSearch
     * @return 
     * @throws AxisFault 
     * @throws DAOExceptionException 
     * @throws ParseException 
     * @throws RemoteException 
     * Process the member data response from the database and return the member list.
     */
   private Map<Integer, MemberDetailsFo> processMemberDataResponse(GetMemberDataResponse getMemberDataResponse, 
    		                                                       String searchNumber, 
    		                                                       PriorAuthHeaderDTO[] searchResponse,
    		                                                       String authType,
    		                                                       boolean authSearch)
    		throws AxisFault, DAOExceptionException, ParseException, RemoteException {
   
		Map<Integer, MemberDetailsFo> mapList = new HashMap<Integer, MemberDetailsFo>();
		String memberId = null;
		
		// Get the addition member data (there is only one of these)
		String addressLine1 = getMemberDataResponse.getGetMemberDataResponse().getAddress().getAddressLine1();
		String addressLine2 = getMemberDataResponse.getGetMemberDataResponse().getAddress().getAddressLine2();
		String city = getMemberDataResponse.getGetMemberDataResponse().getAddress().getCity();
		String state = getMemberDataResponse.getGetMemberDataResponse().getAddress().getStateCode();
		String zipCode = getMemberDataResponse.getGetMemberDataResponse().getAddress().getZipCode();
		String phoneNumber = getMemberDataResponse.getGetMemberDataResponse().getContactData().getPhoneNumber();
		
		// Get the LOB data
		String businessSectorCode = getMemberDataResponse.getGetMemberDataResponse().getBusinessSectorSegment().getBusinessSectorCd();
		String businessSectorDescription = getMemberDataResponse.getGetMemberDataResponse().getBusinessSectorSegment().getBusinessSectorDesc();
		String businessSegmentCode = getMemberDataResponse.getGetMemberDataResponse().getBusinessSectorSegment().getBusinessSegmentCd();
		String businessSegmentDescription = getMemberDataResponse.getGetMemberDataResponse().getBusinessSectorSegment().getBusinessSegmentDesc();
		
		// Get the subscriber name
		String subscriberName = getMemberDataResponse.getGetMemberDataResponse().getMemberDemographicsOutput().getFirstName() + " " +
				                getMemberDataResponse.getGetMemberDataResponse().getMemberDemographicsOutput().getLastName();
		
		log.info("Health Plan Group Id :  " + getMemberDataResponse.getGetMemberDataResponse().getHealthPlanGroupId());
		log.info("LabBenefitId : " + getMemberDataResponse.getGetMemberDataResponse().getLabBenefitId());
		log.info("LabBenefitIdDescription :  " + getMemberDataResponse.getGetMemberDataResponse().getLabBenefitIdDescription());
		log.info("SubscriberNumber :  " + getMemberDataResponse.getGetMemberDataResponse().getSubscriberNumber());
		log.info("Subscriber Middle Name :  " + getMemberDataResponse.getGetMemberDataResponse().getMemberDemographicsOutput().getMiddleName());
		log.info("Subscriber Suffix Name : " + getMemberDataResponse.getGetMemberDataResponse().getMemberDemographicsOutput().getSuffix());
		log.info("Subscriber Gender Code:  " + getMemberDataResponse.getGetMemberDataResponse().getMemberDemographicsOutput().getGenderCode());
		log.info("Subscriber Member Id :  " + getMemberDataResponse.getGetMemberDataResponse().getMemberDemographicsOutput().getMemberId());
		log.info("Subscriber Date Of Birth:  " + getMemberDataResponse.getGetMemberDataResponse().getMemberDemographicsOutput().getDateOfBirth());
		log.info("Address Line 1: " + addressLine1);
		log.info("Address Line 2: " + addressLine2);
		log.info("City: " + city);
		log.info("State: " + state);
		log.info("Zip Code: " + zipCode);
		log.info("Phone Number: " + phoneNumber);

		String memberListCount = getMemberDataResponse.getGetMemberDataResponse().getMemberList().getMemberListCount();
		log.info("memeberListCount: " + memberListCount);
		Member[] member = getMemberDataResponse.getGetMemberDataResponse().getMemberList().getMember();

		int memberIdx = 0;
		for (int i = 0; i < member.length; i++) {

			// Test for the member if the search is by member.
			if (authSearch) {
    			if(GetterUtil.getBoolean(com.liferay.portal.kernel.util.PropsUtil.get(MemberSearchConstants.ENABLED_MOCK_SERVICE))){
    				// Success Mock Code
				    memberId = "1111PR11111111111";
    			}else{
					memberId = member[i].getMemberId();
				}
				log.info("Member Number " + memberId + " for " + member[i].getFirstName() + " " + member[i].getLastName());
			}

			// Get the data from the Prior Auth Header table
        	// Creating the stub object to invoke the authorization search service
			PriorAuthHeaderServiceStub priorAuthHeaderServiceStub = new PriorAuthHeaderServiceStub();
            PriorAuthorizationSearchE priorAuthorizationSearchE = new PriorAuthorizationSearchE();
            PriorAuthorizationSearch priorAuthorizationSearch = new PriorAuthorizationSearch();

            PriorAuthHeader priorAuthHeader = new PriorAuthHeader();

            // Set the operation type for the prior auth search
            priorAuthHeader.setOperationType("AuthorizationSearch");
            
            PriorAuthHeaderDTO[] priorAuthHeaderDTO = new PriorAuthHeaderDTO[1];
            priorAuthHeaderDTO[0] = new PriorAuthHeaderDTO();
            if (authSearch) {
            	
            	priorAuthHeaderDTO[0].setPriorAuthNumber(searchNumber);
            	priorAuthHeaderDTO[0].setIdCardNumber("0");
            	priorAuthHeaderDTO[0].setExtractPerformedQuantity((short)0);
            	priorAuthHeaderDTO[0].setPriorAuthHeaderKey(0);
            } else {
            	priorAuthHeaderDTO[0].setPriorAuthNumber("0");
            	priorAuthHeaderDTO[0].setIdCardNumber(searchNumber);
            	priorAuthHeaderDTO[0].setExtractPerformedQuantity((short)0);
            	priorAuthHeaderDTO[0].setPriorAuthHeaderKey(0);
            }
            priorAuthHeader.setPriorAuthHeaderDTO(priorAuthHeaderDTO);
            priorAuthorizationSearch.setPriorAuthorizationSearch(priorAuthHeader);
            priorAuthorizationSearchE.setPriorAuthorizationSearch(priorAuthorizationSearch);
            PriorAuthorizationSearchResponseE priorAuthorizationSearchResponseE;

            PriorAuthorizationSearchResponse priorAuthorizationSearchResponse = null;
            //TODO: Remove mock service code and condition. Don't remove else condition code
			if(GetterUtil.getBoolean(com.liferay.portal.kernel.util.PropsUtil.get(MemberSearchConstants.ENABLED_MOCK_SERVICE))){
				 //Success Mock Code
				if(authSearch){
					priorAuthorizationSearchResponse = priorAuthorizationSearchResponseSuccessMock(searchNumber,"0");
				}else{
					priorAuthorizationSearchResponse = priorAuthorizationSearchResponseSuccessMock("0",searchNumber);
				}
				
			}else{
				 // Invoke the service
	             priorAuthorizationSearchResponseE = priorAuthHeaderServiceStub.priorAuthorizationSearch(priorAuthorizationSearchE);
	             priorAuthorizationSearchResponse = priorAuthorizationSearchResponseE.getPriorAuthorizationSearchResponse();
	        }
            // Fetching the data from the response object
            PriorAuthHeaderDTO[] priorAuthHeaderDTOResponse = priorAuthorizationSearchResponse.get_return().getPriorAuthHeaderDTO();

			for (int j = 0; j < priorAuthHeaderDTOResponse.length; j++) {

				if (!authSearch) {

					// Include the authorization number if it is in the search response
					boolean recordFound = false;
					int recordCount = searchResponse.length;
					for (int nextRecord = 0; nextRecord < recordCount; nextRecord++) {
						if (searchResponse[nextRecord].getPriorAuthNumber().equals(priorAuthHeaderDTOResponse[j].getPriorAuthNumber())) {
							// Test the authorization type
							if (authType.length() > 0) {
								boolean psrFlag = priorAuthHeaderDTOResponse[j].getPriorAuthNumber().contains("PR");
								if (authType.equals("PA")) {
									if (!psrFlag) {
										recordFound = true;
									}
								} else {
									if (psrFlag) {
										recordFound = true;
									}
								}

							} else {
								recordFound = true;
							}

							if (recordFound) {
								// Set the member ID
								memberId = member[i].getMemberId();
								log.info("====> (Authorization)Member Number : " + memberId + " for " + member[i].getFirstName() + " " + member[i].getLastName());
							}
							break;
						}
					}
					
					if (!recordFound) {
						
						// Skip the member that does not match
						continue;
					}
				}

				MemberDetailsFo members = new MemberDetailsFo();

				// Set the member data
				members.setSubscriberId(memberId);
				log.info("subscriber id: " + members.getSubscriberId());
				members.setMemberNumber(member[i].getMemberId());
				log.info("HealthPlanGroupId:  " + member[i].getHealthPlanGroupId());
				members.setHealthPlan(member[i].getHealthPlanGroupId());
				log.info("First Name: " + member[i].getFirstName());
				members.setMemberFirstName(member[i].getFirstName());
				log.info("Middle Name: " + member[i].getMiddleName());
				members.setMemberMiddleName(member[i].getMiddleName());
				log.info("Last Name: " + member[i].getLastName());
				members.setMemberLastName(member[i].getLastName());
				log.info("Suffix Name: " + member[i].getSuffix());
				members.setMemberSuffixName(member[i].getSuffix());
				log.info("Date Of Birth: " + member[i].getDateOfBirth());
				members.setMemberDob(convertStringToDate(member[i].getDateOfBirth()));
				log.info("Gender Code: " + member[i].getGenderCode());
				members.setGender(member[i].getGenderCode());
				log.info("Patient Id: " + member[i].getPatientId());
				members.setPatientId(member[i].getPatientId()); // patient Id
				log.info("uniqueMemberId: " + member[i].getUniqueMemberId());
				members.setMpi(member[i].getUniqueMemberId());
				log.info("relationshipCodeDescription:  " + member[i].getRelationshipCodeDescription());
				members.setMemberRelationship(member[i].getRelationshipCodeDescription());
				log.info("memberGroupID:  " + member[i].getHealthPlanGroupId());
				members.setGroupId(member[i].getHealthPlanGroupId());
				
				// Set the addition member data (there is only one of these)
				members.setMemberAddressLine1(addressLine1);
				log.info("memberAddressLine1:  " + addressLine1);
				members.setMemberAddressLine2(addressLine2);
				log.info("memberAddressLine2:  " + addressLine2);
				members.setMemberCity(city);
				log.info("memberCity:  " + city);
				members.setMemberStateCode(state);
				log.info("memberState:  " + state);
				members.setMemberZipcode(zipCode);
				log.info("memberZipCode:  " + zipCode);
				members.setMemberPhoneNumber(phoneNumber);
				log.info("memberPhoneNumber:  " + phoneNumber);
				
				members.setBusinessSectorCode(businessSectorCode);
				log.info("businessSectorCode: " + businessSectorCode);
				members.setBusinessSectorDescription(businessSectorDescription);
				log.info("businessSectorDescription: " + businessSectorDescription);
				members.setBusinessSegmentCode(businessSegmentCode);
				log.info("businessSegmentCode: " + businessSegmentCode);
				members.setBusinessSegmentDescription(businessSegmentDescription);
				log.info("businessSegmentDescription: " + businessSegmentDescription);

				members.setSubscriberName(subscriberName);
				log.info("subscriberName: " + subscriberName);

				if ((priorAuthHeaderDTOResponse[j].getMemberNumber() != null)&& !(priorAuthHeaderDTOResponse[j].getMemberNumber().equals(""))) {
	                
				    // Get the submission status code
					String authSubmissionStatusCode = priorAuthHeaderDTOResponse[j].getPriorAuthSubmissionStatusCode();
  	 	            log.info("authSubmissionStatusCode :" + authSubmissionStatusCode);
  	 	            members.setAuthSubmissionStatusCode(authSubmissionStatusCode);
	                
	                // Get the authorization key response
  		            int authorizationKeyResponse = priorAuthHeaderDTOResponse[j].getPriorAuthHeaderKey();
  	 	            log.info("authorizationKeyResponse :" + authorizationKeyResponse);
  	 	            members.setAuthorizationKeyResponse(authorizationKeyResponse);

	                // Set the ID Card Number
	                String memberIdCardNumber = priorAuthHeaderDTOResponse[j].getIdCardNumber().trim();
	                log.info("memberIdCardNumber :" + memberIdCardNumber);
	                members.setMemberId(memberIdCardNumber);
	                
	                // Set the member number
	                String memberNumber = priorAuthHeaderDTOResponse[j].getMemberNumber().trim();
	                log.info("memberNumber :" + memberNumber);
	                members.setMemberNumber(memberNumber);
	                
	                // Set the prior auth number
	                String authNumber = priorAuthHeaderDTOResponse[j].getPriorAuthNumber();
	                log.info("Authorization Number : " + authNumber);
	                members.setPriorAuthNumber(authNumber);
	                
	                // Set the create date
	                String createDatetime = priorAuthHeaderDTOResponse[j].getAppCreateDatetime();
	                log.info("Creation date : " + createDatetime);
	                members.setAppCreateDatetime(createDatetime);
	                
	                // Set the create user ID
	                String createUser = priorAuthHeaderDTOResponse[j].getAppCreateUserId();
	                log.info("Creation user : " + createUser);
	                members.setAppCreateUserId(createUser);
	                
	                // Set the prior auth status description
	                String authStatus = priorAuthHeaderDTOResponse[j].getPriorAuthStatusDesc();
	                log.info("Authorization Status : " + authStatus);
	                members.setPriorAuthStatusCode(authStatus);
	                
	                // Set the prior auth service begin service date (must be saved with procedure info to get this date)
	                String serviceBeginDate = convertDateStringFormat(priorAuthHeaderDTOResponse[j].getPriorAuthBeginServiceDate(), "dd-MM-yyyy", "MM/dd/yyyy");
	                if (serviceBeginDate == null) {
	                	serviceBeginDate = "";
	                }
	                log.info("Service Begin Date : " + serviceBeginDate);
	                members.setPriorAuthBeginServiceDate(serviceBeginDate);
				}
				
				if ((memberId != null) && memberId.equals(members.getMemberNumber())) {

		            // Setting the member details data into details map object
					mapList.put(memberIdx, members);
					memberIdx++;
				}
			}
		}
		
		return mapList;
    }

    
    /**
     * 
     * @param dt
     * @param fromFmt
     * @param toFmt
     * @return 
     * Convert the date string to a different format.
     */
   private String convertDateStringFormat(String dt, String fromFmt, String toFmt) {
    	String returnDt = null;

    	if (dt == null) {
    		dt = "";
    	}

    	try {
    		if (!dt.equals("")) {
    			
    			// Return the input string if it is in the requested format
    			if (toFmt.equals("MM/dd/yyyy") && (dt.matches("^(1[0-2]|0[1-9])/(3[01]|[12][0-9]|0[1-9])/[0-9]{4}$"))) {
    				returnDt = dt;
    			} else if (toFmt.equals("yyyy-MM-dd") && (dt.matches("^[0-9]{4}-(1[0-2]|0[1-9])-(3[01]|[12][0-9]|0[1-9])$"))) {
    				returnDt = dt;
    			} else {
					SimpleDateFormat inputFormat = new SimpleDateFormat(fromFmt);
					SimpleDateFormat outputFormat = new SimpleDateFormat(toFmt);
					Date tmpDate = inputFormat.parse(dt);
					returnDt = outputFormat.format(tmpDate);
    			}
    		}
    	} catch (ParseException e) {
    		log.error("Exception in convertDateStringFormat method",e);
		}
		
		return returnDt;
    }
   
   /**
    * TODO: Remove below success mock code  
    * @param getMemberData
    * @return
    */
   public GetMemberDataResponse getSuccessMockMemberData(GetMemberData getMemberData,String idCardNo){
	   
	    GetMemberDataResponse getMemberDataResponse = new GetMemberDataResponse();
	    MemberDataResponse memberDataResponse = new MemberDataResponse();
	    CoverageList coverageList = new CoverageList();
	   
		BusinessSectorSegment businessSectorSegment = new BusinessSectorSegment();
    	businessSectorSegment.setBusinessSectorCd("SectorCd");
    	businessSectorSegment.setBusinessSectorDesc("SectorDesc");
    	businessSectorSegment.setBusinessSegmentCd("SegmentCd");
    	businessSectorSegment.setBusinessSegmentDesc("SegmentDesc");
    	
	    Coverage coverage = new Coverage();
   		coverage.setHealthPlanGroupId("HPGID1");
   		coverage.setCoverageBeginDate(new Date());
   		coverage.setClientNumber("CNo");
   		coverage.setCesGroupNumber("CESGrpNo");
   		coverage.setCesGroupNumberDescription("CGND");
   		coverage.setAmmsGroupNumber("AMGRPNo");
   		coverage.setBusinessSectorSegment(businessSectorSegment);
   		coverageList.setCoverage(new Coverage[] {coverage});
	    memberDataResponse.setCoverageList(coverageList);
	    memberDataResponse.setResponseCode(new String[] {"000"});
	    
	    Address address = new Address();
	    address.setAddressLine1("ADDLine1");
	    address.setAddressLine2("AddLine2");
	    address.setCity("Ahbd");
	    address.setStateCode("01");
	    address.setZipCode("322345");
	   
	    memberDataResponse.setAddress(address);
	    
	    ContactData contactData = new ContactData();
	    contactData.setPhoneNumber("23823239");
	    memberDataResponse.setContactData(contactData);
	    memberDataResponse.setBusinessSectorSegment(businessSectorSegment);
	    
	    memberDataResponse.setHealthPlanGroupId("HPG2");
	    memberDataResponse.setLabBenefitId("LABBID");
	    memberDataResponse.setLabBenefitIdDescription("LABDesc");
	    memberDataResponse.setSubscriberNumber("SUBNo");
	   
	    MemberDemographicsOutput memberDemographicsOutput = new MemberDemographicsOutput();
	    memberDemographicsOutput.setDateOfBirth(new Date());
	    memberDemographicsOutput.setFirstName("First");
	    memberDemographicsOutput.setGenderCode("01");
	    memberDemographicsOutput.setLastName("Last");
	   // memberDemographicsOutput.setMemberId(getMemberData.getGetMemberData().getSearchCriteria().getIdCardNumber());
	    memberDemographicsOutput.setMemberId(idCardNo);
	    memberDemographicsOutput.setMiddleName("Middle");
	    memberDemographicsOutput.setSuffix("SUF");
	    memberDataResponse.setMemberDemographicsOutput(memberDemographicsOutput);
	    
		MemberList memberList = new MemberList();
    	memberList.setMemberListCount("1");
    	Member member = new Member();
    	member.setMemberId(idCardNo);
    	member.setFirstName("MeMFirstName");
    	member.setDateOfBirth(new Date());
    	member.setHealthPlanGroupId("MemberHPGroupId");
    	member.setLastName("MemLastName");
    	member.setMiddleName("MemMiddleName");
    	member.setSuffix("TestMem");
    	member.setGenderCode("MGEC");
    	member.setPatientId("MPID");
    	member.setUniqueMemberId("UMID");
    	member.setRelationshipCodeDescription("RCD");
    	member.setBusinessSectorSegment(businessSectorSegment);
    	member.setDependentVerificationIndicator(true);
    	member.setGenderDescription("GDES");
    	member.setLabBenefitId("LID");
    	member.setLabBenefitIdDescription("LBID");
    	member.setPrivacyIndicator(false);
    	
    	memberList.setMember(new Member[] {member});
    	memberDataResponse.setMemberList(memberList);
	    
	    getMemberDataResponse.setGetMemberDataResponse(memberDataResponse);
	    
	   return getMemberDataResponse;
   }
   
   /**
	 * TODO: Remove below success mock code 
	 */
	public static PriorAuthorizationSearchResponse priorAuthorizationSearchResponseSuccessMock(String authorizationNo, String idCardNo){
		
		 	PriorAuthorizationSearchResponse priorAuthorizationSearchResponse = new PriorAuthorizationSearchResponse();
		 
		 	PriorAuthHeaderDTO priorAuthHeaderDTOObj = new PriorAuthHeaderDTO();
			priorAuthHeaderDTOObj.setAppCreateDatetime("2018-04-26 01:02:00");
			priorAuthHeaderDTOObj.setPriorAuthSubmissionStatusCode("10");
			priorAuthHeaderDTOObj.setPriorAuthPatientFirstName("PatientFirstName");
			priorAuthHeaderDTOObj.setPriorAuthPatientLastName("PatientLastName");
			priorAuthHeaderDTOObj.setPriorAuthPatientBirthDate("1999-04-26 01:02:00");
			priorAuthHeaderDTOObj.setPriorAuthBusinessSegmentDescription("SegDesc");
			priorAuthHeaderDTOObj.setPriorAuthStatusDesc("StatusDesc");
			priorAuthHeaderDTOObj.setMemberNumber(idCardNo);
			priorAuthHeaderDTOObj.setAppCreateUserId("22");
			priorAuthHeaderDTOObj.setAuthInboundChannel("Channel");
			priorAuthHeaderDTOObj.setAuthNumberSearched(authorizationNo);
			priorAuthHeaderDTOObj.setAuthRequestedDate("2018-04-26 01:02:00");
			priorAuthHeaderDTOObj.setExtractPerformedQuantity((short)1);
			priorAuthHeaderDTOObj.setHealthPlanGroupId("HPGID");
			priorAuthHeaderDTOObj.setHealthPlanId("HID");
			priorAuthHeaderDTOObj.setIcdRevisionNumber("IRNo");
			priorAuthHeaderDTOObj.setIdCardNumber(idCardNo);
			priorAuthHeaderDTOObj.setMasterPatientId("MPId");
			priorAuthHeaderDTOObj.setPriorAuthNumber(authorizationNo);
			priorAuthHeaderDTOObj.setPriorAuthHeaderKey(22);
			
			PriorAuthHeaderServiceStub.ProviderDTO providerDTO = new PriorAuthHeaderServiceStub.ProviderDTO();
			providerDTO.setAppCreatedDateTime("2018-04-26 01:02:00");
			providerDTO.setAppCreatedUserId("CreatedUserId");
			providerDTO.setAppMantDateTime("2018-04-26 01:02:00f");
			providerDTO.setAppMantUserId("AppMantUserId");
			providerDTO.setOrderingAddressLine1("Add1");
			providerDTO.setOrderingAddressLine2("Add2");
			providerDTO.setOrderingCity("TestCity");
			providerDTO.setOrderingFaxNumber("1234567890");
			providerDTO.setOrderingFirstName("OFirst");
			providerDTO.setOrderingLastName("OLast");
			providerDTO.setOrderingNpi("1234567890");
			providerDTO.setOrderingPhoneNumber("0987654321");
			providerDTO.setOrderingState("OState");
			providerDTO.setOrderingTinEin("123456789");
			providerDTO.setOrderingZip("32001");
			providerDTO.setPriorAuthHeaderKey(01);
			providerDTO.setPriorAuthProviderTypeCode("ProviderCode");
			providerDTO.setRenderingAddressLine1("RenAdd1");
			providerDTO.setRenderingAddressLine2("RenAdd2");
			providerDTO.setRenderingCity("RedCity");
			providerDTO.setRenderingFaxNumber("9199129191");
			providerDTO.setRenderingFirstName("Rfirst");
			providerDTO.setRenderingLabName("RLab");
			providerDTO.setRenderingLastName("RLast");
			providerDTO.setRenderingNpi("Rnpi");
			providerDTO.setRenderingPhoneNumber("9191919191");
			providerDTO.setRenderingState("RState");
			providerDTO.setRenderingTinEin("RTinEin");
			providerDTO.setRenderingZip("90800");
			
			PriorAuthHeader priorAuthHeaderObj = new PriorAuthHeader();
			priorAuthHeaderObj.setPriorAuthHeaderDTO(new PriorAuthHeaderDTO[] { priorAuthHeaderDTOObj });
			priorAuthHeaderObj.setProvider(providerDTO);
			priorAuthorizationSearchResponse.set_return(priorAuthHeaderObj);
		return priorAuthorizationSearchResponse;
	}
}
