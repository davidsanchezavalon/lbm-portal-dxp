package com.avalon.lbm.integration.okta.portlet;

import com.avalon.lbm.integration.okta.constants.PartnerLinksPortletKeys;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;

import javax.portlet.Portlet;

import org.osgi.service.component.annotations.Component;

/**
 * @author sandeep.nair
 */
@Component(
	immediate = true,
	property = {
		"com.liferay.portlet.css-class-wrapper=avalon-portlets",
		"com.liferay.portlet.display-category=Avalon Portlets",
		"com.liferay.portlet.instanceable=true",
		"com.liferay.portlet.private-session-attributes=false",
		"com.liferay.portlet.header-portlet-css=/css/main.css",
		"com.liferay.portlet.footer-portlet-javascript=/js/jquery.min.js",
		"com.liferay.portlet.footer-portlet-javascript=/js/main.js",
		"javax.portlet.display-name=avalon-okta-sso-portlet Portlet",
		"javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.name=" + PartnerLinksPortletKeys.PartnerLinks,
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user"
	},
	service = Portlet.class
)
public class PartnerLinksPortlet extends MVCPortlet {
}