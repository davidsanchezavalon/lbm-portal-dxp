package com.avalon.lbm.integration.okta.constants;

/**
 * @author sandeep.nair
 */
public class PartnerLinksPortletKeys {

	public static final String PartnerLinks = "PartnerLinks";

}