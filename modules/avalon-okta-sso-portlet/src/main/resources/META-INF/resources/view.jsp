<%@ include file="init.jsp" %>

<script type="text/javascript">var email = "<%= user.getEmailAddress() %>";</script>

<aui:script>
	<%
		// Define the url for DEV, QA, an UAT
		final String testUrl = "https://uat-benemgmt.oktapreview.com/api/v1/users/";
	
		// Define the URL for PROD
		final String prodUrl = "https://avalonhcs.okta.com/api/v1/users/";
		
		// Get the token from lbm-common.properties
		String oktaToken = "";
		String liferayHome = System.getProperty("LIFERAY_HOME");
		String propertyLocation = liferayHome +"/lbm-common.properties";
		Properties prop = new Properties();
		InputStream input = new FileInputStream(propertyLocation);
		prop.load(input);
		oktaToken = prop.getProperty("oktaToken");
	%>

	$(function() {
		var token = "<%= oktaToken %>";
		
		// Set the URL based on the location
		var url = "<%= testUrl %>";
		var loc = window.location.href;

		if (location.hostname.toLowerCase() == "portal.avalonhcs.com") {
			url = "<%= prodUrl %>";
		}
		/* if ((location.hostname.toLowerCase() == "dev-benemgmt.com") || (location.hostname.toLowerCase() == "qa-benemgmt.com") || (location.hostname.toLowerCase() == "uat-benemgmt.com")) {
			url = "<%= testUrl %>";
		} else if (location.hostname.toLowerCase() == "avalonhcs.com") {
			url = "<%= prodUrl %>";
		} */
		
		var requestUrlUser = url + email;
		
		ajaxCall(requestUrlUser).success(function (user) {
			var requestUrlUserApps = url + user.id + "/appLinks";
			
			ajaxCall(requestUrlUserApps).success(function (apps) {
				generateHTML(apps);
			});
		});
		
		function ajaxCall(requestUrl) {
			return $.ajax({
			    url: requestUrl,
			    type: "GET",
			    beforeSend: function (request) {
			        request.setRequestHeader("Accept", "application/json");
			        request.setRequestHeader("Content-Type", "application/json");
			    	request.setRequestHeader("Authorization", "SSWS " + token);
			    }
			});
		}
		
		function generateHTML(apps) {
			var footerText = "No external applications are available to you at this time";
			
			if (apps && apps.length > 0) {
				for (var i = 0, n = apps.length; i < n; i++) {
					var thisApp = apps[i];
					if ( thisApp.appName.indexOf("avalonportal") >= 0 || thisApp.appName.indexOf("liferay") >= 0  ) {
						continue;
					}
					
			    	var linkElement = document.createElement("a");
			    	
			    	linkElement.setAttribute("href", thisApp.linkUrl);
			    	linkElement.setAttribute("target", "_blank");
			    	linkElement.setAttribute("class", "app-link");
			    	linkElement.innerHTML = thisApp.label;
			    	
			    	document.getElementById("ua-container").getElementsByClassName("ua-body")[0].appendChild(linkElement);
				}
				
				footerText = "Please note: for the above applications you will be directed outside the Avalon Portal. These websites are not maintained by Avalon Health Care Services, LLC, d/b/a Avalon Healthcare Solutions.";
			}
			
			document.getElementById("ua-container").getElementsByTagName("p")[0].innerHTML = footerText;
		}
	});
</aui:script>

<div id="ua-container">
	<div class="ua-header">
		<h3>Additional Applications</h3>
	</div>
	
	<div class="ua-body">
		<% if (RoleLocalServiceUtil.hasUserRole(user.getUserId(), PortalUtil.getDefaultCompanyId(), "AvalonProviderRole", true) ) { %>
		
			<!-- Display the hyperlink for BCBS South Carolina Medical Policies -->
			<a class="app-link"  href="http://web.southcarolinablues.com/providers/educationcenter/medicalpoliciesandclinicalguidelines.aspx"
			   target="_blank">BCBSSC Medical Policies
			</a>
		<% } %>
	</div>
	
	<div class="ua-footer">
		<p>&nbsp;</p>
	</div>
</div>
