package avalon.user.navigation.provider;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.portlet.BasePortletProvider;
import com.liferay.portal.kernel.portlet.ViewPortletProvider;
import com.liferay.portal.kernel.theme.ThemeDisplay;

@Component(
	    immediate = true,
	    property = {"model.class.name=avalon.user.navigation.provider.AvalonUserNavigation"},
	    service = ViewPortletProvider.class
	)
public class AvalonUserNavigationViewPortletProvider extends BasePortletProvider implements ViewPortletProvider{

	@Override
	public String getPortletName() {
		return "AvalonUserNavigation";
	}

	@Override
	protected long getPlid(ThemeDisplay themeDisplay) throws PortalException {
		return themeDisplay.getPlid();
	}

}
