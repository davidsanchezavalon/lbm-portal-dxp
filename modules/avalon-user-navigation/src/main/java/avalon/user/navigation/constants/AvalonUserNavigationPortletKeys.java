package avalon.user.navigation.constants;

/**
 * @author mansi.panchal
 */
public class AvalonUserNavigationPortletKeys {

	public static final String AvalonUserNavigation = "AvalonUserNavigation";
	public static final String USER_GROUP_EVERYONE = "Everyone";

}