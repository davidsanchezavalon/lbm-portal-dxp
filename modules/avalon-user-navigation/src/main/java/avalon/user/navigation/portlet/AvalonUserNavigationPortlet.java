package avalon.user.navigation.portlet;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.UserGroup;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.service.UserGroupLocalServiceUtil;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.WebKeys;

import avalon.user.navigation.constants.AvalonUserNavigationPortletKeys;

/**
 * @author mansi.panchal
 */
@Component(
	immediate = true,
	property = {
		"com.liferay.portlet.add-default-resource=true",
		"com.liferay.portlet.display-category=category.sample",
		"com.liferay.portlet.instanceable=false",
		"javax.portlet.display-name=Avalon User Navigation Portlet",
		"javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.name=" + AvalonUserNavigationPortletKeys.AvalonUserNavigation,
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user"
	},
	service = Portlet.class
)
public class AvalonUserNavigationPortlet extends MVCPortlet {

	//Set user group in userGroupName 
	/*private static final Log log = LogFactoryUtil.getLog(AvalonUserNavigationPortlet.class.getName());
	
	@Override
	public void render(RenderRequest renderRequest, RenderResponse renderResponse)
			throws IOException, PortletException {
		try{
			
			ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
			List<UserGroup> userGroups = UserGroupLocalServiceUtil.getUserUserGroups(themeDisplay.getUserId());
			
			String commaSeparatedUserGroupStr = userGroups.stream()
					 .filter(userGroup -> !userGroup.getName().equals(AvalonUserNavigationPortletKeys.USER_GROUP_EVERYONE))
					 .map(userGroup -> userGroup.getName()).collect(Collectors.joining(","));
			renderRequest.setAttribute("userGroupName",commaSeparatedUserGroupStr);
			super.render(renderRequest, renderResponse);
		}catch(Exception e){
			log.error("Error in render method",e);
		}
	}
	*/
	
}