<%@page import="java.util.stream.Collectors"%>
<%@page import="java.util.Arrays"%>
<%@page import="com.liferay.portal.kernel.service.UserGroupLocalServiceUtil"%>
<%@page import="com.liferay.portal.kernel.model.UserGroup"%>
<%@page import="com.liferay.portal.kernel.service.GroupLocalServiceUtil"%>
<%@page import="com.liferay.portal.kernel.model.Group"%>
<%@page import="java.util.List"%>
<%@page import="com.liferay.portal.kernel.util.HtmlUtil"%>
<%@page import="com.liferay.portal.kernel.util.PortalUtil"%>
<%@ include file="init.jsp" %>

<div class="custom-user btn-group">
	<a class="btn btn-link btn-profile dropdown-toggle" data-toggle="dropdown" href="#">
		<div class="user-img">
			<div class="aspect-ratio-bg-cover user-icon" style="background-image:url(<%=user.getPortraitURL(themeDisplay)%>)">
			</div>
		</div>
		<div class=" user-info">
			<p class="name" title="${user.getFullName()}">
				${user.getFullName()}
			</p>
			<%-- Comment User Group from User Navigation --%>
			<%-- <span class="designation">
				${userGroupName} 
			</span> --%>
		</div>
		<i class="icon-chevron-down dropdown-arrow" aria-hidden="true"></i>
	</a>
	<div class="dropdown-menu user-dropdown">
		<ul>
			<li>
				<a class="btn-profile"
					data-redirect="<%=PortalUtil.isLoginRedirectRequired(request)%>"
					href='<%=HtmlUtil.escape(themeDisplay.getURLSignOut())%>'
					id="sign-out" rel="nofollow"> <i class="glyphicon glyphicon-off"
						aria-hidden="true"></i> <span>Sign Out</span>
				</a>
			</li>
		</ul>
	</div>
</div>
