/**
 * Copyright 2000-present Liferay, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.avalon.auth;

import com.liferay.portal.kernel.configuration.Filter;
import com.liferay.portal.kernel.events.ActionException;
import com.liferay.portal.kernel.events.LifecycleAction;
import com.liferay.portal.kernel.events.LifecycleEvent;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.model.UserGroup;
import com.liferay.portal.kernel.service.UserGroupLocalServiceUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.Validator;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.osgi.service.component.annotations.Component;

@Component(
	immediate = true,
	property = {
        "key=login.events.post"
    },
    service = LifecycleAction.class
)
public class HomeRedirectLoginPostAction implements LifecycleAction {

    @Override
	public void processLifecycleEvent(LifecycleEvent lifecycleEvent)
		throws ActionException {
    	 HttpServletRequest request = lifecycleEvent.getRequest();
         HttpServletResponse response = lifecycleEvent.getResponse();
         String redirect = selectPageRedirect(request, response);
         if (Validator.isNotNull(redirect)) {
        	redirect = PortalUtil.escapeRedirect(redirect);
 			try {
 				response.sendRedirect(redirect);
 			} catch (IOException ioe) {
 				throw new SystemException(ioe);
 			}
         }
	}
    
    private String selectPageRedirect(HttpServletRequest request, HttpServletResponse response) throws ActionException {
		String customLandingPage = null;
		try {
			if (MANAGED_USER_GROUPS.length > 0) {
				User user = PortalUtil.getUser(request);
				List<UserGroup> userGroups = UserGroupLocalServiceUtil.getUserUserGroups(user.getUserId());
				if (userGroups.size() > 0) {
					boolean found = false;
					for (int i = 0; i < MANAGED_USER_GROUPS.length && !found; i++) {
						String groupName = MANAGED_USER_GROUPS[i];
						for (int j = 0; j < userGroups.size() && !found; j++) {
							UserGroup userGroup = userGroups.get(j);
							String userGroupName = userGroup.getName().replaceAll(" ", "_");
							if (userGroupName.equalsIgnoreCase(groupName)) {
								found = true;
								customLandingPage = getLandingPage(groupName);
							}
						}
					}
				}
			}
		} catch (Exception ex) {
			_log.error(ex);
		}
		return customLandingPage;
	}
	
	private String getLandingPage(String groupName) {
		if (landingPages==null) {
			landingPages = new HashMap<String, String>();
			for (String currentName : MANAGED_USER_GROUPS) {
				String landingPage = GetterUtil.getString(PropsUtil.get("default.landing.page.path", new Filter(currentName)), null);
				if (landingPage!=null) {
					landingPages.put(currentName, landingPage);
				}
			}
		}
		return landingPages.get(groupName);
	}
	
	private static Map<String, String> landingPages;
	private static final String[] MANAGED_USER_GROUPS = PropsUtil.getArray("managed.user.groups");
	
	private static final Log _log = LogFactoryUtil.getLog(HomeRedirectLoginPostAction.class);

}