/**
 * Copyright 2000-present Liferay, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.avalon.auth;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.events.ActionException;
import com.liferay.portal.kernel.events.LifecycleAction;
import com.liferay.portal.kernel.events.LifecycleEvent;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.Validator;

@Component(
	immediate = true,
	property = {
        "key=logout.events.post"
    },
    service = LifecycleAction.class
)
public class AvalonLogoutPostAction implements LifecycleAction {

    @Override
	public void processLifecycleEvent(LifecycleEvent lifecycleEvent)
		throws ActionException {
    	_log.info("Inside processLifecycleEvent method");
    	 HttpServletRequest request = lifecycleEvent.getRequest();
         HttpServletResponse response = lifecycleEvent.getResponse();
         String redirect = getLogoutURL(request, response);
         if (Validator.isNotNull(redirect)) {
 			try {
 				response.sendRedirect(redirect);
 			} catch (IOException ioe) {
 				throw new SystemException(ioe);
 			}
         }
	}
    
    private String getLogoutURL(HttpServletRequest request, HttpServletResponse response) throws ActionException {
		return PropsUtil.get("avalon.guest.landing.url");
	}
	
	
	
	private static final Log _log = LogFactoryUtil.getLog(AvalonLogoutPostAction.class);

}