/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.avalon.usergroup.model.impl;

import aQute.bnd.annotation.ProviderType;

import com.avalon.usergroup.model.OktaUserGroup;

import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.util.HashUtil;
import com.liferay.portal.kernel.util.StringBundler;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing OktaUserGroup in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @see OktaUserGroup
 * @generated
 */
@ProviderType
public class OktaUserGroupCacheModel implements CacheModel<OktaUserGroup>,
	Externalizable {
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof OktaUserGroupCacheModel)) {
			return false;
		}

		OktaUserGroupCacheModel oktaUserGroupCacheModel = (OktaUserGroupCacheModel)obj;

		if (oktaUserGroupId == oktaUserGroupCacheModel.oktaUserGroupId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, oktaUserGroupId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(3);

		sb.append("{oktaUserGroupId=");
		sb.append(oktaUserGroupId);

		return sb.toString();
	}

	@Override
	public OktaUserGroup toEntityModel() {
		OktaUserGroupImpl oktaUserGroupImpl = new OktaUserGroupImpl();

		oktaUserGroupImpl.setOktaUserGroupId(oktaUserGroupId);

		oktaUserGroupImpl.resetOriginalValues();

		return oktaUserGroupImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		oktaUserGroupId = objectInput.readLong();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(oktaUserGroupId);
	}

	public long oktaUserGroupId;
}