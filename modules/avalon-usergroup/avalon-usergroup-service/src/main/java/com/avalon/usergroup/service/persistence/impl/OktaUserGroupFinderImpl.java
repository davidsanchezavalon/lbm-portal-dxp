package com.avalon.usergroup.service.persistence.impl;

import com.avalon.usergroup.service.persistence.OktaUserGroupFinder;
import com.liferay.portal.dao.orm.custom.sql.CustomSQLUtil;
import com.liferay.portal.kernel.bean.PortalBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.SQLQuery;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.dao.orm.SessionFactory;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.model.UserGroup;
import com.liferay.portal.kernel.util.PortalClassLoaderUtil;

import java.util.List;

public class OktaUserGroupFinderImpl extends OktaUserGroupFinderBaseImpl implements OktaUserGroupFinder{
	private static final String FIND_BY_NON_MEMBER_GROUPS = "OktaGroupFinder.findByNonMemberGroups";

	public List<UserGroup> getNonMemberOktaUserGroups(long companyId, long userId, String fieldName, long classNameId) throws SystemException {
		SessionFactory sessionFactory = (SessionFactory)PortalBeanLocatorUtil.locate("liferaySessionFactory");

		Session session = null;

		try {
			//session = openSession();
			session = sessionFactory.openSession();

			String sql = CustomSQLUtil.get(this.getClass(),FIND_BY_NON_MEMBER_GROUPS);
			//sql = "SELECT * FROM usergroup WHERE userGroupId IN (SELECT v.classPK FROM expandovalue v INNER JOIN (expandotable t INNER JOIN expandocolumn c ON t.tableId=c.tableId) on v.tableId = t.tableId WHERE c.name=? AND t.companyId=? AND t.classNameId=? AND v.classPK NOT IN (SELECT u1.userGroupId FROM usergroup u1 INNER JOIN users_usergroups u2 ON u1.userGroupId=u2.userGroupId WHERE u2.userId=?))";
			SQLQuery query = session.createSQLQuery(sql);
			query.addEntity("UserGroup", PortalClassLoaderUtil.getClassLoader().loadClass("com.liferay.portal.model.impl.UserGroupImpl"));
			QueryPos pos = QueryPos.getInstance(query);
			pos.add(fieldName);
			pos.add(companyId);
			pos.add(classNameId);
			pos.add(userId);
			List<UserGroup> userGroups = (List<UserGroup>)query.list();

			return userGroups;
		}
		catch (Exception e) {
			throw new SystemException(e);
		}
		finally {
			// closeSession(session);
			sessionFactory.closeSession(session);
		}
	}
}
