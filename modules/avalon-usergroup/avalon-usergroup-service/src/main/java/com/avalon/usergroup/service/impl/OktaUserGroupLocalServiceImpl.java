/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.avalon.usergroup.service.impl;

import com.avalon.usergroup.service.base.OktaUserGroupLocalServiceBaseImpl;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.model.ListType;
import com.liferay.portal.kernel.model.UserGroup;
import com.liferay.portal.kernel.service.persistence.ListTypeUtil;

import java.util.List;

/**
 * The implementation of the okta user group local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.avalon.usergroup.service.OktaUserGroupLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see OktaUserGroupLocalServiceBaseImpl
 * @see com.avalon.usergroup.service.OktaUserGroupLocalServiceUtil
 */
public class OktaUserGroupLocalServiceImpl
	extends OktaUserGroupLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Always use {@link com.avalon.usergroup.service.OktaUserGroupLocalServiceUtil} to access the okta user group local service.
	 */
	
	// Custom methods
	
		public List<UserGroup> getNonMemberOktaUserGroups(long companyId, long userId, String fieldName, long classNameId) throws SystemException {
			return oktaUserGroupFinder.getNonMemberOktaUserGroups(companyId, userId, fieldName, classNameId);
		}
		
		public ListType update(ListType listType) throws SystemException {
			return ListTypeUtil.update(listType);
		}
}