/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.avalon.usergroup.service.persistence.impl;

import com.avalon.usergroup.model.OktaUserGroup;
import com.avalon.usergroup.service.persistence.OktaUserGroupPersistence;

import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;

/**
 * @author Brian Wing Shun Chan
 * @generated
 */
public class OktaUserGroupFinderBaseImpl extends BasePersistenceImpl<OktaUserGroup> {
	public OktaUserGroupFinderBaseImpl() {
		setModelClass(OktaUserGroup.class);
	}

	/**
	 * Returns the okta user group persistence.
	 *
	 * @return the okta user group persistence
	 */
	public OktaUserGroupPersistence getOktaUserGroupPersistence() {
		return oktaUserGroupPersistence;
	}

	/**
	 * Sets the okta user group persistence.
	 *
	 * @param oktaUserGroupPersistence the okta user group persistence
	 */
	public void setOktaUserGroupPersistence(
		OktaUserGroupPersistence oktaUserGroupPersistence) {
		this.oktaUserGroupPersistence = oktaUserGroupPersistence;
	}

	@BeanReference(type = OktaUserGroupPersistence.class)
	protected OktaUserGroupPersistence oktaUserGroupPersistence;
}