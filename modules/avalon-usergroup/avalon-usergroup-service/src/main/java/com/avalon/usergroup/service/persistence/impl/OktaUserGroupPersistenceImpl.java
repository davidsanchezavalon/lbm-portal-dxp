/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.avalon.usergroup.service.persistence.impl;

import aQute.bnd.annotation.ProviderType;

import com.avalon.usergroup.exception.NoSuchOktaUserGroupException;
import com.avalon.usergroup.model.OktaUserGroup;
import com.avalon.usergroup.model.impl.OktaUserGroupImpl;
import com.avalon.usergroup.model.impl.OktaUserGroupModelImpl;
import com.avalon.usergroup.service.persistence.OktaUserGroupPersistence;

import com.liferay.portal.kernel.dao.orm.EntityCache;
import com.liferay.portal.kernel.dao.orm.FinderCache;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.spring.extender.service.ServiceReference;

import java.io.Serializable;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * The persistence implementation for the okta user group service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see OktaUserGroupPersistence
 * @see com.avalon.usergroup.service.persistence.OktaUserGroupUtil
 * @generated
 */
@ProviderType
public class OktaUserGroupPersistenceImpl extends BasePersistenceImpl<OktaUserGroup>
	implements OktaUserGroupPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link OktaUserGroupUtil} to access the okta user group persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = OktaUserGroupImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(OktaUserGroupModelImpl.ENTITY_CACHE_ENABLED,
			OktaUserGroupModelImpl.FINDER_CACHE_ENABLED,
			OktaUserGroupImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(OktaUserGroupModelImpl.ENTITY_CACHE_ENABLED,
			OktaUserGroupModelImpl.FINDER_CACHE_ENABLED,
			OktaUserGroupImpl.class, FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(OktaUserGroupModelImpl.ENTITY_CACHE_ENABLED,
			OktaUserGroupModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);

	public OktaUserGroupPersistenceImpl() {
		setModelClass(OktaUserGroup.class);
	}

	/**
	 * Caches the okta user group in the entity cache if it is enabled.
	 *
	 * @param oktaUserGroup the okta user group
	 */
	@Override
	public void cacheResult(OktaUserGroup oktaUserGroup) {
		entityCache.putResult(OktaUserGroupModelImpl.ENTITY_CACHE_ENABLED,
			OktaUserGroupImpl.class, oktaUserGroup.getPrimaryKey(),
			oktaUserGroup);

		oktaUserGroup.resetOriginalValues();
	}

	/**
	 * Caches the okta user groups in the entity cache if it is enabled.
	 *
	 * @param oktaUserGroups the okta user groups
	 */
	@Override
	public void cacheResult(List<OktaUserGroup> oktaUserGroups) {
		for (OktaUserGroup oktaUserGroup : oktaUserGroups) {
			if (entityCache.getResult(
						OktaUserGroupModelImpl.ENTITY_CACHE_ENABLED,
						OktaUserGroupImpl.class, oktaUserGroup.getPrimaryKey()) == null) {
				cacheResult(oktaUserGroup);
			}
			else {
				oktaUserGroup.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all okta user groups.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(OktaUserGroupImpl.class);

		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the okta user group.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(OktaUserGroup oktaUserGroup) {
		entityCache.removeResult(OktaUserGroupModelImpl.ENTITY_CACHE_ENABLED,
			OktaUserGroupImpl.class, oktaUserGroup.getPrimaryKey());

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<OktaUserGroup> oktaUserGroups) {
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (OktaUserGroup oktaUserGroup : oktaUserGroups) {
			entityCache.removeResult(OktaUserGroupModelImpl.ENTITY_CACHE_ENABLED,
				OktaUserGroupImpl.class, oktaUserGroup.getPrimaryKey());
		}
	}

	/**
	 * Creates a new okta user group with the primary key. Does not add the okta user group to the database.
	 *
	 * @param oktaUserGroupId the primary key for the new okta user group
	 * @return the new okta user group
	 */
	@Override
	public OktaUserGroup create(long oktaUserGroupId) {
		OktaUserGroup oktaUserGroup = new OktaUserGroupImpl();

		oktaUserGroup.setNew(true);
		oktaUserGroup.setPrimaryKey(oktaUserGroupId);

		return oktaUserGroup;
	}

	/**
	 * Removes the okta user group with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param oktaUserGroupId the primary key of the okta user group
	 * @return the okta user group that was removed
	 * @throws NoSuchOktaUserGroupException if a okta user group with the primary key could not be found
	 */
	@Override
	public OktaUserGroup remove(long oktaUserGroupId)
		throws NoSuchOktaUserGroupException {
		return remove((Serializable)oktaUserGroupId);
	}

	/**
	 * Removes the okta user group with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the okta user group
	 * @return the okta user group that was removed
	 * @throws NoSuchOktaUserGroupException if a okta user group with the primary key could not be found
	 */
	@Override
	public OktaUserGroup remove(Serializable primaryKey)
		throws NoSuchOktaUserGroupException {
		Session session = null;

		try {
			session = openSession();

			OktaUserGroup oktaUserGroup = (OktaUserGroup)session.get(OktaUserGroupImpl.class,
					primaryKey);

			if (oktaUserGroup == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchOktaUserGroupException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(oktaUserGroup);
		}
		catch (NoSuchOktaUserGroupException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected OktaUserGroup removeImpl(OktaUserGroup oktaUserGroup) {
		oktaUserGroup = toUnwrappedModel(oktaUserGroup);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(oktaUserGroup)) {
				oktaUserGroup = (OktaUserGroup)session.get(OktaUserGroupImpl.class,
						oktaUserGroup.getPrimaryKeyObj());
			}

			if (oktaUserGroup != null) {
				session.delete(oktaUserGroup);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (oktaUserGroup != null) {
			clearCache(oktaUserGroup);
		}

		return oktaUserGroup;
	}

	@Override
	public OktaUserGroup updateImpl(OktaUserGroup oktaUserGroup) {
		oktaUserGroup = toUnwrappedModel(oktaUserGroup);

		boolean isNew = oktaUserGroup.isNew();

		Session session = null;

		try {
			session = openSession();

			if (oktaUserGroup.isNew()) {
				session.save(oktaUserGroup);

				oktaUserGroup.setNew(false);
			}
			else {
				oktaUserGroup = (OktaUserGroup)session.merge(oktaUserGroup);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew) {
			finderCache.removeResult(FINDER_PATH_COUNT_ALL, FINDER_ARGS_EMPTY);
			finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL,
				FINDER_ARGS_EMPTY);
		}

		entityCache.putResult(OktaUserGroupModelImpl.ENTITY_CACHE_ENABLED,
			OktaUserGroupImpl.class, oktaUserGroup.getPrimaryKey(),
			oktaUserGroup, false);

		oktaUserGroup.resetOriginalValues();

		return oktaUserGroup;
	}

	protected OktaUserGroup toUnwrappedModel(OktaUserGroup oktaUserGroup) {
		if (oktaUserGroup instanceof OktaUserGroupImpl) {
			return oktaUserGroup;
		}

		OktaUserGroupImpl oktaUserGroupImpl = new OktaUserGroupImpl();

		oktaUserGroupImpl.setNew(oktaUserGroup.isNew());
		oktaUserGroupImpl.setPrimaryKey(oktaUserGroup.getPrimaryKey());

		oktaUserGroupImpl.setOktaUserGroupId(oktaUserGroup.getOktaUserGroupId());

		return oktaUserGroupImpl;
	}

	/**
	 * Returns the okta user group with the primary key or throws a {@link com.liferay.portal.kernel.exception.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the okta user group
	 * @return the okta user group
	 * @throws NoSuchOktaUserGroupException if a okta user group with the primary key could not be found
	 */
	@Override
	public OktaUserGroup findByPrimaryKey(Serializable primaryKey)
		throws NoSuchOktaUserGroupException {
		OktaUserGroup oktaUserGroup = fetchByPrimaryKey(primaryKey);

		if (oktaUserGroup == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchOktaUserGroupException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return oktaUserGroup;
	}

	/**
	 * Returns the okta user group with the primary key or throws a {@link NoSuchOktaUserGroupException} if it could not be found.
	 *
	 * @param oktaUserGroupId the primary key of the okta user group
	 * @return the okta user group
	 * @throws NoSuchOktaUserGroupException if a okta user group with the primary key could not be found
	 */
	@Override
	public OktaUserGroup findByPrimaryKey(long oktaUserGroupId)
		throws NoSuchOktaUserGroupException {
		return findByPrimaryKey((Serializable)oktaUserGroupId);
	}

	/**
	 * Returns the okta user group with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the okta user group
	 * @return the okta user group, or <code>null</code> if a okta user group with the primary key could not be found
	 */
	@Override
	public OktaUserGroup fetchByPrimaryKey(Serializable primaryKey) {
		Serializable serializable = entityCache.getResult(OktaUserGroupModelImpl.ENTITY_CACHE_ENABLED,
				OktaUserGroupImpl.class, primaryKey);

		if (serializable == nullModel) {
			return null;
		}

		OktaUserGroup oktaUserGroup = (OktaUserGroup)serializable;

		if (oktaUserGroup == null) {
			Session session = null;

			try {
				session = openSession();

				oktaUserGroup = (OktaUserGroup)session.get(OktaUserGroupImpl.class,
						primaryKey);

				if (oktaUserGroup != null) {
					cacheResult(oktaUserGroup);
				}
				else {
					entityCache.putResult(OktaUserGroupModelImpl.ENTITY_CACHE_ENABLED,
						OktaUserGroupImpl.class, primaryKey, nullModel);
				}
			}
			catch (Exception e) {
				entityCache.removeResult(OktaUserGroupModelImpl.ENTITY_CACHE_ENABLED,
					OktaUserGroupImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return oktaUserGroup;
	}

	/**
	 * Returns the okta user group with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param oktaUserGroupId the primary key of the okta user group
	 * @return the okta user group, or <code>null</code> if a okta user group with the primary key could not be found
	 */
	@Override
	public OktaUserGroup fetchByPrimaryKey(long oktaUserGroupId) {
		return fetchByPrimaryKey((Serializable)oktaUserGroupId);
	}

	@Override
	public Map<Serializable, OktaUserGroup> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {
		if (primaryKeys.isEmpty()) {
			return Collections.emptyMap();
		}

		Map<Serializable, OktaUserGroup> map = new HashMap<Serializable, OktaUserGroup>();

		if (primaryKeys.size() == 1) {
			Iterator<Serializable> iterator = primaryKeys.iterator();

			Serializable primaryKey = iterator.next();

			OktaUserGroup oktaUserGroup = fetchByPrimaryKey(primaryKey);

			if (oktaUserGroup != null) {
				map.put(primaryKey, oktaUserGroup);
			}

			return map;
		}

		Set<Serializable> uncachedPrimaryKeys = null;

		for (Serializable primaryKey : primaryKeys) {
			Serializable serializable = entityCache.getResult(OktaUserGroupModelImpl.ENTITY_CACHE_ENABLED,
					OktaUserGroupImpl.class, primaryKey);

			if (serializable != nullModel) {
				if (serializable == null) {
					if (uncachedPrimaryKeys == null) {
						uncachedPrimaryKeys = new HashSet<Serializable>();
					}

					uncachedPrimaryKeys.add(primaryKey);
				}
				else {
					map.put(primaryKey, (OktaUserGroup)serializable);
				}
			}
		}

		if (uncachedPrimaryKeys == null) {
			return map;
		}

		StringBundler query = new StringBundler((uncachedPrimaryKeys.size() * 2) +
				1);

		query.append(_SQL_SELECT_OKTAUSERGROUP_WHERE_PKS_IN);

		for (Serializable primaryKey : uncachedPrimaryKeys) {
			query.append((long)primaryKey);

			query.append(StringPool.COMMA);
		}

		query.setIndex(query.index() - 1);

		query.append(StringPool.CLOSE_PARENTHESIS);

		String sql = query.toString();

		Session session = null;

		try {
			session = openSession();

			Query q = session.createQuery(sql);

			for (OktaUserGroup oktaUserGroup : (List<OktaUserGroup>)q.list()) {
				map.put(oktaUserGroup.getPrimaryKeyObj(), oktaUserGroup);

				cacheResult(oktaUserGroup);

				uncachedPrimaryKeys.remove(oktaUserGroup.getPrimaryKeyObj());
			}

			for (Serializable primaryKey : uncachedPrimaryKeys) {
				entityCache.putResult(OktaUserGroupModelImpl.ENTITY_CACHE_ENABLED,
					OktaUserGroupImpl.class, primaryKey, nullModel);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		return map;
	}

	/**
	 * Returns all the okta user groups.
	 *
	 * @return the okta user groups
	 */
	@Override
	public List<OktaUserGroup> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the okta user groups.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link OktaUserGroupModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of okta user groups
	 * @param end the upper bound of the range of okta user groups (not inclusive)
	 * @return the range of okta user groups
	 */
	@Override
	public List<OktaUserGroup> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the okta user groups.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link OktaUserGroupModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of okta user groups
	 * @param end the upper bound of the range of okta user groups (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of okta user groups
	 */
	@Override
	public List<OktaUserGroup> findAll(int start, int end,
		OrderByComparator<OktaUserGroup> orderByComparator) {
		return findAll(start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the okta user groups.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link OktaUserGroupModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of okta user groups
	 * @param end the upper bound of the range of okta user groups (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of okta user groups
	 */
	@Override
	public List<OktaUserGroup> findAll(int start, int end,
		OrderByComparator<OktaUserGroup> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<OktaUserGroup> list = null;

		if (retrieveFromCache) {
			list = (List<OktaUserGroup>)finderCache.getResult(finderPath,
					finderArgs, this);
		}

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 2));

				query.append(_SQL_SELECT_OKTAUSERGROUP);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_OKTAUSERGROUP;

				if (pagination) {
					sql = sql.concat(OktaUserGroupModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<OktaUserGroup>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<OktaUserGroup>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the okta user groups from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (OktaUserGroup oktaUserGroup : findAll()) {
			remove(oktaUserGroup);
		}
	}

	/**
	 * Returns the number of okta user groups.
	 *
	 * @return the number of okta user groups
	 */
	@Override
	public int countAll() {
		Long count = (Long)finderCache.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_OKTAUSERGROUP);

				count = (Long)q.uniqueResult();

				finderCache.putResult(FINDER_PATH_COUNT_ALL, FINDER_ARGS_EMPTY,
					count);
			}
			catch (Exception e) {
				finderCache.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected Map<String, Integer> getTableColumnsMap() {
		return OktaUserGroupModelImpl.TABLE_COLUMNS_MAP;
	}

	/**
	 * Initializes the okta user group persistence.
	 */
	public void afterPropertiesSet() {
	}

	public void destroy() {
		entityCache.removeCache(OktaUserGroupImpl.class.getName());
		finderCache.removeCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@ServiceReference(type = EntityCache.class)
	protected EntityCache entityCache;
	@ServiceReference(type = FinderCache.class)
	protected FinderCache finderCache;
	private static final String _SQL_SELECT_OKTAUSERGROUP = "SELECT oktaUserGroup FROM OktaUserGroup oktaUserGroup";
	private static final String _SQL_SELECT_OKTAUSERGROUP_WHERE_PKS_IN = "SELECT oktaUserGroup FROM OktaUserGroup oktaUserGroup WHERE oktaUserGroupId IN (";
	private static final String _SQL_COUNT_OKTAUSERGROUP = "SELECT COUNT(oktaUserGroup) FROM OktaUserGroup oktaUserGroup";
	private static final String _ORDER_BY_ENTITY_ALIAS = "oktaUserGroup.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No OktaUserGroup exists with the primary key ";
	private static final Log _log = LogFactoryUtil.getLog(OktaUserGroupPersistenceImpl.class);
}