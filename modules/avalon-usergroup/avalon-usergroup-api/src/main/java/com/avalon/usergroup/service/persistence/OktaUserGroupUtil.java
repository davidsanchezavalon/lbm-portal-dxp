/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.avalon.usergroup.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.avalon.usergroup.model.OktaUserGroup;

import com.liferay.osgi.util.ServiceTrackerFactory;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import org.osgi.util.tracker.ServiceTracker;

import java.util.List;

/**
 * The persistence utility for the okta user group service. This utility wraps {@link com.avalon.usergroup.service.persistence.impl.OktaUserGroupPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see OktaUserGroupPersistence
 * @see com.avalon.usergroup.service.persistence.impl.OktaUserGroupPersistenceImpl
 * @generated
 */
@ProviderType
public class OktaUserGroupUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(OktaUserGroup oktaUserGroup) {
		getPersistence().clearCache(oktaUserGroup);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<OktaUserGroup> findWithDynamicQuery(
		DynamicQuery dynamicQuery) {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<OktaUserGroup> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<OktaUserGroup> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<OktaUserGroup> orderByComparator) {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static OktaUserGroup update(OktaUserGroup oktaUserGroup) {
		return getPersistence().update(oktaUserGroup);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static OktaUserGroup update(OktaUserGroup oktaUserGroup,
		ServiceContext serviceContext) {
		return getPersistence().update(oktaUserGroup, serviceContext);
	}

	/**
	* Caches the okta user group in the entity cache if it is enabled.
	*
	* @param oktaUserGroup the okta user group
	*/
	public static void cacheResult(OktaUserGroup oktaUserGroup) {
		getPersistence().cacheResult(oktaUserGroup);
	}

	/**
	* Caches the okta user groups in the entity cache if it is enabled.
	*
	* @param oktaUserGroups the okta user groups
	*/
	public static void cacheResult(List<OktaUserGroup> oktaUserGroups) {
		getPersistence().cacheResult(oktaUserGroups);
	}

	/**
	* Creates a new okta user group with the primary key. Does not add the okta user group to the database.
	*
	* @param oktaUserGroupId the primary key for the new okta user group
	* @return the new okta user group
	*/
	public static OktaUserGroup create(long oktaUserGroupId) {
		return getPersistence().create(oktaUserGroupId);
	}

	/**
	* Removes the okta user group with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param oktaUserGroupId the primary key of the okta user group
	* @return the okta user group that was removed
	* @throws NoSuchOktaUserGroupException if a okta user group with the primary key could not be found
	*/
	public static OktaUserGroup remove(long oktaUserGroupId)
		throws com.avalon.usergroup.exception.NoSuchOktaUserGroupException {
		return getPersistence().remove(oktaUserGroupId);
	}

	public static OktaUserGroup updateImpl(OktaUserGroup oktaUserGroup) {
		return getPersistence().updateImpl(oktaUserGroup);
	}

	/**
	* Returns the okta user group with the primary key or throws a {@link NoSuchOktaUserGroupException} if it could not be found.
	*
	* @param oktaUserGroupId the primary key of the okta user group
	* @return the okta user group
	* @throws NoSuchOktaUserGroupException if a okta user group with the primary key could not be found
	*/
	public static OktaUserGroup findByPrimaryKey(long oktaUserGroupId)
		throws com.avalon.usergroup.exception.NoSuchOktaUserGroupException {
		return getPersistence().findByPrimaryKey(oktaUserGroupId);
	}

	/**
	* Returns the okta user group with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param oktaUserGroupId the primary key of the okta user group
	* @return the okta user group, or <code>null</code> if a okta user group with the primary key could not be found
	*/
	public static OktaUserGroup fetchByPrimaryKey(long oktaUserGroupId) {
		return getPersistence().fetchByPrimaryKey(oktaUserGroupId);
	}

	public static java.util.Map<java.io.Serializable, OktaUserGroup> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys) {
		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	* Returns all the okta user groups.
	*
	* @return the okta user groups
	*/
	public static List<OktaUserGroup> findAll() {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the okta user groups.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link OktaUserGroupModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of okta user groups
	* @param end the upper bound of the range of okta user groups (not inclusive)
	* @return the range of okta user groups
	*/
	public static List<OktaUserGroup> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the okta user groups.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link OktaUserGroupModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of okta user groups
	* @param end the upper bound of the range of okta user groups (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of okta user groups
	*/
	public static List<OktaUserGroup> findAll(int start, int end,
		OrderByComparator<OktaUserGroup> orderByComparator) {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the okta user groups.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link OktaUserGroupModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of okta user groups
	* @param end the upper bound of the range of okta user groups (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of okta user groups
	*/
	public static List<OktaUserGroup> findAll(int start, int end,
		OrderByComparator<OktaUserGroup> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findAll(start, end, orderByComparator, retrieveFromCache);
	}

	/**
	* Removes all the okta user groups from the database.
	*/
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of okta user groups.
	*
	* @return the number of okta user groups
	*/
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static OktaUserGroupPersistence getPersistence() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<OktaUserGroupPersistence, OktaUserGroupPersistence> _serviceTracker =
		ServiceTrackerFactory.open(OktaUserGroupPersistence.class);
}