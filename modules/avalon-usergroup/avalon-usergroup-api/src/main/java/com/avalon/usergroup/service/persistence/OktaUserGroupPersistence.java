/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.avalon.usergroup.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.avalon.usergroup.exception.NoSuchOktaUserGroupException;
import com.avalon.usergroup.model.OktaUserGroup;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

/**
 * The persistence interface for the okta user group service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see com.avalon.usergroup.service.persistence.impl.OktaUserGroupPersistenceImpl
 * @see OktaUserGroupUtil
 * @generated
 */
@ProviderType
public interface OktaUserGroupPersistence extends BasePersistence<OktaUserGroup> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link OktaUserGroupUtil} to access the okta user group persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Caches the okta user group in the entity cache if it is enabled.
	*
	* @param oktaUserGroup the okta user group
	*/
	public void cacheResult(OktaUserGroup oktaUserGroup);

	/**
	* Caches the okta user groups in the entity cache if it is enabled.
	*
	* @param oktaUserGroups the okta user groups
	*/
	public void cacheResult(java.util.List<OktaUserGroup> oktaUserGroups);

	/**
	* Creates a new okta user group with the primary key. Does not add the okta user group to the database.
	*
	* @param oktaUserGroupId the primary key for the new okta user group
	* @return the new okta user group
	*/
	public OktaUserGroup create(long oktaUserGroupId);

	/**
	* Removes the okta user group with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param oktaUserGroupId the primary key of the okta user group
	* @return the okta user group that was removed
	* @throws NoSuchOktaUserGroupException if a okta user group with the primary key could not be found
	*/
	public OktaUserGroup remove(long oktaUserGroupId)
		throws NoSuchOktaUserGroupException;

	public OktaUserGroup updateImpl(OktaUserGroup oktaUserGroup);

	/**
	* Returns the okta user group with the primary key or throws a {@link NoSuchOktaUserGroupException} if it could not be found.
	*
	* @param oktaUserGroupId the primary key of the okta user group
	* @return the okta user group
	* @throws NoSuchOktaUserGroupException if a okta user group with the primary key could not be found
	*/
	public OktaUserGroup findByPrimaryKey(long oktaUserGroupId)
		throws NoSuchOktaUserGroupException;

	/**
	* Returns the okta user group with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param oktaUserGroupId the primary key of the okta user group
	* @return the okta user group, or <code>null</code> if a okta user group with the primary key could not be found
	*/
	public OktaUserGroup fetchByPrimaryKey(long oktaUserGroupId);

	@Override
	public java.util.Map<java.io.Serializable, OktaUserGroup> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys);

	/**
	* Returns all the okta user groups.
	*
	* @return the okta user groups
	*/
	public java.util.List<OktaUserGroup> findAll();

	/**
	* Returns a range of all the okta user groups.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link OktaUserGroupModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of okta user groups
	* @param end the upper bound of the range of okta user groups (not inclusive)
	* @return the range of okta user groups
	*/
	public java.util.List<OktaUserGroup> findAll(int start, int end);

	/**
	* Returns an ordered range of all the okta user groups.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link OktaUserGroupModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of okta user groups
	* @param end the upper bound of the range of okta user groups (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of okta user groups
	*/
	public java.util.List<OktaUserGroup> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<OktaUserGroup> orderByComparator);

	/**
	* Returns an ordered range of all the okta user groups.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link OktaUserGroupModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of okta user groups
	* @param end the upper bound of the range of okta user groups (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of okta user groups
	*/
	public java.util.List<OktaUserGroup> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<OktaUserGroup> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Removes all the okta user groups from the database.
	*/
	public void removeAll();

	/**
	* Returns the number of okta user groups.
	*
	* @return the number of okta user groups
	*/
	public int countAll();
}