/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.avalon.usergroup.model;

import aQute.bnd.annotation.ProviderType;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link com.avalon.usergroup.service.http.OktaUserGroupServiceSoap}.
 *
 * @author Brian Wing Shun Chan
 * @see com.avalon.usergroup.service.http.OktaUserGroupServiceSoap
 * @generated
 */
@ProviderType
public class OktaUserGroupSoap implements Serializable {
	public static OktaUserGroupSoap toSoapModel(OktaUserGroup model) {
		OktaUserGroupSoap soapModel = new OktaUserGroupSoap();

		soapModel.setOktaUserGroupId(model.getOktaUserGroupId());

		return soapModel;
	}

	public static OktaUserGroupSoap[] toSoapModels(OktaUserGroup[] models) {
		OktaUserGroupSoap[] soapModels = new OktaUserGroupSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static OktaUserGroupSoap[][] toSoapModels(OktaUserGroup[][] models) {
		OktaUserGroupSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new OktaUserGroupSoap[models.length][models[0].length];
		}
		else {
			soapModels = new OktaUserGroupSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static OktaUserGroupSoap[] toSoapModels(List<OktaUserGroup> models) {
		List<OktaUserGroupSoap> soapModels = new ArrayList<OktaUserGroupSoap>(models.size());

		for (OktaUserGroup model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new OktaUserGroupSoap[soapModels.size()]);
	}

	public OktaUserGroupSoap() {
	}

	public long getPrimaryKey() {
		return _oktaUserGroupId;
	}

	public void setPrimaryKey(long pk) {
		setOktaUserGroupId(pk);
	}

	public long getOktaUserGroupId() {
		return _oktaUserGroupId;
	}

	public void setOktaUserGroupId(long oktaUserGroupId) {
		_oktaUserGroupId = oktaUserGroupId;
	}

	private long _oktaUserGroupId;
}