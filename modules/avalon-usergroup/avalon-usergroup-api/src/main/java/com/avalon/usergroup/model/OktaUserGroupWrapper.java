/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.avalon.usergroup.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * This class is a wrapper for {@link OktaUserGroup}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see OktaUserGroup
 * @generated
 */
@ProviderType
public class OktaUserGroupWrapper implements OktaUserGroup,
	ModelWrapper<OktaUserGroup> {
	public OktaUserGroupWrapper(OktaUserGroup oktaUserGroup) {
		_oktaUserGroup = oktaUserGroup;
	}

	@Override
	public Class<?> getModelClass() {
		return OktaUserGroup.class;
	}

	@Override
	public String getModelClassName() {
		return OktaUserGroup.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("oktaUserGroupId", getOktaUserGroupId());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long oktaUserGroupId = (Long)attributes.get("oktaUserGroupId");

		if (oktaUserGroupId != null) {
			setOktaUserGroupId(oktaUserGroupId);
		}
	}

	@Override
	public OktaUserGroup toEscapedModel() {
		return new OktaUserGroupWrapper(_oktaUserGroup.toEscapedModel());
	}

	@Override
	public OktaUserGroup toUnescapedModel() {
		return new OktaUserGroupWrapper(_oktaUserGroup.toUnescapedModel());
	}

	@Override
	public boolean isCachedModel() {
		return _oktaUserGroup.isCachedModel();
	}

	@Override
	public boolean isEscapedModel() {
		return _oktaUserGroup.isEscapedModel();
	}

	@Override
	public boolean isNew() {
		return _oktaUserGroup.isNew();
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return _oktaUserGroup.getExpandoBridge();
	}

	@Override
	public com.liferay.portal.kernel.model.CacheModel<OktaUserGroup> toCacheModel() {
		return _oktaUserGroup.toCacheModel();
	}

	@Override
	public int compareTo(OktaUserGroup oktaUserGroup) {
		return _oktaUserGroup.compareTo(oktaUserGroup);
	}

	@Override
	public int hashCode() {
		return _oktaUserGroup.hashCode();
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _oktaUserGroup.getPrimaryKeyObj();
	}

	@Override
	public java.lang.Object clone() {
		return new OktaUserGroupWrapper((OktaUserGroup)_oktaUserGroup.clone());
	}

	@Override
	public java.lang.String toString() {
		return _oktaUserGroup.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _oktaUserGroup.toXmlString();
	}

	/**
	* Returns the okta user group ID of this okta user group.
	*
	* @return the okta user group ID of this okta user group
	*/
	@Override
	public long getOktaUserGroupId() {
		return _oktaUserGroup.getOktaUserGroupId();
	}

	/**
	* Returns the primary key of this okta user group.
	*
	* @return the primary key of this okta user group
	*/
	@Override
	public long getPrimaryKey() {
		return _oktaUserGroup.getPrimaryKey();
	}

	@Override
	public void persist() {
		_oktaUserGroup.persist();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_oktaUserGroup.setCachedModel(cachedModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge) {
		_oktaUserGroup.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.kernel.model.BaseModel<?> baseModel) {
		_oktaUserGroup.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		_oktaUserGroup.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public void setNew(boolean n) {
		_oktaUserGroup.setNew(n);
	}

	/**
	* Sets the okta user group ID of this okta user group.
	*
	* @param oktaUserGroupId the okta user group ID of this okta user group
	*/
	@Override
	public void setOktaUserGroupId(long oktaUserGroupId) {
		_oktaUserGroup.setOktaUserGroupId(oktaUserGroupId);
	}

	/**
	* Sets the primary key of this okta user group.
	*
	* @param primaryKey the primary key of this okta user group
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_oktaUserGroup.setPrimaryKey(primaryKey);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		_oktaUserGroup.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof OktaUserGroupWrapper)) {
			return false;
		}

		OktaUserGroupWrapper oktaUserGroupWrapper = (OktaUserGroupWrapper)obj;

		if (Objects.equals(_oktaUserGroup, oktaUserGroupWrapper._oktaUserGroup)) {
			return true;
		}

		return false;
	}

	@Override
	public OktaUserGroup getWrappedModel() {
		return _oktaUserGroup;
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return _oktaUserGroup.isEntityCacheEnabled();
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return _oktaUserGroup.isFinderCacheEnabled();
	}

	@Override
	public void resetOriginalValues() {
		_oktaUserGroup.resetOriginalValues();
	}

	private final OktaUserGroup _oktaUserGroup;
}