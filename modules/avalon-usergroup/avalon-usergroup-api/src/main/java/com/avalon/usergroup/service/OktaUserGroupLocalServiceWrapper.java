/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.avalon.usergroup.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link OktaUserGroupLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see OktaUserGroupLocalService
 * @generated
 */
@ProviderType
public class OktaUserGroupLocalServiceWrapper
	implements OktaUserGroupLocalService,
		ServiceWrapper<OktaUserGroupLocalService> {
	public OktaUserGroupLocalServiceWrapper(
		OktaUserGroupLocalService oktaUserGroupLocalService) {
		_oktaUserGroupLocalService = oktaUserGroupLocalService;
	}

	/**
	* Adds the okta user group to the database. Also notifies the appropriate model listeners.
	*
	* @param oktaUserGroup the okta user group
	* @return the okta user group that was added
	*/
	@Override
	public com.avalon.usergroup.model.OktaUserGroup addOktaUserGroup(
		com.avalon.usergroup.model.OktaUserGroup oktaUserGroup) {
		return _oktaUserGroupLocalService.addOktaUserGroup(oktaUserGroup);
	}

	/**
	* Creates a new okta user group with the primary key. Does not add the okta user group to the database.
	*
	* @param oktaUserGroupId the primary key for the new okta user group
	* @return the new okta user group
	*/
	@Override
	public com.avalon.usergroup.model.OktaUserGroup createOktaUserGroup(
		long oktaUserGroupId) {
		return _oktaUserGroupLocalService.createOktaUserGroup(oktaUserGroupId);
	}

	/**
	* Deletes the okta user group from the database. Also notifies the appropriate model listeners.
	*
	* @param oktaUserGroup the okta user group
	* @return the okta user group that was removed
	*/
	@Override
	public com.avalon.usergroup.model.OktaUserGroup deleteOktaUserGroup(
		com.avalon.usergroup.model.OktaUserGroup oktaUserGroup) {
		return _oktaUserGroupLocalService.deleteOktaUserGroup(oktaUserGroup);
	}

	/**
	* Deletes the okta user group with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param oktaUserGroupId the primary key of the okta user group
	* @return the okta user group that was removed
	* @throws PortalException if a okta user group with the primary key could not be found
	*/
	@Override
	public com.avalon.usergroup.model.OktaUserGroup deleteOktaUserGroup(
		long oktaUserGroupId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _oktaUserGroupLocalService.deleteOktaUserGroup(oktaUserGroupId);
	}

	@Override
	public com.avalon.usergroup.model.OktaUserGroup fetchOktaUserGroup(
		long oktaUserGroupId) {
		return _oktaUserGroupLocalService.fetchOktaUserGroup(oktaUserGroupId);
	}

	/**
	* Returns the okta user group with the primary key.
	*
	* @param oktaUserGroupId the primary key of the okta user group
	* @return the okta user group
	* @throws PortalException if a okta user group with the primary key could not be found
	*/
	@Override
	public com.avalon.usergroup.model.OktaUserGroup getOktaUserGroup(
		long oktaUserGroupId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _oktaUserGroupLocalService.getOktaUserGroup(oktaUserGroupId);
	}

	/**
	* Updates the okta user group in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param oktaUserGroup the okta user group
	* @return the okta user group that was updated
	*/
	@Override
	public com.avalon.usergroup.model.OktaUserGroup updateOktaUserGroup(
		com.avalon.usergroup.model.OktaUserGroup oktaUserGroup) {
		return _oktaUserGroupLocalService.updateOktaUserGroup(oktaUserGroup);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery getActionableDynamicQuery() {
		return _oktaUserGroupLocalService.getActionableDynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _oktaUserGroupLocalService.dynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery getIndexableActionableDynamicQuery() {
		return _oktaUserGroupLocalService.getIndexableActionableDynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.model.ListType update(
		com.liferay.portal.kernel.model.ListType listType)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _oktaUserGroupLocalService.update(listType);
	}

	/**
	* @throws PortalException
	*/
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
		com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _oktaUserGroupLocalService.deletePersistedModel(persistedModel);
	}

	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _oktaUserGroupLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns the number of okta user groups.
	*
	* @return the number of okta user groups
	*/
	@Override
	public int getOktaUserGroupsCount() {
		return _oktaUserGroupLocalService.getOktaUserGroupsCount();
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	@Override
	public java.lang.String getOSGiServiceIdentifier() {
		return _oktaUserGroupLocalService.getOSGiServiceIdentifier();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _oktaUserGroupLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.avalon.usergroup.model.impl.OktaUserGroupModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {
		return _oktaUserGroupLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.avalon.usergroup.model.impl.OktaUserGroupModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {
		return _oktaUserGroupLocalService.dynamicQuery(dynamicQuery, start,
			end, orderByComparator);
	}

	@Override
	public java.util.List<com.liferay.portal.kernel.model.UserGroup> getNonMemberOktaUserGroups(
		long companyId, long userId, java.lang.String fieldName,
		long classNameId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _oktaUserGroupLocalService.getNonMemberOktaUserGroups(companyId,
			userId, fieldName, classNameId);
	}

	/**
	* Returns a range of all the okta user groups.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.avalon.usergroup.model.impl.OktaUserGroupModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of okta user groups
	* @param end the upper bound of the range of okta user groups (not inclusive)
	* @return the range of okta user groups
	*/
	@Override
	public java.util.List<com.avalon.usergroup.model.OktaUserGroup> getOktaUserGroups(
		int start, int end) {
		return _oktaUserGroupLocalService.getOktaUserGroups(start, end);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _oktaUserGroupLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {
		return _oktaUserGroupLocalService.dynamicQueryCount(dynamicQuery,
			projection);
	}

	@Override
	public OktaUserGroupLocalService getWrappedService() {
		return _oktaUserGroupLocalService;
	}

	@Override
	public void setWrappedService(
		OktaUserGroupLocalService oktaUserGroupLocalService) {
		_oktaUserGroupLocalService = oktaUserGroupLocalService;
	}

	private OktaUserGroupLocalService _oktaUserGroupLocalService;
}