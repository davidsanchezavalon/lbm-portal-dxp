package com.avalon.esb.servicehelpers;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

public class EsbUrlDelegate {
	
	private static final Log log = LogFactoryUtil.getLog(EsbUrlDelegate.class.getName());

    private static EsbUrlDelegate esbUrlDelegate = null;
    private static Properties properties = null;

    public static EsbUrlDelegate getEsbUrlDelegate() {
		return esbUrlDelegate;
    }

    /**
     * Fetching the esb end point urls from the properties object Setting to the
     * esb end point urls in the map object
     * 
     */
    public Map<String, String> getEsbUrl() {
		ConcurrentHashMap<String, String> urls = new ConcurrentHashMap<String, String>();
		try {
		    String serverInstance = System.getProperty("serverInstance");
		   log.info("ServerInstanceAtEsbDelegate :" + serverInstance);
		   log.info("memberBenefits : " + properties.getProperty(serverInstance + ".getMemberBenefits.endpoint"));
		   log.info("findClaims : " + properties.getProperty(serverInstance + ".searchClaim.endpoint"));
		    urls.put("memberBenefits", properties.getProperty(serverInstance + ".getMemberBenefits.endpoint"));
		    urls.put("findClaims", properties.getProperty(serverInstance + ".searchClaim.endpoint"));
		} catch (Exception exception) {
			log.error("Exception in getEsbUrl method",exception);

	 	    StackTraceElement[] elements = Thread.currentThread().getStackTrace();
		    for (int i = 1; i < elements.length; i++) {
		    	StackTraceElement s = elements[i];
		    	System.out.println("\tat " + s.getClassName() + "." + s.getMethodName() + "(" + s.getFileName() + ":" + s.getLineNumber() + ")");
		    }
		}
		return urls;
    }

    /**
     * To load the lbm-common.properties files from the server bin directory and to
     * fetch the esb-end point urls.
     * 
     * @return
     */
    static {
		if (properties == null || properties.size()==0) {
		    properties = new Properties();
		    esbUrlDelegate = new EsbUrlDelegate();
		    String liferayHome = System.getProperty("LIFERAY_HOME");
		    String propertyLocation = liferayHome + "/lbm-common.properties";
		    try {
				properties.load(new FileInputStream(new File(propertyLocation)));
				log.info("EsbUrlDelegate for memberservice::propertyLocation: " + propertyLocation);
		    } catch (FileNotFoundException exception) {
		    	log.error("FileNotFoundException in static block",exception);
		    	
		 	    StackTraceElement[] elements = Thread.currentThread().getStackTrace();
			    for (int i = 1; i < elements.length; i++) {
			    	StackTraceElement s = elements[i];
			    	System.out.println("\tat " + s.getClassName() + "." + s.getMethodName() + "(" + s.getFileName() + ":" + s.getLineNumber() + ")");
			    }
		    } catch (IOException exception) {
		    	log.error("IOException in static block",exception);
		    	
		 	    StackTraceElement[] elements = Thread.currentThread().getStackTrace();
			    for (int i = 1; i < elements.length; i++) {
			    	StackTraceElement s = elements[i];
			    	System.out.println("\tat " + s.getClassName() + "." + s.getMethodName() + "(" + s.getFileName() + ":" + s.getLineNumber() + ")");
			    }
		    }
		}
    }
}
