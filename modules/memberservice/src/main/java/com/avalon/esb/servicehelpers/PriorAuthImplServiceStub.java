package com.avalon.esb.servicehelpers;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

/**
 * Description This file contain the default methods for member services.
 *
 * CHANGE History Version 1.0 Initial version
 * 
 */

public class PriorAuthImplServiceStub extends org.apache.axis2.client.Stub {

	private static final Log log = LogFactoryUtil.getLog(PriorAuthImplServiceStub.class.getName());

	private static int counter = 0;
	protected org.apache.axis2.description.AxisOperation[] _operations;

	// hashmaps to keep the fault mapping
	private java.util.HashMap faultExceptionNameMap = new java.util.HashMap();
	private java.util.HashMap faultExceptionClassNameMap = new java.util.HashMap();
	private java.util.HashMap faultMessageMap = new java.util.HashMap();
	private javax.xml.namespace.QName[] opNameArray = null;

	/**
	 * Constructor that takes in a configContext
	 */
	public PriorAuthImplServiceStub(org.apache.axis2.context.ConfigurationContext configurationContext, java.lang.String targetEndpoint)
			throws org.apache.axis2.AxisFault {
		this(configurationContext, targetEndpoint, false);
	}

	/**
	 * Constructor that takes in a configContext and useseperate listner
	 */
	public PriorAuthImplServiceStub(org.apache.axis2.context.ConfigurationContext configurationContext, java.lang.String targetEndpoint,
			boolean useSeparateListener) throws org.apache.axis2.AxisFault {
		// To populate AxisService
		populateAxisService();
		populateFaults();

		_serviceClient = new org.apache.axis2.client.ServiceClient(configurationContext, _service);

		_serviceClient.getOptions().setTo(new org.apache.axis2.addressing.EndpointReference(targetEndpoint));
		_serviceClient.getOptions().setUseSeparateListener(useSeparateListener);
	}

	/**
	 * Default Constructor To set the esb end point url
	 */
	public PriorAuthImplServiceStub(org.apache.axis2.context.ConfigurationContext configurationContext) throws org.apache.axis2.AxisFault {
		this(configurationContext, EsbUrlDelegate.getEsbUrlDelegate().getEsbUrl().get("memberBenefits")); // To Fetch evironment
		// based esb urls
		// "http://10.194.245.77:9090/priorAuthService/services/GetMemberBenefits");
	}

	/**
	 * Default Constructor
	 */
	public PriorAuthImplServiceStub() throws org.apache.axis2.AxisFault {
		this(
		// "http://10.194.245.77:9090/priorAuthService/services/GetMemberBenefits");
				EsbUrlDelegate.getEsbUrlDelegate().getEsbUrl().get("memberBenefits")

		); // To Fetch evironment based esb urls
	}

	/**
	 * Constructor taking the target endpoint
	 */
	public PriorAuthImplServiceStub(java.lang.String targetEndpoint) throws org.apache.axis2.AxisFault {
		this(null, targetEndpoint);
	}

	private static synchronized java.lang.String getUniqueSuffix() {
		// reset the counter if it is greater than 99999
		if (counter > 99999) {
			counter = 0;
		}

		counter = counter + 1;

		return java.lang.Long.toString(java.lang.System.currentTimeMillis()) + "_" + counter;
	}

	private void populateAxisService() throws org.apache.axis2.AxisFault {
		// creating the Service with a unique name
		_service = new org.apache.axis2.description.AxisService("PriorAuthImplService" + getUniqueSuffix());
		addAnonymousOperations();

		// creating the operations
		org.apache.axis2.description.AxisOperation __operation;

		_operations = new org.apache.axis2.description.AxisOperation[1];

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://servicehelpers.esb.avalon.com", "getMemberData"));
		_service.addOperation(__operation);

		_operations[0] = __operation;
	}

	// populates the faults
	private void populateFaults() {
	}

	/**
	 * Auto generated method signature
	 *
	 * @see com.avalon.esb.servicehelpers.PriorAuthImplService#getMemberData
	 * @param getMemberData
	 */
	public com.avalon.esb.servicehelpers.PriorAuthImplServiceStub.GetMemberDataResponse getMemberData(
			com.avalon.esb.servicehelpers.PriorAuthImplServiceStub.GetMemberData getMemberData) throws java.rmi.RemoteException {
		org.apache.axis2.context.MessageContext _messageContext = null;

		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[0].getName());
			_operationClient.getOptions().setAction("http://servicehelpers.esb.avalon.com/PriorAuthService/getMemberDataRequest");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create a message context
			_messageContext = new org.apache.axis2.context.MessageContext();

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), getMemberData, optimizeContent(new javax.xml.namespace.QName(
					"http://servicehelpers.esb.avalon.com", "getMemberData")), new javax.xml.namespace.QName("http://servicehelpers.esb.avalon.com",
					"getMemberData"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient
					.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope(); // Response xml

			Unmarshaller unmarshaller;
			try {
				unmarshaller = JAXBContext.newInstance(MemberDataResponse.class).createUnmarshaller();
				// MemberDataResponse memberDataResponse =
				// (MemberDataResponse)unmarshaller.unmarshal(_returnEnv.getBody().getXMLStreamReader());
				// System.out.println("memberDataResponse : "
				// +memberDataResponse);
			} catch (JAXBException e) {
				log.error("JAXBException in getMemberData method",e);
			}

			log.info("_returnEnv: " + _returnEnv.getBody());

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(),
					com.avalon.esb.servicehelpers.PriorAuthImplServiceStub.GetMemberDataResponse.class, getEnvelopeNamespaces(_returnEnv));

			return (com.avalon.esb.servicehelpers.PriorAuthImplServiceStub.GetMemberDataResponse) object;
		} catch (org.apache.axis2.AxisFault f) {
			org.apache.axiom.om.OMElement faultElt = f.getDetail();

			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "getMemberData"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = (java.lang.String) faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(
								faultElt.getQName(), "getMemberData"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());

						// message class
						java.lang.String messageClassName = (java.lang.String) faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(),
								"getMemberData"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass, null);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * A utility method that copies the namepaces from the SOAPEnvelope
	 */
	private java.util.Map getEnvelopeNamespaces(org.apache.axiom.soap.SOAPEnvelope env) {
		java.util.Map returnMap = new java.util.HashMap();
		java.util.Iterator namespaceIterator = env.getAllDeclaredNamespaces();

		while (namespaceIterator.hasNext()) {
			org.apache.axiom.om.OMNamespace ns = (org.apache.axiom.om.OMNamespace) namespaceIterator.next();
			returnMap.put(ns.getPrefix(), ns.getNamespaceURI());
		}

		return returnMap;
	}

	private boolean optimizeContent(javax.xml.namespace.QName opName) {
		if (opNameArray == null) {
			return false;
		}

		for (int i = 0; i < opNameArray.length; i++) {
			if (opName.equals(opNameArray[i])) {
				return true;
			}
		}

		return false;
	}

	private org.apache.axiom.om.OMElement toOM(com.avalon.esb.servicehelpers.PriorAuthImplServiceStub.GetMemberData param, boolean optimizeContent)
			throws org.apache.axis2.AxisFault {
		try {
			return param.getOMElement(com.avalon.esb.servicehelpers.PriorAuthImplServiceStub.GetMemberData.MY_QNAME,
					org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(com.avalon.esb.servicehelpers.PriorAuthImplServiceStub.GetMemberDataResponse param, boolean optimizeContent)
			throws org.apache.axis2.AxisFault {
		try {
			return param.getOMElement(com.avalon.esb.servicehelpers.PriorAuthImplServiceStub.GetMemberDataResponse.MY_QNAME,
					org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory,
			com.avalon.esb.servicehelpers.PriorAuthImplServiceStub.GetMemberData param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
			throws org.apache.axis2.AxisFault {
		try {
			org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();

			emptyEnvelope.getBody().addChild(param.getOMElement(com.avalon.esb.servicehelpers.PriorAuthImplServiceStub.GetMemberData.MY_QNAME, factory));

			return emptyEnvelope;
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	/* methods to provide back word compatibility */

	/**
	 * get the default envelope
	 */
	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory) {
		return factory.getDefaultEnvelope();
	}

	private java.lang.Object fromOM(org.apache.axiom.om.OMElement param, java.lang.Class type, java.util.Map extraNamespaces) throws org.apache.axis2.AxisFault {
		try {
			if (com.avalon.esb.servicehelpers.PriorAuthImplServiceStub.GetMemberData.class.equals(type)) {
				return com.avalon.esb.servicehelpers.PriorAuthImplServiceStub.GetMemberData.Factory.parse(param.getXMLStreamReaderWithoutCaching());
			}

			if (com.avalon.esb.servicehelpers.PriorAuthImplServiceStub.GetMemberDataResponse.class.equals(type)) {
				return com.avalon.esb.servicehelpers.PriorAuthImplServiceStub.GetMemberDataResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
			}
		} catch (java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}

		return null;
	}

	// http://10.194.245.82:9090/priorAuthService/services/GetMemberBenefits
	public static class CoverageList implements org.apache.axis2.databinding.ADBBean {
		/*
		 * This type was generated from the piece of schema that had name = CoverageList Namespace URI = http://servicehelpers.esb.avalon.com Namespace Prefix =
		 * ns1
		 */

		/**
		 * field for CoverageCount
		 */
		protected java.lang.String localCoverageCount;

		/**
		 * field for Coverage This was an Array!
		 */
		protected Coverage[] localCoverage;

		/**
		 * Auto generated getter method
		 * 
		 * @return java.lang.String
		 */
		public java.lang.String getCoverageCount() {
			return localCoverageCount;
		}

		/**
		 * Auto generated setter method
		 * 
		 * @param param
		 *            CoverageCount
		 */
		public void setCoverageCount(java.lang.String param) {
			this.localCoverageCount = param;
		}

		/**
		 * Auto generated getter method
		 * 
		 * @return Coverage[]
		 */
		public Coverage[] getCoverage() {
			return localCoverage;
		}

		/**
		 * validate the array for Coverage
		 */
		protected void validateCoverage(Coverage[] param) {
			if ((param != null) && (param.length > 10)) {
				throw new java.lang.RuntimeException("Input values do not follow defined XSD restrictions");
			}

			if ((param != null) && (param.length < 1)) {
				throw new java.lang.RuntimeException("Input values do not follow defined XSD restrictions");
			}
		}

		/**
		 * Auto generated setter method
		 * 
		 * @param param
		 *            Coverage
		 */
		public void setCoverage(Coverage[] param) {
			validateCoverage(param);

			this.localCoverage = param;
		}

		/**
		 * Auto generated add method for the array for convenience
		 * 
		 * @param param
		 *            Coverage
		 */
		public void addCoverage(Coverage param) {
			if (localCoverage == null) {
				localCoverage = new Coverage[] {};
			}

			java.util.List list = org.apache.axis2.databinding.utils.ConverterUtil.toList(localCoverage);
			list.add(param);
			this.localCoverage = (Coverage[]) list.toArray(new Coverage[list.size()]);
		}

		/**
		 *
		 * @param parentQName
		 * @param factory
		 * @return org.apache.axiom.om.OMElement
		 */
		public org.apache.axiom.om.OMElement getOMElement(final javax.xml.namespace.QName parentQName, final org.apache.axiom.om.OMFactory factory)
				throws org.apache.axis2.databinding.ADBException {
			org.apache.axiom.om.OMDataSource dataSource = new org.apache.axis2.databinding.ADBDataSource(this, parentQName);

			return factory.createOMElement(dataSource, parentQName);
		}

		public void serialize(final javax.xml.namespace.QName parentQName, javax.xml.stream.XMLStreamWriter xmlWriter)
				throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {
			serialize(parentQName, xmlWriter, false);
		}

		public void serialize(final javax.xml.namespace.QName parentQName, javax.xml.stream.XMLStreamWriter xmlWriter, boolean serializeType)
				throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {
			java.lang.String prefix = null;
			java.lang.String namespace = null;

			prefix = parentQName.getPrefix();
			namespace = parentQName.getNamespaceURI();
			writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);

			if (serializeType) {
				java.lang.String namespacePrefix = registerPrefix(xmlWriter, "http://servicehelpers.esb.avalon.com");

				if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)) {
					writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", namespacePrefix + ":CoverageList", xmlWriter);
				} else {
					writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", "CoverageList", xmlWriter);
				}
			}

			namespace = "";
			writeStartElement(null, namespace, "coverageCount", xmlWriter);

			if (localCoverageCount == null) {
				// write the nil attribute
				throw new org.apache.axis2.databinding.ADBException("coverageCount cannot be null!!");
			} else {
				xmlWriter.writeCharacters(localCoverageCount);
			}

			xmlWriter.writeEndElement();

			if (localCoverage != null) {
				for (int i = 0; i < localCoverage.length; i++) {
					if (localCoverage[i] != null) {
						localCoverage[i].serialize(new javax.xml.namespace.QName("", "coverage"), xmlWriter);
					} else {
						throw new org.apache.axis2.databinding.ADBException("coverage cannot be null!!");
					}
				}
			} else {
				throw new org.apache.axis2.databinding.ADBException("coverage cannot be null!!");
			}

			xmlWriter.writeEndElement();
		}

		private static java.lang.String generatePrefix(java.lang.String namespace) {
			if (namespace.equals("http://servicehelpers.esb.avalon.com")) {
				return "ns1";
			}

			return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
		}

		/**
		 * Utility method to write an element start tag.
		 */
		private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
				javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
			java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

			if (writerPrefix != null) {
				xmlWriter.writeStartElement(namespace, localPart);
			} else {
				if (namespace.length() == 0) {
					prefix = "";
				} else if (prefix == null) {
					prefix = generatePrefix(namespace);
				}

				xmlWriter.writeStartElement(prefix, localPart, namespace);
				xmlWriter.writeNamespace(prefix, namespace);
				xmlWriter.setPrefix(prefix, namespace);
			}
		}

		/**
		 * Util method to write an attribute with the ns prefix
		 */
		private void writeAttribute(java.lang.String prefix, java.lang.String namespace, java.lang.String attName, java.lang.String attValue,
				javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
			if (xmlWriter.getPrefix(namespace) == null) {
				xmlWriter.writeNamespace(prefix, namespace);
				xmlWriter.setPrefix(prefix, namespace);
			}

			xmlWriter.writeAttribute(namespace, attName, attValue);
		}

		/**
		 * Util method to write an attribute without the ns prefix
		 */
		private void writeAttribute(java.lang.String namespace, java.lang.String attName, java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter)
				throws javax.xml.stream.XMLStreamException {
			if (namespace.equals("")) {
				xmlWriter.writeAttribute(attName, attValue);
			} else {
				registerPrefix(xmlWriter, namespace);
				xmlWriter.writeAttribute(namespace, attName, attValue);
			}
		}

		/**
		 * Util method to write an attribute without the ns prefix
		 */
		private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName, javax.xml.namespace.QName qname,
				javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
			java.lang.String attributeNamespace = qname.getNamespaceURI();
			java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);

			if (attributePrefix == null) {
				attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
			}

			java.lang.String attributeValue;

			if (attributePrefix.trim().length() > 0) {
				attributeValue = attributePrefix + ":" + qname.getLocalPart();
			} else {
				attributeValue = qname.getLocalPart();
			}

			if (namespace.equals("")) {
				xmlWriter.writeAttribute(attName, attributeValue);
			} else {
				registerPrefix(xmlWriter, namespace);
				xmlWriter.writeAttribute(namespace, attName, attributeValue);
			}
		}

		/**
		 * method to handle Qnames
		 */
		private void writeQName(javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
			java.lang.String namespaceURI = qname.getNamespaceURI();

			if (namespaceURI != null) {
				java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);

				if (prefix == null) {
					prefix = generatePrefix(namespaceURI);
					xmlWriter.writeNamespace(prefix, namespaceURI);
					xmlWriter.setPrefix(prefix, namespaceURI);
				}

				if (prefix.trim().length() > 0) {
					xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
				} else {
					// i.e this is the default namespace
					xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
				}
			} else {
				xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
			}
		}

		private void writeQNames(javax.xml.namespace.QName[] qnames, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
			if (qnames != null) {
				// we have to store this data until last moment since it is not
				// possible to write any
				// namespace data after writing the charactor data
				java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
				java.lang.String namespaceURI = null;
				java.lang.String prefix = null;

				for (int i = 0; i < qnames.length; i++) {
					if (i > 0) {
						stringToWrite.append(" ");
					}

					namespaceURI = qnames[i].getNamespaceURI();

					if (namespaceURI != null) {
						prefix = xmlWriter.getPrefix(namespaceURI);

						if ((prefix == null) || (prefix.length() == 0)) {
							prefix = generatePrefix(namespaceURI);
							xmlWriter.writeNamespace(prefix, namespaceURI);
							xmlWriter.setPrefix(prefix, namespaceURI);
						}

						if (prefix.trim().length() > 0) {
							stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
						} else {
							stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
						}
					} else {
						stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
					}
				}

				xmlWriter.writeCharacters(stringToWrite.toString());
			}
		}

		/**
		 * Register a namespace prefix
		 */
		private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace)
				throws javax.xml.stream.XMLStreamException {
			java.lang.String prefix = xmlWriter.getPrefix(namespace);

			if (prefix == null) {
				prefix = generatePrefix(namespace);

				javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();

				while (true) {
					java.lang.String uri = nsContext.getNamespaceURI(prefix);

					if ((uri == null) || (uri.length() == 0)) {
						break;
					}

					prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
				}

				xmlWriter.writeNamespace(prefix, namespace);
				xmlWriter.setPrefix(prefix, namespace);
			}

			return prefix;
		}

		/**
		 * databinding method to get an XML representation of this object
		 *
		 */
		public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName) throws org.apache.axis2.databinding.ADBException {
			java.util.ArrayList elementList = new java.util.ArrayList();
			java.util.ArrayList attribList = new java.util.ArrayList();

			elementList.add(new javax.xml.namespace.QName("", "coverageCount"));

			if (localCoverageCount != null) {
				elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCoverageCount));
			} else {
				throw new org.apache.axis2.databinding.ADBException("coverageCount cannot be null!!");
			}

			if (localCoverage != null) {
				for (int i = 0; i < localCoverage.length; i++) {
					if (localCoverage[i] != null) {
						elementList.add(new javax.xml.namespace.QName("", "coverage"));
						elementList.add(localCoverage[i]);
					} else {
						throw new org.apache.axis2.databinding.ADBException("coverage cannot be null !!");
					}
				}
			} else {
				throw new org.apache.axis2.databinding.ADBException("coverage cannot be null!!");
			}

			return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
		}

		/**
		 * Factory class that keeps the parse method
		 */
		public static class Factory {
			/**
			 * static method to create the object Precondition: If this object is an element, the current or next start element starts this object and any
			 * intervening reader events are ignorable If this object is not an element, it is a complex type and the reader is at the event just after the
			 * outer start element Postcondition: If this object is an element, the reader is positioned at its end element If this object is a complex type,
			 * the reader is positioned at the end element of its outer element
			 */
			public static CoverageList parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception {
				CoverageList object = new CoverageList();

				int event;
				java.lang.String nillableValue = null;
				java.lang.String prefix = "";
				java.lang.String namespaceuri = "";

				try {
					while (!reader.isStartElement() && !reader.isEndElement())
						reader.next();

					if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
						java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type");

						if (fullTypeName != null) {
							java.lang.String nsPrefix = null;

							if (fullTypeName.indexOf(":") > -1) {
								nsPrefix = fullTypeName.substring(0, fullTypeName.indexOf(":"));
							}

							nsPrefix = (nsPrefix == null) ? "" : nsPrefix;

							java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":") + 1);

							if (!"CoverageList".equals(type)) {
								// find namespace for the prefix
								java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);

								return (CoverageList) ExtensionMapper.getTypeObject(nsUri, type, reader);
							}
						}
					}

					// Note all attributes that were handled. Used to differ
					// normal attributes
					// from anyAttributes.
					java.util.Vector handledAttributes = new java.util.Vector();

					reader.next();

					java.util.ArrayList list2 = new java.util.ArrayList();

					while (!reader.isStartElement() && !reader.isEndElement())
						reader.next();

					if (reader.isStartElement() && new javax.xml.namespace.QName("", "coverageCount").equals(reader.getName())) {
						nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");

						if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
							throw new org.apache.axis2.databinding.ADBException("The element: " + "coverageCount" + "  cannot be null");
						}

						java.lang.String content = reader.getElementText();

						object.setCoverageCount(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

						reader.next();
					} // End of if for expected property start element

					else {
						// A start element we are not expecting indicates an
						// invalid parameter was passed
						throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
					}

					while (!reader.isStartElement() && !reader.isEndElement())
						reader.next();

					if (reader.isStartElement() && new javax.xml.namespace.QName("", "coverage").equals(reader.getName())) {
						// Process the array and step past its final element's
						// end.
						list2.add(Coverage.Factory.parse(reader));

						// loop until we find a start element that is not part
						// of this array
						boolean loopDone2 = false;

						while (!loopDone2) {
							// We should be at the end element, but make sure
							while (!reader.isEndElement())
								reader.next();

							// Step out of this element
							reader.next();

							// Step to next element event.
							while (!reader.isStartElement() && !reader.isEndElement())
								reader.next();

							if (reader.isEndElement()) {
								// two continuous end elements means we are
								// exiting the xml structure
								loopDone2 = true;
							} else {
								if (new javax.xml.namespace.QName("", "coverage").equals(reader.getName())) {
									list2.add(Coverage.Factory.parse(reader));
								} else {
									loopDone2 = true;
								}
							}
						}

						// call the converter utility to convert and set the
						// array
						object.setCoverage((Coverage[]) org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(Coverage.class, list2));
					} // End of if for expected property start element

					else {
						// A start element we are not expecting indicates an
						// invalid parameter was passed
						throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
					}

					while (!reader.isStartElement() && !reader.isEndElement())
						reader.next();

					if (reader.isStartElement()) {
						// A start element we are not expecting indicates a
						// trailing invalid property
						throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
					}
				} catch (javax.xml.stream.XMLStreamException e) {
					throw new java.lang.Exception(e);
				}

				return object;
			}
		} // end of factory class
	}

	public static class MemberDemographics implements org.apache.axis2.databinding.ADBBean {
		/*
		 * This type was generated from the piece of schema that had name = memberDemographics Namespace URI = http://servicehelpers.esb.avalon.com Namespace
		 * Prefix = ns1
		 */

		/**
		 * field for DateOfBirth
		 */
		protected java.util.Date localDateOfBirth;

		/*
		 * This tracker boolean wil be used to detect whether the user called the set method for this attribute. It will be used to determine whether to include
		 * this field in the serialized XML
		 */
		protected boolean localDateOfBirthTracker = false;

		/**
		 * field for FirstName
		 */
		protected java.lang.String localFirstName;

		/*
		 * This tracker boolean wil be used to detect whether the user called the set method for this attribute. It will be used to determine whether to include
		 * this field in the serialized XML
		 */
		protected boolean localFirstNameTracker = false;

		/**
		 * field for GenderCode
		 */
		protected java.lang.String localGenderCode;

		/*
		 * This tracker boolean wil be used to detect whether the user called the set method for this attribute. It will be used to determine whether to include
		 * this field in the serialized XML
		 */
		protected boolean localGenderCodeTracker = false;

		/**
		 * field for LastName
		 */
		protected java.lang.String localLastName;

		/*
		 * This tracker boolean wil be used to detect whether the user called the set method for this attribute. It will be used to determine whether to include
		 * this field in the serialized XML
		 */
		protected boolean localLastNameTracker = false;

		/**
		 * field for MemberId
		 */
		protected java.lang.String localMemberId;

		/*
		 * This tracker boolean wil be used to detect whether the user called the set method for this attribute. It will be used to determine whether to include
		 * this field in the serialized XML
		 */
		protected boolean localMemberIdTracker = false;

		/**
		 * field for MiddleName
		 */
		protected java.lang.String localMiddleName;

		/*
		 * This tracker boolean wil be used to detect whether the user called the set method for this attribute. It will be used to determine whether to include
		 * this field in the serialized XML
		 */
		protected boolean localMiddleNameTracker = false;

		/**
		 * field for PatientId
		 */
		protected java.lang.String localPatientId;

		/*
		 * This tracker boolean wil be used to detect whether the user called the set method for this attribute. It will be used to determine whether to include
		 * this field in the serialized XML
		 */
		protected boolean localPatientIdTracker = false;

		/**
		 * field for Suffix
		 */
		protected java.lang.String localSuffix;

		/*
		 * This tracker boolean wil be used to detect whether the user called the set method for this attribute. It will be used to determine whether to include
		 * this field in the serialized XML
		 */
		protected boolean localSuffixTracker = false;

		public boolean isDateOfBirthSpecified() {
			return localDateOfBirthTracker;
		}

		/**
		 * Auto generated getter method
		 * 
		 * @return java.util.Calendar
		 */
		public java.util.Date getDateOfBirth() {
			return localDateOfBirth;
		}

		/**
		 * Auto generated setter method
		 * 
		 * @param param
		 *            DateOfBirth
		 */
		public void setDateOfBirth(java.util.Date param) {
			localDateOfBirthTracker = param != null;

			this.localDateOfBirth = param;
		}

		public boolean isFirstNameSpecified() {
			return localFirstNameTracker;
		}

		/**
		 * Auto generated getter method
		 * 
		 * @return java.lang.String
		 */
		public java.lang.String getFirstName() {
			return localFirstName;
		}

		/**
		 * Auto generated setter method
		 * 
		 * @param param
		 *            FirstName
		 */
		public void setFirstName(java.lang.String param) {
			localFirstNameTracker = param != null;

			this.localFirstName = param;
		}

		public boolean isGenderCodeSpecified() {
			return localGenderCodeTracker;
		}

		/**
		 * Auto generated getter method
		 * 
		 * @return java.lang.String
		 */
		public java.lang.String getGenderCode() {
			return localGenderCode;
		}

		/**
		 * Auto generated setter method
		 * 
		 * @param param
		 *            GenderCode
		 */
		public void setGenderCode(java.lang.String param) {
			localGenderCodeTracker = param != null;

			this.localGenderCode = param;
		}

		public boolean isLastNameSpecified() {
			return localLastNameTracker;
		}

		/**
		 * Auto generated getter method
		 * 
		 * @return java.lang.String
		 */
		public java.lang.String getLastName() {
			return localLastName;
		}

		/**
		 * Auto generated setter method
		 * 
		 * @param param
		 *            LastName
		 */
		public void setLastName(java.lang.String param) {
			localLastNameTracker = param != null;

			this.localLastName = param;
		}

		public boolean isMemberIdSpecified() {
			return localMemberIdTracker;
		}

		/**
		 * Auto generated getter method
		 * 
		 * @return java.lang.String
		 */
		public java.lang.String getMemberId() {
			return localMemberId;
		}

		/**
		 * Auto generated setter method
		 * 
		 * @param param
		 *            MemberId
		 */
		public void setMemberId(java.lang.String param) {
			localMemberIdTracker = param != null;

			this.localMemberId = param;
		}

		public boolean isMiddleNameSpecified() {
			return localMiddleNameTracker;
		}

		/**
		 * Auto generated getter method
		 * 
		 * @return java.lang.String
		 */
		public java.lang.String getMiddleName() {
			return localMiddleName;
		}

		/**
		 * Auto generated setter method
		 * 
		 * @param param
		 *            MiddleName
		 */
		public void setMiddleName(java.lang.String param) {
			localMiddleNameTracker = param != null;

			this.localMiddleName = param;
		}

		public boolean isPatientIdSpecified() {
			return localPatientIdTracker;
		}

		/**
		 * Auto generated getter method
		 * 
		 * @return java.lang.String
		 */
		public java.lang.String getPatientId() {
			return localPatientId;
		}

		/**
		 * Auto generated setter method
		 * 
		 * @param param
		 *            PatientId
		 */
		public void setPatientId(java.lang.String param) {
			localPatientIdTracker = param != null;

			this.localPatientId = param;
		}

		public boolean isSuffixSpecified() {
			return localSuffixTracker;
		}

		/**
		 * Auto generated getter method
		 * 
		 * @return java.lang.String
		 */
		public java.lang.String getSuffix() {
			return localSuffix;
		}

		/**
		 * Auto generated setter method
		 * 
		 * @param param
		 *            Suffix
		 */
		public void setSuffix(java.lang.String param) {
			localSuffixTracker = param != null;

			this.localSuffix = param;
		}

		/**
		 *
		 * @param parentQName
		 * @param factory
		 * @return org.apache.axiom.om.OMElement
		 */
		public org.apache.axiom.om.OMElement getOMElement(final javax.xml.namespace.QName parentQName, final org.apache.axiom.om.OMFactory factory)
				throws org.apache.axis2.databinding.ADBException {
			org.apache.axiom.om.OMDataSource dataSource = new org.apache.axis2.databinding.ADBDataSource(this, parentQName);

			return factory.createOMElement(dataSource, parentQName);
		}

		public void serialize(final javax.xml.namespace.QName parentQName, javax.xml.stream.XMLStreamWriter xmlWriter)
				throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {
			serialize(parentQName, xmlWriter, false);
		}

		public void serialize(final javax.xml.namespace.QName parentQName, javax.xml.stream.XMLStreamWriter xmlWriter, boolean serializeType)
				throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {
			java.lang.String prefix = null;
			java.lang.String namespace = null;

			prefix = parentQName.getPrefix();
			namespace = parentQName.getNamespaceURI();
			writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);

			if (serializeType) {
				java.lang.String namespacePrefix = registerPrefix(xmlWriter, "http://servicehelpers.esb.avalon.com");

				if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)) {
					writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", namespacePrefix + ":memberDemographics", xmlWriter);
				} else {
					writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", "memberDemographics", xmlWriter);
				}
			}

			if (localDateOfBirthTracker) {
				namespace = "";
				writeStartElement(null, namespace, "dateOfBirth", xmlWriter);

				if (localDateOfBirth == null) {
					// write the nil attribute
					throw new org.apache.axis2.databinding.ADBException("dateOfBirth cannot be null!!");
				} else {
					xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDateOfBirth));
				}

				xmlWriter.writeEndElement();
			}

			if (localFirstNameTracker) {
				namespace = "";
				writeStartElement(null, namespace, "firstName", xmlWriter);

				if (localFirstName == null) {
					// write the nil attribute
					throw new org.apache.axis2.databinding.ADBException("firstName cannot be null!!");
				} else {
					xmlWriter.writeCharacters(localFirstName);
				}

				xmlWriter.writeEndElement();
			}

			if (localGenderCodeTracker) {
				namespace = "";
				writeStartElement(null, namespace, "genderCode", xmlWriter);

				if (localGenderCode == null) {
					// write the nil attribute
					throw new org.apache.axis2.databinding.ADBException("genderCode cannot be null!!");
				} else {
					xmlWriter.writeCharacters(localGenderCode);
				}

				xmlWriter.writeEndElement();
			}

			if (localLastNameTracker) {
				namespace = "";
				writeStartElement(null, namespace, "lastName", xmlWriter);

				if (localLastName == null) {
					// write the nil attribute
					throw new org.apache.axis2.databinding.ADBException("lastName cannot be null!!");
				} else {
					xmlWriter.writeCharacters(localLastName);
				}

				xmlWriter.writeEndElement();
			}

			if (localMemberIdTracker) {
				namespace = "";
				writeStartElement(null, namespace, "memberId", xmlWriter);

				if (localMemberId == null) {
					// write the nil attribute
					throw new org.apache.axis2.databinding.ADBException("memberId cannot be null!!");
				} else {
					xmlWriter.writeCharacters(localMemberId);
				}

				xmlWriter.writeEndElement();
			}

			if (localMiddleNameTracker) {
				namespace = "";
				writeStartElement(null, namespace, "middleName", xmlWriter);

				if (localMiddleName == null) {
					// write the nil attribute
					throw new org.apache.axis2.databinding.ADBException("middleName cannot be null!!");
				} else {
					xmlWriter.writeCharacters(localMiddleName);
				}

				xmlWriter.writeEndElement();
			}

			if (localPatientIdTracker) {
				namespace = "";
				writeStartElement(null, namespace, "patientId", xmlWriter);

				if (localPatientId == null) {
					// write the nil attribute
					throw new org.apache.axis2.databinding.ADBException("patientId cannot be null!!");
				} else {
					xmlWriter.writeCharacters(localPatientId);
				}

				xmlWriter.writeEndElement();
			}

			if (localSuffixTracker) {
				namespace = "";
				writeStartElement(null, namespace, "suffix", xmlWriter);

				if (localSuffix == null) {
					// write the nil attribute
					throw new org.apache.axis2.databinding.ADBException("suffix cannot be null!!");
				} else {
					xmlWriter.writeCharacters(localSuffix);
				}

				xmlWriter.writeEndElement();
			}

			xmlWriter.writeEndElement();
		}

		private static java.lang.String generatePrefix(java.lang.String namespace) {
			if (namespace.equals("http://servicehelpers.esb.avalon.com")) {
				return "ns1";
			}

			return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
		}

		/**
		 * Utility method to write an element start tag.
		 */
		private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
				javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
			java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

			if (writerPrefix != null) {
				xmlWriter.writeStartElement(namespace, localPart);
			} else {
				if (namespace.length() == 0) {
					prefix = "";
				} else if (prefix == null) {
					prefix = generatePrefix(namespace);
				}

				xmlWriter.writeStartElement(prefix, localPart, namespace);
				xmlWriter.writeNamespace(prefix, namespace);
				xmlWriter.setPrefix(prefix, namespace);
			}
		}

		/**
		 * Util method to write an attribute with the ns prefix
		 */
		private void writeAttribute(java.lang.String prefix, java.lang.String namespace, java.lang.String attName, java.lang.String attValue,
				javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
			if (xmlWriter.getPrefix(namespace) == null) {
				xmlWriter.writeNamespace(prefix, namespace);
				xmlWriter.setPrefix(prefix, namespace);
			}

			xmlWriter.writeAttribute(namespace, attName, attValue);
		}

		/**
		 * Util method to write an attribute without the ns prefix
		 */
		private void writeAttribute(java.lang.String namespace, java.lang.String attName, java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter)
				throws javax.xml.stream.XMLStreamException {
			if (namespace.equals("")) {
				xmlWriter.writeAttribute(attName, attValue);
			} else {
				registerPrefix(xmlWriter, namespace);
				xmlWriter.writeAttribute(namespace, attName, attValue);
			}
		}

		/**
		 * Util method to write an attribute without the ns prefix
		 */
		private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName, javax.xml.namespace.QName qname,
				javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
			java.lang.String attributeNamespace = qname.getNamespaceURI();
			java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);

			if (attributePrefix == null) {
				attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
			}

			java.lang.String attributeValue;

			if (attributePrefix.trim().length() > 0) {
				attributeValue = attributePrefix + ":" + qname.getLocalPart();
			} else {
				attributeValue = qname.getLocalPart();
			}

			if (namespace.equals("")) {
				xmlWriter.writeAttribute(attName, attributeValue);
			} else {
				registerPrefix(xmlWriter, namespace);
				xmlWriter.writeAttribute(namespace, attName, attributeValue);
			}
		}

		/**
		 * method to handle Qnames
		 */
		private void writeQName(javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
			java.lang.String namespaceURI = qname.getNamespaceURI();

			if (namespaceURI != null) {
				java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);

				if (prefix == null) {
					prefix = generatePrefix(namespaceURI);
					xmlWriter.writeNamespace(prefix, namespaceURI);
					xmlWriter.setPrefix(prefix, namespaceURI);
				}

				if (prefix.trim().length() > 0) {
					xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
				} else {
					// i.e this is the default namespace
					xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
				}
			} else {
				xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
			}
		}

		private void writeQNames(javax.xml.namespace.QName[] qnames, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
			if (qnames != null) {
				// we have to store this data until last moment since it is not
				// possible to write any
				// namespace data after writing the charactor data
				java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
				java.lang.String namespaceURI = null;
				java.lang.String prefix = null;

				for (int i = 0; i < qnames.length; i++) {
					if (i > 0) {
						stringToWrite.append(" ");
					}

					namespaceURI = qnames[i].getNamespaceURI();

					if (namespaceURI != null) {
						prefix = xmlWriter.getPrefix(namespaceURI);

						if ((prefix == null) || (prefix.length() == 0)) {
							prefix = generatePrefix(namespaceURI);
							xmlWriter.writeNamespace(prefix, namespaceURI);
							xmlWriter.setPrefix(prefix, namespaceURI);
						}

						if (prefix.trim().length() > 0) {
							stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
						} else {
							stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
						}
					} else {
						stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
					}
				}

				xmlWriter.writeCharacters(stringToWrite.toString());
			}
		}

		/**
		 * Register a namespace prefix
		 */
		private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace)
				throws javax.xml.stream.XMLStreamException {
			java.lang.String prefix = xmlWriter.getPrefix(namespace);

			if (prefix == null) {
				prefix = generatePrefix(namespace);

				javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();

				while (true) {
					java.lang.String uri = nsContext.getNamespaceURI(prefix);

					if ((uri == null) || (uri.length() == 0)) {
						break;
					}

					prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
				}

				xmlWriter.writeNamespace(prefix, namespace);
				xmlWriter.setPrefix(prefix, namespace);
			}

			return prefix;
		}

		/**
		 * databinding method to get an XML representation of this object
		 *
		 */
		public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName) throws org.apache.axis2.databinding.ADBException {
			java.util.ArrayList elementList = new java.util.ArrayList();
			java.util.ArrayList attribList = new java.util.ArrayList();

			if (localDateOfBirthTracker) {
				elementList.add(new javax.xml.namespace.QName("", "dateOfBirth"));

				if (localDateOfBirth != null) {
					elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDateOfBirth));
				} else {
					throw new org.apache.axis2.databinding.ADBException("dateOfBirth cannot be null!!");
				}
			}

			if (localFirstNameTracker) {
				elementList.add(new javax.xml.namespace.QName("", "firstName"));

				if (localFirstName != null) {
					elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localFirstName));
				} else {
					throw new org.apache.axis2.databinding.ADBException("firstName cannot be null!!");
				}
			}

			if (localGenderCodeTracker) {
				elementList.add(new javax.xml.namespace.QName("", "genderCode"));

				if (localGenderCode != null) {
					elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localGenderCode));
				} else {
					throw new org.apache.axis2.databinding.ADBException("genderCode cannot be null!!");
				}
			}

			if (localLastNameTracker) {
				elementList.add(new javax.xml.namespace.QName("", "lastName"));

				if (localLastName != null) {
					elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLastName));
				} else {
					throw new org.apache.axis2.databinding.ADBException("lastName cannot be null!!");
				}
			}

			if (localMemberIdTracker) {
				elementList.add(new javax.xml.namespace.QName("", "memberId"));

				if (localMemberId != null) {
					elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localMemberId));
				} else {
					throw new org.apache.axis2.databinding.ADBException("memberId cannot be null!!");
				}
			}

			if (localMiddleNameTracker) {
				elementList.add(new javax.xml.namespace.QName("", "middleName"));

				if (localMiddleName != null) {
					elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localMiddleName));
				} else {
					throw new org.apache.axis2.databinding.ADBException("middleName cannot be null!!");
				}
			}

			if (localPatientIdTracker) {
				elementList.add(new javax.xml.namespace.QName("", "patientId"));

				if (localPatientId != null) {
					elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPatientId));
				} else {
					throw new org.apache.axis2.databinding.ADBException("patientId cannot be null!!");
				}
			}

			if (localSuffixTracker) {
				elementList.add(new javax.xml.namespace.QName("", "suffix"));

				if (localSuffix != null) {
					elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSuffix));
				} else {
					throw new org.apache.axis2.databinding.ADBException("suffix cannot be null!!");
				}
			}

			return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
		}

		/**
		 * Factory class that keeps the parse method
		 */
		public static class Factory {
			/**
			 * static method to create the object Precondition: If this object is an element, the current or next start element starts this object and any
			 * intervening reader events are ignorable If this object is not an element, it is a complex type and the reader is at the event just after the
			 * outer start element Postcondition: If this object is an element, the reader is positioned at its end element If this object is a complex type,
			 * the reader is positioned at the end element of its outer element
			 */
			public static MemberDemographics parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception {
				MemberDemographics object = new MemberDemographics();

				int event;
				java.lang.String nillableValue = null;
				java.lang.String prefix = "";
				java.lang.String namespaceuri = "";

				try {
					while (!reader.isStartElement() && !reader.isEndElement())
						reader.next();

					if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
						java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type");

						if (fullTypeName != null) {
							java.lang.String nsPrefix = null;

							if (fullTypeName.indexOf(":") > -1) {
								nsPrefix = fullTypeName.substring(0, fullTypeName.indexOf(":"));
							}

							nsPrefix = (nsPrefix == null) ? "" : nsPrefix;

							java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":") + 1);

							if (!"memberDemographics".equals(type)) {
								// find namespace for the prefix
								java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);

								return (MemberDemographics) ExtensionMapper.getTypeObject(nsUri, type, reader);
							}
						}
					}

					// Note all attributes that were handled. Used to differ
					// normal attributes
					// from anyAttributes.
					java.util.Vector handledAttributes = new java.util.Vector();

					reader.next();

					while (!reader.isStartElement() && !reader.isEndElement())
						reader.next();

					if (reader.isStartElement() && new javax.xml.namespace.QName("", "dateOfBirth").equals(reader.getName())) {
						nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");

						if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
							throw new org.apache.axis2.databinding.ADBException("The element: " + "dateOfBirth" + "  cannot be null");
						}

						java.lang.String content = reader.getElementText();

						object.setDateOfBirth(org.apache.axis2.databinding.utils.ConverterUtil.convertToDate(content));

						reader.next();
					} // End of if for expected property start element

					else {
					}

					while (!reader.isStartElement() && !reader.isEndElement())
						reader.next();

					if (reader.isStartElement() && new javax.xml.namespace.QName("", "firstName").equals(reader.getName())) {
						nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");

						if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
							throw new org.apache.axis2.databinding.ADBException("The element: " + "firstName" + "  cannot be null");
						}

						java.lang.String content = reader.getElementText();

						object.setFirstName(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

						reader.next();
					} // End of if for expected property start element

					else {
					}

					while (!reader.isStartElement() && !reader.isEndElement())
						reader.next();

					if (reader.isStartElement() && new javax.xml.namespace.QName("", "genderCode").equals(reader.getName())) {
						nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");

						if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
							throw new org.apache.axis2.databinding.ADBException("The element: " + "genderCode" + "  cannot be null");
						}

						java.lang.String content = reader.getElementText();

						object.setGenderCode(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

						reader.next();
					} // End of if for expected property start element

					else {
					}

					while (!reader.isStartElement() && !reader.isEndElement())
						reader.next();

					if (reader.isStartElement() && new javax.xml.namespace.QName("", "lastName").equals(reader.getName())) {
						nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");

						if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
							throw new org.apache.axis2.databinding.ADBException("The element: " + "lastName" + "  cannot be null");
						}

						java.lang.String content = reader.getElementText();

						object.setLastName(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

						reader.next();
					} // End of if for expected property start element

					else {
					}

					while (!reader.isStartElement() && !reader.isEndElement())
						reader.next();

					if (reader.isStartElement() && new javax.xml.namespace.QName("", "memberId").equals(reader.getName())) {
						nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");

						if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
							throw new org.apache.axis2.databinding.ADBException("The element: " + "memberId" + "  cannot be null");
						}

						java.lang.String content = reader.getElementText();

						object.setMemberId(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

						reader.next();
					} // End of if for expected property start element

					else {
					}

					while (!reader.isStartElement() && !reader.isEndElement())
						reader.next();

					if (reader.isStartElement() && new javax.xml.namespace.QName("", "middleName").equals(reader.getName())) {
						nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");

						if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
							throw new org.apache.axis2.databinding.ADBException("The element: " + "middleName" + "  cannot be null");
						}

						java.lang.String content = reader.getElementText();

						object.setMiddleName(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

						reader.next();
					} // End of if for expected property start element

					else {
					}

					while (!reader.isStartElement() && !reader.isEndElement())
						reader.next();

					if (reader.isStartElement() && new javax.xml.namespace.QName("", "patientId").equals(reader.getName())) {
						nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");

						if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
							throw new org.apache.axis2.databinding.ADBException("The element: " + "patientId" + "  cannot be null");
						}

						java.lang.String content = reader.getElementText();

						object.setPatientId(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

						reader.next();
					} // End of if for expected property start element

					else {
					}

					while (!reader.isStartElement() && !reader.isEndElement())
						reader.next();

					if (reader.isStartElement() && new javax.xml.namespace.QName("", "suffix").equals(reader.getName())) {
						nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");

						if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
							throw new org.apache.axis2.databinding.ADBException("The element: " + "suffix" + "  cannot be null");
						}

						java.lang.String content = reader.getElementText();

						object.setSuffix(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

						reader.next();
					} // End of if for expected property start element

					else {
					}

					while (!reader.isStartElement() && !reader.isEndElement())
						reader.next();

					if (reader.isStartElement()) {
						// A start element we are not expecting indicates a
						// trailing invalid property
						throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
					}
				} catch (javax.xml.stream.XMLStreamException e) {
					throw new java.lang.Exception(e);
				}

				return object;
			}
		} // end of factory class
	}

	public static class MemberList implements org.apache.axis2.databinding.ADBBean {
		/*
		 * This type was generated from the piece of schema that had name = MemberList Namespace URI = http://servicehelpers.esb.avalon.com Namespace Prefix =
		 * ns1
		 */

		/**
		 * field for MemberListCount
		 */
		protected java.lang.String localMemberListCount;

		/**
		 * field for Member This was an Array!
		 */
		protected Member[] localMember;

		/**
		 * Auto generated getter method
		 * 
		 * @return java.lang.String
		 */
		public java.lang.String getMemberListCount() {
			return localMemberListCount;
		}

		/**
		 * Auto generated setter method
		 * 
		 * @param param
		 *            MemberListCount
		 */
		public void setMemberListCount(java.lang.String param) {
			this.localMemberListCount = param;
		}

		/**
		 * Auto generated getter method
		 * 
		 * @return Member[]
		 */
		public Member[] getMember() {
			return localMember;
		}

		/**
		 * validate the array for Member
		 */
		protected void validateMember(Member[] param) {
			if ((param != null) && (param.length > 20)) {
				throw new java.lang.RuntimeException("Input values do not follow defined XSD restrictions");
			}

			if ((param != null) && (param.length < 1)) {
				throw new java.lang.RuntimeException("Input values do not follow defined XSD restrictions");
			}
		}

		/**
		 * Auto generated setter method
		 * 
		 * @param param
		 *            Member
		 */
		public void setMember(Member[] param) {
			validateMember(param);

			this.localMember = param;
		}

		/**
		 * Auto generated add method for the array for convenience
		 * 
		 * @param param
		 *            Member
		 */
		public void addMember(Member param) {
			if (localMember == null) {
				localMember = new Member[] {};
			}

			java.util.List list = org.apache.axis2.databinding.utils.ConverterUtil.toList(localMember);
			list.add(param);
			this.localMember = (Member[]) list.toArray(new Member[list.size()]);
		}

		/**
		 *
		 * @param parentQName
		 * @param factory
		 * @return org.apache.axiom.om.OMElement o
		 */
		public org.apache.axiom.om.OMElement getOMElement(final javax.xml.namespace.QName parentQName, final org.apache.axiom.om.OMFactory factory)
				throws org.apache.axis2.databinding.ADBException {
			org.apache.axiom.om.OMDataSource dataSource = new org.apache.axis2.databinding.ADBDataSource(this, parentQName);

			return factory.createOMElement(dataSource, parentQName);
		}

		public void serialize(final javax.xml.namespace.QName parentQName, javax.xml.stream.XMLStreamWriter xmlWriter)
				throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {
			serialize(parentQName, xmlWriter, false);
		}

		public void serialize(final javax.xml.namespace.QName parentQName, javax.xml.stream.XMLStreamWriter xmlWriter, boolean serializeType)
				throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {
			java.lang.String prefix = null;
			java.lang.String namespace = null;

			prefix = parentQName.getPrefix();
			namespace = parentQName.getNamespaceURI();
			writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);

			if (serializeType) {
				java.lang.String namespacePrefix = registerPrefix(xmlWriter, "http://servicehelpers.esb.avalon.com");

				if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)) {
					writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", namespacePrefix + ":MemberList", xmlWriter);
				} else {
					writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", "MemberList", xmlWriter);
				}
			}

			namespace = "";
			writeStartElement(null, namespace, "memberListCount", xmlWriter);

			if (localMemberListCount == null) {
				// write the nil attribute
				throw new org.apache.axis2.databinding.ADBException("memberListCount cannot be null!!");
			} else {
				xmlWriter.writeCharacters(localMemberListCount);
			}

			xmlWriter.writeEndElement();

			if (localMember != null) {
				for (int i = 0; i < localMember.length; i++) {
					if (localMember[i] != null) {
						localMember[i].serialize(new javax.xml.namespace.QName("", "member"), xmlWriter);
					} else {
						throw new org.apache.axis2.databinding.ADBException("member cannot be null!!");
					}
				}
			} else {
				throw new org.apache.axis2.databinding.ADBException("member cannot be null!!");
			}

			xmlWriter.writeEndElement();
		}

		private static java.lang.String generatePrefix(java.lang.String namespace) {
			if (namespace.equals("http://servicehelpers.esb.avalon.com")) {
				return "ns1";
			}

			return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
		}

		/**
		 * Utility method to write an element start tag.
		 */
		private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
				javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
			java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

			if (writerPrefix != null) {
				xmlWriter.writeStartElement(namespace, localPart);
			} else {
				if (namespace.length() == 0) {
					prefix = "";
				} else if (prefix == null) {
					prefix = generatePrefix(namespace);
				}

				xmlWriter.writeStartElement(prefix, localPart, namespace);
				xmlWriter.writeNamespace(prefix, namespace);
				xmlWriter.setPrefix(prefix, namespace);
			}
		}

		/**
		 * Util method to write an attribute with the ns prefix
		 */
		private void writeAttribute(java.lang.String prefix, java.lang.String namespace, java.lang.String attName, java.lang.String attValue,
				javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
			if (xmlWriter.getPrefix(namespace) == null) {
				xmlWriter.writeNamespace(prefix, namespace);
				xmlWriter.setPrefix(prefix, namespace);
			}

			xmlWriter.writeAttribute(namespace, attName, attValue);
		}

		/**
		 * Util method to write an attribute without the ns prefix
		 */
		private void writeAttribute(java.lang.String namespace, java.lang.String attName, java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter)
				throws javax.xml.stream.XMLStreamException {
			if (namespace.equals("")) {
				xmlWriter.writeAttribute(attName, attValue);
			} else {
				registerPrefix(xmlWriter, namespace);
				xmlWriter.writeAttribute(namespace, attName, attValue);
			}
		}

		/**
		 * Util method to write an attribute without the ns prefix
		 */
		private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName, javax.xml.namespace.QName qname,
				javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
			java.lang.String attributeNamespace = qname.getNamespaceURI();
			java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);

			if (attributePrefix == null) {
				attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
			}

			java.lang.String attributeValue;

			if (attributePrefix.trim().length() > 0) {
				attributeValue = attributePrefix + ":" + qname.getLocalPart();
			} else {
				attributeValue = qname.getLocalPart();
			}

			if (namespace.equals("")) {
				xmlWriter.writeAttribute(attName, attributeValue);
			} else {
				registerPrefix(xmlWriter, namespace);
				xmlWriter.writeAttribute(namespace, attName, attributeValue);
			}
		}

		/**
		 * method to handle Qnames
		 */
		private void writeQName(javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
			java.lang.String namespaceURI = qname.getNamespaceURI();

			if (namespaceURI != null) {
				java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);

				if (prefix == null) {
					prefix = generatePrefix(namespaceURI);
					xmlWriter.writeNamespace(prefix, namespaceURI);
					xmlWriter.setPrefix(prefix, namespaceURI);
				}

				if (prefix.trim().length() > 0) {
					xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
				} else {
					// i.e this is the default namespace
					xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
				}
			} else {
				xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
			}
		}

		private void writeQNames(javax.xml.namespace.QName[] qnames, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
			if (qnames != null) {
				// we have to store this data until last moment since it is not
				// possible to write any
				// namespace data after writing the charactor data
				java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
				java.lang.String namespaceURI = null;
				java.lang.String prefix = null;

				for (int i = 0; i < qnames.length; i++) {
					if (i > 0) {
						stringToWrite.append(" ");
					}

					namespaceURI = qnames[i].getNamespaceURI();

					if (namespaceURI != null) {
						prefix = xmlWriter.getPrefix(namespaceURI);

						if ((prefix == null) || (prefix.length() == 0)) {
							prefix = generatePrefix(namespaceURI);
							xmlWriter.writeNamespace(prefix, namespaceURI);
							xmlWriter.setPrefix(prefix, namespaceURI);
						}

						if (prefix.trim().length() > 0) {
							stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
						} else {
							stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
						}
					} else {
						stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
					}
				}

				xmlWriter.writeCharacters(stringToWrite.toString());
			}
		}

		/**
		 * Register a namespace prefix
		 */
		private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace)
				throws javax.xml.stream.XMLStreamException {
			java.lang.String prefix = xmlWriter.getPrefix(namespace);

			if (prefix == null) {
				prefix = generatePrefix(namespace);

				javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();

				while (true) {
					java.lang.String uri = nsContext.getNamespaceURI(prefix);

					if ((uri == null) || (uri.length() == 0)) {
						break;
					}

					prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
				}

				xmlWriter.writeNamespace(prefix, namespace);
				xmlWriter.setPrefix(prefix, namespace);
			}

			return prefix;
		}

		/**
		 * databinding method to get an XML representation of this object
		 *
		 */
		public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName) throws org.apache.axis2.databinding.ADBException {
			java.util.ArrayList elementList = new java.util.ArrayList();
			java.util.ArrayList attribList = new java.util.ArrayList();

			elementList.add(new javax.xml.namespace.QName("", "memberListCount"));

			if (localMemberListCount != null) {
				elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localMemberListCount));
			} else {
				throw new org.apache.axis2.databinding.ADBException("memberListCount cannot be null!!");
			}

			if (localMember != null) {
				for (int i = 0; i < localMember.length; i++) {
					if (localMember[i] != null) {
						elementList.add(new javax.xml.namespace.QName("", "member"));
						elementList.add(localMember[i]);
					} else {
						throw new org.apache.axis2.databinding.ADBException("member cannot be null !!");
					}
				}
			} else {
				throw new org.apache.axis2.databinding.ADBException("member cannot be null!!");
			}

			return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
		}

		/**
		 * Factory class that keeps the parse method
		 */
		public static class Factory {
			/**
			 * static method to create the object Precondition: If this object is an element, the current or next start element starts this object and any
			 * intervening reader events are ignorable If this object is not an element, it is a complex type and the reader is at the event just after the
			 * outer start element Postcondition: If this object is an element, the reader is positioned at its end element If this object is a complex type,
			 * the reader is positioned at the end element of its outer element
			 */
			public static MemberList parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception {
				MemberList object = new MemberList();

				int event;
				java.lang.String nillableValue = null;
				java.lang.String prefix = "";
				java.lang.String namespaceuri = "";

				try {
					while (!reader.isStartElement() && !reader.isEndElement())
						reader.next();

					if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
						java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type");

						if (fullTypeName != null) {
							java.lang.String nsPrefix = null;

							if (fullTypeName.indexOf(":") > -1) {
								nsPrefix = fullTypeName.substring(0, fullTypeName.indexOf(":"));
							}

							nsPrefix = (nsPrefix == null) ? "" : nsPrefix;

							java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":") + 1);

							if (!"MemberList".equals(type)) {
								// find namespace for the prefix
								java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);

								return (MemberList) ExtensionMapper.getTypeObject(nsUri, type, reader);
							}
						}
					}

					// Note all attributes that were handled. Used to differ
					// normal attributes
					// from anyAttributes.
					java.util.Vector handledAttributes = new java.util.Vector();

					reader.next();

					java.util.ArrayList list2 = new java.util.ArrayList();

					while (!reader.isStartElement() && !reader.isEndElement())
						reader.next();

					if (reader.isStartElement() && new javax.xml.namespace.QName("", "memberListCount").equals(reader.getName())) {
						nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");

						if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
							throw new org.apache.axis2.databinding.ADBException("The element: " + "memberListCount" + "  cannot be null");
						}

						java.lang.String content = reader.getElementText();

						object.setMemberListCount(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

						reader.next();
					} // End of if for expected property start element

					else {
						// A start element we are not expecting indicates an
						// invalid parameter was passed
						throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
					}

					while (!reader.isStartElement() && !reader.isEndElement())
						reader.next();

					if (reader.isStartElement() && new javax.xml.namespace.QName("", "member").equals(reader.getName())) {
						// Process the array and step past its final element's
						// end.
						list2.add(Member.Factory.parse(reader));

						// loop until we find a start element that is not part
						// of this array
						boolean loopDone2 = false;

						while (!loopDone2) {
							// We should be at the end element, but make sure
							while (!reader.isEndElement())
								reader.next();

							// Step out of this element
							reader.next();

							// Step to next element event.
							while (!reader.isStartElement() && !reader.isEndElement())
								reader.next();

							if (reader.isEndElement()) {
								// two continuous end elements means we are
								// exiting the xml structure
								loopDone2 = true;
							} else {
								if (new javax.xml.namespace.QName("", "member").equals(reader.getName())) {
									list2.add(Member.Factory.parse(reader));
								} else {
									loopDone2 = true;
								}
							}
						}

						// call the converter utility to convert and set the
						// array
						object.setMember((Member[]) org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(Member.class, list2));
					} // End of if for expected property start element

					else {
						// A start element we are not expecting indicates an
						// invalid parameter was passed
						throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
					}

					while (!reader.isStartElement() && !reader.isEndElement())
						reader.next();

					if (reader.isStartElement()) {
						// A start element we are not expecting indicates a
						// trailing invalid property
						throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
					}
				} catch (javax.xml.stream.XMLStreamException e) {
					throw new java.lang.Exception(e);
				}

				return object;
			}
		} // end of factory class
	}

	public static class Coverage implements org.apache.axis2.databinding.ADBBean {
		/*
		 * This type was generated from the piece of schema that had name = Coverage Namespace URI = http://servicehelpers.esb.avalon.com Namespace Prefix = ns1
		 */

		/**
		 * field for CoverageBeginDate
		 */
		protected java.util.Date localCoverageBeginDate;

		/**
		 * field for CoverageEndDate
		 */
		protected java.util.Date localCoverageEndDate;

		/**
		 * field for HealthPlanGroupId
		 */
		protected java.lang.String localHealthPlanGroupId;

		/**
		 * field for LabBenefitId
		 */
		protected java.lang.String localLabBenefitId;

		/**
		 * field for LabBenefitIdDescription
		 */
		protected java.lang.String localLabBenefitIdDescription;

		/**
		 * field for ClientNumber
		 */
		protected java.lang.String localClientNumber;

		/**
		 * field for ClientNumberDescription
		 */
		protected java.lang.String localClientNumberDescription;

		/**
		 * field for CesGroupNumber
		 */
		protected java.lang.String localCesGroupNumber;

		/**
		 * field for CesGroupNumberDescription
		 */
		protected java.lang.String localCesGroupNumberDescription;

		/**
		 * field for AmmsGroupNumber
		 */
		protected java.lang.String localAmmsGroupNumber;

		/**
		 * field for ServiceCenterCode
		 */
		protected java.lang.String localServiceCenterCode;

		/**
		 * field for ServiceCenterDescription
		 */
		protected java.lang.String localServiceCenterDescription;

		/**
		 * field for BusinessSectorSegment
		 */
		protected BusinessSectorSegment localBusinessSectorSegment;

		/**
		 * field for MarketingPackage
		 */
		protected MarketingPackage localMarketingPackage;

		/**
		 * Auto generated getter method
		 * 
		 * @return java.util.Date
		 */
		public java.util.Date getCoverageBeginDate() {
			return localCoverageBeginDate;
		}

		/**
		 * Auto generated setter method
		 * 
		 * @param param
		 *            CoverageBeginDate
		 */
		public void setCoverageBeginDate(java.util.Date param) {
			this.localCoverageBeginDate = param;
		}

		/**
		 * Auto generated getter method
		 * 
		 * @return java.util.Date
		 */
		public java.util.Date getCoverageEndDate() {
			return localCoverageEndDate;
		}

		/**
		 * Auto generated setter method
		 * 
		 * @param param
		 *            CoverageEndDate
		 */
		public void setCoverageEndDate(java.util.Date param) {
			this.localCoverageEndDate = param;
		}

		/**
		 * Auto generated getter method
		 * 
		 * @return java.lang.String
		 */
		public java.lang.String getHealthPlanGroupId() {
			return localHealthPlanGroupId;
		}

		/**
		 * Auto generated setter method
		 * 
		 * @param param
		 *            HealthPlanGroupId
		 */
		public void setHealthPlanGroupId(java.lang.String param) {
			this.localHealthPlanGroupId = param;
		}

		/**
		 * Auto generated getter method
		 * 
		 * @return java.lang.String
		 */
		public java.lang.String getLabBenefitId() {
			return localLabBenefitId;
		}

		/**
		 * Auto generated setter method
		 * 
		 * @param param
		 *            LabBenefitId
		 */
		public void setLabBenefitId(java.lang.String param) {
			this.localLabBenefitId = param;
		}

		/**
		 * Auto generated getter method
		 * 
		 * @return java.lang.String
		 */
		public java.lang.String getLabBenefitIdDescription() {
			return localLabBenefitIdDescription;
		}

		/**
		 * Auto generated setter method
		 * 
		 * @param param
		 *            LabBenefitIdDescription
		 */
		public void setLabBenefitIdDescription(java.lang.String param) {
			this.localLabBenefitIdDescription = param;
		}

		/**
		 * Auto generated getter method
		 * 
		 * @return java.lang.String
		 */
		public java.lang.String getClientNumber() {
			return localClientNumber;
		}

		/**
		 * Auto generated setter method
		 * 
		 * @param param
		 *            ClientNumber
		 */
		public void setClientNumber(java.lang.String param) {
			this.localClientNumber = param;
		}

		/**
		 * Auto generated getter method
		 * 
		 * @return java.lang.String
		 */
		public java.lang.String getClientNumberDescription() {
			return localClientNumberDescription;
		}

		/**
		 * Auto generated setter method
		 * 
		 * @param param
		 *            ClientNumberDescription
		 */
		public void setClientNumberDescription(java.lang.String param) {
			this.localClientNumberDescription = param;
		}

		/**
		 * Auto generated getter method
		 * 
		 * @return java.lang.String
		 */
		public java.lang.String getCesGroupNumber() {
			return localCesGroupNumber;
		}

		/**
		 * Auto generated setter method
		 * 
		 * @param param
		 *            CesGroupNumber
		 */
		public void setCesGroupNumber(java.lang.String param) {
			this.localCesGroupNumber = param;
		}

		/**
		 * Auto generated getter method
		 * 
		 * @return java.lang.String
		 */
		public java.lang.String getCesGroupNumberDescription() {
			return localCesGroupNumberDescription;
		}

		/**
		 * Auto generated setter method
		 * 
		 * @param param
		 *            CesGroupNumberDescription
		 */
		public void setCesGroupNumberDescription(java.lang.String param) {
			this.localCesGroupNumberDescription = param;
		}

		/**
		 * Auto generated getter method
		 * 
		 * @return java.lang.String
		 */
		public java.lang.String getAmmsGroupNumber() {
			return localAmmsGroupNumber;
		}

		/**
		 * Auto generated setter method
		 * 
		 * @param param
		 *            AmmsGroupNumber
		 */
		public void setAmmsGroupNumber(java.lang.String param) {
			this.localAmmsGroupNumber = param;
		}

		/**
		 * Auto generated getter method
		 * 
		 * @return java.lang.String
		 */
		public java.lang.String getServiceCenterCode() {
			return localServiceCenterCode;
		}

		/**
		 * Auto generated setter method
		 * 
		 * @param param
		 *            ServiceCenterCode
		 */
		public void setServiceCenterCode(java.lang.String param) {
			this.localServiceCenterCode = param;
		}

		/**
		 * Auto generated getter method
		 * 
		 * @return java.lang.String
		 */
		public java.lang.String getServiceCenterDescription() {
			return localServiceCenterDescription;
		}

		/**
		 * Auto generated setter method
		 * 
		 * @param param
		 *            ServiceCenterDescription
		 */
		public void setServiceCenterDescription(java.lang.String param) {
			this.localServiceCenterDescription = param;
		}

		/**
		 * Auto generated getter method
		 * 
		 * @return BusinessSectorSegment
		 */
		public BusinessSectorSegment getBusinessSectorSegment() {
			return localBusinessSectorSegment;
		}

		/**
		 * Auto generated setter method
		 * 
		 * @param param
		 *            BusinessSectorSegment
		 */
		public void setBusinessSectorSegment(BusinessSectorSegment param) {
			this.localBusinessSectorSegment = param;
		}

		/**
		 * Auto generated getter method
		 * 
		 * @return MarketingPackage
		 */
		public MarketingPackage getMarketingPackage() {
			return localMarketingPackage;
		}

		/**
		 * Auto generated setter method
		 * 
		 * @param param
		 *            MarketingPackage
		 */
		public void setMarketingPackage(MarketingPackage param) {
			this.localMarketingPackage = param;
		}

		/**
		 *
		 * @param parentQName
		 * @param factory
		 * @return org.apache.axiom.om.OMElement
		 */
		public org.apache.axiom.om.OMElement getOMElement(final javax.xml.namespace.QName parentQName, final org.apache.axiom.om.OMFactory factory)
				throws org.apache.axis2.databinding.ADBException {
			org.apache.axiom.om.OMDataSource dataSource = new org.apache.axis2.databinding.ADBDataSource(this, parentQName);

			return factory.createOMElement(dataSource, parentQName);
		}

		public void serialize(final javax.xml.namespace.QName parentQName, javax.xml.stream.XMLStreamWriter xmlWriter)
				throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {
			serialize(parentQName, xmlWriter, false);
		}

		public void serialize(final javax.xml.namespace.QName parentQName, javax.xml.stream.XMLStreamWriter xmlWriter, boolean serializeType)
				throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {
			java.lang.String prefix = null;
			java.lang.String namespace = null;

			prefix = parentQName.getPrefix();
			namespace = parentQName.getNamespaceURI();
			writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);

			if (serializeType) {
				java.lang.String namespacePrefix = registerPrefix(xmlWriter, "http://servicehelpers.esb.avalon.com");

				if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)) {
					writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", namespacePrefix + ":Coverage", xmlWriter);
				} else {
					writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", "Coverage", xmlWriter);
				}
			}

			namespace = "";
			writeStartElement(null, namespace, "coverageBeginDate", xmlWriter);

			if (localCoverageBeginDate == null) {
				// write the nil attribute
				throw new org.apache.axis2.databinding.ADBException("coverageBeginDate cannot be null!!");
			} else {
				xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCoverageBeginDate));
			}

			xmlWriter.writeEndElement();

			namespace = "";
			writeStartElement(null, namespace, "coverageEndDate", xmlWriter);

			if (localCoverageEndDate == null) {
				// write the nil attribute
				throw new org.apache.axis2.databinding.ADBException("coverageEndDate cannot be null!!");
			} else {
				xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCoverageEndDate));
			}

			xmlWriter.writeEndElement();

			namespace = "";
			writeStartElement(null, namespace, "healthPlanGroupId", xmlWriter);

			if (localHealthPlanGroupId == null) {
				// write the nil attribute
				throw new org.apache.axis2.databinding.ADBException("healthPlanGroupId cannot be null!!");
			} else {
				xmlWriter.writeCharacters(localHealthPlanGroupId);
			}

			xmlWriter.writeEndElement();

			namespace = "";
			writeStartElement(null, namespace, "labBenefitId", xmlWriter);

			if (localLabBenefitId == null) {
				// write the nil attribute
				throw new org.apache.axis2.databinding.ADBException("labBenefitId cannot be null!!");
			} else {
				xmlWriter.writeCharacters(localLabBenefitId);
			}

			xmlWriter.writeEndElement();

			namespace = "";
			writeStartElement(null, namespace, "labBenefitIdDescription", xmlWriter);

			if (localLabBenefitIdDescription == null) {
				// write the nil attribute
				throw new org.apache.axis2.databinding.ADBException("labBenefitIdDescription cannot be null!!");
			} else {
				xmlWriter.writeCharacters(localLabBenefitIdDescription);
			}

			xmlWriter.writeEndElement();

			namespace = "";
			writeStartElement(null, namespace, "clientNumber", xmlWriter);

			if (localClientNumber == null) {
				// write the nil attribute
				throw new org.apache.axis2.databinding.ADBException("clientNumber cannot be null!!");
			} else {
				xmlWriter.writeCharacters(localClientNumber);
			}

			xmlWriter.writeEndElement();

			namespace = "";
			writeStartElement(null, namespace, "clientNumberDescription", xmlWriter);

			if (localClientNumberDescription == null) {
				// write the nil attribute
				throw new org.apache.axis2.databinding.ADBException("clientNumberDescription cannot be null!!");
			} else {
				xmlWriter.writeCharacters(localClientNumberDescription);
			}

			xmlWriter.writeEndElement();

			namespace = "";
			writeStartElement(null, namespace, "cesGroupNumber", xmlWriter);

			if (localCesGroupNumber == null) {
				// write the nil attribute
				throw new org.apache.axis2.databinding.ADBException("cesGroupNumber cannot be null!!");
			} else {
				xmlWriter.writeCharacters(localCesGroupNumber);
			}

			xmlWriter.writeEndElement();

			namespace = "";
			writeStartElement(null, namespace, "cesGroupNumberDescription", xmlWriter);

			if (localCesGroupNumberDescription == null) {
				// write the nil attribute
				throw new org.apache.axis2.databinding.ADBException("cesGroupNumberDescription cannot be null!!");
			} else {
				xmlWriter.writeCharacters(localCesGroupNumberDescription);
			}

			xmlWriter.writeEndElement();

			namespace = "";
			writeStartElement(null, namespace, "ammsGroupNumber", xmlWriter);

			if (localAmmsGroupNumber == null) {
				// write the nil attribute
				throw new org.apache.axis2.databinding.ADBException("ammsGroupNumber cannot be null!!");
			} else {
				xmlWriter.writeCharacters(localAmmsGroupNumber);
			}

			xmlWriter.writeEndElement();

			namespace = "";
			writeStartElement(null, namespace, "serviceCenterCode", xmlWriter);

			if (localServiceCenterCode == null) {
				// write the nil attribute
				throw new org.apache.axis2.databinding.ADBException("serviceCenterCode cannot be null!!");
			} else {
				xmlWriter.writeCharacters(localServiceCenterCode);
			}

			xmlWriter.writeEndElement();

			namespace = "";
			writeStartElement(null, namespace, "serviceCenterDescription", xmlWriter);

			if (localServiceCenterDescription == null) {
				// write the nil attribute
				throw new org.apache.axis2.databinding.ADBException("serviceCenterDescription cannot be null!!");
			} else {
				xmlWriter.writeCharacters(localServiceCenterDescription);
			}

			xmlWriter.writeEndElement();

			if (localBusinessSectorSegment == null) {
				throw new org.apache.axis2.databinding.ADBException("businessSectorSegment cannot be null!!");
			}

			localBusinessSectorSegment.serialize(new javax.xml.namespace.QName("", "businessSectorSegment"), xmlWriter);

			if (localMarketingPackage == null) {
				throw new org.apache.axis2.databinding.ADBException("marketingPackage cannot be null!!");
			}

			localMarketingPackage.serialize(new javax.xml.namespace.QName("", "marketingPackage"), xmlWriter);

			xmlWriter.writeEndElement();
		}

		private static java.lang.String generatePrefix(java.lang.String namespace) {
			if (namespace.equals("http://servicehelpers.esb.avalon.com")) {
				return "ns1";
			}

			return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
		}

		/**
		 * Utility method to write an element start tag.
		 */
		private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
				javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
			java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

			if (writerPrefix != null) {
				xmlWriter.writeStartElement(namespace, localPart);
			} else {
				if (namespace.length() == 0) {
					prefix = "";
				} else if (prefix == null) {
					prefix = generatePrefix(namespace);
				}

				xmlWriter.writeStartElement(prefix, localPart, namespace);
				xmlWriter.writeNamespace(prefix, namespace);
				xmlWriter.setPrefix(prefix, namespace);
			}
		}

		/**
		 * Util method to write an attribute with the ns prefix
		 */
		private void writeAttribute(java.lang.String prefix, java.lang.String namespace, java.lang.String attName, java.lang.String attValue,
				javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
			if (xmlWriter.getPrefix(namespace) == null) {
				xmlWriter.writeNamespace(prefix, namespace);
				xmlWriter.setPrefix(prefix, namespace);
			}

			xmlWriter.writeAttribute(namespace, attName, attValue);
		}

		/**
		 * Util method to write an attribute without the ns prefix
		 */
		private void writeAttribute(java.lang.String namespace, java.lang.String attName, java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter)
				throws javax.xml.stream.XMLStreamException {
			if (namespace.equals("")) {
				xmlWriter.writeAttribute(attName, attValue);
			} else {
				registerPrefix(xmlWriter, namespace);
				xmlWriter.writeAttribute(namespace, attName, attValue);
			}
		}

		/**
		 * Util method to write an attribute without the ns prefix
		 */
		private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName, javax.xml.namespace.QName qname,
				javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
			java.lang.String attributeNamespace = qname.getNamespaceURI();
			java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);

			if (attributePrefix == null) {
				attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
			}

			java.lang.String attributeValue;

			if (attributePrefix.trim().length() > 0) {
				attributeValue = attributePrefix + ":" + qname.getLocalPart();
			} else {
				attributeValue = qname.getLocalPart();
			}

			if (namespace.equals("")) {
				xmlWriter.writeAttribute(attName, attributeValue);
			} else {
				registerPrefix(xmlWriter, namespace);
				xmlWriter.writeAttribute(namespace, attName, attributeValue);
			}
		}

		/**
		 * method to handle Qnames
		 */
		private void writeQName(javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
			java.lang.String namespaceURI = qname.getNamespaceURI();

			if (namespaceURI != null) {
				java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);

				if (prefix == null) {
					prefix = generatePrefix(namespaceURI);
					xmlWriter.writeNamespace(prefix, namespaceURI);
					xmlWriter.setPrefix(prefix, namespaceURI);
				}

				if (prefix.trim().length() > 0) {
					xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
				} else {
					// i.e this is the default namespace
					xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
				}
			} else {
				xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
			}
		}

		private void writeQNames(javax.xml.namespace.QName[] qnames, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
			if (qnames != null) {
				// we have to store this data until last moment since it is not
				// possible to write any
				// namespace data after writing the charactor data
				java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
				java.lang.String namespaceURI = null;
				java.lang.String prefix = null;

				for (int i = 0; i < qnames.length; i++) {
					if (i > 0) {
						stringToWrite.append(" ");
					}

					namespaceURI = qnames[i].getNamespaceURI();

					if (namespaceURI != null) {
						prefix = xmlWriter.getPrefix(namespaceURI);

						if ((prefix == null) || (prefix.length() == 0)) {
							prefix = generatePrefix(namespaceURI);
							xmlWriter.writeNamespace(prefix, namespaceURI);
							xmlWriter.setPrefix(prefix, namespaceURI);
						}

						if (prefix.trim().length() > 0) {
							stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
						} else {
							stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
						}
					} else {
						stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
					}
				}

				xmlWriter.writeCharacters(stringToWrite.toString());
			}
		}

		/**
		 * Register a namespace prefix
		 */
		private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace)
				throws javax.xml.stream.XMLStreamException {
			java.lang.String prefix = xmlWriter.getPrefix(namespace);

			if (prefix == null) {
				prefix = generatePrefix(namespace);

				javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();

				while (true) {
					java.lang.String uri = nsContext.getNamespaceURI(prefix);

					if ((uri == null) || (uri.length() == 0)) {
						break;
					}

					prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
				}

				xmlWriter.writeNamespace(prefix, namespace);
				xmlWriter.setPrefix(prefix, namespace);
			}

			return prefix;
		}

		/**
		 * databinding method to get an XML representation of this object
		 *
		 */
		public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName) throws org.apache.axis2.databinding.ADBException {
			java.util.ArrayList elementList = new java.util.ArrayList();
			java.util.ArrayList attribList = new java.util.ArrayList();

			elementList.add(new javax.xml.namespace.QName("", "coverageBeginDate"));

			if (localCoverageBeginDate != null) {
				elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCoverageBeginDate));
			} else {
				throw new org.apache.axis2.databinding.ADBException("coverageBeginDate cannot be null!!");
			}

			elementList.add(new javax.xml.namespace.QName("", "coverageEndDate"));

			if (localCoverageEndDate != null) {
				elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCoverageEndDate));
			} else {
				throw new org.apache.axis2.databinding.ADBException("coverageEndDate cannot be null!!");
			}

			elementList.add(new javax.xml.namespace.QName("", "healthPlanGroupId"));

			if (localHealthPlanGroupId != null) {
				elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localHealthPlanGroupId));
			} else {
				throw new org.apache.axis2.databinding.ADBException("healthPlanGroupId cannot be null!!");
			}

			elementList.add(new javax.xml.namespace.QName("", "labBenefitId"));

			if (localLabBenefitId != null) {
				elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLabBenefitId));
			} else {
				throw new org.apache.axis2.databinding.ADBException("labBenefitId cannot be null!!");
			}

			elementList.add(new javax.xml.namespace.QName("", "labBenefitIdDescription"));

			if (localLabBenefitIdDescription != null) {
				elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLabBenefitIdDescription));
			} else {
				throw new org.apache.axis2.databinding.ADBException("labBenefitIdDescription cannot be null!!");
			}

			elementList.add(new javax.xml.namespace.QName("", "clientNumber"));

			if (localClientNumber != null) {
				elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localClientNumber));
			} else {
				throw new org.apache.axis2.databinding.ADBException("clientNumber cannot be null!!");
			}

			elementList.add(new javax.xml.namespace.QName("", "clientNumberDescription"));

			if (localClientNumberDescription != null) {
				elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localClientNumberDescription));
			} else {
				throw new org.apache.axis2.databinding.ADBException("clientNumberDescription cannot be null!!");
			}

			elementList.add(new javax.xml.namespace.QName("", "cesGroupNumber"));

			if (localCesGroupNumber != null) {
				elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCesGroupNumber));
			} else {
				throw new org.apache.axis2.databinding.ADBException("cesGroupNumber cannot be null!!");
			}

			elementList.add(new javax.xml.namespace.QName("", "cesGroupNumberDescription"));

			if (localCesGroupNumberDescription != null) {
				elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCesGroupNumberDescription));
			} else {
				throw new org.apache.axis2.databinding.ADBException("cesGroupNumberDescription cannot be null!!");
			}

			elementList.add(new javax.xml.namespace.QName("", "ammsGroupNumber"));

			if (localAmmsGroupNumber != null) {
				elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localAmmsGroupNumber));
			} else {
				throw new org.apache.axis2.databinding.ADBException("ammsGroupNumber cannot be null!!");
			}

			elementList.add(new javax.xml.namespace.QName("", "serviceCenterCode"));

			if (localServiceCenterCode != null) {
				elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localServiceCenterCode));
			} else {
				throw new org.apache.axis2.databinding.ADBException("serviceCenterCode cannot be null!!");
			}

			elementList.add(new javax.xml.namespace.QName("", "serviceCenterDescription"));

			if (localServiceCenterDescription != null) {
				elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localServiceCenterDescription));
			} else {
				throw new org.apache.axis2.databinding.ADBException("serviceCenterDescription cannot be null!!");
			}

			elementList.add(new javax.xml.namespace.QName("", "businessSectorSegment"));

			if (localBusinessSectorSegment == null) {
				throw new org.apache.axis2.databinding.ADBException("businessSectorSegment cannot be null!!");
			}

			elementList.add(localBusinessSectorSegment);

			elementList.add(new javax.xml.namespace.QName("", "marketingPackage"));

			if (localMarketingPackage == null) {
				throw new org.apache.axis2.databinding.ADBException("marketingPackage cannot be null!!");
			}

			elementList.add(localMarketingPackage);

			return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
		}

		/**
		 * Factory class that keeps the parse method
		 */
		public static class Factory {
			/**
			 * static method to create the object Precondition: If this object is an element, the current or next start element starts this object and any
			 * intervening reader events are ignorable If this object is not an element, it is a complex type and the reader is at the event just after the
			 * outer start element Postcondition: If this object is an element, the reader is positioned at its end element If this object is a complex type,
			 * the reader is positioned at the end element of its outer element
			 */
			public static Coverage parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception {
				Coverage object = new Coverage();
				boolean readerEnabled = false; // to skip the coverage tag when
												// there is no coverage tag in
												// the cds response
				int event;
				java.lang.String nillableValue = null;
				java.lang.String prefix = "";
				java.lang.String namespaceuri = "";

				try {
					while (!reader.isStartElement() && !reader.isEndElement())
						reader.next();

					if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
						java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type");

						if (fullTypeName != null) {
							java.lang.String nsPrefix = null;

							if (fullTypeName.indexOf(":") > -1) {
								nsPrefix = fullTypeName.substring(0, fullTypeName.indexOf(":"));
							}

							nsPrefix = (nsPrefix == null) ? "" : nsPrefix;

							java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":") + 1);

							if (!"Coverage".equals(type)) {
								// find namespace for the prefix
								java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);

								return (Coverage) ExtensionMapper.getTypeObject(nsUri, type, reader);
							}
						}
					}

					// Note all attributes that were handled. Used to differ
					// normal attributes
					// from anyAttributes.
					java.util.Vector handledAttributes = new java.util.Vector();

					reader.next();

					while (!reader.isStartElement() && !reader.isEndElement())
						reader.next();
					log.info("coverageBeginDate" + reader.getName());
					if (reader.isStartElement() && new javax.xml.namespace.QName("", "coverageBeginDate").equals(reader.getName())) {
						nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");

						if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
							throw new org.apache.axis2.databinding.ADBException("The element: " + "coverageBeginDate" + "  cannot be null");
						}

						java.lang.String content = reader.getElementText();

						log.info("coverageBeginDate content" + content);

						object.setCoverageBeginDate(org.apache.axis2.databinding.utils.ConverterUtil.convertToDate(content));

						reader.next();
					} // End of if for expected property start element

					else {
						// A start element we are not expecting indicates an
						// invalid parameter was passed
						throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
					}

					while (!reader.isStartElement() && !reader.isEndElement())
						reader.next();
					log.info("coverageEndDate" + reader.getName());
					if (new javax.xml.namespace.QName("", "healthPlanGroupId").equals(reader.getName())) {
						readerEnabled = true; // Disabling the reader to read
												// twice.
						log.info("coverageEndDate readerEnabled  " + readerEnabled);
					}
					if (new javax.xml.namespace.QName("", "coverageEndDate").equals(reader.getName())) {
						if (reader.isStartElement() && new javax.xml.namespace.QName("", "coverageEndDate").equals(reader.getName())) {
							nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");

							if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
								throw new org.apache.axis2.databinding.ADBException("The element: " + "coverageEndDate" + "  cannot be null");
							}

							java.lang.String content = reader.getElementText();
							log.info("coverageEndDate content" + content);

							object.setCoverageEndDate(org.apache.axis2.databinding.utils.ConverterUtil.convertToDate(content));

							reader.next();
						} // End of if for expected property start element

						else {
							// A start element we are not expecting indicates an
							// invalid parameter was passed
							throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
						}
					}

					while (!reader.isStartElement() && !reader.isEndElement()) {
						if (readerEnabled == false) { // to read the
														// healthPlanGroupId tag
														// when readerEnabled is
														// false
							reader.next();
						}
					}
					log.info("healthPlanGroupId" + reader.getName());
					if (reader.isStartElement() && new javax.xml.namespace.QName("", "healthPlanGroupId").equals(reader.getName())) {
						nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");

						if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
							throw new org.apache.axis2.databinding.ADBException("The element: " + "healthPlanGroupId" + "  cannot be null");
						}

						java.lang.String content = reader.getElementText();
						log.info("healthPlanGroupId content" + content);
						object.setHealthPlanGroupId(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

						reader.next();
					} // End of if for expected property start element

					else {
						// A start element we are not expecting indicates an
						// invalid parameter was passed
						throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
					}

					while (!reader.isStartElement() && !reader.isEndElement())
						reader.next();
					log.info("labBenefitId" + reader.getName());
					if (reader.isStartElement() && new javax.xml.namespace.QName("", "labBenefitId").equals(reader.getName())) {
						nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");

						if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
							throw new org.apache.axis2.databinding.ADBException("The element: " + "labBenefitId" + "  cannot be null");
						}

						java.lang.String content = reader.getElementText();
						log.info("labBenefitId content" + content);
						object.setLabBenefitId(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

						reader.next();
					} // End of if for expected property start element

					else {
						// A start element we are not expecting indicates an
						// invalid parameter was passed
						throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
					}

					while (!reader.isStartElement() && !reader.isEndElement())
						reader.next();
					log.info("labBenefitIdDescription" + reader.getName());
					if (reader.isStartElement() && new javax.xml.namespace.QName("", "labBenefitIdDescription").equals(reader.getName())) {
						nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");

						if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
							throw new org.apache.axis2.databinding.ADBException("The element: " + "labBenefitIdDescription" + "  cannot be null");
						}

						java.lang.String content = reader.getElementText();

						object.setLabBenefitIdDescription(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

						reader.next();
					} // End of if for expected property start element

					else {
						// A start element we are not expecting indicates an
						// invalid parameter was passed
						throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
					}

					while (!reader.isStartElement() && !reader.isEndElement())
						reader.next();

					if (reader.isStartElement() && new javax.xml.namespace.QName("", "clientNumber").equals(reader.getName())) {
						nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");

						if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
							throw new org.apache.axis2.databinding.ADBException("The element: " + "clientNumber" + "  cannot be null");
						}

						java.lang.String content = reader.getElementText();

						object.setClientNumber(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

						reader.next();
					} // End of if for expected property start element

					else {
						// A start element we are not expecting indicates an
						// invalid parameter was passed
						throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
					}

					while (!reader.isStartElement() && !reader.isEndElement())
						reader.next();

					if (reader.isStartElement() && new javax.xml.namespace.QName("", "clientNumberDescription").equals(reader.getName())) {
						nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");

						if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
							throw new org.apache.axis2.databinding.ADBException("The element: " + "clientNumberDescription" + "  cannot be null");
						}

						java.lang.String content = reader.getElementText();

						object.setClientNumberDescription(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

						reader.next();
					} // End of if for expected property start element

					else {
						// A start element we are not expecting indicates an
						// invalid parameter was passed
						throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
					}

					while (!reader.isStartElement() && !reader.isEndElement())
						reader.next();

					if (reader.isStartElement() && new javax.xml.namespace.QName("", "cesGroupNumber").equals(reader.getName())) {
						nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");

						if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
							throw new org.apache.axis2.databinding.ADBException("The element: " + "cesGroupNumber" + "  cannot be null");
						}

						java.lang.String content = reader.getElementText();

						object.setCesGroupNumber(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

						reader.next();
					} // End of if for expected property start element

					else {
						// A start element we are not expecting indicates an
						// invalid parameter was passed
						throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
					}

					while (!reader.isStartElement() && !reader.isEndElement())
						reader.next();

					if (reader.isStartElement() && new javax.xml.namespace.QName("", "cesGroupNumberDescription").equals(reader.getName())) {
						nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");

						if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
							throw new org.apache.axis2.databinding.ADBException("The element: " + "cesGroupNumberDescription" + "  cannot be null");
						}

						java.lang.String content = reader.getElementText();

						object.setCesGroupNumberDescription(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

						reader.next();
					} // End of if for expected property start element

					else {
						// A start element we are not expecting indicates an
						// invalid parameter was passed
						throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
					}

					while (!reader.isStartElement() && !reader.isEndElement())
						reader.next();

					if (reader.isStartElement() && new javax.xml.namespace.QName("", "ammsGroupNumber").equals(reader.getName())) {
						nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");

						if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
							throw new org.apache.axis2.databinding.ADBException("The element: " + "ammsGroupNumber" + "  cannot be null");
						}

						java.lang.String content = reader.getElementText();

						object.setAmmsGroupNumber(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

						reader.next();
					} // End of if for expected property start element

					else {
						// A start element we are not expecting indicates an
						// invalid parameter was passed
						throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
					}

					while (!reader.isStartElement() && !reader.isEndElement())
						reader.next();

					if (reader.isStartElement() && new javax.xml.namespace.QName("", "serviceCenterCode").equals(reader.getName())) {
						nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");

						if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
							throw new org.apache.axis2.databinding.ADBException("The element: " + "serviceCenterCode" + "  cannot be null");
						}

						java.lang.String content = reader.getElementText();

						object.setServiceCenterCode(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

						reader.next();
					} // End of if for expected property start element

					else {
						// A start element we are not expecting indicates an
						// invalid parameter was passed
						throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
					}

					while (!reader.isStartElement() && !reader.isEndElement())
						reader.next();

					if (reader.isStartElement() && new javax.xml.namespace.QName("", "serviceCenterDescription").equals(reader.getName())) {
						nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");

						if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
							throw new org.apache.axis2.databinding.ADBException("The element: " + "serviceCenterDescription" + "  cannot be null");
						}

						java.lang.String content = reader.getElementText();

						object.setServiceCenterDescription(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

						reader.next();
					} // End of if for expected property start element

					else {
						// A start element we are not expecting indicates an
						// invalid parameter was passed
						throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
					}

					while (!reader.isStartElement() && !reader.isEndElement())
						reader.next();

					if (reader.isStartElement() && new javax.xml.namespace.QName("", "businessSectorSegment").equals(reader.getName())) {
						object.setBusinessSectorSegment(BusinessSectorSegment.Factory.parse(reader));

						reader.next();
					} // End of if for expected property start element

					else {
						// A start element we are not expecting indicates an
						// invalid parameter was passed
						throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
					}

					while (!reader.isStartElement() && !reader.isEndElement())
						reader.next();

					if (reader.isStartElement() && new javax.xml.namespace.QName("", "marketingPackage").equals(reader.getName())) {
						object.setMarketingPackage(MarketingPackage.Factory.parse(reader));

						reader.next();
					} // End of if for expected property start element

					else {
						// A start element we are not expecting indicates an
						// invalid parameter was passed
						throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
					}

					while (!reader.isStartElement() && !reader.isEndElement())
						reader.next();

					if (reader.isStartElement()) {
						// A start element we are not expecting indicates a
						// trailing invalid property
						throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
					}
				} catch (javax.xml.stream.XMLStreamException e) {
					throw new java.lang.Exception(e);
				}

				return object;
			}
		} // end of factory class
	}

	public static class GetMemberData implements org.apache.axis2.databinding.ADBBean {
		public static final javax.xml.namespace.QName MY_QNAME = new javax.xml.namespace.QName("http://servicehelpers.esb.avalon.com", "getMemberData", "ns1");

		/**
		 * field for GetMemberData
		 */
		protected MemberDataRequest localGetMemberData;

		/**
		 * Auto generated getter method
		 * 
		 * @return MemberDataRequest
		 */
		public MemberDataRequest getGetMemberData() {
			return localGetMemberData;
		}

		/**
		 * Auto generated setter method
		 * 
		 * @param param
		 *            GetMemberData
		 */
		public void setGetMemberData(MemberDataRequest param) {
			this.localGetMemberData = param;
		}

		/**
		 *
		 * @param parentQName
		 * @param factory
		 * @return org.apache.axiom.om.OMElement
		 */
		public org.apache.axiom.om.OMElement getOMElement(final javax.xml.namespace.QName parentQName, final org.apache.axiom.om.OMFactory factory)
				throws org.apache.axis2.databinding.ADBException {
			org.apache.axiom.om.OMDataSource dataSource = new org.apache.axis2.databinding.ADBDataSource(this, MY_QNAME);

			return factory.createOMElement(dataSource, MY_QNAME);
		}

		public void serialize(final javax.xml.namespace.QName parentQName, javax.xml.stream.XMLStreamWriter xmlWriter)
				throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {
			serialize(parentQName, xmlWriter, false);
		}

		public void serialize(final javax.xml.namespace.QName parentQName, javax.xml.stream.XMLStreamWriter xmlWriter, boolean serializeType)
				throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {
			// We can safely assume an element has only one type associated with
			// it
			if (localGetMemberData == null) {
				throw new org.apache.axis2.databinding.ADBException("getMemberData cannot be null!");
			}

			localGetMemberData.serialize(MY_QNAME, xmlWriter);
		}

		private static java.lang.String generatePrefix(java.lang.String namespace) {
			if (namespace.equals("http://servicehelpers.esb.avalon.com")) {
				return "ns1";
			}

			return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
		}

		/**
		 * Utility method to write an element start tag.
		 */
		private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
				javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
			java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

			if (writerPrefix != null) {
				xmlWriter.writeStartElement(namespace, localPart);
			} else {
				if (namespace.length() == 0) {
					prefix = "";
				} else if (prefix == null) {
					prefix = generatePrefix(namespace);
				}

				xmlWriter.writeStartElement(prefix, localPart, namespace);
				xmlWriter.writeNamespace(prefix, namespace);
				xmlWriter.setPrefix(prefix, namespace);
			}
		}

		/**
		 * Util method to write an attribute with the ns prefix
		 */
		private void writeAttribute(java.lang.String prefix, java.lang.String namespace, java.lang.String attName, java.lang.String attValue,
				javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
			if (xmlWriter.getPrefix(namespace) == null) {
				xmlWriter.writeNamespace(prefix, namespace);
				xmlWriter.setPrefix(prefix, namespace);
			}

			xmlWriter.writeAttribute(namespace, attName, attValue);
		}

		/**
		 * Util method to write an attribute without the ns prefix
		 */
		private void writeAttribute(java.lang.String namespace, java.lang.String attName, java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter)
				throws javax.xml.stream.XMLStreamException {
			if (namespace.equals("")) {
				xmlWriter.writeAttribute(attName, attValue);
			} else {
				registerPrefix(xmlWriter, namespace);
				xmlWriter.writeAttribute(namespace, attName, attValue);
			}
		}

		/**
		 * Util method to write an attribute without the ns prefix
		 */
		private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName, javax.xml.namespace.QName qname,
				javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
			java.lang.String attributeNamespace = qname.getNamespaceURI();
			java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);

			if (attributePrefix == null) {
				attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
			}

			java.lang.String attributeValue;

			if (attributePrefix.trim().length() > 0) {
				attributeValue = attributePrefix + ":" + qname.getLocalPart();
			} else {
				attributeValue = qname.getLocalPart();
			}

			if (namespace.equals("")) {
				xmlWriter.writeAttribute(attName, attributeValue);
			} else {
				registerPrefix(xmlWriter, namespace);
				xmlWriter.writeAttribute(namespace, attName, attributeValue);
			}
		}

		/**
		 * method to handle Qnames
		 */
		private void writeQName(javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
			java.lang.String namespaceURI = qname.getNamespaceURI();

			if (namespaceURI != null) {
				java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);

				if (prefix == null) {
					prefix = generatePrefix(namespaceURI);
					xmlWriter.writeNamespace(prefix, namespaceURI);
					xmlWriter.setPrefix(prefix, namespaceURI);
				}

				if (prefix.trim().length() > 0) {
					xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
				} else {
					// i.e this is the default namespace
					xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
				}
			} else {
				xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
			}
		}

		private void writeQNames(javax.xml.namespace.QName[] qnames, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
			if (qnames != null) {
				// we have to store this data until last moment since it is not
				// possible to write any
				// namespace data after writing the charactor data
				java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
				java.lang.String namespaceURI = null;
				java.lang.String prefix = null;

				for (int i = 0; i < qnames.length; i++) {
					if (i > 0) {
						stringToWrite.append(" ");
					}

					namespaceURI = qnames[i].getNamespaceURI();

					if (namespaceURI != null) {
						prefix = xmlWriter.getPrefix(namespaceURI);

						if ((prefix == null) || (prefix.length() == 0)) {
							prefix = generatePrefix(namespaceURI);
							xmlWriter.writeNamespace(prefix, namespaceURI);
							xmlWriter.setPrefix(prefix, namespaceURI);
						}

						if (prefix.trim().length() > 0) {
							stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
						} else {
							stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
						}
					} else {
						stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
					}
				}

				xmlWriter.writeCharacters(stringToWrite.toString());
			}
		}

		/**
		 * Register a namespace prefix
		 */
		private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace)
				throws javax.xml.stream.XMLStreamException {
			java.lang.String prefix = xmlWriter.getPrefix(namespace);

			if (prefix == null) {
				prefix = generatePrefix(namespace);

				javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();

				while (true) {
					java.lang.String uri = nsContext.getNamespaceURI(prefix);

					if ((uri == null) || (uri.length() == 0)) {
						break;
					}

					prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
				}

				xmlWriter.writeNamespace(prefix, namespace);
				xmlWriter.setPrefix(prefix, namespace);
			}

			return prefix;
		}

		/**
		 * databinding method to get an XML representation of this object
		 *
		 */
		public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName) throws org.apache.axis2.databinding.ADBException {
			// We can safely assume an element has only one type associated with
			// it
			return localGetMemberData.getPullParser(MY_QNAME);
		}

		/**
		 * Factory class that keeps the parse method
		 */
		public static class Factory {
			/**
			 * static method to create the object Precondition: If this object is an element, the current or next start element starts this object and any
			 * intervening reader events are ignorable If this object is not an element, it is a complex type and the reader is at the event just after the
			 * outer start element Postcondition: If this object is an element, the reader is positioned at its end element If this object is a complex type,
			 * the reader is positioned at the end element of its outer element
			 */
			public static GetMemberData parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception {
				GetMemberData object = new GetMemberData();

				int event;
				java.lang.String nillableValue = null;
				java.lang.String prefix = "";
				java.lang.String namespaceuri = "";

				try {
					while (!reader.isStartElement() && !reader.isEndElement())
						reader.next();

					// Note all attributes that were handled. Used to differ
					// normal attributes
					// from anyAttributes.
					java.util.Vector handledAttributes = new java.util.Vector();

					while (!reader.isEndElement()) {
						if (reader.isStartElement()) {
							if (reader.isStartElement()
									&& new javax.xml.namespace.QName("http://servicehelpers.esb.avalon.com", "getMemberData").equals(reader.getName())) {
								object.setGetMemberData(MemberDataRequest.Factory.parse(reader));

							} // End of if for expected property start element

							else {
								// A start element we are not expecting
								// indicates an invalid parameter was passed
								throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
							}
						} else {
							reader.next();
						}
					} // end of while loop
				} catch (javax.xml.stream.XMLStreamException e) {
					throw new java.lang.Exception(e);
				}

				return object;
			}
		} // end of factory class
	}

	public static class ContactData implements org.apache.axis2.databinding.ADBBean {
		/*
		 * This type was generated from the piece of schema that had name = ContactData Namespace URI = http://servicehelpers.esb.avalon.com Namespace Prefix =
		 * ns1
		 */

		/**
		 * field for PhoneNumber
		 */
		protected java.lang.String localPhoneNumber;

		/**
		 * field for EmailAddress
		 */
		protected java.lang.String localEmailAddress;

		/**
		 * Auto generated getter method
		 * 
		 * @return java.lang.String
		 */
		public java.lang.String getPhoneNumber() {
			return localPhoneNumber;
		}

		/**
		 * Auto generated setter method
		 * 
		 * @param param
		 *            PhoneNumber
		 */
		public void setPhoneNumber(java.lang.String param) {
			this.localPhoneNumber = param;
		}

		/**
		 * Auto generated getter method
		 * 
		 * @return java.lang.String
		 */
		public java.lang.String getEmailAddress() {
			return localEmailAddress;
		}

		/**
		 * Auto generated setter method
		 * 
		 * @param param
		 *            EmailAddress
		 */
		public void setEmailAddress(java.lang.String param) {
			this.localEmailAddress = param;
		}

		/**
		 *
		 * @param parentQName
		 * @param factory
		 * @return org.apache.axiom.om.OMElement
		 */
		public org.apache.axiom.om.OMElement getOMElement(final javax.xml.namespace.QName parentQName, final org.apache.axiom.om.OMFactory factory)
				throws org.apache.axis2.databinding.ADBException {
			org.apache.axiom.om.OMDataSource dataSource = new org.apache.axis2.databinding.ADBDataSource(this, parentQName);

			return factory.createOMElement(dataSource, parentQName);
		}

		public void serialize(final javax.xml.namespace.QName parentQName, javax.xml.stream.XMLStreamWriter xmlWriter)
				throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {
			serialize(parentQName, xmlWriter, false);
		}

		public void serialize(final javax.xml.namespace.QName parentQName, javax.xml.stream.XMLStreamWriter xmlWriter, boolean serializeType)
				throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {
			java.lang.String prefix = null;
			java.lang.String namespace = null;

			prefix = parentQName.getPrefix();
			namespace = parentQName.getNamespaceURI();
			writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);

			if (serializeType) {
				java.lang.String namespacePrefix = registerPrefix(xmlWriter, "http://servicehelpers.esb.avalon.com");

				if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)) {
					writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", namespacePrefix + ":ContactData", xmlWriter);
				} else {
					writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", "ContactData", xmlWriter);
				}
			}

			namespace = "";
			writeStartElement(null, namespace, "phoneNumber", xmlWriter);

			if (localPhoneNumber == null) {
				// write the nil attribute
				throw new org.apache.axis2.databinding.ADBException("phoneNumber cannot be null!!");
			} else {
				xmlWriter.writeCharacters(localPhoneNumber);
			}

			xmlWriter.writeEndElement();

			namespace = "";
			writeStartElement(null, namespace, "emailAddress", xmlWriter);

			if (localEmailAddress == null) {
				// write the nil attribute
				throw new org.apache.axis2.databinding.ADBException("emailAddress cannot be null!!");
			} else {
				xmlWriter.writeCharacters(localEmailAddress);
			}

			xmlWriter.writeEndElement();

			xmlWriter.writeEndElement();
		}

		private static java.lang.String generatePrefix(java.lang.String namespace) {
			if (namespace.equals("http://servicehelpers.esb.avalon.com")) {
				return "ns1";
			}

			return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
		}

		/**
		 * Utility method to write an element start tag.
		 */
		private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
				javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
			java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

			if (writerPrefix != null) {
				xmlWriter.writeStartElement(namespace, localPart);
			} else {
				if (namespace.length() == 0) {
					prefix = "";
				} else if (prefix == null) {
					prefix = generatePrefix(namespace);
				}

				xmlWriter.writeStartElement(prefix, localPart, namespace);
				xmlWriter.writeNamespace(prefix, namespace);
				xmlWriter.setPrefix(prefix, namespace);
			}
		}

		/**
		 * Util method to write an attribute with the ns prefix
		 */
		private void writeAttribute(java.lang.String prefix, java.lang.String namespace, java.lang.String attName, java.lang.String attValue,
				javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
			if (xmlWriter.getPrefix(namespace) == null) {
				xmlWriter.writeNamespace(prefix, namespace);
				xmlWriter.setPrefix(prefix, namespace);
			}

			xmlWriter.writeAttribute(namespace, attName, attValue);
		}

		/**
		 * Util method to write an attribute without the ns prefix
		 */
		private void writeAttribute(java.lang.String namespace, java.lang.String attName, java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter)
				throws javax.xml.stream.XMLStreamException {
			if (namespace.equals("")) {
				xmlWriter.writeAttribute(attName, attValue);
			} else {
				registerPrefix(xmlWriter, namespace);
				xmlWriter.writeAttribute(namespace, attName, attValue);
			}
		}

		/**
		 * Util method to write an attribute without the ns prefix
		 */
		private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName, javax.xml.namespace.QName qname,
				javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
			java.lang.String attributeNamespace = qname.getNamespaceURI();
			java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);

			if (attributePrefix == null) {
				attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
			}

			java.lang.String attributeValue;

			if (attributePrefix.trim().length() > 0) {
				attributeValue = attributePrefix + ":" + qname.getLocalPart();
			} else {
				attributeValue = qname.getLocalPart();
			}

			if (namespace.equals("")) {
				xmlWriter.writeAttribute(attName, attributeValue);
			} else {
				registerPrefix(xmlWriter, namespace);
				xmlWriter.writeAttribute(namespace, attName, attributeValue);
			}
		}

		/**
		 * method to handle Qnames
		 */
		private void writeQName(javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
			java.lang.String namespaceURI = qname.getNamespaceURI();

			if (namespaceURI != null) {
				java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);

				if (prefix == null) {
					prefix = generatePrefix(namespaceURI);
					xmlWriter.writeNamespace(prefix, namespaceURI);
					xmlWriter.setPrefix(prefix, namespaceURI);
				}

				if (prefix.trim().length() > 0) {
					xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
				} else {
					// i.e this is the default namespace
					xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
				}
			} else {
				xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
			}
		}

		private void writeQNames(javax.xml.namespace.QName[] qnames, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
			if (qnames != null) {
				// we have to store this data until last moment since it is not
				// possible to write any
				// namespace data after writing the charactor data
				java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
				java.lang.String namespaceURI = null;
				java.lang.String prefix = null;

				for (int i = 0; i < qnames.length; i++) {
					if (i > 0) {
						stringToWrite.append(" ");
					}

					namespaceURI = qnames[i].getNamespaceURI();

					if (namespaceURI != null) {
						prefix = xmlWriter.getPrefix(namespaceURI);

						if ((prefix == null) || (prefix.length() == 0)) {
							prefix = generatePrefix(namespaceURI);
							xmlWriter.writeNamespace(prefix, namespaceURI);
							xmlWriter.setPrefix(prefix, namespaceURI);
						}

						if (prefix.trim().length() > 0) {
							stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
						} else {
							stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
						}
					} else {
						stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
					}
				}

				xmlWriter.writeCharacters(stringToWrite.toString());
			}
		}

		/**
		 * Register a namespace prefix
		 */
		private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace)
				throws javax.xml.stream.XMLStreamException {
			java.lang.String prefix = xmlWriter.getPrefix(namespace);

			if (prefix == null) {
				prefix = generatePrefix(namespace);

				javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();

				while (true) {
					java.lang.String uri = nsContext.getNamespaceURI(prefix);

					if ((uri == null) || (uri.length() == 0)) {
						break;
					}

					prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
				}

				xmlWriter.writeNamespace(prefix, namespace);
				xmlWriter.setPrefix(prefix, namespace);
			}

			return prefix;
		}

		/**
		 * databinding method to get an XML representation of this object
		 *
		 */
		public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName) throws org.apache.axis2.databinding.ADBException {
			java.util.ArrayList elementList = new java.util.ArrayList();
			java.util.ArrayList attribList = new java.util.ArrayList();

			elementList.add(new javax.xml.namespace.QName("", "phoneNumber"));

			if (localPhoneNumber != null) {
				elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPhoneNumber));
			} else {
				throw new org.apache.axis2.databinding.ADBException("phoneNumber cannot be null!!");
			}

			elementList.add(new javax.xml.namespace.QName("", "emailAddress"));

			if (localEmailAddress != null) {
				elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localEmailAddress));
			} else {
				throw new org.apache.axis2.databinding.ADBException("emailAddress cannot be null!!");
			}

			return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
		}

		/**
		 * Factory class that keeps the parse method
		 */
		public static class Factory {
			/**
			 * static method to create the object Precondition: If this object is an element, the current or next start element starts this object and any
			 * intervening reader events are ignorable If this object is not an element, it is a complex type and the reader is at the event just after the
			 * outer start element Postcondition: If this object is an element, the reader is positioned at its end element If this object is a complex type,
			 * the reader is positioned at the end element of its outer element
			 */
			public static ContactData parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception {
				ContactData object = new ContactData();

				int event;
				java.lang.String nillableValue = null;
				java.lang.String prefix = "";
				java.lang.String namespaceuri = "";

				try {
					while (!reader.isStartElement() && !reader.isEndElement())
						reader.next();

					if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
						java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type");

						if (fullTypeName != null) {
							java.lang.String nsPrefix = null;

							if (fullTypeName.indexOf(":") > -1) {
								nsPrefix = fullTypeName.substring(0, fullTypeName.indexOf(":"));
							}

							nsPrefix = (nsPrefix == null) ? "" : nsPrefix;

							java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":") + 1);

							if (!"ContactData".equals(type)) {
								// find namespace for the prefix
								java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);

								return (ContactData) ExtensionMapper.getTypeObject(nsUri, type, reader);
							}
						}
					}

					// Note all attributes that were handled. Used to differ
					// normal attributes
					// from anyAttributes.
					java.util.Vector handledAttributes = new java.util.Vector();

					reader.next();

					while (!reader.isStartElement() && !reader.isEndElement())
						reader.next();

					if (reader.isStartElement() && new javax.xml.namespace.QName("", "phoneNumber").equals(reader.getName())) {
						nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");

						if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
							throw new org.apache.axis2.databinding.ADBException("The element: " + "phoneNumber" + "  cannot be null");
						}

						java.lang.String content = reader.getElementText();

						object.setPhoneNumber(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

						reader.next();
					} // End of if for expected property start element

					else {
						// A start element we are not expecting indicates an
						// invalid parameter was passed
						throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
					}

					while (!reader.isStartElement() && !reader.isEndElement())
						reader.next();

					if (reader.isStartElement() && new javax.xml.namespace.QName("", "emailAddress").equals(reader.getName())) {
						nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");

						if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
							throw new org.apache.axis2.databinding.ADBException("The element: " + "emailAddress" + "  cannot be null");
						}

						java.lang.String content = reader.getElementText();

						object.setEmailAddress(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

						reader.next();
					} // End of if for expected property start element

					else {
						// A start element we are not expecting indicates an
						// invalid parameter was passed
						throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
					}

					while (!reader.isStartElement() && !reader.isEndElement())
						reader.next();

					if (reader.isStartElement()) {
						// A start element we are not expecting indicates a
						// trailing invalid property
						throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
					}
				} catch (javax.xml.stream.XMLStreamException e) {
					throw new java.lang.Exception(e);
				}

				return object;
			}
		} // end of factory class
	}

	public static class Address implements org.apache.axis2.databinding.ADBBean {
		/*
		 * This type was generated from the piece of schema that had name = Address Namespace URI = http://servicehelpers.esb.avalon.com Namespace Prefix = ns1
		 */

		/**
		 * field for AddressLine1
		 */
		protected java.lang.String localAddressLine1;

		/**
		 * field for AddressLine2
		 */
		protected java.lang.String localAddressLine2;

		/**
		 * field for City
		 */
		protected java.lang.String localCity;

		/**
		 * field for StateCode
		 */
		protected java.lang.String localStateCode;

		/**
		 * field for ZipCode
		 */
		protected java.lang.String localZipCode;

		/**
		 * field for StateCountyCode
		 */
		protected java.lang.String localStateCountyCode;

		/**
		 * field for StateCountyDescription
		 */
		protected java.lang.String localStateCountyDescription;

		/**
		 * field for FederalCountyCode
		 */
		protected java.lang.String localFederalCountyCode;

		/**
		 * field for CountryCode
		 */
		protected java.lang.String localCountryCode;

		/**
		 * Auto generated getter method
		 * 
		 * @return java.lang.String
		 */
		public java.lang.String getAddressLine1() {
			return localAddressLine1;
		}

		/**
		 * Auto generated setter method
		 * 
		 * @param param
		 *            AddressLine1
		 */
		public void setAddressLine1(java.lang.String param) {
			this.localAddressLine1 = param;
		}

		/**
		 * Auto generated getter method
		 * 
		 * @return java.lang.String
		 */
		public java.lang.String getAddressLine2() {
			return localAddressLine2;
		}

		/**
		 * Auto generated setter method
		 * 
		 * @param param
		 *            AddressLine2
		 */
		public void setAddressLine2(java.lang.String param) {
			this.localAddressLine2 = param;
		}

		/**
		 * Auto generated getter method
		 * 
		 * @return java.lang.String
		 */
		public java.lang.String getCity() {
			return localCity;
		}

		/**
		 * Auto generated setter method
		 * 
		 * @param param
		 *            City
		 */
		public void setCity(java.lang.String param) {
			this.localCity = param;
		}

		/**
		 * Auto generated getter method
		 * 
		 * @return java.lang.String
		 */
		public java.lang.String getStateCode() {
			return localStateCode;
		}

		/**
		 * Auto generated setter method
		 * 
		 * @param param
		 *            StateCode
		 */
		public void setStateCode(java.lang.String param) {
			this.localStateCode = param;
		}

		/**
		 * Auto generated getter method
		 * 
		 * @return java.lang.String
		 */
		public java.lang.String getZipCode() {
			return localZipCode;
		}

		/**
		 * Auto generated setter method
		 * 
		 * @param param
		 *            ZipCode
		 */
		public void setZipCode(java.lang.String param) {
			this.localZipCode = param;
		}

		/**
		 * Auto generated getter method
		 * 
		 * @return java.lang.String
		 */
		public java.lang.String getStateCountyCode() {
			return localStateCountyCode;
		}

		/**
		 * Auto generated setter method
		 * 
		 * @param param
		 *            StateCountyCode
		 */
		public void setStateCountyCode(java.lang.String param) {
			this.localStateCountyCode = param;
		}

		/**
		 * Auto generated getter method
		 * 
		 * @return java.lang.String
		 */
		public java.lang.String getStateCountyDescription() {
			return localStateCountyDescription;
		}

		/**
		 * Auto generated setter method
		 * 
		 * @param param
		 *            StateCountyDescription
		 */
		public void setStateCountyDescription(java.lang.String param) {
			this.localStateCountyDescription = param;
		}

		/**
		 * Auto generated getter method
		 * 
		 * @return java.lang.String
		 */
		public java.lang.String getFederalCountyCode() {
			return localFederalCountyCode;
		}

		/**
		 * Auto generated setter method
		 * 
		 * @param param
		 *            FederalCountyCode
		 */
		public void setFederalCountyCode(java.lang.String param) {
			this.localFederalCountyCode = param;
		}

		/**
		 * Auto generated getter method
		 * 
		 * @return java.lang.String
		 */
		public java.lang.String getCountryCode() {
			return localCountryCode;
		}

		/**
		 * Auto generated setter method
		 * 
		 * @param param
		 *            CountryCode
		 */
		public void setCountryCode(java.lang.String param) {
			this.localCountryCode = param;
		}

		/**
		 *
		 * @param parentQName
		 * @param factory
		 * @return org.apache.axiom.om.OMElement
		 */
		public org.apache.axiom.om.OMElement getOMElement(final javax.xml.namespace.QName parentQName, final org.apache.axiom.om.OMFactory factory)
				throws org.apache.axis2.databinding.ADBException {
			org.apache.axiom.om.OMDataSource dataSource = new org.apache.axis2.databinding.ADBDataSource(this, parentQName);

			return factory.createOMElement(dataSource, parentQName);
		}

		public void serialize(final javax.xml.namespace.QName parentQName, javax.xml.stream.XMLStreamWriter xmlWriter)
				throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {
			serialize(parentQName, xmlWriter, false);
		}

		public void serialize(final javax.xml.namespace.QName parentQName, javax.xml.stream.XMLStreamWriter xmlWriter, boolean serializeType)
				throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {
			java.lang.String prefix = null;
			java.lang.String namespace = null;

			prefix = parentQName.getPrefix();
			namespace = parentQName.getNamespaceURI();
			writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);

			if (serializeType) {
				java.lang.String namespacePrefix = registerPrefix(xmlWriter, "http://servicehelpers.esb.avalon.com");

				if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)) {
					writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", namespacePrefix + ":Address", xmlWriter);
				} else {
					writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", "Address", xmlWriter);
				}
			}

			namespace = "";
			writeStartElement(null, namespace, "addressLine1", xmlWriter);

			if (localAddressLine1 == null) {
				// write the nil attribute
				throw new org.apache.axis2.databinding.ADBException("addressLine1 cannot be null!!");
			} else {
				xmlWriter.writeCharacters(localAddressLine1);
			}

			xmlWriter.writeEndElement();

			namespace = "";
			writeStartElement(null, namespace, "addressLine2", xmlWriter);

			if (localAddressLine2 == null) {
				// write the nil attribute
				throw new org.apache.axis2.databinding.ADBException("addressLine2 cannot be null!!");
			} else {
				xmlWriter.writeCharacters(localAddressLine2);
			}

			xmlWriter.writeEndElement();

			namespace = "";
			writeStartElement(null, namespace, "city", xmlWriter);

			if (localCity == null) {
				// write the nil attribute
				throw new org.apache.axis2.databinding.ADBException("city cannot be null!!");
			} else {
				xmlWriter.writeCharacters(localCity);
			}

			xmlWriter.writeEndElement();

			namespace = "";
			writeStartElement(null, namespace, "stateCode", xmlWriter);

			if (localStateCode == null) {
				// write the nil attribute
				throw new org.apache.axis2.databinding.ADBException("stateCode cannot be null!!");
			} else {
				xmlWriter.writeCharacters(localStateCode);
			}

			xmlWriter.writeEndElement();

			namespace = "";
			writeStartElement(null, namespace, "zipCode", xmlWriter);

			if (localZipCode == null) {
				// write the nil attribute
				throw new org.apache.axis2.databinding.ADBException("zipCode cannot be null!!");
			} else {
				xmlWriter.writeCharacters(localZipCode);
			}

			xmlWriter.writeEndElement();

			namespace = "";
			writeStartElement(null, namespace, "stateCountyCode", xmlWriter);

			if (localStateCountyCode == null) {
				// write the nil attribute
				throw new org.apache.axis2.databinding.ADBException("stateCountyCode cannot be null!!");
			} else {
				xmlWriter.writeCharacters(localStateCountyCode);
			}

			xmlWriter.writeEndElement();

			namespace = "";
			writeStartElement(null, namespace, "stateCountyDescription", xmlWriter);

			if (localStateCountyDescription == null) {
				// write the nil attribute
				throw new org.apache.axis2.databinding.ADBException("stateCountyDescription cannot be null!!");
			} else {
				xmlWriter.writeCharacters(localStateCountyDescription);
			}

			xmlWriter.writeEndElement();

			namespace = "";
			writeStartElement(null, namespace, "federalCountyCode", xmlWriter);

			if (localFederalCountyCode == null) {
				// write the nil attribute
				throw new org.apache.axis2.databinding.ADBException("federalCountyCode cannot be null!!");
			} else {
				xmlWriter.writeCharacters(localFederalCountyCode);
			}

			xmlWriter.writeEndElement();

			namespace = "";
			writeStartElement(null, namespace, "countryCode", xmlWriter);

			if (localCountryCode == null) {
				// write the nil attribute
				throw new org.apache.axis2.databinding.ADBException("countryCode cannot be null!!");
			} else {
				xmlWriter.writeCharacters(localCountryCode);
			}

			xmlWriter.writeEndElement();

			xmlWriter.writeEndElement();
		}

		private static java.lang.String generatePrefix(java.lang.String namespace) {
			if (namespace.equals("http://servicehelpers.esb.avalon.com")) {
				return "ns1";
			}

			return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
		}

		/**
		 * Utility method to write an element start tag.
		 */
		private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
				javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
			java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

			if (writerPrefix != null) {
				xmlWriter.writeStartElement(namespace, localPart);
			} else {
				if (namespace.length() == 0) {
					prefix = "";
				} else if (prefix == null) {
					prefix = generatePrefix(namespace);
				}

				xmlWriter.writeStartElement(prefix, localPart, namespace);
				xmlWriter.writeNamespace(prefix, namespace);
				xmlWriter.setPrefix(prefix, namespace);
			}
		}

		/**
		 * Util method to write an attribute with the ns prefix
		 */
		private void writeAttribute(java.lang.String prefix, java.lang.String namespace, java.lang.String attName, java.lang.String attValue,
				javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
			if (xmlWriter.getPrefix(namespace) == null) {
				xmlWriter.writeNamespace(prefix, namespace);
				xmlWriter.setPrefix(prefix, namespace);
			}

			xmlWriter.writeAttribute(namespace, attName, attValue);
		}

		/**
		 * Util method to write an attribute without the ns prefix
		 */
		private void writeAttribute(java.lang.String namespace, java.lang.String attName, java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter)
				throws javax.xml.stream.XMLStreamException {
			if (namespace.equals("")) {
				xmlWriter.writeAttribute(attName, attValue);
			} else {
				registerPrefix(xmlWriter, namespace);
				xmlWriter.writeAttribute(namespace, attName, attValue);
			}
		}

		/**
		 * Util method to write an attribute without the ns prefix
		 */
		private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName, javax.xml.namespace.QName qname,
				javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
			java.lang.String attributeNamespace = qname.getNamespaceURI();
			java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);

			if (attributePrefix == null) {
				attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
			}

			java.lang.String attributeValue;

			if (attributePrefix.trim().length() > 0) {
				attributeValue = attributePrefix + ":" + qname.getLocalPart();
			} else {
				attributeValue = qname.getLocalPart();
			}

			if (namespace.equals("")) {
				xmlWriter.writeAttribute(attName, attributeValue);
			} else {
				registerPrefix(xmlWriter, namespace);
				xmlWriter.writeAttribute(namespace, attName, attributeValue);
			}
		}

		/**
		 * method to handle Qnames
		 */
		private void writeQName(javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
			java.lang.String namespaceURI = qname.getNamespaceURI();

			if (namespaceURI != null) {
				java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);

				if (prefix == null) {
					prefix = generatePrefix(namespaceURI);
					xmlWriter.writeNamespace(prefix, namespaceURI);
					xmlWriter.setPrefix(prefix, namespaceURI);
				}

				if (prefix.trim().length() > 0) {
					xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
				} else {
					// i.e this is the default namespace
					xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
				}
			} else {
				xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
			}
		}

		private void writeQNames(javax.xml.namespace.QName[] qnames, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
			if (qnames != null) {
				// we have to store this data until last moment since it is not
				// possible to write any
				// namespace data after writing the charactor data
				java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
				java.lang.String namespaceURI = null;
				java.lang.String prefix = null;

				for (int i = 0; i < qnames.length; i++) {
					if (i > 0) {
						stringToWrite.append(" ");
					}

					namespaceURI = qnames[i].getNamespaceURI();

					if (namespaceURI != null) {
						prefix = xmlWriter.getPrefix(namespaceURI);

						if ((prefix == null) || (prefix.length() == 0)) {
							prefix = generatePrefix(namespaceURI);
							xmlWriter.writeNamespace(prefix, namespaceURI);
							xmlWriter.setPrefix(prefix, namespaceURI);
						}

						if (prefix.trim().length() > 0) {
							stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
						} else {
							stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
						}
					} else {
						stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
					}
				}

				xmlWriter.writeCharacters(stringToWrite.toString());
			}
		}

		/**
		 * Register a namespace prefix
		 */
		private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace)
				throws javax.xml.stream.XMLStreamException {
			java.lang.String prefix = xmlWriter.getPrefix(namespace);

			if (prefix == null) {
				prefix = generatePrefix(namespace);

				javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();

				while (true) {
					java.lang.String uri = nsContext.getNamespaceURI(prefix);

					if ((uri == null) || (uri.length() == 0)) {
						break;
					}

					prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
				}

				xmlWriter.writeNamespace(prefix, namespace);
				xmlWriter.setPrefix(prefix, namespace);
			}

			return prefix;
		}

		/**
		 * databinding method to get an XML representation of this object
		 *
		 */
		public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName) throws org.apache.axis2.databinding.ADBException {
			java.util.ArrayList elementList = new java.util.ArrayList();
			java.util.ArrayList attribList = new java.util.ArrayList();

			elementList.add(new javax.xml.namespace.QName("", "addressLine1"));

			if (localAddressLine1 != null) {
				elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localAddressLine1));
			} else {
				throw new org.apache.axis2.databinding.ADBException("addressLine1 cannot be null!!");
			}

			elementList.add(new javax.xml.namespace.QName("", "addressLine2"));

			if (localAddressLine2 != null) {
				elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localAddressLine2));
			} else {
				throw new org.apache.axis2.databinding.ADBException("addressLine2 cannot be null!!");
			}

			elementList.add(new javax.xml.namespace.QName("", "city"));

			if (localCity != null) {
				elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCity));
			} else {
				throw new org.apache.axis2.databinding.ADBException("city cannot be null!!");
			}

			elementList.add(new javax.xml.namespace.QName("", "stateCode"));

			if (localStateCode != null) {
				elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localStateCode));
			} else {
				throw new org.apache.axis2.databinding.ADBException("stateCode cannot be null!!");
			}

			elementList.add(new javax.xml.namespace.QName("", "zipCode"));

			if (localZipCode != null) {
				elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localZipCode));
			} else {
				throw new org.apache.axis2.databinding.ADBException("zipCode cannot be null!!");
			}

			elementList.add(new javax.xml.namespace.QName("", "stateCountyCode"));

			if (localStateCountyCode != null) {
				elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localStateCountyCode));
			} else {
				throw new org.apache.axis2.databinding.ADBException("stateCountyCode cannot be null!!");
			}

			elementList.add(new javax.xml.namespace.QName("", "stateCountyDescription"));

			if (localStateCountyDescription != null) {
				elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localStateCountyDescription));
			} else {
				throw new org.apache.axis2.databinding.ADBException("stateCountyDescription cannot be null!!");
			}

			elementList.add(new javax.xml.namespace.QName("", "federalCountyCode"));

			if (localFederalCountyCode != null) {
				elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localFederalCountyCode));
			} else {
				throw new org.apache.axis2.databinding.ADBException("federalCountyCode cannot be null!!");
			}

			elementList.add(new javax.xml.namespace.QName("", "countryCode"));

			if (localCountryCode != null) {
				elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCountryCode));
			} else {
				throw new org.apache.axis2.databinding.ADBException("countryCode cannot be null!!");
			}

			return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
		}

		/**
		 * Factory class that keeps the parse method
		 */
		public static class Factory {
			/**
			 * static method to create the object Precondition: If this object is an element, the current or next start element starts this object and any
			 * intervening reader events are ignorable If this object is not an element, it is a complex type and the reader is at the event just after the
			 * outer start element Postcondition: If this object is an element, the reader is positioned at its end element If this object is a complex type,
			 * the reader is positioned at the end element of its outer element
			 */
			public static Address parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception {
				Address object = new Address();

				int event;
				java.lang.String nillableValue = null;
				java.lang.String prefix = "";
				java.lang.String namespaceuri = "";

				try {
					while (!reader.isStartElement() && !reader.isEndElement())
						reader.next();

					if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
						java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type");

						if (fullTypeName != null) {
							java.lang.String nsPrefix = null;

							if (fullTypeName.indexOf(":") > -1) {
								nsPrefix = fullTypeName.substring(0, fullTypeName.indexOf(":"));
							}

							nsPrefix = (nsPrefix == null) ? "" : nsPrefix;

							java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":") + 1);

							if (!"Address".equals(type)) {
								// find namespace for the prefix
								java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);

								return (Address) ExtensionMapper.getTypeObject(nsUri, type, reader);
							}
						}
					}

					// Note all attributes that were handled. Used to differ
					// normal attributes
					// from anyAttributes.
					java.util.Vector handledAttributes = new java.util.Vector();

					reader.next();

					while (!reader.isStartElement() && !reader.isEndElement())
						reader.next();

					if (reader.isStartElement() && new javax.xml.namespace.QName("", "addressLine1").equals(reader.getName())) {
						nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");

						if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
							throw new org.apache.axis2.databinding.ADBException("The element: " + "addressLine1" + "  cannot be null");
						}

						java.lang.String content = reader.getElementText();

						object.setAddressLine1(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

						reader.next();
					} // End of if for expected property start element

					else {
						// A start element we are not expecting indicates an
						// invalid parameter was passed
						throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
					}

					while (!reader.isStartElement() && !reader.isEndElement())
						reader.next();

					if (reader.isStartElement() && new javax.xml.namespace.QName("", "addressLine2").equals(reader.getName())) {
						nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");

						if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
							throw new org.apache.axis2.databinding.ADBException("The element: " + "addressLine2" + "  cannot be null");
						}

						java.lang.String content = reader.getElementText();

						object.setAddressLine2(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

						reader.next();
					} // End of if for expected property start element

					else {
						// A start element we are not expecting indicates an
						// invalid parameter was passed
						throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
					}

					while (!reader.isStartElement() && !reader.isEndElement())
						reader.next();

					if (reader.isStartElement() && new javax.xml.namespace.QName("", "city").equals(reader.getName())) {
						nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");

						if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
							throw new org.apache.axis2.databinding.ADBException("The element: " + "city" + "  cannot be null");
						}

						java.lang.String content = reader.getElementText();

						object.setCity(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

						reader.next();
					} // End of if for expected property start element

					else {
						// A start element we are not expecting indicates an
						// invalid parameter was passed
						throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
					}

					while (!reader.isStartElement() && !reader.isEndElement())
						reader.next();

					if (reader.isStartElement() && new javax.xml.namespace.QName("", "stateCode").equals(reader.getName())) {
						nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");

						if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
							throw new org.apache.axis2.databinding.ADBException("The element: " + "stateCode" + "  cannot be null");
						}

						java.lang.String content = reader.getElementText();

						object.setStateCode(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

						reader.next();
					} // End of if for expected property start element

					else {
						// A start element we are not expecting indicates an
						// invalid parameter was passed
						throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
					}

					while (!reader.isStartElement() && !reader.isEndElement())
						reader.next();

					if (reader.isStartElement() && new javax.xml.namespace.QName("", "zipCode").equals(reader.getName())) {
						nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");

						if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
							throw new org.apache.axis2.databinding.ADBException("The element: " + "zipCode" + "  cannot be null");
						}

						java.lang.String content = reader.getElementText();

						object.setZipCode(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

						reader.next();
					} // End of if for expected property start element

					else {
						// A start element we are not expecting indicates an
						// invalid parameter was passed
						throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
					}

					while (!reader.isStartElement() && !reader.isEndElement())
						reader.next();

					if (reader.isStartElement() && new javax.xml.namespace.QName("", "stateCountyCode").equals(reader.getName())) {
						nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");

						if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
							throw new org.apache.axis2.databinding.ADBException("The element: " + "stateCountyCode" + "  cannot be null");
						}

						java.lang.String content = reader.getElementText();

						object.setStateCountyCode(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

						reader.next();
					} // End of if for expected property start element

					else {
						// A start element we are not expecting indicates an
						// invalid parameter was passed
						throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
					}

					while (!reader.isStartElement() && !reader.isEndElement())
						reader.next();

					if (reader.isStartElement() && new javax.xml.namespace.QName("", "stateCountyDescription").equals(reader.getName())) {
						nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");

						if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
							throw new org.apache.axis2.databinding.ADBException("The element: " + "stateCountyDescription" + "  cannot be null");
						}

						java.lang.String content = reader.getElementText();

						object.setStateCountyDescription(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

						reader.next();
					} // End of if for expected property start element

					else {
						// A start element we are not expecting indicates an
						// invalid parameter was passed
						throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
					}

					while (!reader.isStartElement() && !reader.isEndElement())
						reader.next();

					if (reader.isStartElement() && new javax.xml.namespace.QName("", "federalCountyCode").equals(reader.getName())) {
						nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");

						if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
							throw new org.apache.axis2.databinding.ADBException("The element: " + "federalCountyCode" + "  cannot be null");
						}

						java.lang.String content = reader.getElementText();

						object.setFederalCountyCode(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

						reader.next();
					} // End of if for expected property start element

					else {
						// A start element we are not expecting indicates an
						// invalid parameter was passed
						throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
					}

					while (!reader.isStartElement() && !reader.isEndElement())
						reader.next();

					if (reader.isStartElement() && new javax.xml.namespace.QName("", "countryCode").equals(reader.getName())) {
						nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");

						if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
							throw new org.apache.axis2.databinding.ADBException("The element: " + "countryCode" + "  cannot be null");
						}

						java.lang.String content = reader.getElementText();

						object.setCountryCode(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

						reader.next();
					} // End of if for expected property start element

					else {
						// A start element we are not expecting indicates an
						// invalid parameter was passed
						throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
					}

					while (!reader.isStartElement() && !reader.isEndElement())
						reader.next();

					if (reader.isStartElement()) {
						// A start element we are not expecting indicates a
						// trailing invalid property
						throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
					}
				} catch (javax.xml.stream.XMLStreamException e) {
					throw new java.lang.Exception(e);
				}

				return object;
			}
		} // end of factory class
	}

	public static class MarketingPackage implements org.apache.axis2.databinding.ADBBean {
		/*
		 * This type was generated from the piece of schema that had name = MarketingPackage Namespace URI = http://servicehelpers.esb.avalon.com Namespace
		 * Prefix = ns1
		 */

		/**
		 * field for CoverageMarketingPackageCode
		 */
		protected java.lang.String localCoverageMarketingPackageCode;

		/**
		 * field for CoverageMarketingPackageDesc
		 */
		protected java.lang.String localCoverageMarketingPackageDesc;

		/**
		 * field for SubMarketingPackageCode
		 */
		protected java.lang.String localSubMarketingPackageCode;

		/**
		 * field for SubMarketingPackageDesc
		 */
		protected java.lang.String localSubMarketingPackageDesc;

		/**
		 * Auto generated getter method
		 * 
		 * @return java.lang.String
		 */
		public java.lang.String getCoverageMarketingPackageCode() {
			return localCoverageMarketingPackageCode;
		}

		/**
		 * Auto generated setter method
		 * 
		 * @param param
		 *            CoverageMarketingPackageCode
		 */
		public void setCoverageMarketingPackageCode(java.lang.String param) {
			this.localCoverageMarketingPackageCode = param;
		}

		/**
		 * Auto generated getter method
		 * 
		 * @return java.lang.String
		 */
		public java.lang.String getCoverageMarketingPackageDesc() {
			return localCoverageMarketingPackageDesc;
		}

		/**
		 * Auto generated setter method
		 * 
		 * @param param
		 *            CoverageMarketingPackageDesc
		 */
		public void setCoverageMarketingPackageDesc(java.lang.String param) {
			this.localCoverageMarketingPackageDesc = param;
		}

		/**
		 * Auto generated getter method
		 * 
		 * @return java.lang.String
		 */
		public java.lang.String getSubMarketingPackageCode() {
			return localSubMarketingPackageCode;
		}

		/**
		 * Auto generated setter method
		 * 
		 * @param param
		 *            SubMarketingPackageCode
		 */
		public void setSubMarketingPackageCode(java.lang.String param) {
			this.localSubMarketingPackageCode = param;
		}

		/**
		 * Auto generated getter method
		 * 
		 * @return java.lang.String
		 */
		public java.lang.String getSubMarketingPackageDesc() {
			return localSubMarketingPackageDesc;
		}

		/**
		 * Auto generated setter method
		 * 
		 * @param param
		 *            SubMarketingPackageDesc
		 */
		public void setSubMarketingPackageDesc(java.lang.String param) {
			this.localSubMarketingPackageDesc = param;
		}

		/**
		 *
		 * @param parentQName
		 * @param factory
		 * @return org.apache.axiom.om.OMElement
		 */
		public org.apache.axiom.om.OMElement getOMElement(final javax.xml.namespace.QName parentQName, final org.apache.axiom.om.OMFactory factory)
				throws org.apache.axis2.databinding.ADBException {
			org.apache.axiom.om.OMDataSource dataSource = new org.apache.axis2.databinding.ADBDataSource(this, parentQName);

			return factory.createOMElement(dataSource, parentQName);
		}

		public void serialize(final javax.xml.namespace.QName parentQName, javax.xml.stream.XMLStreamWriter xmlWriter)
				throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {
			serialize(parentQName, xmlWriter, false);
		}

		public void serialize(final javax.xml.namespace.QName parentQName, javax.xml.stream.XMLStreamWriter xmlWriter, boolean serializeType)
				throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {
			java.lang.String prefix = null;
			java.lang.String namespace = null;

			prefix = parentQName.getPrefix();
			namespace = parentQName.getNamespaceURI();
			writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);

			if (serializeType) {
				java.lang.String namespacePrefix = registerPrefix(xmlWriter, "http://servicehelpers.esb.avalon.com");

				if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)) {
					writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", namespacePrefix + ":MarketingPackage", xmlWriter);
				} else {
					writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", "MarketingPackage", xmlWriter);
				}
			}

			namespace = "";
			writeStartElement(null, namespace, "coverageMarketingPackageCode", xmlWriter);

			if (localCoverageMarketingPackageCode == null) {
				// write the nil attribute
				throw new org.apache.axis2.databinding.ADBException("coverageMarketingPackageCode cannot be null!!");
			} else {
				xmlWriter.writeCharacters(localCoverageMarketingPackageCode);
			}

			xmlWriter.writeEndElement();

			namespace = "";
			writeStartElement(null, namespace, "coverageMarketingPackageDesc", xmlWriter);

			if (localCoverageMarketingPackageDesc == null) {
				// write the nil attribute
				throw new org.apache.axis2.databinding.ADBException("coverageMarketingPackageDesc cannot be null!!");
			} else {
				xmlWriter.writeCharacters(localCoverageMarketingPackageDesc);
			}

			xmlWriter.writeEndElement();

			namespace = "";
			writeStartElement(null, namespace, "subMarketingPackageCode", xmlWriter);

			if (localSubMarketingPackageCode == null) {
				// write the nil attribute
				throw new org.apache.axis2.databinding.ADBException("subMarketingPackageCode cannot be null!!");
			} else {
				xmlWriter.writeCharacters(localSubMarketingPackageCode);
			}

			xmlWriter.writeEndElement();

			namespace = "";
			writeStartElement(null, namespace, "subMarketingPackageDesc", xmlWriter);

			if (localSubMarketingPackageDesc == null) {
				// write the nil attribute
				throw new org.apache.axis2.databinding.ADBException("subMarketingPackageDesc cannot be null!!");
			} else {
				xmlWriter.writeCharacters(localSubMarketingPackageDesc);
			}

			xmlWriter.writeEndElement();

			xmlWriter.writeEndElement();
		}

		private static java.lang.String generatePrefix(java.lang.String namespace) {
			if (namespace.equals("http://servicehelpers.esb.avalon.com")) {
				return "ns1";
			}

			return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
		}

		/**
		 * Utility method to write an element start tag.
		 */
		private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
				javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
			java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

			if (writerPrefix != null) {
				xmlWriter.writeStartElement(namespace, localPart);
			} else {
				if (namespace.length() == 0) {
					prefix = "";
				} else if (prefix == null) {
					prefix = generatePrefix(namespace);
				}

				xmlWriter.writeStartElement(prefix, localPart, namespace);
				xmlWriter.writeNamespace(prefix, namespace);
				xmlWriter.setPrefix(prefix, namespace);
			}
		}

		/**
		 * Util method to write an attribute with the ns prefix
		 */
		private void writeAttribute(java.lang.String prefix, java.lang.String namespace, java.lang.String attName, java.lang.String attValue,
				javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
			if (xmlWriter.getPrefix(namespace) == null) {
				xmlWriter.writeNamespace(prefix, namespace);
				xmlWriter.setPrefix(prefix, namespace);
			}

			xmlWriter.writeAttribute(namespace, attName, attValue);
		}

		/**
		 * Util method to write an attribute without the ns prefix
		 */
		private void writeAttribute(java.lang.String namespace, java.lang.String attName, java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter)
				throws javax.xml.stream.XMLStreamException {
			if (namespace.equals("")) {
				xmlWriter.writeAttribute(attName, attValue);
			} else {
				registerPrefix(xmlWriter, namespace);
				xmlWriter.writeAttribute(namespace, attName, attValue);
			}
		}

		/**
		 * Util method to write an attribute without the ns prefix
		 */
		private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName, javax.xml.namespace.QName qname,
				javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
			java.lang.String attributeNamespace = qname.getNamespaceURI();
			java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);

			if (attributePrefix == null) {
				attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
			}

			java.lang.String attributeValue;

			if (attributePrefix.trim().length() > 0) {
				attributeValue = attributePrefix + ":" + qname.getLocalPart();
			} else {
				attributeValue = qname.getLocalPart();
			}

			if (namespace.equals("")) {
				xmlWriter.writeAttribute(attName, attributeValue);
			} else {
				registerPrefix(xmlWriter, namespace);
				xmlWriter.writeAttribute(namespace, attName, attributeValue);
			}
		}

		/**
		 * method to handle Qnames
		 */
		private void writeQName(javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
			java.lang.String namespaceURI = qname.getNamespaceURI();

			if (namespaceURI != null) {
				java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);

				if (prefix == null) {
					prefix = generatePrefix(namespaceURI);
					xmlWriter.writeNamespace(prefix, namespaceURI);
					xmlWriter.setPrefix(prefix, namespaceURI);
				}

				if (prefix.trim().length() > 0) {
					xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
				} else {
					// i.e this is the default namespace
					xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
				}
			} else {
				xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
			}
		}

		private void writeQNames(javax.xml.namespace.QName[] qnames, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
			if (qnames != null) {
				// we have to store this data until last moment since it is not
				// possible to write any
				// namespace data after writing the charactor data
				java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
				java.lang.String namespaceURI = null;
				java.lang.String prefix = null;

				for (int i = 0; i < qnames.length; i++) {
					if (i > 0) {
						stringToWrite.append(" ");
					}

					namespaceURI = qnames[i].getNamespaceURI();

					if (namespaceURI != null) {
						prefix = xmlWriter.getPrefix(namespaceURI);

						if ((prefix == null) || (prefix.length() == 0)) {
							prefix = generatePrefix(namespaceURI);
							xmlWriter.writeNamespace(prefix, namespaceURI);
							xmlWriter.setPrefix(prefix, namespaceURI);
						}

						if (prefix.trim().length() > 0) {
							stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
						} else {
							stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
						}
					} else {
						stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
					}
				}

				xmlWriter.writeCharacters(stringToWrite.toString());
			}
		}

		/**
		 * Register a namespace prefix
		 */
		private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace)
				throws javax.xml.stream.XMLStreamException {
			java.lang.String prefix = xmlWriter.getPrefix(namespace);

			if (prefix == null) {
				prefix = generatePrefix(namespace);

				javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();

				while (true) {
					java.lang.String uri = nsContext.getNamespaceURI(prefix);

					if ((uri == null) || (uri.length() == 0)) {
						break;
					}

					prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
				}

				xmlWriter.writeNamespace(prefix, namespace);
				xmlWriter.setPrefix(prefix, namespace);
			}

			return prefix;
		}

		/**
		 * databinding method to get an XML representation of this object
		 *
		 */
		public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName) throws org.apache.axis2.databinding.ADBException {
			java.util.ArrayList elementList = new java.util.ArrayList();
			java.util.ArrayList attribList = new java.util.ArrayList();

			elementList.add(new javax.xml.namespace.QName("", "coverageMarketingPackageCode"));

			if (localCoverageMarketingPackageCode != null) {
				elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCoverageMarketingPackageCode));
			} else {
				throw new org.apache.axis2.databinding.ADBException("coverageMarketingPackageCode cannot be null!!");
			}

			elementList.add(new javax.xml.namespace.QName("", "coverageMarketingPackageDesc"));

			if (localCoverageMarketingPackageDesc != null) {
				elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCoverageMarketingPackageDesc));
			} else {
				throw new org.apache.axis2.databinding.ADBException("coverageMarketingPackageDesc cannot be null!!");
			}

			elementList.add(new javax.xml.namespace.QName("", "subMarketingPackageCode"));

			if (localSubMarketingPackageCode != null) {
				elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSubMarketingPackageCode));
			} else {
				throw new org.apache.axis2.databinding.ADBException("subMarketingPackageCode cannot be null!!");
			}

			elementList.add(new javax.xml.namespace.QName("", "subMarketingPackageDesc"));

			if (localSubMarketingPackageDesc != null) {
				elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSubMarketingPackageDesc));
			} else {
				throw new org.apache.axis2.databinding.ADBException("subMarketingPackageDesc cannot be null!!");
			}

			return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
		}

		/**
		 * Factory class that keeps the parse method
		 */
		public static class Factory {
			/**
			 * static method to create the object Precondition: If this object is an element, the current or next start element starts this object and any
			 * intervening reader events are ignorable If this object is not an element, it is a complex type and the reader is at the event just after the
			 * outer start element Postcondition: If this object is an element, the reader is positioned at its end element If this object is a complex type,
			 * the reader is positioned at the end element of its outer element
			 */
			public static MarketingPackage parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception {
				MarketingPackage object = new MarketingPackage();

				int event;
				java.lang.String nillableValue = null;
				java.lang.String prefix = "";
				java.lang.String namespaceuri = "";

				try {
					while (!reader.isStartElement() && !reader.isEndElement())
						reader.next();

					if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
						java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type");

						if (fullTypeName != null) {
							java.lang.String nsPrefix = null;

							if (fullTypeName.indexOf(":") > -1) {
								nsPrefix = fullTypeName.substring(0, fullTypeName.indexOf(":"));
							}

							nsPrefix = (nsPrefix == null) ? "" : nsPrefix;

							java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":") + 1);

							if (!"MarketingPackage".equals(type)) {
								// find namespace for the prefix
								java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);

								return (MarketingPackage) ExtensionMapper.getTypeObject(nsUri, type, reader);
							}
						}
					}

					// Note all attributes that were handled. Used to differ
					// normal attributes
					// from anyAttributes.
					java.util.Vector handledAttributes = new java.util.Vector();

					reader.next();

					while (!reader.isStartElement() && !reader.isEndElement())
						reader.next();

					if (reader.isStartElement() && new javax.xml.namespace.QName("", "coverageMarketingPackageCode").equals(reader.getName())) {
						nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");

						if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
							throw new org.apache.axis2.databinding.ADBException("The element: " + "coverageMarketingPackageCode" + "  cannot be null");
						}

						java.lang.String content = reader.getElementText();

						object.setCoverageMarketingPackageCode(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

						reader.next();
					} // End of if for expected property start element

					else {
						// A start element we are not expecting indicates an
						// invalid parameter was passed
						throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
					}

					while (!reader.isStartElement() && !reader.isEndElement())
						reader.next();

					if (reader.isStartElement() && new javax.xml.namespace.QName("", "coverageMarketingPackageDesc").equals(reader.getName())) {
						nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");

						if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
							throw new org.apache.axis2.databinding.ADBException("The element: " + "coverageMarketingPackageDesc" + "  cannot be null");
						}

						java.lang.String content = reader.getElementText();

						object.setCoverageMarketingPackageDesc(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

						reader.next();
					} // End of if for expected property start element

					else {
						// A start element we are not expecting indicates an
						// invalid parameter was passed
						throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
					}

					while (!reader.isStartElement() && !reader.isEndElement())
						reader.next();

					if (reader.isStartElement() && new javax.xml.namespace.QName("", "subMarketingPackageCode").equals(reader.getName())) {
						nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");

						if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
							throw new org.apache.axis2.databinding.ADBException("The element: " + "subMarketingPackageCode" + "  cannot be null");
						}

						java.lang.String content = reader.getElementText();

						object.setSubMarketingPackageCode(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

						reader.next();
					} // End of if for expected property start element

					else {
						// A start element we are not expecting indicates an
						// invalid parameter was passed
						throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
					}

					while (!reader.isStartElement() && !reader.isEndElement())
						reader.next();

					if (reader.isStartElement() && new javax.xml.namespace.QName("", "subMarketingPackageDesc").equals(reader.getName())) {
						nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");

						if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
							throw new org.apache.axis2.databinding.ADBException("The element: " + "subMarketingPackageDesc" + "  cannot be null");
						}

						java.lang.String content = reader.getElementText();

						object.setSubMarketingPackageDesc(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

						reader.next();
					} // End of if for expected property start element

					else {
						// A start element we are not expecting indicates an
						// invalid parameter was passed
						throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
					}

					while (!reader.isStartElement() && !reader.isEndElement())
						reader.next();

					if (reader.isStartElement()) {
						// A start element we are not expecting indicates a
						// trailing invalid property
						throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
					}
				} catch (javax.xml.stream.XMLStreamException e) {
					throw new java.lang.Exception(e);
				}

				return object;
			}
		} // end of factory class
	}

	public static class MemberDataRequest implements org.apache.axis2.databinding.ADBBean {
		/*
		 * This type was generated from the piece of schema that had name = MemberDataRequest Namespace URI = http://servicehelpers.esb.avalon.com Namespace
		 * Prefix = ns1
		 */

		/**
		 * field for SearchCriteria
		 */
		protected SearchCriteria localSearchCriteria;

		/*
		 * This tracker boolean wil be used to detect whether the user called the set method for this attribute. It will be used to determine whether to include
		 * this field in the serialized XML
		 */
		protected boolean localSearchCriteriaTracker = false;

		/**
		 * field for MemberDemographics
		 */
		protected MemberDemographics localMemberDemographics;

		/*
		 * This tracker boolean wil be used to detect whether the user called the set method for this attribute. It will be used to determine whether to include
		 * this field in the serialized XML
		 */
		protected boolean localMemberDemographicsTracker = false;

		/**
		 * field for MoreDataOptions
		 */
		protected MoreDataOptions localMoreDataOptions;

		/*
		 * This tracker boolean wil be used to detect whether the user called the set method for this attribute. It will be used to determine whether to include
		 * this field in the serialized XML
		 */
		protected boolean localMoreDataOptionsTracker = false;

		public boolean isSearchCriteriaSpecified() {
			return localSearchCriteriaTracker;
		}

		/**
		 * Auto generated getter method
		 * 
		 * @return SearchCriteria
		 */
		public SearchCriteria getSearchCriteria() {
			return localSearchCriteria;
		}

		/**
		 * Auto generated setter method
		 * 
		 * @param param
		 *            SearchCriteria
		 */
		public void setSearchCriteria(SearchCriteria param) {
			localSearchCriteriaTracker = param != null;

			this.localSearchCriteria = param;
		}

		public boolean isMemberDemographicsSpecified() {
			return localMemberDemographicsTracker;
		}

		/**
		 * Auto generated getter method
		 * 
		 * @return MemberDemographics
		 */
		public MemberDemographics getMemberDemographics() {
			return localMemberDemographics;
		}

		/**
		 * Auto generated setter method
		 * 
		 * @param param
		 *            MemberDemographics
		 */
		public void setMemberDemographics(MemberDemographics param) {
			localMemberDemographicsTracker = param != null;

			this.localMemberDemographics = param;
		}

		public boolean isMoreDataOptionsSpecified() {
			return localMoreDataOptionsTracker;
		}

		/**
		 * Auto generated getter method
		 * 
		 * @return MoreDataOptions
		 */
		public MoreDataOptions getMoreDataOptions() {
			return localMoreDataOptions;
		}

		/**
		 * Auto generated setter method
		 * 
		 * @param param
		 *            MoreDataOptions
		 */
		public void setMoreDataOptions(MoreDataOptions param) {
			localMoreDataOptionsTracker = param != null;

			this.localMoreDataOptions = param;
		}

		/**
		 *
		 * @param parentQName
		 * @param factory
		 * @return org.apache.axiom.om.OMElement
		 */
		public org.apache.axiom.om.OMElement getOMElement(final javax.xml.namespace.QName parentQName, final org.apache.axiom.om.OMFactory factory)
				throws org.apache.axis2.databinding.ADBException {
			org.apache.axiom.om.OMDataSource dataSource = new org.apache.axis2.databinding.ADBDataSource(this, parentQName);

			return factory.createOMElement(dataSource, parentQName);
		}

		public void serialize(final javax.xml.namespace.QName parentQName, javax.xml.stream.XMLStreamWriter xmlWriter)
				throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {
			serialize(parentQName, xmlWriter, false);
		}

		public void serialize(final javax.xml.namespace.QName parentQName, javax.xml.stream.XMLStreamWriter xmlWriter, boolean serializeType)
				throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {
			java.lang.String prefix = null;
			java.lang.String namespace = null;

			prefix = parentQName.getPrefix();
			namespace = parentQName.getNamespaceURI();
			writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);

			if (serializeType) {
				java.lang.String namespacePrefix = registerPrefix(xmlWriter, "http://servicehelpers.esb.avalon.com");

				if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)) {
					writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", namespacePrefix + ":MemberDataRequest", xmlWriter);
				} else {
					writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", "MemberDataRequest", xmlWriter);
				}
			}

			if (localSearchCriteriaTracker) {
				if (localSearchCriteria == null) {
					throw new org.apache.axis2.databinding.ADBException("searchCriteria cannot be null!!");
				}

				localSearchCriteria.serialize(new javax.xml.namespace.QName("", "searchCriteria"), xmlWriter);
			}

			if (localMemberDemographicsTracker) {
				if (localMemberDemographics == null) {
					throw new org.apache.axis2.databinding.ADBException("memberDemographics cannot be null!!");
				}

				localMemberDemographics.serialize(new javax.xml.namespace.QName("", "memberDemographics"), xmlWriter);
			}

			if (localMoreDataOptionsTracker) {
				if (localMoreDataOptions == null) {
					throw new org.apache.axis2.databinding.ADBException("moreDataOptions cannot be null!!");
				}

				localMoreDataOptions.serialize(new javax.xml.namespace.QName("", "moreDataOptions"), xmlWriter);
			}

			xmlWriter.writeEndElement();
		}

		private static java.lang.String generatePrefix(java.lang.String namespace) {
			if (namespace.equals("http://servicehelpers.esb.avalon.com")) {
				return "ns1";
			}

			return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
		}

		/**
		 * Utility method to write an element start tag.
		 */
		private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
				javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
			java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

			if (writerPrefix != null) {
				xmlWriter.writeStartElement(namespace, localPart);
			} else {
				if (namespace.length() == 0) {
					prefix = "";
				} else if (prefix == null) {
					prefix = generatePrefix(namespace);
				}

				xmlWriter.writeStartElement(prefix, localPart, namespace);
				xmlWriter.writeNamespace(prefix, namespace);
				xmlWriter.setPrefix(prefix, namespace);
			}
		}

		/**
		 * Util method to write an attribute with the ns prefix
		 */
		private void writeAttribute(java.lang.String prefix, java.lang.String namespace, java.lang.String attName, java.lang.String attValue,
				javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
			if (xmlWriter.getPrefix(namespace) == null) {
				xmlWriter.writeNamespace(prefix, namespace);
				xmlWriter.setPrefix(prefix, namespace);
			}

			xmlWriter.writeAttribute(namespace, attName, attValue);
		}

		/**
		 * Util method to write an attribute without the ns prefix
		 */
		private void writeAttribute(java.lang.String namespace, java.lang.String attName, java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter)
				throws javax.xml.stream.XMLStreamException {
			if (namespace.equals("")) {
				xmlWriter.writeAttribute(attName, attValue);
			} else {
				registerPrefix(xmlWriter, namespace);
				xmlWriter.writeAttribute(namespace, attName, attValue);
			}
		}

		/**
		 * Util method to write an attribute without the ns prefix
		 */
		private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName, javax.xml.namespace.QName qname,
				javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
			java.lang.String attributeNamespace = qname.getNamespaceURI();
			java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);

			if (attributePrefix == null) {
				attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
			}

			java.lang.String attributeValue;

			if (attributePrefix.trim().length() > 0) {
				attributeValue = attributePrefix + ":" + qname.getLocalPart();
			} else {
				attributeValue = qname.getLocalPart();
			}

			if (namespace.equals("")) {
				xmlWriter.writeAttribute(attName, attributeValue);
			} else {
				registerPrefix(xmlWriter, namespace);
				xmlWriter.writeAttribute(namespace, attName, attributeValue);
			}
		}

		/**
		 * method to handle Qnames
		 */
		private void writeQName(javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
			java.lang.String namespaceURI = qname.getNamespaceURI();

			if (namespaceURI != null) {
				java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);

				if (prefix == null) {
					prefix = generatePrefix(namespaceURI);
					xmlWriter.writeNamespace(prefix, namespaceURI);
					xmlWriter.setPrefix(prefix, namespaceURI);
				}

				if (prefix.trim().length() > 0) {
					xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
				} else {
					// i.e this is the default namespace
					xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
				}
			} else {
				xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
			}
		}

		private void writeQNames(javax.xml.namespace.QName[] qnames, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
			if (qnames != null) {
				// we have to store this data until last moment since it is not
				// possible to write any
				// namespace data after writing the charactor data
				java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
				java.lang.String namespaceURI = null;
				java.lang.String prefix = null;

				for (int i = 0; i < qnames.length; i++) {
					if (i > 0) {
						stringToWrite.append(" ");
					}

					namespaceURI = qnames[i].getNamespaceURI();

					if (namespaceURI != null) {
						prefix = xmlWriter.getPrefix(namespaceURI);

						if ((prefix == null) || (prefix.length() == 0)) {
							prefix = generatePrefix(namespaceURI);
							xmlWriter.writeNamespace(prefix, namespaceURI);
							xmlWriter.setPrefix(prefix, namespaceURI);
						}

						if (prefix.trim().length() > 0) {
							stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
						} else {
							stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
						}
					} else {
						stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
					}
				}

				xmlWriter.writeCharacters(stringToWrite.toString());
			}
		}

		/**
		 * Register a namespace prefix
		 */
		private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace)
				throws javax.xml.stream.XMLStreamException {
			java.lang.String prefix = xmlWriter.getPrefix(namespace);

			if (prefix == null) {
				prefix = generatePrefix(namespace);

				javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();

				while (true) {
					java.lang.String uri = nsContext.getNamespaceURI(prefix);

					if ((uri == null) || (uri.length() == 0)) {
						break;
					}

					prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
				}

				xmlWriter.writeNamespace(prefix, namespace);
				xmlWriter.setPrefix(prefix, namespace);
			}

			return prefix;
		}

		/**
		 * databinding method to get an XML representation of this object
		 *
		 */
		public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName) throws org.apache.axis2.databinding.ADBException {
			java.util.ArrayList elementList = new java.util.ArrayList();
			java.util.ArrayList attribList = new java.util.ArrayList();

			if (localSearchCriteriaTracker) {
				elementList.add(new javax.xml.namespace.QName("", "searchCriteria"));

				if (localSearchCriteria == null) {
					throw new org.apache.axis2.databinding.ADBException("searchCriteria cannot be null!!");
				}

				elementList.add(localSearchCriteria);
			}

			if (localMemberDemographicsTracker) {
				elementList.add(new javax.xml.namespace.QName("", "memberDemographics"));

				if (localMemberDemographics == null) {
					throw new org.apache.axis2.databinding.ADBException("memberDemographics cannot be null!!");
				}

				elementList.add(localMemberDemographics);
			}

			if (localMoreDataOptionsTracker) {
				elementList.add(new javax.xml.namespace.QName("", "moreDataOptions"));

				if (localMoreDataOptions == null) {
					throw new org.apache.axis2.databinding.ADBException("moreDataOptions cannot be null!!");
				}

				elementList.add(localMoreDataOptions);
			}

			return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
		}

		/**
		 * Factory class that keeps the parse method
		 */
		public static class Factory {
			/**
			 * static method to create the object Precondition: If this object is an element, the current or next start element starts this object and any
			 * intervening reader events are ignorable If this object is not an element, it is a complex type and the reader is at the event just after the
			 * outer start element Postcondition: If this object is an element, the reader is positioned at its end element If this object is a complex type,
			 * the reader is positioned at the end element of its outer element
			 */
			public static MemberDataRequest parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception {

				MemberDataRequest object = new MemberDataRequest();

				int event;
				java.lang.String nillableValue = null;
				java.lang.String prefix = "";
				java.lang.String namespaceuri = "";

				try {
					while (!reader.isStartElement() && !reader.isEndElement())
						reader.next();

					if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
						java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type");

						if (fullTypeName != null) {
							java.lang.String nsPrefix = null;

							if (fullTypeName.indexOf(":") > -1) {
								nsPrefix = fullTypeName.substring(0, fullTypeName.indexOf(":"));
							}

							nsPrefix = (nsPrefix == null) ? "" : nsPrefix;

							java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":") + 1);

							if (!"MemberDataRequest".equals(type)) {
								// find namespace for the prefix
								java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);

								return (MemberDataRequest) ExtensionMapper.getTypeObject(nsUri, type, reader);
							}
						}
					}

					// Note all attributes that were handled. Used to differ
					// normal attributes
					// from anyAttributes.
					java.util.Vector handledAttributes = new java.util.Vector();

					reader.next();

					while (!reader.isStartElement() && !reader.isEndElement())
						reader.next();

					if (reader.isStartElement() && new javax.xml.namespace.QName("", "searchCriteria").equals(reader.getName())) {
						object.setSearchCriteria(SearchCriteria.Factory.parse(reader));

						reader.next();
					} // End of if for expected property start element

					else {
					}

					while (!reader.isStartElement() && !reader.isEndElement())
						reader.next();

					if (reader.isStartElement() && new javax.xml.namespace.QName("", "memberDemographics").equals(reader.getName())) {
						object.setMemberDemographics(MemberDemographics.Factory.parse(reader));

						reader.next();
					} // End of if for expected property start element

					else {
					}

					while (!reader.isStartElement() && !reader.isEndElement())
						reader.next();

					if (reader.isStartElement() && new javax.xml.namespace.QName("", "moreDataOptions").equals(reader.getName())) {
						object.setMoreDataOptions(MoreDataOptions.Factory.parse(reader));

						reader.next();
					} // End of if for expected property start element

					else {
					}

					while (!reader.isStartElement() && !reader.isEndElement())
						reader.next();

					if (reader.isStartElement()) {
						// A start element we are not expecting indicates a
						// trailing invalid property
						throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
					}
				} catch (javax.xml.stream.XMLStreamException e) {
					throw new java.lang.Exception(e);
				}

				return object;
			}
		} // end of factory class
	}

	public static class GetMemberDataResponse implements org.apache.axis2.databinding.ADBBean {
		public static final javax.xml.namespace.QName MY_QNAME = new javax.xml.namespace.QName("http://servicehelpers.esb.avalon.com", "getMemberDataResponse",
				"ns1");

		/**
		 * field for GetMemberDataResponse
		 */
		protected MemberDataResponse localGetMemberDataResponse;

		/**
		 * Auto generated getter method
		 * 
		 * @return MemberDataResponse
		 */
		public MemberDataResponse getGetMemberDataResponse() {
			return localGetMemberDataResponse;
		}

		/**
		 * Auto generated setter method
		 * 
		 * @param param
		 *            GetMemberDataResponse
		 */
		public void setGetMemberDataResponse(MemberDataResponse param) {
			this.localGetMemberDataResponse = param;
		}

		/**
		 *
		 * @param parentQName
		 * @param factory
		 * @return org.apache.axiom.om.OMElement
		 */
		public org.apache.axiom.om.OMElement getOMElement(final javax.xml.namespace.QName parentQName, final org.apache.axiom.om.OMFactory factory)
				throws org.apache.axis2.databinding.ADBException {
			org.apache.axiom.om.OMDataSource dataSource = new org.apache.axis2.databinding.ADBDataSource(this, MY_QNAME);

			return factory.createOMElement(dataSource, MY_QNAME);
		}

		public void serialize(final javax.xml.namespace.QName parentQName, javax.xml.stream.XMLStreamWriter xmlWriter)
				throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {
			serialize(parentQName, xmlWriter, false);
		}

		public void serialize(final javax.xml.namespace.QName parentQName, javax.xml.stream.XMLStreamWriter xmlWriter, boolean serializeType)
				throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {
			// We can safely assume an element has only one type associated with
			// it
			if (localGetMemberDataResponse == null) {
				throw new org.apache.axis2.databinding.ADBException("getMemberDataResponse cannot be null!");
			}

			localGetMemberDataResponse.serialize(MY_QNAME, xmlWriter);
		}

		private static java.lang.String generatePrefix(java.lang.String namespace) {
			if (namespace.equals("http://servicehelpers.esb.avalon.com")) {
				return "ns1";
			}

			return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
		}

		/**
		 * Utility method to write an element start tag.
		 */
		private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
				javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
			java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

			if (writerPrefix != null) {
				xmlWriter.writeStartElement(namespace, localPart);
			} else {
				if (namespace.length() == 0) {
					prefix = "";
				} else if (prefix == null) {
					prefix = generatePrefix(namespace);
				}

				xmlWriter.writeStartElement(prefix, localPart, namespace);
				xmlWriter.writeNamespace(prefix, namespace);
				xmlWriter.setPrefix(prefix, namespace);
			}
		}

		/**
		 * Util method to write an attribute with the ns prefix
		 */
		private void writeAttribute(java.lang.String prefix, java.lang.String namespace, java.lang.String attName, java.lang.String attValue,
				javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
			if (xmlWriter.getPrefix(namespace) == null) {
				xmlWriter.writeNamespace(prefix, namespace);
				xmlWriter.setPrefix(prefix, namespace);
			}

			xmlWriter.writeAttribute(namespace, attName, attValue);
		}

		/**
		 * Util method to write an attribute without the ns prefix
		 */
		private void writeAttribute(java.lang.String namespace, java.lang.String attName, java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter)
				throws javax.xml.stream.XMLStreamException {
			if (namespace.equals("")) {
				xmlWriter.writeAttribute(attName, attValue);
			} else {
				registerPrefix(xmlWriter, namespace);
				xmlWriter.writeAttribute(namespace, attName, attValue);
			}
		}

		/**
		 * Util method to write an attribute without the ns prefix
		 */
		private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName, javax.xml.namespace.QName qname,
				javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
			java.lang.String attributeNamespace = qname.getNamespaceURI();
			java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);

			if (attributePrefix == null) {
				attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
			}

			java.lang.String attributeValue;

			if (attributePrefix.trim().length() > 0) {
				attributeValue = attributePrefix + ":" + qname.getLocalPart();
			} else {
				attributeValue = qname.getLocalPart();
			}

			if (namespace.equals("")) {
				xmlWriter.writeAttribute(attName, attributeValue);
			} else {
				registerPrefix(xmlWriter, namespace);
				xmlWriter.writeAttribute(namespace, attName, attributeValue);
			}
		}

		/**
		 * method to handle Qnames
		 */
		private void writeQName(javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
			java.lang.String namespaceURI = qname.getNamespaceURI();

			if (namespaceURI != null) {
				java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);

				if (prefix == null) {
					prefix = generatePrefix(namespaceURI);
					xmlWriter.writeNamespace(prefix, namespaceURI);
					xmlWriter.setPrefix(prefix, namespaceURI);
				}

				if (prefix.trim().length() > 0) {
					xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
				} else {
					// i.e this is the default namespace
					xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
				}
			} else {
				xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
			}
		}

		private void writeQNames(javax.xml.namespace.QName[] qnames, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
			if (qnames != null) {
				// we have to store this data until last moment since it is not
				// possible to write any
				// namespace data after writing the charactor data
				java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
				java.lang.String namespaceURI = null;
				java.lang.String prefix = null;

				for (int i = 0; i < qnames.length; i++) {
					if (i > 0) {
						stringToWrite.append(" ");
					}

					namespaceURI = qnames[i].getNamespaceURI();

					if (namespaceURI != null) {
						prefix = xmlWriter.getPrefix(namespaceURI);

						if ((prefix == null) || (prefix.length() == 0)) {
							prefix = generatePrefix(namespaceURI);
							xmlWriter.writeNamespace(prefix, namespaceURI);
							xmlWriter.setPrefix(prefix, namespaceURI);
						}

						if (prefix.trim().length() > 0) {
							stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
						} else {
							stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
						}
					} else {
						stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
					}
				}

				xmlWriter.writeCharacters(stringToWrite.toString());
			}
		}

		/**
		 * Register a namespace prefix
		 */
		private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace)
				throws javax.xml.stream.XMLStreamException {
			java.lang.String prefix = xmlWriter.getPrefix(namespace);

			if (prefix == null) {
				prefix = generatePrefix(namespace);

				javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();

				while (true) {
					java.lang.String uri = nsContext.getNamespaceURI(prefix);

					if ((uri == null) || (uri.length() == 0)) {
						break;
					}

					prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
				}

				xmlWriter.writeNamespace(prefix, namespace);
				xmlWriter.setPrefix(prefix, namespace);
			}

			return prefix;
		}

		/**
		 * databinding method to get an XML representation of this object
		 *
		 */
		public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName) throws org.apache.axis2.databinding.ADBException {
			// We can safely assume an element has only one type associated with
			// it
			return localGetMemberDataResponse.getPullParser(MY_QNAME);
		}

		/**
		 * Factory class that keeps the parse method
		 */
		public static class Factory {
			/**
			 * static method to create the object Precondition: If this object is an element, the current or next start element starts this object and any
			 * intervening reader events are ignorable If this object is not an element, it is a complex type and the reader is at the event just after the
			 * outer start element Postcondition: If this object is an element, the reader is positioned at its end element If this object is a complex type,
			 * the reader is positioned at the end element of its outer element
			 */
			public static GetMemberDataResponse parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception {
				GetMemberDataResponse object = new GetMemberDataResponse();

				int event;
				java.lang.String nillableValue = null;
				java.lang.String prefix = "";
				java.lang.String namespaceuri = "";

				try {
					while (!reader.isStartElement() && !reader.isEndElement())
						reader.next();

					// Note all attributes that were handled. Used to differ
					// normal attributes
					// from anyAttributes.
					java.util.Vector handledAttributes = new java.util.Vector();

					while (!reader.isEndElement()) {
						if (reader.isStartElement()) {
							if (reader.isStartElement()) { // changed code
								object.setGetMemberDataResponse(MemberDataResponse.Factory.parse(reader));
							} // End of if for expected property start element

							else {
								// A start element we are not expecting
								// indicates an invalid parameter was passed
								throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
							}
						} else {
							reader.next();
						}
					} // end of while loop
				} catch (javax.xml.stream.XMLStreamException e) {
					throw new java.lang.Exception(e);
				}

				return object;
			}
		} // end of factory class
	}

	public static class MemberDataResponse implements org.apache.axis2.databinding.ADBBean {
		/*
		 * This type was generated from the piece of schema that had name = memberDataResponse Namespace URI = http://servicehelpers.esb.avalon.com Namespace
		 * Prefix = ns1
		 */

		/**
		 * field for ResponseCode This was an Array!
		 */
		protected java.lang.String[] localResponseCode;

		/*
		 * This tracker boolean wil be used to detect whether the user called the set method for this attribute. It will be used to determine whether to include
		 * this field in the serialized XML
		 */
		protected boolean localResponseCodeTracker = false;

		/**
		 * field for HealthPlanGroupId
		 */
		protected java.lang.String localHealthPlanGroupId;

		/**
		 * field for LabBenefitId
		 */
		protected java.lang.String localLabBenefitId;

		/**
		 * field for LabBenefitIdDescription
		 */
		protected java.lang.String localLabBenefitIdDescription;

		/**
		 * field for SubscriberNumber
		 */
		protected java.lang.String localSubscriberNumber;

		/**
		 * field for BusinessSectorSegment
		 */
		protected BusinessSectorSegment localBusinessSectorSegment;

		/**
		 * field for MemberDemographicsOutput
		 */
		protected MemberDemographicsOutput localMemberDemographicsOutput;

		/**
		 * field for Address
		 */
		protected Address localAddress;

		/**
		 * field for ContactData
		 */
		protected ContactData localContactData;

		/**
		 * field for EnrollmentData
		 */
		protected EnrollmentData localEnrollmentData;

		/**
		 * field for MemberList
		 */
		protected MemberList localMemberList;

		/**
		 * field for CoverageList
		 */
		protected CoverageList localCoverageList;

		public boolean isResponseCodeSpecified() {
			return localResponseCodeTracker;
		}

		/**
		 * Auto generated getter method
		 * 
		 * @return java.lang.String[]
		 */
		public java.lang.String[] getResponseCode() {
			return localResponseCode;
		}

		/**
		 * validate the array for ResponseCode
		 */
		protected void validateResponseCode(java.lang.String[] param) {
			if ((param != null) && (param.length > 8)) {
				throw new java.lang.RuntimeException("Input values do not follow defined XSD restrictions");
			}
		}

		/**
		 * Auto generated setter method
		 * 
		 * @param param
		 *            ResponseCode
		 */
		public void setResponseCode(java.lang.String[] param) {
			validateResponseCode(param);

			localResponseCodeTracker = param != null;

			this.localResponseCode = param;
		}

		/**
		 * Auto generated add method for the array for convenience
		 * 
		 * @param param
		 *            java.lang.String
		 */
		public void addResponseCode(java.lang.String param) {
			if (localResponseCode == null) {
				localResponseCode = new java.lang.String[] {};
			}

			// update the setting tracker
			localResponseCodeTracker = true;

			java.util.List list = org.apache.axis2.databinding.utils.ConverterUtil.toList(localResponseCode);
			list.add(param);
			this.localResponseCode = (java.lang.String[]) list.toArray(new java.lang.String[list.size()]);
		}

		/**
		 * Auto generated getter method
		 * 
		 * @return java.lang.String
		 */
		public java.lang.String getHealthPlanGroupId() {
			return localHealthPlanGroupId;
		}

		/**
		 * Auto generated setter method
		 * 
		 * @param param
		 *            HealthPlanGroupId
		 */
		public void setHealthPlanGroupId(java.lang.String param) {
			this.localHealthPlanGroupId = param;
		}

		/**
		 * Auto generated getter method
		 * 
		 * @return java.lang.String
		 */
		public java.lang.String getLabBenefitId() {
			return localLabBenefitId;
		}

		/**
		 * Auto generated setter method
		 * 
		 * @param param
		 *            LabBenefitId
		 */
		public void setLabBenefitId(java.lang.String param) {
			this.localLabBenefitId = param;
		}

		/**
		 * Auto generated getter method
		 * 
		 * @return java.lang.String
		 */
		public java.lang.String getLabBenefitIdDescription() {
			return localLabBenefitIdDescription;
		}

		/**
		 * Auto generated setter method
		 * 
		 * @param param
		 *            LabBenefitIdDescription
		 */
		public void setLabBenefitIdDescription(java.lang.String param) {
			this.localLabBenefitIdDescription = param;
		}

		/**
		 * Auto generated getter method
		 * 
		 * @return java.lang.String
		 */
		public java.lang.String getSubscriberNumber() {
			return localSubscriberNumber;
		}

		/**
		 * Auto generated setter method
		 * 
		 * @param param
		 *            SubscriberNumber
		 */
		public void setSubscriberNumber(java.lang.String param) {
			this.localSubscriberNumber = param;
		}

		/**
		 * Auto generated getter method
		 * 
		 * @return BusinessSectorSegment
		 */
		public BusinessSectorSegment getBusinessSectorSegment() {
			return localBusinessSectorSegment;
		}

		/**
		 * Auto generated setter method
		 * 
		 * @param param
		 *            BusinessSectorSegment
		 */
		public void setBusinessSectorSegment(BusinessSectorSegment param) {
			this.localBusinessSectorSegment = param;
		}

		/**
		 * Auto generated getter method
		 * 
		 * @return MemberDemographicsOutput
		 */
		public MemberDemographicsOutput getMemberDemographicsOutput() {
			return localMemberDemographicsOutput;
		}

		/**
		 * Auto generated setter method
		 * 
		 * @param param
		 *            MemberDemographicsOutput
		 */
		public void setMemberDemographicsOutput(MemberDemographicsOutput param) {
			this.localMemberDemographicsOutput = param;
		}

		/**
		 * Auto generated getter method
		 * 
		 * @return Address
		 */
		public Address getAddress() {
			return localAddress;
		}

		/**
		 * Auto generated setter method
		 * 
		 * @param param
		 *            Address
		 */
		public void setAddress(Address param) {
			this.localAddress = param;
		}

		/**
		 * Auto generated getter method
		 * 
		 * @return ContactData
		 */
		public ContactData getContactData() {
			return localContactData;
		}

		/**
		 * Auto generated setter method
		 * 
		 * @param param
		 *            ContactData
		 */
		public void setContactData(ContactData param) {
			this.localContactData = param;
		}

		/**
		 * Auto generated getter method
		 * 
		 * @return EnrollmentData
		 */
		public EnrollmentData getEnrollmentData() {
			return localEnrollmentData;
		}

		/**
		 * Auto generated setter method
		 * 
		 * @param param
		 *            EnrollmentData
		 */
		public void setEnrollmentData(EnrollmentData param) {
			this.localEnrollmentData = param;
		}

		/**
		 * Auto generated getter method
		 * 
		 * @return MemberList
		 */
		public MemberList getMemberList() {
			return localMemberList;
		}

		/**
		 * Auto generated setter method
		 * 
		 * @param param
		 *            MemberList
		 */
		public void setMemberList(MemberList param) {
			this.localMemberList = param;
		}

		/**
		 * Auto generated getter method
		 * 
		 * @return CoverageList
		 */
		public CoverageList getCoverageList() {
			return localCoverageList;
		}

		/**
		 * Auto generated setter method
		 * 
		 * @param param
		 *            CoverageList
		 */
		public void setCoverageList(CoverageList param) {
			this.localCoverageList = param;
		}

		/**
		 *
		 * @param parentQName
		 * @param factory
		 * @return org.apache.axiom.om.OMElement
		 */
		public org.apache.axiom.om.OMElement getOMElement(final javax.xml.namespace.QName parentQName, final org.apache.axiom.om.OMFactory factory)
				throws org.apache.axis2.databinding.ADBException {
			org.apache.axiom.om.OMDataSource dataSource = new org.apache.axis2.databinding.ADBDataSource(this, parentQName);

			return factory.createOMElement(dataSource, parentQName);
		}

		public void serialize(final javax.xml.namespace.QName parentQName, javax.xml.stream.XMLStreamWriter xmlWriter)
				throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {
			serialize(parentQName, xmlWriter, false);
		}

		public void serialize(final javax.xml.namespace.QName parentQName, javax.xml.stream.XMLStreamWriter xmlWriter, boolean serializeType)
				throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {
			java.lang.String prefix = null;
			java.lang.String namespace = null;

			prefix = parentQName.getPrefix();
			namespace = parentQName.getNamespaceURI();
			writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);

			if (serializeType) {
				java.lang.String namespacePrefix = registerPrefix(xmlWriter, "http://servicehelpers.esb.avalon.com");

				if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)) {
					writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", namespacePrefix + ":memberDataResponse", xmlWriter);
				} else {
					writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", "memberDataResponse", xmlWriter);
				}
			}

			if (localResponseCodeTracker) {
				if (localResponseCode != null) {
					namespace = "";

					for (int i = 0; i < localResponseCode.length; i++) {
						if (localResponseCode[i] != null) {
							writeStartElement(null, namespace, "responseCode", xmlWriter);

							xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localResponseCode[i]));

							xmlWriter.writeEndElement();
						} else {
							// we have to do nothing since minOccurs is zero
						}
					}
				} else {
					throw new org.apache.axis2.databinding.ADBException("responseCode cannot be null!!");
				}
			}

			namespace = "";
			writeStartElement(null, namespace, "healthPlanGroupId", xmlWriter);

			if (localHealthPlanGroupId == null) {
				// write the nil attribute
				throw new org.apache.axis2.databinding.ADBException("healthPlanGroupId cannot be null!!");
			} else {
				xmlWriter.writeCharacters(localHealthPlanGroupId);
			}

			xmlWriter.writeEndElement();

			namespace = "";
			writeStartElement(null, namespace, "labBenefitId", xmlWriter);

			if (localLabBenefitId == null) {
				// write the nil attribute
				throw new org.apache.axis2.databinding.ADBException("labBenefitId cannot be null!!");
			} else {
				xmlWriter.writeCharacters(localLabBenefitId);
			}

			xmlWriter.writeEndElement();

			namespace = "";
			writeStartElement(null, namespace, "labBenefitIdDescription", xmlWriter);

			if (localLabBenefitIdDescription == null) {
				// write the nil attribute
				throw new org.apache.axis2.databinding.ADBException("labBenefitIdDescription cannot be null!!");
			} else {
				xmlWriter.writeCharacters(localLabBenefitIdDescription);
			}

			xmlWriter.writeEndElement();

			namespace = "";
			writeStartElement(null, namespace, "subscriberNumber", xmlWriter);

			if (localSubscriberNumber == null) {
				// write the nil attribute
				throw new org.apache.axis2.databinding.ADBException("subscriberNumber cannot be null!!");
			} else {
				xmlWriter.writeCharacters(localSubscriberNumber);
			}

			xmlWriter.writeEndElement();

			if (localBusinessSectorSegment == null) {
				throw new org.apache.axis2.databinding.ADBException("businessSectorSegment cannot be null!!");
			}

			localBusinessSectorSegment.serialize(new javax.xml.namespace.QName("", "businessSectorSegment"), xmlWriter);

			if (localMemberDemographicsOutput == null) {
				throw new org.apache.axis2.databinding.ADBException("memberDemographicsOutput cannot be null!!");
			}

			localMemberDemographicsOutput.serialize(new javax.xml.namespace.QName("", "memberDemographicsOutput"), xmlWriter);

			if (localAddress == null) {
				throw new org.apache.axis2.databinding.ADBException("address cannot be null!!");
			}

			localAddress.serialize(new javax.xml.namespace.QName("", "address"), xmlWriter);

			if (localContactData == null) {
				throw new org.apache.axis2.databinding.ADBException("contactData cannot be null!!");
			}

			localContactData.serialize(new javax.xml.namespace.QName("", "contactData"), xmlWriter);

			if (localEnrollmentData == null) {
				throw new org.apache.axis2.databinding.ADBException("enrollmentData cannot be null!!");
			}

			localEnrollmentData.serialize(new javax.xml.namespace.QName("", "enrollmentData"), xmlWriter);

			if (localMemberList == null) {
				throw new org.apache.axis2.databinding.ADBException("memberList cannot be null!!");
			}

			localMemberList.serialize(new javax.xml.namespace.QName("", "memberList"), xmlWriter);

			if (localCoverageList == null) {
				throw new org.apache.axis2.databinding.ADBException("coverageList cannot be null!!");
			}

			localCoverageList.serialize(new javax.xml.namespace.QName("", "coverageList"), xmlWriter);

			xmlWriter.writeEndElement();
		}

		private static java.lang.String generatePrefix(java.lang.String namespace) {
			if (namespace.equals("http://servicehelpers.esb.avalon.com")) {
				return "ns1";
			}

			return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
		}

		/**
		 * Utility method to write an element start tag.
		 */
		private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
				javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
			java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

			if (writerPrefix != null) {
				xmlWriter.writeStartElement(namespace, localPart);
			} else {
				if (namespace.length() == 0) {
					prefix = "";
				} else if (prefix == null) {
					prefix = generatePrefix(namespace);
				}

				xmlWriter.writeStartElement(prefix, localPart, namespace);
				xmlWriter.writeNamespace(prefix, namespace);
				xmlWriter.setPrefix(prefix, namespace);
			}
		}

		/**
		 * Util method to write an attribute with the ns prefix
		 */
		private void writeAttribute(java.lang.String prefix, java.lang.String namespace, java.lang.String attName, java.lang.String attValue,
				javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
			if (xmlWriter.getPrefix(namespace) == null) {
				xmlWriter.writeNamespace(prefix, namespace);
				xmlWriter.setPrefix(prefix, namespace);
			}

			xmlWriter.writeAttribute(namespace, attName, attValue);
		}

		/**
		 * Util method to write an attribute without the ns prefix
		 */
		private void writeAttribute(java.lang.String namespace, java.lang.String attName, java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter)
				throws javax.xml.stream.XMLStreamException {
			if (namespace.equals("")) {
				xmlWriter.writeAttribute(attName, attValue);
			} else {
				registerPrefix(xmlWriter, namespace);
				xmlWriter.writeAttribute(namespace, attName, attValue);
			}
		}

		/**
		 * Util method to write an attribute without the ns prefix
		 */
		private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName, javax.xml.namespace.QName qname,
				javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
			java.lang.String attributeNamespace = qname.getNamespaceURI();
			java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);

			if (attributePrefix == null) {
				attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
			}

			java.lang.String attributeValue;

			if (attributePrefix.trim().length() > 0) {
				attributeValue = attributePrefix + ":" + qname.getLocalPart();
			} else {
				attributeValue = qname.getLocalPart();
			}

			if (namespace.equals("")) {
				xmlWriter.writeAttribute(attName, attributeValue);
			} else {
				registerPrefix(xmlWriter, namespace);
				xmlWriter.writeAttribute(namespace, attName, attributeValue);
			}
		}

		/**
		 * method to handle Qnames
		 */
		private void writeQName(javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
			java.lang.String namespaceURI = qname.getNamespaceURI();

			if (namespaceURI != null) {
				java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);

				if (prefix == null) {
					prefix = generatePrefix(namespaceURI);
					xmlWriter.writeNamespace(prefix, namespaceURI);
					xmlWriter.setPrefix(prefix, namespaceURI);
				}

				if (prefix.trim().length() > 0) {
					xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
				} else {
					// i.e this is the default namespace
					xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
				}
			} else {
				xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
			}
		}

		private void writeQNames(javax.xml.namespace.QName[] qnames, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
			if (qnames != null) {
				// we have to store this data until last moment since it is not
				// possible to write any
				// namespace data after writing the charactor data
				java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
				java.lang.String namespaceURI = null;
				java.lang.String prefix = null;

				for (int i = 0; i < qnames.length; i++) {
					if (i > 0) {
						stringToWrite.append(" ");
					}

					namespaceURI = qnames[i].getNamespaceURI();

					if (namespaceURI != null) {
						prefix = xmlWriter.getPrefix(namespaceURI);

						if ((prefix == null) || (prefix.length() == 0)) {
							prefix = generatePrefix(namespaceURI);
							xmlWriter.writeNamespace(prefix, namespaceURI);
							xmlWriter.setPrefix(prefix, namespaceURI);
						}

						if (prefix.trim().length() > 0) {
							stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
						} else {
							stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
						}
					} else {
						stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
					}
				}

				xmlWriter.writeCharacters(stringToWrite.toString());
			}
		}

		/**
		 * Register a namespace prefix
		 */
		private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace)
				throws javax.xml.stream.XMLStreamException {
			java.lang.String prefix = xmlWriter.getPrefix(namespace);

			if (prefix == null) {
				prefix = generatePrefix(namespace);

				javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();

				while (true) {
					java.lang.String uri = nsContext.getNamespaceURI(prefix);

					if ((uri == null) || (uri.length() == 0)) {
						break;
					}

					prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
				}

				xmlWriter.writeNamespace(prefix, namespace);
				xmlWriter.setPrefix(prefix, namespace);
			}

			return prefix;
		}

		/**
		 * databinding method to get an XML representation of this object
		 *
		 */
		public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName) throws org.apache.axis2.databinding.ADBException {
			java.util.ArrayList elementList = new java.util.ArrayList();
			java.util.ArrayList attribList = new java.util.ArrayList();

			if (localResponseCodeTracker) {
				if (localResponseCode != null) {
					for (int i = 0; i < localResponseCode.length; i++) {
						if (localResponseCode[i] != null) {
							elementList.add(new javax.xml.namespace.QName("", "responseCode"));
							elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localResponseCode[i]));
						} else {
							// have to do nothing
						}
					}
				} else {
					throw new org.apache.axis2.databinding.ADBException("responseCode cannot be null!!");
				}
			}

			elementList.add(new javax.xml.namespace.QName("", "healthPlanGroupId"));

			if (localHealthPlanGroupId != null) {
				elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localHealthPlanGroupId));
			} else {
				throw new org.apache.axis2.databinding.ADBException("healthPlanGroupId cannot be null!!");
			}

			elementList.add(new javax.xml.namespace.QName("", "labBenefitId"));

			if (localLabBenefitId != null) {
				elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLabBenefitId));
			} else {
				throw new org.apache.axis2.databinding.ADBException("labBenefitId cannot be null!!");
			}

			elementList.add(new javax.xml.namespace.QName("", "labBenefitIdDescription"));

			if (localLabBenefitIdDescription != null) {
				elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLabBenefitIdDescription));
			} else {
				throw new org.apache.axis2.databinding.ADBException("labBenefitIdDescription cannot be null!!");
			}

			elementList.add(new javax.xml.namespace.QName("", "subscriberNumber"));

			if (localSubscriberNumber != null) {
				elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSubscriberNumber));
			} else {
				throw new org.apache.axis2.databinding.ADBException("subscriberNumber cannot be null!!");
			}

			elementList.add(new javax.xml.namespace.QName("", "businessSectorSegment"));

			if (localBusinessSectorSegment == null) {
				throw new org.apache.axis2.databinding.ADBException("businessSectorSegment cannot be null!!");
			}

			elementList.add(localBusinessSectorSegment);

			elementList.add(new javax.xml.namespace.QName("", "memberDemographicsOutput"));

			if (localMemberDemographicsOutput == null) {
				throw new org.apache.axis2.databinding.ADBException("memberDemographicsOutput cannot be null!!");
			}

			elementList.add(localMemberDemographicsOutput);

			elementList.add(new javax.xml.namespace.QName("", "address"));

			if (localAddress == null) {
				throw new org.apache.axis2.databinding.ADBException("address cannot be null!!");
			}

			elementList.add(localAddress);

			elementList.add(new javax.xml.namespace.QName("", "contactData"));

			if (localContactData == null) {
				throw new org.apache.axis2.databinding.ADBException("contactData cannot be null!!");
			}

			elementList.add(localContactData);

			elementList.add(new javax.xml.namespace.QName("", "enrollmentData"));

			if (localEnrollmentData == null) {
				throw new org.apache.axis2.databinding.ADBException("enrollmentData cannot be null!!");
			}

			elementList.add(localEnrollmentData);

			elementList.add(new javax.xml.namespace.QName("", "memberList"));

			if (localMemberList == null) {
				throw new org.apache.axis2.databinding.ADBException("memberList cannot be null!!");
			}

			elementList.add(localMemberList);

			elementList.add(new javax.xml.namespace.QName("", "coverageList"));

			if (localCoverageList == null) {
				throw new org.apache.axis2.databinding.ADBException("coverageList cannot be null!!");
			}

			elementList.add(localCoverageList);

			return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
		}

		/**
		 * Factory class that keeps the parse method
		 */
		public static class Factory {
			/**
			 * static method to create the object Precondition: If this object is an element, the current or next start element starts this object and any
			 * intervening reader events are ignorable If this object is not an element, it is a complex type and the reader is at the event just after the
			 * outer start element Postcondition: If this object is an element, the reader is positioned at its end element If this object is a complex type,
			 * the reader is positioned at the end element of its outer element
			 */
			public static MemberDataResponse parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception {
				MemberDataResponse object = new MemberDataResponse();

				int event;
				java.lang.String nillableValue = null;
				java.lang.String prefix = "";
				java.lang.String namespaceuri = "";

				try {
					while (!reader.isStartElement() && !reader.isEndElement())
						reader.next();

					if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
						java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type");

						if (fullTypeName != null) {
							java.lang.String nsPrefix = null;

							if (fullTypeName.indexOf(":") > -1) {
								nsPrefix = fullTypeName.substring(0, fullTypeName.indexOf(":"));
							}

							nsPrefix = (nsPrefix == null) ? "" : nsPrefix;

							java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":") + 1);

							if (!"memberDataResponse".equals(type)) {
								// find namespace for the prefix
								java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);

								return (MemberDataResponse) ExtensionMapper.getTypeObject(nsUri, type, reader);
							}
						}
					}

					// Note all attributes that were handled. Used to differ
					// normal attributes
					// from anyAttributes.
					java.util.Vector handledAttributes = new java.util.Vector();

					reader.next();

					java.util.ArrayList list1 = new java.util.ArrayList();

					while (!reader.isStartElement() && !reader.isEndElement())
						reader.next();

					if (reader.isStartElement() && new javax.xml.namespace.QName("", "responseCode").equals(reader.getName())) {
						// Process the array and step past its final element's
						// end.
						list1.add(reader.getElementText());

						// loop until we find a start element that is not part
						// of this array
						boolean loopDone1 = false;

						while (!loopDone1) {
							// Ensure we are at the EndElement
							while (!reader.isEndElement()) {
								reader.next();
							}

							// Step out of this element
							reader.next();

							// Step to next element event.
							while (!reader.isStartElement() && !reader.isEndElement())
								reader.next();

							if (reader.isEndElement()) {
								// two continuous end elements means we are
								// exiting the xml structure
								loopDone1 = true;
							} else {
								if (new javax.xml.namespace.QName("", "responseCode").equals(reader.getName())) {
									list1.add(reader.getElementText());
								} else {
									loopDone1 = true;
								}
							}
						}

						// call the converter utility to convert and set the
						// array
						object.setResponseCode((java.lang.String[]) list1.toArray(new java.lang.String[list1.size()]));
					} // End of if for expected property start element

					else {
					}

					while (!reader.isStartElement() && !reader.isEndElement())
						reader.next();

					if (reader.isStartElement() && new javax.xml.namespace.QName("", "healthPlanGroupId").equals(reader.getName())) {
						nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");

						if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
							throw new org.apache.axis2.databinding.ADBException("The element: " + "healthPlanGroupId" + "  cannot be null");
						}

						java.lang.String content = reader.getElementText();

						object.setHealthPlanGroupId(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

						reader.next();
					} // End of if for expected property start element

					else {
						// A start element we are not expecting indicates an
						// invalid parameter was passed
						throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
					}

					while (!reader.isStartElement() && !reader.isEndElement())
						reader.next();

					if (reader.isStartElement() && new javax.xml.namespace.QName("", "labBenefitId").equals(reader.getName())) {
						nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");

						if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
							throw new org.apache.axis2.databinding.ADBException("The element: " + "labBenefitId" + "  cannot be null");
						}

						java.lang.String content = reader.getElementText();

						object.setLabBenefitId(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

						reader.next();
					} // End of if for expected property start element

					else {
						// A start element we are not expecting indicates an
						// invalid parameter was passed
						throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
					}

					while (!reader.isStartElement() && !reader.isEndElement())
						reader.next();

					if (reader.isStartElement() && new javax.xml.namespace.QName("", "labBenefitIdDescription").equals(reader.getName())) {
						nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");

						if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
							throw new org.apache.axis2.databinding.ADBException("The element: " + "labBenefitIdDescription" + "  cannot be null");
						}

						java.lang.String content = reader.getElementText();

						object.setLabBenefitIdDescription(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

						reader.next();
					} // End of if for expected property start element

					else {
						// A start element we are not expecting indicates an
						// invalid parameter was passed
						throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
					}

					while (!reader.isStartElement() && !reader.isEndElement())
						reader.next();

					if (reader.isStartElement() && new javax.xml.namespace.QName("", "subscriberNumber").equals(reader.getName())) {
						nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");

						if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
							throw new org.apache.axis2.databinding.ADBException("The element: " + "subscriberNumber" + "  cannot be null");
						}

						java.lang.String content = reader.getElementText();

						object.setSubscriberNumber(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

						reader.next();
					} // End of if for expected property start element

					else {
						// A start element we are not expecting indicates an
						// invalid parameter was passed
						throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
					}

					while (!reader.isStartElement() && !reader.isEndElement())
						reader.next();

					if (reader.isStartElement() && new javax.xml.namespace.QName("", "businessSectorSegment").equals(reader.getName())) {
						object.setBusinessSectorSegment(BusinessSectorSegment.Factory.parse(reader));

						reader.next();
					} // End of if for expected property start element

					else {
						// A start element we are not expecting indicates an
						// invalid parameter was passed
						throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
					}

					while (!reader.isStartElement() && !reader.isEndElement())
						reader.next();

					if (reader.isStartElement() && new javax.xml.namespace.QName("", "memberDemographicsOutput").equals(reader.getName())) {
						object.setMemberDemographicsOutput(MemberDemographicsOutput.Factory.parse(reader));

						reader.next();
					} // End of if for expected property start element

					else {
						// A start element we are not expecting indicates an
						// invalid parameter was passed
						throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
					}

					while (!reader.isStartElement() && !reader.isEndElement())
						reader.next();

					if (reader.isStartElement() && new javax.xml.namespace.QName("", "address").equals(reader.getName())) {
						object.setAddress(Address.Factory.parse(reader));

						reader.next();
					} // End of if for expected property start element

					else {
						// A start element we are not expecting indicates an
						// invalid parameter was passed
						throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
					}

					while (!reader.isStartElement() && !reader.isEndElement())
						reader.next();

					if (reader.isStartElement() && new javax.xml.namespace.QName("", "contactData").equals(reader.getName())) {
						object.setContactData(ContactData.Factory.parse(reader));

						reader.next();
					} // End of if for expected property start element

					else {
						// A start element we are not expecting indicates an
						// invalid parameter was passed
						throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
					}

					while (!reader.isStartElement() && !reader.isEndElement())
						reader.next();

					if (reader.isStartElement() && new javax.xml.namespace.QName("", "enrollmentData").equals(reader.getName())) {
						object.setEnrollmentData(EnrollmentData.Factory.parse(reader));

						reader.next();
					} // End of if for expected property start element

					else {
						// A start element we are not expecting indicates an
						// invalid parameter was passed
						throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
					}

					while (!reader.isStartElement() && !reader.isEndElement())
						reader.next();

					if (reader.isStartElement() && new javax.xml.namespace.QName("", "memberList").equals(reader.getName())) {
						object.setMemberList(MemberList.Factory.parse(reader));

						reader.next();
					} // End of if for expected property start element

					else {
						// A start element we are not expecting indicates an
						// invalid parameter was passed
						throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
					}

					while (!reader.isStartElement() && !reader.isEndElement())
						reader.next();

					if (reader.isStartElement() && new javax.xml.namespace.QName("", "coverageList").equals(reader.getName())) {
						object.setCoverageList(CoverageList.Factory.parse(reader));

						reader.next();
					} // End of if for expected property start element

					else {
						// A start element we are not expecting indicates an
						// invalid parameter was passed
						throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
					}

					while (!reader.isStartElement() && !reader.isEndElement())
						reader.next();

					if (reader.isStartElement()) {
						// A start element we are not expecting indicates a
						// trailing invalid property
						throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
					}
				} catch (javax.xml.stream.XMLStreamException e) {
					throw new java.lang.Exception(e);
				}

				return object;
			}
		} // end of factory class
	}

	public static class EnrollmentData implements org.apache.axis2.databinding.ADBBean {
		/*
		 * This type was generated from the piece of schema that had name = EnrollmentData Namespace URI = http://servicehelpers.esb.avalon.com Namespace Prefix
		 * = ns1
		 */

		/**
		 * field for SubscriberEnrollmentBeginDate
		 */
		protected java.util.Date localSubscriberEnrollmentBeginDate;

		/**
		 * field for MemberEnrollmentBeginDate
		 */
		protected java.util.Date localMemberEnrollmentBeginDate;

		/**
		 * field for AmmsGroupNumber
		 */
		protected java.lang.String localAmmsGroupNumber;

		/**
		 * Auto generated getter method
		 * 
		 * @return java.util.Date
		 */
		public java.util.Date getSubscriberEnrollmentBeginDate() {
			return localSubscriberEnrollmentBeginDate;
		}

		/**
		 * Auto generated setter method
		 * 
		 * @param param
		 *            SubscriberEnrollmentBeginDate
		 */
		public void setSubscriberEnrollmentBeginDate(java.util.Date param) {
			this.localSubscriberEnrollmentBeginDate = param;
		}

		/**
		 * Auto generated getter method
		 * 
		 * @return java.util.Date
		 */
		public java.util.Date getMemberEnrollmentBeginDate() {
			return localMemberEnrollmentBeginDate;
		}

		/**
		 * Auto generated setter method
		 * 
		 * @param param
		 *            MemberEnrollmentBeginDate
		 */
		public void setMemberEnrollmentBeginDate(java.util.Date param) {
			this.localMemberEnrollmentBeginDate = param;
		}

		/**
		 * Auto generated getter method
		 * 
		 * @return java.lang.String
		 */
		public java.lang.String getAmmsGroupNumber() {
			return localAmmsGroupNumber;
		}

		/**
		 * Auto generated setter method
		 * 
		 * @param param
		 *            AmmsGroupNumber
		 */
		public void setAmmsGroupNumber(java.lang.String param) {
			this.localAmmsGroupNumber = param;
		}

		/**
		 *
		 * @param parentQName
		 * @param factory
		 * @return org.apache.axiom.om.OMElement
		 */
		public org.apache.axiom.om.OMElement getOMElement(final javax.xml.namespace.QName parentQName, final org.apache.axiom.om.OMFactory factory)
				throws org.apache.axis2.databinding.ADBException {
			org.apache.axiom.om.OMDataSource dataSource = new org.apache.axis2.databinding.ADBDataSource(this, parentQName);

			return factory.createOMElement(dataSource, parentQName);
		}

		public void serialize(final javax.xml.namespace.QName parentQName, javax.xml.stream.XMLStreamWriter xmlWriter)
				throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {
			serialize(parentQName, xmlWriter, false);
		}

		public void serialize(final javax.xml.namespace.QName parentQName, javax.xml.stream.XMLStreamWriter xmlWriter, boolean serializeType)
				throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {
			java.lang.String prefix = null;
			java.lang.String namespace = null;

			prefix = parentQName.getPrefix();
			namespace = parentQName.getNamespaceURI();
			writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);

			if (serializeType) {
				java.lang.String namespacePrefix = registerPrefix(xmlWriter, "http://servicehelpers.esb.avalon.com");

				if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)) {
					writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", namespacePrefix + ":EnrollmentData", xmlWriter);
				} else {
					writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", "EnrollmentData", xmlWriter);
				}
			}

			namespace = "";
			writeStartElement(null, namespace, "subscriberEnrollmentBeginDate", xmlWriter);

			if (localSubscriberEnrollmentBeginDate == null) {
				// write the nil attribute
				throw new org.apache.axis2.databinding.ADBException("subscriberEnrollmentBeginDate cannot be null!!");
			} else {
				xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSubscriberEnrollmentBeginDate));
			}

			xmlWriter.writeEndElement();

			namespace = "";
			writeStartElement(null, namespace, "memberEnrollmentBeginDate", xmlWriter);

			if (localMemberEnrollmentBeginDate == null) {
				// write the nil attribute
				throw new org.apache.axis2.databinding.ADBException("memberEnrollmentBeginDate cannot be null!!");
			} else {
				xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localMemberEnrollmentBeginDate));
			}

			xmlWriter.writeEndElement();

			namespace = "";
			writeStartElement(null, namespace, "ammsGroupNumber", xmlWriter);

			if (localAmmsGroupNumber == null) {
				// write the nil attribute
				throw new org.apache.axis2.databinding.ADBException("ammsGroupNumber cannot be null!!");
			} else {
				xmlWriter.writeCharacters(localAmmsGroupNumber);
			}

			xmlWriter.writeEndElement();

			xmlWriter.writeEndElement();
		}

		private static java.lang.String generatePrefix(java.lang.String namespace) {
			if (namespace.equals("http://servicehelpers.esb.avalon.com")) {
				return "ns1";
			}

			return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
		}

		/**
		 * Utility method to write an element start tag.
		 */
		private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
				javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
			java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

			if (writerPrefix != null) {
				xmlWriter.writeStartElement(namespace, localPart);
			} else {
				if (namespace.length() == 0) {
					prefix = "";
				} else if (prefix == null) {
					prefix = generatePrefix(namespace);
				}

				xmlWriter.writeStartElement(prefix, localPart, namespace);
				xmlWriter.writeNamespace(prefix, namespace);
				xmlWriter.setPrefix(prefix, namespace);
			}
		}

		/**
		 * Util method to write an attribute with the ns prefix
		 */
		private void writeAttribute(java.lang.String prefix, java.lang.String namespace, java.lang.String attName, java.lang.String attValue,
				javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
			if (xmlWriter.getPrefix(namespace) == null) {
				xmlWriter.writeNamespace(prefix, namespace);
				xmlWriter.setPrefix(prefix, namespace);
			}

			xmlWriter.writeAttribute(namespace, attName, attValue);
		}

		/**
		 * Util method to write an attribute without the ns prefix
		 */
		private void writeAttribute(java.lang.String namespace, java.lang.String attName, java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter)
				throws javax.xml.stream.XMLStreamException {
			if (namespace.equals("")) {
				xmlWriter.writeAttribute(attName, attValue);
			} else {
				registerPrefix(xmlWriter, namespace);
				xmlWriter.writeAttribute(namespace, attName, attValue);
			}
		}

		/**
		 * Util method to write an attribute without the ns prefix
		 */
		private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName, javax.xml.namespace.QName qname,
				javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
			java.lang.String attributeNamespace = qname.getNamespaceURI();
			java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);

			if (attributePrefix == null) {
				attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
			}

			java.lang.String attributeValue;

			if (attributePrefix.trim().length() > 0) {
				attributeValue = attributePrefix + ":" + qname.getLocalPart();
			} else {
				attributeValue = qname.getLocalPart();
			}

			if (namespace.equals("")) {
				xmlWriter.writeAttribute(attName, attributeValue);
			} else {
				registerPrefix(xmlWriter, namespace);
				xmlWriter.writeAttribute(namespace, attName, attributeValue);
			}
		}

		/**
		 * method to handle Qnames
		 */
		private void writeQName(javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
			java.lang.String namespaceURI = qname.getNamespaceURI();

			if (namespaceURI != null) {
				java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);

				if (prefix == null) {
					prefix = generatePrefix(namespaceURI);
					xmlWriter.writeNamespace(prefix, namespaceURI);
					xmlWriter.setPrefix(prefix, namespaceURI);
				}

				if (prefix.trim().length() > 0) {
					xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
				} else {
					// i.e this is the default namespace
					xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
				}
			} else {
				xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
			}
		}

		private void writeQNames(javax.xml.namespace.QName[] qnames, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
			if (qnames != null) {
				// we have to store this data until last moment since it is not
				// possible to write any
				// namespace data after writing the charactor data
				java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
				java.lang.String namespaceURI = null;
				java.lang.String prefix = null;

				for (int i = 0; i < qnames.length; i++) {
					if (i > 0) {
						stringToWrite.append(" ");
					}

					namespaceURI = qnames[i].getNamespaceURI();

					if (namespaceURI != null) {
						prefix = xmlWriter.getPrefix(namespaceURI);

						if ((prefix == null) || (prefix.length() == 0)) {
							prefix = generatePrefix(namespaceURI);
							xmlWriter.writeNamespace(prefix, namespaceURI);
							xmlWriter.setPrefix(prefix, namespaceURI);
						}

						if (prefix.trim().length() > 0) {
							stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
						} else {
							stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
						}
					} else {
						stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
					}
				}

				xmlWriter.writeCharacters(stringToWrite.toString());
			}
		}

		/**
		 * Register a namespace prefix
		 */
		private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace)
				throws javax.xml.stream.XMLStreamException {
			java.lang.String prefix = xmlWriter.getPrefix(namespace);

			if (prefix == null) {
				prefix = generatePrefix(namespace);

				javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();

				while (true) {
					java.lang.String uri = nsContext.getNamespaceURI(prefix);

					if ((uri == null) || (uri.length() == 0)) {
						break;
					}

					prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
				}

				xmlWriter.writeNamespace(prefix, namespace);
				xmlWriter.setPrefix(prefix, namespace);
			}

			return prefix;
		}

		/**
		 * databinding method to get an XML representation of this object
		 *
		 */
		public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName) throws org.apache.axis2.databinding.ADBException {
			java.util.ArrayList elementList = new java.util.ArrayList();
			java.util.ArrayList attribList = new java.util.ArrayList();

			elementList.add(new javax.xml.namespace.QName("", "subscriberEnrollmentBeginDate"));

			if (localSubscriberEnrollmentBeginDate != null) {
				elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSubscriberEnrollmentBeginDate));
			} else {
				throw new org.apache.axis2.databinding.ADBException("subscriberEnrollmentBeginDate cannot be null!!");
			}

			elementList.add(new javax.xml.namespace.QName("", "memberEnrollmentBeginDate"));

			if (localMemberEnrollmentBeginDate != null) {
				elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localMemberEnrollmentBeginDate));
			} else {
				throw new org.apache.axis2.databinding.ADBException("memberEnrollmentBeginDate cannot be null!!");
			}

			elementList.add(new javax.xml.namespace.QName("", "ammsGroupNumber"));

			if (localAmmsGroupNumber != null) {
				elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localAmmsGroupNumber));
			} else {
				throw new org.apache.axis2.databinding.ADBException("ammsGroupNumber cannot be null!!");
			}

			return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
		}

		/**
		 * Factory class that keeps the parse method
		 */
		public static class Factory {
			/**
			 * static method to create the object Precondition: If this object is an element, the current or next start element starts this object and any
			 * intervening reader events are ignorable If this object is not an element, it is a complex type and the reader is at the event just after the
			 * outer start element Postcondition: If this object is an element, the reader is positioned at its end element If this object is a complex type,
			 * the reader is positioned at the end element of its outer element
			 */
			public static EnrollmentData parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception {
				EnrollmentData object = new EnrollmentData();

				int event;
				java.lang.String nillableValue = null;
				java.lang.String prefix = "";
				java.lang.String namespaceuri = "";

				try {
					while (!reader.isStartElement() && !reader.isEndElement())
						reader.next();

					if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
						java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type");

						if (fullTypeName != null) {
							java.lang.String nsPrefix = null;

							if (fullTypeName.indexOf(":") > -1) {
								nsPrefix = fullTypeName.substring(0, fullTypeName.indexOf(":"));
							}

							nsPrefix = (nsPrefix == null) ? "" : nsPrefix;

							java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":") + 1);

							if (!"EnrollmentData".equals(type)) {
								// find namespace for the prefix
								java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);

								return (EnrollmentData) ExtensionMapper.getTypeObject(nsUri, type, reader);
							}
						}
					}

					// Note all attributes that were handled. Used to differ
					// normal attributes
					// from anyAttributes.
					java.util.Vector handledAttributes = new java.util.Vector();

					reader.next();

					while (!reader.isStartElement() && !reader.isEndElement())
						reader.next();

					if (reader.isStartElement() && new javax.xml.namespace.QName("", "subscriberEnrollmentBeginDate").equals(reader.getName())) {
						nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");

						if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
							throw new org.apache.axis2.databinding.ADBException("The element: " + "subscriberEnrollmentBeginDate" + "  cannot be null");
						}

						java.lang.String content = reader.getElementText();

						object.setSubscriberEnrollmentBeginDate(org.apache.axis2.databinding.utils.ConverterUtil.convertToDate(content));

						reader.next();
					} // End of if for expected property start element

					else {
						// A start element we are not expecting indicates an
						// invalid parameter was passed
						throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
					}

					while (!reader.isStartElement() && !reader.isEndElement())
						reader.next();

					if (reader.isStartElement() && new javax.xml.namespace.QName("", "memberEnrollmentBeginDate").equals(reader.getName())) {
						nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");

						if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
							throw new org.apache.axis2.databinding.ADBException("The element: " + "memberEnrollmentBeginDate" + "  cannot be null");
						}

						java.lang.String content = reader.getElementText();

						object.setMemberEnrollmentBeginDate(org.apache.axis2.databinding.utils.ConverterUtil.convertToDate(content));

						reader.next();
					} // End of if for expected property start element

					else {
						// A start element we are not expecting indicates an
						// invalid parameter was passed
						throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
					}

					while (!reader.isStartElement() && !reader.isEndElement())
						reader.next();

					if (reader.isStartElement() && new javax.xml.namespace.QName("", "ammsGroupNumber").equals(reader.getName())) {
						nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");

						if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
							throw new org.apache.axis2.databinding.ADBException("The element: " + "ammsGroupNumber" + "  cannot be null");
						}

						java.lang.String content = reader.getElementText();

						object.setAmmsGroupNumber(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

						reader.next();
					} // End of if for expected property start element

					else {
						// A start element we are not expecting indicates an
						// invalid parameter was passed
						throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
					}

					while (!reader.isStartElement() && !reader.isEndElement())
						reader.next();

					if (reader.isStartElement()) {
						// A start element we are not expecting indicates a
						// trailing invalid property
						throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
					}
				} catch (javax.xml.stream.XMLStreamException e) {
					throw new java.lang.Exception(e);
				}

				return object;
			}
		} // end of factory class
	}

	public static class SearchCriteria implements org.apache.axis2.databinding.ADBBean {
		/*
		 * This type was generated from the piece of schema that had name = searchCriteria Namespace URI = http://servicehelpers.esb.avalon.com Namespace Prefix
		 * = ns1
		 */

		/**
		 * field for HealthPlanGroupId
		 */

		protected java.lang.String localHealthPlanGroupId;

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.lang.Object#toString()
		 */
		@Override
		public String toString() {
			return "SearchCriteria [localHealthPlanGroupId=" + localHealthPlanGroupId + ", localHealthPlanGroupIdTracker=" + localHealthPlanGroupIdTracker
					+ ", localHealthPlanId=" + localHealthPlanId + ", localHealthPlanIdTracker=" + localHealthPlanIdTracker + ", localHealthPlanIdType="
					+ localHealthPlanIdType + ", localHealthPlanIdTypeTracker=" + localHealthPlanIdTypeTracker + ", localIdCardNumber=" + localIdCardNumber
					+ ", localIdCardNumberTracker=" + localIdCardNumberTracker + ", localPlanCode=" + localPlanCode + ", localPlanCodeTracker="
					+ localPlanCodeTracker + ", localProductCode=" + localProductCode + ", localProductCodeTracker=" + localProductCodeTracker
					+ ", localReasonForInquiry=" + localReasonForInquiry + ", localReasonForInquiryTracker=" + localReasonForInquiryTracker
					+ ", localRequestDate=" + localRequestDate + ", localRequestDateTracker=" + localRequestDateTracker + ", localRpn=" + localRpn
					+ ", localRpnTracker=" + localRpnTracker + ", localSubscriberNumber=" + localSubscriberNumber + ", localSubscriberNumberTracker="
					+ localSubscriberNumberTracker + "]";
		}

		/*
		 * This tracker boolean wil be used to detect whether the user called the set method for this attribute. It will be used to determine whether to include
		 * this field in the serialized XML
		 */
		protected boolean localHealthPlanGroupIdTracker = false;

		/**
		 * field for HealthPlanId
		 */
		protected java.lang.String localHealthPlanId;

		/*
		 * This tracker boolean wil be used to detect whether the user called the set method for this attribute. It will be used to determine whether to include
		 * this field in the serialized XML
		 */
		protected boolean localHealthPlanIdTracker = false;

		/**
		 * field for HealthPlanIdType
		 */
		protected java.lang.String localHealthPlanIdType;

		/*
		 * This tracker boolean wil be used to detect whether the user called the set method for this attribute. It will be used to determine whether to include
		 * this field in the serialized XML
		 */
		protected boolean localHealthPlanIdTypeTracker = false;

		/**
		 * field for IdCardNumber
		 */
		protected java.lang.String localIdCardNumber;

		/*
		 * This tracker boolean wil be used to detect whether the user called the set method for this attribute. It will be used to determine whether to include
		 * this field in the serialized XML
		 */
		protected boolean localIdCardNumberTracker = false;

		/**
		 * field for PlanCode
		 */
		protected java.lang.String localPlanCode;

		/*
		 * This tracker boolean wil be used to detect whether the user called the set method for this attribute. It will be used to determine whether to include
		 * this field in the serialized XML
		 */
		protected boolean localPlanCodeTracker = false;

		/**
		 * field for ProductCode
		 */
		protected java.lang.String localProductCode;

		/*
		 * This tracker boolean wil be used to detect whether the user called the set method for this attribute. It will be used to determine whether to include
		 * this field in the serialized XML
		 */
		protected boolean localProductCodeTracker = false;

		/**
		 * field for ReasonForInquiry
		 */
		protected java.lang.String localReasonForInquiry;

		/*
		 * This tracker boolean wil be used to detect whether the user called the set method for this attribute. It will be used to determine whether to include
		 * this field in the serialized XML
		 */
		protected boolean localReasonForInquiryTracker = false;

		/**
		 * field for RequestDate
		 */
		protected java.util.Calendar localRequestDate;

		/*
		 * This tracker boolean wil be used to detect whether the user called the set method for this attribute. It will be used to determine whether to include
		 * this field in the serialized XML
		 */
		protected boolean localRequestDateTracker = false;

		/**
		 * field for Rpn
		 */
		protected java.lang.String localRpn;

		/*
		 * This tracker boolean wil be used to detect whether the user called the set method for this attribute. It will be used to determine whether to include
		 * this field in the serialized XML
		 */
		protected boolean localRpnTracker = false;

		/**
		 * field for SubscriberNumber
		 */
		protected java.lang.String localSubscriberNumber;

		/*
		 * This tracker boolean wil be used to detect whether the user called the set method for this attribute. It will be used to determine whether to include
		 * this field in the serialized XML
		 */
		protected boolean localSubscriberNumberTracker = false;

		public boolean isHealthPlanGroupIdSpecified() {
			return localHealthPlanGroupIdTracker;
		}

		/**
		 * Auto generated getter method
		 * 
		 * @return java.lang.String
		 */
		public java.lang.String getHealthPlanGroupId() {
			return localHealthPlanGroupId;
		}

		/**
		 * Auto generated setter method
		 * 
		 * @param param
		 *            HealthPlanGroupId
		 */
		public void setHealthPlanGroupId(java.lang.String param) {
			localHealthPlanGroupIdTracker = param != null;

			this.localHealthPlanGroupId = param;
		}

		public boolean isHealthPlanIdSpecified() {
			return localHealthPlanIdTracker;
		}

		/**
		 * Auto generated getter method
		 * 
		 * @return java.lang.String
		 */
		public java.lang.String getHealthPlanId() {
			return localHealthPlanId;
		}

		/**
		 * Auto generated setter method
		 * 
		 * @param param
		 *            HealthPlanId
		 */
		public void setHealthPlanId(java.lang.String param) {
			localHealthPlanIdTracker = param != null;

			this.localHealthPlanId = param;
		}

		public boolean isHealthPlanIdTypeSpecified() {
			return localHealthPlanIdTypeTracker;
		}

		/**
		 * Auto generated getter method
		 * 
		 * @return java.lang.String
		 */
		public java.lang.String getHealthPlanIdType() {
			return localHealthPlanIdType;
		}

		/**
		 * Auto generated setter method
		 * 
		 * @param param
		 *            HealthPlanIdType
		 */
		public void setHealthPlanIdType(java.lang.String param) {
			localHealthPlanIdTypeTracker = param != null;

			this.localHealthPlanIdType = param;
		}

		public boolean isIdCardNumberSpecified() {
			return localIdCardNumberTracker;
		}

		/**
		 * Auto generated getter method
		 * 
		 * @return java.lang.String
		 */
		public java.lang.String getIdCardNumber() {
			return localIdCardNumber;
		}

		/**
		 * Auto generated setter method
		 * 
		 * @param param
		 *            IdCardNumber
		 */
		public void setIdCardNumber(java.lang.String param) {
			localIdCardNumberTracker = param != null;

			this.localIdCardNumber = param;
		}

		public boolean isPlanCodeSpecified() {
			return localPlanCodeTracker;
		}

		/**
		 * Auto generated getter method
		 * 
		 * @return java.lang.String
		 */
		public java.lang.String getPlanCode() {
			return localPlanCode;
		}

		/**
		 * Auto generated setter method
		 * 
		 * @param param
		 *            PlanCode
		 */
		public void setPlanCode(java.lang.String param) {
			localPlanCodeTracker = param != null;

			this.localPlanCode = param;
		}

		public boolean isProductCodeSpecified() {
			return localProductCodeTracker;
		}

		/**
		 * Auto generated getter method
		 * 
		 * @return java.lang.String
		 */
		public java.lang.String getProductCode() {
			return localProductCode;
		}

		/**
		 * Auto generated setter method
		 * 
		 * @param param
		 *            ProductCode
		 */
		public void setProductCode(java.lang.String param) {
			localProductCodeTracker = param != null;

			this.localProductCode = param;
		}

		public boolean isReasonForInquirySpecified() {
			return localReasonForInquiryTracker;
		}

		/**
		 * Auto generated getter method
		 * 
		 * @return java.lang.String
		 */
		public java.lang.String getReasonForInquiry() {
			return localReasonForInquiry;
		}

		/**
		 * Auto generated setter method
		 * 
		 * @param param
		 *            ReasonForInquiry
		 */
		public void setReasonForInquiry(java.lang.String param) {
			localReasonForInquiryTracker = param != null;

			this.localReasonForInquiry = param;
		}

		public boolean isRequestDateSpecified() {
			return localRequestDateTracker;
		}

		/**
		 * Auto generated getter method
		 * 
		 * @return java.util.Calendar
		 */
		public java.util.Calendar getRequestDate() {
			return localRequestDate;
		}

		/**
		 * Auto generated setter method
		 * 
		 * @param param
		 *            RequestDate
		 */
		public void setRequestDate(java.util.Calendar param) {
			localRequestDateTracker = param != null;

			this.localRequestDate = param;
		}

		public boolean isRpnSpecified() {
			return localRpnTracker;
		}

		/**
		 * Auto generated getter method
		 * 
		 * @return java.lang.String
		 */
		public java.lang.String getRpn() {
			return localRpn;
		}

		/**
		 * Auto generated setter method
		 * 
		 * @param param
		 *            Rpn
		 */
		public void setRpn(java.lang.String param) {
			localRpnTracker = param != null;

			this.localRpn = param;
		}

		public boolean isSubscriberNumberSpecified() {
			return localSubscriberNumberTracker;
		}

		/**
		 * Auto generated getter method
		 * 
		 * @return java.lang.String
		 */
		public java.lang.String getSubscriberNumber() {
			return localSubscriberNumber;
		}

		/**
		 * Auto generated setter method
		 * 
		 * @param param
		 *            SubscriberNumber
		 */
		public void setSubscriberNumber(java.lang.String param) {
			localSubscriberNumberTracker = param != null;

			this.localSubscriberNumber = param;
		}

		/**
		 *
		 * @param parentQName
		 * @param factory
		 * @return org.apache.axiom.om.OMElement
		 */
		public org.apache.axiom.om.OMElement getOMElement(final javax.xml.namespace.QName parentQName, final org.apache.axiom.om.OMFactory factory)
				throws org.apache.axis2.databinding.ADBException {
			org.apache.axiom.om.OMDataSource dataSource = new org.apache.axis2.databinding.ADBDataSource(this, parentQName);

			return factory.createOMElement(dataSource, parentQName);
		}

		public void serialize(final javax.xml.namespace.QName parentQName, javax.xml.stream.XMLStreamWriter xmlWriter)
				throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {
			serialize(parentQName, xmlWriter, false);
		}

		public void serialize(final javax.xml.namespace.QName parentQName, javax.xml.stream.XMLStreamWriter xmlWriter, boolean serializeType)
				throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {
			java.lang.String prefix = null;
			java.lang.String namespace = null;

			prefix = parentQName.getPrefix();
			namespace = parentQName.getNamespaceURI();
			writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);

			if (serializeType) {
				java.lang.String namespacePrefix = registerPrefix(xmlWriter, "http://servicehelpers.esb.avalon.com");

				if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)) {
					writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", namespacePrefix + ":searchCriteria", xmlWriter);
				} else {
					writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", "searchCriteria", xmlWriter);
				}
			}

			if (localHealthPlanGroupIdTracker) {
				namespace = "";
				writeStartElement(null, namespace, "healthPlanGroupId", xmlWriter);

				if (localHealthPlanGroupId == null) {
					// write the nil attribute
					throw new org.apache.axis2.databinding.ADBException("healthPlanGroupId cannot be null!!");
				} else {
					xmlWriter.writeCharacters(localHealthPlanGroupId);
				}

				xmlWriter.writeEndElement();
			}

			if (localHealthPlanIdTracker) {
				namespace = "";
				writeStartElement(null, namespace, "healthPlanId", xmlWriter);

				if (localHealthPlanId == null) {
					// write the nil attribute
					throw new org.apache.axis2.databinding.ADBException("healthPlanId cannot be null!!");
				} else {
					xmlWriter.writeCharacters(localHealthPlanId);
				}

				xmlWriter.writeEndElement();
			}

			if (localHealthPlanIdTypeTracker) {
				namespace = "";
				writeStartElement(null, namespace, "healthPlanIdType", xmlWriter);

				if (localHealthPlanIdType == null) {
					// write the nil attribute
					throw new org.apache.axis2.databinding.ADBException("healthPlanIdType cannot be null!!");
				} else {
					xmlWriter.writeCharacters(localHealthPlanIdType);
				}

				xmlWriter.writeEndElement();
			}

			if (localIdCardNumberTracker) {
				namespace = "";
				writeStartElement(null, namespace, "idCardNumber", xmlWriter);

				if (localIdCardNumber == null) {
					// write the nil attribute
					throw new org.apache.axis2.databinding.ADBException("idCardNumber cannot be null!!");
				} else {
					xmlWriter.writeCharacters(localIdCardNumber);
				}

				xmlWriter.writeEndElement();
			}

			if (localPlanCodeTracker) {
				namespace = "";
				writeStartElement(null, namespace, "planCode", xmlWriter);

				if (localPlanCode == null) {
					// write the nil attribute
					throw new org.apache.axis2.databinding.ADBException("planCode cannot be null!!");
				} else {
					xmlWriter.writeCharacters(localPlanCode);
				}

				xmlWriter.writeEndElement();
			}

			if (localProductCodeTracker) {
				namespace = "";
				writeStartElement(null, namespace, "productCode", xmlWriter);

				if (localProductCode == null) {
					// write the nil attribute
					throw new org.apache.axis2.databinding.ADBException("productCode cannot be null!!");
				} else {
					xmlWriter.writeCharacters(localProductCode);
				}

				xmlWriter.writeEndElement();
			}

			if (localReasonForInquiryTracker) {
				namespace = "";
				writeStartElement(null, namespace, "reasonForInquiry", xmlWriter);

				if (localReasonForInquiry == null) {
					// write the nil attribute
					throw new org.apache.axis2.databinding.ADBException("reasonForInquiry cannot be null!!");
				} else {
					xmlWriter.writeCharacters(localReasonForInquiry);
				}

				xmlWriter.writeEndElement();
			}

			if (localRequestDateTracker) {
				namespace = "";
				writeStartElement(null, namespace, "requestDate", xmlWriter);

				if (localRequestDate == null) {
					// write the nil attribute
					throw new org.apache.axis2.databinding.ADBException("requestDate cannot be null!!");
				} else {
					xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localRequestDate));
				}

				xmlWriter.writeEndElement();
			}

			if (localRpnTracker) {
				namespace = "";
				writeStartElement(null, namespace, "rpn", xmlWriter);

				if (localRpn == null) {
					// write the nil attribute
					throw new org.apache.axis2.databinding.ADBException("rpn cannot be null!!");
				} else {
					xmlWriter.writeCharacters(localRpn);
				}

				xmlWriter.writeEndElement();
			}

			if (localSubscriberNumberTracker) {
				namespace = "";
				writeStartElement(null, namespace, "subscriberNumber", xmlWriter);

				if (localSubscriberNumber == null) {
					// write the nil attribute
					throw new org.apache.axis2.databinding.ADBException("subscriberNumber cannot be null!!");
				} else {
					xmlWriter.writeCharacters(localSubscriberNumber);
				}

				xmlWriter.writeEndElement();
			}

			xmlWriter.writeEndElement();
		}

		private static java.lang.String generatePrefix(java.lang.String namespace) {
			if (namespace.equals("http://servicehelpers.esb.avalon.com")) {
				return "ns1";
			}

			return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
		}

		/**
		 * Utility method to write an element start tag.
		 */
		private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
				javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
			java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

			if (writerPrefix != null) {
				xmlWriter.writeStartElement(namespace, localPart);
			} else {
				if (namespace.length() == 0) {
					prefix = "";
				} else if (prefix == null) {
					prefix = generatePrefix(namespace);
				}

				xmlWriter.writeStartElement(prefix, localPart, namespace);
				xmlWriter.writeNamespace(prefix, namespace);
				xmlWriter.setPrefix(prefix, namespace);
			}
		}

		/**
		 * Util method to write an attribute with the ns prefix
		 */
		private void writeAttribute(java.lang.String prefix, java.lang.String namespace, java.lang.String attName, java.lang.String attValue,
				javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
			if (xmlWriter.getPrefix(namespace) == null) {
				xmlWriter.writeNamespace(prefix, namespace);
				xmlWriter.setPrefix(prefix, namespace);
			}

			xmlWriter.writeAttribute(namespace, attName, attValue);
		}

		/**
		 * Util method to write an attribute without the ns prefix
		 */
		private void writeAttribute(java.lang.String namespace, java.lang.String attName, java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter)
				throws javax.xml.stream.XMLStreamException {
			if (namespace.equals("")) {
				xmlWriter.writeAttribute(attName, attValue);
			} else {
				registerPrefix(xmlWriter, namespace);
				xmlWriter.writeAttribute(namespace, attName, attValue);
			}
		}

		/**
		 * Util method to write an attribute without the ns prefix
		 */
		private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName, javax.xml.namespace.QName qname,
				javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
			java.lang.String attributeNamespace = qname.getNamespaceURI();
			java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);

			if (attributePrefix == null) {
				attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
			}

			java.lang.String attributeValue;

			if (attributePrefix.trim().length() > 0) {
				attributeValue = attributePrefix + ":" + qname.getLocalPart();
			} else {
				attributeValue = qname.getLocalPart();
			}

			if (namespace.equals("")) {
				xmlWriter.writeAttribute(attName, attributeValue);
			} else {
				registerPrefix(xmlWriter, namespace);
				xmlWriter.writeAttribute(namespace, attName, attributeValue);
			}
		}

		/**
		 * method to handle Qnames
		 */
		private void writeQName(javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
			java.lang.String namespaceURI = qname.getNamespaceURI();

			if (namespaceURI != null) {
				java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);

				if (prefix == null) {
					prefix = generatePrefix(namespaceURI);
					xmlWriter.writeNamespace(prefix, namespaceURI);
					xmlWriter.setPrefix(prefix, namespaceURI);
				}

				if (prefix.trim().length() > 0) {
					xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
				} else {
					// i.e this is the default namespace
					xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
				}
			} else {
				xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
			}
		}

		private void writeQNames(javax.xml.namespace.QName[] qnames, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
			if (qnames != null) {
				// we have to store this data until last moment since it is not
				// possible to write any
				// namespace data after writing the charactor data
				java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
				java.lang.String namespaceURI = null;
				java.lang.String prefix = null;

				for (int i = 0; i < qnames.length; i++) {
					if (i > 0) {
						stringToWrite.append(" ");
					}

					namespaceURI = qnames[i].getNamespaceURI();

					if (namespaceURI != null) {
						prefix = xmlWriter.getPrefix(namespaceURI);

						if ((prefix == null) || (prefix.length() == 0)) {
							prefix = generatePrefix(namespaceURI);
							xmlWriter.writeNamespace(prefix, namespaceURI);
							xmlWriter.setPrefix(prefix, namespaceURI);
						}

						if (prefix.trim().length() > 0) {
							stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
						} else {
							stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
						}
					} else {
						stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
					}
				}

				xmlWriter.writeCharacters(stringToWrite.toString());
			}
		}

		/**
		 * Register a namespace prefix
		 */
		private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace)
				throws javax.xml.stream.XMLStreamException {
			java.lang.String prefix = xmlWriter.getPrefix(namespace);

			if (prefix == null) {
				prefix = generatePrefix(namespace);

				javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();

				while (true) {
					java.lang.String uri = nsContext.getNamespaceURI(prefix);

					if ((uri == null) || (uri.length() == 0)) {
						break;
					}

					prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
				}

				xmlWriter.writeNamespace(prefix, namespace);
				xmlWriter.setPrefix(prefix, namespace);
			}

			return prefix;
		}

		/**
		 * databinding method to get an XML representation of this object
		 *
		 */
		public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName) throws org.apache.axis2.databinding.ADBException {
			java.util.ArrayList elementList = new java.util.ArrayList();
			java.util.ArrayList attribList = new java.util.ArrayList();

			if (localHealthPlanGroupIdTracker) {
				elementList.add(new javax.xml.namespace.QName("", "healthPlanGroupId"));

				if (localHealthPlanGroupId != null) {
					elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localHealthPlanGroupId));
				} else {
					throw new org.apache.axis2.databinding.ADBException("healthPlanGroupId cannot be null!!");
				}
			}

			if (localHealthPlanIdTracker) {
				elementList.add(new javax.xml.namespace.QName("", "healthPlanId"));

				if (localHealthPlanId != null) {
					elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localHealthPlanId));
				} else {
					throw new org.apache.axis2.databinding.ADBException("healthPlanId cannot be null!!");
				}
			}

			if (localHealthPlanIdTypeTracker) {
				elementList.add(new javax.xml.namespace.QName("", "healthPlanIdType"));

				if (localHealthPlanIdType != null) {
					elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localHealthPlanIdType));
				} else {
					throw new org.apache.axis2.databinding.ADBException("healthPlanIdType cannot be null!!");
				}
			}

			if (localIdCardNumberTracker) {
				elementList.add(new javax.xml.namespace.QName("", "idCardNumber"));

				if (localIdCardNumber != null) {
					elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIdCardNumber));
				} else {
					throw new org.apache.axis2.databinding.ADBException("idCardNumber cannot be null!!");
				}
			}

			if (localPlanCodeTracker) {
				elementList.add(new javax.xml.namespace.QName("", "planCode"));

				if (localPlanCode != null) {
					elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPlanCode));
				} else {
					throw new org.apache.axis2.databinding.ADBException("planCode cannot be null!!");
				}
			}

			if (localProductCodeTracker) {
				elementList.add(new javax.xml.namespace.QName("", "productCode"));

				if (localProductCode != null) {
					elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localProductCode));
				} else {
					throw new org.apache.axis2.databinding.ADBException("productCode cannot be null!!");
				}
			}

			if (localReasonForInquiryTracker) {
				elementList.add(new javax.xml.namespace.QName("", "reasonForInquiry"));

				if (localReasonForInquiry != null) {
					elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localReasonForInquiry));
				} else {
					throw new org.apache.axis2.databinding.ADBException("reasonForInquiry cannot be null!!");
				}
			}

			if (localRequestDateTracker) {
				elementList.add(new javax.xml.namespace.QName("", "requestDate"));

				if (localRequestDate != null) {
					elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localRequestDate));
				} else {
					throw new org.apache.axis2.databinding.ADBException("requestDate cannot be null!!");
				}
			}

			if (localRpnTracker) {
				elementList.add(new javax.xml.namespace.QName("", "rpn"));

				if (localRpn != null) {
					elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localRpn));
				} else {
					throw new org.apache.axis2.databinding.ADBException("rpn cannot be null!!");
				}
			}

			if (localSubscriberNumberTracker) {
				elementList.add(new javax.xml.namespace.QName("", "subscriberNumber"));

				if (localSubscriberNumber != null) {
					elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSubscriberNumber));
				} else {
					throw new org.apache.axis2.databinding.ADBException("subscriberNumber cannot be null!!");
				}
			}

			return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
		}

		/**
		 * Factory class that keeps the parse method
		 */
		public static class Factory {
			/**
			 * static method to create the object Precondition: If this object is an element, the current or next start element starts this object and any
			 * intervening reader events are ignorable If this object is not an element, it is a complex type and the reader is at the event just after the
			 * outer start element Postcondition: If this object is an element, the reader is positioned at its end element If this object is a complex type,
			 * the reader is positioned at the end element of its outer element
			 */
			public static SearchCriteria parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception {
				SearchCriteria object = new SearchCriteria();

				int event;
				java.lang.String nillableValue = null;
				java.lang.String prefix = "";
				java.lang.String namespaceuri = "";

				try {
					while (!reader.isStartElement() && !reader.isEndElement())
						reader.next();

					if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
						java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type");

						if (fullTypeName != null) {
							java.lang.String nsPrefix = null;

							if (fullTypeName.indexOf(":") > -1) {
								nsPrefix = fullTypeName.substring(0, fullTypeName.indexOf(":"));
							}

							nsPrefix = (nsPrefix == null) ? "" : nsPrefix;

							java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":") + 1);

							if (!"searchCriteria".equals(type)) {
								// find namespace for the prefix
								java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);

								return (SearchCriteria) ExtensionMapper.getTypeObject(nsUri, type, reader);
							}
						}
					}

					// Note all attributes that were handled. Used to differ
					// normal attributes
					// from anyAttributes.
					java.util.Vector handledAttributes = new java.util.Vector();

					reader.next();

					while (!reader.isStartElement() && !reader.isEndElement())
						reader.next();

					if (reader.isStartElement() && new javax.xml.namespace.QName("", "healthPlanGroupId").equals(reader.getName())) {
						nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");

						if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
							throw new org.apache.axis2.databinding.ADBException("The element: " + "healthPlanGroupId" + "  cannot be null");
						}

						java.lang.String content = reader.getElementText();

						object.setHealthPlanGroupId(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

						reader.next();
					} // End of if for expected property start element

					else {
					}

					while (!reader.isStartElement() && !reader.isEndElement())
						reader.next();

					if (reader.isStartElement() && new javax.xml.namespace.QName("", "healthPlanId").equals(reader.getName())) {
						nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");

						if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
							throw new org.apache.axis2.databinding.ADBException("The element: " + "healthPlanId" + "  cannot be null");
						}

						java.lang.String content = reader.getElementText();

						object.setHealthPlanId(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

						reader.next();
					} // End of if for expected property start element

					else {
					}

					while (!reader.isStartElement() && !reader.isEndElement())
						reader.next();

					if (reader.isStartElement() && new javax.xml.namespace.QName("", "healthPlanIdType").equals(reader.getName())) {
						nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");

						if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
							throw new org.apache.axis2.databinding.ADBException("The element: " + "healthPlanIdType" + "  cannot be null");
						}

						java.lang.String content = reader.getElementText();

						object.setHealthPlanIdType(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

						reader.next();
					} // End of if for expected property start element

					else {
					}

					while (!reader.isStartElement() && !reader.isEndElement())
						reader.next();

					if (reader.isStartElement() && new javax.xml.namespace.QName("", "idCardNumber").equals(reader.getName())) {
						nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");

						if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
							throw new org.apache.axis2.databinding.ADBException("The element: " + "idCardNumber" + "  cannot be null");
						}

						java.lang.String content = reader.getElementText();

						object.setIdCardNumber(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

						reader.next();
					} // End of if for expected property start element

					else {
					}

					while (!reader.isStartElement() && !reader.isEndElement())
						reader.next();

					if (reader.isStartElement() && new javax.xml.namespace.QName("", "planCode").equals(reader.getName())) {
						nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");

						if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
							throw new org.apache.axis2.databinding.ADBException("The element: " + "planCode" + "  cannot be null");
						}

						java.lang.String content = reader.getElementText();

						object.setPlanCode(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

						reader.next();
					} // End of if for expected property start element

					else {
					}

					while (!reader.isStartElement() && !reader.isEndElement())
						reader.next();

					if (reader.isStartElement() && new javax.xml.namespace.QName("", "productCode").equals(reader.getName())) {
						nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");

						if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
							throw new org.apache.axis2.databinding.ADBException("The element: " + "productCode" + "  cannot be null");
						}

						java.lang.String content = reader.getElementText();

						object.setProductCode(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

						reader.next();
					} // End of if for expected property start element

					else {
					}

					while (!reader.isStartElement() && !reader.isEndElement())
						reader.next();

					if (reader.isStartElement() && new javax.xml.namespace.QName("", "reasonForInquiry").equals(reader.getName())) {
						nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");

						if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
							throw new org.apache.axis2.databinding.ADBException("The element: " + "reasonForInquiry" + "  cannot be null");
						}

						java.lang.String content = reader.getElementText();

						object.setReasonForInquiry(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

						reader.next();
					} // End of if for expected property start element

					else {
					}

					while (!reader.isStartElement() && !reader.isEndElement())
						reader.next();

					if (reader.isStartElement() && new javax.xml.namespace.QName("", "requestDate").equals(reader.getName())) {
						nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");

						if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
							throw new org.apache.axis2.databinding.ADBException("The element: " + "requestDate" + "  cannot be null");
						}

						java.lang.String content = reader.getElementText();

						object.setRequestDate(org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));

						reader.next();
					} // End of if for expected property start element

					else {
					}

					while (!reader.isStartElement() && !reader.isEndElement())
						reader.next();

					if (reader.isStartElement() && new javax.xml.namespace.QName("", "rpn").equals(reader.getName())) {
						nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");

						if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
							throw new org.apache.axis2.databinding.ADBException("The element: " + "rpn" + "  cannot be null");
						}

						java.lang.String content = reader.getElementText();

						object.setRpn(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

						reader.next();
					} // End of if for expected property start element

					else {
					}

					while (!reader.isStartElement() && !reader.isEndElement())
						reader.next();

					if (reader.isStartElement() && new javax.xml.namespace.QName("", "subscriberNumber").equals(reader.getName())) {
						nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");

						if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
							throw new org.apache.axis2.databinding.ADBException("The element: " + "subscriberNumber" + "  cannot be null");
						}

						java.lang.String content = reader.getElementText();

						object.setSubscriberNumber(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

						reader.next();
					} // End of if for expected property start element

					else {
					}

					while (!reader.isStartElement() && !reader.isEndElement())
						reader.next();

					if (reader.isStartElement()) {
						// A start element we are not expecting indicates a
						// trailing invalid property
						throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
					}
				} catch (javax.xml.stream.XMLStreamException e) {
					throw new java.lang.Exception(e);
				}

				return object;
			}
		} // end of factory class
	}

	public static class ExtensionMapper {
		public static java.lang.Object getTypeObject(java.lang.String namespaceURI, java.lang.String typeName, javax.xml.stream.XMLStreamReader reader)
				throws java.lang.Exception {
			if ("http://servicehelpers.esb.avalon.com".equals(namespaceURI) && "CoverageList".equals(typeName)) {
				return CoverageList.Factory.parse(reader);
			}

			if ("http://servicehelpers.esb.avalon.com".equals(namespaceURI) && "memberDemographics".equals(typeName)) {
				return MemberDemographics.Factory.parse(reader);
			}

			if ("http://servicehelpers.esb.avalon.com".equals(namespaceURI) && "MemberList".equals(typeName)) {
				return MemberList.Factory.parse(reader);
			}

			if ("http://servicehelpers.esb.avalon.com".equals(namespaceURI) && "Coverage".equals(typeName)) {
				return Coverage.Factory.parse(reader);
			}

			if ("http://servicehelpers.esb.avalon.com".equals(namespaceURI) && "ContactData".equals(typeName)) {
				return ContactData.Factory.parse(reader);
			}

			if ("http://servicehelpers.esb.avalon.com".equals(namespaceURI) && "Address".equals(typeName)) {
				return Address.Factory.parse(reader);
			}

			if ("http://servicehelpers.esb.avalon.com".equals(namespaceURI) && "MarketingPackage".equals(typeName)) {
				return MarketingPackage.Factory.parse(reader);
			}

			if ("http://servicehelpers.esb.avalon.com".equals(namespaceURI) && "MemberDataRequest".equals(typeName)) {
				return MemberDataRequest.Factory.parse(reader);
			}

			if ("http://servicehelpers.esb.avalon.com".equals(namespaceURI) && "EnrollmentData".equals(typeName)) {
				return EnrollmentData.Factory.parse(reader);
			}

			if ("http://servicehelpers.esb.avalon.com".equals(namespaceURI) && "moreDataOptions".equals(typeName)) {
				return MoreDataOptions.Factory.parse(reader);
			}

			if ("http://servicehelpers.esb.avalon.com".equals(namespaceURI) && "memberDataResponse".equals(typeName)) {
				return MemberDataResponse.Factory.parse(reader);
			}

			if ("http://servicehelpers.esb.avalon.com".equals(namespaceURI) && "Member".equals(typeName)) {
				return Member.Factory.parse(reader);
			}

			if ("http://servicehelpers.esb.avalon.com".equals(namespaceURI) && "searchCriteria".equals(typeName)) {
				return SearchCriteria.Factory.parse(reader);
			}

			if ("http://servicehelpers.esb.avalon.com".equals(namespaceURI) && "MemberDemographicsOutput".equals(typeName)) {
				return MemberDemographicsOutput.Factory.parse(reader);
			}

			if ("http://servicehelpers.esb.avalon.com".equals(namespaceURI) && "BusinessSectorSegment".equals(typeName)) {
				return BusinessSectorSegment.Factory.parse(reader);
			}

			throw new org.apache.axis2.databinding.ADBException("Unsupported type " + namespaceURI + " " + typeName);
		}
	}

	public static class Member implements org.apache.axis2.databinding.ADBBean {
		/*
		 * This type was generated from the piece of schema that had name = Member Namespace URI = http://servicehelpers.esb.avalon.com Namespace Prefix = ns1
		 */

		/**
		 * field for HealthPlanGroupId
		 */
		protected java.lang.String localHealthPlanGroupId;

		/**
		 * field for LabBenefitId
		 */
		protected java.lang.String localLabBenefitId;

		/**
		 * field for LabBenefitIdDescription
		 */
		protected java.lang.String localLabBenefitIdDescription;

		/**
		 * field for FirstName
		 */
		protected java.lang.String localFirstName;

		/**
		 * field for MiddleName
		 */
		protected java.lang.String localMiddleName;

		/**
		 * field for LastName
		 */
		protected java.lang.String localLastName;

		/**
		 * field for Suffix
		 */
		protected java.lang.String localSuffix;

		/**
		 * field for DateOfBirth
		 */
		protected java.util.Date localDateOfBirth;

		/**
		 * field for GenderCode
		 */
		protected java.lang.String localGenderCode;

		/**
		 * field for GenderDescription
		 */
		protected java.lang.String localGenderDescription;

		/**
		 * field for PatientId
		 */
		protected java.lang.String localPatientId;

		/**
		 * field for MemberId
		 */
		protected java.lang.String localMemberId;

		/**
		 * field for UniqueMemberId
		 */
		protected java.lang.String localUniqueMemberId;

		/**
		 * field for RelationshipCodeDescription
		 */
		protected java.lang.String localRelationshipCodeDescription;

		/**
		 * field for DependentVerificationIndicator
		 */
		protected boolean localDependentVerificationIndicator;

		/**
		 * field for PrivacyIndicator
		 */
		protected boolean localPrivacyIndicator;

		/**
		 * field for BusinessSectorSegment
		 */
		protected BusinessSectorSegment localBusinessSectorSegment;

		/**
		 * Auto generated getter method
		 * 
		 * @return java.lang.String
		 */
		public java.lang.String getHealthPlanGroupId() {
			return localHealthPlanGroupId;
		}

		/**
		 * Auto generated setter method
		 * 
		 * @param param
		 *            HealthPlanGroupId
		 */
		public void setHealthPlanGroupId(java.lang.String param) {
			this.localHealthPlanGroupId = param;
		}

		/**
		 * Auto generated getter method
		 * 
		 * @return java.lang.String
		 */
		public java.lang.String getLabBenefitId() {
			return localLabBenefitId;
		}

		/**
		 * Auto generated setter method
		 * 
		 * @param param
		 *            LabBenefitId
		 */
		public void setLabBenefitId(java.lang.String param) {
			this.localLabBenefitId = param;
		}

		/**
		 * Auto generated getter method
		 * 
		 * @return java.lang.String
		 */
		public java.lang.String getLabBenefitIdDescription() {
			return localLabBenefitIdDescription;
		}

		/**
		 * Auto generated setter method
		 * 
		 * @param param
		 *            LabBenefitIdDescription
		 */
		public void setLabBenefitIdDescription(java.lang.String param) {
			this.localLabBenefitIdDescription = param;
		}

		/**
		 * Auto generated getter method
		 * 
		 * @return java.lang.String
		 */
		public java.lang.String getFirstName() {
			return localFirstName;
		}

		/**
		 * Auto generated setter method
		 * 
		 * @param param
		 *            FirstName
		 */
		public void setFirstName(java.lang.String param) {
			this.localFirstName = param;
		}

		/**
		 * Auto generated getter method
		 * 
		 * @return java.lang.String
		 */
		public java.lang.String getMiddleName() {
			return localMiddleName;
		}

		/**
		 * Auto generated setter method
		 * 
		 * @param param
		 *            MiddleName
		 */
		public void setMiddleName(java.lang.String param) {
			this.localMiddleName = param;
		}

		/**
		 * Auto generated getter method
		 * 
		 * @return java.lang.String
		 */
		public java.lang.String getLastName() {
			return localLastName;
		}

		/**
		 * Auto generated setter method
		 * 
		 * @param param
		 *            LastName
		 */
		public void setLastName(java.lang.String param) {
			this.localLastName = param;
		}

		/**
		 * Auto generated getter method
		 * 
		 * @return java.lang.String
		 */
		public java.lang.String getSuffix() {
			return localSuffix;
		}

		/**
		 * Auto generated setter method
		 * 
		 * @param param
		 *            Suffix
		 */
		public void setSuffix(java.lang.String param) {
			this.localSuffix = param;
		}

		/**
		 * Auto generated getter method
		 * 
		 * @return java.util.Date
		 */
		public java.util.Date getDateOfBirth() {
			return localDateOfBirth;
		}

		/**
		 * Auto generated setter method
		 * 
		 * @param param
		 *            DateOfBirth
		 */
		public void setDateOfBirth(java.util.Date param) {
			this.localDateOfBirth = param;
		}

		/**
		 * Auto generated getter method
		 * 
		 * @return java.lang.String
		 */
		public java.lang.String getGenderCode() {
			return localGenderCode;
		}

		/**
		 * Auto generated setter method
		 * 
		 * @param param
		 *            GenderCode
		 */
		public void setGenderCode(java.lang.String param) {
			this.localGenderCode = param;
		}

		/**
		 * Auto generated getter method
		 * 
		 * @return java.lang.String
		 */
		public java.lang.String getGenderDescription() {
			return localGenderDescription;
		}

		/**
		 * Auto generated setter method
		 * 
		 * @param param
		 *            GenderDescription
		 */
		public void setGenderDescription(java.lang.String param) {
			this.localGenderDescription = param;
		}

		/**
		 * Auto generated getter method
		 * 
		 * @return java.lang.String
		 */
		public java.lang.String getPatientId() {
			return localPatientId;
		}

		/**
		 * Auto generated setter method
		 * 
		 * @param param
		 *            PatientId
		 */
		public void setPatientId(java.lang.String param) {
			this.localPatientId = param;
		}

		/**
		 * Auto generated getter method
		 * 
		 * @return java.lang.String
		 */
		public java.lang.String getMemberId() {
			return localMemberId;
		}

		/**
		 * Auto generated setter method
		 * 
		 * @param param
		 *            MemberId
		 */
		public void setMemberId(java.lang.String param) {
			this.localMemberId = param;
		}

		/**
		 * Auto generated getter method
		 * 
		 * @return java.lang.String
		 */
		public java.lang.String getUniqueMemberId() {
			return localUniqueMemberId;
		}

		/**
		 * Auto generated setter method
		 * 
		 * @param param
		 *            UniqueMemberId
		 */
		public void setUniqueMemberId(java.lang.String param) {
			this.localUniqueMemberId = param;
		}

		/**
		 * Auto generated getter method
		 * 
		 * @return java.lang.String
		 */
		public java.lang.String getRelationshipCodeDescription() {
			return localRelationshipCodeDescription;
		}

		/**
		 * Auto generated setter method
		 * 
		 * @param param
		 *            RelationshipCodeDescription
		 */
		public void setRelationshipCodeDescription(java.lang.String param) {
			this.localRelationshipCodeDescription = param;
		}

		/**
		 * Auto generated getter method
		 * 
		 * @return boolean
		 */
		public boolean getDependentVerificationIndicator() {
			return localDependentVerificationIndicator;
		}

		/**
		 * Auto generated setter method
		 * 
		 * @param param
		 *            DependentVerificationIndicator
		 */
		public void setDependentVerificationIndicator(boolean param) {
			this.localDependentVerificationIndicator = param;
		}

		/**
		 * Auto generated getter method
		 * 
		 * @return boolean
		 */
		public boolean getPrivacyIndicator() {
			return localPrivacyIndicator;
		}

		/**
		 * Auto generated setter method
		 * 
		 * @param param
		 *            PrivacyIndicator
		 */
		public void setPrivacyIndicator(boolean param) {
			this.localPrivacyIndicator = param;
		}

		/**
		 * Auto generated getter method
		 * 
		 * @return BusinessSectorSegment
		 */
		public BusinessSectorSegment getBusinessSectorSegment() {
			return localBusinessSectorSegment;
		}

		/**
		 * Auto generated setter method
		 * 
		 * @param param
		 *            BusinessSectorSegment
		 */
		public void setBusinessSectorSegment(BusinessSectorSegment param) {
			this.localBusinessSectorSegment = param;
		}

		/**
		 *
		 * @param parentQName
		 * @param factory
		 * @return org.apache.axiom.om.OMElement
		 */
		public org.apache.axiom.om.OMElement getOMElement(final javax.xml.namespace.QName parentQName, final org.apache.axiom.om.OMFactory factory)
				throws org.apache.axis2.databinding.ADBException {
			org.apache.axiom.om.OMDataSource dataSource = new org.apache.axis2.databinding.ADBDataSource(this, parentQName);

			return factory.createOMElement(dataSource, parentQName);
		}

		public void serialize(final javax.xml.namespace.QName parentQName, javax.xml.stream.XMLStreamWriter xmlWriter)
				throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {
			serialize(parentQName, xmlWriter, false);
		}

		public void serialize(final javax.xml.namespace.QName parentQName, javax.xml.stream.XMLStreamWriter xmlWriter, boolean serializeType)
				throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {
			java.lang.String prefix = null;
			java.lang.String namespace = null;

			prefix = parentQName.getPrefix();
			namespace = parentQName.getNamespaceURI();
			writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);

			if (serializeType) {
				java.lang.String namespacePrefix = registerPrefix(xmlWriter, "http://servicehelpers.esb.avalon.com");

				if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)) {
					writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", namespacePrefix + ":Member", xmlWriter);
				} else {
					writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", "Member", xmlWriter);
				}
			}

			namespace = "";
			writeStartElement(null, namespace, "healthPlanGroupId", xmlWriter);

			if (localHealthPlanGroupId == null) {
				// write the nil attribute
				throw new org.apache.axis2.databinding.ADBException("healthPlanGroupId cannot be null!!");
			} else {
				xmlWriter.writeCharacters(localHealthPlanGroupId);
			}

			xmlWriter.writeEndElement();

			namespace = "";
			writeStartElement(null, namespace, "labBenefitId", xmlWriter);

			if (localLabBenefitId == null) {
				// write the nil attribute
				throw new org.apache.axis2.databinding.ADBException("labBenefitId cannot be null!!");
			} else {
				xmlWriter.writeCharacters(localLabBenefitId);
			}

			xmlWriter.writeEndElement();

			namespace = "";
			writeStartElement(null, namespace, "labBenefitIdDescription", xmlWriter);

			if (localLabBenefitIdDescription == null) {
				// write the nil attribute
				throw new org.apache.axis2.databinding.ADBException("labBenefitIdDescription cannot be null!!");
			} else {
				xmlWriter.writeCharacters(localLabBenefitIdDescription);
			}

			xmlWriter.writeEndElement();

			namespace = "";
			writeStartElement(null, namespace, "firstName", xmlWriter);

			if (localFirstName == null) {
				// write the nil attribute
				throw new org.apache.axis2.databinding.ADBException("firstName cannot be null!!");
			} else {
				xmlWriter.writeCharacters(localFirstName);
			}

			xmlWriter.writeEndElement();

			namespace = "";
			writeStartElement(null, namespace, "middleName", xmlWriter);

			if (localMiddleName == null) {
				// write the nil attribute
				throw new org.apache.axis2.databinding.ADBException("middleName cannot be null!!");
			} else {
				xmlWriter.writeCharacters(localMiddleName);
			}

			xmlWriter.writeEndElement();

			namespace = "";
			writeStartElement(null, namespace, "lastName", xmlWriter);

			if (localLastName == null) {
				// write the nil attribute
				throw new org.apache.axis2.databinding.ADBException("lastName cannot be null!!");
			} else {
				xmlWriter.writeCharacters(localLastName);
			}

			xmlWriter.writeEndElement();

			namespace = "";
			writeStartElement(null, namespace, "suffix", xmlWriter);

			if (localSuffix == null) {
				// write the nil attribute
				throw new org.apache.axis2.databinding.ADBException("suffix cannot be null!!");
			} else {
				xmlWriter.writeCharacters(localSuffix);
			}

			xmlWriter.writeEndElement();

			namespace = "";
			writeStartElement(null, namespace, "dateOfBirth", xmlWriter);

			if (localDateOfBirth == null) {
				// write the nil attribute
				throw new org.apache.axis2.databinding.ADBException("dateOfBirth cannot be null!!");
			} else {
				xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDateOfBirth));
			}

			xmlWriter.writeEndElement();

			namespace = "";
			writeStartElement(null, namespace, "genderCode", xmlWriter);

			if (localGenderCode == null) {
				// write the nil attribute
				throw new org.apache.axis2.databinding.ADBException("genderCode cannot be null!!");
			} else {
				xmlWriter.writeCharacters(localGenderCode);
			}

			xmlWriter.writeEndElement();

			namespace = "";
			writeStartElement(null, namespace, "genderDescription", xmlWriter);

			if (localGenderDescription == null) {
				// write the nil attribute
				throw new org.apache.axis2.databinding.ADBException("genderDescription cannot be null!!");
			} else {
				xmlWriter.writeCharacters(localGenderDescription);
			}

			xmlWriter.writeEndElement();

			namespace = "";
			writeStartElement(null, namespace, "patientId", xmlWriter);

			if (localPatientId == null) {
				// write the nil attribute
				throw new org.apache.axis2.databinding.ADBException("patientId cannot be null!!");
			} else {
				xmlWriter.writeCharacters(localPatientId);
			}

			xmlWriter.writeEndElement();

			namespace = "";
			writeStartElement(null, namespace, "memberId", xmlWriter);

			if (localMemberId == null) {
				// write the nil attribute
				throw new org.apache.axis2.databinding.ADBException("memberId cannot be null!!");
			} else {
				xmlWriter.writeCharacters(localMemberId);
			}

			xmlWriter.writeEndElement();

			namespace = "";
			writeStartElement(null, namespace, "uniqueMemberId", xmlWriter);

			if (localUniqueMemberId == null) {
				// write the nil attribute
				throw new org.apache.axis2.databinding.ADBException("uniqueMemberId cannot be null!!");
			} else {
				xmlWriter.writeCharacters(localUniqueMemberId);
			}

			xmlWriter.writeEndElement();

			namespace = "";
			writeStartElement(null, namespace, "relationshipCodeDescription", xmlWriter);

			if (localRelationshipCodeDescription == null) {
				// write the nil attribute
				throw new org.apache.axis2.databinding.ADBException("relationshipCodeDescription cannot be null!!");
			} else {
				xmlWriter.writeCharacters(localRelationshipCodeDescription);
			}

			xmlWriter.writeEndElement();

			namespace = "";
			writeStartElement(null, namespace, "dependentVerificationIndicator", xmlWriter);

			if (false) {
				throw new org.apache.axis2.databinding.ADBException("dependentVerificationIndicator cannot be null!!");
			} else {
				xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDependentVerificationIndicator));
			}

			xmlWriter.writeEndElement();

			namespace = "";
			writeStartElement(null, namespace, "privacyIndicator", xmlWriter);

			if (false) {
				throw new org.apache.axis2.databinding.ADBException("privacyIndicator cannot be null!!");
			} else {
				xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPrivacyIndicator));
			}

			xmlWriter.writeEndElement();

			if (localBusinessSectorSegment == null) {
				throw new org.apache.axis2.databinding.ADBException("businessSectorSegment cannot be null!!");
			}

			localBusinessSectorSegment.serialize(new javax.xml.namespace.QName("", "businessSectorSegment"), xmlWriter);

			xmlWriter.writeEndElement();
		}

		private static java.lang.String generatePrefix(java.lang.String namespace) {
			if (namespace.equals("http://servicehelpers.esb.avalon.com")) {
				return "ns1";
			}

			return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
		}

		/**
		 * Utility method to write an element start tag.
		 */
		private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
				javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
			java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

			if (writerPrefix != null) {
				xmlWriter.writeStartElement(namespace, localPart);
			} else {
				if (namespace.length() == 0) {
					prefix = "";
				} else if (prefix == null) {
					prefix = generatePrefix(namespace);
				}

				xmlWriter.writeStartElement(prefix, localPart, namespace);
				xmlWriter.writeNamespace(prefix, namespace);
				xmlWriter.setPrefix(prefix, namespace);
			}
		}

		/**
		 * Util method to write an attribute with the ns prefix
		 */
		private void writeAttribute(java.lang.String prefix, java.lang.String namespace, java.lang.String attName, java.lang.String attValue,
				javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
			if (xmlWriter.getPrefix(namespace) == null) {
				xmlWriter.writeNamespace(prefix, namespace);
				xmlWriter.setPrefix(prefix, namespace);
			}

			xmlWriter.writeAttribute(namespace, attName, attValue);
		}

		/**
		 * Util method to write an attribute without the ns prefix
		 */
		private void writeAttribute(java.lang.String namespace, java.lang.String attName, java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter)
				throws javax.xml.stream.XMLStreamException {
			if (namespace.equals("")) {
				xmlWriter.writeAttribute(attName, attValue);
			} else {
				registerPrefix(xmlWriter, namespace);
				xmlWriter.writeAttribute(namespace, attName, attValue);
			}
		}

		/**
		 * Util method to write an attribute without the ns prefix
		 */
		private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName, javax.xml.namespace.QName qname,
				javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
			java.lang.String attributeNamespace = qname.getNamespaceURI();
			java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);

			if (attributePrefix == null) {
				attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
			}

			java.lang.String attributeValue;

			if (attributePrefix.trim().length() > 0) {
				attributeValue = attributePrefix + ":" + qname.getLocalPart();
			} else {
				attributeValue = qname.getLocalPart();
			}

			if (namespace.equals("")) {
				xmlWriter.writeAttribute(attName, attributeValue);
			} else {
				registerPrefix(xmlWriter, namespace);
				xmlWriter.writeAttribute(namespace, attName, attributeValue);
			}
		}

		/**
		 * method to handle Qnames
		 */
		private void writeQName(javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
			java.lang.String namespaceURI = qname.getNamespaceURI();

			if (namespaceURI != null) {
				java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);

				if (prefix == null) {
					prefix = generatePrefix(namespaceURI);
					xmlWriter.writeNamespace(prefix, namespaceURI);
					xmlWriter.setPrefix(prefix, namespaceURI);
				}

				if (prefix.trim().length() > 0) {
					xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
				} else {
					// i.e this is the default namespace
					xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
				}
			} else {
				xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
			}
		}

		private void writeQNames(javax.xml.namespace.QName[] qnames, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
			if (qnames != null) {
				// we have to store this data until last moment since it is not
				// possible to write any
				// namespace data after writing the charactor data
				java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
				java.lang.String namespaceURI = null;
				java.lang.String prefix = null;

				for (int i = 0; i < qnames.length; i++) {
					if (i > 0) {
						stringToWrite.append(" ");
					}

					namespaceURI = qnames[i].getNamespaceURI();

					if (namespaceURI != null) {
						prefix = xmlWriter.getPrefix(namespaceURI);

						if ((prefix == null) || (prefix.length() == 0)) {
							prefix = generatePrefix(namespaceURI);
							xmlWriter.writeNamespace(prefix, namespaceURI);
							xmlWriter.setPrefix(prefix, namespaceURI);
						}

						if (prefix.trim().length() > 0) {
							stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
						} else {
							stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
						}
					} else {
						stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
					}
				}

				xmlWriter.writeCharacters(stringToWrite.toString());
			}
		}

		/**
		 * Register a namespace prefix
		 */
		private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace)
				throws javax.xml.stream.XMLStreamException {
			java.lang.String prefix = xmlWriter.getPrefix(namespace);

			if (prefix == null) {
				prefix = generatePrefix(namespace);

				javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();

				while (true) {
					java.lang.String uri = nsContext.getNamespaceURI(prefix);

					if ((uri == null) || (uri.length() == 0)) {
						break;
					}

					prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
				}

				xmlWriter.writeNamespace(prefix, namespace);
				xmlWriter.setPrefix(prefix, namespace);
			}

			return prefix;
		}

		/**
		 * databinding method to get an XML representation of this object
		 *
		 */
		public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName) throws org.apache.axis2.databinding.ADBException {
			java.util.ArrayList elementList = new java.util.ArrayList();
			java.util.ArrayList attribList = new java.util.ArrayList();

			elementList.add(new javax.xml.namespace.QName("", "healthPlanGroupId"));

			if (localHealthPlanGroupId != null) {
				elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localHealthPlanGroupId));
			} else {
				throw new org.apache.axis2.databinding.ADBException("healthPlanGroupId cannot be null!!");
			}

			elementList.add(new javax.xml.namespace.QName("", "labBenefitId"));

			if (localLabBenefitId != null) {
				elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLabBenefitId));
			} else {
				throw new org.apache.axis2.databinding.ADBException("labBenefitId cannot be null!!");
			}

			elementList.add(new javax.xml.namespace.QName("", "labBenefitIdDescription"));

			if (localLabBenefitIdDescription != null) {
				elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLabBenefitIdDescription));
			} else {
				throw new org.apache.axis2.databinding.ADBException("labBenefitIdDescription cannot be null!!");
			}

			elementList.add(new javax.xml.namespace.QName("", "firstName"));

			if (localFirstName != null) {
				elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localFirstName));
			} else {
				throw new org.apache.axis2.databinding.ADBException("firstName cannot be null!!");
			}

			elementList.add(new javax.xml.namespace.QName("", "middleName"));

			if (localMiddleName != null) {
				elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localMiddleName));
			} else {
				throw new org.apache.axis2.databinding.ADBException("middleName cannot be null!!");
			}

			elementList.add(new javax.xml.namespace.QName("", "lastName"));

			if (localLastName != null) {
				elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLastName));
			} else {
				throw new org.apache.axis2.databinding.ADBException("lastName cannot be null!!");
			}

			elementList.add(new javax.xml.namespace.QName("", "suffix"));

			if (localSuffix != null) {
				elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSuffix));
			} else {
				throw new org.apache.axis2.databinding.ADBException("suffix cannot be null!!");
			}

			elementList.add(new javax.xml.namespace.QName("", "dateOfBirth"));

			if (localDateOfBirth != null) {
				elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDateOfBirth));
			} else {
				throw new org.apache.axis2.databinding.ADBException("dateOfBirth cannot be null!!");
			}

			elementList.add(new javax.xml.namespace.QName("", "genderCode"));

			if (localGenderCode != null) {
				elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localGenderCode));
			} else {
				throw new org.apache.axis2.databinding.ADBException("genderCode cannot be null!!");
			}

			elementList.add(new javax.xml.namespace.QName("", "genderDescription"));

			if (localGenderDescription != null) {
				elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localGenderDescription));
			} else {
				throw new org.apache.axis2.databinding.ADBException("genderDescription cannot be null!!");
			}

			elementList.add(new javax.xml.namespace.QName("", "patientId"));

			if (localPatientId != null) {
				elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPatientId));
			} else {
				throw new org.apache.axis2.databinding.ADBException("patientId cannot be null!!");
			}

			elementList.add(new javax.xml.namespace.QName("", "memberId"));

			if (localMemberId != null) {
				elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localMemberId));
			} else {
				throw new org.apache.axis2.databinding.ADBException("memberId cannot be null!!");
			}

			elementList.add(new javax.xml.namespace.QName("", "uniqueMemberId"));

			if (localUniqueMemberId != null) {
				elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localUniqueMemberId));
			} else {
				throw new org.apache.axis2.databinding.ADBException("uniqueMemberId cannot be null!!");
			}

			elementList.add(new javax.xml.namespace.QName("", "relationshipCodeDescription"));

			if (localRelationshipCodeDescription != null) {
				elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localRelationshipCodeDescription));
			} else {
				throw new org.apache.axis2.databinding.ADBException("relationshipCodeDescription cannot be null!!");
			}

			elementList.add(new javax.xml.namespace.QName("", "dependentVerificationIndicator"));

			elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDependentVerificationIndicator));

			elementList.add(new javax.xml.namespace.QName("", "privacyIndicator"));

			elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPrivacyIndicator));

			elementList.add(new javax.xml.namespace.QName("", "businessSectorSegment"));

			if (localBusinessSectorSegment == null) {
				throw new org.apache.axis2.databinding.ADBException("businessSectorSegment cannot be null!!");
			}

			elementList.add(localBusinessSectorSegment);

			return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
		}

		/**
		 * Factory class that keeps the parse method
		 */
		public static class Factory {
			/**
			 * static method to create the object Precondition: If this object is an element, the current or next start element starts this object and any
			 * intervening reader events are ignorable If this object is not an element, it is a complex type and the reader is at the event just after the
			 * outer start element Postcondition: If this object is an element, the reader is positioned at its end element If this object is a complex type,
			 * the reader is positioned at the end element of its outer element
			 */
			public static Member parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception {
				Member object = new Member();

				int event;
				java.lang.String nillableValue = null;
				java.lang.String prefix = "";
				java.lang.String namespaceuri = "";

				try {
					while (!reader.isStartElement() && !reader.isEndElement())
						reader.next();

					if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
						java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type");

						if (fullTypeName != null) {
							java.lang.String nsPrefix = null;

							if (fullTypeName.indexOf(":") > -1) {
								nsPrefix = fullTypeName.substring(0, fullTypeName.indexOf(":"));
							}

							nsPrefix = (nsPrefix == null) ? "" : nsPrefix;

							java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":") + 1);

							if (!"Member".equals(type)) {
								// find namespace for the prefix
								java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);

								return (Member) ExtensionMapper.getTypeObject(nsUri, type, reader);
							}
						}
					}

					// Note all attributes that were handled. Used to differ
					// normal attributes
					// from anyAttributes.
					java.util.Vector handledAttributes = new java.util.Vector();

					reader.next();

					while (!reader.isStartElement() && !reader.isEndElement())
						reader.next();

					if (reader.isStartElement() && new javax.xml.namespace.QName("", "healthPlanGroupId").equals(reader.getName())) {
						nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");

						if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
							throw new org.apache.axis2.databinding.ADBException("The element: " + "healthPlanGroupId" + "  cannot be null");
						}

						java.lang.String content = reader.getElementText();

						object.setHealthPlanGroupId(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

						reader.next();
					} // End of if for expected property start element

					else {
						// A start element we are not expecting indicates an
						// invalid parameter was passed
						throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
					}

					while (!reader.isStartElement() && !reader.isEndElement())
						reader.next();

					if (reader.isStartElement() && new javax.xml.namespace.QName("", "labBenefitId").equals(reader.getName())) {
						nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");

						if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
							throw new org.apache.axis2.databinding.ADBException("The element: " + "labBenefitId" + "  cannot be null");
						}

						java.lang.String content = reader.getElementText();

						object.setLabBenefitId(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

						reader.next();
					} // End of if for expected property start element

					else {
						// A start element we are not expecting indicates an
						// invalid parameter was passed
						throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
					}

					while (!reader.isStartElement() && !reader.isEndElement())
						reader.next();

					if (reader.isStartElement() && new javax.xml.namespace.QName("", "labBenefitIdDescription").equals(reader.getName())) {
						nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");

						if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
							throw new org.apache.axis2.databinding.ADBException("The element: " + "labBenefitIdDescription" + "  cannot be null");
						}

						java.lang.String content = reader.getElementText();

						object.setLabBenefitIdDescription(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

						reader.next();
					} // End of if for expected property start element

					else {
						// A start element we are not expecting indicates an
						// invalid parameter was passed
						throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
					}

					while (!reader.isStartElement() && !reader.isEndElement())
						reader.next();

					if (reader.isStartElement() && new javax.xml.namespace.QName("", "firstName").equals(reader.getName())) {
						nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");

						if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
							throw new org.apache.axis2.databinding.ADBException("The element: " + "firstName" + "  cannot be null");
						}

						java.lang.String content = reader.getElementText();

						object.setFirstName(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

						reader.next();
					} // End of if for expected property start element

					else {
						// A start element we are not expecting indicates an
						// invalid parameter was passed
						throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
					}

					while (!reader.isStartElement() && !reader.isEndElement())
						reader.next();

					if (reader.isStartElement() && new javax.xml.namespace.QName("", "middleName").equals(reader.getName())) {
						nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");

						if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
							throw new org.apache.axis2.databinding.ADBException("The element: " + "middleName" + "  cannot be null");
						}

						java.lang.String content = reader.getElementText();

						object.setMiddleName(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

						reader.next();
					} // End of if for expected property start element

					else {
						// A start element we are not expecting indicates an
						// invalid parameter was passed
						throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
					}

					while (!reader.isStartElement() && !reader.isEndElement())
						reader.next();

					if (reader.isStartElement() && new javax.xml.namespace.QName("", "lastName").equals(reader.getName())) {
						nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");

						if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
							throw new org.apache.axis2.databinding.ADBException("The element: " + "lastName" + "  cannot be null");
						}

						java.lang.String content = reader.getElementText();

						object.setLastName(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

						reader.next();
					} // End of if for expected property start element

					else {
						// A start element we are not expecting indicates an
						// invalid parameter was passed
						throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
					}

					while (!reader.isStartElement() && !reader.isEndElement())
						reader.next();

					if (reader.isStartElement() && new javax.xml.namespace.QName("", "suffix").equals(reader.getName())) {
						nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");

						if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
							throw new org.apache.axis2.databinding.ADBException("The element: " + "suffix" + "  cannot be null");
						}

						java.lang.String content = reader.getElementText();

						object.setSuffix(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

						reader.next();
					} // End of if for expected property start element

					else {
						// A start element we are not expecting indicates an
						// invalid parameter was passed
						throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
					}

					while (!reader.isStartElement() && !reader.isEndElement())
						reader.next();

					if (reader.isStartElement() && new javax.xml.namespace.QName("", "dateOfBirth").equals(reader.getName())) {
						nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");

						if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
							throw new org.apache.axis2.databinding.ADBException("The element: " + "dateOfBirth" + "  cannot be null");
						}

						java.lang.String content = reader.getElementText();

						object.setDateOfBirth(org.apache.axis2.databinding.utils.ConverterUtil.convertToDate(content));

						reader.next();
					} // End of if for expected property start element

					else {
						// A start element we are not expecting indicates an
						// invalid parameter was passed
						throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
					}

					while (!reader.isStartElement() && !reader.isEndElement())
						reader.next();

					if (reader.isStartElement() && new javax.xml.namespace.QName("", "genderCode").equals(reader.getName())) {
						nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");

						if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
							throw new org.apache.axis2.databinding.ADBException("The element: " + "genderCode" + "  cannot be null");
						}

						java.lang.String content = reader.getElementText();

						object.setGenderCode(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

						reader.next();
					} // End of if for expected property start element

					else {
						// A start element we are not expecting indicates an
						// invalid parameter was passed
						throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
					}

					while (!reader.isStartElement() && !reader.isEndElement())
						reader.next();

					if (reader.isStartElement() && new javax.xml.namespace.QName("", "genderDescription").equals(reader.getName())) {
						nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");

						if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
							throw new org.apache.axis2.databinding.ADBException("The element: " + "genderDescription" + "  cannot be null");
						}

						java.lang.String content = reader.getElementText();

						object.setGenderDescription(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

						reader.next();
					} // End of if for expected property start element

					else {
						// A start element we are not expecting indicates an
						// invalid parameter was passed
						throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
					}

					while (!reader.isStartElement() && !reader.isEndElement())
						reader.next();

					if (reader.isStartElement() && new javax.xml.namespace.QName("", "patientId").equals(reader.getName())) {
						nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");

						if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
							throw new org.apache.axis2.databinding.ADBException("The element: " + "patientId" + "  cannot be null");
						}

						java.lang.String content = reader.getElementText();

						object.setPatientId(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

						reader.next();
					} // End of if for expected property start element

					else {
						// A start element we are not expecting indicates an
						// invalid parameter was passed
						throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
					}

					while (!reader.isStartElement() && !reader.isEndElement())
						reader.next();

					if (reader.isStartElement() && new javax.xml.namespace.QName("", "memberId").equals(reader.getName())) {
						nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");

						if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
							throw new org.apache.axis2.databinding.ADBException("The element: " + "memberId" + "  cannot be null");
						}

						java.lang.String content = reader.getElementText();

						object.setMemberId(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

						reader.next();
					} // End of if for expected property start element

					else {
						// A start element we are not expecting indicates an
						// invalid parameter was passed
						throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
					}

					while (!reader.isStartElement() && !reader.isEndElement())
						reader.next();

					if (reader.isStartElement() && new javax.xml.namespace.QName("", "uniqueMemberId").equals(reader.getName())) {
						nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");

						if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
							throw new org.apache.axis2.databinding.ADBException("The element: " + "uniqueMemberId" + "  cannot be null");
						}

						java.lang.String content = reader.getElementText();

						object.setUniqueMemberId(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

						reader.next();
					} // End of if for expected property start element

					else {
						// A start element we are not expecting indicates an
						// invalid parameter was passed
						throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
					}

					while (!reader.isStartElement() && !reader.isEndElement())
						reader.next();

					if (reader.isStartElement() && new javax.xml.namespace.QName("", "relationshipCodeDescription").equals(reader.getName())) {
						nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");

						if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
							throw new org.apache.axis2.databinding.ADBException("The element: " + "relationshipCodeDescription" + "  cannot be null");
						}

						java.lang.String content = reader.getElementText();

						object.setRelationshipCodeDescription(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

						reader.next();
					} // End of if for expected property start element

					else {
						// A start element we are not expecting indicates an
						// invalid parameter was passed
						throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
					}

					while (!reader.isStartElement() && !reader.isEndElement())
						reader.next();

					if (reader.isStartElement() && new javax.xml.namespace.QName("", "dependentVerificationIndicator").equals(reader.getName())) {
						nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");

						if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
							throw new org.apache.axis2.databinding.ADBException("The element: " + "dependentVerificationIndicator" + "  cannot be null");
						}

						java.lang.String content = reader.getElementText();

						object.setDependentVerificationIndicator(org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));

						reader.next();
					} // End of if for expected property start element

					else {
						// A start element we are not expecting indicates an
						// invalid parameter was passed
						throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
					}

					while (!reader.isStartElement() && !reader.isEndElement())
						reader.next();

					if (reader.isStartElement() && new javax.xml.namespace.QName("", "privacyIndicator").equals(reader.getName())) {
						nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");

						if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
							throw new org.apache.axis2.databinding.ADBException("The element: " + "privacyIndicator" + "  cannot be null");
						}

						java.lang.String content = reader.getElementText();

						object.setPrivacyIndicator(org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));

						reader.next();
					} // End of if for expected property start element

					else {
						// A start element we are not expecting indicates an
						// invalid parameter was passed
						throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
					}

					while (!reader.isStartElement() && !reader.isEndElement())
						reader.next();

					if (reader.isStartElement() && new javax.xml.namespace.QName("", "businessSectorSegment").equals(reader.getName())) {
						object.setBusinessSectorSegment(BusinessSectorSegment.Factory.parse(reader));

						reader.next();
					} // End of if for expected property start element

					else {
						// A start element we are not expecting indicates an
						// invalid parameter was passed
						throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
					}

					while (!reader.isStartElement() && !reader.isEndElement())
						reader.next();

					if (reader.isStartElement()) {
						// A start element we are not expecting indicates a
						// trailing invalid property
						throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
					}
				} catch (javax.xml.stream.XMLStreamException e) {
					throw new java.lang.Exception(e);
				}

				return object;
			}
		} // end of factory class
	}

	public static class MemberDemographicsOutput extends MemberDemographics implements org.apache.axis2.databinding.ADBBean {
		/*
		 * This type was generated from the piece of schema that had name = MemberDemographicsOutput Namespace URI = http://servicehelpers.esb.avalon.com
		 * Namespace Prefix = ns1
		 */

		/**
		 * field for UniqueMemberId
		 */
		protected java.lang.String localUniqueMemberId;

		/**
		 * Auto generated getter method
		 * 
		 * @return java.lang.String
		 */
		public java.lang.String getUniqueMemberId() {
			return localUniqueMemberId;
		}

		/**
		 * Auto generated setter method
		 * 
		 * @param param
		 *            UniqueMemberId
		 */
		public void setUniqueMemberId(java.lang.String param) {
			this.localUniqueMemberId = param;
		}

		/**
		 *
		 * @param parentQName
		 * @param factory
		 * @return org.apache.axiom.om.OMElement
		 */
		public org.apache.axiom.om.OMElement getOMElement(final javax.xml.namespace.QName parentQName, final org.apache.axiom.om.OMFactory factory)
				throws org.apache.axis2.databinding.ADBException {
			org.apache.axiom.om.OMDataSource dataSource = new org.apache.axis2.databinding.ADBDataSource(this, parentQName);

			return factory.createOMElement(dataSource, parentQName);
		}

		public void serialize(final javax.xml.namespace.QName parentQName, javax.xml.stream.XMLStreamWriter xmlWriter)
				throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {
			serialize(parentQName, xmlWriter, false);
		}

		public void serialize(final javax.xml.namespace.QName parentQName, javax.xml.stream.XMLStreamWriter xmlWriter, boolean serializeType)
				throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {
			java.lang.String prefix = null;
			java.lang.String namespace = null;

			prefix = parentQName.getPrefix();
			namespace = parentQName.getNamespaceURI();
			writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);

			java.lang.String namespacePrefix = registerPrefix(xmlWriter, "http://servicehelpers.esb.avalon.com");

			if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)) {
				writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", namespacePrefix + ":MemberDemographicsOutput", xmlWriter);
			} else {
				writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", "MemberDemographicsOutput", xmlWriter);
			}

			if (localDateOfBirthTracker) {
				namespace = "";
				writeStartElement(null, namespace, "dateOfBirth", xmlWriter);

				if (localDateOfBirth == null) {
					// write the nil attribute
					throw new org.apache.axis2.databinding.ADBException("dateOfBirth cannot be null!!");
				} else {
					xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDateOfBirth));
				}

				xmlWriter.writeEndElement();
			}

			if (localFirstNameTracker) {
				namespace = "";
				writeStartElement(null, namespace, "firstName", xmlWriter);

				if (localFirstName == null) {
					// write the nil attribute
					throw new org.apache.axis2.databinding.ADBException("firstName cannot be null!!");
				} else {
					xmlWriter.writeCharacters(localFirstName);
				}

				xmlWriter.writeEndElement();
			}

			if (localGenderCodeTracker) {
				namespace = "";
				writeStartElement(null, namespace, "genderCode", xmlWriter);

				if (localGenderCode == null) {
					// write the nil attribute
					throw new org.apache.axis2.databinding.ADBException("genderCode cannot be null!!");
				} else {
					xmlWriter.writeCharacters(localGenderCode);
				}

				xmlWriter.writeEndElement();
			}

			if (localLastNameTracker) {
				namespace = "";
				writeStartElement(null, namespace, "lastName", xmlWriter);

				if (localLastName == null) {
					// write the nil attribute
					throw new org.apache.axis2.databinding.ADBException("lastName cannot be null!!");
				} else {
					xmlWriter.writeCharacters(localLastName);
				}

				xmlWriter.writeEndElement();
			}

			if (localMemberIdTracker) {
				namespace = "";
				writeStartElement(null, namespace, "memberId", xmlWriter);

				if (localMemberId == null) {
					// write the nil attribute
					throw new org.apache.axis2.databinding.ADBException("memberId cannot be null!!");
				} else {
					xmlWriter.writeCharacters(localMemberId);
				}

				xmlWriter.writeEndElement();
			}

			if (localMiddleNameTracker) {
				namespace = "";
				writeStartElement(null, namespace, "middleName", xmlWriter);

				if (localMiddleName == null) {
					// write the nil attribute
					throw new org.apache.axis2.databinding.ADBException("middleName cannot be null!!");
				} else {
					xmlWriter.writeCharacters(localMiddleName);
				}

				xmlWriter.writeEndElement();
			}

			if (localPatientIdTracker) {
				namespace = "";
				writeStartElement(null, namespace, "patientId", xmlWriter);

				if (localPatientId == null) {
					// write the nil attribute
					throw new org.apache.axis2.databinding.ADBException("patientId cannot be null!!");
				} else {
					xmlWriter.writeCharacters(localPatientId);
				}

				xmlWriter.writeEndElement();
			}

			if (localSuffixTracker) {
				namespace = "";
				writeStartElement(null, namespace, "suffix", xmlWriter);

				if (localSuffix == null) {
					// write the nil attribute
					throw new org.apache.axis2.databinding.ADBException("suffix cannot be null!!");
				} else {
					xmlWriter.writeCharacters(localSuffix);
				}

				xmlWriter.writeEndElement();
			}

			namespace = "";
			writeStartElement(null, namespace, "uniqueMemberId", xmlWriter);

			if (localUniqueMemberId == null) {
				// write the nil attribute
				throw new org.apache.axis2.databinding.ADBException("uniqueMemberId cannot be null!!");
			} else {
				xmlWriter.writeCharacters(localUniqueMemberId);
			}

			xmlWriter.writeEndElement();

			xmlWriter.writeEndElement();
		}

		private static java.lang.String generatePrefix(java.lang.String namespace) {
			if (namespace.equals("http://servicehelpers.esb.avalon.com")) {
				return "ns1";
			}

			return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
		}

		/**
		 * Utility method to write an element start tag.
		 */
		private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
				javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
			java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

			if (writerPrefix != null) {
				xmlWriter.writeStartElement(namespace, localPart);
			} else {
				if (namespace.length() == 0) {
					prefix = "";
				} else if (prefix == null) {
					prefix = generatePrefix(namespace);
				}

				xmlWriter.writeStartElement(prefix, localPart, namespace);
				xmlWriter.writeNamespace(prefix, namespace);
				xmlWriter.setPrefix(prefix, namespace);
			}
		}

		/**
		 * Util method to write an attribute with the ns prefix
		 */
		private void writeAttribute(java.lang.String prefix, java.lang.String namespace, java.lang.String attName, java.lang.String attValue,
				javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
			if (xmlWriter.getPrefix(namespace) == null) {
				xmlWriter.writeNamespace(prefix, namespace);
				xmlWriter.setPrefix(prefix, namespace);
			}

			xmlWriter.writeAttribute(namespace, attName, attValue);
		}

		/**
		 * Util method to write an attribute without the ns prefix
		 */
		private void writeAttribute(java.lang.String namespace, java.lang.String attName, java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter)
				throws javax.xml.stream.XMLStreamException {
			if (namespace.equals("")) {
				xmlWriter.writeAttribute(attName, attValue);
			} else {
				registerPrefix(xmlWriter, namespace);
				xmlWriter.writeAttribute(namespace, attName, attValue);
			}
		}

		/**
		 * Util method to write an attribute without the ns prefix
		 */
		private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName, javax.xml.namespace.QName qname,
				javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
			java.lang.String attributeNamespace = qname.getNamespaceURI();
			java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);

			if (attributePrefix == null) {
				attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
			}

			java.lang.String attributeValue;

			if (attributePrefix.trim().length() > 0) {
				attributeValue = attributePrefix + ":" + qname.getLocalPart();
			} else {
				attributeValue = qname.getLocalPart();
			}

			if (namespace.equals("")) {
				xmlWriter.writeAttribute(attName, attributeValue);
			} else {
				registerPrefix(xmlWriter, namespace);
				xmlWriter.writeAttribute(namespace, attName, attributeValue);
			}
		}

		/**
		 * method to handle Qnames
		 */
		private void writeQName(javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
			java.lang.String namespaceURI = qname.getNamespaceURI();

			if (namespaceURI != null) {
				java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);

				if (prefix == null) {
					prefix = generatePrefix(namespaceURI);
					xmlWriter.writeNamespace(prefix, namespaceURI);
					xmlWriter.setPrefix(prefix, namespaceURI);
				}

				if (prefix.trim().length() > 0) {
					xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
				} else {
					// i.e this is the default namespace
					xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
				}
			} else {
				xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
			}
		}

		private void writeQNames(javax.xml.namespace.QName[] qnames, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
			if (qnames != null) {
				// we have to store this data until last moment since it is not
				// possible to write any
				// namespace data after writing the charactor data
				java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
				java.lang.String namespaceURI = null;
				java.lang.String prefix = null;

				for (int i = 0; i < qnames.length; i++) {
					if (i > 0) {
						stringToWrite.append(" ");
					}

					namespaceURI = qnames[i].getNamespaceURI();

					if (namespaceURI != null) {
						prefix = xmlWriter.getPrefix(namespaceURI);

						if ((prefix == null) || (prefix.length() == 0)) {
							prefix = generatePrefix(namespaceURI);
							xmlWriter.writeNamespace(prefix, namespaceURI);
							xmlWriter.setPrefix(prefix, namespaceURI);
						}

						if (prefix.trim().length() > 0) {
							stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
						} else {
							stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
						}
					} else {
						stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
					}
				}

				xmlWriter.writeCharacters(stringToWrite.toString());
			}
		}

		/**
		 * Register a namespace prefix
		 */
		private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace)
				throws javax.xml.stream.XMLStreamException {
			java.lang.String prefix = xmlWriter.getPrefix(namespace);

			if (prefix == null) {
				prefix = generatePrefix(namespace);

				javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();

				while (true) {
					java.lang.String uri = nsContext.getNamespaceURI(prefix);

					if ((uri == null) || (uri.length() == 0)) {
						break;
					}

					prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
				}

				xmlWriter.writeNamespace(prefix, namespace);
				xmlWriter.setPrefix(prefix, namespace);
			}

			return prefix;
		}

		/**
		 * databinding method to get an XML representation of this object
		 *
		 */
		public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName) throws org.apache.axis2.databinding.ADBException {
			java.util.ArrayList elementList = new java.util.ArrayList();
			java.util.ArrayList attribList = new java.util.ArrayList();

			attribList.add(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema-instance", "type"));
			attribList.add(new javax.xml.namespace.QName("http://servicehelpers.esb.avalon.com", "MemberDemographicsOutput"));

			if (localDateOfBirthTracker) {
				elementList.add(new javax.xml.namespace.QName("", "dateOfBirth"));

				if (localDateOfBirth != null) {
					elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDateOfBirth));
				} else {
					throw new org.apache.axis2.databinding.ADBException("dateOfBirth cannot be null!!");
				}
			}

			if (localFirstNameTracker) {
				elementList.add(new javax.xml.namespace.QName("", "firstName"));

				if (localFirstName != null) {
					elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localFirstName));
				} else {
					throw new org.apache.axis2.databinding.ADBException("firstName cannot be null!!");
				}
			}

			if (localGenderCodeTracker) {
				elementList.add(new javax.xml.namespace.QName("", "genderCode"));

				if (localGenderCode != null) {
					elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localGenderCode));
				} else {
					throw new org.apache.axis2.databinding.ADBException("genderCode cannot be null!!");
				}
			}

			if (localLastNameTracker) {
				elementList.add(new javax.xml.namespace.QName("", "lastName"));

				if (localLastName != null) {
					elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLastName));
				} else {
					throw new org.apache.axis2.databinding.ADBException("lastName cannot be null!!");
				}
			}

			if (localMemberIdTracker) {
				elementList.add(new javax.xml.namespace.QName("", "memberId"));

				if (localMemberId != null) {
					elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localMemberId));
				} else {
					throw new org.apache.axis2.databinding.ADBException("memberId cannot be null!!");
				}
			}

			if (localMiddleNameTracker) {
				elementList.add(new javax.xml.namespace.QName("", "middleName"));

				if (localMiddleName != null) {
					elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localMiddleName));
				} else {
					throw new org.apache.axis2.databinding.ADBException("middleName cannot be null!!");
				}
			}

			if (localPatientIdTracker) {
				elementList.add(new javax.xml.namespace.QName("", "patientId"));

				if (localPatientId != null) {
					elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPatientId));
				} else {
					throw new org.apache.axis2.databinding.ADBException("patientId cannot be null!!");
				}
			}

			if (localSuffixTracker) {
				elementList.add(new javax.xml.namespace.QName("", "suffix"));

				if (localSuffix != null) {
					elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSuffix));
				} else {
					throw new org.apache.axis2.databinding.ADBException("suffix cannot be null!!");
				}
			}

			elementList.add(new javax.xml.namespace.QName("", "uniqueMemberId"));

			if (localUniqueMemberId != null) {
				elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localUniqueMemberId));
			} else {
				throw new org.apache.axis2.databinding.ADBException("uniqueMemberId cannot be null!!");
			}

			return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
		}

		/**
		 * Factory class that keeps the parse method
		 */
		public static class Factory {
			/**
			 * static method to create the object Precondition: If this object is an element, the current or next start element starts this object and any
			 * intervening reader events are ignorable If this object is not an element, it is a complex type and the reader is at the event just after the
			 * outer start element Postcondition: If this object is an element, the reader is positioned at its end element If this object is a complex type,
			 * the reader is positioned at the end element of its outer element
			 */
			public static MemberDemographicsOutput parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception {
				MemberDemographicsOutput object = new MemberDemographicsOutput();

				int event;
				java.lang.String nillableValue = null;
				java.lang.String prefix = "";
				java.lang.String namespaceuri = "";

				try {
					while (!reader.isStartElement() && !reader.isEndElement())
						reader.next();

					if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
						java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type");

						if (fullTypeName != null) {
							java.lang.String nsPrefix = null;

							if (fullTypeName.indexOf(":") > -1) {
								nsPrefix = fullTypeName.substring(0, fullTypeName.indexOf(":"));
							}

							nsPrefix = (nsPrefix == null) ? "" : nsPrefix;

							java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":") + 1);

							if (!"MemberDemographicsOutput".equals(type)) {
								// find namespace for the prefix
								java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);

								return (MemberDemographicsOutput) ExtensionMapper.getTypeObject(nsUri, type, reader);
							}
						}
					}

					// Note all attributes that were handled. Used to differ
					// normal attributes
					// from anyAttributes.
					java.util.Vector handledAttributes = new java.util.Vector();

					reader.next();

					while (!reader.isStartElement() && !reader.isEndElement())
						reader.next();

					if (reader.isStartElement() && new javax.xml.namespace.QName("", "firstName").equals(reader.getName())) {
						nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");

						if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
							throw new org.apache.axis2.databinding.ADBException("The element: " + "firstName" + "  cannot be null");
						}

						java.lang.String content = reader.getElementText();

						object.setFirstName(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

						reader.next();
					} // End of if for expected property start element

					else {
					}

					while (!reader.isStartElement() && !reader.isEndElement())
						reader.next();

					if (reader.isStartElement() && new javax.xml.namespace.QName("", "middleName").equals(reader.getName())) {
						nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");

						if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
							throw new org.apache.axis2.databinding.ADBException("The element: " + "middleName" + "  cannot be null");
						}

						java.lang.String content = reader.getElementText();

						object.setMiddleName(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

						reader.next();
					} // End of if for expected property start element

					else {
					}

					while (!reader.isStartElement() && !reader.isEndElement())
						reader.next();

					if (reader.isStartElement() && new javax.xml.namespace.QName("", "lastName").equals(reader.getName())) {
						nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");

						if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
							throw new org.apache.axis2.databinding.ADBException("The element: " + "lastName" + "  cannot be null");
						}

						java.lang.String content = reader.getElementText();

						object.setLastName(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

						reader.next();
					} // End of if for expected property start element

					else {
					}

					while (!reader.isStartElement() && !reader.isEndElement())
						reader.next();

					if (reader.isStartElement() && new javax.xml.namespace.QName("", "suffix").equals(reader.getName())) {
						nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");

						if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
							throw new org.apache.axis2.databinding.ADBException("The element: " + "suffix" + "  cannot be null");
						}

						java.lang.String content = reader.getElementText();

						object.setSuffix(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

						reader.next();
					} // End of if for expected property start element

					else {
					}

					while (!reader.isStartElement() && !reader.isEndElement())
						reader.next();

					if (reader.isStartElement() && new javax.xml.namespace.QName("", "dateOfBirth").equals(reader.getName())) {
						nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");

						if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
							throw new org.apache.axis2.databinding.ADBException("The element: " + "dateOfBirth" + "  cannot be null");
						}

						java.lang.String content = reader.getElementText();

						object.setDateOfBirth(org.apache.axis2.databinding.utils.ConverterUtil.convertToDate(content));

						reader.next();
					} // End of if for expected property start element

					else {
					}

					while (!reader.isStartElement() && !reader.isEndElement())
						reader.next();

					if (reader.isStartElement() && new javax.xml.namespace.QName("", "genderCode").equals(reader.getName())) {
						nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");

						if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
							throw new org.apache.axis2.databinding.ADBException("The element: " + "genderCode" + "  cannot be null");
						}

						java.lang.String content = reader.getElementText();

						object.setGenderCode(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

						reader.next();
					} // End of if for expected property start element

					else {
					}

					while (!reader.isStartElement() && !reader.isEndElement())
						reader.next();

					if (reader.isStartElement() && new javax.xml.namespace.QName("", "patientId").equals(reader.getName())) {
						nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");

						if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
							throw new org.apache.axis2.databinding.ADBException("The element: " + "patientId" + "  cannot be null");
						}

						java.lang.String content = reader.getElementText();

						object.setPatientId(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

						reader.next();
					} // End of if for expected property start element

					else {
					}

					while (!reader.isStartElement() && !reader.isEndElement())
						reader.next();

					if (reader.isStartElement() && new javax.xml.namespace.QName("", "memberId").equals(reader.getName())) {
						nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");

						if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
							throw new org.apache.axis2.databinding.ADBException("The element: " + "memberId" + "  cannot be null");
						}

						java.lang.String content = reader.getElementText();

						object.setMemberId(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

						reader.next();
					} // End of if for expected property start element

					else {
					}

					while (!reader.isStartElement() && !reader.isEndElement())
						reader.next();

					if (reader.isStartElement() && new javax.xml.namespace.QName("", "uniqueMemberId").equals(reader.getName())) {
						nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");

						if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
							throw new org.apache.axis2.databinding.ADBException("The element: " + "uniqueMemberId" + "  cannot be null");
						}

						java.lang.String content = reader.getElementText();

						object.setUniqueMemberId(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

						reader.next();
					} // End of if for expected property start element

					else {
						// A start element we are not expecting indicates an
						// invalid parameter was passed
						throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
					}

					while (!reader.isStartElement() && !reader.isEndElement())
						reader.next();

					if (reader.isStartElement()) {
						// A start element we are not expecting indicates a
						// trailing invalid property
						throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
					}
				} catch (javax.xml.stream.XMLStreamException e) {
					throw new java.lang.Exception(e);
				}

				return object;
			}
		} // end of factory class
	}

	public static class MoreDataOptions implements org.apache.axis2.databinding.ADBBean {
		/*
		 * This type was generated from the piece of schema that had name = moreDataOptions Namespace URI = http://servicehelpers.esb.avalon.com Namespace
		 * Prefix = ns1
		 */

		/**
		 * field for RetrieveCoverageData
		 */
		protected boolean localRetrieveCoverageData;

		/**
		 * field for RetrieveMemberList
		 */
		protected boolean localRetrieveMemberList;

		/**
		 * Auto generated getter method
		 * 
		 * @return boolean
		 */
		public boolean getRetrieveCoverageData() {
			return localRetrieveCoverageData;
		}

		/**
		 * Auto generated setter method
		 * 
		 * @param param
		 *            RetrieveCoverageData
		 */
		public void setRetrieveCoverageData(boolean param) {
			this.localRetrieveCoverageData = param;
		}

		/**
		 * Auto generated getter method
		 * 
		 * @return boolean
		 */
		public boolean getRetrieveMemberList() {
			return localRetrieveMemberList;
		}

		/**
		 * Auto generated setter method
		 * 
		 * @param param
		 *            RetrieveMemberList
		 */
		public void setRetrieveMemberList(boolean param) {
			this.localRetrieveMemberList = param;
		}

		/**
		 *
		 * @param parentQName
		 * @param factory
		 * @return org.apache.axiom.om.OMElement
		 */
		public org.apache.axiom.om.OMElement getOMElement(final javax.xml.namespace.QName parentQName, final org.apache.axiom.om.OMFactory factory)
				throws org.apache.axis2.databinding.ADBException {
			org.apache.axiom.om.OMDataSource dataSource = new org.apache.axis2.databinding.ADBDataSource(this, parentQName);

			return factory.createOMElement(dataSource, parentQName);
		}

		public void serialize(final javax.xml.namespace.QName parentQName, javax.xml.stream.XMLStreamWriter xmlWriter)
				throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {
			serialize(parentQName, xmlWriter, false);
		}

		public void serialize(final javax.xml.namespace.QName parentQName, javax.xml.stream.XMLStreamWriter xmlWriter, boolean serializeType)
				throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {
			java.lang.String prefix = null;
			java.lang.String namespace = null;

			prefix = parentQName.getPrefix();
			namespace = parentQName.getNamespaceURI();
			writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);

			if (serializeType) {
				java.lang.String namespacePrefix = registerPrefix(xmlWriter, "http://servicehelpers.esb.avalon.com");

				if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)) {
					writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", namespacePrefix + ":moreDataOptions", xmlWriter);
				} else {
					writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", "moreDataOptions", xmlWriter);
				}
			}

			namespace = "";
			writeStartElement(null, namespace, "retrieveCoverageData", xmlWriter);

			if (false) {
				throw new org.apache.axis2.databinding.ADBException("retrieveCoverageData cannot be null!!");
			} else {
				xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localRetrieveCoverageData));
			}

			xmlWriter.writeEndElement();

			namespace = "";
			writeStartElement(null, namespace, "retrieveMemberList", xmlWriter);

			if (false) {
				throw new org.apache.axis2.databinding.ADBException("retrieveMemberList cannot be null!!");
			} else {
				xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localRetrieveMemberList));
			}

			xmlWriter.writeEndElement();

			xmlWriter.writeEndElement();
		}

		private static java.lang.String generatePrefix(java.lang.String namespace) {
			if (namespace.equals("http://servicehelpers.esb.avalon.com")) {
				return "ns1";
			}

			return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
		}

		/**
		 * Utility method to write an element start tag.
		 */
		private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
				javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
			java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

			if (writerPrefix != null) {
				xmlWriter.writeStartElement(namespace, localPart);
			} else {
				if (namespace.length() == 0) {
					prefix = "";
				} else if (prefix == null) {
					prefix = generatePrefix(namespace);
				}

				xmlWriter.writeStartElement(prefix, localPart, namespace);
				xmlWriter.writeNamespace(prefix, namespace);
				xmlWriter.setPrefix(prefix, namespace);
			}
		}

		/**
		 * Util method to write an attribute with the ns prefix
		 */
		private void writeAttribute(java.lang.String prefix, java.lang.String namespace, java.lang.String attName, java.lang.String attValue,
				javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
			if (xmlWriter.getPrefix(namespace) == null) {
				xmlWriter.writeNamespace(prefix, namespace);
				xmlWriter.setPrefix(prefix, namespace);
			}

			xmlWriter.writeAttribute(namespace, attName, attValue);
		}

		/**
		 * Util method to write an attribute without the ns prefix
		 */
		private void writeAttribute(java.lang.String namespace, java.lang.String attName, java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter)
				throws javax.xml.stream.XMLStreamException {
			if (namespace.equals("")) {
				xmlWriter.writeAttribute(attName, attValue);
			} else {
				registerPrefix(xmlWriter, namespace);
				xmlWriter.writeAttribute(namespace, attName, attValue);
			}
		}

		/**
		 * Util method to write an attribute without the ns prefix
		 */
		private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName, javax.xml.namespace.QName qname,
				javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
			java.lang.String attributeNamespace = qname.getNamespaceURI();
			java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);

			if (attributePrefix == null) {
				attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
			}

			java.lang.String attributeValue;

			if (attributePrefix.trim().length() > 0) {
				attributeValue = attributePrefix + ":" + qname.getLocalPart();
			} else {
				attributeValue = qname.getLocalPart();
			}

			if (namespace.equals("")) {
				xmlWriter.writeAttribute(attName, attributeValue);
			} else {
				registerPrefix(xmlWriter, namespace);
				xmlWriter.writeAttribute(namespace, attName, attributeValue);
			}
		}

		/**
		 * method to handle Qnames
		 */
		private void writeQName(javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
			java.lang.String namespaceURI = qname.getNamespaceURI();

			if (namespaceURI != null) {
				java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);

				if (prefix == null) {
					prefix = generatePrefix(namespaceURI);
					xmlWriter.writeNamespace(prefix, namespaceURI);
					xmlWriter.setPrefix(prefix, namespaceURI);
				}

				if (prefix.trim().length() > 0) {
					xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
				} else {
					// i.e this is the default namespace
					xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
				}
			} else {
				xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
			}
		}

		private void writeQNames(javax.xml.namespace.QName[] qnames, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
			if (qnames != null) {
				// we have to store this data until last moment since it is not
				// possible to write any
				// namespace data after writing the charactor data
				java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
				java.lang.String namespaceURI = null;
				java.lang.String prefix = null;

				for (int i = 0; i < qnames.length; i++) {
					if (i > 0) {
						stringToWrite.append(" ");
					}

					namespaceURI = qnames[i].getNamespaceURI();

					if (namespaceURI != null) {
						prefix = xmlWriter.getPrefix(namespaceURI);

						if ((prefix == null) || (prefix.length() == 0)) {
							prefix = generatePrefix(namespaceURI);
							xmlWriter.writeNamespace(prefix, namespaceURI);
							xmlWriter.setPrefix(prefix, namespaceURI);
						}

						if (prefix.trim().length() > 0) {
							stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
						} else {
							stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
						}
					} else {
						stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
					}
				}

				xmlWriter.writeCharacters(stringToWrite.toString());
			}
		}

		/**
		 * Register a namespace prefix
		 */
		private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace)
				throws javax.xml.stream.XMLStreamException {
			java.lang.String prefix = xmlWriter.getPrefix(namespace);

			if (prefix == null) {
				prefix = generatePrefix(namespace);

				javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();

				while (true) {
					java.lang.String uri = nsContext.getNamespaceURI(prefix);

					if ((uri == null) || (uri.length() == 0)) {
						break;
					}

					prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
				}

				xmlWriter.writeNamespace(prefix, namespace);
				xmlWriter.setPrefix(prefix, namespace);
			}

			return prefix;
		}

		/**
		 * databinding method to get an XML representation of this object
		 *
		 */
		public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName) throws org.apache.axis2.databinding.ADBException {
			java.util.ArrayList elementList = new java.util.ArrayList();
			java.util.ArrayList attribList = new java.util.ArrayList();

			elementList.add(new javax.xml.namespace.QName("", "retrieveCoverageData"));

			elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localRetrieveCoverageData));

			elementList.add(new javax.xml.namespace.QName("", "retrieveMemberList"));

			elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localRetrieveMemberList));

			return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
		}

		/**
		 * Factory class that keeps the parse method
		 */
		public static class Factory {
			/**
			 * static method to create the object Precondition: If this object is an element, the current or next start element starts this object and any
			 * intervening reader events are ignorable If this object is not an element, it is a complex type and the reader is at the event just after the
			 * outer start element Postcondition: If this object is an element, the reader is positioned at its end element If this object is a complex type,
			 * the reader is positioned at the end element of its outer element
			 */
			public static MoreDataOptions parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception {
				MoreDataOptions object = new MoreDataOptions();

				int event;
				java.lang.String nillableValue = null;
				java.lang.String prefix = "";
				java.lang.String namespaceuri = "";

				try {
					while (!reader.isStartElement() && !reader.isEndElement())
						reader.next();

					if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
						java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type");

						if (fullTypeName != null) {
							java.lang.String nsPrefix = null;

							if (fullTypeName.indexOf(":") > -1) {
								nsPrefix = fullTypeName.substring(0, fullTypeName.indexOf(":"));
							}

							nsPrefix = (nsPrefix == null) ? "" : nsPrefix;

							java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":") + 1);

							if (!"moreDataOptions".equals(type)) {
								// find namespace for the prefix
								java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);

								return (MoreDataOptions) ExtensionMapper.getTypeObject(nsUri, type, reader);
							}
						}
					}

					// Note all attributes that were handled. Used to differ
					// normal attributes
					// from anyAttributes.
					java.util.Vector handledAttributes = new java.util.Vector();

					reader.next();

					while (!reader.isStartElement() && !reader.isEndElement())
						reader.next();

					if (reader.isStartElement() && new javax.xml.namespace.QName("", "retrieveCoverageData").equals(reader.getName())) {
						nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");

						if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
							throw new org.apache.axis2.databinding.ADBException("The element: " + "retrieveCoverageData" + "  cannot be null");
						}

						java.lang.String content = reader.getElementText();

						object.setRetrieveCoverageData(org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));

						reader.next();
					} // End of if for expected property start element

					else {
						// A start element we are not expecting indicates an
						// invalid parameter was passed
						throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
					}

					while (!reader.isStartElement() && !reader.isEndElement())
						reader.next();

					if (reader.isStartElement() && new javax.xml.namespace.QName("", "retrieveMemberList").equals(reader.getName())) {
						nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");

						if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
							throw new org.apache.axis2.databinding.ADBException("The element: " + "retrieveMemberList" + "  cannot be null");
						}

						java.lang.String content = reader.getElementText();

						object.setRetrieveMemberList(org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));

						reader.next();
					} // End of if for expected property start element

					else {
						// A start element we are not expecting indicates an
						// invalid parameter was passed
						throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
					}

					while (!reader.isStartElement() && !reader.isEndElement())
						reader.next();

					if (reader.isStartElement()) {
						// A start element we are not expecting indicates a
						// trailing invalid property
						throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
					}
				} catch (javax.xml.stream.XMLStreamException e) {
					throw new java.lang.Exception(e);
				}

				return object;
			}
		} // end of factory class
	}

	public static class BusinessSectorSegment implements org.apache.axis2.databinding.ADBBean {
		/*
		 * This type was generated from the piece of schema that had name = BusinessSectorSegment Namespace URI = http://servicehelpers.esb.avalon.com Namespace
		 * Prefix = ns1
		 */

		/**
		 * field for BusinessSectorCd
		 */
		protected java.lang.String localBusinessSectorCd;

		/**
		 * field for BusinessSectorDesc
		 */
		protected java.lang.String localBusinessSectorDesc;

		/**
		 * field for BusinessSegmentCd
		 */
		protected java.lang.String localBusinessSegmentCd;

		/**
		 * field for BusinessSegmentDesc
		 */
		protected java.lang.String localBusinessSegmentDesc;

		/**
		 * Auto generated getter method
		 * 
		 * @return java.lang.String
		 */
		public java.lang.String getBusinessSectorCd() {
			return localBusinessSectorCd;
		}

		/**
		 * Auto generated setter method
		 * 
		 * @param param
		 *            BusinessSectorCd
		 */
		public void setBusinessSectorCd(java.lang.String param) {
			this.localBusinessSectorCd = param;
		}

		/**
		 * Auto generated getter method
		 * 
		 * @return java.lang.String
		 */
		public java.lang.String getBusinessSectorDesc() {
			return localBusinessSectorDesc;
		}

		/**
		 * Auto generated setter method
		 * 
		 * @param param
		 *            BusinessSectorDesc
		 */
		public void setBusinessSectorDesc(java.lang.String param) {
			this.localBusinessSectorDesc = param;
		}

		/**
		 * Auto generated getter method
		 * 
		 * @return java.lang.String
		 */
		public java.lang.String getBusinessSegmentCd() {
			return localBusinessSegmentCd;
		}

		/**
		 * Auto generated setter method
		 * 
		 * @param param
		 *            BusinessSegmentCd
		 */
		public void setBusinessSegmentCd(java.lang.String param) {
			this.localBusinessSegmentCd = param;
		}

		/**
		 * Auto generated getter method
		 * 
		 * @return java.lang.String
		 */
		public java.lang.String getBusinessSegmentDesc() {
			return localBusinessSegmentDesc;
		}

		/**
		 * Auto generated setter method
		 * 
		 * @param param
		 *            BusinessSegmentDesc
		 */
		public void setBusinessSegmentDesc(java.lang.String param) {
			this.localBusinessSegmentDesc = param;
		}

		/**
		 *
		 * @param parentQName
		 * @param factory
		 * @return org.apache.axiom.om.OMElement
		 */
		public org.apache.axiom.om.OMElement getOMElement(final javax.xml.namespace.QName parentQName, final org.apache.axiom.om.OMFactory factory)
				throws org.apache.axis2.databinding.ADBException {
			org.apache.axiom.om.OMDataSource dataSource = new org.apache.axis2.databinding.ADBDataSource(this, parentQName);

			return factory.createOMElement(dataSource, parentQName);
		}

		public void serialize(final javax.xml.namespace.QName parentQName, javax.xml.stream.XMLStreamWriter xmlWriter)
				throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {
			serialize(parentQName, xmlWriter, false);
		}

		public void serialize(final javax.xml.namespace.QName parentQName, javax.xml.stream.XMLStreamWriter xmlWriter, boolean serializeType)
				throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {
			java.lang.String prefix = null;
			java.lang.String namespace = null;

			prefix = parentQName.getPrefix();
			namespace = parentQName.getNamespaceURI();
			writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);

			if (serializeType) {
				java.lang.String namespacePrefix = registerPrefix(xmlWriter, "http://servicehelpers.esb.avalon.com");

				if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)) {
					writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", namespacePrefix + ":BusinessSectorSegment", xmlWriter);
				} else {
					writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", "BusinessSectorSegment", xmlWriter);
				}
			}

			namespace = "";
			writeStartElement(null, namespace, "businessSectorCd", xmlWriter);

			if (localBusinessSectorCd == null) {
				// write the nil attribute
				throw new org.apache.axis2.databinding.ADBException("businessSectorCd cannot be null!!");
			} else {
				xmlWriter.writeCharacters(localBusinessSectorCd);
			}

			xmlWriter.writeEndElement();

			namespace = "";
			writeStartElement(null, namespace, "businessSectorDesc", xmlWriter);

			if (localBusinessSectorDesc == null) {
				// write the nil attribute
				throw new org.apache.axis2.databinding.ADBException("businessSectorDesc cannot be null!!");
			} else {
				xmlWriter.writeCharacters(localBusinessSectorDesc);
			}

			xmlWriter.writeEndElement();

			namespace = "";
			writeStartElement(null, namespace, "businessSegmentCd", xmlWriter);

			if (localBusinessSegmentCd == null) {
				// write the nil attribute
				throw new org.apache.axis2.databinding.ADBException("businessSegmentCd cannot be null!!");
			} else {
				xmlWriter.writeCharacters(localBusinessSegmentCd);
			}

			xmlWriter.writeEndElement();

			namespace = "";
			writeStartElement(null, namespace, "businessSegmentDesc", xmlWriter);

			if (localBusinessSegmentDesc == null) {
				// write the nil attribute
				throw new org.apache.axis2.databinding.ADBException("businessSegmentDesc cannot be null!!");
			} else {
				xmlWriter.writeCharacters(localBusinessSegmentDesc);
			}

			xmlWriter.writeEndElement();

			xmlWriter.writeEndElement();
		}

		private static java.lang.String generatePrefix(java.lang.String namespace) {
			if (namespace.equals("http://servicehelpers.esb.avalon.com")) {
				return "ns1";
			}

			return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
		}

		/**
		 * Utility method to write an element start tag.
		 */
		private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
				javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
			java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);

			if (writerPrefix != null) {
				xmlWriter.writeStartElement(namespace, localPart);
			} else {
				if (namespace.length() == 0) {
					prefix = "";
				} else if (prefix == null) {
					prefix = generatePrefix(namespace);
				}

				xmlWriter.writeStartElement(prefix, localPart, namespace);
				xmlWriter.writeNamespace(prefix, namespace);
				xmlWriter.setPrefix(prefix, namespace);
			}
		}

		/**
		 * Util method to write an attribute with the ns prefix
		 */
		private void writeAttribute(java.lang.String prefix, java.lang.String namespace, java.lang.String attName, java.lang.String attValue,
				javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
			if (xmlWriter.getPrefix(namespace) == null) {
				xmlWriter.writeNamespace(prefix, namespace);
				xmlWriter.setPrefix(prefix, namespace);
			}

			xmlWriter.writeAttribute(namespace, attName, attValue);
		}

		/**
		 * Util method to write an attribute without the ns prefix
		 */
		private void writeAttribute(java.lang.String namespace, java.lang.String attName, java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter)
				throws javax.xml.stream.XMLStreamException {
			if (namespace.equals("")) {
				xmlWriter.writeAttribute(attName, attValue);
			} else {
				registerPrefix(xmlWriter, namespace);
				xmlWriter.writeAttribute(namespace, attName, attValue);
			}
		}

		/**
		 * Util method to write an attribute without the ns prefix
		 */
		private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName, javax.xml.namespace.QName qname,
				javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
			java.lang.String attributeNamespace = qname.getNamespaceURI();
			java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);

			if (attributePrefix == null) {
				attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
			}

			java.lang.String attributeValue;

			if (attributePrefix.trim().length() > 0) {
				attributeValue = attributePrefix + ":" + qname.getLocalPart();
			} else {
				attributeValue = qname.getLocalPart();
			}

			if (namespace.equals("")) {
				xmlWriter.writeAttribute(attName, attributeValue);
			} else {
				registerPrefix(xmlWriter, namespace);
				xmlWriter.writeAttribute(namespace, attName, attributeValue);
			}
		}

		/**
		 * method to handle Qnames
		 */
		private void writeQName(javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
			java.lang.String namespaceURI = qname.getNamespaceURI();

			if (namespaceURI != null) {
				java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);

				if (prefix == null) {
					prefix = generatePrefix(namespaceURI);
					xmlWriter.writeNamespace(prefix, namespaceURI);
					xmlWriter.setPrefix(prefix, namespaceURI);
				}

				if (prefix.trim().length() > 0) {
					xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
				} else {
					// i.e this is the default namespace
					xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
				}
			} else {
				xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
			}
		}

		private void writeQNames(javax.xml.namespace.QName[] qnames, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
			if (qnames != null) {
				// we have to store this data until last moment since it is not
				// possible to write any
				// namespace data after writing the charactor data
				java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
				java.lang.String namespaceURI = null;
				java.lang.String prefix = null;

				for (int i = 0; i < qnames.length; i++) {
					if (i > 0) {
						stringToWrite.append(" ");
					}

					namespaceURI = qnames[i].getNamespaceURI();

					if (namespaceURI != null) {
						prefix = xmlWriter.getPrefix(namespaceURI);

						if ((prefix == null) || (prefix.length() == 0)) {
							prefix = generatePrefix(namespaceURI);
							xmlWriter.writeNamespace(prefix, namespaceURI);
							xmlWriter.setPrefix(prefix, namespaceURI);
						}

						if (prefix.trim().length() > 0) {
							stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
						} else {
							stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
						}
					} else {
						stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
					}
				}

				xmlWriter.writeCharacters(stringToWrite.toString());
			}
		}

		/**
		 * Register a namespace prefix
		 */
		private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace)
				throws javax.xml.stream.XMLStreamException {
			java.lang.String prefix = xmlWriter.getPrefix(namespace);

			if (prefix == null) {
				prefix = generatePrefix(namespace);

				javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();

				while (true) {
					java.lang.String uri = nsContext.getNamespaceURI(prefix);

					if ((uri == null) || (uri.length() == 0)) {
						break;
					}

					prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
				}

				xmlWriter.writeNamespace(prefix, namespace);
				xmlWriter.setPrefix(prefix, namespace);
			}

			return prefix;
		}

		/**
		 * databinding method to get an XML representation of this object
		 *
		 */
		public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName) throws org.apache.axis2.databinding.ADBException {
			java.util.ArrayList elementList = new java.util.ArrayList();
			java.util.ArrayList attribList = new java.util.ArrayList();

			elementList.add(new javax.xml.namespace.QName("", "businessSectorCd"));

			if (localBusinessSectorCd != null) {
				elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localBusinessSectorCd));
			} else {
				throw new org.apache.axis2.databinding.ADBException("businessSectorCd cannot be null!!");
			}

			elementList.add(new javax.xml.namespace.QName("", "businessSectorDesc"));

			if (localBusinessSectorDesc != null) {
				elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localBusinessSectorDesc));
			} else {
				throw new org.apache.axis2.databinding.ADBException("businessSectorDesc cannot be null!!");
			}

			elementList.add(new javax.xml.namespace.QName("", "businessSegmentCd"));

			if (localBusinessSegmentCd != null) {
				elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localBusinessSegmentCd));
			} else {
				throw new org.apache.axis2.databinding.ADBException("businessSegmentCd cannot be null!!");
			}

			elementList.add(new javax.xml.namespace.QName("", "businessSegmentDesc"));

			if (localBusinessSegmentDesc != null) {
				elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localBusinessSegmentDesc));
			} else {
				throw new org.apache.axis2.databinding.ADBException("businessSegmentDesc cannot be null!!");
			}

			return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
		}

		/**
		 * Factory class that keeps the parse method
		 */
		public static class Factory {
			/**
			 * static method to create the object Precondition: If this object is an element, the current or next start element starts this object and any
			 * intervening reader events are ignorable If this object is not an element, it is a complex type and the reader is at the event just after the
			 * outer start element Postcondition: If this object is an element, the reader is positioned at its end element If this object is a complex type,
			 * the reader is positioned at the end element of its outer element
			 */
			public static BusinessSectorSegment parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception {
				BusinessSectorSegment object = new BusinessSectorSegment();

				int event;
				java.lang.String nillableValue = null;
				java.lang.String prefix = "";
				java.lang.String namespaceuri = "";

				try {
					while (!reader.isStartElement() && !reader.isEndElement())
						reader.next();

					if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
						java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type");

						if (fullTypeName != null) {
							java.lang.String nsPrefix = null;

							if (fullTypeName.indexOf(":") > -1) {
								nsPrefix = fullTypeName.substring(0, fullTypeName.indexOf(":"));
							}

							nsPrefix = (nsPrefix == null) ? "" : nsPrefix;

							java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":") + 1);

							if (!"BusinessSectorSegment".equals(type)) {
								// find namespace for the prefix
								java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);

								return (BusinessSectorSegment) ExtensionMapper.getTypeObject(nsUri, type, reader);
							}
						}
					}

					// Note all attributes that were handled. Used to differ
					// normal attributes
					// from anyAttributes.
					java.util.Vector handledAttributes = new java.util.Vector();

					reader.next();

					while (!reader.isStartElement() && !reader.isEndElement())
						reader.next();

					if (reader.isStartElement() && new javax.xml.namespace.QName("", "businessSectorCd").equals(reader.getName())) {
						nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");

						if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
							throw new org.apache.axis2.databinding.ADBException("The element: " + "businessSectorCd" + "  cannot be null");
						}

						java.lang.String content = reader.getElementText();

						object.setBusinessSectorCd(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

						reader.next();
					} // End of if for expected property start element

					else {
						// A start element we are not expecting indicates an
						// invalid parameter was passed
						throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
					}

					while (!reader.isStartElement() && !reader.isEndElement())
						reader.next();

					if (reader.isStartElement() && new javax.xml.namespace.QName("", "businessSectorDesc").equals(reader.getName())) {
						nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");

						if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
							throw new org.apache.axis2.databinding.ADBException("The element: " + "businessSectorDesc" + "  cannot be null");
						}

						java.lang.String content = reader.getElementText();

						object.setBusinessSectorDesc(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

						reader.next();
					} // End of if for expected property start element

					else {
						// A start element we are not expecting indicates an
						// invalid parameter was passed
						throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
					}

					while (!reader.isStartElement() && !reader.isEndElement())
						reader.next();

					if (reader.isStartElement() && new javax.xml.namespace.QName("", "businessSegmentCd").equals(reader.getName())) {
						nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");

						if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
							throw new org.apache.axis2.databinding.ADBException("The element: " + "businessSegmentCd" + "  cannot be null");
						}

						java.lang.String content = reader.getElementText();

						object.setBusinessSegmentCd(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

						reader.next();
					} // End of if for expected property start element

					else {
						// A start element we are not expecting indicates an
						// invalid parameter was passed
						throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
					}

					while (!reader.isStartElement() && !reader.isEndElement())
						reader.next();

					if (reader.isStartElement() && new javax.xml.namespace.QName("", "businessSegmentDesc").equals(reader.getName())) {
						nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");

						if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
							throw new org.apache.axis2.databinding.ADBException("The element: " + "businessSegmentDesc" + "  cannot be null");
						}

						java.lang.String content = reader.getElementText();

						object.setBusinessSegmentDesc(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

						reader.next();
					} // End of if for expected property start element

					else {
						// A start element we are not expecting indicates an
						// invalid parameter was passed
						throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
					}

					while (!reader.isStartElement() && !reader.isEndElement())
						reader.next();

					if (reader.isStartElement()) {
						// A start element we are not expecting indicates a
						// trailing invalid property
						throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
					}
				} catch (javax.xml.stream.XMLStreamException e) {
					throw new java.lang.Exception(e);
				}

				return object;
			}
		} // end of factory class
	}
}
