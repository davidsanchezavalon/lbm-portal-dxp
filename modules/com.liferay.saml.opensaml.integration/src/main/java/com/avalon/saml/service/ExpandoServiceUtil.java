package com.avalon.saml.service;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.avalon.saml.constants.OktaConstants;
import com.avalon.usergroup.service.OktaUserGroupLocalServiceUtil;
import com.liferay.expando.kernel.model.ExpandoBridge;
import com.liferay.expando.kernel.model.ExpandoColumnConstants;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.model.UserGroup;
import com.liferay.portal.kernel.service.ClassNameLocalServiceUtil;
import com.liferay.portal.kernel.util.GetterUtil;

/**
 * @author Asier del Pozo
 */
public class ExpandoServiceUtil {

	// This class will manage the expando attributes in a simple way, using the expando bridge.
	// No special roles are attached to the attribute.
	
	/**
	 * Updates a expando attribute
	 * @param expandoBridge
	 * @param expandoName
	 * @param expandoValue
	 * @param expandoType
	 */
	public static void updateExpandoAttribute(ExpandoBridge expandoBridge, String expandoName, Serializable expandoValue, int expandoType) {
		try {
			// The expando attribute creation might be moved to another place so it is not checked everytime
			if (!expandoBridge.hasAttribute(expandoName)) {
				expandoBridge.addAttribute(expandoName, expandoType, false);
			}
			expandoBridge.setAttribute(expandoName, expandoValue, false);
		} catch(Exception e) {
			_log.error(e);
		}
	}
	
	// Expando group management
	/**
	 * Checks if the Liferay group is an okta group
	 * @param userGroup
	 * @return
	 */
	public static boolean isOktaGroup(UserGroup userGroup) {
		return GetterUtil.getBoolean(userGroup.getExpandoBridge().getAttribute(OktaConstants.OKTA_GROUP_EXPANDO_ID, false), false);
	}

	/**
	 * Return true if and only if the userGroup is linked with the given oktaUserGroup, checking an attached expando attribute if needed
	 * @param userGroup
	 * @param oktaUserGroup
	 * @return
	 */
	public static boolean isLinkedOktaGroup(UserGroup userGroup, String oktaUserGroup) {
		// The usergroup and the okta user group will share the name
		if (isOktaGroup(userGroup)) {
			return userGroup.getName().equalsIgnoreCase(oktaUserGroup);
		} else {
			return false;
		}
	}

	/**
	 * Updates a user group with the related expando okta attribute
	 * @param userGroup
	 * @param oktaUserGroupName
	 */
	public static void saveOktaUserGroup(UserGroup userGroup, String oktaUserGroupName) {
		updateExpandoAttribute(userGroup.getExpandoBridge(), OktaConstants.OKTA_GROUP_EXPANDO_ID, true, ExpandoColumnConstants.BOOLEAN);		
	}

	/**
	 * Gets the user groups that have been linked with an Okta group, where the user is not member of the group
	 * @param user
	 * @return
	 */
	public static List<UserGroup> getOktaUserGroups(User user) {
		// We need to execute something like:
		/*
		SELECT v.classPK FROM expandovalue v INNER JOIN (expandotable t INNER JOIN expandocolumn c ON t.tableId=c.tableId) ON v.tableId = t.tableId	
		WHERE c.name = ? AND t.companyId = ? AND t.classNameId = ? AND v.classPK NOT IN (
		SELECT u1.userGroupId FROM usergroup u1 INNER JOIN users_usergroups u2 ON u1.userGroupId=u2.userGroupId WHERE u2.userId = ?)
		*/
		// This class needs to call a custom finder. 
		// We are using the existing service to implement the custom finder. This is why the existing service has been modified.
		List<UserGroup> list = new ArrayList<UserGroup>();
		try {
			list = OktaUserGroupLocalServiceUtil.getNonMemberOktaUserGroups(user.getCompanyId(), user.getUserId(), OktaConstants.OKTA_GROUP_EXPANDO_ID, ClassNameLocalServiceUtil.getClassNameId(UserGroup.class));
		} catch (Exception e) {
			_log.error(e);
		}
		return list;
	}

	private static final Log _log = LogFactoryUtil.getLog(ExpandoServiceUtil.class);
}
