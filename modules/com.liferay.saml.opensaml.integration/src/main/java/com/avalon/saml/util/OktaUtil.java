package com.avalon.saml.util;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.ListType;
import com.liferay.portal.kernel.model.ListTypeConstants;
import com.liferay.portal.kernel.service.CountryServiceUtil;
import com.liferay.portal.kernel.service.ListTypeServiceUtil;
import com.liferay.portal.kernel.util.ListUtil;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Asier del Pozo
 */
public class OktaUtil {

	public static String getValueAsString(
		String key, Map<String, List<Serializable>> attributesMap) {

		List<Serializable> values = attributesMap.get(key);

		if (ListUtil.isEmpty(values)) {
			return null;
		}

		return String.valueOf(values.get(0));
	}
	
	public static List<String> getValueAsStringList(
		String key, Map<String, List<Serializable>> attributesMap) {

		List<Serializable> values = attributesMap.get(key);

		if (ListUtil.isEmpty(values)) {
			return null;
		}
		
		List<String> returnValues = new ArrayList<String>();
		
		for (int i=0; i<values.size(); i++) {
			returnValues.add(String.valueOf(values.get(i)));
		}
		
		return returnValues;
	}

	public static long getAddressTypeId(String typeName) {
		try {
			List<ListType> listTypes = ListTypeServiceUtil.getListTypes(ListTypeConstants.CONTACT_ADDRESS);
			for (ListType listType : listTypes) {
				if (listType.getName().equalsIgnoreCase(typeName)) {
					return listType.getListTypeId();
				}
			}
		} catch(Exception e) {
			_log.error(e);
		}
		return 0;
	}

	public static long getAddressCountryId(String countryA2) {
		try {
			return CountryServiceUtil.getCountryByA2(countryA2).getCountryId();
		} catch(Exception e) {
			_log.error(e);
		}
		return 0;
	}
	
	public static long getPhoneTypeId(String typeName) {
		try {
			List<ListType> listTypes = ListTypeServiceUtil.getListTypes(ListTypeConstants.CONTACT_PHONE);
			for (ListType listType : listTypes) {
				if (listType.getName().equalsIgnoreCase(typeName)) {
					return listType.getListTypeId();
				}
			}
		} catch(Exception e) {
			_log.error(e);
		}
		return 0;
	}
	
	private static final Log _log = LogFactoryUtil.getLog(OktaUtil.class);

}
