package com.avalon.saml.constants;

import com.avalon.saml.util.OktaUtil;
import com.liferay.portal.kernel.configuration.Filter;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.PropsUtil;

/**
 * @author Asier del Pozo
 */
public class OktaConstants {
	
	// Groups
	
	public static final String OKTA_GROUPS_NAME = PropsUtil.get("okta.attribute.name.group");
	
	// Okta attributes
	// firstName, lastName and emailAddress are not needed; they are checked by configuration

	public static final String OKTA_MIDDLE_NAME = PropsUtil.get("okta.attribute.name.middleName");
	public static final String OKTA_HONORIFIC_SUFFIX = PropsUtil.get("okta.attribute.name.honorificSuffix");
	public static final String OKTA_PROFESSIONAL_DEGREE = PropsUtil.get("okta.attribute.name.professionalDegree");
	public static final String OKTA_ORGANIZATION_NAME = PropsUtil.get("okta.attribute.name.organizationName");
	public static final String OKTA_PROVIDER_CONTRACT_STATUS = PropsUtil.get("okta.attribute.name.providerContractStatus");
	public static final String OKTA_ADDRESS_1 = PropsUtil.get("okta.attribute.name.address1");
	public static final String OKTA_ADDRESS_2 = PropsUtil.get("okta.attribute.name.address2");
	public static final String OKTA_CITY = PropsUtil.get("okta.attribute.name.city");
	public static final String OKTA_POSTAL_CODE = PropsUtil.get("okta.attribute.name.postalCode");
	public static final String OKTA_STATE = PropsUtil.get("okta.attribute.name.stateProvince");
	public static final String OKTA_PROVIDER_PHONE_NUMBER = PropsUtil.get("okta.attribute.name.providerPhoneNumber");
	public static final String OKTA_NPI_INDIVIDUAL_NUMBER = PropsUtil.get("okta.attribute.name.NPIIndividualNumber");
	public static final String OKTA_NPI_GROUP_NUMBER = PropsUtil.get("okta.attribute.name.NPIGroupNumber");
	public static final String OKTA_TAX_IDENTIFIER = PropsUtil.get("okta.attribute.name.taxIdentifier");
	public static final String OKTA_PROVIDER_SPECIALTY = PropsUtil.get("okta.attribute.name.providerSpecialty");
	public static final String OKTA_PROVIDER_TAXONOMY = PropsUtil.get("okta.attribute.name.providerTaxonomy");
	public static final String OKTA_PHONE_NUMBER = PropsUtil.get("okta.attribute.name.phoneNumber");
	public static final String OKTA_NAME_QUALIFIER = PropsUtil.get("okta.attribute.name.nameQualifier");
	public static final String OKTA_CONTACT_NAME = PropsUtil.get("okta.attribute.name.contactName");
	
	// Liferay constants

	// Address
	public static final long DEFAULT_ADDRESS_TYPE_ID = OktaUtil.getAddressTypeId(PropsUtil.get("liferay.default.address.type"));
	public static final long DEFAULT_ADDRESS_COUNTRY_ID = OktaUtil.getAddressCountryId(PropsUtil.get("liferay.default.address.country.code"));
	public static final boolean DEFAULT_ADDRESS_MAILING = GetterUtil.getBoolean(PropsUtil.get("liferay.default.address.mailing"));
	public static final boolean DEFAULT_ADDRESS_PRIMARY = GetterUtil.getBoolean(PropsUtil.get("liferay.default.address.primary"));
	
	// Phones
	public static final boolean DEFAULT_PHONE_NUMBER_PRIMARY_ = GetterUtil.getBoolean(PropsUtil.get("liferay.default.phone.primary"));
	public static final boolean DEFAULT_PHONE_NUMBER_PRIMARY_1 = GetterUtil.getBoolean(PropsUtil.get("liferay.default.phone.primary", new Filter("phone Number")));
	public static final boolean DEFAULT_PHONE_NUMBER_PRIMARY = GetterUtil.getBoolean(PropsUtil.get("liferay.default.phone.primary", new Filter("phoneNumber")));
	public static final boolean DEFAULT_PROVIDER_PHONE_NUMBER_PRIMARY = GetterUtil.getBoolean(PropsUtil.get("liferay.default.phone.primary", new Filter("providerPhoneNumber")));
	public static final long DEFAULT_PHONE_NUMBER_TYPE_ID = OktaUtil.getPhoneTypeId(PropsUtil.get("liferay.default.phone.type", new Filter("phoneNumber")));
	public static final long DEFAULT_PROVIDER_PHONE_NUMBER_TYPE_ID = OktaUtil.getPhoneTypeId(PropsUtil.get("liferay.default.phone.type", new Filter("providerPhoneNumber")));

	public static final String DEFAULT_UNKNOWN_VALUE = PropsUtil.get("liferay.default.unknown.value");

	public static final String OKTA_GROUP_EXPANDO_ID = PropsUtil.get("expando.okta.group.attribute");

}
