package com.avalon.saml.service;

import com.avalon.saml.constants.OktaConstants;
import com.avalon.saml.util.OktaUtil;
import com.avalon.usergroup.service.OktaUserGroupLocalServiceUtil;
import com.liferay.counter.kernel.service.CounterLocalServiceUtil;
import com.liferay.expando.kernel.model.ExpandoColumnConstants;
import com.liferay.portal.kernel.exception.AddressCityException;
import com.liferay.portal.kernel.exception.AddressStreetException;
import com.liferay.portal.kernel.exception.AddressZipException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Address;
import com.liferay.portal.kernel.model.Contact;
import com.liferay.portal.kernel.model.ListType;
import com.liferay.portal.kernel.model.ListTypeConstants;
import com.liferay.portal.kernel.model.Phone;
import com.liferay.portal.kernel.model.Region;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.AddressLocalServiceUtil;
import com.liferay.portal.kernel.service.ContactLocalServiceUtil;
import com.liferay.portal.kernel.service.ListTypeServiceUtil;
import com.liferay.portal.kernel.service.PhoneLocalServiceUtil;
import com.liferay.portal.kernel.service.RegionServiceUtil;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.UserLocalServiceUtil;
import com.liferay.portal.kernel.service.persistence.ListTypeUtil;
import com.liferay.portal.kernel.util.Validator;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * @author Asier del Pozo
 */
public class OktaUserServiceUtil {

	public static void updateUserAttributes(long userId,
			Map<String, List<Serializable>> attributesMap) {
		// At this point, we are only pairing the attributes that comes from Okta and that can be
		// paired with default Liferay attributes
		// Such as addresses and phone numbers
		// If something else is needed, this is the class you need to change.
		// User attributes
		updateUser(userId, attributesMap);
		// User contact attributes
		updateUserContact(userId, attributesMap);
		// Phones
		updateUserPhoneNumbers(userId, attributesMap);
		// Addresses
		updateUserAddress(userId, attributesMap);
		// Expandos
		updateUserExpandos(userId, attributesMap);
	}

	private static void updateUser(long userId, Map<String, List<Serializable>> attributesMap) {
		try {
			String middleName = OktaUtil.getValueAsString(OktaConstants.OKTA_MIDDLE_NAME, attributesMap);
			User user = UserLocalServiceUtil.getUser(userId);
			user.setMiddleName(middleName);
			UserLocalServiceUtil.updateUser(user);
		} catch (Exception ex) {
			_log.error(ex);
		}
	}

	private static void updateUserContact(long userId, Map<String, List<Serializable>> attributesMap) {
		try {
			User user = UserLocalServiceUtil.getUser(userId);
			Contact contact = user.getContact();
			String suffix = OktaUtil.getValueAsString(OktaConstants.OKTA_HONORIFIC_SUFFIX, attributesMap);
			if (Validator.isNotNull(suffix)) {
				// get suffix id from suffix
				long suffixId = 0;
				List<ListType> suffixes = ListTypeServiceUtil.getListTypes(ListTypeConstants.CONTACT_SUFFIX);
				for (ListType type : suffixes) {
					if (type.getName().equalsIgnoreCase(suffix)) {
						suffixId = type.getListTypeId();
						break;
					}
				}
				if (suffixId == 0) {
					// Create new suffix
					// Exceptionally in this case, Liferay doesn't consider the id as long, but as int
					suffixId = Integer.parseInt(String.valueOf(CounterLocalServiceUtil.increment(ListType.class.getName(), 1)));
					// Default ids are created manually using portal-data-common.sql. There could be a potential issue if the suffix id is between 10000 and 12020.
					while (suffixId>9999 && suffixId<12021) {
						suffixId = Integer.parseInt(String.valueOf(CounterLocalServiceUtil.increment(ListType.class.getName(), 100)));
					}
					ListType newSuffix = ListTypeUtil.create(suffixId);
					newSuffix.setName(suffix.toLowerCase());
					newSuffix.setType(ListTypeConstants.CONTACT_SUFFIX);
					//ListTypeUtil.update(newSuffix);
					OktaUserGroupLocalServiceUtil.update(newSuffix);
				}
				contact.setSuffixId(suffixId);
			} else {
				contact.setSuffixId(0);
			}
			ContactLocalServiceUtil.updateContact(contact);
		} catch(Exception ex) {
			_log.error(ex);
		}
	}

	private static void updateUserPhoneNumbers(long userId,
			Map<String, List<Serializable>> attributesMap) {
		// Remove previous phone numbers, if any
		
		try {
			User user = UserLocalServiceUtil.getUser(userId);
			List<Phone> phones = PhoneLocalServiceUtil.getPhones(user.getCompanyId(), Contact.class.getName(), user.getContactId());
			for (Phone phone : phones) {
				PhoneLocalServiceUtil.deletePhone(phone);
			}
		} catch(Exception e) {
			// To control if Not Found Exception is send from Liferay
		}
		// Create new phone numbers
		addPhoneNumber(userId, OktaUtil.getValueAsString(OktaConstants.OKTA_PHONE_NUMBER, attributesMap), OktaConstants.DEFAULT_PHONE_NUMBER_PRIMARY, OktaConstants.DEFAULT_PHONE_NUMBER_TYPE_ID);
		addPhoneNumber(userId, OktaUtil.getValueAsString(OktaConstants.OKTA_PROVIDER_PHONE_NUMBER, attributesMap), OktaConstants.DEFAULT_PROVIDER_PHONE_NUMBER_PRIMARY, OktaConstants.DEFAULT_PROVIDER_PHONE_NUMBER_TYPE_ID);
	}
	
	private static void addPhoneNumber(long userId, String phoneNumber, boolean primary, long typeId) {
		if (Validator.isNotNull(phoneNumber)) {
			String extension = null;
			try {
				User user = UserLocalServiceUtil.getUser(userId);
				PhoneLocalServiceUtil.addPhone(user.getUserId(), Contact.class.getName(), user.getContactId(), phoneNumber, extension, typeId, primary, new ServiceContext());
			} catch (Exception e) {
				_log.error(e);
			}
		}
	}

	private static void updateUserAddress(long userId, Map<String, List<Serializable>> attributesMap) {
		try {
			String street1 = OktaUtil.getValueAsString(OktaConstants.OKTA_ADDRESS_1, attributesMap);
			String street2 = OktaUtil.getValueAsString(OktaConstants.OKTA_ADDRESS_2, attributesMap);
			String city = OktaUtil.getValueAsString(OktaConstants.OKTA_CITY, attributesMap);
			String zip = OktaUtil.getValueAsString(OktaConstants.OKTA_POSTAL_CODE, attributesMap);
			String state = OktaUtil.getValueAsString(OktaConstants.OKTA_STATE, attributesMap);
			String street3 = null;
			// Only US
			long countryId = OktaConstants.DEFAULT_ADDRESS_COUNTRY_ID;
			long regionId = 0; // get the regionId from the state
			List<Region> regions = RegionServiceUtil.getRegions(countryId);
			for (Region region : regions) {
				if (region.getName().equalsIgnoreCase(state) || region.getRegionCode().equalsIgnoreCase(state)) {
					regionId = region.getRegionId();
					break;
				}
			}
			// What happens if the region is not found?
			long typeId = OktaConstants.DEFAULT_ADDRESS_TYPE_ID;
			boolean mailing = OktaConstants.DEFAULT_ADDRESS_MAILING;
			boolean primary = OktaConstants.DEFAULT_ADDRESS_PRIMARY;
			User user = UserLocalServiceUtil.getUser(userId);
			// We will remove previous addresses and add new ones
			// This will increase the addressid (long type value)
			// Remove previous addresses, if any
			try {
				List<Address> addresses = AddressLocalServiceUtil.getAddresses(user.getCompanyId(), Contact.class.getName(), user.getContactId());
				for (Address address : addresses) {
					AddressLocalServiceUtil.deleteAddress(address);
				}
			} catch(Exception e) {
				// To control if Not Found Exception is send from Liferay
				if (_log.isDebugEnabled()) {
					_log.error(e);
				}
			}
			// Add address
			try {
				if (Validator.isNull(city)) {
					city = OktaConstants.DEFAULT_UNKNOWN_VALUE;
				}
				if (Validator.isNull(street1)) {
					street1 = OktaConstants.DEFAULT_UNKNOWN_VALUE;
				}
				if (Validator.isNull(zip)) {
					zip = OktaConstants.DEFAULT_UNKNOWN_VALUE;
				}
				AddressLocalServiceUtil.addAddress(user.getUserId(), Contact.class.getName(), user.getContactId(), street1, street2, street3, city, zip, regionId, countryId, typeId, mailing, primary, new ServiceContext());
			} catch(AddressStreetException ase) {
				_log.error("Street address attribute "+OktaConstants.OKTA_ADDRESS_1+" should not be empty. Address will not be added: "+
						street1+","+street2+","+city+","+zip+","+regionId+","+countryId+","+typeId+","+mailing+","+primary);
			} catch(AddressCityException ace) {
				_log.error("City address attribute "+OktaConstants.OKTA_CITY+" should not be empty. Address will not be added: "+
						street1+","+street2+","+city+","+zip+","+regionId+","+countryId+","+typeId+","+mailing+","+primary);
			} catch(AddressZipException ace) {
				_log.error("Zip address attribute "+OktaConstants.OKTA_POSTAL_CODE+" should not be empty. Address will not be added: "+
					street1+","+street2+","+city+","+zip+","+regionId+","+countryId+","+typeId+","+mailing+","+primary);
			}
		} catch(Exception ex) {
			_log.error(ex);
		}
	}

	private static void updateUserExpandos(long userId, Map<String, List<Serializable>> attributesMap) {
		updateExpandoAttribute(OktaConstants.OKTA_ORGANIZATION_NAME, OktaConstants.OKTA_ORGANIZATION_NAME, userId, attributesMap);
		updateExpandoAttribute(OktaConstants.OKTA_PROFESSIONAL_DEGREE, OktaConstants.OKTA_PROFESSIONAL_DEGREE, userId, attributesMap);
		updateExpandoAttribute(OktaConstants.OKTA_NPI_INDIVIDUAL_NUMBER, OktaConstants.OKTA_NPI_INDIVIDUAL_NUMBER, userId, attributesMap);
		updateExpandoAttribute(OktaConstants.OKTA_NPI_GROUP_NUMBER, OktaConstants.OKTA_NPI_GROUP_NUMBER, userId, attributesMap);
		updateExpandoAttribute(OktaConstants.OKTA_TAX_IDENTIFIER, OktaConstants.OKTA_TAX_IDENTIFIER, userId, attributesMap);
		updateExpandoAttribute(OktaConstants.OKTA_PROVIDER_CONTRACT_STATUS, OktaConstants.OKTA_PROVIDER_CONTRACT_STATUS, userId, attributesMap);
		updateExpandoAttribute(OktaConstants.OKTA_PROVIDER_SPECIALTY, OktaConstants.OKTA_PROVIDER_SPECIALTY, userId, attributesMap);
		updateExpandoAttribute(OktaConstants.OKTA_PROVIDER_TAXONOMY, OktaConstants.OKTA_PROVIDER_TAXONOMY, userId, attributesMap);
		updateExpandoAttribute(OktaConstants.OKTA_NAME_QUALIFIER, OktaConstants.OKTA_NAME_QUALIFIER, userId, attributesMap);
		updateExpandoAttribute(OktaConstants.OKTA_CONTACT_NAME, OktaConstants.OKTA_CONTACT_NAME, userId, attributesMap);
	}

	private static void updateExpandoAttribute(String oktaAttributeName, String expandoName, long userId, Map<String, List<Serializable>> attributesMap) {
		try {
			String expandoValue = OktaUtil.getValueAsString(oktaAttributeName, attributesMap);
			User user = UserLocalServiceUtil.getUser(userId);
			ExpandoServiceUtil.updateExpandoAttribute(user.getExpandoBridge(), expandoName, expandoValue, ExpandoColumnConstants.STRING);
		} catch(Exception e) {
			_log.error(e);
		}
	}

	private static final Log _log = LogFactoryUtil.getLog(OktaUserServiceUtil.class);

}
