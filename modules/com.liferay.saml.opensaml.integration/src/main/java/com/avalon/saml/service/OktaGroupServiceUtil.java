package com.avalon.saml.service;

import com.avalon.saml.constants.OktaConstants;
import com.avalon.saml.util.OktaUtil;
import com.liferay.portal.kernel.exception.DuplicateUserGroupException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.model.UserGroup;
import com.liferay.portal.kernel.service.UserGroupLocalServiceUtil;
import com.liferay.portal.kernel.service.UserLocalServiceUtil;
import com.liferay.portal.kernel.util.GetterUtil;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * @author Asier del Pozo
 */
public class OktaGroupServiceUtil {

	public static void updateMembership(long userId, Map<String, List<Serializable>> attributesMap) {
		try {
		// Update okta group(s) membership
		// Get groups
		User user = UserLocalServiceUtil.getUser(userId);	
		List<String> oktaUserGroups = OktaUtil.getValueAsStringList(OktaConstants.OKTA_GROUPS_NAME, attributesMap);
		if (_log.isDebugEnabled()) {
			_log.debug("We have "+(oktaUserGroups!=null?oktaUserGroups.size():0)+" groups!");
		}
		// Get user groups in Liferay
		List<UserGroup> currentMembership = UserGroupLocalServiceUtil.getUserUserGroups(user.getUserId());
		boolean found;
		int currentLiferayMembershipCount = currentMembership!=null?currentMembership.size():0;
		int currentOktaMembershipCount = 0;
		int removeOktaMembershipCount = 0; 
		int initialOktaGroupCount = GetterUtil.getInteger(oktaUserGroups.size());
		if (_log.isDebugEnabled()) {
			_log.debug("User "+user.getUserId()+" is member of "+currentLiferayMembershipCount+" groups in Liferay");
		}
		if (currentLiferayMembershipCount>0) {
			for (UserGroup userGroup : currentMembership) {
				found = false;
				if (ExpandoServiceUtil.isOktaGroup(userGroup)) {
					if (_log.isDebugEnabled()) {
						_log.debug(userGroup.getName()+" is an Okta group");
						currentOktaMembershipCount++;
					}
					for (String oktaUserGroup : oktaUserGroups) {
						if (ExpandoServiceUtil.isLinkedOktaGroup(userGroup, oktaUserGroup)) {
							oktaUserGroups.remove(oktaUserGroup);
							found = true;
							break;
						}
					}
					if (!found) {
						// Remove other okta memberships in Liferay
						if (_log.isDebugEnabled()) {
							_log.debug("Membership of "+ user.getUserId() + " will be removed from " + userGroup.getName() + "["+userGroup.getUserGroupId()+"]");
							removeOktaMembershipCount++;
						}
						removeMembership(user, userGroup);
					} else { 
						if (_log.isDebugEnabled()) {
							_log.debug("Membership of "+ user.getUserId() + " in group " + userGroup.getName() + "["+userGroup.getUserGroupId()+"] is already assigned");
						}
					}
				} else {
					if (_log.isDebugEnabled()) {
						_log.debug(userGroup.getName()+" is not an Okta group");
					}
				}
			}
		}
		int newMembershipsCount = oktaUserGroups.size();
		// Add membership if doesn't exist in Liferay. Create group if doesn't exist in Liferay
		addMembership(user, oktaUserGroups);
		if (_log.isDebugEnabled()) {
			_log.debug("**** Statistics ****");
			_log.debug("User "+user.getUserId());
			_log.debug("Okta groups membership: "+initialOktaGroupCount+" ["+currentOktaMembershipCount+" in Liferay, "+removeOktaMembershipCount+" removed, "+newMembershipsCount+" new]");
			_log.debug("Liferay groups membership: "+currentMembership.size()+" ["+currentOktaMembershipCount+" Okta, "+(currentMembership.size()-currentOktaMembershipCount)+" Liferay)");
			_log.debug("********************");
		}
		} catch(Exception e) {
			_log.error(e);
		}
	}
	
	// Group membership addition
	private static void addMembership(User user, List<String> oktaUserGroups) {
		if (oktaUserGroups != null && oktaUserGroups.size()>0) {
			int groupAdded = 0;
			int membershipAdded = 0;
			List<UserGroup> existingOktaUserGroups = ExpandoServiceUtil.getOktaUserGroups(user);
			if (_log.isDebugEnabled()) {
				_log.debug("User '"+user.getUserId()+"' is member of "+(existingOktaUserGroups!=null?existingOktaUserGroups.size():0)+" okta groups in Liferay.");
			}
			for (UserGroup userGroup : existingOktaUserGroups) {
				if (ExpandoServiceUtil.isOktaGroup(userGroup)) {
					for (String oktaUserGroup : oktaUserGroups) {
						if (ExpandoServiceUtil.isLinkedOktaGroup(userGroup, oktaUserGroup)) {
							addMembership(user, userGroup);
							oktaUserGroups.remove(oktaUserGroup);
							if (_log.isDebugEnabled()) {
								membershipAdded++;
							}
							break;
						}
					}
				}
			}
			if (_log.isDebugEnabled()) {
				_log.debug("Membership for existing user groups finished. Creating new groups...");
			}
			for (String oktaUserGroup : oktaUserGroups) {
				UserGroup userGroup = createUserGroup(user, oktaUserGroup);
				if (userGroup!=null) {
					addMembership(user, userGroup);
					if (_log.isDebugEnabled()) {
						groupAdded++;
					}
				}
			}
			if (_log.isDebugEnabled()) {
				_log.debug(groupAdded+" new groups created in Liferay, membership added to "+membershipAdded+" groups");
				if (oktaUserGroups.size()>groupAdded) {
					_log.debug(oktaUserGroups.size()+" new groups should have been created in Liferay; check the logs to look for issues");
				}
			}
		}
	}

	// Liferay Group membership management
	private static UserGroup createUserGroup(User user, String oktaUserGroupName) {
		UserGroup userGroup = null;
		try {
			userGroup = UserGroupLocalServiceUtil.addUserGroup(user.getUserId(), user.getCompanyId(), oktaUserGroupName, null, null);
			// Set expando attribute
			ExpandoServiceUtil.saveOktaUserGroup(userGroup, oktaUserGroupName);
		} catch (DuplicateUserGroupException duge) {
			try {
				if (_log.isDebugEnabled()) {
					_log.debug("Trying to import the user group from okta, the user group already exists in Liferay ("+oktaUserGroupName+")");
					_log.debug("Checking if the user group is marked as okta group...");
				}
				userGroup = UserGroupLocalServiceUtil.fetchUserGroup(user.getCompanyId(), oktaUserGroupName);
				if (ExpandoServiceUtil.isLinkedOktaGroup(userGroup, oktaUserGroupName)) {
					if (_log.isDebugEnabled()) {
						_log.debug("Okta group '"+oktaUserGroupName+"' is already linked with a Liferay group");
					}
				} else {
					if (_log.isDebugEnabled()) {
						_log.debug("Group '"+oktaUserGroupName+"' will be linked to a Liferay group.");
					}
					ExpandoServiceUtil.saveOktaUserGroup(userGroup, oktaUserGroupName);
				}
			} catch (Exception e) {
				_log.error(e);
			}
		} catch (Exception e) {
			_log.error(e);
		}
		return userGroup;
	}

	private static void addMembership(User user, UserGroup userGroup) {
		try {
			if (_log.isDebugEnabled()) {
				_log.debug("Adding membership (user "+user.getUserId()+", group "+userGroup.getUserGroupId()+")");
			}
			UserGroupLocalServiceUtil.addUserUserGroup(user.getUserId(), userGroup.getUserGroupId());
		} catch (SystemException e) {
			_log.error(e);
		}
	}

	private static void removeMembership(User user, UserGroup userGroup) {
		try {
			if (_log.isDebugEnabled()) {
				_log.debug("Removing membership (user "+user.getUserId()+", group "+userGroup.getUserGroupId()+")");
			}
			UserGroupLocalServiceUtil.deleteUserUserGroup(user.getUserId(), userGroup.getUserGroupId());
		} catch (SystemException e) {
			_log.error(e);
		}
	}

	private static final Log _log = LogFactoryUtil.getLog(OktaGroupServiceUtil.class);
	
}
