<%--
/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of the Liferay Enterprise
 * Subscription License ("License"). You may not use this file except in
 * compliance with the License. You can obtain a copy of the License by
 * contacting Liferay, Inc. See the License for the specific language governing
 * permissions and limitations under the License, including but not limited to
 * distribution rights of the Software.
 *
 *
 *
 */
--%>

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@ include file="/html/portal/init.jsp" %>

<%@ page import="java.util.*" %>
<%@ page import="java.io.FileInputStream" %>
<%@ page import="java.io.InputStream" %>
<%@ page import="java.util.Properties" %>

<portlet:defineObjects />

<style>
	.portSelected{
		text-decoration: underline !important;
	}
</style>

<aui:script>
	YUI().ready('aui-node','event',
	  function(Y) {
		if(Y.one('#heading span')){
			var portletTitle = Y.one('#heading span').getHTML();
			if(portletTitle==Y.one('.pta0').getHTML()){
				Y.one('.pta0').addClass('portSelected');
			}
			else if(portletTitle==Y.one('.pta1').getHTML()){
				Y.one('.pta1').addClass('portSelected');
			}
			else if(portletTitle==Y.one('.pta2').getHTML()){
				Y.one('.pta2').addClass('portSelected');
			}
			else if(portletTitle==Y.one('.pta3').getHTML()){
				Y.one('.pta3').addClass('portSelected');
			}
			else if(portletTitle==Y.one('.pta4').getHTML()){
				Y.one('.pta4').addClass('portSelected');
			}
			else if(portletTitle==Y.one('.pta5').getHTML()){
				Y.one('.pta5').addClass('portSelected');
			}
			else if(portletTitle==Y.one('.pta6').getHTML()){
				Y.one('.pta6').addClass('portSelected');
			}
			else if(portletTitle==Y.one('.pta7').getHTML()){
            	Y.one('.pta7').addClass('portSelected');
			}
		}
	});
</aui:script>

<%
PortletCategory portletCategory = (PortletCategory)request.getAttribute(WebKeys.PORTLET_CATEGORY);

List<PortletCategory> portletCategories = ListUtil.fromCollection(portletCategory.getCategories());

portletCategories = ListUtil.sort(portletCategories, new PortletCategoryComparator(locale));

List<Portlet> portlets = new ArrayList<Portlet>();

Set<String> portletIds = portletCategory.getPortletIds();

List<String> list = new LinkedList<String>();

String externalPortletCategory = null;

if (portletCategory.getName().equals("Authorization Navigation")) {
	String warName=com.liferay.portal.kernel.util.PropsUtil.get("avalon.prior.authorization.war.name");
		
	list.add("priorauthproviderinformation" + warName);
	list.add("priorauthprocedureinformation" + warName);
	list.add("priorauthadditionalinformation" + warName);
	list.add("priorauthintakereview" + warName);
	list.add("priorauthnursereview" + warName);
	list.add("priorauthphysicianreview" + warName);
	list.add("priorauthnotification" + warName);
	list.add("priorauthpeertopeer" + warName);
//	list.add("priorauthreconsideration" + warName);
	for (String portletId : list) {
		Portlet portlet = PortletLocalServiceUtil.getPortletById(user.getCompanyId(), portletId);
	 	if ((portlet != null) && PortletPermissionUtil.contains(permissionChecker, layout, portlet, ActionKeys.ADD_TO_PAGE)) {
	 		portlets.add(portlet);
	 		PortletApp portletApp = portlet.getPortletApp();

	 		if (portletApp.isWARFile() && Validator.isNull(externalPortletCategory)) {
	 			PortletConfig curPortletConfig = PortletConfigFactoryUtil.create(portlet, application);
	 			ResourceBundle portletResourceBundle = curPortletConfig.getResourceBundle(locale);
				externalPortletCategory = ResourceBundleUtil.getString(portletResourceBundle, portletCategory.getName());
	 		}
	 	}
	 }
} else if (portletCategory.getName().equals("Post Service Review (PSR) Navigation")) {

	    String warName=com.liferay.portal.kernel.util.PropsUtil.get("avalon.post.service.review.war.name");
		list.add("postserviceproviderinformation" + warName);
		list.add("postserviceprocedureinformation" + warName);
		list.add("postserviceadditionalinformation" + warName);
		list.add("postserviceintakereview" + warName);
		list.add("postservicenursereview" + warName);
		list.add("postservicephysicianreview" + warName);
		list.add("postservicenotification" + warName);
		for (String portletId : list) {
			Portlet portlet = PortletLocalServiceUtil.getPortletById(user.getCompanyId(), portletId);
		 	if ((portlet != null) && PortletPermissionUtil.contains(permissionChecker, layout, portlet, ActionKeys.ADD_TO_PAGE)) {
		 		portlets.add(portlet);
		 		PortletApp portletApp = portlet.getPortletApp();

		 		if (portletApp.isWARFile() && Validator.isNull(externalPortletCategory)) {
		 			PortletConfig curPortletConfig = PortletConfigFactoryUtil.create(portlet, application);
		 			ResourceBundle portletResourceBundle = curPortletConfig.getResourceBundle(locale);
					externalPortletCategory = ResourceBundleUtil.getString(portletResourceBundle, portletCategory.getName());
		 		}
		 	}
		 }

} else {
for (String portletId : portletIds) {
	Portlet portlet = PortletLocalServiceUtil.getPortletById(user.getCompanyId(), portletId);

	if ((portlet != null) && PortletPermissionUtil.contains(permissionChecker, layout, portlet, ActionKeys.ADD_TO_PAGE)) {
		portlets.add(portlet);

		PortletApp portletApp = portlet.getPortletApp();

		if (portletApp.isWARFile() && Validator.isNull(externalPortletCategory)) {
			PortletConfig curPortletConfig = PortletConfigFactoryUtil.create(portlet, application);

			ResourceBundle portletResourceBundle = curPortletConfig.getResourceBundle(locale);

			externalPortletCategory = ResourceBundleUtil.getString(portletResourceBundle, portletCategory.getName());
		}
	}
}

portlets = ListUtil.sort(portlets, new PortletTitleComparator(application, locale));
}
if (!portletCategories.isEmpty() || !portlets.isEmpty()) {
	String title = Validator.isNotNull(externalPortletCategory) ? externalPortletCategory : LanguageUtil.get(request, portletCategory.getName());
%>

	<liferay-ui:panel collapsible="<%= true %>" cssClass="lfr-content-category list-unstyled panel-page-category" extended="<%= true %>" title="<%= title %>">
		<aui:nav cssClass="list-group">

			<%
			for (PortletCategory curPortletCategory : portletCategories) {
				request.setAttribute(WebKeys.PORTLET_CATEGORY, curPortletCategory);
			%>

				<liferay-util:include page="/html/portal/layout/view/view_category.jsp" />

			<%
			}
			int count=0;
			for (Portlet portlet : portlets) {
			%>

				<c:if test="<%= !portlet.isInstanceable() %>">

					<%
					PortletURL portletURL = PortletURLFactoryUtil.create(request, portlet.getRootPortlet(), PortletRequest.ACTION_PHASE);

					portletURL.setPortletMode(PortletMode.VIEW);
					portletURL.setWindowState(WindowState.MAXIMIZED);
					%>
					<%
						if (count == 0) {
					%>
					<div>
						<a data-senna-off="true" class="pta<%=count %>" href="<%=portletURL%>"><%=PortalUtil.getPortletTitle(
													portlet, application, locale)%></a>
					</div>
					<%
						} else {
					%>
					<div>
						<a data-senna-off="true" class="pta<%=count %> disabletab inactiveLink" href="<%=portletURL%>"><%=PortalUtil.getPortletTitle(
													portlet, application, locale)%></a>
					</div>
					<%
						}
					%>
					</c:if>

					<%
						count++;
	   				}
					%>
		</aui:nav>
	</liferay-ui:panel>

<%
}
%>