AUI().ready(

	/*
	This function gets loaded when all the HTML, not including the portlets, is
	loaded.
	*/

	function() {
		$(document).ready(function() {

			/* Main Navigation bheaviour function */
			window.updateNaivgation = function() {

				$("#navigation .dropdown").unbind( "click" );
	
				$("#navigation .lfr-nav-item").hover(function () {
						$(this).addClass("hover open");
					},
					function () {
						$(this).removeClass("hover open");
					}
				);
			};
			
			window.updateNaivgation();			
			/* Main Navigation bheaviour function End */
		
		});
	}
);

Liferay.Portlet.ready(

	/*
	This function gets loaded after each and every portlet on the page.

	portletId: the current portlet's id
	node: the Alloy Node object of the current portlet
	*/

	function(portletId, node) {
	}
);

Liferay.on(
	'allPortletsReady',

	/*
	This function gets loaded when everything, including the portlets, is on
	the page.
	*/

	function() {
	}
);

Liferay.on(
	'beforeNavigate', 
	
	/*
	Fires before navigation starts. This event passes a JSON object with 
	the path to the content being navigated to and whether to update 
	the history. Below is an example event payload:
	{ path: '/pages/page1.html', replaceHistory: false }
	*/
	
	function(event) {
//		console.log("Get ready to navigate to :- " + event.path);
	}

);

Liferay.on(
		'startNavigate', 
		
		/*
		Fires when navigation begins. Below is an example event payload:
		{ form: '<form name="form"></form>', path: '/pages/page1.html', replaceHistory: false }
		*/
		
		function(event) {
//			console.log("Get ready to startNavigate to :- " + event.path);
		}

);

Liferay.on(
		'endNavigate', 
		
		/*
		Fired after the content has been retrieved and inserted onto the page.
		This event passes the following JSON object:
		{ form: '<form name="form"></form>', path: '/pages/page1.html' }
		*/
		
		function(event) {
//			console.log("Get ready to endNavigate to :- " + event.path);
			
			window.updateNaivgation();
		}

);