<#assign PropsUtil = staticUtil["com.liferay.portal.kernel.util.PropsUtil"] />
<div class="row">
    <header id="banner" role="banner" class="header-sec">
        <div class="container">

            <div class="row">
                
                <div class="col-md-6">
                
                    <div id="heading">
					
                        <h1 class="header-sec__title">
						<#assign avalon_guest_url = PropsUtil.get("avalon.guest.landing.url") >
                            <a class="${logo_css_class}" href="${avalon_guest_url}" title="<@liferay.language_format arguments="${site_name}" key="go-to-x" />">
                                
								<img alt="${logo_description}" height="${site_logo_height}" src="${site_logo}" width="${site_logo_width}" />
								
                            </a>

                        </h1>
                    </div>

                </div>
				
				<div class="col-md-6">
				
					<#if !is_signed_in>
					
                        <a data-redirect="${is_login_redirect_required?string}" href="${sign_in_url}" id="sign-in" rel="nofollow">${sign_in_text}</a>
						
                    </#if>

                    <div class="user-noti-section pull-right">
						<#assign VOID = freeMarkerPortletPreferences.setValue("portletSetupPortletDecoratorId", "barebone") />

						<@liferay_portlet["runtime"]
							defaultPreferences="${freeMarkerPortletPreferences}"
							portletProviderAction=portletProviderAction.VIEW
							portletProviderClassName="avalon.user.navigation.provider.AvalonUserNavigation"
						/>

					<#assign VOID = freeMarkerPortletPreferences.reset() />
					</div>
					
				</div>

            </div>

        </div>
		
    </header>
	
</div>