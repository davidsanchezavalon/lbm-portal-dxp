<#assign VOID = freeMarkerPortletPreferences.setValue("portletSetupPortletDecoratorId", "borderless")>

	<div class="row primary-links">
		
        <nav class="${nav_css_class} cus-main-navigation clearfix" id="navigation" role="navigation">
        
            <div class="container">
                
                <div class="row">
                    
                    <div class="col-md-12">
                    
                        <@liferay.navigation_menu default_preferences="${freeMarkerPortletPreferences}" />
                            
                    </div>
        
                </div>
            
            </div>
            
        </nav>
		
	</div>
