<div class="row">
    <footer id="footer" role="contentinfo" class="footer-sec">

        <div class="container">
            
            <div class="row">

                <div class="col-md-12">

                    <div class="footer-sec__text">

                        <p>
                            &copy;2018 Avalon Healthcare Solutions, LLC.
                        </p>

                    </div>

                </div>
                
            </div>
			
		</div>

    </footer>
</div>